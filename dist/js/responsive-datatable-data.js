/*Responsive Datatable Init*/

"use strict"; 

$(document).ready(function() {
	
	$('#myTable1').DataTable( {
		responsive: true,
		"columnDefs": [ {
	          "targets": 'no-sort',
	          "orderable": false,
	    } ],
	    "aaSorting": [],
	    "dom": 'Bfrt<"bottom"ilp><"clear">',
		buttons: [
			'excel'
		],
		lengthMenu: [
			[ 10, 25, 50, -1 ],
			[ '10', '25', '50', 'All' ]
		],
	} );

    $('input.myTable1globalfilter').on( 'keyup click', function () {
        $('#myTable1').DataTable().search(
	        $('#myTable1globalfilter').val()
	    ).draw();
    });

    $('#myTable2').DataTable( {
		responsive: true,
		"columnDefs": [ {
	          "targets": 'no-sort',
	          "orderable": false,
	    } ],
	    "aaSorting": [],
	    "dom": 'rt<"bottom"ilp><"clear">',
		buttons: [
			'excel'
		],
		lengthMenu: [
			[ 10, 25, 50, -1 ],
			[ '10', '25', '50', 'All' ]
		],
	} );

    $('input.myTable1globalfilter').on( 'keyup click', function () {
        $('#myTable1').DataTable().search(
	        $('#myTable1globalfilter').val()
	    ).draw();
    });
});

$(document).ready(function() {
	$('#ContactsTblEAD, #LocationsTblEAD, #WorkOrdersTblEAD, #EstimatesTblEAD, #InvoicesTblEAD, #FilesTblEAD, #EventsTblEAD, #TasksTblEAD').DataTable( {
		responsive: true,
		searching: false, 
		paging: false,
		bInfo: false
	} );
} );
