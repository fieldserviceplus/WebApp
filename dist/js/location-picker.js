

function set_location_in_map(Prefix="", latitude=37.8072282, longitude=-121.9947565){
  
	  var map;
	  var marker;
	  var myLatlng = new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude));
	  var geocoder = new google.maps.Geocoder();
	  var infowindow = new google.maps.InfoWindow();
	  var mapOptions = {
	    zoom: 17,
	    center: myLatlng,
	    mapTypeId: google.maps.MapTypeId.ROADMAP
	  };

	  map = new google.maps.Map(document.getElementById(Prefix+"popupmap"), mapOptions);
	  
	  var input = document.getElementById(Prefix+"Location");
	  var searchBox = new google.maps.places.SearchBox(input);

	  // Bias the SearchBox results towards current map's viewport.
	  map.addListener('bounds_changed', function() {
	    searchBox.setBounds(map.getBounds());
	  });
	  
	  var markers = [];
	  
	  marker = new google.maps.Marker({
	    map: map,
	    position: myLatlng,
	    draggable: true
	  });
	  map.setCenter(marker.getPosition());
	  geocoder.geocode({'latLng': myLatlng }, function(results, status) {
	    if (status == google.maps.GeocoderStatus.OK) {
	      if (results[0]) {
	        
	        $("#"+Prefix+"Location").val(results[0].formatted_address);
	       // $('#latitude').val(marker.getPosition().lat());
	       // $('#longitude').val(marker.getPosition().lng());

	      	$(".close").click();
	      }
	    }
	  });
      
	   searchBox.addListener('places_changed', function(results, status) 
	   {
		        var places = searchBox.getPlaces();
		        
		        if (places.length == 0) {
		          return;
		        }
		        
		        // For each place, get the icon, name and location.
		        var bounds = new google.maps.LatLngBounds();
		        places.forEach(function(place) {
		          
			          marker.setPosition(place.geometry.location);
			          
			          geocoder.geocode({'latLng': place.geometry.location}, function(results, status) {
			            if (status == google.maps.GeocoderStatus.OK) {

			             	FillAddressFields(results, marker, Prefix);
			            }
			          });
			          
			          if (place.geometry.viewport) {
			            // Only geocodes have viewport.
			            bounds.union(place.geometry.viewport);
			          } else {
			            bounds.extend(place.geometry.location);
			          }
		        });
		        map.fitBounds(bounds);
		        map.setZoom(17);
		        map.setCenter(marker.getPosition());
	        
	    });
	      
	    google.maps.event.addListener(marker, 'dragend', function() {

	        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
	          if (status == google.maps.GeocoderStatus.OK) {

	          	FillAddressFields(results, marker, Prefix);
	            
	          }
	        });
	    });
 }

function FillAddressFields(results, marker, Prefix)
{ 
	if (results[0]) {  //console.log(results);
	      $("#"+Prefix+"Address").val('');
	     // $("#"+Prefix+"Street").val('');
	      $("#"+Prefix+"City").val('');
	      $("#"+Prefix+"PostalCode").val('');
	      $("#"+Prefix+"State").val('');
	      $("#"+Prefix+"Country").val('');
	      lat = marker.getPosition().lat();
	      lng = marker.getPosition().lng();
	      $("#"+Prefix+"Latitude").val(lat);
	      $("#"+Prefix+"Longitude").val(lng);
	      
	      arrAddress = results[0].address_components;
	      for (ac = 0; ac < arrAddress.length; ac++) { 
	        //if (arrAddress[ac].types[0] == "street_number") { document.getElementById("Address").value = arrAddress[ac].long_name }
	        document.getElementById(Prefix+"Location").value = results[0].formatted_address; 
	
	        if (arrAddress[ac].types[0] == "street_number") { document.getElementById(Prefix+"Address").value = arrAddress[ac].long_name+' ' }
	        if (arrAddress[ac].types[0] == "route") { document.getElementById(Prefix+"Address").value += arrAddress[ac].long_name }
	        if (arrAddress[ac].types[0] == "locality") { document.getElementById(Prefix+"City").value = arrAddress[ac].long_name }
	        if (arrAddress[ac].types[0] == "postal_code") { document.getElementById(Prefix+"PostalCode").value = arrAddress[ac].long_name }
	        if (arrAddress[ac].types[0] == "administrative_area_level_1") { document.getElementById(Prefix+"State").value = arrAddress[ac].long_name; }
	        if (arrAddress[ac].types[0] == "country") { document.getElementById(Prefix+"Country").value = arrAddress[ac].long_name; }
	      }
	      
	      $(".close").click();
    }
}