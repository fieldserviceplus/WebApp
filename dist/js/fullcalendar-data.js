/*FullCalendar Init*/
$(document).ready(function() {
	'use strict';
	
    var drag =  function() { 

        $('.calendar-event').each(function() {

        // store data so the calendar knows to render an event upon drop
        $(this).data('event', {
            title: $.trim($(this).text()), // use the element's text as the event title
            stick: true, // maintain when user navigates (see docs on the renderEvent method)
            WorkOrderID: $(this).data('workorderid'),
            WorkOrderFlag: $(this).data('workorderflag'),
        });
        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 1111999,
            revert: true,      // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });
    });
    };
    
    var removeEvent =  function() {
		$(document).on('click','.remove-calendar-event',function(e) {
			$(this).closest('.calendar-event').fadeOut();
        return false;
    });
    };
    
    $(".add-event").keypress(function (e) { 
        if ((e.which == 13)&&(!$(this).val().length == 0)) {
            $('<div class="btn btn-success calendar-event">' + $(this).val() + '<a href="javascript:void(0);" class="remove-calendar-event"><i class="ti-close"></i></a></div>').insertBefore(".add-event");
            $(this).val('');
        } else if(e.which == 13) {
            alert('Please enter event name');
        }
        drag();
        removeEvent();
    });
    
    
    drag();
    removeEvent();
    
    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth();
    var year = date.getFullYear();
    
    $('#calendar').fullCalendar({
       
        header: 
        {
            left: 'prev,next today',
            center: 'title',
            /*right: 'month,agendaWeek,agendaDay'*/
        },
        titleFormat: 'MMMM Do, YYYY',
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar
        eventLimit: true, // allow "more" link when too many events
        eventOverlap: false,
        eventBackgroundColor:'#5fc55f',
        eventBorderColor:'#5fc55f',
        	
        /*eventMouseover: function (data, event, view) {
			var tooltip = '<div class="tooltiptopicevent tooltip tooltip-inner" style="width:auto;height:auto;position:absolute;z-index:10001;">10:00 AM ' + data.title + '</div>';
			$("body").append(tooltip);
            $(this).mouseover(function (e) {
                $(this).css('z-index', 10000);
                $('.tooltiptopicevent').fadeIn('500');
                $('.tooltiptopicevent').fadeTo('10', 1.9);
            }).mousemove(function (e) {
                $('.tooltiptopicevent').css('top', e.pageY + 10);
                $('.tooltiptopicevent').css('left', e.pageX + 20);
            });
        },/*
        eventMouseout: function (data, event, view) {
           // $(this).css('z-index', 8);
            //$('.tooltiptopicevent').remove();
        },*/
        eventClick: function(event, jsEvent, view ) { 
            $.ajax({
                url: 'GetEventTaskDetails/',
                method: "post",
                data: { Type: event.type, ID : event.id},
                dataType: 'html',
                success: function (data) {
                    if (data != '') { 

                        $("#calendarEventDetailModal").modal();
                        $(".calendar-event-title").html(event.type+" Details");
                        $(".calendar-event-data").html(data);

                    } else {
                        alert("Something went wrong.Please try again!");
                        return false;
                    }
                }
            });
        },
        dayClick: function () {
            //tooltip.hide()
        },
        eventResizeStart: function () {
            //tooltip.hide()
        },
        eventDragStart: function () { 
           // tooltip.hide()
        },
        viewRender: function(view, element) { 

            setTimeout(function(){ 
              var title = view.title.replace("â€“", '-');
              $(".showdatefulldate").html(title); 
              $(".fc-center h2").html(title);

            }, 50);
        },
        drop: function(date, jsEvent, ui, resourceId) { 
            //var date = new Date(date);
       
            var date = date.format("YYYY-MM-DD HH:mm:ss");
            var event = $(this).data('event'); 
            var view = $('#calendar').fullCalendar('getView');
             
            $(this).remove();
             
            $.ajax({
                url: 'UpdateWorkOrderDetails/',
                method: "post",
                data: { WorkOrderID : event.WorkOrderID, Date : date, WorkOrderFlag : event.WorkOrderFlag, View : view.name},
                dataType: 'json',
                success: function (result) {

                    if (result) {

                        if($("div[data-unassigned^=1]").length == 0)
                            $("#home_6").html('<hr class="light-grey-hr row mt-10 mb-10"><span class="pull-left inline-block capitalize-font txt-dark mb-10"><div >No unassigned work order available.</div></span><div class="clearfix"></div>');
                        
                        if($("div[data-unscheduled^=1]").length == 0)
                            $("#profile_6").html('<hr class="light-grey-hr row mt-10 mb-10"><span class="pull-left inline-block capitalize-font txt-dark mb-10"><div >No unscheduled work order available.</div></span><div class="clearfix"></div>');

                        return true;    
                    } else {
                        alert("Something went wrong.Please try again!");
                        return false;
                    }
                }
            });
        },
        eventRender: function(event, element) { 
            //prepare the data for dropping as external item 
            if(event.icon){          
                element.find(".fc-title").prepend("<i class='fa fa-"+event.icon+"'></i> ");
            }        
            element.data('event', event);   
        },
        eventDrop: function(event, delta, revertFunc, jsEvent, ui, view) {

            
            if (!confirm("Are you sure you want to edit this event?")) {
              revertFunc();
            } else {
                //console.log(event);
                if(event.end === null){
                    var StartDate = event.start.format("YYYY-MM-DD HH:mm:ss");
                    var EndDate = event.start.format("YYYY-MM-DD HH:mm:ss");
                } else {
                    var StartDate = event.start.format("YYYY-MM-DD HH:mm:ss");
                    var EndDate = event.end.format("YYYY-MM-DD HH:mm:ss");
                }
               //console.log(event);
                $.ajax({
                    url: 'UpdateEventTaskDetails/',
                    method: "post",
                    data: { StartDate : StartDate, EndDate : EndDate, ID : event.id, Type : event.type, View : view.name},
                    dataType: 'json',
                    success: function (result) {
                        if (result) {
                            return true;    
                        } else {
                            alert("Something went wrong.Please try again!");
                            return false;
                        }
                    }
                });
            }
        },
         eventResize: function(event, delta, revertFunc, jsEvent, ui, view) {

            //alert(event.title + " end is now " + event.end.format());

            if (!confirm("Are you sure you want to edit this event?")) {
              revertFunc();
            } else {
                if(event.end === null){
                    var StartDate = event.start.format("YYYY-MM-DD HH:mm:ss");
                    var EndDate = event.start.format("YYYY-MM-DD HH:mm:ss");
                } else {
                    var StartDate = event.start.format("YYYY-MM-DD HH:mm:ss");
                    var EndDate = event.end.format("YYYY-MM-DD HH:mm:ss");
                }
               //console.log(event);
                $.ajax({
                    url: 'UpdateEventTaskDetails/',
                    method: "post",
                    data: { StartDate : StartDate, EndDate : EndDate, ID : event.id, Type : event.type, View : view.name},
                    dataType: 'json',
                    success: function (result) {
                        if (result) {
                            return true;    
                        } else {
                            alert("Something went wrong.Please try again!");
                            return false;
                        }
                    }
                });
            }
        },
        /*events: {
            url: 'Calendar/',
            type: 'POST',
            // data: { CalanderViewForm: $("#CalanderViewForm").serialize() },
            error: function() {
                //alert('there was an error while fetching events!');
            },
            overlap: false,
           // color: 'yellow',   // a non-ajax option
            //textColor: 'black' // a non-ajax option
        },*/
        events: function(start, end, timezone, callback) {

            var start = start.format("YYYY-MM-DD");
            var end = end.format("YYYY-MM-DD");

            $.ajax({
              url: 'Calendar/?start='+start+'&end='+end,
              data: $("#CalanderViewForm").serialize(),
              dataType: 'json',
              method: "get",
              /*success: function(data) {

                $('#calendar').fullCalendar( 'removeEvents');

                $.each(data, function(index, event){
                    $('#calendar').fullCalendar('renderEvent', event);
                });
              }*/
            }).then( function(events) {
                $('#calendar').fullCalendar( 'removeEvents'); 
                callback(events);
            });
        },
        viewDisplay: function () {
            //tooltip.hide()
        },

			/*events: [
                {
                    title: 'Conference',
					start: '2018-09-05',
                    end: '2018-09-09',
                },
                {     // For Disable date
                    start: '2018-09-06',
					end: '2018-09-08',
					overlap: false,
					rendering: 'background',
					color: 'rgba(234, 108, 65, 0.3)'
                }
            ]*/
		});
    
	 /*$('#calendar_small').fullCalendar({
       
            header: {
                left: 'title',
                right: 'prev,next',
			},
			height: 'auto',
            editable: false,
            droppable: false, // this allows things to be dropped onto the calendar
            eventLimit: true, 
			
        eventMouseover: function (data, event, view) {
			var tooltip = '<div class="tooltiptopicevent tooltip tooltip-inner" style="width:auto;height:auto;position:absolute;z-index:10001;">10:00 AM ' + data.title + '</div>';
			$("body").append(tooltip);
            $(this).mouseover(function (e) {
                $(this).css('z-index', 10000);
                $('.tooltiptopicevent').fadeIn('500');
                $('.tooltiptopicevent').fadeTo('10', 1.9);
            }).mousemove(function (e) {
                $('.tooltiptopicevent').css('top', e.pageY + 10);
                $('.tooltiptopicevent').css('left', e.pageX + 20);
            });
        },
        eventMouseout: function (data, event, view) {
            //$(this).css('z-index', 8);
            //$('.tooltiptopicevent').remove();
        },
        dayClick: function () {
            //tooltip.hide()
        },
        eventResizeStart: function () {
            //tooltip.hide()
        },
        eventDragStart: function () {
            //tooltip.hide()
        },
        viewDisplay: function () {
            //tooltip.hide()
        },
			events: [
                {
                    title: 'Conference',
					start: '2017-02-25'
                },
				{
                    title: 'Meetings',
					start: '2017-02-27'
                },
                {
                    start: '2017-02-06',
					end: '2017-02-08',
					overlap: false,
					rendering: 'background',
					color: 'rgba(234, 108, 65, 0.3)'
                },
				{
                    start: '2017-02-19',
					end: '2017-02-19',
					overlap: false,
					rendering: 'background',
					color: 'rgba(234, 108, 65, 0.3)'
                },
				 {
                    title: 'Conference',
					start: '2017-03-21'
                },
				{
                    title: 'party',
					start: '2017-03-22'
                },
                {
                    start: '2017-03-06',
					end: '2017-03-08',
					overlap: false,
					rendering: 'background',
					color: 'rgba(234, 108, 65, 0.3)'
                },
				{
                    start: '2017-03-19',
					end: '2017-03-19',
					overlap: false,
					rendering: 'background',
					color: 'rgba(234, 108, 65, 0.3)'
                },
				{
                    title: 'Conference',
					start: '2017-04-26'
                },
				{
                    title: 'meeting',
					start: '2017-04-21'
                },
                {
                    start: '2017-04-06',
					end: '2017-04-08',
					overlap: false,
					rendering: 'background',
					color: 'rgba(234, 108, 65, 0.3)'
                },
				{
                    start: '2017-04-29',
					end: '2017-04-29',
					overlap: false,
					rendering: 'background',
					color: 'rgba(234, 108, 65, 0.3)'
                }
				,
				{
                    title: 'Conference',
					start: '2017-05-20'
                },
				{
                    title: 'meeting',
					start: '2017-05-01'
                },
                {
                    start: '2017-05-07',
					end: '2017-05-08',
					overlap: false,
					rendering: 'background',
					color: 'rgba(234, 108, 65, 0.3)'
                },
				{
                    start: '2017-05-19',
					end: '2017-05-19',
					overlap: false,
					rendering: 'background',
					color: 'rgba(234, 108, 65, 0.3)'
                }
            ]
		});*/
    
});