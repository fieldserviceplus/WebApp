$(function() { // document ready


    /* initialize the external events
    -----------------------------------------------------------------*/

   /* $('.calendar-event').each(function() {

      // store data so the calendar knows to render an event upon drop
       $(this).data('event', {
            title: $.trim($(this).text()), // use the element's text as the event title
            stick: true, // maintain when user navigates (see docs on the renderEvent method)
            WorkOrderID: $(this).data('workorderid'),
            WorkOrderFlag: $(this).data('workorderflag'),
        }); 

      // make the event draggable using jQuery UI
      $(this).draggable({
        zIndex: 999,
        revert: true,      // will cause the event to go back to its
        revertDuration: 0  //  original position after the drag
      });

    });*/


    var drag =  function() { 
        $('.calendar-event').each(function() {

        // store data so the calendar knows to render an event upon drop
        $(this).data('event', {
            title: $.trim($(this).text()), // use the element's text as the event title
            stick: true, // maintain when user navigates (see docs on the renderEvent method)
            WorkOrderID: $(this).data('workorderid'),
            WorkOrderFlag: $(this).data('workorderflag'),
        });
        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 1111999,
            revert: true,      // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });
    });
    };
    
    var removeEvent =  function() {
    $(document).on('click','.remove-calendar-event',function(e) {
      $(this).closest('.calendar-event').fadeOut();
        return false;
    });
    };
    
    $(".add-event").keypress(function (e) { 
        if ((e.which == 13)&&(!$(this).val().length == 0)) {
            $('<div class="btn btn-success calendar-event">' + $(this).val() + '<a href="javascript:void(0);" class="remove-calendar-event"><i class="ti-close"></i></a></div>').insertBefore(".add-event");
            $(this).val('');
        } else if(e.which == 13) {
            alert('Please enter event name');
        }
        drag();
        removeEvent();
    });
    
    
    drag();
    removeEvent();
    
  
    /* initialize the calendar
    -----------------------------------------------------------------*/

    $('#calendar').fullCalendar({
      schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
      now: new Date(),
      editable: true, // enable draggable events
      droppable: true, // this allows things to be dropped onto the calendar
      eventOverlap: false,
      aspectRatio: 1.8,
      scrollTime: '00:00', // undo default 6am scrollTime
      header: false,
      titleFormat: 'MMMM Do, YYYY',
       header: {
        left: 'prev,next today',
        center: 'title',
        /*right: 'timelineDay,timelineWeek,'*/
      },
      defaultView: 'timelineDay',
      views: {
        timelineWeek: {
          type: 'timeline',
          duration: { days: 7 },
          titleFormat: 'MMMM Do, YYYY',
      /*  slotDuration: '08:00',
          slotLabelInterval:'06:00',
          snapDuration : '07:00:00',
          minTime: "07:00:00",
          maxTime: "24:00:00",*/
        }
      },
      eventClick: function(event, jsEvent, view ) { 
          $.ajax({
              url: 'GetEventTaskDetails/',
              method: "post",
              data: { Type: event.type, ID : event.id},
              dataType: 'html',
              success: function (data) {
                  if (data != '') { 
                       /*swal({
                            title: event.type+" Details",
                            text: data,
                            html: true,
                            customClass: 'swal-wide',
                          });*/

                        $("#ganttEventDetailModal").modal();
                        $(".calendar-event-title").html(event.type+" Details");
                        $(".calendar-event-data").html(data);

                  } else {
                      alert("Something went wrong.Please try again!");
                      return false;
                  }
              }
          });
      },
      viewRender: function(view) {
        setTimeout(function(){
         // var title = view.title.replace("â€“", '-');
          $(".showdatefulldate").html(view.title);   
         // $(".fc-center h2").html(title);

        }, 50);
        
      },
      resourceLabelText: 'Assigned To',
      refetchResourcesOnNavigate: true,
      resources: function(callback) { 
          setTimeout(function() { // necessary hack
            var view = $('#calendar').fullCalendar('getView'); // TODO: update to your selector
            $.ajax({
                url: 'GetAssignToResource',
                dataType: 'json',
                cache: false,
                /*data: {
                    start: view.start.format(),
                    end: view.end.format(),
                    timezone: view.options.timezone
                }*/
                data: $("#GanttViewForm").serialize(),
            }).then( function(resources) {
                callback(resources);
            });
        }, 0);
      },
      events: function(start, end, timezone, callback) { 

            var start = start.format("YYYY-MM-DD");
            var end = end.format("YYYY-MM-DD");

            $.ajax({
              url: 'Gantt/?start='+start+'&end='+end,
              data: $("#GanttViewForm").serialize(),
              dataType: 'json',
              method: "get",
              /*success: function(data) {
                $('#calendar').fullCalendar( 'removeEvents');
                $.each(data, function(index, event){
                    $('#calendar').fullCalendar('renderEvent', event);
                });
              }*/
            }).then( function(events) {
                $('#calendar').fullCalendar( 'removeEvents');
                callback(events);
            });
        },
        /*events: {
            url: 'Gantt/',
            type: 'POST',
            //data: { CalanderViewForm: $("#CalanderViewForm").serialize() },
            error: function() {
                //alert('there was an error while fetching events!');
            },
            overlap: false,
           // color: 'yellow',   // a non-ajax option
           //textColor: 'black' // a non-ajax option
        },*/
      /*[
        { 
          id: 'a', 
          title: 'Michael Parry', 
          eventColor: '#5fc55f' 
        },
      ],
      events: [
        { 
          id: '1', 
          resourceId: '32', 
          start: '2018-09-08 00:00:00', 
          end: '2018-09-08 02:30:00', 
          title: ' WO-001555 PM Maintenance Danville, CA' 
        },
      ],*/
      drop: function(date, jsEvent, ui, resourceId) {

        //console.log('drop', date.format(), resourceId);
        
          var date = date.format("YYYY-MM-DD HH:mm:ss");
          var event = $(this).data('event'); 
          var view = $('#calendar').fullCalendar('getView');

          $(this).remove();

          $.ajax({
              url: 'UpdateWorkOrderDetails/',
              method: "post",
              data: { WorkOrderID : event.WorkOrderID, Date : date, WorkOrderFlag : event.WorkOrderFlag, View : view.name, AssignedTo : resourceId},
              dataType: 'json',
              success: function (result) {
                  if (result) {

                      if($("div[data-unassigned^=1]").length == 0)
                          $("#home_6").html('<hr class="light-grey-hr row mt-10 mb-10"><span class="pull-left inline-block capitalize-font txt-dark mb-10"><div >No unassigned work order available.</div></span><div class="clearfix"></div>');
                        
                      if($("div[data-unscheduled^=1]").length == 0)
                          $("#profile_6").html('<hr class="light-grey-hr row mt-10 mb-10"><span class="pull-left inline-block capitalize-font txt-dark mb-10"><div >No unscheduled work order available.</div></span><div class="clearfix"></div>');

                      return true;    
                  } else {
                      alert("Something went wrong.Please try again!");
                      return false;
                  }
              }
          });
      },
      eventRender: function(event, element) { 
            //prepare the data for dropping as external item
            element.data('event', event);            
      },
      eventReceive: function(event) { // called when a proper external event is dropped
        console.log('eventReceive', event);
      },
      eventDrop: function(event, delta, revertFunc, jsEvent, ui, view) { // called when an event (already on the calendar) is moved
        if (!confirm("Are you sure you want to edit this event?")) {
              revertFunc();
            } else {
                //console.log(event);
                if(event.end === null){
                    var StartDate = event.start.format("YYYY-MM-DD HH:mm:ss");
                    var EndDate = event.start.format("YYYY-MM-DD HH:mm:ss");
                } else {
                    var StartDate = event.start.format("YYYY-MM-DD HH:mm:ss");
                    var EndDate = event.end.format("YYYY-MM-DD HH:mm:ss");
                }
                $.ajax({
                    url: 'UpdateEventTaskDetails/',
                    method: "post",
                    data: { StartDate : StartDate, EndDate : EndDate, ID : event.id, Type : event.type, View : view.name, AssignedTo : event.resourceId},
                    dataType: 'json',
                    success: function (result) {
                        if (result) {
                            return true;    
                        } else {
                            alert("Something went wrong.Please try again!");
                            return false;
                        }
                    }
                });
            }
      },
      eventResize: function(event, delta, revertFunc, jsEvent, ui, view) {

            //alert(event.title + " end is now " + event.end.format());

            if (!confirm("Are you sure you want to edit this event?")) {
              revertFunc();
            } else {
                if(event.end === null){
                    var StartDate = event.start.format("YYYY-MM-DD HH:mm:ss");
                    var EndDate = event.start.format("YYYY-MM-DD HH:mm:ss");
                } else {
                    var StartDate = event.start.format("YYYY-MM-DD HH:mm:ss");
                    var EndDate = event.end.format("YYYY-MM-DD HH:mm:ss");
                }
               //console.log(event);
                $.ajax({
                    url: 'UpdateEventTaskDetails/',
                    method: "post",
                    data: { StartDate : StartDate, EndDate : EndDate, ID : event.id, Type : event.type, View : view.name, AssignedTo : event.resourceId},
                    dataType: 'json',
                    success: function (result) {
                        if (result) {
                            return true;    
                        } else {
                            alert("Something went wrong.Please try again!");
                            return false;
                        }
                    }
                });
            }
        }
    });

});