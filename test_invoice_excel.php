
<?php
$file="demo.xls";
$test="<table  ><tr><td>Cell 1</td><td>Cell 2</td></tr></table>";
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=$file");



$str = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title>Work Order Form Template</title>
	<meta name="generator" content="LibreOffice 6.1.3.2 (Linux)"/>
	<meta name="author" content="Vertex42.com"/>
	<meta name="created" content="2009-04-10T15:20:03"/>
	<meta name="changedby" content="Michael Parry"/>
	<meta name="changed" content="2019-01-02T05:04:42"/>
	<meta name="description" content="(c) 2011-2014 Vertex42 LLC. All Rights Reserved."/>
	<meta name="AppVersion" content="14.0300"/>
	<meta name="Company" content="Vertex42 LLC"/>
	<meta name="Copyright" content="2011-2014 Vertex42 LLC"/>
	<meta name="DocSecurity" content="0"/>
	<meta name="HyperlinksChanged" content="false"/>
	<meta name="LinksUpToDate" content="false"/>
	<meta name="ScaleCrop" content="false"/>
	<meta name="ShareDoc" content="false"/>
	<meta name="Source" content="https://www.vertex42.com/ExcelTemplates/work-order-form.html"/>
	<meta name="Version" content="1.2.1"/>
	
	<style type="text/css">
		body,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Trebuchet MS"; font-size:x-small }
		a.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } 
		a.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } 
		comment { display:none;  } 
	</style>
	
</head>

<body>
<table align="left" cellspacing="0" border="0">
	<colgroup width="66"></colgroup>
	<colgroup width="171"></colgroup>
	<colgroup width="120"></colgroup>
	<colgroup width="119"></colgroup>
	<colgroup span="2" width="96"></colgroup>
	<colgroup width="103"></colgroup>
	<tr>
		<td height="58" align="left" valign=middle><font face="Arial" size=4 color="#2C3B65">&lt;&lt;Company Name / Logo&gt;&gt;</font></td>
		<td align="left" valign=middle><font face="Arial" size=4><br></font></td>
		<td align="left" valign=middle><font face="Arial" size=4><br></font></td>
		<td align="left" valign=middle><font face="Arial" size=4><br></font></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=middle><font face="Arial" size=6 color="#3A5D9C"><br></font></td>
		<td align="right" valign=middle><font face="Arial" size=6 color="#5FC55F">Work Order</font></td>
	</tr>
	<tr>
		<td height="17" align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="right" valign=bottom><b>WO # :</b></td>
		<td style="border-top: 1px solid #bfbfbf; border-bottom: 1px solid #bfbfbf; border-left: 1px solid #bfbfbf; border-right: 1px solid #bfbfbf" align="center" valign=bottom><b>&lt;&lt;WO-123&gt;&gt;</b></td>
	</tr>
	<tr>
		<td height="17" align="left" valign=bottom>&lt;&lt;Street Address&gt;&gt;</td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="right" valign=bottom><b>WO Date :</b></td>
		<td style="border-top: 1px solid #bfbfbf; border-bottom: 1px solid #bfbfbf; border-left: 1px solid #bfbfbf; border-right: 1px solid #bfbfbf" align="center" valign=bottom sdval="43357" sdnum="1033;1033;M/D/YYYY">9/14/2018</td>
	</tr>
	<tr>
		<td height="16" align="left" valign=bottom>&lt;&lt;City, State  Zip Code&gt;&gt;</td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
	</tr>
	<tr>
		<td height="16" align="left" valign=bottom>Phone: &lt;&lt;000-000-0000&gt;&gt;</td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="right" valign=bottom>Requested By: </td>
		<td style="border-top: 1px solid #bfbfbf; border-bottom: 1px solid #bfbfbf; border-left: 1px solid #bfbfbf; border-right: 1px solid #bfbfbf" colspan=2 align="left" valign=bottom>&lt;&lt;Customer Name&gt;&gt;</td>
		</tr>
	<tr>
		<td height="16" align="left" valign=bottom>Fax: &lt;&lt;000-000-0000&gt;&gt;</td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="right" valign=bottom>Customer ID: </td>
		<td style="border-top: 1px solid #bfbfbf; border-bottom: 1px solid #bfbfbf; border-left: 1px solid #bfbfbf; border-right: 1px solid #bfbfbf" colspan=2 align="left" valign=bottom>&lt;&lt;abc123&gt;&gt;</td>
		</tr>
	<tr>
		<td height="16" align="left" valign=bottom>&lt;&lt;Website&gt;&gt;</td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="right" valign=bottom><br></td>
		<td colspan=2 align="left" valign=bottom><br></td>
		</tr>
	<tr>
		<td height="16" align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
	</tr>
	<tr>
		<td colspan=3 height="16" align="left" valign=bottom bgcolor="#4E6EA2"><b><font face="Arial" color="#FFFFFF">JOB DESCRIPTION</font></b></td>
		<td align="left" valign=bottom bgcolor="#4E6EA2"><br></td>
		<td colspan=3 align="left" valign=bottom bgcolor="#4E6EA2"><b><font face="Arial" color="#FFFFFF">BILL TO</font></b></td>
		</tr>
	<tr>
		<td colspan=4 rowspan=5 height="80" align="left" valign=top>&lt;&lt;Enter description of work&gt;&gt;</td>
		<td colspan=3 align="left" valign=bottom>&lt;&lt;Name&gt;&gt;</td>
		</tr>
	<tr>
		<td colspan=3 align="left" valign=bottom>&lt;&lt;Company Name]</td>
		</tr>
	<tr>
		<td colspan=3 align="left" valign=bottom>&lt;&lt;Street Address]</td>
		</tr>
	<tr>
		<td colspan=3 align="left" valign=bottom>&lt;&lt;City, State  Zip Code&gt;&gt;</td>
		</tr>
	<tr>
		<td colspan=3 align="left" valign=bottom>&lt;&lt;Phone&gt;&gt;</td>
		</tr>
	<tr>
		<td height="16" align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #3b4e87; border-left: 1px solid #3b4e87" height="16" align="center" valign=bottom bgcolor="#4E6EA2" sdnum="1033;0;0.00%"><b><font face="Arial" color="#FFFFFF">QTY</font></b></td>
		<td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #3b4e87" align="left" valign=bottom bgcolor="#4E6EA2" sdnum="1033;0;0.00%"><b><font face="Arial" color="#FFFFFF">DESCRIPTION</font></b></td>
		<td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #3b4e87" align="left" valign=bottom bgcolor="#4E6EA2" sdnum="1033;0;0.00%"><b><font face="Arial" color="#FFFFFF"><br></font></b></td>
		<td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #3b4e87" align="left" valign=bottom bgcolor="#4E6EA2" sdnum="1033;0;0.00%"><b><font face="Arial" color="#FFFFFF"><br></font></b></td>
		<td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #3b4e87" align="center" valign=bottom bgcolor="#4E6EA2"><b><font face="Arial" color="#FFFFFF">TAXED</font></b></td>
		<td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #3b4e87" align="center" valign=bottom bgcolor="#4E6EA2"><b><font face="Arial" color="#FFFFFF">UNIT PRICE</font></b></td>
		<td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #3b4e87; border-right: 1px solid #3b4e87" align="center" valign=bottom bgcolor="#4E6EA2"><b><font face="Arial" color="#FFFFFF">LINE TOTAL</font></b></td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" height="16" align="center" valign=bottom sdval="2" sdnum="1033;">2</td>
		<td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=bottom>CX-1000 Part</td>
		<td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #d9d9d9; border-right: 1px solid #000000" align="center" valign=bottom>Y</td>
		<td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom sdval="150" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> 150.00 </td>
		<td style="border-top: 1px solid #3b4e87; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom bgcolor="#F2F2F2" sdval="300" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> 300.00 </td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" height="16" align="center" valign=bottom sdval="5" sdnum="1033;">5</td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=bottom>Adapter - A100</td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-right: 1px solid #000000" align="center" valign=bottom>Y</td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom sdval="50" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> 50.00 </td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom bgcolor="#F2F2F2" sdval="250" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> 250.00 </td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" height="16" align="center" valign=bottom sdval="10" sdnum="1033;">10</td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=bottom>Labor - Per Hour</td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-right: 1px solid #000000" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom sdval="100" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> 100.00 </td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom bgcolor="#F2F2F2" sdval="1000" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> 1,000.00 </td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" height="16" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-right: 1px solid #000000" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom bgcolor="#F2F2F2" sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> -   </td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" height="16" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-right: 1px solid #000000" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom bgcolor="#F2F2F2" sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> -   </td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" height="16" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-right: 1px solid #000000" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom bgcolor="#F2F2F2" sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> -   </td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" height="16" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-right: 1px solid #000000" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom bgcolor="#F2F2F2" sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> -   </td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" height="16" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-right: 1px solid #000000" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom bgcolor="#F2F2F2" sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> -   </td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" height="16" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-right: 1px solid #000000" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom bgcolor="#F2F2F2" sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> -   </td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" height="16" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-right: 1px solid #000000" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom bgcolor="#F2F2F2" sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> -   </td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" height="16" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-right: 1px solid #000000" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom bgcolor="#F2F2F2" sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> -   </td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" height="16" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-right: 1px solid #000000" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom bgcolor="#F2F2F2" sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> -   </td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" height="16" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-right: 1px solid #000000" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom bgcolor="#F2F2F2" sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> -   </td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" height="16" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-right: 1px solid #000000" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #d9d9d9; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom bgcolor="#F2F2F2" sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> -   </td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="16" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"><br></td>
		<td style="border-top: 1px solid #d9d9d9; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" valign=bottom bgcolor="#F2F2F2" sdval="0" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> -   </td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #000000" colspan=4 height="16" align="center" valign=bottom><br></td>
		<td style="border-top: 1px solid #000000" align="left" valign=bottom><font size=1 color="#FFFFFF">[42]</font></td>
		<td style="border-top: 1px solid #000000" align="left" valign=bottom>SUBTOTAL</td>
		<td style="border-top: 1px solid #000000" align="right" valign=bottom bgcolor="#F2F2F2" sdval="1550" sdnum="1033;0;_(&quot;$&quot;* #,##0.00_);_(&quot;$&quot;* \(#,##0.00\);_(&quot;$&quot;* &quot;-&quot;??_);_(@_)"> $1,550.00 </td>
	</tr>
	<tr>
		<td colspan=4 height="16" align="left" valign=bottom bgcolor="#D9D9D9"><b><font face="Arial">Other Comments or Special Instructions</font></b></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom>TAXABLE</td>
		<td align="right" valign=bottom bgcolor="#F2F2F2" sdval="550" sdnum="1033;0;_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)"> 550.00 </td>
	</tr>
	<tr>
		<td style="border-top: 1px solid #808080; border-left: 1px solid #808080; border-right: 1px solid #808080" colspan=4 height="20" align="left" valign=top>1. Payment due upon receipt of invoice.</td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom>TAX RATE</td>
		<td style="border-top: 1px solid #bfbfbf; border-bottom: 1px solid #bfbfbf; border-left: 1px solid #bfbfbf; border-right: 1px solid #bfbfbf" align="right" valign=bottom sdval="0.0875" sdnum="1033;0;0.000%">8.750%</td>
	</tr>
	<tr>
		<td style="border-left: 1px solid #808080; border-right: 1px solid #808080" colspan=4 height="16" align="left" valign=top>2. Special Note #2</td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom>TAX</td>
		<td align="right" valign=bottom bgcolor="#F2F2F2" sdval="48.125" sdnum="1033;0;_(&quot;$&quot;* #,##0.00_);_(&quot;$&quot;* \(#,##0.00\);_(&quot;$&quot;* &quot;-&quot;??_);_(@_)"> $48.13 </td>
	</tr>
	<tr>
		<td style="border-left: 1px solid #808080; border-right: 1px solid #808080" colspan=4 height="16" align="left" valign=top>3. Special Note #3</td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom>S &amp; H</td>
		<td style="border-top: 1px solid #bfbfbf; border-bottom: 1px solid #bfbfbf; border-left: 1px solid #bfbfbf; border-right: 1px solid #bfbfbf" align="right" valign=bottom sdval="0" sdnum="1033;0;_(&quot;$&quot;* #,##0.00_);_(&quot;$&quot;* \(#,##0.00\);_(&quot;$&quot;* &quot;-&quot;??_);_(@_)"> $-   </td>
	</tr>
	<tr>
		<td style="border-left: 1px solid #808080; border-right: 1px solid #808080" colspan=4 height="17" align="left" valign=top><b><br></b></td>
		<td align="left" valign=bottom><br></td>
		<td style="border-bottom: 2px double #000000" align="left" valign=bottom>OTHER</td>
		<td style="border-top: 1px solid #bfbfbf; border-bottom: 2px double #000000; border-left: 1px solid #bfbfbf; border-right: 1px solid #bfbfbf" align="right" valign=bottom sdval="0" sdnum="1033;0;_(&quot;$&quot;* #,##0.00_);_(&quot;$&quot;* \(#,##0.00\);_(&quot;$&quot;* &quot;-&quot;??_);_(@_)"> $-   </td>
	</tr>
	<tr>
		<td style="border-left: 1px solid #808080; border-right: 1px solid #808080" colspan=4 height="17" align="left" valign=top><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><b>TOTAL</b></td>
		<td style="border-top: 2px double #000000" align="right" valign=bottom bgcolor="#D3D9EC" sdval="1598.125" sdnum="1033;0;_(&quot;$&quot;* #,##0.00_);_(&quot;$&quot;* \(#,##0.00\);_(&quot;$&quot;* &quot;-&quot;??_);_(@_)"><b> $1,598.13 </b></td>
	</tr>
	<tr>
		<td style="border-left: 1px solid #808080; border-right: 1px solid #808080" colspan=4 height="16" align="left" valign=top><br></td>
		<td align="left" valign=bottom><br></td>
		<td colspan=2 align="center" valign=top><br></td>
		</tr>
	<tr>
		<td style="border-left: 1px solid #808080; border-right: 1px solid #808080" colspan=4 height="16" align="left" valign=top><br></td>
		<td align="left" valign=bottom><br></td>
		<td colspan=2 align="center" valign=top>Make checks payable to</td>
		</tr>
	<tr>
		<td style="border-bottom: 1px solid #808080; border-left: 1px solid #808080; border-right: 1px solid #808080" colspan=4 height="16" align="left" valign=top><br></td>
		<td align="left" valign=bottom><br></td>
		<td colspan=2 align="center" valign=top><b>&lt;&lt;Enter Company Name&gt;&gt;</b></td>
		</tr>
	<tr>
		<td height="16" align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
	</tr>
	<tr>
		<td height="16" align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
	</tr>
	<tr>
		<td height="16" align="left" valign=top><br></td>
		<td style="border-bottom: 1px dotted #666666" align="left" valign=top><br></td>
		<td style="border-bottom: 1px dotted #666666" align="left" valign=top><br></td>
		<td style="border-bottom: 1px dotted #666666" align="left" valign=top><br></td>
		<td style="border-bottom: 1px dotted #666666" align="left" valign=bottom><br></td>
		<td style="border-bottom: 1px dotted #666666" align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
	</tr>
	<tr>
		<td height="16" align="left" valign=bottom><br></td>
		<td style="border-top: 1px dotted #666666" colspan=5 align="center" valign=bottom><i>I agree that all work has been performed to my satisfaction.</i></td>
		<td align="center" valign=top><b><br></b></td>
	</tr>
	<tr>
		<td height="34" align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="right" valign=bottom><br></td>
		<td colspan=2 align="center" valign=bottom><br></td>
		</tr>
	<tr>
		<td height="34" align="right" valign=bottom>Signature:</td>
		<td style="border-bottom: 1px solid #000000" colspan=3 align="center" valign=bottom><br></td>
		<td align="right" valign=bottom>Date:</td>
		<td style="border-bottom: 1px solid #000000" colspan=2 align="center" valign=bottom><br></td>
		</tr>
	<tr>
		<td height="16" align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
		<td align="left" valign=bottom><br></td>
	</tr>
	<tr>
		<td colspan=7 height="20" align="center" valign=bottom><b><i><font size=3>Thank You For Your Business!</font></i></b></td>
		</tr>
</table>
<img src="d51a3484fc634d08a8874a53d0220fd0_htm_b1ed7f36466079aa.png" width=101 height=57>
<br clear=left>
<!-- ************************************************************************** -->
</body>

</html>';

echo $str;
