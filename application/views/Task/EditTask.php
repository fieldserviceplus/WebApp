<?php $this->load->view('Template/HeaderView'); ?>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['Task']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['Task']['title']?></span>
									<span class="panel-title txt-dark"><?=$TaskData->Subject;?></span>
								</div>
							</div>
						</div>
					</ul>
					<div class="btn-group pull-right">
						<!-- <div class="dropdown">
							<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
							<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
								<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
									<a><span><strong>Task Actions</strong></span></a>
								</li>
								<li> 
									<a href="<?=SITE_URL;?>Task/Actions/CreateTask/<?=$TaskData->TaskID?>/"><span>Copy Task</span></a>
								</li>
								<li>
									<a href="#" onclick="DeleteTask();"><span>Delete Task</span></a>
								</li>
							</ul>
						</div> -->
					</div>
					<div class="btn-group pull-right">
						<a href="<?=SITE_URL;?>Task/Actions/TaskDetails/<?=$TaskData->TaskID?>" class="btn btn-default">Cancel</a>
						<a href="#" class="btn btn-primary" id="EditTaskLink">Save</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
			 	<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Task Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>  -->
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Task/Actions/EditTask/<?=$TaskData->TaskID?>" class="form-horizontal" id="EditTask" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font">
													<strong>Task Information</strong>
													<span class="pull-right" style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong>
													</span>
												</div>
												
												<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-4">Subject<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Subject" id="Subject" value="<?=isset($TaskData->Subject)?$TaskData->Subject:"";?>">
															<?php echo form_error('Subject');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Due Date<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control showdatepicker" name="DueDate" id="DueDate" value="<?=isset($TaskData->DueDate)?$TaskData->DueDate:"";?>">
															<?php echo form_error('DueDate');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Contact</label>
														<div class="col-md-8">
														<select name="Who" id="Who" class="form-control select2" OnChange="GetContactDetails(this.value);">
														<option value="">Select Contact</option>
														<?php $Contacts = $this->ActionsModel->GetContacts();
															foreach ($Contacts as $Contact) { 
																if(isset($TaskData->Who) && $TaskData->Who == $Contact->ContactID){ ?>
																	<option value="<?=$Contact->ContactID?>" selected="selected"><?=$Contact->FirstName.' '.$Contact->LastName?></option>
																<?php } else { ?>
																	<option value="<?=$Contact->ContactID?>"><?=$Contact->FirstName.' '.$Contact->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('Who');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Related To<span class="required-sign">*</span></label>
														<div class="col-md-3">
															<select name="RelatedTo" id="RelatedTo" class="form-control" OnChange="GetRelatedToObjectData(this.value)">
																<option value="">Select Related To</option>
																<option value="Account" <?=($TaskData->RelatedTo=='Account')?"selected=''":"";?>>Account</option>
																<option value="WorkOrder" <?=($TaskData->RelatedTo=='WorkOrder')?"selected=''":"";?>>WorkOrder</option>
																<option value="Estimate" <?=($TaskData->RelatedTo=='Estimate')?"selected=''":"";?>>Estimate</option>
																<option value="Invoice" <?=($TaskData->RelatedTo=='Invoice')?"selected=''":"";?>>Invoice</option>
																<option value="Contact" <?=($TaskData->RelatedTo=='Contact')?"selected=''":"";?>>Contact</option>
															</select>
															<?php echo form_error('RelatedTo');  ?>
														</div>
														<div class="col-md-5">
															<select name="What" id="What" class="form-control SelectWhat select2">
																<option value="" selected="selected">Select Related To</option>
															</select>
															<?php echo form_error('What');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Email</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Email" id="Email" value="<?=isset($TaskData->Email)?$TaskData->Email:"";?>">
															<?php echo form_error('Email');  ?>
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-4">Phone #</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Phone" id="Phone" value="<?=isset($TaskData->Phone)?$TaskData->Phone:"";?>">
															<?php echo form_error('Phone');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Description</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Description" id="Description" value="<?=isset($TaskData->Description)?$TaskData->Description:"";?>">
															<?php echo form_error('Description');  ?>
														</div>
													</div>
													
												</div>
												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-4">Assigned To<span class="required-sign">*</span></label>
														<div class="col-md-8">
														<select name="AssignedTo" id="AssignedTo" class="form-control select2">
														<option value="">Select Assigned To</option>
														<?php $Users = $this->ActionsModel->GetUsers();
															foreach ($Users as $User) { 
																if(isset($TaskData->AssignedTo) && $TaskData->AssignedTo == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else if($this->session->userdata('UserID') == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else { ?>
																	<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('AssignedTo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Type<span class="required-sign">*</span></label>
														<div class="col-md-8">
														<select name="TaskType" id="TaskType" class="form-control">
														<option value="">Select Type</option>
														<?php $TaskTypes = $this->ActionsModel->GetTaskTypes();
															foreach ($TaskTypes as $TaskType) { 
																if(isset($TaskData->TaskTypeID) && ($TaskData->TaskTypeID == $TaskType->TaskTypeID)){ ?>
																	<option value="<?=$TaskType->TaskTypeID?>" selected="selected"><?=$TaskType->TaskType?></option>
																<?php } else { ?>
																	<option value="<?=$TaskType->TaskTypeID?>"><?=$TaskType->TaskType?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('TaskType');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Task Status<span class="required-sign">*</span></label>
														<div class="col-md-8">
														<select name="TaskStatus" id="TaskStatus" class="form-control">
														<option value="">Select Task Status</option>
														<?php $TaskStatus = $this->ActionsModel->GetTaskStatus();
															foreach ($TaskStatus as $TStatus) { 
																if(isset($TaskData->TaskStatusID) && $TaskData->TaskStatusID == $TStatus->TaskStatusID){ ?>
																	<option value="<?=$TStatus->TaskStatusID?>" selected="selected"><?=$TStatus->TaskStatus?></option>
																<?php } else { ?>
																	<option value="<?=$TStatus->TaskStatusID?>"><?=$TStatus->TaskStatus?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('TaskStatus');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Task Priority<span class="required-sign">*</span></label>
														<div class="col-md-8">
														<select name="TaskPriority" id="TaskPriority" class="form-control">
														<option value="">Select Task Priority</option>
														<?php $TaskPriority = $this->ActionsModel->GetTaskPriority();
															foreach ($TaskPriority as $TPriority) { 
																if(isset($TaskData->TaskPriority) && $TaskData->TaskPriority == $TPriority->TaskPriorityID){ ?>
																	<option value="<?=$TPriority->TaskPriorityID?>" selected="selected"><?=$TPriority->Priority?></option>
																<?php } else { ?>
																	<option value="<?=$TPriority->TaskPriorityID?>"><?=$TPriority->Priority?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('TaskPriority');  ?>
														</div>
													</div>

												</div>
											</div>

											<!-- Start Custom Fields Row -->
											<?php $this->load->view('Setup/Customize/Fields/CustomFieldsForm', array('Object' => 'Task', 'ObjectID' => $TaskData->TaskID)); ?>
											<!-- End Custom Fields Row -->

											<div class="col-md-12">

												<div class="txt-dark capitalize-font pt-10"><strong>System Information</strong></div>
												<hr class="light-grey-hr">
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-4">Create Date</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$TaskData->CreatedDate?>" readonly="">
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-4">Created By</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$TaskData->CreatedByName?>" readonly="">
														</div>
													</div>
												</div>
												<div class="col-md-6">

												   <div class="form-group">
														<label class="control-label col-md-4">Last Modified Date</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$TaskData->LastModifiedDate?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Last Modified By</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$TaskData->LastModifiedByName?>" readonly="">
														</div>
													</div>
												</div>
											</div>


										</div>
											<!-- /Row -->
									</div>

									<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="EditTask" id="EditTaskBtn">Save</button>
															<a href="<?=SITE_URL;?>Task/Actions/TaskDetails/<?=$TaskData->TaskID?>" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			</div>
			<div id="profile_13" class="tab-pane fade" role="tabpanel">
				
				<?php //$this->load->view('Task/Include/RelatedInformation'); ?>
				
			</div>
		</div>
		
		</div>

		<!-- <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
			<?php //$this->load->view('Task/Include/ActivityTimeline'); ?>
		</div> -->

	</div>
	<!-- /Row -->

</div>



<?php //$this->load->view('Task/Modal/CreateNoteView'); ?>

<?php //$this->load->view('Task/Modal/LocationPickerView'); ?>

<!-- End Modal -->

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<!-- <script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>
 -->
<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>
		
<script type="text/javascript">

$(document).ready(function () {

$(document).on('click', "#EditTaskLink", function () {
	$('#EditTaskBtn').click(); 
});

    $('#EditTask').validate({ 
        rules: {
            Subject: {
                required: true
            },
            RelatedTo: {
                required: true
            },
            What: {
                required: true
            },
            AssignedTo: {
                required: true
            },
           /* Who: {
                required: true
            },*/
            TaskStatus: {
                required: true
            },
            TaskPriority: {
                required: true
            },
            TaskType: {
                required: true
            },
            DueDate: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
}); 

function DeleteTask(TaskID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#5fc55f",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>Task/Actions/DeleteTask/<?=$TaskData->TaskID;?>/";
		}, 500);
    });
	return false;
}

function GetRelatedToObjectData(RelatedTo, What = 0){
	$.ajax({
        url: '<?=SITE_URL?>Task/Actions/GetRelatedToObjectOptions/'+RelatedTo+'/'+What,
        method: "post",
        data: { RelatedTo : RelatedTo, What : What},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$(".SelectWhat").html(result);

               	return true;  	
            } else {
                $(".SelectWhat").html('');
                return false;
            }
        }
    });
}
$(document).ready(function () {
	GetRelatedToObjectData('<?=$TaskData->RelatedTo?>', '<?=$TaskData->What?>');
	
});

function GetContactDetails(ContactID){
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/GetContactDetails/'+ContactID,
        method: "post",
      /*  data: { AccountID : AccountID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$("#Phone").val(result.PhoneNo);
            	$("#Email").val(result.Email);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}
</script>




