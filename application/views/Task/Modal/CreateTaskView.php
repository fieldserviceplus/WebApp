<div class="modal fade" id="CreateNewTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Create New Task</h5>
			</div>
			<form class="form-horizontal" id="NewTaskForm" method="post" action="<?=SITE_URL;?>Account/Actions/CreateTask">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						 <div class="form-group">
							<label class="control-label col-md-3">Assigned To</label>
							<div class="col-md-9">
								<select name="AssignedTo" id="AssignedTo" class="form-control select2">
								<option value="">Select Owner</option>
								<?php $Users = $this->ActionsModel->GetUsers();
									foreach ($Users as $User) { 
										if($User->UserID == $this->session->userdata('UserID')){ ?>
											<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
										<?php } else { ?>
											<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
										<?php } ?>
									<?php } ?>
								</select>
								<input type="hidden" name="AccountID" value="<?=$AccountData->AccountID?>">
							</div>
						</div>
					    <div class="form-group">
							<label class="control-label col-md-3">Subject</label>
							<div class="col-md-9">
								<input type="text" name="Subject" value="" class="form-control">
							</div>
						</div>
					    <div class="form-group">
							<label class="control-label col-md-3">Description</label>
							<div class="col-md-9">
								<input type="text" name="Description" value="" class="form-control">
							</div>
						</div>
					    <div class="form-group">
							<label class="control-label col-md-3">Type</label>
							<div class="col-md-9">
								<select name="TaskType" id="TaskType" class="form-control">
								<option value="">Select Task Type</option>
								<?php $TaskTypes = $this->ActionsModel->GetTaskTypes();
									foreach ($TaskTypes as $TaskType) { ?>
										<option value="<?=$TaskType->TaskTypeID?>"><?=$TaskType->TaskType?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					    <div class="form-group">
							<label class="control-label col-md-3">Status</label>
							<div class="col-md-9">
								<select name="TaskStatus" id="TaskStatus" class="form-control">
								<?php $TaskStatus = $this->ActionsModel->GetTaskStatus();
									foreach ($TaskStatus as $value) { ?>
										<option value="<?=$value->TaskStatusID?>"><?=$value->TaskStatus?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Priority</label>
							<div class="col-md-9">
								<select name="TaskPriority" id="TaskPriority" class="form-control">
								<?php $Priorities = $this->ActionsModel->GetTaskPriorities();
									foreach ($Priorities as $Priority) { ?>
										<option value="<?=$Priority->TaskPriorityID?>"><?=$Priority->Priority?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					    <div class="form-group">
							<label class="control-label col-md-3">Due Date</label>
							<div class="col-md-9">
								<input type="text" name="DueDate" value="<?=date("m/d/Y h:i A");?>" class="form-control showdatetimepicker">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Name</label>
							<div class="col-md-9">
								<select name="Who" id="Who" class="form-control select2">
								<option value="">Select Contact</option>
								<?php $Contacts = $this->ActionsModel->GetContacts();
									foreach ($Contacts as $Contact) { ?>
										<option value="<?=$Contact->ContactID?>"><?=$Contact->FirstName?> <?=$Contact->LastName?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Related To</label>
							<div class="col-md-9">
								<input type="text" value="<?=$AccountData->AccountName?>" class="form-control" readonly="">
								<input type="hidden" name="RelatedTo" value="Account">
								<input type="hidden" name="What" value="<?=$AccountData->AccountID?>">
							</div>
						</div>
					    <div class="form-group">
							<label class="control-label col-md-3">Is Recurring</label>
							<div class="col-md-9 ">
								<div class="checkbox checkbox-primary">
									<input id="IsRecurringTask" name="IsRecurring" type="checkbox" value="1">
									<label for="checkbox2"></label>
									<input id="WORecurrenceID" name="WORecurrenceID" type="hidden" class="WORecurrenceIDForTask">
								</div>
							</div>
						</div>
					</div>
				</div>
				 
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="CreateTask">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>