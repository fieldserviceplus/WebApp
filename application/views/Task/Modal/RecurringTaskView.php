
<!-- modal -->
<div class="modal fade" id="RecurringModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="RecurrenceClose"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Recurrence Settings</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" id="RecurringForm">
				<input id="WORecurrenceID" name="WORecurrenceID" type="hidden" class="WORecurrenceID" value="">
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">

							<div class="form-group">
								<label class="control-label col-md-3">Start On</label>
								<div class="col-md-6">
									<input type="text" name="StartOn" class="form-control" id="StartOn" required="" value="<?=date("m/d/Y");?>" readonly="">
								</div>
							</div>

						    <div class="form-group">
								<label class="control-label col-md-3">Repeats</label>
								<div class="col-md-6">
									<select name="RepeatEvery" class="form-control" onchange="RepeatsChange(this.value)" required="">
										<option value="Daily">Daily</option>
										<option value="Weekly">Weekly</option>
										<option value="Monthly">Monthly</option>
										<option value="Yearly">Yearly</option>
										<!-- <option value="Periodically">Periodically</option> -->
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Interval every</label>
								<div class="col-md-3">
										<input class="vertical-spin" type="text" data-bts-button-down-class="btn btn-default" data-bts-button-up-class="btn btn-default" data-plugin="touchSpin" name="IntervalEvery" required="" value="1" readonly="">
								</div>
								<label class="control-label col-md-4 interval_every_txt" style="text-align:left;">day on</label>
							</div>

							<div class="form-group" id="days-cb" style="display: none;">
							   <label class="control-label col-md-3">Repeat on</label>
								<div class="col-md-1">
									<div class="checkbox checkbox-primary">
										<input id="sun_chb" type="checkbox" name="RepeatOn[]" value="Sun" checked="">
										<label for="sun_chb">S</label>
									</div>
								</div>
								<div class="col-md-1">
									<div class="checkbox checkbox-primary">
										<input id="mon_chb" type="checkbox" name="RepeatOn[]" value="Mon">
										<label for="mon_chb">M</label>
									</div>
								</div>
								<div class="col-md-1">
									<div class="checkbox checkbox-primary">
										<input id="tue_chb" type="checkbox" name="RepeatOn[]" value="Tue">
										<label for="tue_chb">T</label>
									</div>
								</div>
								<div class="col-md-1">
									<div class="checkbox checkbox-primary">
										<input id="wed_chb" type="checkbox" name="RepeatOn[]" value="Wed">
										<label for="wed_chb">W</label>
									</div>
								</div>
								<div class="col-md-1">
									<div class="checkbox checkbox-primary">
										<input id="thu_chb" type="checkbox" name="RepeatOn[]" value="Thu">
										<label for="thu_chb">T</label>
									</div>
								</div>
								<div class="col-md-1">
									<div class="checkbox checkbox-primary">
										<input id="fri_chb" type="checkbox" name="RepeatOn[]" value="Fri">
										<label for="fri_chb">F</label>
									</div>
								</div>
								<div class="col-md-1">
									<div class="checkbox checkbox-primary">
										<input id="sat_chb" type="checkbox" name="RepeatOn[]" value="Sat">
										<label for="sat_chb">S</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Ends</label>
								<div class="col-md-9">
									<div class="radio radio-primary">
										<input id="radio1" type="radio" name="Ends" value="Never">
										<label for="radio1">Never</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"></label>
								<div class="col-md-9">
									<div class="radio radio-primary col-md-2">
										<input id="radio2" type="radio" name="Ends" value="On">
										<label for="radio2">On</label>
									</div>
									<div class="col-md-6">
										<input type="text" id="EndsOnDate" name="EndsOnDate" class="form-control" value="<?=date("m/d/Y");?>">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3"></label>
								<div class="col-md-9">
									<div class="radio radio-primary col-md-2">
										<input id="radio3" type="radio" name="Ends" value="After" checked="">
										<label for="radio3">After</label>
									</div>
									<div class="col-md-3">
										<!-- <div class="form-group"> -->
											<input class="vertical-spin-occurrence" type="text" data-bts-button-down-class="btn btn-default" data-bts-button-up-class="btn btn-default" data-plugin="touchSpin" name="EndsAfterOccurrences" value="100">
										<!-- </div> -->
									</div>
									<div class="col-md-3">
										<label class="control-label" style="text-align:left;">occurences</label>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Start Time</label>
								<div class="col-md-6">
									<input type="text" name="StartTime" class="form-control timepicker">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">End Time</label>
								<div class="col-md-6">
									<input type="text" name="EndTime" class="form-control timepicker">
								</div>
							</div>

						</div>
					</div>
				</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" id="RecurrenceClose">Close</button>
				<button type="button" class="btn btn-primary" id="RecurrenceSave">Save</button>
			</div>
		</div>
	</div>
</div>

