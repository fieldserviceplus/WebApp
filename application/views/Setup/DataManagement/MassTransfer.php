<?php $this->load->view('Template/HeaderView')?>

<!-- Bootstrap Dropify CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" type="text/css"/>

<div class="container-fluid pt-25">

<!-- Row -->
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">Mass Transfer Records</h6>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default card-view">
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
					<div class="row">
					<div class="col-md-12">
					<div class="form-wrap">
					<form action="<?=SITE_URL;?>Setup/DataManagement/Transfer/FindRecords/" class="form-horizontal" id="MassTransferForm" method="post">	
						<div class="form-body">

						<p>This process allows you to mass transfer ownership of records from one user to another.</p>
						
						<div class="txt-dark capitalize-font pt-10"><strong>Step 1: What Object Records do you want to transfer?</strong></div>
						<hr class="light-grey-hr">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-3 col-md-offset-3">
									<div class="form-group">
										<select class="form-control" name="Object" id="Object" required="" onchange="GetViewFields();" required="">
											<option value="">List of Objects</option>
											<option value="Account" <?=(isset($Object) && $Object == "Account") ? "selected=''" : ""; ?>>Account</option>
											<option value="Contact" <?=(isset($Object) && $Object == "Contact") ? "selected=''" : ""; ?>>Contact</option>
											<option value="WorkOrder" <?=(isset($Object) && $Object == "WorkOrder") ? "selected=''" : ""; ?>>Work Order</option> 
											<option value="Estimate" <?=(isset($Object) && $Object == "Estimate") ? "selected=''" : ""; ?>>Estimate</option>
											<option value="Invoice" <?=(isset($Object) && $Object == "Invoice") ? "selected=''" : ""; ?>>Invoice</option>
											<option value="Task" <?=(isset($Object) && $Object == "Task") ? "selected=''" : ""; ?>>Task</option>
									 		<option value="Event" <?=(isset($Object) && $Object == "Event") ? "selected=''" : ""; ?>>Event</option>
										</select> 
									</div>
								</div>
							</div>
						</div>
						<div class="txt-dark capitalize-font pt-10"><strong>Step 2: Select Users</strong></div>
						<hr class="light-grey-hr">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-4 col-md-offset-2">
										<div class="form-group">
											<div class="form-group">
												<label class="control-label col-md-3">Transfer from</label>
												<div class="col-md-9">
													<select name="TransferFrom" class="form-control select2" required="">
														<option value="">Select User</option>
														<?php $Users = MY_Model::getAllUsers();
														foreach ($Users as $key => $User) { 
															if(isset($TransferFrom) && $User->UserID == $TransferFrom) { ?>
																<option value="<?=$User->UserID?>" selected=""><?=$User->FirstName?> <?=$User->LastName?></option>
															<?php } else { ?>
																<option value="<?=$User->UserID?>"><?=$User->FirstName?> <?=$User->LastName?></option>	
															<?php } ?>
														<?php } ?>
													</select>
													<?php echo form_error('TransferFrom');  ?>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Transfer to</label>
												<div class="col-md-9">
													<select name="TransferTo" class="form-control select2" required="">
														<option value="">Select User</option>
														<?php $Users = MY_Model::getAllUsers();
														foreach ($Users as $key => $User) { 
															if(isset($TransferTo) && $User->UserID == $TransferTo) { ?>
																<option value="<?=$User->UserID?>" selected=""><?=$User->FirstName?> <?=$User->LastName?></option>
															<?php } else { ?>
																<option value="<?=$User->UserID?>"><?=$User->FirstName?> <?=$User->LastName?></option>	
															<?php } ?>
														<?php } ?>
													</select>
													<?php echo form_error('TransferTo');  ?>
												</div>
											</div>
										</div>	
									</div>
								</div>
							</div>

							<div class="txt-dark capitalize-font pt-10"><strong>Step 2: Find Records to Transfer</strong></div>
							<hr class="light-grey-hr">
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-8 col-md-offset-2">
											<div class="col-md-12 pb-10">Enter Additional Filters Criteria (optional):</div>

											<?php if(isset($_POST['FilterFields']) && !empty($_POST['FilterFields'])){  
												foreach($_POST['FilterFields'] as $key => $value){ ?>

													<div class="form-group">
														<div class="col-md-1"></div>
														<div class="col-md-2">
															<select class="form-control FilterFields" name="FilterFields[]" id="FilterFields">
																<option value="">Select Filter Field</option>
															</select>
														</div>
														<div class="col-md-2">
															<select class="form-control" name="FilterConditions[]">

															<option value="Equals" <?=($_POST['FilterConditions'][$key] == "Equals")?"selected=''":"";?>>Equals</option>
															<option value="NotEqualTo" <?=($_POST['FilterConditions'][$key] == "NotEqualTo")?"selected=''":"";?> >Not Equal To</option>
															<option value="LessThan" <?=($_POST['FilterConditions'][$key] == "LessThan")?"selected=''":"";?>>Less Than</option>
															<option value="GreaterThan" <?=($_POST['FilterConditions'][$key] == "GreaterThan")?"selected=''":"";?>>Greater Than</option>
															<option value="LessOREqualTo" <?=($_POST['FilterConditions'][$key] == "LessOREqualTo")?"selected=''":"";?>>Less OR Equal To</option>
															<option value="GreaterOREqualTo" <?=($_POST['FilterConditions'][$key] == "GreaterOREqualTo")?"selected=''":"";?>>Greater OR Equal To</option>
															<option value="Contains" <?=($_POST['FilterConditions'][$key] == "Contains")?"selected=''":"";?>>Contains</option>
															<option value="StartsWith" <?=($_POST['FilterConditions'][$key] == "StartsWith")?"selected=''":"";?>>Starts With</option>
															<option value="DoesNotContain" <?=($_POST['FilterConditions'][$key] == "DoesNotContain")?"selected=''":"";?>>Does Not Contain</option>
															</select>
														</div>
														<div class="col-md-3">
															<input type="text" class="form-control" name="FilterValues[]" value="<?=$_POST['FilterValues'][$key]?>">
														</div>
														<div class="col-md-1 btn-group btn-group-vertical pl-0">
															<button type="button" id="" class="btn btn-default btn-icon-anim btn-circle remove_field_filter_criteria"><i class="fa fa-close"></i></button>
														</div>
													</div>
												<?php } } ?>

												<div class="input_fields_wrap" >
												</div>

												<div class="col-md-12 pb-15">
													<button type="button" class="btn btn-default btn-outline add_field_button">Add Filter +</button>
												</div>
										</div>
									</div>
								</div>
						
							<div class="form-actions mt-10">
								<div class="row">
									<div class="col-md-12">
										<div class="row">
											<div class="text-center">
												<a href="<?=SITE_URL;?>Setup/DataManagement/Transfer/" class="btn btn-default">Cancel</a>
												<button type="submit" class="btn btn-primary mr-10" name="FindRecords">Find</button>
											</div>
										</div>
									</div>
								</div>
							</div>
					</div>
					</form>

					<?php if (isset($Records)){ ?>
					<hr class="light-grey-hr">
					<form action="<?=SITE_URL;?>Setup/DataManagement/Transfer/MassTransfer/" class="form-horizontal" id="MassTransferFrm" method="post">

					<input type="hidden" name="Object" value="<?=$Object?>">
					<input type="hidden" name="TransferFrom" value="<?=$TransferFrom?>">
					<input type="hidden" name="TransferTo" value="<?=$TransferTo?>">

					<div class="form-body">

						<div class="form-actions mt-10">
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="text-center">
											<button type="submit" class="btn btn-primary mr-10" name="FindRecords">Transfer</button>
										</div>
									</div>
								</div>
							</div>
						</div>

						<?php if($Object == 'Account'){ ?>

								<div class="table-responsive">
									<table id="massTransferTable" class="table table-hover display  pb-30" width="100%">
										<thead>
											<tr>
												<th class="text-center no-sort"><div class="checkbox checkbox-primary">
												<input type="checkbox" name="select_all" value="1" id="example-select-all">
												<label for="checkbox2"></label>
												</div></th>
												<th class="text-center">Account Name</th>
												<th class="text-center">Type</th>
												<th class="text-center">Billing City</th>
												<th class="text-center">Billing State</th>
												<th class="text-center">Created Date</th>
												<th class="text-center">Last Modified Date</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($Records as $key => $Record) { ?>
												<tr>
													<td class="text-center"><div class="checkbox checkbox-primary pt-0 pb-0">
													<input type="checkbox" name="ID[]" value="<?=$Record->AccountID?>" class="pb-0 tbl-checkbox">
													<label for="checkbox2"></label>
													</div></td>
													<td><a href="<?=SITE_URL;?>Account/Actions/AccountDetails/<?=$Record->AccountID?>"><?=$Record->AccountName?></a></td>
													<td class="text-center"><?=$Record->AccountType?></td>
													<td class="text-center"><?=$Record->BillingCity?></td>
													<td class="text-center"><?=$Record->BillingState?></td>
													<td class="text-center"><?=$Record->CreatedDate?></td>
													<td class="text-center"><?=$Record->LastModifiedDate?></td>
												</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>

						<?php }  
							  else if($Object == 'Contact')
							  { ?>

								<div class="table-responsive">
									<table id="massTransferTable" class="table table-hover display  pb-30" width="100%">
										<thead>
											<tr>
												<th class="text-center no-sort"><div class="checkbox checkbox-primary">
												<input type="checkbox" name="select_all" value="1" id="example-select-all">
												<label for="checkbox2"></label>
												</div></th>
												<th class="text-center">Contact Name</th>
												<th class="text-center">Phone No</th>
												<th class="text-center">City</th>
												<th class="text-center">State</th>
												<th class="text-center">Created Date</th>
												<th class="text-center">Last Modified Date</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($Records as $key => $Record) { ?>
												<tr>
													<td class="text-center"><div class="checkbox checkbox-primary pt-0 pb-0">
													<input type="checkbox" name="ID[]" value="<?=$Record->ContactID?>" class="pb-0 tbl-checkbox">
													<label for="checkbox2"></label>
													</div></td>
													<td><a href="<?=SITE_URL;?>Contact/Actions/ContactDetails/<?=$Record->ContactID?>"><?=$Record->ContactName?></a></td>
													<td class="text-center"><?=$Record->PhoneNo?></td>
													<td class="text-center"><?=$Record->MailingCity?></td>
													<td class="text-center"><?=$Record->MailingState?></td>
													<td class="text-center"><?=$Record->CreatedDate?></td>
													<td class="text-center"><?=$Record->LastModifiedDate?></td>
												</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
						<?php }  
							  else if($Object == 'WorkOrder')
							  { ?>

								<div class="table-responsive">
									<table id="massTransferTable" class="table table-hover display  pb-30" width="100%">
										<thead>
											<tr>
												<th class="text-center no-sort"><div class="checkbox checkbox-primary">
												<input type="checkbox" name="select_all" value="1" id="example-select-all">
												<label for="checkbox2"></label>
												</div></th>
												<th class="text-center">Subject</th>
												<th class="text-center">Type</th>
												<th class="text-center">City</th>
												<th class="text-center">State</th>
												<th class="text-center">Created Date</th>
												<th class="text-center">Last Modified Date</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($Records as $key => $Record) { ?>
												<tr>
													<td class="text-center"><div class="checkbox checkbox-primary pt-0 pb-0">
													<input type="checkbox" name="ID[]" value="<?=$Record->WorkOrderID?>" class="pb-0 tbl-checkbox">
													<label for="checkbox2"></label>
													</div></td>
													<td><a href="<?=SITE_URL;?>WorkOrder/Actions/WorkOrderDetails/<?=$Record->WorkOrderID?>"><?=$Record->Subject?></a></td>
													<td class="text-center"><?=$Record->WorkOrderType?></td>
													<td class="text-center"><?=$Record->City?></td>
													<td class="text-center"><?=$Record->State?></td>
													<td class="text-center"><?=$Record->CreatedDate?></td>
													<td class="text-center"><?=$Record->LastModifiedDate?></td>
												</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
						<?php }   
							  else if($Object == 'Estimate')
							  { ?>

								<div class="table-responsive">
									<table id="massTransferTable" class="table table-hover display pb-30" width="100%">
										<thead>
											<tr>
												<th class="text-center no-sort"><div class="checkbox checkbox-primary">
												<input type="checkbox" name="select_all" value="1" id="example-select-all">
												<label for="checkbox2"></label>
												</div></th>
												<th class="text-center">Estimate Name</th>
												<th class="text-center">Account</th>
												<th class="text-center">City</th>
												<th class="text-center">State</th>
												<th class="text-center">Created Date</th>
												<th class="text-center">Last Modified Date</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($Records as $key => $Record) { ?>
												<tr>
													<td class="text-center"><div class="checkbox checkbox-primary pt-0 pb-0">
													<input type="checkbox" name="ID[]" value="<?=$Record->EstimateID?>" class="pb-0 tbl-checkbox">
													<label for="checkbox2"></label>
													</div></td>
													<td><a href="<?=SITE_URL;?>Estimate/Actions/EstimateDetails/<?=$Record->EstimateID?>"><?=$Record->EstimateName?></a></td>
													<td class="text-center"><?=$Record->AccountName?></td>
													<td class="text-center"><?=$Record->BillingCity?></td>
													<td class="text-center"><?=$Record->BillingState?></td>
													<td class="text-center"><?=$Record->CreatedDate?></td>
													<td class="text-center"><?=$Record->LastModifiedDate?></td>
												</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
						<?php }  
							  else if($Object == 'Task')
							  { ?>
								<div class="table-responsive">
									<table id="massTransferTable" class="table table-hover display  pb-30" width="100%">
										<thead>
											<tr>
												<th class="text-center no-sort"><div class="checkbox checkbox-primary">
												<input type="checkbox" name="select_all" value="1" id="example-select-all">
												<label for="checkbox2"></label>
												</div></th>
												<th class="text-center">Subject</th>
												<th class="text-center">Name</th>
												<th class="text-center">Date</th>
												<th class="text-center">Status</th>
												<th class="text-center">Created Date</th>
												<th class="text-center">Last Modified Date</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($Records as $key => $Record) { ?>
												<tr>
													<td class="text-center"><div class="checkbox checkbox-primary pt-0 pb-0">
													<input type="checkbox" name="ID[]" value="<?=$Record->TaskID?>" class="pb-0 tbl-checkbox">
													<label for="checkbox2"></label>
													</div></td>
													<td><a href="<?=SITE_URL;?>Task/Actions/TaskDetails/<?=$Record->TaskID?>"><?=$Record->Subject?></a></td>
													<td class="text-center"><?=$Record->ContactName?></td>
													<td class="text-center"><?=$Record->Date?></td>
													<td class="text-center"><?=$Record->TaskStatus?></td>
													<td class="text-center"><?=$Record->CreatedDate?></td>
													<td class="text-center"><?=$Record->LastModifiedDate?></td>
												</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
						<?php } 
							  else if($Object == 'Invoice')
							  { ?>
								<div class="table-responsive">
									<table id="massTransferTable" class="table table-hover display  pb-30" width="100%">
										<thead>
											<tr>
												<th class="text-center no-sort"><div class="checkbox checkbox-primary">
												<input type="checkbox" name="select_all" value="1" id="example-select-all">
												<label for="checkbox2"></label>
												</div></th>
												<th class="text-center">IN #</th>
												<th class="text-center">Account Name</th>
												<th class="text-center">City</th>
												<th class="text-center">Status</th>
												<th class="text-center">Created Date</th>
												<th class="text-center">Last Modified Date</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($Records as $key => $Record) { ?>
												<tr>
													<td class="text-center"><div class="checkbox checkbox-primary pt-0 pb-0">
													<input type="checkbox" name="ID[]" value="<?=$Record->InvoiceID?>" class="pb-0 tbl-checkbox">
													<label for="checkbox2"></label>
													</div></td>
													<td><a href="<?=SITE_URL;?>Invoice/Actions/InvoiceDetails/<?=$Record->InvoiceID?>"><?=$Record->InvoiceNo?></a></td>
													<td class="text-center"><?=$Record->AccountName?></td>
													<td class="text-center"><?=$Record->City?></td>
													<td class="text-center"><?=$Record->State?></td>
													<td class="text-center"><?=$Record->CreatedDate?></td>
													<td class="text-center"><?=$Record->LastModifiedDate?></td>
												</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
						<?php }
							  else if($Object == 'Event')
							  { ?>
								<div class="table-responsive">
									<table id="massTransferTable" class="table table-hover display  pb-30" width="100%">
										<thead>
											<tr>
												<th class="text-center no-sort"><div class="checkbox checkbox-primary">
												<input type="checkbox" name="select_all" value="1" id="example-select-all">
												<label for="checkbox2"></label>
												</div></th>
												<th class="text-center">Subject</th>
												<th class="text-center">Type</th>
												<th class="text-center">Start Date</th>
												<th class="text-center">End Date</th>
												<th class="text-center">Created Date</th>
												<th class="text-center">Last Modified Date</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($Records as $key => $Record) { ?>
												<tr>
													<td class="text-center"><div class="checkbox checkbox-primary pt-0 pb-0">
													<input type="checkbox" name="ID[]" value="<?=$Record->EventID?>" class="pb-0 tbl-checkbox">
													<label for="checkbox2"></label>
													</div></td>
													<td><a href="<?=SITE_URL;?>Event/Actions/EventDetails/<?=$Record->EventID?>"><?=$Record->Subject?></td>
													<td class="text-center"><?=$Record->EventTypeName?></td>
													<td class="text-center"><?=$Record->StartDate?></td>
													<td class="text-center"><?=$Record->EndDate?></td>
													<td class="text-center"><?=$Record->CreatedDate?></td>
													<td class="text-center"><?=$Record->LastModifiedDate?></td>
												</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
						<?php }  ?>
				
						</div>
						</form>
					<?php } ?>

						</div>
						</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->
								
</div>	
<!-- Start Modal -->

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {

	<?php if(isset($_POST['FilterFields']) && !empty($_POST['FilterFields'])){  
		foreach($_POST['FilterFields'] as $key => $value){ ?>
			setTimeout(function(){ 
				$('.FilterFields').eq('<?=$key?>').val('<?=$value?>');
			}, 2000);
	<?php }} else { ?>
			setTimeout(function(){  $(".btn-outline.add_field_button").trigger("click"); }, 500);
	<?php } ?>

	<?php if(isset($_POST['Object'])){ ?>
		setTimeout(function(){ GetViewFields() }, 600);
	<?php } ?>
 
    $('#MassTransferForm').validate({ 
        /*rules: {
            AssignedTo: {
                required: true
            },
        },*/
        errorPlacement: function(){
            return false;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    /* Add new filter fields */
    var max_fields      = 5; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
   
    var x = 1; //initlal text box count
    $(".add_field_button").click(function(e){ //on add input button click
        e.preventDefault();

        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="form-group" >'
					+'<div class="col-md-1"></div>'
					+'<div class="col-md-2">'
					+'	<select class="form-control FilterFields" name="FilterFields[]" id="FilterFields">'
					+'		<option value="">Select Filter Field</option>'
					+'	</select>'
					+'</div>'
					+'<div class="col-md-2">'
					+'		<select class="form-control" name="FilterConditions[]">'
					+'	<option value="Equals">Equals</option>'
					+'	<option value="NotEqualTo">Not Equal To</option>'
					+'	<option value="LessThan">Less Than</option>'
					+'	<option value="GreaterThan">Greater Than</option>'
					+'	<option value="LessOREqualTo">Less OR Equal To</option>'
					+'	<option value="GreaterOREqualTo">Greater OR Equal To</option>'
					+'	<option value="Contains">Contains</option>'
					+'	<option value="StartsWith">Starts With</option>'
					+'	<option value="DoesNotContain">Does Not Contain</option>'
					+'</select>'
					+'</div>'
					+'<div class="col-md-3">'
					+'	<input type="text" class="form-control" name="FilterValues[]">'
					+'</div>'
					+'<div class="col-md-1 btn-group btn-group-vertical pl-0">'
					+	 '<button type="button" id="" class="btn btn-default btn-icon-anim btn-circle remove_field_filter_criteria"><i class="fa fa-close"></i></button>'
					+'</div>'
				+'</div>'); //add input box
		}
		GetViewFields(1);
    });
   
    $(document).on("click",".remove_field_filter_criteria", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.form-group').remove(); x--;
    });
});

function GetViewFields(last_node = 0) {

	var Object = $("#Object").val();
	if(Object != ''){
		$.ajax({
	        url: '<?=SITE_URL?>Setup/DataManagement/Transfer/GetViewFields/' + Object,
	        type: "POST",
	       /* data: {ObjectID : },*/
	        dataType: 'html',
	        success: function(data)
	        {  
	        	if(last_node == 0)
	            	$(".FilterFields").html(data);
	            else 
	            	$(".FilterFields:last").html(data);
	        }
	    });
	}
}

$(document).ready(function (){
   var table = $('#massTransferTable').DataTable({
		'columnDefs': [{
		 "targets": 'no-sort',
		 'orderable': false,
		/* 'className': 'dt-body-center',
		 'render': function (data, type, full, meta){
		     return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
		 }*/
		}],
		searching: false,
		responsive: true,
		"aaSorting": [],
		"dom": 'frt<ilp><"clear">',
		lengthMenu: [
			[ 10, 25, 50, -1 ],
			[ '10', '25', '50', 'All' ]
		]
   });

   // Handle click on "Select all" control
   $('#example-select-all').on('click', function(){
      // Get all rows with search applied
      var rows = table.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('input[type="checkbox"]', rows).prop('checked', this.checked);

      if(!this.checked){ 
      		$('.chk-hidden').remove();
      }
   });

   // Handle click on checkbox to set state of "Select all" control
   $('#massTransferTable tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         $('input[value='+this.value+'].chk-hidden').remove();
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control
            // as 'indeterminate'
            el.indeterminate = true;
         }
      }
   });

   // Handle form submission event
   $('#MassTransferFrm').on('submit', function(e){

   	$('.chk-hidden').remove();
    var form = this;

      // Iterate over all checkboxes in the table
      table.$('input[type="checkbox"].tbl-checkbox').each(function(){
         // If checkbox doesn't exist in DOM
         if(!$.contains(document, this)){
            // If checkbox is checked
            if(this.checked){
               // Create a hidden element
               $(form).append(
                  $('<input>')
                     .attr('type', 'hidden')
                     .attr('class', 'chk-hidden')
                     .attr('name', this.name)
                     .val(this.value)
               );
            }
         }
      });
   });
});

</script>