<?php $this->load->view('Template/HeaderView')?>


<!-- Bootstrap Dropify CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" type="text/css"/>

<div class="container-fluid pt-25">

<!-- Row -->
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">Import Your Data</h6>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default card-view">
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
					<div class="row">
					<div class="col-md-12">
					<div class="form-wrap">
					<form action="<?=SITE_URL;?>Setup/DataManagement/Import/UploadFile/" class="form-horizontal" id="UploadDataForm" method="post" enctype="multipart/form-data">	
						<div class="form-body">

						<p>You can import up to 25,000 records at a time.</p>
						
						<div class="txt-dark capitalize-font pt-10"><strong>Step 1: What type of data are you importing?</strong></div>
						<hr class="light-grey-hr">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<select class="form-control" name="Object" id="Object" required="">
											<option value="">List of Objects</option>
											<option value="Account">Account</option>
											<option value="Contact">Contact</option>
											<option value="WorkOrder">Work Order</option>
											<option value="Invoice">Invoice</option>
											<option value="Estimate">Estimate</option>
											<option value="Task">Task</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="txt-dark capitalize-font pt-10"><strong>Step 2: Where is your data located?</strong></div>
						<hr class="light-grey-hr">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-3">
										<div class="form-group">
											<input type="file" id="input-file-now" class="dropify" name="UploadedFile" required="" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>
										</div>	
									</div>

									<div class="col-md-9" id="download_formate">
										
									</div>
								</div>
							</div>
						
							<div class="form-actions mt-10">
								<div class="row">
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-offset-5 col-md-6">
												<a href="<?=SITE_URL;?>Setup/DataManagement/Import/" class="btn btn-default">Cancel</a>
												<button type="submit" class="btn btn-primary mr-10" name="UploadFile">Next</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						</form>
						</div>
						</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->
								
</div>	
<!-- Start Modal -->

<?php $this->load->view('Template/FooterView')?>

<!-- Bootstrap Daterangepicker JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/dropify/dist/js/dropify.min.js"></script>

<!-- Form Flie Upload Data JavaScript -->
<script src="<?=SITE_URL?>dist/js/form-file-upload-data.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
 
    $('#UploadDataForm').validate({ 
        /*rules: {
            AssignedTo: {
                required: true
            },
        },*/
        errorPlacement: function(){
            return false;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    $("#Object").change( function() {
    	if($(this).val() == 'Account'){
    		$("#download_formate").html('<a href="<?=SITE_URL;?>import_file_formats/Account.xlsx">Download import file formats</a>');  	
    	} else if($(this).val() == 'Contact'){
    		$("#download_formate").html('<a href="<?=SITE_URL;?>import_file_formats/Contact.xlsx">Download import file formats</a>');  	
    	} else if($(this).val() == 'WorkOrder'){
    		$("#download_formate").html('<a href="<?=SITE_URL;?>import_file_formats/WorkOrder.xlsx">Download import file formats</a>');  	
    	} else if($(this).val() == 'Invoice'){
    		$("#download_formate").html('<a href="<?=SITE_URL;?>import_file_formats/Invoice.xlsx">Download import file formats</a>');  	
    	} else if($(this).val() == 'Estimate'){
    		$("#download_formate").html('<a href="<?=SITE_URL;?>import_file_formats/Estimate.xlsx">Download import file formats</a>');  	
    	} else if($(this).val() == 'Task'){
    		$("#download_formate").html('<a href="<?=SITE_URL;?>import_file_formats/Task.xls">Download import file formats</a>');  	
    	} else {
    		$("#download_formate").html('');  	
    	}
    });
});
</script>