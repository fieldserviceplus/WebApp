<?php $this->load->view('Template/HeaderView');?>


<div class="container-fluid pt-25">

<!-- Row -->
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">Import Your Data</h6>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default card-view">
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
					<div class="row">
					<div class="col-md-12">
					<div class="form-wrap">
					<form action="<?=SITE_URL;?>Setup/DataManagement/Import/ImportData/" class="form-horizontal" id="MapDataForm" method="post" enctype="multipart/form-data">	
						<div class="form-body">

						<input type="hidden" name="Object" value="<?=$Object?>">
						<input type="hidden" name="FileName" value="<?=$FileName?>">

						<div class="txt-dark capitalize-font pt-10"><strong>Step 3 - Map Your Data to Service Hub</strong>
							<div class="pull-right">
								<span class="denotes-required-field-txt"><span class="required-sign">*</span> Denotes required field</span>
							</div>
						</div>
						<hr class="light-grey-hr">

						<p>Your file has been auto-mapped to existing Service Hub fields, but you can edit the mappings if you wish. Unmapped fields will not be imported.</p>

						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table id="userTable" class="table table-hover display pb-30" width="100%">
										<thead>
											<tr>
												<th class="text-center">CSV Labels</th>
												<th class="text-center">Service Hub Field</th>
												<th class="text-center">Edit</th>
											</tr>
										</thead>
										<tbody>
											<?php $RequiredColumns = $this->ImportModel->GetRequiredColumnsArray($Object);

											foreach ($ExcelColumns as $key => $ExcelColumn) {?>
											<tr>
												<td><?=$ExcelColumn?><input type="hidden" name="ExcelColumnKeys[]" value="<?=$key?>"></td>	
												<?php if(in_array($DBColumns[$key], $RequiredColumns)){?>
														<td id="td_<?=$key?>"><span id="DBColumnName<?=$key?>"><?=$DBColumns[$key]?><span class="required-sign">*</span></span><input type="hidden" name="DBColumnNames[<?=$key?>]" id="DBColumnKey<?=$key?>" value="<?=$DBColumns[$key]?>"></td>	
												<?php } else { ?>
														<td id="td_<?=$key?>"><span id="DBColumnName<?=$key?>"><?=$DBColumns[$key]?></span><input type="hidden" name="DBColumnNames[<?=$key?>]" id="DBColumnKey<?=$key?>" value="<?=$DBColumns[$key]?>"></td>
												<?php } ?>
												<td class="text-center"><a href="#" onclick="ChangeMapField('<?=$DBColumns[$key]?>', <?=$key?>, '<?=$ExcelColumn?>')"><i class="fa fa-pencil-square-o"></i></a></td>	
											</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>

						<div class="form-actions mt-10">
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-offset-7 col-sm-offset-6 col-lg-offset-8 col-xs-offset-1 pull-right"> 
											<a href="<?=SITE_URL;?>Setup/DataManagement/Import/" class="btn btn-default">Cancel</a>
											<a href="#" onclick="window.history.go(-1);" class="btn btn-default">Previous</a>
											<button type="submit" class="btn btn-primary mr-10" name="ImportData">Start Import</button>
										</div>
									</div>
								</div>
							</div>
						</div>

							
						</div>
						</form>
						</div>
						</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->
								
</div>	


<!-- Start Modal -->
<!-- modal -->
<div class="modal fade" id="change-map-field-modal" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Map field: <span id="MapFieldName"></span></h5>
			</div>
			<form class="form-horizontal" method="post" action="">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-3">View Name</label>
								<div class="col-md-9">
									<input type="hidden" id="ExcelColumnKey">
									<select class="form-control select2" id="MapDBFieldID" name="MapDBFieldID">
									<option value="UNMAPPED">Un-Mapped</option>
									<?php $ObjFields = $this->ImportModel->GetObjFields($Object);
									foreach ($ObjFields as $key => $ObjField) { ?>
											<option value="<?=$ObjField->FieldName?>"><?=$ObjField->FieldName?></option>
									<?php } ?>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="SaveMapField();">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>

<?php $this->load->view('Template/FooterView');?>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
 
    $('#CreateCustomFieldForm1').validate({ 
        /*rules: {
            AssignedTo: {
                required: true
            },
        },*/
        errorPlacement: function(){
            return false;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
});

function ChangeMapField(DBColumnKey, ExcelColumnKey, ExcelColumnName)
{
	$("#change-map-field-modal").modal();
	$("#MapFieldName").html(ExcelColumnName);
	$("#ExcelColumnKey").val(ExcelColumnKey);
	$("#MapDBFieldID").val(DBColumnKey);
	$('#MapDBFieldID').val(DBColumnKey).trigger('change');
	
}

function SaveMapField()
{
	var MapDBFieldID = $("#MapDBFieldID").val();
	var ExcelColumnKey = $("#ExcelColumnKey").val();
	$("#DBColumnName"+ExcelColumnKey).html(MapDBFieldID);
	$("#DBColumnKey"+ExcelColumnKey).val(MapDBFieldID);
	if(MapDBFieldID == 'UNMAPPED'){
		$("#td_"+ExcelColumnKey).css('color','red');
	} else {
		$("#td_"+ExcelColumnKey).css('color','#878787');
	}
}
</script>