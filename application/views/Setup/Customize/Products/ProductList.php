
<?php $this->load->view('Template/HeaderView')?>
<style type="text/css">.dataTables_filter { display: none; }</style>

    <div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10 pb-10">
					<h6 class="panel-title txt-dark pt-10">Products</h6>
					
				</div>
				<div  class="panel-wrapper collapse in">
					<div class="panel-body">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 pr-0 pl-0">
							<form method="post" action="<?=SITE_URL?>Estimate/View/" id="SelectedWoForm">
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-3">
									<label class="control-label mt-10"><strong>View:</strong></label>
								</div>
								<div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 pr-0">
									<select name="SelectedEstimate" class="selectpicker" data-style="form-control btn-default btn-outline" onchange="GoToNewView(this.value)"> 
										<option value="AllProducts">All Products</option>
										<option value="ActiveProducts">Active Products</option>
									</select>
								</div>	
							</form>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						</div>
						<div class="col-lg-5 col-md-4 col-sm-3 col-xs-12">
							<div class="btn-group pull-right">
							</div>
							<div class="pull-right">
								<a href="<?=SITE_URL;?>Setup/Customize/Products/CreateProduct/" class="btn btn-primary btn-outline">Add Product</a>
							</div>	
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							
								<div class="table-responsive">
									<table id="productTable" class="table table-hover display pb-30" width="100%">
										<thead>
											<tr> 
												<th class="text-center">Edit</th>
												<th class="text-center">Product Name</th>
												<th class="text-center">Active</th>
												<th class="text-center">Product Family</th>
												<th class="text-center">List Price</th>
												<th class="text-center">Last Modified By</th>
												<th class="text-center">Last Modified</th>
											</tr>
										</thead>
										<tbody id="tblData">
										
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
		</div>
		<!-- /Row -->
	</div>	

<!-- Start Modal -->

<?php $this->load->view('Template/FooterView')?>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript" src="<?=SITE_URL?>dist/js/multiselect.min.js"></script>


<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>
<script type="text/javascript">
/*$(document).ready(function() {
	$('.buttons-excel').hide();
	$("#Export-Excel").on("click", function() {
	    $( '.buttons-excel' ).click();
	});
});*/

function GoToNewView(View){

	var table = $('#productTable').DataTable();

	$.ajax({
	    url: '<?=SITE_URL?>Setup/Customize/Products/ProductList/'+View,
	    data: { View:View },
	    method: "post",
	    dataType: 'html',
	    success: function (result) {
	        if (result != '') { 
	        	
	           table.destroy();

	           $('#tblData').html(result); 
	           
	           table = $('#productTable').DataTable( {
					responsive: true,
					"columnDefs": [ {
				          "targets": 'no-sort',
				          "orderable": false,
				    } ],
				    "aaSorting": [],
				    "dom": 'frt<"bottom"ilp><"clear">',
					lengthMenu: [
						[ 10, 25, 50, -1 ],
						[ '10', '25', '50', 'All' ]
					],
					destroy: false,
				} );

			    $('input.myTable1globalfilter').on( 'keyup click', function () {
			        $('#myTable1').DataTable().search(
				        $('#myTable1globalfilter').val()
				    ).draw();
			    });

	           return true;  	
	        } else {
	        	$('#tblData').html('');
	            //alert("Something went wrong.Please try again!");
	            return false;
	        }
	    }
	});	
}
$(document).ready(function() {
	GoToNewView("AllProducts");
});

/*function ChangeStatus(ProductID, Status){

	alert($('IsActive'+ProductID).is(':checked'));
	$.ajax({
	    url: '<?=SITE_URL?>Setup/Customize/Products/ChangeStatus/'+ProductID,
	    data: { Status:Status },
	    method: "post",
	    dataType: 'html',
	    success: function (result) {

	    }
	});	
}*/

$('body').on('click', '.pckb', function (){

    if($(this).is(':checked'))
        var Status = 1;
    else
        var Status = 0;

    var ProductID = $(this).attr('id');
    $.ajax({
	    url: '<?=SITE_URL?>Setup/Customize/Products/ChangeStatus/'+ProductID,
	    data: { Status:Status },
	    method: "post",
	    dataType: 'html',
	    success: function (result) {

	    }
	});	
});
</script>