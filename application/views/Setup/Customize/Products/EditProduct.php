<?php $this->load->view('Template/HeaderView')?>

<div class="container-fluid pt-25">

	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Edit Product</h6>
					</div>
					<div class="pull-right">
						<span style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong></span>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Setup/Customize/Products/EditProduct/<?=$ProductData->ProductID?>" class="form-horizontal" id="EditProduct" method="post">
										<div class="form-body">

											<!-- /Row -->
											<div class="row">

												<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>Product Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Product Name<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="ProductName" id="ProductName" value="<?=isset($ProductData->ProductName)?$ProductData->ProductName:"";?>">
															<?php echo form_error('ProductName');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Product Code</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="ProductCode" id="ProductCode" value="<?=isset($ProductData->ProductCode)?$ProductData->ProductCode:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Product Family<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="ProductFamily" id="ProductFamily" class="form-control select2">
														<option value="">Select Product Family</option>
														<?php $ProductFamily = $this->ProductsModel->GetProductFamily();

															foreach ($ProductFamily as $Family) { 
																if($Family->ProductFamilyID == $ProductData->ProductFamily){ ?>
																	<option value="<?=$Family->ProductFamilyID?>" selected=""><?=$Family->ProductFamily?></option>
																<?php } else { ?>
																	<option value="<?=$Family->ProductFamilyID?>"><?=$Family->ProductFamily?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('ProductFamily');  ?>
														</div>
													</div>

													<!-- <div class="form-group">
														<label class="control-label col-md-3">Account<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="Account" id="Account" class="form-control select2">
														<option value="">Select Account</option>
														<?php /*$Accounts = $this->ProductsModel->GetAccounts();
															foreach ($Accounts as $Account) { 
																if($Account->AccountID == $ProductData->Account){ ?>
																	<option value="<?=$Account->AccountID?>" selected=""><?=$Account->AccountName?></option>
																<?php } else { ?>
																	<option value="<?=$Account->AccountID?>"><?=$Account->AccountName?></option>
																<?php } ?>
															<?php }*/ ?>
														</select>
														<?php //echo form_error('ProductFamily');  ?>
														</div>
													</div> -->

													<div class="form-group">
														<label class="control-label col-md-3">Stock Keeping Unit</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="StockKeepingUnit" id="StockKeepingUnit" value="<?=isset($ProductData->StockKeepingUnit)?$ProductData->StockKeepingUnit:"";?>">
															<?php echo form_error('StockKeepingUnit');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Display Url</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="DisplayUrl" id="DisplayUrl" value="<?=isset($ProductData->DisplayUrl)?$ProductData->DisplayUrl:"";?>">
															<?php echo form_error('DisplayUrl');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Description</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Description" id="Description" value="<?=isset($ProductData->Description)?$ProductData->Description:"";?>">
															<?php echo form_error('Description');  ?>
														</div>
													</div>
												</div>

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Is Active</label>
														<div class="col-md-9">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="IsActive" id="IsActive" value="1" <?=($ProductData->IsActive==1)?"checked=''":"";?>>
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Default Quantity<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="DefaultQuantity" id="DefaultQuantity" value="<?=isset($ProductData->DefaultQuantity)?$ProductData->DefaultQuantity:"1";?>">
															<?php echo form_error('DefaultQuantity');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Quantity Editable</label>
														<div class="col-md-9">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="IsQuantityEditable" id="IsQuantityEditable" value="1" <?=($ProductData->IsQuantityEditable==1)?"checked=''":"";?>>
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Is Discountable</label>
														<div class="col-md-9">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="IsDiscountable" id="IsDiscountable" value="1" <?=($ProductData->IsDiscountable==1)?"checked=''":"";?>>
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

													<!-- <div class="form-group">
														<label class="control-label col-md-3">Track Installed Product</label>
														<div class="col-md-9">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="TrackInstalledProduct" id="TrackInstalledProduct" value="1">
																<label for="checkbox2"></label>
															</div>
														</div>
													</div> -->

													<div class="form-group">
														<label class="control-label col-md-3">Is Chemical</label>
														<div class="col-md-9">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="IsChemical" id="IsChemical" value="1" <?=($ProductData->IsChemical==1)?"checked=''":"";?> >
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Is Renewable</label>
														<div class="col-md-9">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="IsRenewable" id="IsRenewable" value="1" <?=($ProductData->IsRenewable==1)?"checked=''":"";?>>
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Replacement Available</label>
														<div class="col-md-9">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="ReplacementAvailable" id="ReplacementAvailable" value="1" <?=($ProductData->ReplacementAvailable==1)?"checked=''":"";?>>
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>
											
												</div>
												<!--/span-->
												</div>

												<div class="col-md-12">
												<div class="txt-dark capitalize-font"><strong>Financials</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-3">List Price<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<div class="input-group"> <span class="input-group-addon">$</span>
																<input type="text" class="form-control" name="ListPrice" id="ListPrice"  value="<?=isset($ProductData->ListPrice)?$ProductData->ListPrice:"";?>">
															</div>
															<?php echo form_error('ListPrice');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Product Cost</label>
														<div class="col-md-9">
															<div class="input-group"> <span class="input-group-addon">$</span>
																<input type="text" class="form-control" name="ProductCost" id="ProductCost"  value="<?=isset($ProductData->ProductCost)?$ProductData->ProductCost:"";?>">
															</div>
															<?php echo form_error('ProductCost');  ?>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-3">Quantity Unit Of Measure<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="QuantityUnitOfMeasure" id="QuantityUnitOfMeasure" class="form-control select2">
														<option value="">Select Quantity Unit Of Measure</option>
														<?php $UnitOfMeasurements = $this->ProductsModel->GetUnitOfMeasurements();

															foreach ($UnitOfMeasurements as $UnitOfMeasurement) { 
																if($UnitOfMeasurement->ProductQtyUnitOfMeasureID == $ProductData->QuantityUnitOfMeasure){ ?>
																	<option value="<?=$UnitOfMeasurement->ProductQtyUnitOfMeasureID?>" selected=""><?=$UnitOfMeasurement->UnitOfMeasure?></option>
																<?php } else { ?>
																<option value="<?=$UnitOfMeasurement->ProductQtyUnitOfMeasureID?>"><?=$UnitOfMeasurement->UnitOfMeasure?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('QuantityUnitOfMeasure');  ?>
														</div>
													</div>

												</div>

											</div>
											<!-- /Row -->
										</div>

										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="EditProduct">Save</button>
															<a href="<?=SITE_URL;?>Setup/Customize/Products/ProductList" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>

<script type="text/javascript">
$(document).ready(function () {
 
    $('#EditProduct').validate({ 
        rules: {
            ProductName: {
                required: true
            },
            Account: {
                required: true
            },
            ProductFamily: {
                required: true
            },
            QuantityUnitOfMeasure: {
                required: true
            },
            ListPrice: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    }); 
});
</script>



