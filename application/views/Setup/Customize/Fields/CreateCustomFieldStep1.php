<?php $this->load->view('Template/HeaderView')?>
<div class="container-fluid pt-25">

<!-- Row -->
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">Create New Custom Field</h6>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default card-view">
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
					<div class="row">
					<div class="col-md-12">
					<div class="form-wrap">
					<?php if(isset($CustomFieldData->CustomFieldID)){?>
							<form action="<?=SITE_URL;?>Setup/Customize/Fields/CreateCustomField/<?=$Object?>/<?=$CustomFieldData->CustomFieldID?>" class="form-horizontal" id="CreateCustomFieldForm1" method="post">
						<?php } else { ?>
							<form action="<?=SITE_URL;?>Setup/Customize/Fields/CreateCustomField/<?=$Object?>" class="form-horizontal" id="CreateCustomFieldForm1" method="post">	
						<?php } ?>
							<div class="form-body">
							
							<div class="txt-dark capitalize-font"><strong>Step 1 : Choose the Field Type</strong></div>
							<hr class="light-grey-hr">
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
											<div class="radio radio-primary">
											  <input type="radio" name="FieldType" id="Checkbox" value="Checkbox" required="" <?=(isset($CustomFieldData->FieldType) && $CustomFieldData->FieldType == 'Checkbox')?"checked=''":"";?>>
											  <label for="Checkbox"> Checkbox </label>
											</div>
											<div class="radio radio-primary">
											  <input type="radio" name="FieldType" id="Currency" value="Currency" required="" <?=(isset($CustomFieldData->FieldType) && $CustomFieldData->FieldType == 'Currency')?"checked=''":"";?>>
											  <label for="Currency"> Currency </label>
											</div>
											<div class="radio radio-primary">
											  <input type="radio" name="FieldType" id="Number" required="" value="Number" <?=(isset($CustomFieldData->FieldType) && $CustomFieldData->FieldType == 'Number')?"checked=''":"";?>>
											  <label for="Number"> Number </label>
											</div>
											<div class="radio radio-primary">
											  <input type="radio" name="FieldType" id="Date" required="" value="Date" <?=(isset($CustomFieldData->FieldType) && $CustomFieldData->FieldType == 'Date')?"checked=''":"";?>>
											  <label for="Date"> Date </label>
											</div>
											<div class="radio radio-primary">
											  <input type="radio" name="FieldType" id="DateTime" required="" value="DateTime" <?=(isset($CustomFieldData->FieldType) && $CustomFieldData->FieldType == 'DateTime')?"checked=''":"";?>>
											  <label for="DateTime"> Date Time </label>
											</div>
											<div class="radio radio-primary">
											  <input type="radio" name="FieldType" id="LongText" required="" value="LongText" <?=(isset($CustomFieldData->FieldType) && $CustomFieldData->FieldType == 'LongText')?"checked=''":"";?>>
											  <label for="LongText"> Long Text </label>
											</div>
											<div class="radio radio-primary">
											  <input type="radio" name="FieldType" id="Text" required="" value="Text" <?=(isset($CustomFieldData->FieldType) && $CustomFieldData->FieldType == 'Text')?"checked=''":"";?>>
											  <label for="Text"> Text </label>
											</div>
											<div class="radio radio-primary">
											  <input type="radio" name="FieldType" id="URL" required="" value="URL" <?=(isset($CustomFieldData->FieldType) && $CustomFieldData->FieldType == 'URL')?"checked=''":"";?>>
											  <label for="URL"> URL </label>
											</div>
										</div>
										</div>
									</div>
								</div>
								</div>
								<div class="form-actions mt-10">
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-offset-5 col-md-6">
													<button type="submit" class="btn btn-primary mr-10" name="CreateCustomFieldStep1">Next</button>
													<a href="<?=SITE_URL;?>Setup/Customize/Fields/<?=$Object?>" class="btn btn-default">Cancel</a>
												</div>
											</div>
										</div>
									</div>
								</div>
						</form>
						</div>
						</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->
								
	</div>	

<!-- Start Modal -->

<?php $this->load->view('Template/FooterView')?>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
 
    $('#CreateCustomFieldForm1').validate({ 
        /*rules: {
            AssignedTo: {
                required: true
            },
        },*/
        errorPlacement: function(){
            return false;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
});
</script>