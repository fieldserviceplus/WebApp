<?php
if(isset($Object) && isset($ObjectID))
{
	$LblColClass = ($ObjectID==0)?"col-md-3":"col-md-4";
	$InpColClass = ($ObjectID==0)?"col-md-9":"col-md-8";

	$CustomFieldData = MY_Model::getCustomFieldsByObject($Object, $ObjectID); 
	$Cnt = count($CustomFieldData['Fields']);
	
	if($Cnt > 0)
	{
	?>

		<div class="col-md-12">
		<div class="txt-dark capitalize-font"><strong>Extra Fields</strong></div>
		<hr class="light-grey-hr">

			<div class="col-md-6">

			<?php foreach ($CustomFieldData['Fields'] as $ky => $CustomField) { 

				$RequiredVld = ($CustomField->IsRequired==1)?"required=''":"";
				$RequiredSign = ($CustomField->IsRequired==1)?'<span class="required-sign">*</span>':'';
				$FieldValues = isset($CustomFieldData['FieldValues'][$CustomField->CustomFieldID])?$CustomFieldData['FieldValues'][$CustomField->CustomFieldID]->FieldValue:"";
				$FieldLength = ($CustomField->FieldLength!=NULL)?"maxlength='".$CustomField->FieldLength."'":"";
				?>

				<input type="hidden" name="CustomFieldIDs[]" value="<?=$CustomField->CustomFieldID?>">
			
			<?php if(isset($CustomFieldData['FieldValues'][$CustomField->CustomFieldID])){ ?>
				<input type="hidden" name="CustomFieldValueIDs[<?=$CustomField->CustomFieldID?>]" value="<?=$CustomFieldData['FieldValues'][$CustomField->CustomFieldID]->CustomFieldValueID?>">
			<?php } ?>

			<?php if($CustomField->FieldType == 'Checkbox'){

				$OptionValues = explode(",", trim($CustomField->OptionValues));
				$OptionLabels = explode(",", trim($CustomField->OptionLabels));
				$FieldValue = explode(",", trim($FieldValues));?>

					<div class="form-group">
						<label class="control-label <?=$LblColClass?>"><?=$CustomField->FieldLabel?><?=$RequiredSign?></label>
						<div class="<?=$InpColClass?>">
						<?php foreach ($OptionValues as $key => $Option) { ?>
							<div class="checkbox checkbox-primary col-md-4">
								<input id="<?=$Option?>" type="checkbox" name="<?=$CustomField->FieldName?>[]" value="<?=$Option?>" <?=(in_array($Option, $FieldValue)?"checked=''":"");?> <?=$RequiredVld?> >
								<label for="<?=$Option?>"> <?=$OptionLabels[$key]?> </label>
							</div>
						<?php } ?>
						</div>
					</div> 

			<?php } else if($CustomField->FieldType == 'LongText'){ ?>

					<div class="form-group">
						<label class="control-label <?=$LblColClass?>"><?=$CustomField->FieldLabel?><?=$RequiredSign?></label>
						<div class="<?=$InpColClass?>">
							<textarea name="<?=$CustomField->FieldName?>" id="<?=$CustomField->FieldName?>" class="form-control" <?=$RequiredVld?> <?=$FieldLength?> > <?=$FieldValues?></textarea>
						</div>
					</div> 

			<?php } else { ?>

					<div class="form-group">
						<label class="control-label <?=$LblColClass?>"><?=$CustomField->FieldLabel?><?=$RequiredSign?></label>
						<div class="<?=$InpColClass?>">
							<input type="text" class="form-control <?=($CustomField->FieldType == 'Date')?"showdatepicker":"";?> <?=($CustomField->FieldType == 'DateTime')?"showdatetimepicker":"";?> <?=($CustomField->FieldType == 'Currency' || $CustomField->FieldType == 'Number')?"allow-number-dot":"";?>" name="<?=$CustomField->FieldName?>" id="<?=$CustomField->FieldName?>" value="<?=$FieldValues?>" <?=$RequiredVld?> <?=$FieldLength?> >
						</div>
					</div> 
			<?php } 
				if(ceil((($Cnt) / 2)) == ($ky+1)){
					echo '</div><div class="col-md-6">';
				}
			 } ?>
			 </div>
		</div>
	<?php } ?>
<?php } ?>