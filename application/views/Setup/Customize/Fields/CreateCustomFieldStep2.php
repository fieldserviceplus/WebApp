<?php $this->load->view('Template/HeaderView');?>

<div class="container-fluid pt-25">

<!-- Row -->
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">Create New Custom Field</h6>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>


<!-- Row -->
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
				<div class="row">
				<div class="col-md-12">
				<div class="form-wrap">
					<?php if(isset($CustomFieldData->CustomFieldID)){?>
						<form action="<?=SITE_URL;?>Setup/Customize/Fields/CreateCustomField/<?=$Object?>/<?=$CustomFieldData->CustomFieldID?>" class="form-horizontal" id="CreateCustomFieldForm2" method="post">
					<?php } else { ?>
						<form action="<?=SITE_URL;?>Setup/Customize/Fields/CreateCustomField/<?=$Object?>" class="form-horizontal" id="CreateCustomFieldForm2" method="post">	
					<?php } ?>

						<input type="hidden" name="FieldType" value="<?=$FieldType?>">

						<div class="form-body">
						
						<div class="txt-dark capitalize-font"><strong>Step 2: Enter the Field Details</strong></div>
						<hr class="light-grey-hr">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-6"> 
										<div class="form-group">
											<label class="control-label col-md-3">Field Name<span class="required-sign">*</span><p class="text-danger font-10 text-capitalize uppercase-font">Allow (numbers and letters)</p></label>
											<div class="col-md-9">
												<input type="text" class="form-control allow-num-letter" name="FieldName" id="FieldName" value="<?=isset($CustomFieldData->FieldName)?$CustomFieldData->FieldName:"";?>" required="">
												<?php echo form_error('FieldName');  ?>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Field Label<span class="required-sign">*</span></label>
											<div class="col-md-9">
												<input type="text" class="form-control" name="FieldLabel" id="FieldLabel" value="<?=isset($CustomFieldData->FieldLabel)?$CustomFieldData->FieldLabel:"";?>" required="">
												<?php echo form_error('FieldLabel');  ?>
											</div>
										</div>
										<?php if($FieldType == 'Checkbox'){ ?>
											<div class="form-group">
												<label class="control-label col-md-3">Option Labels<span class="required-sign">*</span><p class="text-danger font-10 text-capitalize uppercase-font">Use commas(,) to separate labels</p></label>
												<div class="col-md-9">
													<input type="text" class="form-control" name="OptionLabels" id="OptionLabels" value="<?=isset($CustomFieldData->OptionLabels)?$CustomFieldData->OptionLabels:"";?>" required="">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Option Values<span class="required-sign">*</span><p class="text-danger font-10 text-capitalize uppercase-font">Use commas(,) to separate values</p></label>
												<div class="col-md-9">
													<input type="text" class="form-control allow-num-letter-comma" name="OptionValues" id="OptionValues" value="<?=isset($CustomFieldData->OptionValues)?$CustomFieldData->OptionValues:"";?>" required="">
												</div>
											</div>
										<?php } else { ?>
											<div class="form-group">
												<label class="control-label col-md-3">Length</label>
												<div class="col-md-9">
													<input type="text" class="form-control" name="FieldLength" id="FieldLength" value="<?=isset($CustomFieldData->FieldLength)?$CustomFieldData->FieldLength:"";?>">
												</div>
											</div>
										<?php } ?> 
										<div class="form-group">
											<label class="control-label col-md-3">Required</label>
											<div class="col-md-9">
												<div class="checkbox checkbox-primary">
													<input type="checkbox" name="IsRequired" id="IsRequired" value="1" <?=(isset($CustomFieldData->FieldType) && $CustomFieldData->IsRequired == 1)?"checked=''":"";?>>
													<label for="checkbox2"></label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Default Value</label>
											<div class="col-md-9">
												<input type="text" class="form-control" name="DefaultValue" id="DefaultValue" value="<?=isset($CustomFieldData->DefaultValue)?$CustomFieldData->DefaultValue:"";?>">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3">Add To Page Layout</label>
											<div class="col-md-9">
												<div class="checkbox checkbox-primary">
													<input type="checkbox" name="AddToPageLayout" id="AddToPageLayout" value="1" <?=(isset($CustomFieldData->FieldType) && $CustomFieldData->AddToPageLayout == 1)?"checked=''":"";?>>
													<label for="checkbox2"></label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							</div>
							<div class="form-actions mt-10">
								<div class="row">
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-offset-5 col-md-6">
												<a href="#" onclick="window.history.go(-1);" class="btn btn-default">Previous</a>
												<button type="submit" class="btn btn-primary" name="CreateCustomFieldStep2">Save</button>
												<a href="<?=SITE_URL;?>Setup/Customize/Fields/<?=$Object?>" class="btn btn-default">Cancel</a>
											</div>
										</div>
									</div>
								</div>
							</div>
					</form>
					</div>
					</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /Row -->						
</div>	

<!-- Start Modal -->
<?php $this->load->view('Template/FooterView')?>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<script type="text/javascript">
	$(document).ready(function () {

	 $.validator.addMethod("FieldNameRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");
 
    $('#CreateCustomFieldForm2').validate({ 
        rules: {
            FieldName: {
                required: true,
                FieldNameRegex: true,
            },
        },
        errorPlacement: function(){
            return false;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
});

$('.allow-num-letter').bind('keypress', function (e) {  
    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    specialKeys.push(9); //Tab
    specialKeys.push(46); //Delete
    specialKeys.push(36); //Home
    specialKeys.push(35); //End
    specialKeys.push(37); //Left
    specialKeys.push(39); //Right
    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
    var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode)) && (e.which != 118);
   
    return ret;
});

$('.allow-num-letter-comma').bind('keypress', function (e) {  
    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    specialKeys.push(9); //Tab
    specialKeys.push(46); //Delete
    specialKeys.push(36); //Home
    specialKeys.push(35); //End
    specialKeys.push(37); //Left
    specialKeys.push(39); //Right
    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
    var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || e.which == 44) && (e.which != 118);
   
    return ret;
});
</script>

