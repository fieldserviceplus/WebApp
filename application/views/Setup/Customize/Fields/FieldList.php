<?php $this->load->view('Template/HeaderView'); ?>


<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark"><?=$Object?> Fields</h6>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">

							<div class="txt-dark capitalize-font"><strong>Standard <?=$Object?> Fields</strong></div>
							<hr class="light-grey-hr">

								<div class="table-responsive">
									<table id="acTable1" class="table table-hover display pb-30" width="100%">
										<thead>
											<tr> 
												<th class="text-center">Field Label</th>
												<th class="text-center">Field Name</th>
												<th class="text-center">Type</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($StandardFields as $key => $StandardField) { ?>
													<tr>
														<td class="text-left"><?=preg_replace('/(?<!\ )[A-Z]/', ' $0', $StandardField->Field);?></td>
														<td class="text-left"><?=$StandardField->Field;?></td>
														<td class="text-left text-capitalize"><?=isset($ReferenceFields[$StandardField->Field])?'Reference ('.$ReferenceFields[$StandardField->Field].')':$StandardField->Type;?></td>
													</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>

								<div class="txt-dark capitalize-font pt-25"><strong>Custom <?=$Object?> Fields</strong></div>
								<hr class="light-grey-hr mb-20">

								<div class="row">
									<div class="col-sm-12 text-center pb-10">
										<a href="<?=SITE_URL;?>Setup/Customize/Fields/CreateCustomField/<?=$Object?>" class="btn btn-primary btn-outline">New Custom Field</a>
									</div>
								</div>
								<div class="table-responsive">
									<table id="acTable2" class="table table-hover display pb-30" width="100%">
										<thead>
											<tr> 
												<th class="text-center">Actions</th>
												<th class="text-center">Field Label</th>
												<th class="text-center">Field Name</th>
												<th class="text-center">Type</th>
												<th class="text-center">Created By</th>
												<th class="text-center">Created Date</th>
												<th class="text-center">Last Modified By</th>
												<th class="text-center">Last Modified Date</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($CustomFields as $key => $CustomField) { ?>
												<tr>
													<td class="text-center"><a href="<?=SITE_URL;?>Setup/Customize/Fields/CreateCustomField/<?=$Object?>/<?=$CustomField->CustomFieldID;?>"><i class="fa fa-pencil-square-o"></i></a></td>
													<td class="text-left"><?=$CustomField->FieldLabel;?></td>
													<td class="text-left"><?=$CustomField->FieldName;?></td>
													<td class="text-center"><?=$CustomField->FieldType;?></td>
													<td class="text-center"><?=$CustomField->CreatedBy;?></td>
													<td class="text-center"><?=$CustomField->CreatedDate;?></td>
													<td class="text-center"><?=$CustomField->LastModifiedBy;?></td>
													<td class="text-center"><?=$CustomField->LastModifiedDate;?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>

							</div>
						</div>
					</div>
				</div>	
		</div>
		<!-- /Row -->
	</div>	

<!-- Start Modal -->

<?php $this->load->view('Template/FooterView')?>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript" src="<?=SITE_URL?>dist/js/multiselect.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	$('#acTable1').DataTable( {
		responsive: true,
		"columnDefs": [ {
	          "targets": 'no-sort',
	          "orderable": false,
	    } ],
	    "aaSorting": [],
	    "order": [[ 0, "asc" ]],
	    "dom": 'rt<"bottom"ilp><"clear">',
	} );

    $('input.myTable1globalfilter').on( 'keyup click', function () {
        $('#myTable1').DataTable().search(
	        $('#myTable1globalfilter').val()
	    ).draw();
    });

    $('#acTable2').DataTable( {
		responsive: true,
		"columnDefs": [ {
	          "targets": 'no-sort',
	          "orderable": false,
	    } ],
	    "aaSorting": [],
	    "order": [[ 0, "asc" ]],
	    "dom": 'rt<"bottom"ilp><"clear">',
	} );

    $('input.myTable1globalfilter').on( 'keyup click', function () {
        $('#myTable1').DataTable().search(
	        $('#myTable1globalfilter').val()
	    ).draw();
    });
});
</script> 