<?php 
if(isset($Object) && isset($ObjectID))
{ 
	$CustomFieldData = MY_Model::getCustomFieldsByObject($Object, $ObjectID); 
	$Cnt = count($CustomFieldData['Fields']); 
	
	if($Cnt > 0)
	{
	 ?>

	<div class="col-md-12">
	<div class="txt-dark capitalize-font"><strong>Extra Fields</strong></div>
	<hr class="light-grey-hr">

	<div class="col-md-6">

	<?php foreach ($CustomFieldData['Fields'] as $ky => $CustomField) { 

		$FieldValues = isset($CustomFieldData['FieldValues'][$CustomField->CustomFieldID])?$CustomFieldData['FieldValues'][$CustomField->CustomFieldID]->FieldValue:"";

		if($CustomField->FieldType == 'Checkbox'){

			$OptionValues = explode(",", trim($CustomField->OptionValues));
			$OptionLabels = explode(",", trim($CustomField->OptionLabels));
			$FieldValue = explode(",", trim($FieldValues));?>

				<div class="form-group pt-5 pb-5">
					<div class="col-md-4 text-left txt-dark">
						<?=$CustomField->FieldLabel?>
					</div>
					<div class="col-md-8">
						<?php foreach ($OptionValues as $key => $Option) { ?>
						<div class="checkbox checkbox-primary col-md-5">
							<input type="checkbox" value="<?=$Option?>" <?=(in_array($Option, $FieldValue)?"checked=''":"");?> disabled="">
							<label for="<?=$Option?>"> <?=$OptionLabels[$key]?> </label>
						</div>
					<?php } ?>
					</div>
				</div>

			<?php } else if($CustomField->FieldType == 'LongText'){ ?>

				<div class="form-group pt-5 pb-5">
					<div class="col-md-4 text-left txt-dark">
						<?=$CustomField->FieldLabel?>
					</div>
					<div class="col-md-8">
						<?=$FieldValues;?>
					</div>
				</div>

			<?php } else if($CustomField->FieldType == 'URL'){ ?>

				<div class="form-group pt-5 pb-5">
					<div class="col-md-4 text-left txt-dark">
						<?=$CustomField->FieldLabel?>
					</div>
					<div class="col-md-8">
						<a href="<?=$FieldValues;?>"><?=$FieldValues;?></a>
					</div>
				</div>

			<?php } else { ?>

				<div class="form-group pt-5 pb-5">
					<div class="col-md-4 text-left txt-dark">
						<?=$CustomField->FieldLabel?>
					</div>
					<div class="col-md-8">
						<?=$FieldValues;?>
					</div>
				</div>

		<?php } 
		
			if(ceil((($Cnt) / 2)) == ($ky+1)){
				echo '</div><div class="col-md-6">';
			}
		 } ?>	
		 </div>
	</div> 

	<?php } ?>	

<?php } ?>