<?php $this->load->view('Template/HeaderView')?>

<div class="container-fluid pt-25">

	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Chemicals Settings</h6>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Setup/Customize/Settings/ChemicalSettings" class="form-horizontal" id="ChemicalSettings" method="post">
										<div class="form-body">

											<!-- /Row -->
											<div class="row">
												<div class="col-md-12">

													<div class="col-md-6">
														<div class="form-group">
															<!-- <label class="control-label col-md-3">Enable Chemicals</label> -->
															<div class="col-md-9">
																<div class="checkbox checkbox-primary">
																	<input type="checkbox" name="EnableChemicals" id="EnableChemicals" value="1" <?=(isset($ChemicalSettings->EnableChemicals) && $ChemicalSettings->EnableChemicals == 1)? "checked=''" : "";?>>
																	<label for="EnableChemicals">Enable Chemicals</label>
																</div>
															</div>
														</div>
													</div>
												</div>
											<!-- /Row -->
										</div>

										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="ChemicalSettings">Save</button>
															<a href="javascript:window.history.back();" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->

<?php $this->load->view('Template/FooterView')?>


