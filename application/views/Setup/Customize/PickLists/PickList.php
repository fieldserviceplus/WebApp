<?php $this->load->view('Template/HeaderView')?>


<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark"><?=$PickListName?> Pick List</h6>
					</div>
					<div class="pull-right">
						<a href="<?=SITE_URL;?>Setup/Customize/PickLists/CreatePickList/<?=$PickList?>" class="btn btn-primary btn-outline pull-right">New <?=$PickListName?></a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
								<div class="table-responsive">
									<table id="pkTable1" class="table table-hover display pb-30" width="100%">
										<thead>
											<tr> 
												<th class="text-center no-sort">Actions</th>
												<th class="text-center">Name</th>
												<?=$ShowDefaultCol ? '<th class="text-center">Is Default</th>' : '';?>
												<?=$ShowColorCodeCol ? '<th class="text-center">Color Code</th>' : '';?>
												<th class="text-center">Created By</th>
												<th class="text-center">Created Date</th>
												<th class="text-center">Last Modified By</th>
												<th class="text-center">Last Modified Date</th>
												<th class="text-center no-sort">Delete</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($PickLists as $key => $PickListValue) { ?>
												<tr>
													<td class="text-center"><a href="<?=SITE_URL;?>Setup/Customize/PickLists/EditPickList/<?=$PickList?>/<?=$PickListValue->ID;?>"><i class="fa fa-pencil-square-o"></i></a></td>
													<td class="text-left"><?=$PickListValue->Name;?></td>
													<?=$ShowDefaultCol ? '<td class="text-center">'.$PickListValue->IsDefault.'</td>' : '';?>
													<?=$ShowColorCodeCol ? '<td class="text-center">'.$PickListValue->ColorCode.'</td>' : '';?>
													<td class="text-center"><a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$PickListValue->CreatedBy?>"><?=$PickListValue->CreatedByName;?></a></td>
													<td class="text-center"><?=$PickListValue->CreatedDate;?></td>
													<td class="text-center"><a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$PickListValue->CreatedBy?>"><?=$PickListValue->LastModifiedByName;?></a></td>
													<td class="text-center"><?=$PickListValue->LastModifiedDate;?></td>
													<td class="text-center"><a href="<?=SITE_URL;?>Setup/Customize/PickLists/DeletePickList/<?=$PickList?>/<?=$PickListValue->ID;?>" onclick="javascript:return confirm('Are you sure you want to delete?')"><i class="fa fa-trash-o"></i></a></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>

							</div>
						</div>
					</div>
				</div>	
		</div>
		<!-- /Row -->
	</div>	

<!-- Start Modal -->


<?php $this->load->view('Template/FooterView')?>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript" src="<?=SITE_URL?>dist/js/multiselect.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	$('#pkTable1').DataTable( {
		responsive: true,
		"columnDefs": [ {
	          "targets": 'no-sort',
	          "orderable": false,
	    } ],
	    "aaSorting": [],
	    "order": [],
	    "dom": 'rt<"bottom"ilp><"clear">',
	} );

    $('input.pkTable1globalfilter').on( 'keyup click', function () {
        $('#pkTable1').DataTable().search(
	        $('#pkTable1globalfilter').val()
	    ).draw();
    });
});
</script>