<?php $this->load->view('Template/HeaderView');?>
<div class="container-fluid pt-25">

<!-- Row -->
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">Create <?=$PickListName?></h6>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<!-- Row -->
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
				<div class="row">
				<div class="col-md-12">
				<div class="form-wrap">
				
					<form action="<?=SITE_URL;?>Setup/Customize/PickLists/CreatePickList/<?=$PickList?>" class="form-horizontal" id="CreatePickList" method="post">

						<input type="hidden" name="PickList" value="<?=$PickList?>">

						<div class="form-body">
						
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-6"> 
										<div class="form-group">
											<label class="control-label col-md-3">Name<span class="required-sign">*</span></label>
											<div class="col-md-7">
												<input type="text" class="form-control" name="Name" id="Name" value="" required="">
												<?php echo form_error('Name');  ?>
											</div>
										</div>
									</div>
								</div>	

								<?php if(isset($ShowColorCodeCol) && $ShowColorCodeCol == 1){ ?>
								<div class="col-md-12">
									<div class="col-md-6"> 
										<div class="form-group">
											<label class="control-label col-md-3">Color Code<span class="required-sign">*</span></label>
											<div class="col-md-7">
												<input type="text" class="form-control" name="ColorCode" id="ColorCode" required="">
											</div>
										</div>
									</div>
								</div>
								<?php } ?>

								<?php if(isset($ShowDefaultCol) && $ShowDefaultCol == 1){ ?>
								<div class="col-md-12">
									<div class="col-md-6"> 
										<div class="form-group">
											<label class="control-label col-md-3">Is Default</label>
											<div class="col-md-7">
												<div class="checkbox checkbox-primary">
													<input type="checkbox" name="IsDefault" id="IsDefault" value="1">
													<label for="checkbox2"></label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php } ?>
								
								
							</div>
							</div>
							<div class="form-actions mt-10">
								<div class="row">
									<div class="col-md-12">
										<div class="row"> 
											<div class="col-md-offset-5 col-md-6">
												<button type="submit" class="btn btn-primary" name="CreatePickList">Save</button>
												<a href="<?=SITE_URL;?>Setup/Customize/PickLists/<?=$PickList?>" class="btn btn-default">Cancel</a>
											</div>
										</div>
									</div>
								</div>
							</div>
					</form>
					</div>
					</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /Row -->						
</div>	

<!-- Start Modal -->
<?php $this->load->view('Template/FooterView')?>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<script type="text/javascript">
$(document).ready(function () {

    $('#CreatePickList').validate({ 
        rules: {
            /*FieldName: {
                required: true,
            },*/
        },
        errorPlacement: function(){
            return false;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
});
</script>

