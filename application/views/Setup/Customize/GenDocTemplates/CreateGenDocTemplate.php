<?php $this->load->view('Template/HeaderView'); ?>

<!-- Jasny-bootstrap CSS -->
<link href="<?=SITE_URL;?>vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>

<div class="container-fluid pt-25">
	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Create Template</h6>
					</div>
					<div class="pull-right">
						<span style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong></span>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Setup/Customize/GenDocTemplates/CreateTemplate/<?=$Object?>" class="form-horizontal" id="CreateTemplateForm" method="post" enctype="multipart/form-data">
										<div class="form-body">

											<!-- /Row -->
											<div class="row">
												<div class="col-md-12">

													<div class="form-group">
														<label class="control-label col-md-2">Template Name<span class="required-sign">*</span></label>
														<div class="col-md-4">
															<input type="text" name="TemplateName" class="form-control">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-2">Description</label>
														<div class="col-md-4">
															<textarea name="Description" class="form-control"></textarea>
														</div>
													</div>
													
													<div class="form-group">
														<label class="control-label col-md-2">Is Active</label>
														<div class="col-md-4">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="IsActive" id="IsActive" value="1" checked="">
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-2">File upload<span class="required-sign">*</span></label>
														<div class="col-md-4">
															<div class="fileinput fileinput-new input-group" data-provides="fileinput">
																<div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
																<span class="input-group-addon fileupload btn btn-primary btn-anim btn-file"><i class="fa fa-upload"></i> <span class="fileinput-new btn-text">Select file</span> <span class="fileinput-exists btn-text">Change</span>
																	<input type="file" name="TemplateFile" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document">
																</span>
															</div>
														</div>
													</div>

												</div>
											</div>
											<!-- /Row -->
										</div>
										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary mr-10" name="CreateTemplate" id="CreateTemplate">Submit</button>
															<button type="button" class="btn btn-default" onclick="window.history.go(-1); return false;">Cancel</button>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->
</div>

<?php $this->load->view('Template/FooterView')?>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>
<script src="<?=SITE_URL?>dist/js/additional-methods.min.js"></script>

<script src="<?=SITE_URL?>vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>

<script type="text/javascript">
$(document).ready(function () {

    $('#CreateTemplateForm').validate({ 
        rules: { 
            TemplateName: {
        		required: true
            },
            TemplateFile: {
                required: true,
                accept: "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
            }
        },
        messages: {
	        TemplateFile: {
	            accept: "Only allow docx file."
	        }
	    },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            // if element is file type, we put the error message in its grand parent
            if (element.prop("type") === "file") {
                error.insertAfter(element.parent().parent());
            } 
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
});
</script>




