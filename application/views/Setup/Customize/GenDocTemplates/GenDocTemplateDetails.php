<?php $this->load->view('Template/HeaderView'); ?>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<div class="container-fluid pt-25">
	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">

			<div class="panel-heading pt-10 pb-10">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Template</h6>
					</div>
					<div class="btn-group pull-right">
						<div class="dropdown">
							<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
							<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
								<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
									<a><span><strong>Template Actions</strong></span></a>
								</li>
								<li> 
									<a href="<?=SITE_URL;?>Setup/Customize/GenDocTemplates/DownloadTemplate/<?=$TemplateData->TemplateFile?>/"><span>Download</span></a>
								</li>
								<li>
									<a href="#" onclick="DeleteTemplate();"><span>Delete</span></a>
								</li>
							</ul>
						</div>
					</div>
					<div class="btn-group pull-right">
						<a href="<?=SITE_URL;?>Setup/Customize/GenDocTemplates/EditTemplate/<?=$TemplateData->GenDocTemplateID?>" class="btn btn-primary btn-outline">Edit</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
			 	<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Product Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>  -->
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Setup/Customize/GenDocTemplates/EditGenDocTemplate/<?=$TemplateData->GenDocTemplateID?>" class="form-horizontal" id="EditGenDocTemplate" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>Template Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Template Name
														</div>
														<div class="col-md-8">
															<?=$TemplateData->TemplateName;?>
														</div>
													</div>
												
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Description
														</div>
														<div class="col-md-8">
															<?=$TemplateData->Description;?>
														</div>
													</div>

													<div class="form-group pt-5">
														<div class="col-md-4 text-left txt-dark">
															Is Active
														</div>
														<div class="col-md-8">
															<div class="checkbox checkbox-primary pt-0 pb-0">
																<input id="checkbox12" disabled="" <?=$TemplateData->IsActive==1?'checked':'';?> type="checkbox">
																<label for="checkbox12"></label>
															</div>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															File
														</div>
														<div class="col-md-8">
															<?=$TemplateData->TemplateFile;?>
														</div>
													</div>
													
												</div>

												<div class="col-md-6">

												</div>
											</div>

											<div class="col-md-12">

												<div class="txt-dark capitalize-font pt-10"><strong>System Information</strong></div>
												<hr class="light-grey-hr">
												
												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Create Date
														</div>
														<div class="col-md-8">
															<?=$TemplateData->CreatedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Created By
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/<?=$TemplateData->CreatedBy?>"><?=$TemplateData->CreatedByName;?></a>
														</div>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified Date
														</div>
														<div class="col-md-8">
															<?=$TemplateData->LastModifiedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified By
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/<?=$TemplateData->LastModifiedBy?>"><?=$TemplateData->LastModifiedByName;?></a>
														</div>
													</div>
												</div>
											</div>


										</div>
											<!-- /Row -->
									</div>

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
			
		</div>
		</div>
	</div>
	<!-- /Row -->
</div>

<!-- End Modal -->

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
function DeleteTemplate()
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#5fc55f",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>Setup/Customize/GenDocTemplates/DeleteTemplate/<?=$TemplateData->GenDocTemplateID;?>/";
		}, 500);
    });
	return false;
}

</script>


