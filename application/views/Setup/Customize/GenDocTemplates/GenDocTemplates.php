<?php $this->load->view('Template/HeaderView')?>

<style type="text/css">.dataTables_filter { display: none; }</style>

    <div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Template</h6>
					</div>
					<div class="pull-right">
						<a href="<?=SITE_URL;?>Setup/Customize/GenDocTemplates/CreateTemplate/<?=$Object?>" class="btn btn-primary btn-outline">Add Template</a>
					</div> 
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
								<div class="table-responsive">
									<table id="myTable2" class="table table-hover display pb-30" width="100%">
										<thead>
											<tr> 
												<th class="text-center">Template Name</th>
												<th class="text-center">Active</th>
												<th class="text-center">Description</th>
												<th class="text-center">Last Modified By</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($Templates as $key => $Template) { ?>
													<tr> 
														<td class="text-left"><a href="<?=SITE_URL;?>Setup/Customize/GenDocTemplates/TemplateDetails/<?=$Template->GenDocTemplateID?>"><?=$Template->TemplateName;?></a></td>
														<td class="text-center"><?=($Template->IsActive) ? "Yes" : "No";?></td>
														<td class="text-center"><?=$Template->CreatedByName.' '.$Template->CreatedDate;?></td>
														<td class="text-center"><?=$Template->LastModifiedByName.' '.$Template->LastModifiedDate;?></td>
													</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
		</div>
		<!-- /Row -->
	</div>	
<!-- Start Modal -->

<?php $this->load->view('Template/FooterView')?>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript" src="<?=SITE_URL?>dist/js/multiselect.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>
<script type="text/javascript">

function GoToNewView(View){
	$.ajax({
	    url: '<?=SITE_URL?>Administration/Actions/UserList/'+View,
	    data: { View:View },
	    method: "post",
	    dataType: 'html',
	    success: function (result) {
	        if (result != '') { 
	        	
	           $('#tblData').html(result); 
	           $("#userTable").DataTable().fnDestroy();
	           $('#userTable').DataTable( {
					responsive: true,
					"columnDefs": [ {
				          "targets": 'no-sort',
				          "orderable": false,
				    } ],
				    "aaSorting": [],
				    "dom": 'Bfrt<"bottom"ilp><"clear">',
					buttons: [
						'excel'
					],
					lengthMenu: [
						[ 10, 25, 50, -1 ],
						[ '10', '25', '50', 'All' ]
					],
					"bDestroy": true
				} );

			    $('input.myTable1globalfilter').on( 'keyup click', function () {
			        $('#myTable1').DataTable().search(
				        $('#myTable1globalfilter').val()
				    ).draw();
			    });

	           return true;  	
	        } else {
	        	$('#tblData').html('');
	            alert("Something went wrong.Please try again!");
	            return false;
	        }
	    }
	});	
}

$(document).ready(function() {
	GoToNewView("AllUsers");
});
</script>