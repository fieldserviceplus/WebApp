<?php $this->load->view('Template/HeaderView'); ?>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<div class="container-fluid pt-25">

	<!-- Row -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form class="form-horizontal" id="EditOrganization" method="post">
										<div class="form-body">
											<div class="txt-dark capitalize-font">
												<strong>Company Information</strong>
											</div>
											<hr class="light-grey-hr">
											<div class="form-actions mt-10 pb-25">
												<div class="row">
													<div class="col-md-12">
														<div class="row">
															<div class="col-md-offset-5 col-md-6">
																<a href="<?=SITE_URL;?>Setup/Organization/Actions/EditOrganization" class="btn btn-primary">Edit</a>
															</div>
														</div>
													</div>
													<div class="col-md-12"> </div>
												</div>
											</div>
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="col-md-6">

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Company Name
														</div>
														<div class="col-md-8">
															<?=$OrganizationData->CompanyName;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Phone #
														</div>
														<div class="col-md-8">
															<?=$OrganizationData->Phone;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Billing Address
														</div>
														<div class="col-md-8">
															<div><?=$OrganizationData->BillingAddress;?></div>
															<div><?=$OrganizationData->BillingCity;?>, <?=$OrganizationData->BillingState;?> <?=$OrganizationData->BillingPostalCode;?></div>
															<div><?=$OrganizationData->BillingCountry;?></div>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Created By
														</div>
														<div class="col-md-8">
															<?=$OrganizationData->CreatedBy;?> <?=$OrganizationData->CreatedDate;?>
														</div>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Primary Contact
														</div>
														<div class="col-md-8">
															<?=$OrganizationData->ContactName;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Website
														</div>
														<div class="col-md-8">
															<?=$OrganizationData->Website;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Org ID
														</div>
														<div class="col-md-8">
															<?=$OrganizationData->OrganizationID;?>
														</div>
													</div>

													<div class="form-group pt-45 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified By
														</div>
														<div class="col-md-8">
															<?=$OrganizationData->LastModifiedBy;?> <?=$OrganizationData->LastModifiedDate;?>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="txt-dark capitalize-font pt-50">
											<strong>License Information</strong>
										</div>
											<hr class="light-grey-hr">
											<div class="form-actions mt-10 pb-25">
												<div class="row">
													<div class="col-md-12">
														<div class="row">
															<div class="col-md-offset-5 col-md-6">
																<a href="<?=SITE_URL?>Setup/Organization/Actions/ChangePlan" class="btn btn-primary">Change Plan</a>
																<a href="<?=SITE_URL?>Setup/Organization/Actions/ChangeLicenses" class="btn btn-default">Add/Remove Licenses</a>
															</div>
														</div>
													</div>
													<div class="col-md-12"> </div>
												</div>
											</div>
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="col-md-6">

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Plan
														</div>
														<div class="col-md-8">
															<?=$OrganizationData->PlanName;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Contract
														</div>
														<div class="col-md-8">
															<?=$OrganizationData->ContractType?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Contract End Date
														</div>
														<div class="col-md-8">
															<?=$OrganizationData->ContractRenewalDate?>
														</div>
													</div>
												</div>

												<div class="col-md-6">

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Purchased Licenses
														</div>
														<div class="col-md-8">
															<?=$OrganizationData->TotalLicenses?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Used Licenses
														</div>
														<div class="col-md-8">
															<?=$OrganizationData->UsedLicenses?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Licenses Remaining
														</div>
														<div class="col-md-8">
															<?=($OrganizationData->TotalLicenses - $OrganizationData->UsedLicenses)?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			</div>
			<div id="profile_13" class="tab-pane fade" role="tabpanel">
				
				<?php //$this->load->view('Setup/Organization/Include/RelatedInformation'); ?>
				
			</div>
		</div>
		
		</div>

		<!-- <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
			<?php //$this->load->view('Setup/Organization/Include/ActivityTimeline'); ?>
		</div> -->
	</div>
	<!-- /Row -->
</div>


<!-- End Modal -->

<?php $this->load->view('Template/FooterView')?>





