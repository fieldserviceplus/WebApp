<?php $this->load->view('Template/HeaderView'); ?>

<script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_JS_KEY?>&libraries=places"></script>
 
<!-- Bootstrap Dropzone CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css"/>
<div class="container-fluid pt-25">
	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Edit Company Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">

						<div class="row">

							<div class="col-md-12">
								<div class="form-wrap"> 
									<form action="<?=SITE_URL;?>Setup/Organization/Actions/EditOrganization" class="form-horizontal" id="OrganizationForm" method="post">
										<div class="form-body">

											<!-- /Row -->
											<div class="row">

												<div class="col-md-12">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Company Name<span class="required-sign">*</span></label>
															<div class="col-md-9">
																<input type="text" class="form-control" name="CompanyName" id="CompanyName" value="<?=isset($OrganizationData->CompanyName)?$OrganizationData->CompanyName:"";?>">
																<?php echo form_error('CompanyName');  ?>
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-md-3">Contact</label>
															<div class="col-md-9">
															<?php $Contacts = $this->ActionsModel->GetContacts(); ?>
																<select class="form-control select2 SelectPrimaryContact" name="Contact" id="Contact">
																	<option value="">Select Contact</option>
																	<?php foreach ($Contacts as $Contact) { 
																	if($OrganizationData->Contact == $Contact->ContactID){?>
																			<option value="<?=$Contact->ContactID?>" selected=""><?=$Contact->FirstName.' '.$Contact->LastName?></option>
																		<?php } else {?>
																			<option value="<?=$Contact->ContactID?>"><?=$Contact->FirstName.' '.$Contact->LastName?></option>
																<?php }} ?>
																</select>
																<?php echo form_error('Contact');  ?>
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-md-3">Phone #<span class="required-sign">*</span></label>
															<div class="col-md-9">
																<input type="text" class="form-control" name="Phone" id="Phone" value="<?=isset($OrganizationData->Phone)?$OrganizationData->Phone:"";?>">
																<?php echo form_error('Phone');  ?>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Website<span class="required-sign">*</span></label>
															<div class="col-md-9">
																<input type="text" class="form-control" name="Website" id="Website" value="<?=isset($OrganizationData->Website)?$OrganizationData->Website:"";?>">
																<?php echo form_error('Website');  ?>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Edition<span class="required-sign">*</span></label>
															<div class="col-md-9">
																<input type="text" class="form-control" name="CompanyEdition" id="CompanyEdition" value="<?=isset($OrganizationData->CompanyEdition)?$OrganizationData->CompanyEdition:"";?>">
																<?php echo form_error('CompanyEdition');  ?>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Billing Address</label>
															<div class="col-md-9">
																<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-search"></i>Search Address</button>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Billing Street<span class="required-sign">*</span></label>
															<div class="col-md-9">
																<input type="text" class="form-control" name="BillingAddress" id="BillingAddress" value="<?=isset($OrganizationData->BillingAddress)?$OrganizationData->BillingAddress:"";?>">
																<?php echo form_error('BillingAddress');  ?>
																<input type="hidden" name="BillingLatitude" id="BillingLatitude" value="<?=isset($OrganizationData->BillingLatitude)?$OrganizationData->BillingLatitude:"";?>">
																<input type="hidden" name="BillingLongitude" id="BillingLongitude" value="<?=isset($OrganizationData->BillingLongitude)?$OrganizationData->BillingLongitude:"";?>">
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Billing City<span class="required-sign">*</span></label>
															<div class="col-md-9">
																<input type="text" class="form-control" name="BillingCity" id="BillingCity" value="<?=isset($OrganizationData->BillingCity)?$OrganizationData->BillingCity:"";?>">
																<?php echo form_error('BillingCity');  ?>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Billing State<span class="required-sign">*</span></label>
															<div class="col-md-9">
																<input type="text" class="form-control" name="BillingState" id="BillingState" value="<?=isset($OrganizationData->BillingState)?$OrganizationData->BillingState:"";?>">
																<?php echo form_error('BillingState');  ?>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Billing Country<span class="required-sign">*</span></label>
															<div class="col-md-9">
																<input type="text" class="form-control" name="BillingCountry" id="BillingCountry" value="<?=isset($OrganizationData->BillingCountry)?$OrganizationData->BillingCountry:"";?>">
																<?php echo form_error('BillingCountry');  ?>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Billing Postal Code<span class="required-sign">*</span></label>
															<div class="col-md-9">
																<input type="text" class="form-control" name="BillingPostalCode" id="BillingPostalCode" value="<?=isset($OrganizationData->BillingPostalCode)?$OrganizationData->BillingPostalCode:"";?>">
																<?php echo form_error('BillingPostalCode');  ?>
															</div>
														</div>

													</div>
												</div>
												
											</div>
											<!-- /Row -->
										</div>
										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary mr-10" name="EditOrganization" id="EditOrganization">Save</button>
															<button type="button" class="btn btn-default" onclick="window.history.go(-1); return false;">Cancel</button>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->


</div>

<?php $this->load->view('Setup/Organization/Modal/LocationPickerView')?>

<?php $this->load->view('Template/FooterView')?>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<script type="text/javascript">
	
$(document).ready(function () {

    $('#OrganizationForm').validate({ 
        ignore: [],
        debug: false,
        rules: { 
            CompanyName: {
        		required: true
            },
            Phone: {
                required: true
            },
            Website: {
                required: true
            },
            CompanyEdition: {
                required: true
            },
            BillingAddress: {
                required: true
            },
            BillingCity: {
                required: true
            },
            BillingState: {
                required: true
            },
            BillingCountry: {
                required: true
            },
            BillingPostalCode: {
                required: true
            },
            BillingCountry: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
});
</script>

<!-- Set Location -->
<script src="<?=SITE_URL?>dist/js/location-picker.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	set_location_in_map("Billing");
});
</script>







