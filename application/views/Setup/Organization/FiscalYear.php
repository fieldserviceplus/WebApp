<?php $this->load->view('Template/HeaderView'); ?>
<!-- Bootstrap Dropzone CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css"/>
<div class="container-fluid pt-25">
	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Organization Fiscal Year</h6>
						<p class="sub-title">To specify the fiscal year type for your organization, choose one of the options below.</p>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Setup/Organization/Actions/FiscalYear" class="form-horizontal" id="FiscalYearForm" method="post" enctype="multipart/form-data">
										<div class="form-body">

											<!-- /Row -->
											<div class="row">
												<div class="col-md-4"></div>
												<div class="col-md-4">

													<div class="form-group">
														<label class="control-label col-md-4 text-left">Fiscal Year Start Month<span class="required-sign">*</span></label>
														<div class="col-md-4">
															<select name="FiscalYearStart" id="FiscalYearStart" class="form-control select2">
																<?php $Months = $this->ActionsModel->OrgFiscalYearStart();
																	foreach ($Months as $Month) { 
																		if($OrganizationData->FiscalYearStart == $Month->OrgFiscalYearStartID){ ?>
																			<option value="<?=$Month->OrgFiscalYearStartID?>" selected=""><?=$Month->FiscalYearStart?></option>
																	<?php } else { ?>
																		<option value="<?=$Month->OrgFiscalYearStartID?>"><?=$Month->FiscalYearStart?></option>
																		<?php }
																	} ?>
															</select>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4 text-left">Fiscal Year is Based On<span class="required-sign">*</span></label>
														<div class="col-md-4">
															<div>
																<div class="radio radio-primary">
																	<input type="radio" name="FiscalYearIsBasedOn" id="FiscalYearIsBasedOn1" value="EndingMonth" <?=($OrganizationData->FiscalYearIsBasedOn == 'EndingMonth') ? "checked=''" : "";?>>
																	<label for="FiscalYearIsBasedOn1"> Ending Month </label>
																</div>
																<div class="radio radio-primary">
																	<input type="radio" name="FiscalYearIsBasedOn" id="FiscalYearIsBasedOn2" value="StartingMonth" <?=($OrganizationData->FiscalYearIsBasedOn == 'StartingMonth') ? "checked=''" : "";?>>
																	<label for="FiscalYearIsBasedOn2"> Starting Month</label>
																</div>
															</div>
														</div>
													</div>

												</div>
											</div>
											<!-- /Row -->
										</div>
										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary mr-10" name="FiscalYear" id="FiscalYear">Save Changes</button>
															<button type="button" class="btn btn-default" onclick="window.history.go(-1); return false;">Cancel</button>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->


</div>

<?php $this->load->view('Template/FooterView')?>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<script type="text/javascript">
	
$(document).ready(function () {

    $('#CreateNewView').validate({ 
        rules: {
            ContactViewName: {
                required: true
            },
            "SpecifyFieldsDisplay[]": {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
});
</script>
<!-- For Send Email Madal Start --> 








