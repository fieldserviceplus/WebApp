<?php $this->load->view('Template/HeaderView'); ?>
<!-- Bootstrap Dropzone CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css"/>
<div class="container-fluid pt-25">
	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Add/Remove Licenses</h6>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Setup/Organization/Actions/ChangeLicenses" class="form-horizontal" id="ChangePlanForm" method="post" enctype="multipart/form-data">
										<div class="form-body">

											<!-- /Row -->
											<div class="row">
												<div class="col-md-3"></div>
												<div class="col-md-3">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															<strong>Current licenses</strong>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Purchased
														</div>
														<div class="col-md-8">
															<?=$OrganizationData->TotalLicenses?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Used
														</div>
														<div class="col-md-8">
															<?=$OrganizationData->UsedLicenses?>
														</div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-5 text-left txt-dark">
															Current Plan
														</div>
														<div class="col-md-7">
															<?=$OrganizationData->PlanName?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-5 text-left txt-dark">
															Contract Term
														</div>
														<div class="col-md-7">
															<?=$OrganizationData->ContractType?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-5 text-left txt-dark">
															Contract End Date
														</div>
														<div class="col-md-7">
															<?=$OrganizationData->ContractRenewalDate?>
														</div>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12 pt-10">
													<div class="col-md-4"></div>
													<div class="col-md-6">
														<div class="form-group">
															<div class="col-md-3">
																<select name="Action" id="Action" class="form-control select2">
																	<option>Add</option>
																	<option>Remove</option>
																</select>
															</div>
															<div class="col-md-2">
																<input type="text" class="form-control vertical-spin" name="Licenses" id="Licenses" value="1" onchange="ChangeTotalPrice(this.value);" readonly="" >
															</div>
															<div class="col-md-7 pt-5 pb-5">
																Current Price - 
																<?php if($OrganizationData->ContractType=="Annual"){ ?>
																	$<?=$OrganizationData->UserPriceAnnual?> / user / year
																<?php } else { ?>
																	$<?=$OrganizationData->UserPriceMonthly?> / user / month
																<?php }?>
															</div>
														</div>
													</div>
													<div class="col-md-12">
														<div class="col-md-4"></div>
														<div class="col-md-6">Total Price - 
															<?php if($OrganizationData->ContractType=="Annual"){ ?>
																$<span id="TotalPrice"><?=$OrganizationData->UserPriceAnnual?></span> / user / year
															<?php } else { ?>
																$<span id="TotalPrice"><?=$OrganizationData->UserPriceMonthly?></span> / user / month
															<?php }?>
														</div>
													</div>
												</div>
												<div class="col-md-12 pt-25 pb-10">
													<div class="col-md-3"></div>
													<div class="col-md-6">
														Some text goes here that explains the terms of the transaction...It will be different language based on whether they are adding or removing licenses.
													</div>
												</div>
											</div>
											<!-- /Row -->
										</div>
										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary mr-10" name="ChangeLicenses" id="ChangeLicenses">Save Changes</button>
															<button type="button" class="btn btn-default" onclick="window.history.go(-1); return false;">Cancel</button>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->


</div>

<?php $this->load->view('Template/FooterView')?>

<!-- Bootstrap Touchspin JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script type="text/javascript">
	$(".vertical-spin").TouchSpin({
		min: 1,
        max: 9,
		verticalbuttons: true,
		verticalupclass: 'ti-plus',
		verticaldownclass: 'ti-minus'
	});
	var vspinTrue = $(".vertical-spin").TouchSpin({
		verticalbuttons: true
	});
	if (vspinTrue) {
		$('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
	}

	$(".vertical-spin-occurrence").TouchSpin({
		min: 1,
		verticalbuttons: true,
		verticalupclass: 'ti-plus',
		verticaldownclass: 'ti-minus'
	});
</script>
<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<script type="text/javascript">
	
$(document).ready(function () {

    $('#CreateNewView').validate({ 
        rules: {
            ContactViewName: {
                required: true
            },
            "SpecifyFieldsDisplay[]": {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
});

function ChangeTotalPrice(Licenses)
{
	<?php if($OrganizationData->ContractType=="Annual"){ ?>
		$("#TotalPrice").html(parseFloat(Licenses) * parseFloat('<?=$OrganizationData->UserPriceAnnual?>'));
	<?php } else { ?>
		$("#TotalPrice").html(parseFloat(Licenses) * parseFloat('<?=$OrganizationData->UserPriceMonthly?>'));
	<?php }?>
	
}
</script>
<!-- For Send Email Madal Start --> 








