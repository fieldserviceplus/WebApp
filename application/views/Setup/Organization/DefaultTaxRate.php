<?php $this->load->view('Template/HeaderView'); ?>

<div class="container-fluid pt-25">
	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Organization Tax Rate</h6>
						<p class="sub-title">To specify the default tax rate for your organization to be used for Estimates, Work Orders and Invoices. Enter a value below.</p>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Setup/Organization/Actions/DefaultTaxRate" class="form-horizontal" id="DefaultTaxRateForm" method="post" enctype="multipart/form-data">
										<div class="form-body">

											<!-- /Row -->
											<div class="row">
												<!-- <div class="col-md-2"></div> -->
												<div class="col-md-4">

													<div class="form-group">
														<label class="control-label col-md-4 text-left">Default Tax Rate<span class="required-sign">*</span></label>
														<div class="col-md-5">
															<div class="input-group"> 
																<input type="text" class="form-control allow-number-dot" name="TaxRate" id="TaxRate" value="<?=$OrganizationData->Tax;?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
													</div>

												</div>
											</div>
											<!-- /Row -->
										</div>
										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary mr-10" name="DefaultTaxRate" id="DefaultTaxRate">Save</button>
															<button type="button" class="btn btn-default" onclick="window.history.go(-1); return false;">Cancel</button>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->


</div>

<?php $this->load->view('Template/FooterView')?>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<script type="text/javascript">
	
$(document).ready(function () {

    $('#DefaultTaxRateForm').validate({ 
        rules: {
            DefaultTaxRate: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
});
</script>
<!-- For Send Email Madal Start --> 








