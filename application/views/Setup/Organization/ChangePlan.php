<?php $this->load->view('Template/HeaderView'); ?>
<!-- Bootstrap Dropzone CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css"/>
<div class="container-fluid pt-25">
	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Change Plan</h6>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Setup/Organization/Actions/ChangePlan" class="form-horizontal" id="ChangePlanForm" method="post" enctype="multipart/form-data">
										<div class="form-body">

											<!-- /Row -->
											<div class="row">
												<div class="col-md-4"></div>
												<div class="col-md-5">

													<div class="form-group">
														<label class="control-label col-md-4 text-left">Plan<span class="required-sign">*</span></label>
														<div class="col-md-4">
															<select name="OrgPlanID" id="OrgPlanID" class="form-control select2">
																<?php $Plans = $this->ActionsModel->GetPlans();
																	foreach ($Plans as $Plan) { 
																		if($OrganizationData->Plan == $Plan->OrgPlanID){ ?>
																			<option value="<?=$Plan->OrgPlanID?>" selected=""><?=$Plan->PlanName?></option>
																	<?php } else { ?>
																		<option value="<?=$Plan->OrgPlanID?>"><?=$Plan->PlanName?></option>
																		<?php }
																	} ?>
															</select>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Billing Cycle
														</div>
														<div class="col-md-8">
															<?=$OrganizationData->ContractType?>	
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Contract End Date
														</div>
														<div class="col-md-8">
															<?=$OrganizationData->ContractRenewalDate?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Currently <?=$OrganizationData->ContractType?> Payment
														</div>
														<div class="col-md-8">
															<?php if($OrganizationData->ContractType=="Annual"){ ?>
																$<?=$OrganizationData->UserPriceAnnual?> / user / year
															<?php } else { ?>
																$<?=$OrganizationData->UserPriceMonthly?> / user / month
															<?php }?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															New <?=$OrganizationData->ContractType?> Payment
														</div>
														<div class="col-md-8">
															<?php if($OrganizationData->ContractType=="Annual"){ ?>
																$<?=$OrganizationData->UserPriceAnnual?> / user / year
															<?php } else { ?>
																$<?=$OrganizationData->UserPriceMonthly?> / user / month
															<?php }?>
														</div>
													</div>

													<div class="form-group pt-10">
														<div class="col-md-12 text-center">
															Some text about the transaction goes here...
														</div>
													</div>

												</div>
											</div>
											<!-- /Row -->
										</div>
										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary mr-10" name="ChangePlan" id="ChangePlan">Save Changes</button>
															<button type="button" class="btn btn-default" onclick="window.history.go(-1); return false;">Cancel</button>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>

										<div class="row">

											<hr class="light-grey-hr">
											<!-- item -->
											<div class="col-lg-3 col-md-6 col-sm-12 text-center mb-20"></div>
											<div class="col-lg-3 col-md-6 col-sm-12 text-center mb-20">
												<div class="panel panel-pricing mb-0">
													<div class="panel-heading">
														<i class=" ti-bookmark-alt"></i>
														<h6>FREE</h6>
														<span class="panel-price"><span class="pricing-dolor">$</span>0</span>
													</div>
													<div class="panel-body text-center pl-0 pr-0">
														<hr class="mb-30">
														<ul class="list-group mb-0 text-center">
															<li class="list-group-item"><i class="fa fa-check"></i> Free forever</li>
															<li><hr class="mt-5 mb-5"></li>
															<li class="list-group-item"><i class="fa fa-check"></i> Up to 3 Users</li>
															<li><hr class="mt-5 mb-5"></li>
															<li class="list-group-item"><i class="fa fa-check"></i> 27/7 support</li>
														</ul>
													</div>
													<div class="panel-footer pb-35">
														<a class="btn btn-success btn-rounded btn-lg" href="#">Try Free</a>
													</div>
												</div>
											</div>
											<!-- /item -->
											<!-- item -->
											<div class="col-lg-3 col-md-6 col-sm-12 text-center mb-20">
												<div class="panel panel-pricing mb-0">
													<div class="panel-heading">
														<i class="ti-wallet"></i>
														<h6>Basic</h6>
														<span class="panel-price"><span class="pricing-dolor">$</span>9</span>
													</div>
													<div class="panel-body text-center pl-0 pr-0">
														<hr class="mb-30">
														<ul class="list-group mb-0 text-center">
															<li class="list-group-item"><i class="fa fa-check"></i> Personal use</li>
															<li><hr class="mt-5 mb-5"></li>
															<li class="list-group-item"><i class="fa fa-check"></i> 10 projects/month</li>
															<li><hr class="mt-5 mb-5"></li>
															<li class="list-group-item"><i class="fa fa-check"></i> 27/7 support</li>
														</ul>
													</div>
													<div class="panel-footer pb-35">
														<a class="btn btn-success btn-rounded btn-lg" href="#">Get Basic</a>
													</div>
												</div>
											</div>
											<!-- /item -->
											<!-- item -->
											<!-- <div class="col-lg-3 col-md-6 col-sm-12 text-center mb-30">
												<div class="panel panel-pricing mb-0">
													<div class="panel-heading">
														<i class=" ti-crown"></i>
														<h6>business</h6>
														<span class="panel-price"><span class="pricing-dolor">$</span>55</span>
													</div>
													<div class="panel-body text-center pl-0 pr-0">
														<hr class="mb-30">
														<ul class="list-group mb-0 text-center">
															<li class="list-group-item"><i class="fa fa-check"></i> Personal use</li>
															<li><hr class="mt-5 mb-5"></li>
															<li class="list-group-item"><i class="fa fa-check"></i> 135 projects/year</li>
															<li><hr class="mt-5 mb-5"></li>
															<li class="list-group-item"><i class="fa fa-check"></i> 27/7 support</li>
														</ul>
													</div>
													<div class="panel-footer pb-35">
														<a class="btn btn-success btn-rounded btn-lg" href="#">subscribe now</a>
													</div>
												</div>
											</div> -->
											<!-- /item -->
											<!-- item -->
											<!-- <div class="col-lg-3 col-md-6 col-sm-12 text-center mb-30">
												<div class="panel panel-pricing mb-0">
													<div class="panel-heading">
														<i class=" ti-shield"></i>
														<h6>corporate</h6>
														<span class="panel-price"><span class="pricing-dolor">$</span>89</span>
													</div>
													<div class="panel-body text-center pl-0 pr-0">
														<hr class="mb-30">
														<ul class="list-group mb-0 text-center">
															<li class="list-group-item"><i class="fa fa-check"></i> Personal use</li>
															<li><hr class="mt-5 mb-5"></li>
															<li class="list-group-item"><i class="fa fa-check"></i> Unlimited projects</li>
															<li><hr class="mt-5 mb-5"></li>
															<li class="list-group-item"><i class="fa fa-check"></i> 27/7 support</li>
														</ul>
													</div>
													<div class="panel-footer pb-35">
														<a class="btn btn-success btn-rounded btn-lg" href="#">subscribe now</a>
													</div>
												</div>
											</div> -->
											<!-- /item -->
											
										</div>

										<hr class="light-grey-hr">
										<!-- /Row -->
										<div class="row">
											<div class="col-md-4"></div>
											<div class="col-md-5">

												<div class="form-group">
													<label class="control-label col-md-4 text-left">Deactivate Account</label>
													<div class="col-md-4 text-center">
														<a href="<?=SITE_URL;?>Setup/Organization/Actions/DeactivateAccount" class="btn btn-danger">Deactivate</a>
													</div>
												</div>
											</div>
										</div>

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->


</div>

<?php $this->load->view('Template/FooterView')?>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<script type="text/javascript">
	
$(document).ready(function () {

    $('#CreateNewView').validate({ 
        rules: {
            ContactViewName: {
                required: true
            },
            "SpecifyFieldsDisplay[]": {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
});
</script>
<!-- For Send Email Madal Start --> 








