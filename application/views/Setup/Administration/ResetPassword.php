<?php $this->load->view('Template/HeaderView')?>

<div class="container-fluid pt-25">
	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Reset Password</h6>
					</div>
					<div class="pull-right">
						<span style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong></span>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Setup/Administration/Actions/ResetPassword" class="form-horizontal" id="ResetPassword" method="post">
										<div class="form-body">
											<!-- /Row -->
											<div class="row">

												<div class="col-md-12">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Old Password<span class="required-sign">*</span></label>
														<div class="col-md-4">
															<input type="password" class="form-control" name="OldPassword" id="OldPassword">
															<?php echo form_error('OldPassword');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">New Password<span class="required-sign">*</span></label>
														<div class="col-md-4">
															<input type="password" class="form-control" name="NewPassword" id="NewPassword">
															<?php echo form_error('NewPassword');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Confirm New Password<span class="required-sign">*</span></label>
														<div class="col-md-4">
															<input type="password" class="form-control" name="ConfirmNewPassword" id="ConfirmNewPassword">
															<?php echo form_error('ConfirmNewPassword');  ?>
														</div>
													</div>

												</div>
											</div>
										</div>
										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary" name="ResetPassword">Save</button>
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserList" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->

<?php $this->load->view('Setup/Administration/Modal/LocationPickerView'); ?>
<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">
$(document).ready(function () {
 
    $('#ResetPassword').validate({ 
        rules: {
            OldPassword: {
                required: true,
		        minlength: 6
            },
            NewPassword: {
                required: true,
		        minlength: 6
            },
            ConfirmNewPassword: {
                required: true,
		        minlength: 6,
		        equalTo: "#NewPassword"
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    }); 
});
</script>




