
<?php $this->load->view('Template/HeaderView')?>
<style type="text/css">.dataTables_filter { display: none; }</style>

    <div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10 pb-10">
					<h6 class="panel-title txt-dark pt-10">Users</h6>
					
				</div>
				<div  class="panel-wrapper collapse in">
					<div class="panel-body">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 pr-0 pl-0">
							<form method="post" action="#" id="SelectedWoForm">
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-3">
									<label class="control-label mt-10"><strong>View:</strong></label>
								</div>
								<div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 pr-0">
									<select name="SelectedEstimate" class="selectpicker" data-style="form-control btn-default btn-outline" onchange="GoToNewView(this.value)"> 
										<option value="AllUsers">All Users</option>
										<option value="ActiveUsers">Active Users</option>
										<option value="AllAdministrators">All Administrators</option>
									</select>
								</div>	
							</form>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						</div>
						<div class="col-lg-5 col-md-4 col-sm-3 col-xs-12">
							<div class="btn-group pull-right">
							</div>
							<div class="pull-right">
								<a href="<?=SITE_URL?>Setup/Administration/Actions/CreateUser" class="btn btn-primary btn-outline">Add User</a>
							</div>	
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							
								<div class="table-responsive">
									<table id="userTable" class="table table-hover display pb-30" width="100%">
										<thead>
											<tr> 
												<th class="text-center">Action</th>
												<th class="text-center">Full Name</th>
												<th class="text-center">Email</th>
												<th class="text-center">Last Login</th>
												<th class="text-center">Active</th>
												<th class="text-center">Profile</th>
											</tr>
										</thead>
										<tbody id="tblData">
										
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
		</div>
		<!-- /Row -->
	</div>	

<!-- Start Modal -->

<?php $this->load->view('Template/FooterView')?>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript" src="<?=SITE_URL?>dist/js/multiselect.min.js"></script>


<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>
<script type="text/javascript">
/*$(document).ready(function() {
	$('.buttons-excel').hide();
	$("#Export-Excel").on("click", function() {
	    $( '.buttons-excel' ).click();
	});
});*/

function GoToNewView(View){
	
	var table = $('#userTable').DataTable();

	$.ajax({
	    url: '<?=SITE_URL?>Setup/Administration/Actions/UserList/'+View,
	    data: { View:View },
	    method: "post",
	    dataType: 'html',
	    success: function (result) {
	        if (result != '') { 
	       
		        table.destroy();

		        $('#tblData').html(result);

		        table = $('#userTable').DataTable( {
					responsive: true,
					"columnDefs": [ {
				          "targets": 'no-sort',
				          "orderable": false,
				    } ],
				    "aaSorting": [],
				    "dom": 'frt<"bottom"ilp><"clear">',
					/*buttons: [
						'excel'
					],*/
					lengthMenu: [
						[ 10, 25, 50, -1 ],
						[ '10', '25', '50', 'All' ]
					],
					destroy: false,
					/*"bDestroy": false*/
				} );

			    $('input.myTable1globalfilter').on( 'keyup click', function () {
			        $('#myTable1').DataTable().search(
				        $('#myTable1globalfilter').val()
				    ).draw();
			    });

	           return true;  	
	        } else {
	        	$('#tblData').html('');
	            alert("Something went wrong.Please try again!");
	            return false;
	        }
	    }
	});	
}
$(document).ready(function() {

	GoToNewView("AllUsers");
});
</script>