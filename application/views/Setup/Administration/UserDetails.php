<?php $this->load->view('Template/HeaderView'); ?>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10 pb-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['User']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['User']['title']?></span>
									<span class="panel-title txt-dark"><?=$UserData->FirstName;?> <?=$UserData->LastName;?></span>
								</div>
							</div>
						</div>
					</ul>
					<div class="pull-right pb-10">
						<a href="<?=SITE_URL?>Setup/Administration/Actions/EditUser/<?=$UserData->UserID;?>" class="btn btn-primary">Edit</a>
						<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserList" class="btn btn-default">Cancel</a>
						<a href="<?=SITE_URL;?>Setup/Administration/Actions/ResetPassword" class="btn btn-primary btn-outline">Reset Password</a>
					</div>
					<div class="clearfix"></div>
				</div>
				<!-- <div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">
							
							<div class="btn-group pull-right">
									<div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li>
												<a><span><strong>Communication</strong></span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>User/Actions/SendEmail/<?=$UserData->UserID;?>"><span>Send Email</span></a>
											</li>
											<li>
												<a><span><strong>Add New Actions</strong></span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>WorkOrder/Actions/CreateWorkOrder/"><span>New Work Order</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>Estimate/Actions/CreateEstimate/"><span>New Estimate</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>Invoice/Actions/CreateInvoice/"><span>New Invoice</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>Event/Actions/CreateEvent/"><span>New Event</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>Task/Actions/CreateTask/"><span>New Task</span></a>
											</li>
											<li> 
												<a href="JavaScript:void(0);" data-toggle="modal" ><span>New File</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#CreateNewNote"><span>New Note</span></a>
											</li>
											<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
												<a><span><strong>User Actions</strong></span></a>
											</li>
											<li> 
												<a href="<?=SITE_URL;?>User/Actions/CreateUser/<?=$UserData->UserID?>/"><span>Copy User</span></a>
											</li>
											<li>
												<a href="#" onclick="DeleteUser();"><span>Delete User</span></a>
											</li>
										</ul>
									</div>
								</div>
								<div class="btn-group pull-right">
									<a href="<?=SITE_URL?>Setup/Administration/Actions/EditUser/<?=$UserData->UserID;?>" class="btn btn-primary btn-outline">Edit</a>
								</div> 
						</div>
					</div>
				</div> -->
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
			 	<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">User Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>  -->
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>User/Actions/EditUser/<?=$UserData->UserID?>" class="form-horizontal" id="EditUser" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>User Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
														 Name
														</div>
														<div class="col-md-8">
															<?=$UserData->Title;?> <?=$UserData->FirstName;?> <?=$UserData->LastName;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Email
														</div>
														<div class="col-md-8">
															<?=$UserData->Email;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Company
														</div>
														<div class="col-md-8">
															<?=$UserData->CompanyName;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Department
														</div>
														<div class="col-md-8">
															<?=$UserData->DepartmentName;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Division
														</div>
														<div class="col-md-8">
															<?=$UserData->DivisionName;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Manager
														</div>
														<div class="col-md-8">
															<?=$UserData->ManagerName;?>
														</div>
													</div>
												</div>

												<div class="col-md-6">

													<div class="form-group pt-5">
														<div class="col-md-4 text-left txt-dark">
															Is Active
														</div>
														<div class="col-md-8">
															<div class="checkbox checkbox-primary pt-0 pb-0">
																<input id="checkbox12" disabled="" <?=$UserData->IsActive==1?'checked':'';?> type="checkbox">
																<label for="checkbox12"></label>
															</div>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Phone No #
														</div>
														<div class="col-md-8">
															<?=$UserData->PhoneNo;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Cell No
														</div>
														<div class="col-md-8">
															<?=$UserData->MobileNo;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Profile
														</div>
														<div class="col-md-8">
															<?=$UserData->ProfileName;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Start Date
														</div>
														<div class="col-md-8">
															<?=$UserData->StartDate;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															End Date
														</div>
														<div class="col-md-8">
															<?=$UserData->EndDate;?>
														</div>
													</div>

												</div>
											</div>

											<div class="col-md-12">
											<div class="txt-dark capitalize-font pt-10"><strong>Address Information</strong></div>
											<hr class="light-grey-hr">
												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Mailing Address
														</div>
														<div class="col-md-8">
															<div><?=$UserData->Address;?></div>
															<div><?=$UserData->City;?>, <?=$UserData->State;?> <?=$UserData->PostalCode;?></div>
															<div><?=$UserData->Country;?></div>
														</div>
													</div>
												</div>
											</div>

											<div class="col-md-12">
												<div class="txt-dark capitalize-font pt-10"><strong>Locale Settings</strong></div>
												<hr class="light-grey-hr">
												
												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Time Zone
														</div>
														<div class="col-md-8">
															<?=$UserData->TimeZoneName;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Start Of Day
														</div>
														<div class="col-md-8">
															<?=$UserData->StartOfDay;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															End Of Day
														</div>
														<div class="col-md-8">
															<?=$UserData->EndOfDay;?>
														</div>
													</div>
												</div>
											</div>

											<div class="col-md-12">
												<div class="txt-dark capitalize-font pt-10"><strong>Email Settings</strong></div>
												<hr class="light-grey-hr">
												
												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Email Sender Address
														</div>
														<div class="col-md-8">
															<?=$UserData->SenderEmail;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Email Sender Name
														</div>
														<div class="col-md-8">
															<?=$UserData->SenderName;?>
														</div>
													</div>
													<!-- <div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Email Signature
														</div>
														<div class="col-md-8">
															<?=$UserData->EmailSignature;?>
														</div>
													</div> -->
												</div>
											</div>

											<div class="col-md-12">
												<div class="txt-dark capitalize-font pt-10"><strong>Notifications</strong></div>
												<hr class="light-grey-hr">
												
												<div class="col-md-6">
													<!-- <div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Default Group Notification Frequency
														</div>
														<div class="col-md-8">
															<?=$UserData->DefaultGrpNotificationFreq;?>
														</div>
													</div> -->
													<div class="form-group pt-5">
														<div class="col-md-4 text-left txt-dark">
															Receive Admin Emails
														</div>
														<div class="col-md-8">
															<div class="checkbox checkbox-primary pt-0 pb-0">
																<input id="checkbox12" disabled="" <?=$UserData->ReceiveAdminEmails==1?'checked':'';?> type="checkbox">
																<label for="checkbox12"></label>
															</div>
														</div>
													</div>

												</div>
											</div>

											<div class="col-md-12">
												<div class="txt-dark capitalize-font pt-10"><strong>System Information</strong></div>
												<hr class="light-grey-hr">
												
												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Create Date
														</div>
														<div class="col-md-8">
															<?=$UserData->CreatedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Created By
														</div>
														<div class="col-md-8">
															<a href="#"><?=$UserData->CreatedBy;?></a>
														</div>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified Date
														</div>
														<div class="col-md-8">
															<?=$UserData->LastModifiedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified By
														</div>
														<div class="col-md-8">
															<a href="#"><?=$UserData->LastModifiedBy;?></a>
														</div>
													</div>
												</div>
											</div>


										</div>
											<!-- /Row -->
									</div>

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		
			</div>
			<div id="profile_13" class="tab-pane fade" role="tabpanel">

			</div>
		</div>
		</div>
	</div>
	<!-- /Row -->
</div>

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<!-- Use for common object  -->
<script src="<?=SITE_URL?>dist/js/common.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">
function DeleteUser(UserID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#f8b32d",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>User/Actions/DeleteUser/<?=$UserData->UserID;?>/";
		}, 500);
    });
	return false;
}
</script>


