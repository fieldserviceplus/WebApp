<?php $this->load->view('Template/HeaderView')?>

<script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_JS_KEY?>&libraries=places"></script>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
 
<div class="container-fluid pt-25">

	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Edit User</h6>
					</div>
					<div class="pull-right">
						<span style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong></span>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Setup/Administration/Actions/EditUser/<?=$UserData->UserID?>" class="form-horizontal" id="CreateUser" method="post">
										<div class="form-body">

											<div class="form-actions mt-10">
												<div class="row">
													<div class="col-md-12">
														<div class="row">
															<div class="col-md-offset-5 col-md-6">
																<button type="submit" class="btn btn-primary" name="EditUser">Save</button>
																<button type="submit" class="btn btn-primary" name="EditUserSaveNew">Save & New</button>
																<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserList" class="btn btn-default">Cancel</a>
															</div>
														</div>
													</div>
													<div class="col-md-12"> </div>
												</div>
											</div>
									
											<!-- /Row -->
											<div class="row">

												<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>User Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">First Name<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="FirstName" id="FirstName" value="<?=isset($UserData->FirstName)?$UserData->FirstName:"";?>">
															<?php echo form_error('FirstName');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Last Name<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="LastName" id="LastName" value="<?=isset($UserData->LastName)?$UserData->LastName:"";?>">
															<?php echo form_error('LastName');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Email<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="email" class="form-control" name="Email" id="Email" value="<?=isset($UserData->Email)?$UserData->Email:"";?>">
															<?php echo form_error('Email');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Title</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Title" id="Title" value="<?=isset($UserData->Title)?$UserData->Title:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Company<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="OrganizationID" id="OrganizationID" class="form-control" disabled="">
														<option value="">Select Company</option>
														<?php $Organizations = $this->ActionsModel->GetOrganizations();
															foreach ($Organizations as $Organization) { 
																if(isset($UserData->OrganizationID) && $UserData->OrganizationID == $Organization->OrganizationID){ ?>
																	<option value="<?=$Organization->OrganizationID?>" selected="selected"><?=$Organization->CompanyName?></option>
																<?php } else { ?>
																	<option value="<?=$Organization->OrganizationID?>"><?=$Organization->CompanyName?></option>
																<?php } ?>
															<?php } ?>
														</select>
															<?php echo form_error('OrganizationID');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Department</label>
														<div class="col-md-9">
														<select name="Department" id="Department" class="form-control">
														<option value="">Select Department</option>
															<?php $Departments = $this->ActionsModel->GetDepartments();
															foreach ($Departments as $Department) { 
																if(isset($UserData->Department) && $UserData->Department == $Department->DepartmentID){ ?>
																	<option value="<?=$Department->DepartmentID?>" selected="selected"><?=$Department->DepartmentName?></option>
																<?php } else { ?>
																	<option value="<?=$Department->DepartmentID?>"><?=$Department->DepartmentName?></option>
																<?php }
																} ?>
														</select>
															<?php echo form_error('Department');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Division</label>
														<div class="col-md-9">
														<select name="Division" id="Division" class="form-control">
														<option value="">Select Division</option>
															<?php $Divisions = $this->ActionsModel->GetDivisions();
															foreach ($Divisions as $Division) { 
																if(isset($UserData->Division) && $UserData->Division == $Division->DivisionID){ ?>
																<option value="<?=$Division->DivisionID?>" selected="selected"><?=$Division->DivisionName?></option>
															<?php } else { ?>
																	<option value="<?=$Division->DivisionID?>"><?=$Division->DivisionName?></option>
																<?php } 
															}?>
														</select>
															<?php echo form_error('Division');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Manager<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="Manager" id="Manager" class="form-control select2">
														<option value="">Select Manager</option>
															<?php $Users = MY_Model::GetAllUsers();
															foreach ($Users as $User) { 
																if(isset($UserData->Manager) && $UserData->Manager == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName?> <?=$User->LastName?></option>
															<?php } else { ?>
																	<option value="<?=$User->UserID?>"><?=$User->FirstName?> <?=$User->LastName?></option>
																<?php } 
																}?>
														</select>
															<?php echo form_error('Manager');  ?>
														</div>
													</div>
												</div>

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Is Active</label>
														<div class="col-md-9">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="IsActive" id="IsActive" value="1" <?=($UserData->IsActive == 1)?"checked=''":"";?>>
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Phone No</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="PhoneNo" id="PhoneNo" value="<?=isset($UserData->PhoneNo)?$UserData->PhoneNo:"";?>">
															<?php echo form_error('PhoneNo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Cell No<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="MobileNo" id="MobileNo" value="<?=isset($UserData->MobileNo)?$UserData->MobileNo:"";?>">
															<?php echo form_error('MobileNo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Profile<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="Profile" id="Profile" class="form-control select2">
														<option value="">Select Profile</option>
															<?php $Profiles = $this->ActionsModel->GetProfiles();
															foreach ($Profiles as $Profile) { 
																if(isset($UserData->Profile) && $UserData->Profile == $Profile->ProfileID){ ?>
																	<option value="<?=$Profile->ProfileID?>" selected="selected"><?=$Profile->Name?></option>
															<?php } else { ?>
																	<option value="<?=$Profile->ProfileID?>"><?=$Profile->Name?></option>
																<?php }
																} ?>
														</select>
														<?php echo form_error('Profile');  ?>
														</div>
													</div>

													<!-- <div class="form-group">
														<label class="control-label col-md-3">Employee No</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="EmployeeNo" id="EmployeeNo" value="">
														</div>
													</div>  -->

													<div class="form-group">
														<label class="control-label col-md-3">Start Date</label>
														<div class="col-md-9">
															<input type="text" class="form-control showdatepicker" name="StartDate" id="StartDate" value="<?=isset($UserData->StartDate)?$UserData->StartDate:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">End Date</label>
														<div class="col-md-9">
															<input type="text" class="form-control showdatepicker" name="EndDate" id="EndDate" value="<?=isset($UserData->EndDate)?$UserData->EndDate:"";?>">
														</div>
													</div>
											
												</div>
												<!--/span-->
												</div>

												<div class="col-md-12">
												<div class="txt-dark capitalize-font"><strong>Mailing Address</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Mailing Address</label>
														<div class="col-md-9">
																<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-search"></i>Search Address</button>
														</div>
													</div>
												
													<div class="form-group">
														<label class="col-sm-3 control-label">Mailing Street<span class="required-sign">*</span></label>
														<div class="col-sm-9">
															<input type="text" class="form-control" name="Address" id="Address" value="<?=isset($UserData->Address)?$UserData->Address:"";?>">
															<?php echo form_error('Address');  ?>
															<input type="hidden" name="Latitude" id="Latitude" value="<?=isset($UserData->Latitude)?$UserData->Latitude:"";?>">
															<input type="hidden" name="Longitude" id="Longitude" value="<?=isset($UserData->Longitude)?$UserData->Longitude:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Mailing City<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="City" id="City" value="<?=isset($UserData->City)?$UserData->City:"";?>">
															<?php echo form_error('City');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Mailing State<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="State" id="State" value="<?=isset($UserData->State)?$UserData->State:"";?>">
															<?php echo form_error('State');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-3">Mailing Country<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Country" id="Country" value="<?=isset($UserData->Country)?$UserData->Country:"";?>">
															<?php echo form_error('Country');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Mailing Postal Code<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="PostalCode" id="PostalCode" value="<?=isset($UserData->PostalCode)?$UserData->PostalCode:"";?>">
															<?php echo form_error('PostalCode');  ?>
														</div>
													</div>
												</div>

												</div>


												<div class="col-md-12">
												<div class="txt-dark capitalize-font"><strong>Locale Settings</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-3">Time Zone<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="TimeZone" id="TimeZone" class="form-control select2">
														<option value="">Select Time Zone</option>
															<?php $TimeZones = $this->ActionsModel->GetTimeZones();
															foreach ($TimeZones as $TimeZone) { 
																if(isset($UserData->TimeZone) && $UserData->TimeZone == $TimeZone->OrgTimeZoneID){ ?>
																	<option value="<?=$TimeZone->OrgTimeZoneID?>" selected="selected"><?=$TimeZone->TimeZone?></option>
															<?php } else { ?>
																	<option value="<?=$TimeZone->OrgTimeZoneID?>"><?=$TimeZone->TimeZone?></option>
																<?php }
																} ?>
														</select>
															<?php echo form_error('TimeZone');  ?>
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-3">Start of Day<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control timepicker" name="StartOfDay" id="StartOfDay" value="<?=isset($UserData->StartOfDay)?$UserData->StartOfDay:"";?>">
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-3">End of Day<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control timepicker" name="EndOfDay" id="EndOfDay" value="<?=isset($UserData->EndOfDay)?$UserData->EndOfDay:"";?>">
														</div>
													</div>
												</div>
												</div>

												<div class="col-md-12">
												<div class="txt-dark capitalize-font"><strong>Email Settings</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">
												
													<div class="form-group">
														<label class="control-label col-md-3">Email Sender Address<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="email" class="form-control" name="SenderEmail" id="SenderEmail" value="<?=isset($UserData->SenderEmail)?$UserData->SenderEmail:"";?>">
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-3">Email Sender Name<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="SenderName" id="SenderName" value="<?=isset($UserData->SenderName)?$UserData->SenderName:"";?>">
														</div>
													</div>

													<!-- <div class="form-group">
														<label class="control-label col-md-3">Email Signature<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="EmailSignature" id="EmailSignature" value="<?=isset($UserData->EmailSignature)?$UserData->EmailSignature:"";?>">
														</div>
													</div> -->
												</div>
												</div>

												<div class="col-md-12">
												<div class="txt-dark capitalize-font"><strong>Notifications</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">
												
													<!-- <div class="form-group">
														<label class="control-label col-md-3">Default Group Notification Frequency</label>
														<div class="col-md-9">
															<select name="DefaultGrpNotificationFreq" id="DefaultGrpNotificationFreq" class="form-control">
																<option value="All" <?=($UserData->DefaultGrpNotificationFreq=='All')?"selected=''":"";?>>All</option>
																<option value="DailyDigest" <?=($UserData->DefaultGrpNotificationFreq=='DailyDigest')?"selected=''":"";?>>Daily Digest</option>
															</select>
														</div>
													</div> -->
													<div class="form-group">
														<label class="control-label col-md-3">Receive Admin Emails</label>
														<div class="col-md-9">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="ReceiveAdminEmails" id="ReceiveAdminEmails" value="1" <?=($UserData->ReceiveAdminEmails==1)?"checked=''":"";?>>
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>
												</div>
												</div>

												<div class="col-md-12">
												<div class="txt-dark capitalize-font pt-10"><strong>System Information</strong></div>
												<hr class="light-grey-hr">
												
												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-3 text-left txt-dark">
															Create Date
														</div>
														<div class="col-md-9">
															<?=$UserData->CreatedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-3 text-left txt-dark">
															Created By
														</div>
														<div class="col-md-9">
															<a href="#"><?=$UserData->CreatedBy;?></a>
														</div>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-3 text-left txt-dark">
															Last Modified Date
														</div>
														<div class="col-md-9">
															<?=$UserData->LastModifiedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-3 text-left txt-dark">
															Last Modified By
														</div>
														<div class="col-md-9">
															<a href="#"><?=$UserData->LastModifiedBy;?></a>
														</div>
													</div>
												</div>
											</div>

											</div>
											<!-- /Row -->
										</div>

										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary" name="EditUser">Save</button>
															<button type="submit" class="btn btn-primary" name="EditUser">Save & New</button>
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserList" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->

<?php $this->load->view('Setup/Administration/Modal/LocationPickerView'); ?>
<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">
$(document).ready(function () {
 
    $('#CreateUser').validate({ 
        rules: {
            FirstName: {
                required: true
            },
            LastName: {
                required: true
            },
            Email: {
                required: true
            },
           /* OrganizationID: {
                required: true
            },*/
           /* Department: {
                required: true
            },
            Division: {
                required: true
            },*/
            Manager: {
                required: true
            },
            MobileNo: {
                required: true
            },
            Profile: {
                required: true
            },
            /*StartDate: {
                required: true
            },
            EndDate: {
                required: true
            },*/
            Address: {
                required: true
            },
            Latitude: {
                required: true
            },
            Longitude: {
                required: true
            },
            City: {
                required: true
            },
            State: {
                required: true
            },
            Country: {
                required: true
            },
            PostalCode: {
                required: true
            },
            TimeZone: {
                required: true
            },
            StartOfDay: {
                required: true
            },
            EndOfDay: {
                required: true
            },
            SenderEmail: {
                required: true
            },
            SenderName: {
                required: true
            },
            EmailSignature: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    }); 

    $('.timepicker').datetimepicker({
		format: 'LT',
		useCurrent: false,
		icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
	});
});
</script>
<!-- Set Location -->
<script src="<?=SITE_URL?>dist/js/location-picker.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	set_location_in_map("");
});
</script>




