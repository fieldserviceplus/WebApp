<?php $this->load->view('Template/HeaderView'); ?>
<!-- Bootstrap Dropzone CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css"/>
<div class="container-fluid pt-25">
	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Create Email Template</h6>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Setup/Communication/Actions/CreateEmailTemplate" class="form-horizontal" id="EmailTemplateForm" method="post" enctype="multipart/form-data">
										<div class="form-body">

											<!-- /Row -->
											<div class="row">
												<div class="col-md-12">

													<div class="form-group">
														<label class="control-label col-md-2">Title<span class="required-sign">*</span></label>
														<div class="col-md-7">
															<input type="text" name="Title" class="form-control" value="<?=isset($ObjData->Title)?$ObjData->Title:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-2">Type<span class="required-sign">*</span></label>
														<div class="col-md-7">
															<select name="Type" id="Type" class="form-control">
																<option value="">Select Type</option>
																<option value="Text">Text</option>
																<option value="HTML">HTML</option>
															</select>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-2">Description</label>
														<div class="col-md-7">
															<textarea name="Description" class="form-control"></textarea>
														</div>
													</div>
													
													<div class="form-group">
														<label class="control-label col-md-2">Available For Use</label>
														<div class="col-md-7">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="AvailableForUse" id="AvailableForUse" value="1" checked="">
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-2">Body<span class="required-sign">*</span></label>
														<div class="col-md-7">
															<textarea class="form-control" id="CKEditor1" name="Body"></textarea>
														</div>
													</div>
												</div>
											</div>
											<!-- /Row -->
										</div>
										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary mr-10" name="CreateEmailTemplate" id="CreateEmailTemplate">Send</button>
															<button type="button" class="btn btn-default" onclick="window.history.go(-1); return false;">Cancel</button>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->


</div>

<?php $this->load->view('Template/FooterView')?>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<script type="text/javascript">
	
$(document).ready(function () {

    $('#EmailTemplateForm').validate({ 
        ignore: [],
        debug: false,
        rules: { 
            cktext:{
                required: function() 
                {
                 CKEDITOR.instances.cktext.updateElement();
                }
            },
            Title: {
        		required: true
            },
            Type: {
                required: true
            },
            Body: {
                required: true
            },
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
});
</script>
<!-- For Send Email Madal Start --> 
<!-- CKEditor 4 -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/ckeditor4/ckeditor.js"></script>
<script>
	CKEDITOR.replace( 'CKEditor1' );
</script>




