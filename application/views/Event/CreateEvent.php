<?php $this->load->view('Template/HeaderView')?>

<!-- Bootstrap Datetimepicker CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<div class="container-fluid pt-25">

	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Create Event</h6>
					</div>
					<div class="pull-right">
						<span style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong></span>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Event/Actions/CreateEvent" class="form-horizontal" id="CreateEvent" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

												<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>Event Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-3">Subject<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Subject" id="Subject" value="<?=isset($EventData->Subject)?$EventData->Subject:"";?>">
															<?php echo form_error('Subject');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Assigned To<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="AssignedTo" id="AssignedTo" class="form-control select2">
														<option value="">Select Assigned To</option>
														<?php $Users = MY_Model::GetAllUsers();
															foreach ($Users as $User) { 
																if(isset($EventData->AssignedTo) && $EventData->AssignedTo == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else if($this->session->userdata('UserID') == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else { ?>
																	<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('AssignedTo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Event Start Date / Time<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control showdatetimepicker" name="EventStartDate" id="EventStartDate" value="<?=isset($EventData->EventStartDate)?$EventData->EventStartDate:"";?>">
															<?php echo form_error('EventStartDate');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Event End Date / Time<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control showdatetimepicker" name="EventEndDate" id="EventEndDate" value="<?=isset($EventData->EventEndDate)?$EventData->EventEndDate:"";?>">
															<?php echo form_error('EventEndDate');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Is All Day Event</label>
														<div class="col-md-9">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="IsAllDayEvent" id="IsAllDayEvent" value="1" <?=(isset($EventData->EventEndDate) && $EventData->IsAllDayEvent=1)?"checked=''":"";?>   >
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Description</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Description" id="Description" value="<?=isset($EventData->Description)?$EventData->Description:"";?>">
															<?php echo form_error('Description');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Is Recurring?</label>
														<div class="col-md-8">
															<div class="checkbox checkbox-primary">
																<input id="IsRecurring" name="IsRecurring" type="checkbox" value="1">
																<label for="checkbox2"></label>
																<input id="WORecurrenceID" name="RecurrenceID" class="WORecurrenceID" type="hidden">
															</div>
														</div>
													</div> 
													
												</div>
												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Event Type<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="EventType" id="EventType" class="form-control">
														<option value="">Select Type</option>
														<?php $EventTypes = $this->ActionsModel->GetEventTypes();
															foreach ($EventTypes as $EventType) { 
																if(isset($EventData->EventTypeID) && ($EventData->EventTypeID == $EventType->EventTypeID)){ ?>
																	<option value="<?=$EventType->EventTypeID?>" selected="selected"><?=$EventType->EventTypeName?></option>
																<?php } else { ?>
																	<option value="<?=$EventType->EventTypeID?>"><?=$EventType->EventTypeName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('EventType');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Contact</label>
														<div class="col-md-9">
														<select name="Who" id="Who" class="form-control select2" OnChange="GetContactDetails(this.value);">
														<option value="">Select Contact</option>
														<?php $Contacts = $this->ActionsModel->GetContacts();
															foreach ($Contacts as $Contact) { 
																if((isset($EventData->Who) && $EventData->Who == $Contact->ContactID) || (isset($_GET['Contact']) && $_GET['Contact'] == $Contact->ContactID)){ ?>
																	<option value="<?=$Contact->ContactID?>" selected="selected"><?=$Contact->FirstName.' '.$Contact->LastName?></option>
																<?php } else { ?>
																	<option value="<?=$Contact->ContactID?>"><?=$Contact->FirstName.' '.$Contact->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('Who');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Related To<span class="required-sign">*</span></label>
														<div class="col-md-3">
															<select name="RelatedTo" id="RelatedTo" class="form-control" OnChange="GetRelatedToObjectData(this.value)">
																<option value="">Select Related To</option>
																<option value="Account" <?=(isset($EventData->RelatedTo) && ($EventData->RelatedTo=='Account'))?"selected=''":"";?>>Account</option>
																<option value="WorkOrder" <?=(isset($EventData->RelatedTo) && ($EventData->RelatedTo=='WorkOrder'))?"selected=''":"";?>>WorkOrder</option>
																<option value="Estimate" <?=(isset($EventData->RelatedTo) && ($EventData->RelatedTo=='Estimate'))?"selected=''":"";?>>Estimate</option>
																<option value="Invoice" <?=(isset($EventData->RelatedTo) && ($EventData->RelatedTo=='Invoice'))?"selected=''":"";?>>Invoice</option>
																<option value="Contact" <?=(isset($EventData->RelatedTo) && ($EventData->RelatedTo=='Contact'))?"selected=''":"";?>>Contact</option>

															</select>
															<?php echo form_error('RelatedTo');  ?>
														</div>

														<div class="col-md-6">
															<select name="What" id="What" class="form-control SelectWhat select2">
																<option value="" selected="selected">Select Related To</option>
															</select>
															<?php echo form_error('What');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Email</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Email" id="Email" value="<?=isset($EventData->Email)?$EventData->Email:"";?>">
															<?php echo form_error('Email');  ?>
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-3">Phone #</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="PhoneNo" id="PhoneNo" value="<?=isset($EventData->Phone)?$EventData->Phone:"";?>">
															<?php echo form_error('Phone');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Event Priority<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="EventPriority" id="EventPriority" class="form-control">
														<option value="">Select Event Priority</option>
														<?php $EventPriority = $this->ActionsModel->GetEventPriority();
															foreach ($EventPriority as $TPriority) { 
																if(isset($EventData->EventPriority) && $EventData->EventPriority == $TPriority->EventPriorityID){ ?>
																	<option value="<?=$TPriority->EventPriorityID?>" selected="selected"><?=$TPriority->Priority?></option>
																<?php } else { ?>
																	<option value="<?=$TPriority->EventPriorityID?>"><?=$TPriority->Priority?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('EventPriority');  ?>
														</div>
													</div>

												</div>
												<!--/span-->
												</div>

											<!-- /Row -->
										</div>

										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="CreateEvent">Save</button>
															<a href="<?=SITE_URL;?>Event/View" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->

<?php $this->load->view('Event/Modal/RecurringTaskView'); ?>

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>

<script type="text/javascript">
$(document).ready(function () {
 
    $('#CreateEvent').validate({ 
        rules: {
            Subject: {
                required: true
            },
            RelatedTo: {
                required: true
            },
            What: {
                required: true
            },
            AssignedTo: {
                required: true
            },
            EventStatus: {
                required: true
            },
            EventPriority: {
                required: true
            },
            EventType: {
                required: true
            },
            DueDate: {
                required: true
            },
            EventStartDate: {
                required: true
            },
            EventEndDate: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    }); 
});

function GetRelatedToObjectData(RelatedTo, What = 0){
	$.ajax({
        url: '<?=SITE_URL?>Event/Actions/GetRelatedToObjectOptions/'+RelatedTo+'/'+What,
        method: "post",
        data: { RelatedTo : RelatedTo, What : What},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$(".SelectWhat").html(result);

               	return true;  	
            } else {
                $(".SelectWhat").html('');
                return false;
            }
        }
    });
}

</script>

<!-- Bootstrap Touchspin JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script type="text/javascript">
	$(".vertical-spin").TouchSpin({
		min: 1,
        max: 150,
		verticalbuttons: true,
		verticalupclass: 'ti-plus',
		verticaldownclass: 'ti-minus'
	});
	var vspinTrue = $(".vertical-spin").TouchSpin({
		verticalbuttons: true
	});
	if (vspinTrue) {
		$('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
	}

	$(".vertical-spin-occurrence").TouchSpin({
		min: 1,
		verticalbuttons: true,
		verticalupclass: 'ti-plus',
		verticaldownclass: 'ti-minus'
	});

	/* Recurring Setting */
function RepeatsChange(Repeats){ 
	if(Repeats == 'Daily'){ 
			
		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 150});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('day on');
	} else if(Repeats == 'Weekly'){ 

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 104});
	
		$( "#days-cb" ).show();
		$("input[name*='RepeatOn[]']").attr("required",true);
		$('.interval_every_txt').html('week on');
	} else if(Repeats == 'Monthly'){

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 60});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('month on');
	} else if(Repeats == 'Yearly'){

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 10});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('year on');
	} else if(Repeats == 'Periodically'){

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 150});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('periodical on');
	}
}
$("input[name='Ends']").change(function() { 
	if($("input[name='Ends']:checked").val()=='On') {
       $("#EndsOnDate").attr("required",true);
 	} else {
 		$("#EndsOnDate").attr("required",false);
 	}
}); 

$('#IsRecurring').change(function() { 
     if(this.checked) {
        $('#RecurringModal').modal({
		    backdrop: 'static',
		    keyboard: false  // to prevent closing with Esc button (if you want this too)
		}); 
         
     } else {

     	if($('.WORecurrenceID').val() != ''){

     		var WORecurrenceID = $('.WORecurrenceID').val();
	     	$.ajax({
	            url: '<?=SITE_URL?>Event/Actions/removeRecurrenceSetting/',
	            data: { WORecurrenceID:WORecurrenceID },
	            method: "post",
	            dataType: 'html',
	            success: function (result) {
	                if (result != '') {
	                   $('.WORecurrenceID').val(''); 
	                   return true;  	
	                } else {
	                    alert("Something went wrong.Please try again!");
	                    return false;
	                }
	            }
	        });	
	     }
     }
});

$('#RecurringForm').validate({
	errorPlacement: function(){
	    return false;
	},
	/* submitHandler: function (form) { 
	    return false; 
	},*/
	highlight: function(element) {
		$(element).closest('.form-group').addClass('has-error');
	},
	unhighlight: function(element) {
	    $(element).closest('.form-group').removeClass('has-error');
	}
});	

$(document).on('click', "#RecurrenceSave", function () {
    if ($("#RecurringForm").valid()) {	
        $.ajax({
            url: '<?=SITE_URL?>Event/Actions/saveRecurrenceSetting/',
            data: $("#RecurringForm").serialize(),
            method: "post",
            dataType: 'html',
            success: function (result) {
                if (result != '') {
                   	$('.WORecurrenceID').val(result); 
                   
                   	$('#RecurringModal').modal('hide'); 
                   	return true;  	
                } else {
                    alert("Something went wrong.Please try again!");
                    return false;
                }
            }
        });
    }
    return false;
});

$(document).on('click', "#RecurrenceClose", function () { 
   	$('#IsRecurring').prop('checked', false);
});

$("#StartOn").datepicker({
        todayBtn:  1,
        autoclose: true,
}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#EndsOnDate').datepicker('setStartDate', minDate);
});
$('#EndsOnDate').datepicker({
    todayBtn:  1,
    autoclose: true,
    startDate: new Date()
});

$('.showdatetimepicker').datetimepicker({
		useCurrent: true,
		icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
	});

$('.timepicker').datetimepicker({
		format: 'LT',
		useCurrent: false,
		icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
}).data("DateTimePicker").date(moment());

function GetContactDetails(ContactID){
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/GetContactDetails/'+ContactID,
        method: "post",
      /*  data: { AccountID : AccountID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$("#PhoneNo").val(result.PhoneNo);
            	$("#Email").val(result.Email);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}
RelatedTo
$(document).ready(function () {
<?php if(isset($_GET['Account']) && !empty($_GET['Account'])){ ?>
		$("#RelatedTo").val('Account');
		GetRelatedToObjectData('Account', <?=$_GET['Account'];?>);
<?php } else if(isset($_GET['Contact']) && !empty($_GET['Contact'])){ ?>
		$("#RelatedTo").val('Contact');
		GetContactDetails(<?=$_GET['Contact']?>);
		GetRelatedToObjectData('Contact', <?=$_GET['Contact'];?>);
<?php } else if(isset($_GET['WorkOrder']) && !empty($_GET['WorkOrder'])){ ?>
		$("#RelatedTo").val('WorkOrder');
		GetRelatedToObjectData('WorkOrder', <?=$_GET['WorkOrder'];?>);
<?php } else if(isset($_GET['Estimate']) && !empty($_GET['Estimate'])){ ?>
		$("#RelatedTo").val('Estimate');
		GetRelatedToObjectData('Estimate', <?=$_GET['Estimate'];?>);
<?php } else if(isset($_GET['Invoice']) && !empty($_GET['Invoice'])){ ?>
		$("#RelatedTo").val('Invoice');
		GetRelatedToObjectData('Invoice', <?=$_GET['Invoice'];?>);
<?php }?>
 });
</script>




