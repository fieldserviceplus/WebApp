<?php $this->load->view('Template/HeaderView'); ?>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">


<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark"><?=$EventData->Subject;?></h6>
					</div>
					<div class="clearfix"></div>
				</div> -->
				<div class="panel-heading pt-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['Event']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['Event']['title']?></span>
									<span class="panel-title txt-dark"><?=$EventData->Subject;?></span>
								</div>
							</div>
						</div>
					</ul>
					<div class="btn-group pull-right">
						<div class="dropdown">
							<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
							<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
								<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
									<a><span><strong>Event Actions</strong></span></a>
								</li>
								<li>
									<a href="#" onclick="DeleteEvent();"><span>Delete Event</span></a>
								</li>
							</ul>
						</div>
					</div>
					<div class="btn-group pull-right">
						<a href="<?=SITE_URL?>Event/Actions/EditEvent/<?=$EventData->EventID;?>" class="btn btn-primary btn-outline">Edit</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
			 	<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Event Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>  -->
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Event/Actions/EditEvent/<?=$EventData->EventID?>" class="form-horizontal" id="EditEvent" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>Event Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Subject
														</div>
														<div class="col-md-8">
															<?=$EventData->Subject; ?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Assigned To
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$EventData->AssignedTo?>"><?=$EventData->AssignedToName;?></a>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
														 Event Start Date / Time
														</div>
														<div class="col-md-8">
															<?=$EventData->EventStartDate;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
														 Event End Date / Time
														</div>
														<div class="col-md-8">
															<?=$EventData->EventEndDate;?>
														</div>
													</div>
												
													<div class="form-group pt-5">
														<div class="col-md-4 text-left txt-dark">
															Is All Day Event
														</div>
														<div class="col-md-8">
															<div class="checkbox checkbox-primary pt-0 pb-0">
																<input id="checkbox12" disabled="" <?=$EventData->IsAllDayEvent==1?'checked':'';?> type="checkbox">
																<label for="checkbox12"></label>
															</div>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Description
														</div>
														<div class="col-md-8">
															<?=$EventData->Description;?>
														</div>
													</div>
													
												</div>

												<div class="col-md-6">


													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Event Type
														</div>
														<div class="col-md-8">
															<?=$EventData->EventTypeName;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Contact
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Contact/Actions/ContactDetails/<?=$EventData->Who;?>"><?=$EventData->ContactName;?></a>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															RelatedTo
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?><?=$EventData->RelatedTo;?>/Actions/<?=$EventData->RelatedTo;?>Details/<?=$EventData->What;?>"><?=$EventData->RelatedObjNo;?></a>
														</div>
													</div>
													
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Email
														</div>
														<div class="col-md-8">
															<?=$EventData->Email;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Phone #
														</div>
														<div class="col-md-8">
															<?=$EventData->PhoneNo;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Event Priority
														</div>
														<div class="col-md-8">
															<?=$EventData->Priority;?>
														</div>
													</div>

												</div>
											</div>

											<div class="col-md-12">
												<div class="txt-dark capitalize-font pt-10"><strong>System Information</strong></div>
												<hr class="light-grey-hr">
												
												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Create Date
														</div>
														<div class="col-md-8">
															<?=$EventData->CreatedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Created By
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$EventData->CreatedBy?>"><?=$EventData->CreatedByName;?></a>
														</div>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified Date
														</div>
														<div class="col-md-8">
															<?=$EventData->LastModifiedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified By
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$EventData->LastModifiedBy?>"><?=$EventData->LastModifiedByName;?></a>
														</div>
													</div>
												</div>
											</div>


										</div>
											<!-- /Row -->
									</div>

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		
			</div>
			<div id="profile_13" class="tab-pane fade" role="tabpanel">

				<?php //$this->load->view('Event/Include/RelatedInformation'); ?>
				
			</div>
		</div>
		
		</div>
 
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">

			<?php //$this->load->view('Event/Include/ActivityTimeline'); ?>
			
		</div>

	</div>
	<!-- /Row -->
</div>

<?php //$this->load->view('Event/Modal/CreateNoteView'); ?>

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">
function DeleteEvent(EventID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#5fc55f",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>Event/Actions/DeleteEvent/<?=$EventData->EventID;?>/<?=$EventData->RelatedTo?>/<?=$EventData->What?>";
		}, 500);
    });
	return false;
}
</script>


