<?php $this->load->view('Template/HeaderView'); ?>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['Event']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['Event']['title']?></span>
									<span class="panel-title txt-dark"><?=$EventData->Subject;?></span>
								</div>
							</div>
						</div>
					</ul>
					<div class="btn-group pull-right">
						<div class="dropdown">
							<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
							<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
								<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
									<a><span><strong>Event Actions</strong></span></a>
								</li>
								<li>
									<a href="#" onclick="DeleteEvent();"><span>Delete Event</span></a>
								</li>
							</ul>
						</div>
					</div>
					<div class="btn-group pull-right">
						<a href="<?=SITE_URL;?>Event/Actions/EventDetails/<?=$EventData->EventID?>" class="btn btn-default">Cancel</a>
						<a href="#" class="btn btn-primary" id="EditEventLink">Save</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
			 	<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Event Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>  -->
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Event/Actions/EditEvent/<?=$EventData->EventID?>" class="form-horizontal" id="EditEvent" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font">
													<strong>Event Information</strong>
													<span class="pull-right" style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong>
													</span>
												</div>
												
												<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-3">Subject<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Subject" id="Subject" value="<?=isset($EventData->Subject)?$EventData->Subject:"";?>">
															<?php echo form_error('Subject');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Assigned To<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="AssignedTo" id="AssignedTo" class="form-control select2">
														<option value="">Select Assigned To</option>
														<?php $Users = MY_Model::GetAllUsers();
															foreach ($Users as $User) { 
																if(isset($EventData->AssignedTo) && $EventData->AssignedTo == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else if($this->session->userdata('UserID') == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else { ?>
																	<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('AssignedTo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Event Start Date / Time<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control showdatetimepicker" name="EventStartDate" id="EventStartDate" value="<?=isset($EventData->EventStartDate)?$EventData->EventStartDate:"";?>">
															<?php echo form_error('EventStartDate');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Event End Date / Time<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control showdatetimepicker" name="EventEndDate" id="EventEndDate" value="<?=isset($EventData->EventEndDate)?$EventData->EventEndDate:"";?>">
															<?php echo form_error('EventEndDate');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Is All Day Event</label>
														<div class="col-md-9">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="IsAllDayEvent" id="IsAllDayEvent" value="1" <?=$EventData->IsAllDayEvent=1?"checked=''":"";?>   >
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Description</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Description" id="Description" value="<?=isset($EventData->Description)?$EventData->Description:"";?>">
															<?php echo form_error('Description');  ?>
														</div>
													</div>
													
												</div>
												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Event Type<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="EventType" id="EventType" class="form-control">
														<option value="">Select Type</option>
														<?php $EventTypes = $this->ActionsModel->GetEventTypes();
															foreach ($EventTypes as $EventType) { 
																if(isset($EventData->EventTypeID) && ($EventData->EventTypeID == $EventType->EventTypeID)){ ?>
																	<option value="<?=$EventType->EventTypeID?>" selected="selected"><?=$EventType->EventTypeName?></option>
																<?php } else { ?>
																	<option value="<?=$EventType->EventTypeID?>"><?=$EventType->EventTypeName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('EventType');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Contact</label>
														<div class="col-md-9">
														<select name="Who" id="Who" class="form-control select2" OnChange="GetContactDetails(this.value);">
														<option value="">Select Contact</option>
														<?php $Contacts = $this->ActionsModel->GetContacts();
															foreach ($Contacts as $Contact) { 
																if(isset($EventData->Who) && $EventData->Who == $Contact->ContactID){ ?>
																	<option value="<?=$Contact->ContactID?>" selected="selected"><?=$Contact->FirstName.' '.$Contact->LastName?></option>
																<?php } else { ?>
																	<option value="<?=$Contact->ContactID?>"><?=$Contact->FirstName.' '.$Contact->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('Who');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Related To<span class="required-sign">*</span></label>
														<div class="col-md-3">
															<select name="RelatedTo" id="RelatedTo" class="form-control" OnChange="GetRelatedToObjectData(this.value)">
																<option value="">Select Related To</option>
																<option value="Account" <?=($EventData->RelatedTo=='Account')?"selected=''":"";?>>Account</option>
																<option value="WorkOrder" <?=($EventData->RelatedTo=='WorkOrder')?"selected=''":"";?>>WorkOrder</option>
																<option value="Estimate" <?=($EventData->RelatedTo=='Estimate')?"selected=''":"";?>>Estimate</option>
																<option value="Invoice" <?=($EventData->RelatedTo=='Invoice')?"selected=''":"";?>>Invoice</option>
																<option value="Contact" <?=($EventData->RelatedTo=='Contact')?"selected=''":"";?>>Contact</option>
															</select>
															<?php echo form_error('RelatedTo');  ?>
														</div>

														<div class="col-md-6">
															<select name="What" id="What" class="form-control SelectWhat select2">
																<option value="" selected="selected">Select Related To</option>
															</select>
															<?php echo form_error('What');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Email</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Email" id="Email" value="<?=isset($EventData->Email)?$EventData->Email:"";?>">
															<?php echo form_error('Email');  ?>
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-3">Phone #</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="PhoneNo" id="PhoneNo" value="<?=isset($EventData->Phone)?$EventData->Phone:"";?>">
															<?php echo form_error('Phone');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Event Priority<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="EventPriority" id="EventPriority" class="form-control">
														<option value="">Select Event Priority</option>
														<?php $EventPriority = $this->ActionsModel->GetEventPriority();
															foreach ($EventPriority as $TPriority) { 
																if(isset($EventData->EventPriority) && $EventData->EventPriority == $TPriority->EventPriorityID){ ?>
																	<option value="<?=$TPriority->EventPriorityID?>" selected="selected"><?=$TPriority->Priority?></option>
																<?php } else { ?>
																	<option value="<?=$TPriority->EventPriorityID?>"><?=$TPriority->Priority?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('EventPriority');  ?>
														</div>
													</div>

												</div>
											</div>

											<div class="col-md-12">

												<div class="txt-dark capitalize-font pt-10"><strong>System Information</strong></div>
												<hr class="light-grey-hr">
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-3">Create Date</label>
														<div class="col-md-9">
															<input type="text" class="form-control" value="<?=$EventData->CreatedDate?>" readonly="">
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-3">Created By</label>
														<div class="col-md-9">
															<input type="text" class="form-control" value="<?=$EventData->CreatedByName?>" readonly="">
														</div>
													</div>
												</div>
												<div class="col-md-6">

												   <div class="form-group">
														<label class="control-label col-md-3">Last Modified Date</label>
														<div class="col-md-9">
															<input type="text" class="form-control" value="<?=$EventData->LastModifiedDate?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Last Modified By</label>
														<div class="col-md-9">
															<input type="text" class="form-control" value="<?=$EventData->LastModifiedByName?>" readonly="">
														</div>
													</div>
												</div>
											</div>


										</div>
											<!-- /Row -->
									</div>

									<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="EditEvent" id="EditEventBtn">Save</button>
															<a href="<?=SITE_URL;?>Event/Actions/EventDetails/<?=$EventData->EventID?>" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			</div>
			<!-- <div id="profile_13" class="tab-pane fade" role="tabpanel">
				
			</div> -->
		</div>
		
		</div>
		<!-- <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
		</div> -->
	</div>
	<!-- /Row -->
</div>

<!-- End Modal -->

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<!-- <script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>
 -->
<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>
		
<script type="text/javascript">

$(document).ready(function () {

	$('.showdatetimepicker').datetimepicker({
		useCurrent: true,
		icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
	});

	$(document).on('click', "#EditEventLink", function () {
		$('#EditEventBtn').click(); 
	});

    $('#EditEvent').validate({ 
        rules: {
            Subject: {
                required: true
            },
            RelatedTo: {
                required: true
            },
            What: {
                required: true
            },
            AssignedTo: {
                required: true
            },
            EventPriority: {
                required: true
            },
            EventType: {
                required: true
            },
            EventStartDate: {
                required: true
            },
            EventEndDate: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
}); 

function DeleteEvent(EventID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#5fc55f",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>Event/Actions/DeleteEvent/<?=$EventData->EventID;?>/<?=$EventData->RelatedTo?>/<?=$EventData->What?>";
		}, 500);
    });
	return false;
}

function GetRelatedToObjectData(RelatedTo, What = 0){
	$.ajax({
        url: '<?=SITE_URL?>Event/Actions/GetRelatedToObjectOptions/'+RelatedTo+'/'+What,
        method: "post",
        data: { RelatedTo : RelatedTo, What : What},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$(".SelectWhat").html(result);

               	return true;  	
            } else {
                $(".SelectWhat").html('');
                return false;
            }
        }
    });
}
$(document).ready(function () {
	GetRelatedToObjectData('<?=$EventData->RelatedTo?>', '<?=$EventData->What?>');
});

function GetContactDetails(ContactID){
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/GetContactDetails/'+ContactID,
        method: "post",
      /*  data: { AccountID : AccountID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$("#PhoneNo").val(result.PhoneNo);
            	$("#Email").val(result.Email);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}
</script>




