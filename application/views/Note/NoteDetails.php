<?php $this->load->view('Template/HeaderView'); ?>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">


<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark"><?=$NoteData->Subject;?></h6>
					</div>
					<div class="clearfix"></div>
				</div> -->
				<div class="panel-heading pt-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['Note']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['Note']['title']?></span>
									<span class="panel-title txt-dark"><?=$NoteData->Subject;?></span>
								</div>
							</div>
						</div>
					</ul>
					<div class="btn-group pull-right">
						<div class="dropdown">
							<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
							<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
								<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
									<a><span><strong>Note Actions</strong></span></a>
								</li>
								<li>
									<a href="#" onclick="DeleteNote();"><span>Delete Note</span></a>
								</li>
							</ul>
						</div>
					</div>
					<div class="btn-group pull-right">
						<a href="<?=SITE_URL?>Note/Actions/EditNote/<?=$NoteData->NoteID;?>" class="btn btn-primary btn-outline">Edit</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Note/Actions/EditNote/<?=$NoteData->NoteID?>" class="form-horizontal" id="EditNote" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>Note Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Owner
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$NoteData->Owner?>"><?=$NoteData->OwnerName;?></a>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Related To
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?><?=$NoteData->RelatedTo;?>/Actions/<?=$NoteData->RelatedTo;?>Details/<?=$NoteData->What;?>"><?=$NoteData->RelatedObjNo;?></a>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Subject
														</div>
														<div class="col-md-8">
															<?=$NoteData->Subject; ?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Body
														</div>
														<div class="col-md-8">
															<?=$NoteData->Body;?>
														</div>
													</div>
												</div>
											</div>

											<div class="col-md-12">
												<div class="txt-dark capitalize-font pt-10"><strong>System Information</strong></div>
												<hr class="light-grey-hr">
												
												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Create Date
														</div>
														<div class="col-md-8">
															<?=$NoteData->CreatedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Created By
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$NoteData->CreatedBy?>"><?=$NoteData->CreatedByName;?></a>
														</div>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified Date
														</div>
														<div class="col-md-8">
															<?=$NoteData->LastModifiedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified By
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$NoteData->LastModifiedBy?>"><?=$NoteData->LastModifiedByName;?></a>
														</div>
													</div>
												</div>
											</div>

										</div>
											<!-- /Row -->
									</div>

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		
			</div>
			<!-- <div id="profile_13" class="tab-pane fade" role="tabpanel">
				
			</div> -->
		</div>
		
		</div>
 
		<!-- <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
			
		</div> -->

	</div>
	<!-- /Row -->
</div>

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>


<script type="text/javascript">
function DeleteNote(NoteID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#5fc55f",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>Note/Actions/DeleteNote/<?=$NoteData->NoteID;?>/<?=$NoteData->RelatedTo;?>/<?=$NoteData->What;?>";
		}, 500);
    });
	return false;
}
</script>


