
<?php $this->load->view('Template/HeaderView')?>
 
<style type="text/css">.dataTables_filter { display: none; }</style>
    <div class="container-fluid pt-25">

		<!-- Row -->
        <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-default card-view">
					<div  class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 pr-0 pl-0">
								<form method="post" action="<?=SITE_URL?>Account/View/" id="SelectedAcForm">
								
										<div class="col-lg-2 col-md-3 col-sm-3 col-xs-3">
											<label class="control-label mt-10"><strong>View:</strong></label>
										</div>
										<div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 pr-0">
											<select name="SelectedAccount" class="selectpicker" data-style="form-control btn-default btn-outline" onchange="GoToNewView()"> <!-- onchange="ViewAccounts(this.value)" -->
												<option disabled="" selected="">Select Views</option>
												<optgroup label="Default Views">
													<option value="MyAccounts">My Accounts</option>
												<option value="AllAccounts">All Accounts</option>
												<option value="NewAccountsThisWeek">New Accounts This Week</option>
												</optgroup>
												<optgroup label="My Custom Views">
													<?php $Views = $this->ActionsModel->GetViews();
													foreach ($Views as $View) { ?>
														<option value="<?=$View->AccountViewID?>"><?=$View->AccountViewName?></option>
												<?php } ?>
												</optgroup>
											</select>
										</div>	
								
								</form>
							</div>	
							<div class="col-lg-4 col-md-3 col-sm-5 col-xs-12">
							<div class="form-group">
								<!-- <div class="btn-group">
									<a href="#" class="btn btn-primary btn-outline" onclick="GoToNewView()">Go</a>
								</div> -->
								<div class="btn-group">
									<div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button"> <i class="fa fa-pencil"></i> <span class="caret"></span></button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li>
												<a><span><strong>LIST VIEW ACTIONS</strong></span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>Account/View/CreateNewView"><span>Create New View</span></a>
											</li>
										</ul>
									</div>
								</div>
								</div>
							</div>
							<div class="col-lg-5 col-md-5 col-sm-3 col-xs-12">
								<div class="pull-right">
									<a href="<?=SITE_URL?>Account/Actions/CreateAccount" class="btn btn-primary btn-outline">New Account</a>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-heading">
						<div class="pull-left col-lg-10 col-sm-8 col-md-8">
							<h6 class="panel-title txt-dark">Recent Accounts</h6>
						</div>
						<div class="pull-right col-lg-2 col-sm-4 col-md-4">
							<div class="input-group">
								<input id="accountglobalfilter" class="form-control" placeholder="Search" type="text">
								<span class="input-group-addon"><i class="zmdi zmdi-search"></i></span> 
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">

								<div class="table-responsive">
									<table id="accounttable" class="table table-hover display  pb-30" width="100%">
										<thead>
											<tr>
												<th class="text-center">#</th>
												<th class="text-center">Account Name</th>
												<th class="text-center">Phone No</th>
												<th class="text-center">Type</th>
												<th class="text-center">City</th>
												<th class="text-center">State</th>
												<th class="text-center">Assigned To </th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->


		<!-- <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="panel panel-default card-view">
						<div class="panel-heading">
							<div class="pull-left">
								<h6 class="panel-title txt-dark">Account Tools</h6>
							</div>
							<div class="clearfix"></div>
						</div>
						<div  class="panel-wrapper collapse in">
							<div  class="panel-body">
								<ul class="list-icons">
									<li class="mb-10"><i class="fa fa-check text-danger mr-5"></i> <a href="#"><u>Merge Accounts</u></a></li>
									<li class="mb-10"><i class="fa fa-check text-danger mr-5"></i> <a href="#"><u>Mass Import Accounts</u></a></li>
									<li class="mb-10"><i class="fa fa-check text-danger mr-5"></i> <a href="#"><u>Export Accounts</u></a></li>
									<li class="mb-10"><i class="fa fa-check text-danger mr-5"></i> <a href="#"><u>Mass Delete Accounts</u></a></li>
									<li class="mb-10"><i class="fa fa-check text-danger mr-5"></i> <a href="#"><u>Transfer Accounts</u></a></li>
								</ul>
							</div>
						</div>
				</div>
			</div>

		</div> -->
	</div>	

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<script type="text/javascript">
 
 	function ViewAccounts(flag){

		var dataTable = $('#accounttable').DataTable({
	       	"responsive": true,
	       	"paging": true,
	       	"bDestroy": true,
	       	"processing": true, //Feature control the processing indicator.
	        "serverSide": true, //Feature control DataTables' server-side processing mode.
	        "sAjaxSource": "<?=SITE_URL?>Account/Actions/RecentAccounts?flag="+flag,
	       	"columnDefs": [ {
		        sortable: false,
		        targets: 'no-sort',
		        class: "index",
		        targets: 0
	       	} ],
	       	"aaSorting": [],
	       	/*"order": [[ 1, 'asc' ]],*/
	       	"fixedColumns": true, 
	       	"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 ] }],
	       	"columns": [
	       		{ className: "text-center" },
	            { className: "text-left" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	         ], 
	         "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {  
	                var oSettings = $("#accounttable").dataTable().fnSettings(); 
	                $("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
	                //$(nRow).attr("id",'row_' + aData.id); 
	                //return nRow;
	          },
	          "dom": 'rt<"bottom"ilp><"clear">' 
	    });
    }

    $(document).ready(function(){

    	ViewAccounts('MyAccounts');
	    $('#accountglobalfilter').keyup(function(){
	    	var dataTable = $('#accounttable').DataTable();
	      	dataTable.search($(this).val()).draw() ;
	    }); 
 	});

 	function GoToNewView()
 	{
 		$('#SelectedAcForm').submit();
 	}
</script>


