<?php $this->load->view('Template/HeaderView'); ?>


<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10 pb-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['Account']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['Account']['title']?></span>
									<span class="panel-title txt-dark"><?=$AccountData->AccountName;?></span>
								</div>
							</div>
						</div>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div  class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="col-lg-7 col-md-5 col-sm-4 col-xs-12">
							<!-- <div class="form-group"> -->
								<ul role="tablist" class="nav nav-pills" id="myTabs_13">
									<li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_13" href="#home_13">Detail</a></li>
									<li role="presentation" class=""><a data-toggle="tab" id="profile_tab_13" role="tab" href="#profile_13" aria-expanded="false">Related</a></li>
								</ul>
							<!-- </div> -->
						</div>

						<div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">
							<!-- <ul role="tablist" class="nav nav-pills pull-right" id="myTabs_6">
								<li><a class="btn-outline btn btn-primary" href="<?=SITE_URL?>Account/Actions/EditAccount/<?=$AccountData->AccountID;?>">Edit</a></li>
							<li class="custom_li_btn"><a data-toggle="tab" href="#">Copy</a></li> 
								<li>
									<select class="selectpicker" data-style="form-control btn-default btn-outline">
										<option disabled="" selected="">Actions</option>
										<optgroup label="Add New Actions">
											<option>New Work Order</option>
											<option>New Estimate</option>
											<option>New Contact</option>
											<option>New Location</option>
											<option>New Invoice</option>
											<option>New File</option>
										</optgroup>
										<optgroup label="Account Actions">
											<option OnClick="CopyAccount()">Copy Account</option>
											<option>Delete Account</option>
										</optgroup>
									</select>

								</li>
							</ul> -->
							
							<div class="btn-group pull-right">
									<div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li>
												<a><span><strong>Add New Actions</strong></span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>Contact/Actions/CreateContact/?Account=<?=$AccountData->AccountID?>"><span>New Contact</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>WorkOrder/Actions/CreateWorkOrder/?Account=<?=$AccountData->AccountID?>" ><span>New Work Order</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>Estimate/Actions/CreateEstimate?Account=<?=$AccountData->AccountID?>"><span>New Estimate</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>Invoice/Actions/CreateInvoice?Account=<?=$AccountData->AccountID?>"><span>New Invoice</span></a>
											</li>
											<li> 
												<a href="#" data-toggle="modal" data-target="#CreateNewFile"><span>New File</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>Task/Actions/CreateTask/?Account=<?=$AccountData->AccountID;?>"><span>New Task</span></a> <!--  data-toggle="modal" data-target="#CreateNewTask" -->
											</li>
											<li>
												<a href="<?=SITE_URL?>Event/Actions/CreateEvent/?Account=<?=$AccountData->AccountID;?>"><span>New Event</span></a> <!-- data-toggle="modal" data-target="#CreateNewEvent" -->
											</li>
											<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
												<a><span><strong>Account Actions</strong></span></a>
											</li>
											<li> 
												<a href="<?=SITE_URL;?>Account/Actions/CreateAccount/<?=$AccountData->AccountID?>/"><span>Copy Account</span></a>
											</li>
											<li>
												<a href="#" onclick="DeleteAccount();"><span>Delete Account</span></a>
											</li>
										</ul>
									</div>
								</div>
								<div class="btn-group pull-right">
									<a href="<?=SITE_URL?>Account/Actions/EditAccount/<?=$AccountData->AccountID;?>" class="btn btn-primary btn-outline">Edit</a>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
			 	<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Account Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>  -->
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Account/Actions/EditAccount/<?=$AccountData->AccountID?>" class="form-horizontal" id="EditAccount" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>Account Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Assigned To
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$AccountData->AssignedTo?>"><?=$AccountData->AssignedToName; ?></a>
														</div>
													</div>
												
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Account Name
														</div>
														<div class="col-md-8">
															<?=$AccountData->AccountName;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Primary Contact
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Contact/Actions/ContactDetails/<?=$AccountData->PrimaryContact?>"><?=$AccountData->PrimaryContactName;?></a>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Phone #
														</div>
														<div class="col-md-8">
															<?=$AccountData->PhoneNo;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Preferred Technician
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$AccountData->PreferredTechnician?>"><?=$AccountData->PreferredTechnicianName;?></a>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Access Notes
														</div>
														<div class="col-md-8">
															<?=$AccountData->AccessNotes;?>
														</div>
													</div>
													
												</div>

												<div class="col-md-6">

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Account No
														</div>
														<div class="col-md-8">
															<?=$AccountData->AccountNo;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Account Type
														</div>
														<div class="col-md-8">
															<?=$AccountData->AccountTypeName;?>
														</div>
													</div>

													<div class="form-group pt-5">
														<div class="col-md-4 text-left txt-dark">
															Is Active
														</div>
														<div class="col-md-8">
															<div class="checkbox checkbox-primary pt-0 pb-0">
																<input id="checkbox12" disabled="" <?=$AccountData->IsActive==1?'checked':'';?> type="checkbox">
																<label for="checkbox12"></label>
															</div>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Activity
														</div>
														<div class="col-md-8">
															<?=$AccountData->LastActivityDate;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Service Date
														</div>
														<div class="col-md-8">
															<?=$AccountData->LastServiceDate;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Pop Up Reminder
														</div>
														<div class="col-md-8">
															<?=$AccountData->PopUpReminder;?>
														</div>
													</div>

												</div>
													<div class="col-md-12">
														<div class="form-group pt-5 pb-5">
															<div class="col-md-2 text-left txt-dark">
																Notes
															</div>
															<div class="col-md-10" style="word-wrap: break-word;">
																<?=$AccountData->Notes;?>
															</div>
														</div>
													</div>
											</div>

											<div class="col-md-12">
											<div class="txt-dark capitalize-font pt-10"><strong>Address Information</strong></div>
											<hr class="light-grey-hr">
												
												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Billing Address
														</div>
														<div class="col-md-8">
															<div><?=$AccountData->BillingAddress;?></div>
															<div><?=$AccountData->BillingCity;?>, <?=$AccountData->BillingState;?> <?=$AccountData->BillingPostalCode;?></div>
															<div><?=$AccountData->BillingCountry;?></div>
														</div>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Shipping Address
														</div>
														<div class="col-md-8">
															<div><?=$AccountData->ShippingAddress;?></div>
															<div><?=$AccountData->ShippingCity;?>, <?=$AccountData->ShippingState;?> <?=$AccountData->ShippingPostalCode;?></div>
															<div><?=$AccountData->ShippingCountry;?></div>
														</div>
													</div>
												</div>
											</div>

											<!-- Start Custom Fields Row -->
											<?php $this->load->view('Setup/Customize/Fields/CustomFieldsView', array('Object' => 'Account', 'ObjectID' => $AccountData->AccountID)); ?>
											<!-- End Custom Fields Row -->

											<div class="col-md-12">

												<div class="txt-dark capitalize-font pt-10"><strong>System Information</strong></div>
												<hr class="light-grey-hr">
												
												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Create Date
														</div>
														<div class="col-md-8">
															<?=$AccountData->CreatedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Created By
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$AccountData->CreatedBy?>"><?=$AccountData->CreatedByName;?></a>
														</div>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified Date
														</div>
														<div class="col-md-8">
															<?=$AccountData->LastModifiedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified By
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$AccountData->LastModifiedBy?>"><?=$AccountData->LastModifiedByName;?></a>
														</div>
													</div>
												</div>
											</div>


										</div>
											<!-- /Row -->
									</div>

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		
			</div>
			<div id="profile_13" class="tab-pane fade" role="tabpanel">

				<?php $this->load->view('Account/Include/RelatedInformation'); ?>
				
			</div>
		</div>
		
		</div>
 
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">

			<?php $this->load->view('Account/Include/ActivityTimeline'); ?>
			
		</div>

	</div>
	<!-- /Row -->

</div>

<!-- Start Modal -->
<?php $this->load->view('Account/Modal/CreateTaskView'); ?>

<?php $this->load->view('Account/Modal/CreateEventView'); ?>

<?php $this->load->view('Account/Modal/RecurringTaskView'); ?>

<?php $this->load->view('Common/Modal/CreateNewFile', array('data' => array('RelatedTo' => 'Account', 'What' => $AccountData->AccountID, 'AssignedTo' => $AccountData->AssignedTo))); ?>

<!-- modal -->
 <div class="modal fade" id="location-picker-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Select Location</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
				<div id="Billingpopupmap" style="height: 400px;"></div><br />
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-2">Location</label>
								<div class="col-md-10">
									<input id="BillingLocation" type="text" name="" class="form-control" readonly="">
								</div>
							</div>
						</div>
					</div>
				</div>
				</form>
				
			</div>
		</div>
	</div>
</div> 

 <div class="modal fade" id="location-picker-modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Select Location</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
				<div id="Shippingpopupmap" style="height: 400px;"></div><br />
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-2">Location</label>
								<div class="col-md-10">
									<input id="ShippingLocation" type="text" name="" class="form-control" readonly="">
								</div>
							</div>
						</div>
					</div>
				</div>
				</form>
				
			</div>
		</div>
	</div>
</div> 

<!-- End Modal -->

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<!-- Use for common object  -->
<script src="<?=SITE_URL?>dist/js/common.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Bootstrap Touchspin JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script type="text/javascript">
	$(".vertical-spin").TouchSpin({
		min: 1,
        max: 150,
		verticalbuttons: true,
		verticalupclass: 'ti-plus',
		verticaldownclass: 'ti-minus'
	});

	$(".vertical-spin-occurrence").TouchSpin({
		min: 1,
		verticalbuttons: true,
		verticalupclass: 'ti-plus',
		verticaldownclass: 'ti-minus'
	});
	var vspinTrue = $(".vertical-spin").TouchSpin({
		verticalbuttons: true
	});
	if (vspinTrue) {
		$('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
	}
</script>
<!-- Form Advance Init JavaScript -->

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>

<script type="text/javascript">

$(document).ready(function () {

    $('.showdatetimepicker').datetimepicker({
		useCurrent: true,
		icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
	});

    $('.timepicker').datetimepicker({
			format: 'LT',
			useCurrent: false,
			icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
	}).data("DateTimePicker").date(moment());

     $('#NewTaskForm').validate({ 
        rules: {
            AssignedTo: {
                required: true
            },
            Subject: {
                required: true
            },
            TaskType: {
                required: true
            },
            TaskStatus: {
                required: true
            },
            TaskPriority: {
                required: true
            },
            DueDate: {
                required: true
            },
            RelatedTo: {
                required: true
            },
            What: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    $('#NewEventForm').validate({ 
        rules: {
            AssignedTo: {
                required: true
            },
            Subject: {
                required: true
            },
            EventType: {
                required: true
            },
            EventPriority: {
                required: true
            },
            EventStartDate: {
                required: true
            },
            EventEndDate: {
                required: true
            },
            RelatedTo: {
                required: true
            },
            What: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

}); 

/* Recurring Setting */
function RepeatsChange(Repeats){
	if(Repeats == 'Daily'){ 
			
		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 150});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('day on');
	} else if(Repeats == 'Weekly'){ 

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 104});
	
		$( "#days-cb" ).show();
		$("input[name*='RepeatOn[]']").attr("required",true);
		$('.interval_every_txt').html('week on');
	} else if(Repeats == 'Monthly'){

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 60});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('month on');
	} else if(Repeats == 'Yearly'){

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 10});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('year on');
	} else if(Repeats == 'Periodically'){

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 150});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('periodical on');
	}
}
$("input[name='Ends']").change(function() { 
	if($("input[name='Ends']:checked").val()=='On') {
       $("#EndsOnDate").attr("required",true);
 	} else {
 		$("#EndsOnDate").attr("required",false);
 	}
}); 

$('#IsRecurringTask').change(function() { 
     if(this.checked) {
        $('#RecurringTaskModal').modal({
		    backdrop: 'static',
		    keyboard: false  // to prevent closing with Esc button (if you want this too)
		}); 
         $("#RecurringFormType").remove();
         $('#RecurringForm').append('<input type="hidden" id="RecurringFormType" name="RecurringFormType" value="Task">'); 
         
     } else {

     	if($('.WORecurrenceIDForTask').val() != ''){

     		var WORecurrenceID = $('.WORecurrenceIDForTask').val();
	     	$.ajax({
	            url: '<?=SITE_URL?>Account/Actions/removeRecurrenceSetting/',
	            data: { WORecurrenceID:WORecurrenceID },
	            method: "post",
	            dataType: 'html',
	            success: function (result) {
	                if (result != '') {
	                   $('.WORecurrenceIDForTask').val(''); 
	                   return true;  	
	                } else {
	                    alert("Something went wrong.Please try again!");
	                    return false;
	                }
	            }
	        });	
	     }
     }
});

$('#IsRecurringEvent').change(function() { 
     if(this.checked) {
        $('#RecurringTaskModal').modal({
		    backdrop: 'static',
		    keyboard: false  // to prevent closing with Esc button (if you want this too)
		}); 
        $("#RecurringFormType").remove();
        $('#RecurringForm').append('<input type="hidden" id="RecurringFormType" name="RecurringFormType" value="Event">'); 
     } else {

     	if($('.WORecurrenceIDForEvent').val() != ''){

     		var WORecurrenceID = $('.WORecurrenceIDForEvent').val();
	     	$.ajax({
	            url: '<?=SITE_URL?>Account/Actions/removeRecurrenceSetting/',
	            data: { WORecurrenceID:WORecurrenceID },
	            method: "post",
	            dataType: 'html',
	            success: function (result) {
	                if (result != '') {
	                   $('.WORecurrenceIDForEvent').val(''); 
	                   return true;  	
	                } else {
	                    alert("Something went wrong.Please try again!");
	                    return false;
	                }
	            }
	        });	
	     }
     }
});

$('#RecurringForm').validate({
	errorPlacement: function(){
	    return false;
	},
	/* submitHandler: function (form) { 
	    return false; 
	},*/
	highlight: function(element) {
		$(element).closest('.form-group').addClass('has-error');
	},
	unhighlight: function(element) {
	    $(element).closest('.form-group').removeClass('has-error');
	}
});	

$(document).on('click', "#RecurrenceSave", function () {
    if ($("#RecurringForm").valid()) {	
        $.ajax({
            url: '<?=SITE_URL?>WorkOrder/Actions/saveRecurrenceSetting/',
            data: $("#RecurringForm").serialize(),
            method: "post",
            dataType: 'html',
            success: function (result) {
                if (result != '') {
                	if($("#RecurringFormType").val() == 'Event')
                   		$('.WORecurrenceIDForEvent').val(result); 
                   	else if($("#RecurringFormType").val() == 'Task')
                   		$('.WORecurrenceIDForTask').val(result); 
                   	$('#RecurringTaskModal').modal('hide'); 
                   	return true;  	
                } else {
                    alert("Something went wrong.Please try again!");
                    return false;
                }
            }
        });
    }
    return false;
});

$(document).on('click', "#RecurrenceClose", function () { 
	if($("#RecurringFormType").val() == 'Event')
   		$('#IsRecurringEvent').prop('checked', false);
   	else if($("#RecurringFormType").val() == 'Task')
   		$('#IsRecurringTask').prop('checked', false);
});

function DeleteAccount(AccountID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#5fc55f",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>Account/Actions/DeleteAccount/<?=$AccountData->AccountID;?>/";
		}, 500);
    });
	return false;
}
</script>


