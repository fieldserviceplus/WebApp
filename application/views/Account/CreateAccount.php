<?php $this->load->view('Template/HeaderView')?>

<script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_JS_KEY?>&libraries=places"></script>
 
<div class="container-fluid pt-25">

	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Create Account</h6>
					</div>
					<div class="pull-right">
						<span style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong></span>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Account/Actions/CreateAccount" class="form-horizontal" id="CreateAccount" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

												<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>Account Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-3">Assigned To<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="AssignedTo" id="AssignedTo" class="form-control select2">
														<option value="">Select Owner</option>
														<?php $Users = $this->ActionsModel->GetUsers();
															foreach ($Users as $User) { 
																if(isset($AccountData->AssignedTo) && $AccountData->AssignedTo == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else { ?>
																	<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('AssignedTo');  ?>
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-3">Account Name<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="AccountName" id="AccountName" value="<?=isset($AccountData->AccountName)?$AccountData->AccountName:"";?>">
															<?php echo form_error('AccountName');  ?>
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-3">Phone #<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="PhoneNo" id="PhoneNo" value="<?=isset($AccountData->PhoneNo)?$AccountData->PhoneNo:"";?>">
															<?php echo form_error('PhoneNo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Preferred Technician</label>
														<div class="col-md-9">
														<select name="PreferredTechnician" id="PreferredTechnician" class="form-control select2">
														<option value="">Select Technician</option>
														<?php $Users = $this->ActionsModel->GetUsers();
															foreach ($Users as $User) {  
															if(isset($AccountData->PreferredTechnician) && $AccountData->PreferredTechnician == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else { ?>
																	<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
															<?php } ?>
														<?php } ?>
														</select>
														<?php echo form_error('PreferredTechnician');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Access Notes</label>
														<div class="col-md-9">
															<input type="text" class="form-control" id="AccessNotes" name="AccessNotes" value="<?=isset($AccountData->AccessNotes)?$AccountData->AccessNotes:"";?>">
															<?php echo form_error('AccessNotes');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Notes</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Notes" id="Notes" value="<?=isset($AccountData->Notes)?$AccountData->Notes:"";?>">
															<?php echo form_error('Notes');  ?>
														</div>
													</div>
												</div>

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Account Type<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<?php $AccountTypes = $this->ActionsModel->GetAccountTypes(); ?>
															<select class="form-control" name="AccountType" id="AccountType">
															<option value="">Select Account Type</option>
															<?php foreach ($AccountTypes as $AccountType) { 
															if(isset($AccountData->AccountType) && $AccountData->AccountType == $AccountType->AccountTypeID){ ?>
																	<option value="<?=$AccountType->AccountTypeID?>" selected="selected"><?=$AccountType->AccountType?></option>
																<?php } else { ?>
																	<option value="<?=$AccountType->AccountTypeID?>"><?=$AccountType->AccountType?></option>
																<?php } ?>
															<?php } ?>
															<?php echo form_error('AccountType');  ?>
														</select>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Is Active</label>
														<div class="col-md-9">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="IsActive" id="IsActive" value="1" checked="">
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

													<!--<div class="form-group">
														<label class="control-label col-md-3">Last Activity</label>
														<div class="col-md-9">
															<input type="text" class="form-control showdatepicker" name="LastActivityDate" id="LastActivityDate" value="<?=isset($AccountData->LastActivityDate)?$AccountData->LastActivityDate:"";?>">
															<?php //echo form_error('LastActivityDate');  ?>
														</div>
													</div>

													 <div class="form-group">
														<label class="control-label col-md-3">Last Service Date</label>
														<div class="col-md-9">
															<input type="text" class="form-control showdatepicker" name="LastServiceDate" id="LastServiceDate" value="<?=isset($AccountData->LastServiceDate)?$AccountData->LastServiceDate:"";?>">
															<?php //echo form_error('LastServiceDate');  ?>
														</div>
													</div>-->

													<div class="form-group">
														<label class="control-label col-md-3">Pop Up Reminder</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="PopUpReminder" id="PopUpReminder" value="<?=isset($AccountData->PopUpReminder)?$AccountData->PopUpReminder:"";?>">
															<?php //echo form_error('PopUpReminder');  ?>
														</div>
													</div> 
											
												</div>
												<!--/span-->
												</div>

												<div class="col-md-12">
												<div class="txt-dark capitalize-font"><strong>Address Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Billing Address</label>
														<div class="col-md-9">
															<!-- <div class="input-group"> -->
																<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-search"></i>Search Address</button>
																<!-- <div class="input-group-addon"><a href="#" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-map-marker"></i></a></div> -->
															<!-- </div> -->
														</div>
													</div>
												
													<div class="form-group">
														<label class="col-sm-3 control-label">Billing Street<span class="required-sign">*</span></label>
														<div class="col-sm-9">
															<!-- <div class="input-group"> -->
																<input type="text" class="form-control" name="BillingAddress" id="BillingAddress" placeholder="" data-toggle="modal" data-target="#location-picker-modal" value="<?=isset($AccountData->BillingAddress)?$AccountData->BillingAddress:"";?>">
																<!-- <div class="input-group-addon"><a href="#" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-map-marker"></i></a></div> -->
															<!-- </div> -->
															<?php echo form_error('BillingAddress');  ?>
															<input type="hidden" name="BillingLatitude" id="BillingLatitude" value="<?=isset($AccountData->BillingLatitude)?$AccountData->BillingLatitude:"";?>">
															<input type="hidden" name="BillingLongitude" id="BillingLongitude" value="<?=isset($AccountData->BillingLongitude)?$AccountData->BillingLongitude:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Billing City<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="BillingCity" id="BillingCity" value="<?=isset($AccountData->BillingCity)?$AccountData->BillingCity:"";?>">
															<?php echo form_error('BillingCity');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Billing State<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="BillingState" id="BillingState" value="<?=isset($AccountData->BillingState)?$AccountData->BillingState:"";?>">
															<?php echo form_error('BillingState');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-3">Billing Country<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="BillingCountry" id="BillingCountry" value="<?=isset($AccountData->BillingCountry)?$AccountData->BillingCountry:"";?>">
															<?php echo form_error('BillingCountry');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Billing Postal Code<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="BillingPostalCode" id="BillingPostalCode" value="<?=isset($AccountData->BillingPostalCode)?$AccountData->BillingPostalCode:"";?>">
															<?php echo form_error('BillingPostalCode');  ?>
														</div>
													</div>
												</div>

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Shipping Address</label>
														<div class="col-md-9">
															<!-- <div class="input-group"> -->
																<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal2"><i class="fa fa-search"></i>Search Address</button>
																<!-- <div class="input-group-addon"><a href="#" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-map-marker"></i></a></div> -->
															<!-- </div> -->
														</div>
													</div>
												
													<div class="form-group">
														<label class="col-sm-3 control-label">Shipping Street<span class="required-sign">*</span></label>
														<div class="col-sm-9">
															<!-- <div class="input-group"> -->
																<input type="text" class="form-control" name="ShippingAddress" id="ShippingAddress" placeholder="" value="<?=isset($AccountData->ShippingAddress)?$AccountData->ShippingAddress:"";?>">
																<!-- <div class="input-group-addon"><a href="#" data-toggle="modal" data-target="#location-picker-modal2"><i class="fa fa-map-marker"></i></a></div> -->
															<!-- </div> -->
															<?php echo form_error('ShippingAddress');  ?>
															<input type="hidden" name="ShippingLatitude" id="ShippingLatitude" value="<?=isset($AccountData->ShippingLatitude)?$AccountData->ShippingLatitude:"";?>">
															<input type="hidden" name="ShippingLongitude" id="ShippingLongitude" value="<?=isset($AccountData->ShippingLongitude)?$AccountData->ShippingLongitude:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Shipping City<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="ShippingCity" id="ShippingCity" value="<?=isset($AccountData->ShippingCity)?$AccountData->ShippingCity:"";?>">
															<?php echo form_error('ShippingCity');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Shipping State<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="ShippingState" id="ShippingState" value="<?=isset($AccountData->ShippingState)?$AccountData->ShippingState:"";?>">
															<?php echo form_error('ShippingState');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-3">Shipping Country<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="ShippingCountry" id="ShippingCountry" value="<?=isset($AccountData->ShippingCountry)?$AccountData->ShippingCountry:"";?>">
															<?php echo form_error('ShippingCountry');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Shipping Postal Code<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="ShippingPostalCode" id="ShippingPostalCode" value="<?=isset($AccountData->ShippingPostalCode)?$AccountData->ShippingPostalCode:"";?>">
															<?php echo form_error('ShippingPostalCode');  ?>
														</div>
													</div>
													
												</div>

											</div>
											<!-- /Row -->
										</div>

										<!-- Start Custom Fields Row -->
										<?php $this->load->view('Setup/Customize/Fields/CustomFieldsForm', array('Object' => 'Account', 'ObjectID' => 0)); ?>
										<!-- End Custom Fields Row -->

										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="CreateAccount">Save</button>
															<a href="<?=SITE_URL;?>Account/View" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->
<!-- modal -->
 <div class="modal fade" id="location-picker-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Address Search</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" action="javascript:void(0)">
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-2">Location</label>
								<div class="col-md-10">
									<input id="BillingLocation" type="text" name="" class="form-control" placeholder="Enter Address">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="Billingpopupmap" style="height: 400px;"></div><br />
				
				</form>
				
			</div>
		</div>
	</div>
</div> 

 <div class="modal fade" id="location-picker-modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Address Search</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" action="javascript:void(0)">
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-2">Location</label>
								<div class="col-md-10">
									<input id="ShippingLocation" type="text" name="" class="form-control" placeholder="Enter Address">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="Shippingpopupmap" style="height: 400px;"></div><br />
				
				</form>
				
			</div>
		</div>
	</div>
</div> 

<?php //$this->load->view('Modal/LocationPickerView')?>

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>


<script type="text/javascript">
function setState(CountryID)
{    
    $.ajax({
        url: '<?=SITE_URL?>Account/Actions/getState/',
        type: "POST",
        data: {CountryID : CountryID},
        dataType: 'html',
        async: true,
        success: function(data)
        {  
            $("#BillingState").html(data);
            $("#BillingCity").empty();
        }
    });
}
    
function getCity(StateID)
{   
    $.ajax({
        url: '<?=SITE_URL?>Account/Actions/getCity/',
        type: "POST",
        data: {StateID : StateID},
        dataType: 'html',
        async: true,
        success: function(data)
        {  
            $("#BillingCity").html(data);
        }
    });
}

$(document).ready(function () {
 
    $('#CreateAccount').validate({ 
        rules: {
            AssignedTo: {
                required: true
            },
            AccountName: {
                required: true
            },
            PhoneNo: {
                required: true
            },
           /* PreferredTechnician: {
                required: true
            },*/
            BillingCountry: {
                required: true
            },
            BillingState: {
                required: true
            },
            BillingCity: {
                required: true
            },
            BillingStreet: {
                required: true
            },
            BillingAddress: {
                required: true
            },
            BillingPostalCode: {
                required: true
            },
           /* AccessNotes: {
                required: true
            },
            PopUpReminder: {
                required: true
            },
            Notes: {
                required: true
            },*/
            AccountType: {
                required: true
            },
            Website: {
                required: true
            },
           /* LastActivityDate: {
                required: true
            },
            LastServiceDate: {
                required: true
            },*/
           /* PrimaryContact: {
                required: true
            },*/
            Country: {
                required: true
            },
            ShippingState: {
                required: true
            },
            ShippingCity: {
                required: true
            },
            ShippingStreet: {
                required: true
            },
            ShippingAddress: {
                required: true
            },
            ShippingPostalCode: {
                required: true
            },
            ShippingCountry: {
                required: true
            },
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    /*$(".datepicker").datepicker();*/
     /*$('.datepicker').datepicker({
	    format: 'mm/dd/yyyy',
	    startDate: '-3d'
	});*/

	
    
});
</script>
<!-- Set Location -->
<script src="<?=SITE_URL?>dist/js/location-picker.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	set_location_in_map("Billing");
	set_location_in_map("Shipping");
});
</script>




