<?php $this->load->view('Template/HeaderView');?>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

    <div class="container-fluid pt-25">

       <!-- Row -->
	    <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-default card-view">
					<div  class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-lg-3 col-md-3 col-sm-5 col-xs-12 pr-0 pl-0">
								<form method="post" action="<?=SITE_URL?>Account/View/" id="SelectedAcForm">
								
										<div class="col-lg-2 col-md-3 col-sm-3 col-xs-3">
											<label class="control-label mt-10"><strong>View:</strong></label>
										</div>
										<div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 pr-0">
											<select name="SelectedAccount" class="selectpicker" data-style="form-control btn-default btn-outline" onchange="GoToNewView()"> 
												<option disabled="" selected="">Select Views</option>
												<optgroup label="Default Views">
													<option value="MyAccounts">My Accounts</option>
												<option value="AllAccounts">All Accounts</option>
												<option value="NewAccountsThisWeek">New Accounts This Week</option>
												</optgroup>
												<optgroup label="My Custom Views">
													<?php $Views = $this->ViewModel->GetViews();
													foreach ($Views as $View) { ?>
													<option value="<?=$View->AccountViewID?>" <?=(isset($SelectedAccount) && $SelectedAccount==$View->AccountViewID)?"selected":""?>><?=$View->AccountViewName?></option>
												<?php } ?>
												</optgroup>
											</select>
										</div>	
								
								</form>	
							</div>	
							<div class="col-lg-5 col-md-6 col-sm-7 col-xs-12">
								<div class="form-group" >
								<!-- <div class="btn-group">
									<button class="btn btn-primary btn-outline">Go</button>
								</div> --> 
								<div class="btn-group">
									<div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button"> <i class="fa fa-pencil"></i> <span class="caret"></span></button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li>
												<a><span><strong>LIST VIEW ACTIONS</strong></span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>Account/View/CreateNewView"><span>Create New View</span></a>
											</li>
											<li>
												<a href="#" OnClick="CopyCustomView(<?=$SelectedAccount?>)"><span>Copy</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#account-rename-view-modal"><span>Rename</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#account-edit-sharing-view-modal"><span>Edit Sharing</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#account-edit-filters-view-modal"><span>Edit Filters</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#account-edit-displayed-columns-view-modal"><span>Edit Displayed Columns</span></a>
											</li> 
											 <li>
												<a href="#" OnClick="DeleteCustomView(<?=$SelectedAccount?>)"><span>Delete View</span></a>
											</li>
										</ul>
									</div>
								</div>
								<div class="btn-group">
									<button class="btn btn-default btn-outline" title="Refresh" ><i class="fa fa-refresh"></i></button>
									<button class="btn btn-default btn-outline" title="Export" id="Export-Excel"><i class="fa fa-file-excel-o"></i></button>
								</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
								<div class="pull-right">
									<div class="form-group">
										<a href="<?=SITE_URL?>Account/Actions/CreateAccount" class="btn btn-primary btn-outline">New Account</a>
									</div>
								</div>
							</div>
							<!-- <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
								<div class="pull-right">
									<div class="input-group">
										<input id="myTable1globalfilter" class="form-control myTable1globalfilter" placeholder="Search" type="text">
										<span class="input-group-addon"><i class="zmdi zmdi-search"></i></span> 
									</div>
								</div>
							</div> -->
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view loading-view" style="min-height: 700px;">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">

								<div class="cus-view-loading-img text-center">
									<img src="<?=SITE_URL?>/img/loader.gif">
								</div>

								<div class="table-responsive">

									<input type="hidden" id="cur_page" value="1">
									<table id="postsList" class="table table-hover" width="100%">
										<thead>
											<?php /*<tr> 
												foreach ($AccountData['ColumnsNames'] as $key => $value) { ?>
														<th class="text-center"><a href="<?= base_url() ?>Account/View/List/<?=$SelectedAccount?>/0/<?=$value?>/<?=$order?>"><?=$value?></a></th>
												<?php }
											</tr> */ ?>
										</thead>
										<tbody>
										<?php /*foreach ($AccountData['ColumnsValues'] as $key1 => $Account) { ?>
											<tr> 
											<?php foreach ($AccountData['ColumnsNames'] as $value) {
													if($value == 'AccountName'){ ?>
														<td class="text-center"><a href="<?=SITE_URL?>Account/Actions/AccountDetails/<?=$Account->AccountID?>"><?=$Account->$value?></a></td>
													<?php } else if($value == 'AssignedTo'){ ?>  
														<td class="text-center"><a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$Account->AssignedToID?>"><?=$Account->$value?></a></td>
													<?php } else if($value == 'PreferredTechnician'){ ?>  
														<td class="text-center"><a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$Account->AssignedToID?>"><?=$Account->$value?></a></td>
													<?php } else if($value == 'PrimaryContact'){ ?>  
														<td class="text-center"><a href="<?=SITE_URL?>Contact/Actions/ContactDetails/<?=$Account->ContactID?>"><?=$Account->$value?></a></td>
													<?php } else if($value == 'CreatedBy'){ ?>  
														<td class="text-center"><a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$Account->CreatedByID?>"><?=$Account->$value?></a></td>
													<?php } else if($value == 'LastModifiedBy'){ ?>  
														<td class="text-center"><a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$Account->LastModifiedByID?>"><?=$Account->$value?></a></td>
													<?php } else {?>
														<td class="text-center"><?=$Account->$value?></td>
													<?php } ?>
											<?php } ?>
											</tr>
										<?php }*/ ?>
										</tbody>
									</table> 
   
								</div>

								<!-- <div class="dataTables_wrapper no-footer">
									<div class="bottom">
										<div class="dataTables_paginate paging_simple_numbers" id="accounttable_paginate">	<?=$pagination;?>
										</div>
									</div>
								</div> -->
	 
								<div class="dataTables_wrapper no-footer row-per-page-div">
									<div class="bottom">
										<div class="dataTables_info result-count" id="accounttable_info" role="status" aria-live="polite"></div>
										<div class="dataTables_length" id="accounttable_length">
											<label>Show 
												<select name="accounttable_length" class="rowperpage">
													<option value="10">10</option>
													<option value="25">25</option>
													<option value="50">50</option>
													<option value="100">100</option>
												</select> entries</label>
										</div>
										<div id='pagination' class="dataTables_paginate paging_simple_numbers" id="accounttable_paginate">
										</div>
									</div>
								</div>
						
							</div>
						</div>
					</div>
				</div>	
		</div>
		<!-- /Row -->
</div>	
<!-- Start Modal -->

<?php $this->load->view('Account/Modal/AccountEditSharingView'); ?>
<?php $this->load->view('Account/Modal/AccountEditFiltersView'); ?>
<?php $this->load->view('Account/Modal/AccountEditDisplayedColumnsView'); ?>
<?php $this->load->view('Account/Modal/AccountRenameView'); ?>

<?php $this->load->view('Template/FooterView')?>

<!-- Multiselect JavaScript -->
<!-- <script src="<?=SITE_URL?>vendors/bower_components/multiselect/js/jquery.multi-select.js"></script> -->
<!-- Bootstrap Switch JavaScript -->
<!-- <script src="<?=SITE_URL?>vendors/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script> -->

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript" src="<?=SITE_URL?>dist/js/multiselect.min.js"></script>


<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>
	
<script type="text/javascript">

$(document).ready(function () {

    $('#FiltersViewForm').validate({ 
        rules: {
            "FilterFields[]": {
                required: true
            },
            "FilterValues[]": {
                required: true
            },
            "FilterConditions[]": {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    $('#RenameViewForm').validate({ 
        rules: {
            AccountViewName: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    $('#FieldsDisplayViewForm').validate({ 
        rules: {
            "SpecifyFieldsDisplay[]": {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
});

 	/*var dataTable = $('#myTable1').DataTable();

 	$('#accountglobalfilter').keyup(function(){
    	var dataTable = $('#accounttable').DataTable();
      	dataTable.search($(this).val()).draw() ;
    }); */
    function GoToNewView()
 	{
 		$('#SelectedAcForm').submit();
 	}
</script>
<script type="text/javascript">
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
   
    var x = 1; //initlal text box count
    $(".add_field_button").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="form-group" >'
					+'<div class="col-md-4">'
					+'	<select class="form-control" name="FilterFields[]">'
					+'		<option value="">Select Field</option>'
					<?php $Fields = $this->ViewModel->AccountViewFields(); 
					foreach($Fields as $Field){  ?>
						+'	<option value="<?=$Field->FieldName;?>"><?=$Field->FieldName;?></option>'	
					<?php } ?>
					+'	</select>'
					+'</div>'
					+'<div class="col-md-3">'
					+'		<select class="form-control" name="FilterConditions[]">'
					+'	<option value="Equals">Equals</option>'
					+'	<option value="NotEqualTo">Not Equal To</option>'
					+'	<option value="LessThan">Less Than</option>'
					+'	<option value="GreaterThan">Greater Than</option>'
					+'	<option value="LessOREqualTo">Less OR Equal To</option>'
					+'	<option value="GreaterOREqualTo">Greater OR Equal To</option>'
					+'	<option value="Contains">Contains</option>'
					+'	<option value="StartsWith">Starts With</option>'
					+'	<option value="DoesNotContain">Does Not Contain</option>'
					+'</select>'
					+'</div>'
					+'<div class="col-md-4">'
					+'	<input type="text" class="form-control" name="FilterValues[]">'
					+'</div>'
					+'<div class="col-md-1 btn-group btn-group-vertical pl-0">'
					+	 '<button type="button" id="" class="btn btn-default btn-icon-anim btn-circle remove_field_filter_criteria"><i class="fa fa-close"></i></button>'
					+'</div>'
				+'</div>'); //add input box
		}
    });
   
    $(document).on("click",".remove_field_filter_criteria", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.form-group').remove(); x--;
    });
});

/* Multiselect Init*/     
/*$('#my-select').multiSelect({ selectableOptgroup: true, selectableHeader: '<div style="text-align: center;">Available Fields</div>', selectionHeader: '<div style="text-align: center;">Displayed Fields</div>' });*/


function DeleteCustomView(AccountViewID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#5fc55f",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>Account/View/DeleteCustomView/<?=$SelectedAccount?>/";
		}, 500);
    });
	return false;
}

function CopyCustomView(AccountViewID)
{ 
	swal({
	  title: "Nice!",
	  text: "View copy successfully.",
	  type: "success",
	  showConfirmButton: false, 
	});

	setTimeout(function () {
	    window.location.href = "<?=SITE_URL;?>Account/View/CopyCustomView/<?=$SelectedAccount?>/";
	}, 1000);
}

/*function RenameCustomView(AccountViewID)
{ 
    swal({
	  title: "Rename View",
	  text: "Enter View Name:",
	  type: "input",
	  showCancelButton: true,
	  closeOnConfirm: false,
	  confirmButtonText: "Save",   
	  inputPlaceholder: "View Name",
	  confirmButtonColor: "#5fc55f",
	  showLoaderOnConfirm: true,
	  inputValue: "<?=$AccountCustomViewData['AccountViewData']->AccountViewName?>"
	}, function (inputValue) {
	  if (inputValue === false) return false;
	  if (inputValue === "") {
	    swal.showInputError("You need to write view name!");
	    return false
	  }
	  swal("Nice!", "View rename successfully.", "success");
	  setTimeout(function () {
	    window.location.href = "<?=SITE_URL;?>Account/View/RenameCustomView/<?=$SelectedAccount?>/"+ encodeURIComponent(inputValue);
	  }, 1000);
	});
}*/
</script>
<script type="text/javascript">
$(document).ready(function() {

    $('#multiselect').multiselect({ keepRenderingSort: true });

});
function moveTop() {
    $("#multiselect_to option:selected").each(function () {
        var listItem = $(this);
        listItem.insertBefore(listItem.siblings().first());
    });
}

function moveBottom() {
    var itemsCount = $("#multiselect_to option").length;

    $($("#multiselect_to option:selected").get().reverse()).each(function () {
        var listItem = $(this);
        listItem.insertAfter(listItem.siblings().last());
    });
}

$(document).ready(function() {
	$('.buttons-excel').hide();
	$("#Export-Excel").on("click", function() {
	    $( '.buttons-excel' ).click();
	});
});
</script>

<script type='text/javascript'>
 $(document).ready(function(){

     $('#pagination').on('click','a',function(e){

       e.preventDefault(); 
       var pageno = $(this).attr('data-ci-pagination-page');
       loadPagination(pageno);
     });

     $('.rowperpage').on('change',function(e){

       e.preventDefault(); 

       var pageno = $("#cur_page").val();
       var rowperpage = $(this).val();
       loadPagination(pageno,'','',rowperpage);
     });

     $(document).on('click','th.sorting a',function(e){
 
       e.preventDefault(); 
       var sorting_col = $(this).attr('data-ci-sorting-col');
       var sorting_val = $(this).attr('data-ci-sorting-val');

       sorting_val = (sorting_val == '') ? "desc" : sorting_val;

       loadPagination(0, sorting_col, sorting_val);
     });

     loadPagination(0);

     function loadPagination(pagno, sortby='', order='',rowperpage=10){

       $("#cur_page").val(pagno);
       $('.dataTables_length').hide();
       
       $(".cus-view-loading-img").show();
       $(".loading-view").css('opacity','0.5');
       $.ajax({
         url: '<?=SITE_URL?>Account/View/List/<?=$SelectedAccount?>/'+pagno+'/'+rowperpage+'/'+sortby+'/'+order,
         type: 'get',
         dataType: 'json',
         success: function(response){
            $('#pagination').html(response.pagination);
            $('.result-count').html(response.result_count);
            createTable(response);

            $('.dataTables_length').show();
            $(".cus-view-loading-img").hide();
            $(".loading-view").css('opacity','1');

            if(response.AccountData.TotalRows == 0){
            	$('#postsList tbody').html("<tr><td colspan="+response.AccountData.ColumnsNames.length+" align='center'>No records found.</td></tr>");
            }
            else {
            	$(".row-per-page-div").show();
            }
         }
       });
     }

     function createTable(result){

       $('#postsList tbody').empty();
       
        $.each(result.AccountData.ColumnsValues, function( key1, Account ) {

        	var tr = "<tr>";

        	$.each(result.AccountData.ColumnsNames, function( key2, value ) {

        		var account_val = (Account[value] == null) ? "" : Account[value];

		  		if(value == 'AccountName'){ 
		  			tr += '<td class="text-center"><a href="<?=SITE_URL?>Account/Actions/AccountDetails/'+Account.AccountID+'">'+account_val+'</a></td>';

		  		} else if(value == 'AssignedTo'){ 
		  			
		  			tr += '<td class="text-center"><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/'+Account.AssignedToID+'">'+account_val+'</a></td>';
		  		} else if(value == 'PreferredTechnician'){ 
		  			
		  			tr += '<td class="text-center"><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/'+Account.PreferredTechnicianID+'">'+account_val+'</a></td>';
		  		} else if(value == 'PrimaryContact'){ 
		  			
		  			tr += '<td class="text-center"><a href="<?=SITE_URL?>Contact/Actions/ContactDetails/'+Account.ContactID+'">'+account_val+'</a></td>';
		  		} else if(value == 'CreatedBy'){ 
		  			
		  			tr += '<td class="text-center"><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/'+Account.CreatedByID+'">'+account_val+'</a></td>';
		  		} else if(value == 'LastModifiedBy'){ 
		  			
		  			tr += '<td class="text-center"><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/'+Account.LastModifiedByID+'">'+account_val+'</a></td>';

		  		} else { 
		  			
		  			tr += '<td class="text-center">'+account_val+'</td>';
		  		}
		  	});

		  	tr += "</tr>";
		  	$('#postsList tbody').append(tr);
		});

   
        var tr = "<tr>";
		$.each(result.AccountData.ColumnsNames, function( key2, value ) {
		 
		  	var sort_val = (typeof result.order[value] === 'undefined') ? "" : result.order[value];
		  	tr += '<th class="sorting text-center"><a href="#" class="sort'+sort_val+'" data-ci-sorting-col="'+value+'" data-ci-sorting-val="'+sort_val+'">'+value+'</a></th>';
	  	});
	  	tr += "</tr>";

	  	$('#postsList thead').html(tr);
}
});
</script>


