<div class="modal fade" id="CreateNewEvent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Create New Event</h5>
			</div>
			<form class="form-horizontal" id="NewEventForm" method="post" action="<?=SITE_URL;?>Account/Actions/CreateEvent">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
						    <div class="form-group">
								<label class="control-label col-md-3">Assigned To<span class="required-sign">*</span></label>
								<div class="col-md-9">
									<select name="AssignedTo" id="AssignedTo" class="form-control select2">
									<option value="">Select Owner</option>
									<?php $Users = $this->ActionsModel->GetUsers();
									foreach ($Users as $User) {  
										if($User->UserID == $this->session->userdata('UserID')){ ?>
											<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
										<?php } else { ?>
											<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
										<?php } ?>
									<?php } ?>
									</select>
									<input type="hidden" name="AccountID" value="<?=$AccountData->AccountID?>">
								</div>
							</div>
						</div>
						<div class="input_fields_wrap">
						    <div class="form-group">
								<label class="control-label col-md-3">Subject<span class="required-sign">*</span></label>
								<div class="col-md-9">
									<input type="text" name="Subject" value="" class="form-control">
								</div>
							</div>
						</div>
						<div class="input_fields_wrap">
						    <div class="form-group">
								<label class="control-label col-md-3">Description</label>
								<div class="col-md-9">
									<input type="text" name="Description" value="" class="form-control">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Type<span class="required-sign">*</span></label>
							<div class="col-md-9">
								<select name="EventType" id="EventType" class="form-control">
								<option value="">Select Task Type</option>
								<?php $EventTypes = $this->ActionsModel->GetEventTypes();
									foreach ($EventTypes as $EventType) { ?>
										<option value="<?=$EventType->EventTypeID?>"><?=$EventType->EventTypeName?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Priority<span class="required-sign">*</span></label>
							<div class="col-md-9">
								<select name="EventPriority" id="EventPriority" class="form-control">
								<?php $Priorities = $this->ActionsModel->GetEventPriorities();
									foreach ($Priorities as $Priority) { ?>
										<option value="<?=$Priority->EventPriorityID?>"><?=$Priority->Priority?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="input_fields_wrap">
						    <div class="form-group">
								<label class="control-label col-md-3">Start Date<span class="required-sign">*</span></label>
								<div class="col-md-9">
									<input type="text" name="EventStartDate" value="<?=date("m/d/Y h:i A")?>" class="form-control showdatetimepicker">
								</div>
							</div>
						</div>
						<div class="input_fields_wrap">
						    <div class="form-group">
								<label class="control-label col-md-3">End Date<span class="required-sign">*</span></label>
								<div class="col-md-9">
									<input type="text" name="EventEndDate" value="<?=date("m/d/Y h:i A")?>" class="form-control showdatetimepicker">
								</div>
							</div>
						</div>
						<div class="input_fields_wrap">
						    <div class="form-group">
								<label class="control-label col-md-3">All Day</label>
								<div class="col-md-9 ">
									<div class="checkbox checkbox-primary">
										<input id="checkbox2" type="checkbox" name="IsAllDayEvent" value="1">
										<label for="checkbox2"></label>
									</div>

								</div>
							</div>
						</div>
						<div class="input_fields_wrap">
						    <div class="form-group">
								<label class="control-label col-md-3">Is Recurring</label>
								<div class="col-md-9 ">
									<div class="checkbox checkbox-primary">
										<input id="IsRecurringEvent" name="IsRecurring" type="checkbox" value="1">
										<label for="checkbox2"></label>
										<input id="WORecurrenceID" name="WORecurrenceID" type="hidden" class="WORecurrenceIDForEvent">
									</div>

								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Name</label>
							<div class="col-md-9">
								<select name="Who" id="Who" class="form-control select2">
								<option value="">Select Contact</option>
								<?php $Contacts = $this->ActionsModel->GetContacts($AccountData->AccountID);
									foreach ($Contacts as $Contact) { ?>
										<option value="<?=$Contact->ContactID?>"><?=$Contact->FirstName?> <?=$Contact->LastName?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">Related To</label>
							<div class="col-md-9">
								<input type="text" value="<?=$AccountData->AccountName?>" class="form-control" readonly="">
								<input type="hidden" name="RelatedTo" value="Account">
								<input type="hidden" name="What" value="<?=$AccountData->AccountID?>">
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="CreateEvent">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>