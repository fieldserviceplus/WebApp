<div class="panel panel-default card-view"> 
	 <div class="panel-heading">
		<div class="pull-left">
			<h6 class="panel-title txt-dark">Related Information</h6>
		</div>
		<div class="clearfix"></div>
	</div> 
	<div class="panel-wrapper collapse in">
		<div class="panel-body">

		<div class="row">
			<div class="col-md-12">

				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
						<div class="txt-dark capitalize-font mt-20"><strong>Contacts</strong></div>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<a href="<?=SITE_URL?>Contact/Actions/CreateContact/?Account=<?=$AccountData->AccountID?>"  class="btn btn-primary btn-outline pull-right mb-10 mt-15"><i class="fa fa-plus"></i> New Contact</a>
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="table-wrap">
				<div class="table-responsive">
				<table id="ContactsTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>Name </th>
							<th>Title</th>
							<th>Phone</th>
							<th>Email</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($Contacts as $key => $Contact) { ?>
						<tr>
							<td><a href="<?=SITE_URL?>Contact/Actions/ContactDetails/<?=$Contact->ContactID?>"><?=$Contact->FirstName.' '.$Contact->LastName?></a></td>
							<td><?=$Contact->Title?></td>
							<td><?=$Contact->PhoneNo?></td>
							<td><?=$Contact->Email?></td>
						</tr>
						<?php } ?> 
					</tbody>
				</table>
				</div>
				<?php if(count($Contacts) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Account/Actions/RelatedObject/<?=$AccountData->AccountID;?>/Contacts" type="button" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
				<?php } ?>
				</div>
			</div> 

			<!-- <div class="col-md-12">
			<div class="clearfix"></div>
			<div class="col-md-12 pl-0 pr-0">
				<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
					<div class="txt-dark capitalize-font mt-20"><strong>Locations</strong></div>
				</div>
				<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
					<button type="button" class="btn btn-primary btn-outline pull-right mb-10 mt-15"><i class="fa fa-plus"></i> New Location</button>
				</div>
			</div>
			<div class="clearfix"></div>

			<div class="table-wrap">
			<div class="table-responsive">
			<table id="LocationsTblEAD" class="table table-hover display table-bordered" width="99%">
				<thead>
					<tr>
						<th>Name </th>
						<th>Street</th>
						<th>City</th>
					</tr>
				</thead>
				<tbody>
					<?php /*foreach ($Locations as $key => $Location) { ?>
						<tr>
							<td><a href="#"><?=$Location->Name?></a></td>
							<td><?=$Location->Address?></td>
							<td><?=$Location->City?></td>
						</tr>
					<?php }*/ ?>
				</tbody>
			</table>
			</div>
			<?php /*if(count($Locations) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Account/Actions/RelatedObject/<?=$AccountData->AccountID;?>/Locations" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
				<?php }*/ ?>
			</div>
			</div> -->

			<div class="col-md-12">
				<div class="clearfix"></div>
				
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
						<div class="txt-dark capitalize-font mt-20"><strong>Work Orders</strong></div>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<a href="<?=SITE_URL?>WorkOrder/Actions/CreateWorkOrder/?Account=<?=$AccountData->AccountID?>" class="btn btn-primary btn-outline pull-right mb-10 mt-15"><i class="fa fa-plus"></i> New Work Order</a>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="table-wrap">
				<div class="table-responsive">
				<table id="WorkOrdersTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>WO # </th>
							<th>Subject</th>
							<th>Type</th>
							<th>Date</th>
							<th>Status</th>
							<th>Priority</th>
							<th>Assigned To</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($WorkOrders as $key => $WorkOrder) { ?>
							<tr>
								<td><a href="<?=SITE_URL?>WorkOrder/Actions/WorkOrderDetails/<?=$WorkOrder->WorkOrderID?>"><?=$WorkOrder->WorkOrderNo?></a></td>
								<td><a href="<?=SITE_URL?>WorkOrder/Actions/WorkOrderDetails/<?=$WorkOrder->WorkOrderID?>"><?=$WorkOrder->Subject?></a></td>
								<td><?=$WorkOrder->WorkOrderType?></td>
								<td><?=$WorkOrder->CreatedDate?></td>
								<td><?=$WorkOrder->Status?></td>
								<td><?=$WorkOrder->Priority?></td>
								<td><a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$WorkOrder->AssignedTo?>"><?=$WorkOrder->AssignedToName?></a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				</div>
				<?php if(count($WorkOrders) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Account/Actions/RelatedObject/<?=$AccountData->AccountID;?>/WorkOrders" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
				<?php } ?>
				</div>
			</div>

			<div class="col-md-12">
				<div class="clearfix"></div>
				
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
						<div class="txt-dark capitalize-font mt-20"><strong>Estimates</strong></div>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<a href="<?=SITE_URL?>Estimate/Actions/CreateEstimate/?Account=<?=$AccountData->AccountID?>" class="btn btn-primary btn-outline pull-right mb-10 mt-15"><i class="fa fa-plus"></i> New Estimate</a>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="table-wrap">
				<div class="table-responsive">
				<table id="EstimatesTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>ET # </th>
							<th>Estimate Name</th>
							<th>Expiration Date</th>
							<th>Status</th>
							<th>Grand Total</th>
							<th>Owner</th>
							<th>Created Date</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($Estimates as $key => $Estimate) { ?>
							<tr>
								<td><a href="<?=SITE_URL?>Estimate/Actions/EstimateDetails/<?=$Estimate->EstimateID?>"><?=$Estimate->EstimateNo?></a></td>
								<td><a href="<?=SITE_URL?>Estimate/Actions/EstimateDetails/<?=$Estimate->EstimateID?>"><?=$Estimate->EstimateName?></a></td>
								<td><?=$Estimate->ExpirationDate?></td>
								<td><?=$Estimate->Status?></td>
								<td><?=$Estimate->GrandTotal?></td>
								<td><a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$Estimate->Owner?>"><?=$Estimate->OwnerName?></a></td>
								<td><?=$Estimate->CreatedDate?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				</div>
				<?php if(count($Estimates) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Account/Actions/RelatedObject/<?=$AccountData->AccountID;?>/Estimates" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
				<?php } ?>
				</div>
			</div>

			<div class="col-md-12">
				<div class="clearfix"></div>
				
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
						<div class="txt-dark capitalize-font mt-20"><strong>Invoices</strong></div>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<a href="<?=SITE_URL?>Invoice/Actions/CreateInvoice?Account=<?=$AccountData->AccountID?>" class="btn btn-primary btn-outline pull-right mb-10 mt-15"><i class="fa fa-plus"></i> New Invoice</a>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="table-wrap">
				<div class="table-responsive">
				<table id="InvoicesTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>INV # </th>
							<th>Invoice Date</th>
							<th>Due Date</th>
							<th>Status</th>
							<th>Sub Total</th>
							<th>Total Price</th>
							<th>Work Order</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($Invoices as $key => $Invoice) { ?>
							<tr>
								<td><a href="<?=SITE_URL?>Invoice/Actions/InvoiceDetails/<?=$Invoice->InvoiceID?>"><?=$Invoice->InvoiceNo?></a></td>
								<td><?=$Invoice->InvoiceDate?></td>
								<td><?=$Invoice->DueDate?></td>
								<td><?=$Invoice->InvoiceStatus?></td>
								<td><?=$Invoice->SubTotal?></td>
								<td><?=$Invoice->TotalPrice?></td>
								<td><a href="<?=SITE_URL?>WorkOrder/Actions/WorkOrderDetails/<?=$Invoice->WorkOrder?>"><?=$Invoice->WorkOrderName?></a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				</div>
				<?php if(count($Invoices) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Account/Actions/RelatedObject/<?=$AccountData->AccountID;?>/Invoices" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
				<?php } ?>
				</div>
			</div>

			<div class="col-md-12">
				<div class="clearfix"></div>
				
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
						<div class="txt-dark capitalize-font mt-20"><strong>Files</strong></div>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<a href="#" data-toggle="modal" data-target="#CreateNewFile" class="btn btn-primary btn-outline pull-right mb-10 mt-15"><i class="fa fa-plus"></i> New File</a>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="table-wrap">
				<div class="table-responsive">
				<table id="FilesTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>File Name </th>
							<th>Content Type</th>
							<th>Subject</th>
							<th>Created Date</th>
							<th>Created By</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($Files as $key => $File) { ?>
							<tr>
								<td><?=$File->FileName?></td>
								<td><?=$File->ContentType?></td>
								<td><a href="<?=SITE_URL?>File/Actions/FileDetails/<?=$File->FileID?>"><?=$File->Subject?></a></td>
								<td><?=$File->CreatedDate?></td>
								<td><a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$File->CreatedBy?>"><?=$File->CreatedByName?></a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				</div>
				<?php if(count($Files) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Account/Actions/RelatedObject/<?=$AccountData->AccountID;?>/Files" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
				<?php } ?>
				</div>
			</div>
 
			<div class="col-md-12">
				<div class="clearfix"></div>
				
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
						<div class="txt-dark capitalize-font mt-20"><strong>Events</strong></div>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<a class="btn btn-primary btn-outline pull-right mb-10 mt-15"  href="<?=SITE_URL?>Event/Actions/CreateEvent/?Account=<?=$AccountData->AccountID;?>"><i class="fa fa-plus" ></i> New Event</a> <!-- data-toggle="modal" data-target="#CreateNewEvent" -->
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="table-wrap">
				<div class="table-responsive">
				<table id="EventsTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>Subject </th>
							<th>Start Date</th>
							<th>End Date</th>
							<th>Assigned To</th>
							<th>Name</th>
							<th>Created Date</th>
							<th>Created By</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($Events as $key => $Event) { ?>
							<tr>
								<td><a href="<?=SITE_URL?>Event/Actions/EventDetails/<?=$Event->EventID?>"><?=$Event->Subject?></a></td>
								<td><?=$Event->EventStartDate?></td>
								<td><?=$Event->EventEndDate?></td>
								<td><a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$Event->AssignedTo?>"><?=$Event->AssignedToName?></a></td>
								<td><a href="<?=SITE_URL?>Contact/Actions/ContactDetails/<?=$Event->Who?>"><?=$Event->ContactName?></a></td>
								<td><?=$Event->CreatedDate?></td>
								<td><a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$Event->CreatedBy?>"><?=$Event->CreatedByName?></a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				</div>
				<?php if(count($Events) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Account/Actions/RelatedObject/<?=$AccountData->AccountID;?>/Events" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
				<?php } ?>
				</div>
			</div>

			<div class="col-md-12">
				<div class="clearfix"></div>
				
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
						<div class="txt-dark capitalize-font mt-20"><strong>Tasks</strong></div>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<a href="<?=SITE_URL?>Task/Actions/CreateTask/?Account=<?=$AccountData->AccountID;?>" class="btn btn-primary btn-outline pull-right mb-10 mt-15" ><i class="fa fa-plus" ></i> New Task</a> <!-- data-toggle="modal" data-target="#CreateNewTask" -->
					</div>
				</div> 
				<div class="clearfix"></div>
				<div class="table-wrap">
				<div class="table-responsive">
				<table id="TasksTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>Subject </th>
							<th>Name </th>
							<th>Type </th>
							<th>Assigned To </th>
							<th>Date</th>
							<th>Status</th>
							<th>Priority</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($Tasks as $key => $Task) { ?>
							<tr>
								<td><a href="<?=SITE_URL?>Task/Actions/TaskDetails/<?=$Task->TaskID?>"><?=$Task->Subject?></a></td>
								<td><a href="<?=SITE_URL?>Contact/Actions/ContactDetails/<?=$Task->Who?>"><?=$Task->ContactName?></a></td>
								<td><?=$Task->TaskType?></td>
								<td><a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$Task->AssignedTo?>"><?=$Task->AssignedToName?></a></td>
								<td><?=$Task->DueDate?></td>
								<td><?=$Task->TaskStatus?></td>
								<td><?=$Task->Priority?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				</div>
				<?php if(count($Tasks) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Account/Actions/RelatedObject/<?=$AccountData->AccountID;?>/Tasks" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
				<?php } ?>
				</div>
			</div>

			<!-- <div class="col-md-12">
				<div class="clearfix"></div>
				
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
						<div class="txt-dark capitalize-font mt-20"><strong>Chemicals</strong></div>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<button type="button" class="btn btn-primary btn-outline pull-right mb-10 mt-15"><i class="fa fa-plus"></i> New Chemical</button>
					</div>
				</div> 
				<div class="clearfix"></div>
				<div class="table-wrap">
				<div class="table-responsive">
				<table id="TasksTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>Chemical # </th>
							<th>Product </th>
							<th>Amount </th>
							<th>Created By</th>
							<th>Created Date</th>
							<th>WorkOrder</th>
						</tr>
					</thead>
					<tbody>
						<?php /*foreach ($Chemicals as $key => $Chemical) { ?>
							<tr>
								<td><a href="#"><?=$Chemical->ChemicalNo?></a></td>
								<td><a href="#"><?=$Chemical->ProductName?></a></td>
								<td><i class="fa fa-usd"></i> <?=$Chemical->ApplicationAmount?></td>
								<td><a href="#"><?=$Chemical->CreatedBy?></a></td>
								<td><?=$Chemical->CreatedDate?></td>
								<td><a href="<?=SITE_URL?>WorkOrder/Actions/WorkOrderDetails/<?=$Chemical->WorkOrder?>"><?=$Chemical->Subject?></a></td> 
							</tr>
						<?php }*/ ?>
					</tbody>
				</table>
				</div>
				<?php /*if(count($Chemicals) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Account/Actions/RelatedObject/<?=$AccountData->AccountID;?>/Chemicals" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
				<?php }*/ ?>
				</div>
			</div> -->

			<!-- <div class="col-md-12">
				<div class="clearfix"></div>
				
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
						<div class="txt-dark capitalize-font mt-20"><strong>Installed Products</strong></div>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<button type="button" class="btn btn-primary btn-outline pull-right mb-10 mt-15"><i class="fa fa-plus"></i> New Product</button>
					</div>
				</div> 
				<div class="clearfix"></div>
				<div class="table-wrap">
				<div class="table-responsive">
				<table id="TasksTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>IP # </th>
							<th>Product </th>
							<th>Quantity </th>
							<th>Sales Price </th>
							<th>Date Purchased</th>
							<th>Created By</th>
							<th>Created Date</th>
						</tr>
					</thead>
					<tbody>
						<?php /*foreach ($Products as $key => $Product) { ?>
							<tr>
								<td><?=$Product->ProductCode?></td>
								<td><a href="#"><?=$Product->ProductName?></a></td>
								<td><?=$Product->DefaultQuantity?></td>
								<td><?=$Product->ListPrice?></td>
								<td><?=$Product->DatePurchased?></td>
								<td><a href="#"><?=$Product->CreatedBy?></a></td>
								<td><?=$Product->CreatedDate?></td>
							</tr>
						<?php }*/ ?>
					</tbody>
				</table>
				</div>
				<?php /*if(count($Products) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Account/Actions/RelatedObject/<?=$AccountData->AccountID;?>/Products" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
				<?php }*/ ?>
				</div>
			</div> -->

			</div>
		</div>
	</div>
</div>