<div class="panel panel-default card-view">
	<div class="panel-heading">
		<div class="pull-left">
			<h6 class="panel-title txt-dark">Activity Timeline</h6>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="panel-wrapper collapse in">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12 text-center">
						<div class="btn-group">
							<a href="<?=SITE_URL?>Task/Actions/CreateTask/?Account=<?=$AccountData->AccountID;?>" class="btn btn-default btn-outline">New Task</a> <!-- data-toggle="modal" data-target="#CreateNewTask" -->
							<a href="<?=SITE_URL?>Event/Actions/CreateEvent/?Account=<?=$AccountData->AccountID;?>" class="btn btn-default btn-outline">New Event</a> <!-- data-toggle="modal" data-target="#CreateNewEvent" -->
						</div>
				</div>

				<div class="col-md-12 mt-10">
					<div class="panel-heading">
						<div class="pull-left">
							<div class="txt-dark"><strong>Open Activities</strong></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
					<table class="table table-striped table-bordered mb-0">
							<thead>
								<tr>
									<th>Subject</th>
									<th width="20%">Type</th>
									<th width="35%">Date</th>
								</tr>
							</thead>
							<tbody>
							<?php $x = 1; 
							foreach ($OpenActivities as $key => $OpenActivity) { ?>
									<tr>
									<?php if($OpenActivity['Type'] == 'Task'){ ?>
											<td><a href="<?=SITE_URL?>Task/Actions/TaskDetails/<?=$OpenActivity['TaskID']?>"><?=$OpenActivity['Subject']?></a></td>
										<?php } else { ?>
											<td><a href="<?=SITE_URL?>Event/Actions/EventDetails/<?=$OpenActivity['EventID']?>"><?=$OpenActivity['Subject']?></a></td>
										<?php } ?>
										<td><?=$OpenActivity['Type']?></td>
										<td><?=$OpenActivity['Date']?></td>
									</tr>
							<?php $x++;
									if($x == 10)
										break;
									 } 

								for($y = $x;$y <= 3;$y++){?>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr> 
							<?php } ?>
							</tbody>
						</table>
					<div class="clearfix"></div>
					<?php if(count($OpenActivities) >= 10) { ?>
					<div class="pull-right pt-10">
						<a href="<?=SITE_URL?>Account/Actions/OpenActivities/<?=$AccountData->AccountID;?>" class="btn btn-default btn-outline">View All</a>
					</div>
					<?php } ?>
				</div>
			
				<div class="col-md-12 mt-10">
					<div class="panel-heading">
						<div class="pull-left">
							<div class="txt-dark"><strong>Completed Activities</strong></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<table class="table table-striped table-bordered mb-0">
							<thead>
								<tr>
									<th>Subject</th>
									<th width="20%">Type</th>
									<th width="35%">Date</th>
								</tr>
							</thead>
							<tbody> 
								<?php $x = 1; 
									foreach ($CompletedActivities as $key => $CompletedActivity) { ?>
									<tr>
										<?php if($CompletedActivity['Type'] == 'Task'){ ?>
											<td><a href="<?=SITE_URL?>Task/Actions/TaskDetails/<?=$CompletedActivity['TaskID']?>"><?=$CompletedActivity['Subject']?></a></td>
										<?php } else { ?>
											<td><a href="<?=SITE_URL?>Event/Actions/EventDetails/<?=$CompletedActivity['EventID']?>"><?=$CompletedActivity['Subject']?></a></td>
										<?php } ?>
										<td><?=$CompletedActivity['Type']?></td>
										<td><?=$CompletedActivity['Date']?></td>
									</tr>
							<?php $x++; 
									if($x == 10)
										break;
									} 

							for($y = $x;$y <= 3;$y++){?>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr> 
							<?php } ?>
							</tbody>
						</table>
						<div class="clearfix"></div>
						<?php if(count($CompletedActivities) >= 10) { ?>
						<div class="pull-right pt-10">
							<a href="<?=SITE_URL?>Account/Actions/CompletedActivities/<?=$AccountData->AccountID;?>" class="btn btn-default btn-outline">View All</a>
						</div>
						<?php } ?>
					</div>
			</div>
		</div>
	</div>
</div>