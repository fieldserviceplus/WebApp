
<?php $this->load->view('Template/HeaderView')?>
<style type="text/css">.dataTables_filter { display: none; }</style>

    <div class="container-fluid pt-25">

    <!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark"><?=$AccountData->AccountName;?></h6>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
				<div class="">
					<div class="pull-left">
						<div class="txt-dark pt-10">
							<a href="javascript:void();" OnClick="window.history.go(-1); return false;">Back to <?=$AccountData->AccountName;?></a> <strong> <?=$Flag?> Activities</strong>
						</div>
					</div>
					<div class="pull-right">
						<div class="btn-group">
							<button class="btn btn-default btn-outline" title="Refresh" ><i class="fa fa-refresh"></i></button>
							<button class="btn btn-default btn-outline" title="Export" id="Export-Excel"><i class="fa fa-file-excel-o"></i></button>
							<div class="input-group">
							<input id="myTable1globalfilter" class="form-control myTable1globalfilter" placeholder="Search" type="text">
							<span class="input-group-addon"><i class="zmdi zmdi-search"></i></span> 
						</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<hr class="light-grey-hr mb-0">
				</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							
								<div class="table-responsive">
									<table id="myTable1" class="table table-hover display pb-30" width="100%">
										<thead>
											<tr> 
												<th class="text-center">Subject</th>
												<th class="text-center">Type</th>
												<th class="text-center">Date</th>
											</tr>
										</thead>
										<tbody>
										<?php  foreach ($Activities as $Activity) { ?>
											<tr> 
												<?php if($Activity['Type'] == 'Task'){ ?>
													<td><a href="<?=SITE_URL?>Task/Actions/TaskDetails/<?=$Activity['TaskID']?>"><?=$Activity['Subject']?></a></td>
												<?php } else { ?>
													<td><a href="<?=SITE_URL?>Event/Actions/EventDetails/<?=$Activity['EventID']?>"><?=$Activity['Subject']?></a></td>
												<?php } ?>
												<td class="text-center"><?=$Activity['Type']?></td>
												<td class="text-center"><?=$Activity['Date']?></td>
											</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
		</div>
		<!-- /Row -->
	</div>	

<!-- Start Modal -->

<?php $this->load->view('Template/FooterView')?>

<!-- Multiselect JavaScript -->
<!-- <script src="<?=SITE_URL?>vendors/bower_components/multiselect/js/jquery.multi-select.js"></script> -->
<!-- Bootstrap Switch JavaScript -->
<!-- <script src="<?=SITE_URL?>vendors/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script> -->

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript" src="<?=SITE_URL?>dist/js/multiselect.min.js"></script>


<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('.buttons-excel').hide();
	$("#Export-Excel").on("click", function() {
	    $( '.buttons-excel' ).click();
	});
});</script>
	