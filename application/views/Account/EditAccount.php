<?php $this->load->view('Template/HeaderView'); ?>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_JS_KEY?>&libraries=places"></script>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10 pb-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['Account']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['Account']['title']?></span>
									<span class="panel-title txt-dark"><?=$AccountData->AccountName;?></span>
								</div>
							</div>
						</div>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div  class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="col-lg-7 col-md-5 col-sm-4 col-xs-12">
							<ul role="tablist" class="nav nav-pills" id="myTabs_13">
								<li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_13" href="#home_13">Detail</a></li>
								<li role="presentation" class=""><a data-toggle="tab" id="profile_tab_13" role="tab" href="#profile_13" aria-expanded="false">Related</a></li>
							</ul>
						</div>

						<div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">

							<div class="btn-group pull-right">
									<!-- <div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li>
												<a><span><strong>Add New Actions</strong></span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>Contact/Actions/CreateContact/?Account=<?=$AccountData->AccountID?>"><span>New Contact</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>WorkOrder/Actions/CreateWorkOrder/?Account=<?=$AccountData->AccountID?>" ><span>New Work Order</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>Estimate/Actions/CreateEstimate?Account=<?=$AccountData->AccountID?>"><span>New Estimate</span></a>
											</li>
											<li>
												<a href="#"><span>New Invoice</span></a>
											</li>
											<li>  
												<a href="#" data-toggle="modal" data-target="#CreateNewFile"><span>New File</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>Task/Actions/CreateTask/?Account=<?=$AccountData->AccountID;?>"><span>New Task</span></a> 
											</li>
											<li>
												<a href="<?=SITE_URL?>Event/Actions/CreateEvent/?Account=<?=$AccountData->AccountID;?>"><span>New Event</span></a> 
											</li>
											<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
												<a><span><strong>Account Actions</strong></span></a>
											</li>
											<li> 
												<a href="<?=SITE_URL;?>Account/Actions/CreateAccount/<?=$AccountData->AccountID?>/"><span>Copy Account</span></a>
											</li>
											<li>
												<a href="#" onclick="DeleteAccount();"><span>Delete Account</span></a>
											</li>
										</ul>
									</div> -->
								</div>
								<div class="btn-group pull-right">
									<a href="<?=SITE_URL;?>Account/Actions/AccountDetails/<?=$AccountData->AccountID?>" class="btn btn-default">Cancel</a>
									<a href="#" class="btn btn-primary" id="EditAccountLink">Save</a>

									<!-- <li class="custom_li_btn"><a href="<?=SITE_URL;?>Account/Actions/AccountDetails/<?=$AccountData->AccountID?>" class="btn btn-default">Cancel</a></li>
									<li><button class="btn btn-primary" id="EditAccountLink">Save</button></li> -->
								</div>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
			 	<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Account Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>  -->
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Account/Actions/EditAccount/<?=$AccountData->AccountID?>" class="form-horizontal" id="EditAccount" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font">
													<strong>Account Information</strong>
													<span class="pull-right" style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong>
													</span>
												</div>
												
												<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-4">Assigned To<span class="required-sign">*</span></label>
														<div class="col-md-8">
														<select name="AssignedTo" id="AssignedTo" class="form-control select2">
														<option value="">Select Owner</option>
														<?php $Users = $this->ActionsModel->GetUsers();
															foreach ($Users as $User) { 
																if($User->UserID == $AccountData->AssignedTo){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else { ?>
																	<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('AssignedTo');  ?>
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-4">Account Name<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="AccountName" id="AccountName"  value="<?=$AccountData->AccountName;?>">
															<?php echo form_error('AccountName');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Primary Contact</label>
														<div class="col-md-8">

														<?php $AllContacts = $this->ActionsModel->GetContacts($AccountData->AccountID); ?>
															<select class="form-control select2" name="PrimaryContact" id="PrimaryContact">
															<option value="">Select Contact</option>
															<?php foreach ($AllContacts as $Contact) { 
																if($Contact->ContactID == $AccountData->PrimaryContact){ ?>
																	<option value="<?=$Contact->ContactID?>" selected="selected"><?=$Contact->FirstName.' '.$Contact->LastName?></option>
																<?php } else {?>
																	<option value="<?=$Contact->ContactID?>"><?=$Contact->FirstName.' '.$Contact->LastName?></option>
																<?php } ?>
															<?php } ?>
															<?php echo form_error('PrimaryContact');  ?>
														</select>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Phone #<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="PhoneNo" id="PhoneNo" value="<?=$AccountData->PhoneNo;?>">
															<?php echo form_error('PhoneNo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Preferred Technician</label>
														<div class="col-md-8">
														<select name="PreferredTechnician" id="PreferredTechnician" class="form-control select2">
														<option value="">Select Technician</option>
														<?php $Users = $this->ActionsModel->GetUsers();
															foreach ($Users as $User) { 
																if($User->UserID == $AccountData->PreferredTechnician){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else { ?>
																	<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('PreferredTechnician');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Access Notes</label>
														<div class="col-md-8">
															<input type="text" class="form-control" id="AccessNotes" name="AccessNotes" value="<?=$AccountData->AccessNotes;?>">
															<?php echo form_error('AccessNotes');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Notes</label>
														<div class="col-md-8">
															<textarea class="form-control" name="Notes" id="Notes"><?=$AccountData->Notes;?></textarea>
															<?php echo form_error('Notes');  ?>
														</div>
													</div>
												</div>
												

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-4">Account No</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$AccountData->AccountNo;?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Account Type<span class="required-sign">*</span></label>
														<div class="col-md-8">

														<?php $AccountTypes = $this->ActionsModel->GetAccountTypes(); ?>
															<select class="form-control " name="AccountType" id="AccountType">
															<option value="">Select Account Type</option>
															<?php foreach ($AccountTypes as $AccountType) { 
																if($AccountType->AccountTypeID == $AccountData->AccountType){ ?>
																	<option value="<?=$AccountType->AccountTypeID?>" selected="selected"><?=$AccountType->AccountType?></option>
																<?php } else { ?>
																	<option value="<?=$AccountType->AccountTypeID?>"><?=$AccountType->AccountType?></option>
																<?php } ?>
															<?php } ?>
															<?php echo form_error('AccountType');  ?>
														</select>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Is Active</label>
														<div class="col-md-8">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="IsActive" id="IsActive" value="1" <?=$AccountType->IsActive=1?"checked=''":"";?>   >
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Last Activity</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="LastActivityDate" id="LastActivityDate" value="<?=$AccountData->LastActivityDate;?>" readonly="">
															<?php echo form_error('LastActivityDate');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Last Service Date</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="LastServiceDate" id="LastServiceDate" value="<?=$AccountData->LastServiceDate;?>" readonly="">
															<?php echo form_error('LastServiceDate');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Pop Up Reminder</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="PopUpReminder" id="PopUpReminder" value="<?=$AccountData->PopUpReminder;?>">
															<?php echo form_error('PopUpReminder');  ?>
														</div>
													</div>

													<!-- <div class="form-group">
														<label class="control-label col-md-4">Website</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Website" id="Website" value="<?=$AccountData->Website;?>">
															<?php //echo form_error('Website');  ?>
														</div>
													</div> -->


												</div>
											</div>

											<div class="col-md-12">

												<div class="txt-dark capitalize-font pt-10"><strong>Address Information</strong></div>
												<hr class="light-grey-hr">
												

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-4">Billing Address</label>
														<div class="col-md-8">
															<!-- <div class="input-group"> -->
																<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-search"></i>Search Address</button>
																<!-- <div class="input-group-addon"><a href="#" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-map-marker"></i></a></div> -->
															<!-- </div> -->
														</div>
													</div>

													<div class="form-group">
														<label class="col-md-4 control-label">Billing Street<span class="required-sign">*</span></label>
														<div class="col-md-8">
															 <input type="text" class="form-control" name="BillingAddress" id="BillingAddress" placeholder="" value="<?=$AccountData->BillingAddress;?>">
															<?php echo form_error('BillingAddress');  ?>
															<input type="hidden" name="BillingLatitude" id="BillingLatitude" value="<?=$AccountData->BillingLatitude;?>">
															<input type="hidden" name="BillingLongitude" id="BillingLongitude" value="<?=$AccountData->BillingLongitude;?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Billing City<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="BillingCity" id="BillingCity" value="<?=$AccountData->BillingCity;?>">
															<?php echo form_error('BillingCity');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Billing State<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="BillingState" id="BillingState" value="<?=$AccountData->BillingState;?>">
															<?php echo form_error('BillingState');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-4">Billing Country<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="BillingCountry" id="BillingCountry" value="<?=$AccountData->BillingCountry;?>">
															<?php echo form_error('BillingCountry');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Billing Postal Code<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="BillingPostalCode" id="BillingPostalCode" value="<?=$AccountData->BillingPostalCode;?>">
															<?php echo form_error('BillingPostalCode');  ?>
														</div>
													</div>
												</div>

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-4">Shipping Address</label>
														<div class="col-md-8">
															<!-- <div class="input-group"> -->
																<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal2"><i class="fa fa-search"></i>Search Address</button>
																<!-- <div class="input-group-addon"><a href="#" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-map-marker"></i></a></div> -->
															<!-- </div> -->
														</div>
													</div>
												
													<div class="form-group">
														<label class="col-md-4 control-label">Shipping Street<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<!-- <div class="input-group"> -->
																<input type="text" class="form-control" name="ShippingAddress" id="ShippingAddress" placeholder="" value="<?=$AccountData->ShippingAddress;?>">
																<!-- <div class="input-group-addon"><a href="#" data-toggle="modal" data-target="#location-picker-modal2"><i class="fa fa-map-marker"></i></a></div> -->
															<!-- </div> -->
															<?php echo form_error('ShippingAddress');  ?>
															<input type="hidden" name="ShippingLatitude" id="ShippingLatitude" value="<?=$AccountData->ShippingLatitude;?>">
															<input type="hidden" name="ShippingLongitude" id="ShippingLongitude" value="<?=$AccountData->ShippingLongitude;?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Shipping City<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="ShippingCity" id="ShippingCity" value="<?=$AccountData->ShippingCity;?>">
															<?php echo form_error('ShippingCity');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Shipping State<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="ShippingState" id="ShippingState" value="<?=$AccountData->ShippingState;?>">
															<?php echo form_error('ShippingState');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-4">Shipping Country<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="ShippingCountry" id="ShippingCountry" value="<?=$AccountData->ShippingCountry;?>">
															<?php echo form_error('ShippingCountry');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Shipping Postal Code<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="ShippingPostalCode" id="ShippingPostalCode" value="<?=$AccountData->ShippingPostalCode;?>">
															<?php echo form_error('ShippingPostalCode');  ?>
														</div>
													</div>
													
												</div>

											</div>

											<!-- Start Custom Fields Row -->
											<?php $this->load->view('Setup/Customize/Fields/CustomFieldsForm', array('Object' => 'Account', 'ObjectID' => $AccountData->AccountID)); ?>
											<!-- End Custom Fields Row -->

											<div class="col-md-12">

												<div class="txt-dark capitalize-font pt-10"><strong>System Information</strong></div>
												<hr class="light-grey-hr">
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-4">Create Date</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$AccountData->CreatedDate?>" readonly="">
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-4">Created By</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$AccountData->CreatedByName?>" readonly="">
														</div>
													</div>
												</div>
												<div class="col-md-6">

												   <div class="form-group">
														<label class="control-label col-md-4">Last Modified Date</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$AccountData->LastModifiedDate?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Last Modified By</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$AccountData->LastModifiedByName?>" readonly="">
														</div>
													</div>
												</div>
											</div>

										</div>
											<!-- /Row -->
									</div>

									<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="EditAccount" id="EditAccountBtn">Save</button>
															<a href="<?=SITE_URL;?>Account/Actions/AccountDetails/<?=$AccountData->AccountID?>" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			</div>
			<div id="profile_13" class="tab-pane fade" role="tabpanel">
				
				<?php $this->load->view('Account/Include/RelatedInformation'); ?>
				
			</div>
		</div>
		
		</div>

		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
			<?php $this->load->view('Account/Include/ActivityTimeline'); ?>
		</div>

	</div>
	<!-- /Row -->

</div>


<!-- Start Modal -->
<?php $this->load->view('Account/Modal/CreateTaskView'); ?>

<?php $this->load->view('Account/Modal/CreateEventView'); ?>

<?php $this->load->view('Account/Modal/RecurringTaskView'); ?>

<!-- modal -->
 <div class="modal fade" id="location-picker-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Address Search</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" action="javascript:void(0)">
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-2">Location</label>
								<div class="col-md-10">
									<input id="BillingLocation" type="text" name="" class="form-control" placeholder="Enter Address" autocomplete="false">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="Billingpopupmap" style="height: 400px;"></div><br />
				</form>
				
			</div>
		</div>
	</div>
</div> 

 <div class="modal fade" id="location-picker-modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Address Search</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" action="javascript:void(0)">
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-2">Location</label>
								<div class="col-md-10">
									<input id="ShippingLocation" type="text" name="" class="form-control" placeholder="Enter Address" autocomplete="false">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="Shippingpopupmap" style="height: 400px;"></div><br />
				
				</form>
				
			</div>
		</div>
	</div>
</div> 

<!-- End Modal -->

<?php $this->load->view('Template/FooterView')?>

<?php $this->load->view('Common/Modal/CreateNewFile', array('data' => array('RelatedTo' => 'Account', 'What' => $AccountData->AccountID, 'AssignedTo' => $AccountData->AssignedTo))); ?>

<!-- Data table JavaScript -->
<!-- <script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>
 -->
<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<!-- Us for common object  -->
<script src="<?=SITE_URL?>dist/js/common.js"></script>

<!-- Bootstrap Touchspin JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script type="text/javascript">
	$(".vertical-spin").TouchSpin({
		min: 1,
        max: 150,
		verticalbuttons: true,
		verticalupclass: 'ti-plus',
		verticaldownclass: 'ti-minus'
	});
	var vspinTrue = $(".vertical-spin").TouchSpin({
		verticalbuttons: true
	});
	if (vspinTrue) {
		$('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
	}

	$(".vertical-spin-occurrence").TouchSpin({
		min: 1,
		verticalbuttons: true,
		verticalupclass: 'ti-plus',
		verticaldownclass: 'ti-minus'
	});
</script>
<!-- Form Advance Init JavaScript -->

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>
		
<script type="text/javascript">

$(document).ready(function () {

    $('#EditAccount').validate({ 
        rules: {
            AssignedTo: {
                required: true
            },
            AccountName: {
                required: true
            },
           /* PreferredTechnician: {
                required: true
            },*/
            PhoneNo: {
                required: true
            },
            BillingCountry: {
                required: true
            },
            BillingState: {
                required: true
            },
            BillingCity: {
                required: true
            },
           /* BillingStreet: {
                required: true
            },*/
            BillingAddress: {
                required: true
            },
            BillingPostalCode: {
                required: true
            },
          /*  AccessNotes: {
                required: true
            },
            PopUpReminder: {
                required: true
            },
            Notes: {
                required: true
            },*/
            AccountType: {
                required: true
            },
            /*Website: {
                required: true
            },*/
           /* LastActivityDate: {
                required: true
            },
            LastServiceDate: {
                required: true
            },*/
           /* PrimaryContact: {
                required: true
            },*/
            Country: {
                required: true
            },
            ShippingState: {
                required: true
            },
            ShippingCity: {
                required: true
            },
          /*  ShippingStreet: {
                required: true
            },*/
            ShippingAddress: {
                required: true
            },
            ShippingPostalCode: {
                required: true
            },
            ShippingCountry: {
                required: true
            },
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });


    $('.showdatetimepicker').datetimepicker({
		useCurrent: true,
		icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
	});

    $('.timepicker').datetimepicker({
			format: 'LT',
			useCurrent: false,
			icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
	}).data("DateTimePicker").date(moment());

     $('#NewTaskForm').validate({ 
        rules: {
            AssignedTo: {
                required: true
            },
            Subject: {
                required: true
            },
            TaskType: {
                required: true
            },
            TaskStatus: {
                required: true
            },
            TaskPriority: {
                required: true
            },
            DueDate: {
                required: true
            },
            RelatedTo: {
                required: true
            },
            What: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    $('#NewEventForm').validate({ 
        rules: {
            AssignedTo: {
                required: true
            },
            Subject: {
                required: true
            },
            EventType: {
                required: true
            },
            EventPriority: {
                required: true
            },
            EventStartDate: {
                required: true
            },
            EventEndDate: {
                required: true
            },
            RelatedTo: {
                required: true
            },
            What: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

}); 

/* Recurring Setting */
function RepeatsChange(Repeats){
	if(Repeats == 'Daily'){ 
			
		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 150});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('day on');
	} else if(Repeats == 'Weekly'){ 

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 104});
	
		$( "#days-cb" ).show();
		$("input[name*='RepeatOn[]']").attr("required",true);
		$('.interval_every_txt').html('week on');
	} else if(Repeats == 'Monthly'){

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 60});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('month on');
	} else if(Repeats == 'Yearly'){

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 10});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('year on');
	} else if(Repeats == 'Periodically'){

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 150});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('periodical on');
	}
}
$("input[name='Ends']").change(function() { 
	if($("input[name='Ends']:checked").val()=='On') {
       $("#EndsOnDate").attr("required",true);
 	} else {
 		$("#EndsOnDate").attr("required",false);
 	}
}); 

$('#IsRecurringTask').change(function() { 
     if(this.checked) {
        $('#RecurringTaskModal').modal({
		    backdrop: 'static',
		    keyboard: false  // to prevent closing with Esc button (if you want this too)
		}); 
         $("#RecurringFormType").remove();
         $('#RecurringForm').append('<input type="hidden" id="RecurringFormType" name="RecurringFormType" value="Task">'); 
         
     } else {

     	if($('.WORecurrenceIDForTask').val() != ''){

     		var WORecurrenceID = $('.WORecurrenceIDForTask').val();
	     	$.ajax({
	            url: '<?=SITE_URL?>Account/Actions/removeRecurrenceSetting/',
	            data: { WORecurrenceID:WORecurrenceID },
	            method: "post",
	            dataType: 'html',
	            success: function (result) {
	                if (result != '') {
	                   $('.WORecurrenceIDForTask').val(''); 
	                   return true;  	
	                } else {
	                    alert("Something went wrong.Please try again!");
	                    return false;
	                }
	            }
	        });	
	     }
     }
});

$('#IsRecurringEvent').change(function() { 
     if(this.checked) {
        $('#RecurringTaskModal').modal({
		    backdrop: 'static',
		    keyboard: false  // to prevent closing with Esc button (if you want this too)
		}); 
        $("#RecurringFormType").remove();
        $('#RecurringForm').append('<input type="hidden" id="RecurringFormType" name="RecurringFormType" value="Event">'); 
     } else {

     	if($('.WORecurrenceIDForEvent').val() != ''){

     		var WORecurrenceID = $('.WORecurrenceIDForEvent').val();
	     	$.ajax({
	            url: '<?=SITE_URL?>Account/Actions/removeRecurrenceSetting/',
	            data: { WORecurrenceID:WORecurrenceID },
	            method: "post",
	            dataType: 'html',
	            success: function (result) {
	                if (result != '') {
	                   $('.WORecurrenceIDForEvent').val(''); 
	                   return true;  	
	                } else {
	                    alert("Something went wrong.Please try again!");
	                    return false;
	                }
	            }
	        });	
	     }
     }
});

$('#RecurringForm').validate({
	errorPlacement: function(){
	    return false;
	},
	/* submitHandler: function (form) { 
	    return false; 
	},*/
	highlight: function(element) {
		$(element).closest('.form-group').addClass('has-error');
	},
	unhighlight: function(element) {
	    $(element).closest('.form-group').removeClass('has-error');
	}
});	

$(document).on('click', "#RecurrenceSave", function () {
    if ($("#RecurringForm").valid()) {	
        $.ajax({
            url: '<?=SITE_URL?>WorkOrder/Actions/saveRecurrenceSetting/',
            data: $("#RecurringForm").serialize(),
            method: "post",
            dataType: 'html',
            success: function (result) {
                if (result != '') {
                	if($("#RecurringFormType").val() == 'Event')
                   		$('.WORecurrenceIDForEvent').val(result); 
                   	else if($("#RecurringFormType").val() == 'Task')
                   		$('.WORecurrenceIDForTask').val(result); 
                   	$('#RecurringTaskModal').modal('hide'); 
                   	return true;  	
                } else {
                    alert("Something went wrong.Please try again!");
                    return false;
                }
            }
        });
    }
    return false;
});

$(document).on('click', "#RecurrenceClose", function () { 
	if($("#RecurringFormType").val() == 'Event')
   		$('#IsRecurringEvent').prop('checked', false);
   	else if($("#RecurringFormType").val() == 'Task')
   		$('#IsRecurringTask').prop('checked', false);
});

$(document).on('click', "#EditAccountLink", function () {
	$('#EditAccountBtn').click(); 
});
</script>
<script src="<?=SITE_URL?>dist/js/location-picker.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	
	<?php if(!empty($AccountData->BillingLatitude) && !empty($AccountData->BillingLongitude)){ ?>
		set_location_in_map("Billing", <?=$AccountData->BillingLatitude;?>, <?=$AccountData->BillingLongitude;?>);
	<?php } else { ?>
		set_location_in_map("Billing");
	<?php } ?>

	<?php if(!empty($AccountData->ShippingLatitude) && !empty($AccountData->ShippingLongitude)){ ?>
		set_location_in_map("Shipping", <?=$AccountData->ShippingLatitude;?>, <?=$AccountData->ShippingLongitude;?>);
	<?php } else { ?>
		set_location_in_map("Shipping");
	<?php } ?>
});


function DeleteAccount(AccountID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#5fc55f",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>Account/Actions/DeleteAccount/<?=$AccountData->AccountID;?>/";
		}, 500);
    });
	return false;
}
</script>




