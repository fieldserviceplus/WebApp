
<div class="modal fade" id="CreateNewFile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Create New File</h5>
			</div>
			<form class="form-horizontal" id="NewFileForm" method="post" action="<?=SITE_URL;?>Common/Actions/CreateNewFile" enctype="multipart/form-data">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						 
						<input type="hidden" name="RelatedTo" value="<?=$data['RelatedTo']?>">
						<input type="hidden" name="What" value="<?=$data['What']?>">
						<input type="hidden" name="AssignedTo" value="<?=$data['AssignedTo']?>">

						<div class="form-group">
							<label class="control-label col-md-3">Upload<span class="required-sign">*</span></label>
							<div class="col-md-9">
								<input type="file" name="Upload[]" id="Upload" multiple="">
							</div>
						</div>

					    <div class="form-group">
							<label class="control-label col-md-3">Subject<span class="required-sign">*</span></label>
							<div class="col-md-9">
								<input type="text" name="Subject" value="" class="form-control">
							</div>
						</div>
					    <div class="form-group">
							<label class="control-label col-md-3">Description</label>
							<div class="col-md-9">
								<textarea name="Description" class="form-control"></textarea>
							</div>
						</div>
					</div>
				</div>
				 
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="CreateFile">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>