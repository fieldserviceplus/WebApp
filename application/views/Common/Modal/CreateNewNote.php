
<!-- modal --> 
<div class="modal fade" id="CreateNewNote" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Create Note</h5>
			</div>
			<form class="form-horizontal" id="CreateNoteForm" method="post" action="<?=SITE_URL;?>Common/Actions/CreateNewNote">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">

							<input type="hidden" name="RelatedTo" value="<?=$data['RelatedTo']?>">
							<input type="hidden" name="What" value="<?=$data['What']?>">
							<input type="hidden" name="AssignedTo" value="<?=$data['AssignedTo']?>">

							<div class="form-group">
								<label class="control-label col-md-3">Subject</label>
								<div class="col-md-9">
									<input type="text" name="Subject" class="form-control">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Body</label>
								<div class="col-md-9">
									<textarea class="form-control" name="Body" style="resize: vertical;"></textarea>
								</div>
							</div>
					
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer" style="text-align: center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="CreateNote">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>

