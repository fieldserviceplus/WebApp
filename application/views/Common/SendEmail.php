<?php $this->load->view('Template/HeaderView'); ?>
<!-- Bootstrap Dropzone CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css"/>
<div class="container-fluid pt-25">
	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Send Email</h6>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Common/Actions/SendEmail/<?=$data['RelatedTo'];?>/<?=$data['What'];?>" class="form-horizontal" id="SendEmailForm" method="post" enctype="multipart/form-data">
										<div class="form-body">

											<!-- /Row -->
											<div class="row">
												<div class="col-md-12">

													<div class="form-group">
														<label class="control-label col-md-2">From<span class="required-sign">*</span></label>
														<div class="col-md-3">
															<input type="text" name="FromName" class="form-control" value="<?=$this->session->userdata('FirstName').' '.$this->session->userdata('LastName')?>">
														</div>
														<div class="col-md-4">
															<select name="FromEmailID" id="FromEmailID" class="form-control select2">
																<option value="">Select Email</option>
																<?php $Users = MY_Model::getAllUsers();
																	foreach ($Users as $User) { 
																		if($this->LoginUserID == $User->UserID){?>
																			<option value="<?=$User->Email?>" selected=""><?=$User->Email?></option>
																		<?php } else { ?>
																			<option value="<?=$User->Email?>"><?=$User->Email?></option>
																		<?php } ?>
																<?php } ?>
															</select>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-2">To<span class="required-sign">*</span></label>
														<div class="col-md-7">
															<input type="text" name="ToEmailID" class="form-control" value="<?=isset($data['ObjData']->Email)?$data['ObjData']->Email:"";?>">
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-2"></label>
														<label class="control-label col-md-7"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_2" href="#collapse_cc" aria-expanded="false">CC</a> &nbsp;<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_2" href="#collapse_bcc" aria-expanded="false">BCC</a></label>
													</div>
													<div id="collapse_cc" class="form-group panel-collapse collapse" role="tabpanel" aria-expanded="false">
														<label class="control-label col-md-2">CC</label>
														<div class="col-md-7">
															<input type="text" name="CCEmailID" class="form-control">
														</div>
													</div>
													<div id="collapse_bcc" class="form-group panel-collapse collapse" role="tabpanel" aria-expanded="false">
														<label class="control-label col-md-2">BCC</label>
														<div class="col-md-7">
															<input type="text" name="BCCEmailID" class="form-control">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-2">Choose Template</label>
														<div class="col-md-7">
															<select name="EmailTemplate" id="EmailTemplate" class="form-control" onchange="setTemplateData(this.value);">
																<option value="">Select Template</option>
																<?php $Templates = MY_Model::GetEmailTemplates();
																	foreach ($Templates as $Template) { ?>
																		<option value="<?=$Template->EmailTemplateID?>"><?=$Template->Title?></option>
																<?php } ?>
															</select>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-2">Subject<span class="required-sign">*</span></label>
														<div class="col-md-7">
															<input type="text" name="EmailSubject" class="form-control" id="EmailSubject">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-2">Body</label>
														<div class="col-md-7">
															<textarea class="form-control" id="CKEditor1" name="EmailBody"></textarea>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-2">Attach File</label>
														<div class="col-md-2">	
															<img id="ImgFileUpload" alt="Select Files" title="Select Files" src="<?=SITE_URL?>img/attach file icon.png" style="cursor: pointer; width: 33px;" />
															<input type="file" id="EmailAttachment" name="EmailAttachment[]" multiple="" style="display: none" accept=".jpg,.jpeg,.png,.doc,.pptx,.xlsx,.docx,.zip"/>
														</div>
														<div class="col-md-8">
															<span style="font-style:italic;color: #f33923;">Max upload file size : 25MB <br />Allowed file extension : jpg, jpeg, png, doc, pptx, xlsx, docx, zip</span>
														</div>
														<div class="col-md-offset-2 col-md-10">
															<ul id="output">
															</ul>
														</div>
													</div>
												</div>
											</div>
											<!-- /Row -->
										</div>
										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary mr-10" name="SendEmail" id="SendEmail">Send</button>
															<button type="button" class="btn btn-default" onclick="window.history.go(-1); return false;">Cancel</button>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->


</div>

<?php $this->load->view('Template/FooterView')?>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<script type="text/javascript">
	
$(document).ready(function () {

    $('#SendEmailForm').validate({ 
        rules: {
            FromName: {
                required: true
            },
            FromEmailID: {
                required: true
            },
            ToEmailID: {
                required: true
            },
            EmailSubject: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
});
</script>
<!-- For Send Email Madal Start --> 
<!-- CKEditor 4 -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/ckeditor4/ckeditor.js"></script>
<script>
	CKEDITOR.replace( 'CKEditor1' );
</script>

<!-- For image upload -->
<script type="text/javascript">
$(function () {
    var fileupload = $("#EmailAttachment");
    var image = $("#ImgFileUpload");
    image.click(function () {
        fileupload.click();
    });
    fileupload.change(function (e) {

    	$('#output').html('');
        var numFiles = e.currentTarget.files.length;
	    for (i=0;i<numFiles;i++){

	    	var fileSize = parseInt(e.currentTarget.files[i].size, 10)/1024;
	        filesize = Math.round(fileSize);

	    	if(fileSize > 25600){
		       alert("File is too big!");
		       $('#EmailAttachment').val('');
		       return false;
		    } 
	        
	        $('<li />').text(e.currentTarget.files[i].name).appendTo($('#output'));
	        $('<span />').addClass('filesize').text(' (' + filesize + 'kb)').appendTo($('#output li:last'));
		    
	    }
    });
});

function setTemplateData(EmailTemplateID)
{    
    $.ajax({
        url: '<?=SITE_URL?>Common/Actions/GetTemplateDetails/'+EmailTemplateID,
        type: "POST",
        data: { EmailTemplateID : EmailTemplateID },
        dataType: 'json',
        async: true,
        success: function(data)
        {  
            $("#EmailSubject").val(data.Title);
            $("#CKEditor1").val(data.Body);

            CKEDITOR.instances.CKEditor1.setData(data.Body);
        }
    });
}
</script>







