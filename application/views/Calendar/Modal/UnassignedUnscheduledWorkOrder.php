<div class="col-lg-1 col-md-1 col-sm-2 col-xs-3"> 
<a href="javascript:void(0);" class="slider-arrow show">&laquo;</a>		
	<div class="row right-slide-panel">
		<div class="col-lg-12 col-sm-12">
			<div class="panel panel-default card-view">
			
		<div class="panel-wrapper collapse in">
		<div  class="pills-struct">
			<ul role="tablist" class="nav nav-pills" id="myTabs_6">
				<li class="active" role="presentation" style="width:48%;"><a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_6" href="#home_6" >Unassigned</a></li>
				<li role="presentation" class="" style="width:48%;"><a  data-toggle="tab" id="profile_tab_6" role="tab" href="#profile_6" aria-expanded="false" >Unscheduled</a></li>
			</ul> 
			<div class="tab-content" id="myTabContent_6">
				<div id="home_6" class="tab-pane fade active in" role="tabpanel">
				<?php if(!empty($UnassignedUnscheduled['Unassigned'])){
				foreach ($UnassignedUnscheduled['Unassigned'] as $key => $value) {?>
						<hr class="light-grey-hr row mt-10 mb-10">
						<span class="pull-left inline-block capitalize-font txt-dark">
							<div class="calendar-event" data-workorderid="<?=$value->WorkOrderID?>" data-workorderflag="Unassigned" data-unassigned="1"><?=$value->WorkOrderNo?> <?=$value->Subject?> <?=$value->City?>, <?=$value->State?></div>
						</span>
						<div class="clearfix"></div>
				<?php }
				} else { ?>
						<hr class="light-grey-hr row mt-10 mb-10">
						<span class="pull-left inline-block capitalize-font txt-dark mb-10">
							<div>No unassigned work order available.</div>
						</span>
						<div class="clearfix"></div>
					<?php } ?>
				</div>
				<div id="profile_6" class="tab-pane fade" role="tabpanel">
					<?php if(!empty($UnassignedUnscheduled['Unscheduled'])){
					foreach ($UnassignedUnscheduled['Unscheduled'] as $key => $value) {?>
						<hr class="light-grey-hr row mt-10 mb-10">
						<span class="pull-left inline-block capitalize-font txt-dark">
							<div class="calendar-event" data-workorderid="<?=$value->WorkOrderID?>" data-workorderflag="Unscheduled" data-unscheduled="1"><?=$value->WorkOrderNo?> <?=$value->Subject?> <?=$value->City?>, <?=$value->State?></div>
						</span>
						<div class="clearfix"></div>
				<?php }
				} else { ?>
					<hr class="light-grey-hr row mt-10 mb-10">
					<span class="pull-left inline-block capitalize-font txt-dark mb-10">
						<div >No unscheduled work order available.</div>
					</span>
					<div class="clearfix"></div>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
	</div>
</div>