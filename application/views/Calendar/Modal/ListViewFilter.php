<div class="modal fade" id="list-view-filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h5 class="modal-title" id="exampleModalLabel1">Filters</h5>
				</div>
				<div class="modal-body">
					<!-- <form class="form-horizontal"> -->
						<div class="col-md-12">
							<div class="col-md-2">
									<label class="control-label">Show Tasks</label>
							</div>
							<div class="col-md-10">
							    <div class="form-group">
									<div class="checkbox checkbox-primary">
										<input id="ShowTasks" checked="" type="checkbox" name="ShowTasks" value="1">
										<label for="ShowTasks"></label>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<hr class="light-grey-hr row mt-10 mb-10">
						</div>

						<div class="col-md-12">
							<div class="txt-dark capitalize-font pt-10"><strong>Work Order</strong></div>
							<hr class="light-grey-hr row mt-10 mb-10">
						</div>
						<div class="col-md-12">
							<div class="col-md-1">
								<div class="form-group">
									<label class="control-label">Type</label>
								</div>
							</div>
							<div class="col-md-11">
							    <div class="form-group">
									<?php foreach ($WOType as $key => $value) { ?>
										<div class="col-md-4">
											<div class="checkbox checkbox-primary">
												<input id="TaskTypeCheckbox<?=$key?>" checked="" type="checkbox" name="WorkOrderType[]" value="<?=$value->WorkOrderTypeID?>">
												<label for="TaskTypeCheckbox<?=$key?>"> <?=$value->WorkOrderType?> </label>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
						<div class="col-md-12">
						<hr class="light-grey-hr row mt-10 mb-10">
							<div class="col-md-1">
								<div class="form-group">
									<label class="control-label">Status</label>
								</div>
							</div>
							<div class="col-md-11">
								 <div class="form-group">
									<?php foreach ($WOStatus as $key => $value) { ?>
										<div class="col-md-4">
											<div class="checkbox checkbox-primary">
												<input id="TaskStatusCheckbox<?=$key?>" checked="" type="checkbox" name="WOStatus[]" value="<?=$value->WOStatusID?>">
												<label for="TaskStatusCheckbox<?=$key?>"> <?=$value->Status?> </label>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
						
						<div class="col-md-12">
						<hr class="light-grey-hr row mt-10 mb-10">
							<div class="col-md-1">
								<div class="form-group">
									<label class="control-label">Priority</label>
								</div>
							</div>
							<div class="col-md-11">
								 <div class="form-group">
									<?php foreach ($WOPriority as $key => $value) { ?>
										<div class="col-md-4">
											<div class="checkbox checkbox-primary">
												<input id="TaskPriorityCheckbox<?=$key?>" checked="" type="checkbox" name="WOPriority[]" value="<?=$value->WOPriorityID?>">
												<label for="TaskPriorityCheckbox<?=$key?>"> <?=$value->Priority?> </label>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<hr class="light-grey-hr row mt-10 mb-10">
						</div>
					<!-- </form> -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" OnClick="DayData();">Save</button>
				</div>
			</div>
		</div>
	</div>