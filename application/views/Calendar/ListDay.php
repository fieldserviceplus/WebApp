
<?php $this->load->view('Template/HeaderView')?>
 
<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="panel panel-default card-view">
				<!-- <div class="panel-heading">
					<div class="pull-left col-lg-10 col-sm-8 col-md-8">
						<h6 class="panel-title txt-dark">Calendar</h6>
					</div>
					<div class="pull-right col-lg-2 col-sm-4 col-md-4">
						<div class="input-group">
							<input id="myTable1globalfilter" class="form-control myTable1globalfilter" placeholder="Search" type="text">
							<span class="input-group-addon"><i class="zmdi zmdi-search"></i></span> 
						</div> 
					</div>
					<div class="clearfix"></div>
				</div> -->
				<div class="panel-heading pt-10 pb-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['Calendar']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['Calendar']['title']?></span>
									<span class="panel-title txt-dark">List View</span>
								</div>
							</div>
						</div>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-md-1">
								<label class="control-label mt-10">View</label>
							</div>
							<div class="btn-group col-md-9">
								<div class="form-group">
									<a href="<?=SITE_URL?>Calendar/ListView/" class="btn btn-primary">List</a>
									<a href="<?=SITE_URL?>Calendar/CalendarView/" class="btn btn-default btn-outline">Calendar</a>
									<a href="<?=SITE_URL?>Calendar/GanttView/" class="btn btn-default btn-outline">Gantt</a>
									<a href="<?=SITE_URL?>Calendar/MapView/" class="btn btn-default btn-outline">Map</a> 
								</div>
							</div>
						</div>
						<form id="DayViewForm" method="post">
						<input type="hidden" name="ViewType" value="Day">

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-10">
							<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
								<label class="control-label mt-10">Show</label>
							</div>
							<div class="btn-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<a href="<?=SITE_URL?>Calendar/ListView/" class="btn btn-primary">Day</a>
									<a href="<?=SITE_URL?>Calendar/ListView/Week/" class="btn btn-default btn-outline">Week</a>
									<a href="<?=SITE_URL?>Calendar/ListView/Month/" class="btn btn-default btn-outline">Month</a>
								</div>
							</div>

							<div class="btn-group col-lg-2 col-md-2 col-sm-3 col-xs-12">
								<div class="form-group">
									<div class='input-group date showdatepicker'>
										<input type='text' class="form-control" name="Date" id="Date" value="<?=date("m/d/Y");?>"/>
										<span class="input-group-addon">
											<span class="fa fa-calendar"></span>
										</span>
									</div>
								</div>
							</div>
							<div class="col-lg-1 col-md-1 col-sm-1 col-xs-6">
								<div class="form-group">
									<a href="#" class="btn btn-default btn-outline" id="TodayDate">Today</a>
								</div>
							</div>
							<div class="btn-group col-lg-2 col-md-1 col-sm-1 col-xs-6">
								<div class="form-group">
									<a class="btn btn-default btn-outline" data-toggle="modal" data-target="#list-view-filter" ><i class="fa fa-filter"></i></a>
									<?php $this->load->view('Calendar/Modal/ListViewFilter')?>
								</div>
							</div>
							<div class="btn-group col-lg-2 col-md-3 col-sm-3 col-xs-12">

								 <select id="AssignedTo" class="selectpicker" multiple data-style="form-control btn-default btn-outline" name="AssignedTo[]">
								 <option value="All" selected="selected">Select All</option>
								 <?php $Users = $this->ListViewModel->GetUsers();
								 foreach ($Users as $key => $User) { ?>
								 		<option value="<?=$User->UserID?>" selected><?=$User->FirstName?> <?=$User->LastName?></option>
								 <?php } ?>
								</select> 
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default card-view">
				<div class="panel-wrapper collapse in">
					<div class="panel-body pt-0">
						<div class="table-wrap">
							<div class="table-responsive">
								<table id="myTable15" class="table table-hover display  pb-30" width="100%">
									<thead>
										<tr>
											<th width="10%" class="text-center">Assigned To</th>
											<th width="20%" class="text-center">Subject</th>
											<th width="5%" class="text-center">Related To</th>
											<th width="8%"" class="text-center">Start Date/Time</th>
											<th width="8%" class="text-center">End Date/Time</th>
											<th width="10%" class="text-center">Status</th>
											<th width="10%" class="text-center">Status Image</th>
											<th width="5%" class="text-center">Priority</th>
										</tr>
									</thead>
									<tbody id="DayDataTbl">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
	<!-- /Row -->
</div>

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script type="text/javascript">
function toggleSelectAll(control) { 
    var allOptionIsSelected = (control.val() || []).indexOf("All") > -1;
    function valuesOf(elements) {
        return $.map(elements, function(element) {
            return element.value;
        });
    }
    if(control.data('allOptionIsSelected') != allOptionIsSelected) {
        // User clicked 'All' option
        if (allOptionIsSelected) {
            // Can't use .selectpicker('selectAll') because multiple "change" events will be triggered
            control.selectpicker('val', valuesOf(control.find('option')));
            DayData();
        } else {
            control.selectpicker('val', []);
            DayData();
        }
    } else {
    	DayData();
    } 
    control.data('allOptionIsSelected', allOptionIsSelected);
}
$('#AssignedTo').selectpicker().change(function(){toggleSelectAll($(this));}).trigger('change');

function DayData()
{
 	$.ajax({
        url: '<?=SITE_URL?>Calendar/ListView/GetList/',
        data: $("#DayViewForm").serialize(),
        method: "post",
        dataType: 'html',
        success: function (result) {

       		$('#myTable15').DataTable().destroy();
       		$("#DayDataTbl").empty();
        	$("#DayDataTbl").html(result);
        	
        	$('#myTable15').DataTable( {
        		searching: false,
				responsive: true,
				"columnDefs": [ {
			          "targets": 'no-sort',
			          "orderable": false,
			    } ],
			    "aaSorting": [],
			    "dom": 'frt<"bottom"ilp><"clear">', // B
				/*buttons: [
					'excel'
				],*/
			} );
		    /*$('input.myTable1globalfilter').on( 'keyup click', function () {
		        $('#myTable15').DataTable().search(
			        $('#myTable1globalfilter').val()
			    ).draw();
		    });*/ 
        }
    });
}

$(document).on('click', "#TodayDate", function () { 
	$("#Date").val('<?=date("m/d/Y");?>');
	DayData();
});

$('.showdatepicker').on('changeDate', function(ev){
    DayData();
});
</script>

