
<?php $this->load->view('Template/HeaderView')?>

<link href="<?=SITE_URL?>vendors/bower_components/fullcalendar/dist/fullcalendar.css" rel="stylesheet" type="text/css"/>
<link href='<?=SITE_URL?>vendors/bower_components/fullcalendar-scheduler/dist/css/scheduler.min.css' rel='stylesheet' />

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<style type="text/css">
.right-slide-panel {
	width:300px;
	position:absolute;
	left:100px;
	z-index: 5;
	top:0px;
	display: none;
}
.slider-arrow {
	font:400 30px Arial, Helvetica, sans-serif;
	color:#000;
	position:absolute;
	left:73px;
	/*z-index: 555555;*/
	background-color: white;
	padding: 10px 15px 10px 15px;
	border: 1px solid #5fc55f;
	border-radius: 7px 0px 0px 7px;
}

.fc-toolbar .fc-center h2 {
    font-size: 18px;
    font-weight: bold;
    line-height: 15px;
}

tr[data-resource-id="NULL"] {
    border-top: 2px solid gray;
}
.fc-timeline .fc-divider
{
	width: 0px;
}
.fc-widget-content
{
	height: 70px;
	white-space: normal !important;
	word-wrap : break-word !important;
	text-align:center !important;
}
.fc-content
{
	white-space: normal !important;
	word-wrap : break-word !important;
	max-height : 60px;
}
.fc-resource-area{
	width: 10%;
}
.fc-cell-content{
	text-align:center !important;
}

.fc-widget-header:nth-child(24n){
	border-right: 2px solid gray !important;
}
td.fc-widget-content.fc-major:nth-child(24n) {
    border-right: 2px solid gray !important;
}
tbody tr:first-child:not(.fc-chrono) th {
  border-right: 2px solid gray !important;
}

.swal-wide{
    width: 30%;
}

</style>


<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10 pb-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['Calendar']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['Calendar']['title']?></span>
									<span class="panel-title txt-dark">Gantt View</span>
								</div>
							</div>
						</div>
					</ul>
					<div class="clearfix"></div>
				</div>

				<form id="GanttViewForm" method="post">

				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-md-1">
								<label class="control-label mt-10">View</label>
							</div>
							<div class="btn-group col-md-10 col-sm-10 col-xs-9">
								<div class="form-group">
									<a href="<?=SITE_URL?>Calendar/ListView/" class="btn btn-default btn-outline">List</a>
									<a href="<?=SITE_URL?>Calendar/CalendarView/" class="btn btn-default btn-outline">Calendar</a>
									<a href="<?=SITE_URL?>Calendar/GanttView/" class="btn btn-primary">Gantt</a>
									<a href="<?=SITE_URL?>Calendar/MapView/" class="btn btn-default btn-outline">Map</a>
								</div>
							</div>

							<?php $this->load->view('Calendar/Modal/UnassignedUnscheduledWorkOrder')?>

						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-10">
							<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
								<label class="control-label mt-10">Show</label>
							</div>
							<div class="form-group col-lg-3 col-md-3 col-sm-4 col-xs-12">
								<ul role="tablist" class="nav nav-pills" id="myTabs_12">
									<li class="active custom_li_btn" id="dayView"><a href="#" data-toggle="tab">Day</a></li>
									<li class="custom_li_btn" id="weekView"><a href="#" data-toggle="tab">Week</a></li>
								</ul>
							</div>

							<div class="btn-group col-lg-3 col-md-3 col-sm-5 col-xs-12">
								<div class="form-group">
									<div class='input-group date showdatepicker'>
										<input type='hidden'  class="form-control" id="showdate" />
										<span class="showdatefulldate form-control"></span>
										<span class="input-group-addon">
											<span class="fa fa-calendar"></span>
										</span>
									</div>
								</div>
							</div>
							<div class="col-lg-1 col-md-1 col-sm-3 col-xs-6">
								<div class="form-group">
									<button type="button" class="btn btn-default btn-outline todat-btn">Today</button>
								</div>
							</div>
							<div class="btn-group col-lg-2 col-md-2 col-sm-2 col-xs-6">
								<div class="form-group">
									<button type="button" class="btn btn-default btn-outline" data-toggle="modal" data-target="#list-view-filter" ><i class="fa fa-filter"></i></button>
									<?php $this->load->view('Calendar/Modal/ListViewFilter')?>
								</div>
							</div>
							<div class="btn-group col-lg-2 col-md-2 col-sm-4 col-xs-12">
								 <select id="AssignedTo" name="AssignedTo[]" class="selectpicker" multiple data-style="form-control btn-default btn-outline">
								  <option value="All" selected="selected">Select All</option>
									<?php $Users = $this->GanttViewModel->GetUsers();
								 foreach ($Users as $key => $User) { ?>
								 		<option value="<?=$User->UserID?>" selected><?=$User->FirstName?> <?=$User->LastName?></option>
								 <?php } ?>
								</select> 
							</div>

						</div>

					</div>
				</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default card-view">
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						  <div id="calendar"></div>
					</div>
				</div>
			</div>	
		</div>	
	</div>	
	<!-- /Row -->		

</div>


<div id="ganttEventDetailModal" class="modal fade">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
            <h4 id="modalTitle" class="modal-title calendar-event-title"></h4>
        </div>
        <div id="modalBody" class="modal-body calendar-event-data"> </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>


<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script type="text/javascript">
	function toggleSelectAll(control) {
	    var allOptionIsSelected = (control.val() || []).indexOf("All") > -1;
	    function valuesOf(elements) {
	        return $.map(elements, function(element) {
	            return element.value;
	        });
	    }
	    if(control.data('allOptionIsSelected') != allOptionIsSelected) {
	        // User clicked 'All' option
	        if (allOptionIsSelected) {
	            // Can't use .selectpicker('selectAll') because multiple "change" events will be triggered
	            control.selectpicker('val', valuesOf(control.find('option')));
	            DayData();
	        } else {
	            control.selectpicker('val', []);
	            DayData();
	        }
	    } else {
	    	DayData();
	    } 
	    control.data('allOptionIsSelected', allOptionIsSelected);
	}
	$('#AssignedTo').selectpicker().change(function(){toggleSelectAll($(this));}).trigger('change');
</script>

<!-- Calender JavaScripts -->
<script src="<?=SITE_URL?>vendors/bower_components/moment/min/moment.min.js"></script> 
<script src="<?=SITE_URL?>vendors/jquery-ui.min.js"></script>
<script src='<?=SITE_URL?>vendors/bower_components/fullcalendar-scheduler/dist/js/fullcalendar.min.js'></script>
<script src='<?=SITE_URL?>vendors/bower_components/fullcalendar-scheduler/dist/js/scheduler.min.js'></script>
<script src='<?=SITE_URL?>dist/js/fullcalendar-scheduler-data.js'></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<!-- Calender JavaScripts -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-datepicker/ajax/libs/bootstrap-datepicker.js"></script>

<script type="text/javascript">
$("#dayView").click(function(){
   $('#calendar').fullCalendar('changeView', 'timelineDay');
}); 
$("#weekView").click(function(){
   $('#calendar').fullCalendar('changeView', 'timelineWeek');
}); 

$("#showdate").change(function(){
   var date = $("#showdate").val(); 
   $('#calendar').fullCalendar('gotoDate', date);
}); 

$(function(){
	$('.slider-arrow.show').click(function(){
		$( ".right-slide-panel" ).css('display','block'); 
		setTimeout(function(){  
	    	$('.slider-arrow.show').html('&raquo;').removeClass('show').addClass('hid'); 

	    }, 600);
	    $( " .right-slide-panel" ).animate({
          left: "-169px"
		  }, 400, function() {
            // Animation complete.
        });
	    $( " .slider-arrow" ).animate({
          left: "-230px"
		  }, 400, function() {
            // Animation complete.
        });
    });
    $(document).on('click', ".slider-arrow.hid", function() {
	   
    	setTimeout(function(){  
    		$( ".right-slide-panel" ).css('display','none'); 
	    	$('.slider-arrow.hid').html('&laquo;').removeClass('hid').addClass('show'); 
	    }, 700);
	    $( ".right-slide-panel" ).animate({
          left: "100px"
		  }, 400, function() {
            // Animation complete.
        });
	    $( " .slider-arrow" ).animate({
          left: "73px"
		  }, 400, function() {
            // Animation complete.
        }); 
       
	});
});

$(document).on('click', ".todat-btn", function() {
 	$(".fc-today-button").click();
});

function getFormattedDate(date) {
		
	  var year = date.getFullYear();
	  var month = (1 + date.getMonth()).toString();
	  month = month.length > 1 ? month : '0' + month;
	  var day = date.getDate().toString();
	  day = day.length > 1 ? day : '0' + day;
	  return month + '/' + day + '/' + year;
}
/*setTimeout(function(){  
alert($('.fc-widget-content .fc-major').length);
 }, 700);*/

function DayData()
{ 
	//$('#calendar').fullCalendar('rerenderEvents');
	//$('#calendar').fullCalendar('refetchEvents');
	$('#calendar').fullCalendar( 'removeEvents');
	$('#calendar').fullCalendar('refetchResources');
  	$.ajax({
        type:"GET",
        url: "<?=SITE_URL?>Calendar/GanttView/Gantt/",
        dataType: 'json',
        data: $('#GanttViewForm').serialize(),
        success: function(data){
            $.each(data, function(index, event){ 
              	$('#calendar').fullCalendar('renderEvent', event);
            });
        }
    });
    return false;  // to stop the form from actually submitting
} 
</script>


