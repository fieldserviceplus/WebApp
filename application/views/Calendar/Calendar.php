
<?php $this->load->view('Template/HeaderView')?>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<link href="<?=SITE_URL?>vendors/bower_components/fullcalendar/dist/fullcalendar.css" rel="stylesheet" type="text/css"/>

<style type="text/css">
.right-slide-panel {
	width:300px;
	position:absolute;
	left:100px;
	z-index: 5;
	top:0px;
	display: none;
}
.slider-arrow {
	font:400 30px Arial, Helvetica, sans-serif;
	color:#000;
	position:absolute;
	left:73px;
	/*z-index: 555555;*/
	background-color: white;
	padding: 10px 15px 10px 15px;
	border: 1px solid #5fc55f;
	border-radius: 7px 0px 0px 7px;
}

.fc-toolbar .fc-center h2 {
    font-size: 18px;
    font-weight: bold;
    line-height: 15px;
}
.right-slide-panel div.panel.panel-default.card-view{
	border: 1px solid #dedede;
}

/*.sweet-alert { margin: auto; transform: translateX(-50%); }*/
.swal-wide{
    width: 30%;
}
</style>

<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10 pb-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['Calendar']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['Calendar']['title']?></span>
									<span class="panel-title txt-dark">Calendar View</span>
								</div>
							</div>
						</div>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div  class="panel-wrapper collapse in">
				<form id="CalanderViewForm" method="post">
				<input type="hidden" name="ViewType" value="Day">
					<div class="panel-body">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-md-1">
								<label class="control-label mt-10">View</label>
							</div>
							<div class="btn-group col-md-10 col-sm-10 col-xs-9">
								<div class="form-group">
									<a href="<?=SITE_URL?>Calendar/ListView/" class="btn btn-default btn-outline">List</a>
									<a href="<?=SITE_URL?>Calendar/CalendarView/" class="btn btn-primary">Calendar</a>
									<a href="<?=SITE_URL?>Calendar/GanttView/" class="btn btn-default btn-outline">Gantt</a>
									<a href="<?=SITE_URL?>Calendar/MapView/" class="btn btn-default btn-outline">Map</a>
								</div>
							</div>

							<?php $this->load->view('Calendar/Modal/UnassignedUnscheduledWorkOrder')?>

						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-10">
							<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
								<label class="control-label mt-10">Show</label>
							</div>
							<div class="btn-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<ul role="tablist" class="nav nav-pills" id="myTabs_12">
									<li class="custom_li_btn" id="dayView"><a href="#" data-toggle="tab">Day</a></li>
									<li class="custom_li_btn" id="weekView"><a href="#" data-toggle="tab">Week</a></li>
									<li class="custom_li_btn active" id="monthView"><a href="#" data-toggle="tab" >Month</a></li>
								</ul>
							</div>

							<div class="btn-group col-lg-3 col-md-3 col-sm-4 col-xs-12">
								<div class="form-group">

									<div class='input-group date showdatepicker'>
										<input type='hidden'  class="form-control" id="showdate" />
										<span class="showdatefulldate form-control"></span>
										<span class="input-group-addon">
											<span class="fa fa-calendar"></span>
										</span>
									</div>
									
								</div>
							</div>
							<div class="col-lg-1 col-md-1 col-sm-2 col-xs-6">
								<div class="form-group">
									<button type="button" class="btn btn-default btn-outline todat-btn">Today</button>
								</div>
							</div>
							<div class="btn-group col-lg-2 col-md-2 col-sm-2 col-xs-6">
								<div class="form-group">
									<button type="button" class="btn btn-default btn-outline" data-toggle="modal" data-target="#list-view-filter"><i class="fa fa-filter"></i></button>
									<?php $this->load->view('Calendar/Modal/ListViewFilter')?>
								</div>
							</div>
							<div class="btn-group col-lg-2 col-md-2 col-sm-8 col-xs-9">
								 <select id="AssignedTo" name="AssignedTo[]" class="selectpicker" multiple data-style="form-control btn-default btn-outline" >
								 	 <option value="All" selected="selected">Select All</option>
									 <?php $Users = $this->CalendarViewModel->GetUsers();
								 foreach ($Users as $key => $User) { ?>
								 		<option value="<?=$User->UserID?>" selected><?=$User->FirstName?> <?=$User->LastName?></option>
								 <?php } ?>
								</select> 
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default card-view">
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="calendar-wrap big-calendar">
						  <div id="calendar"></div>
						</div>
					</div>
				</div>
			</div>	
		</div>	
	</div>	
	<!-- /Row -->		

</div>


<div id="calendarEventDetailModal" class="modal fade">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
            <h4 id="modalTitle" class="modal-title calendar-event-title"></h4>
        </div>
        <div id="modalBody" class="modal-body calendar-event-data"> </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>
			

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Calender JavaScripts -->
<script src="<?=SITE_URL?>vendors/bower_components/moment/min/moment.min.js"></script>
<script src="<?=SITE_URL?>vendors/jquery-ui.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="<?=SITE_URL?>dist/js/fullcalendar-data.js"></script>

<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-datepicker/ajax/libs/bootstrap-datepicker.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
$("#dayView").click(function(){
   $('#calendar').fullCalendar('changeView', 'agendaDay');
}); 
$("#weekView").click(function(){
   $('#calendar').fullCalendar('changeView', 'agendaWeek');
}); 
$("#monthView").click(function(){
   $('#calendar').fullCalendar('changeView', 'month');
}); 

$("#showdate").change(function(){
   var date = $("#showdate").val(); 
   if(date != ''){
   		$('#calendar').fullCalendar('gotoDate', date);
   }
}); 

$(function(){
	$('.slider-arrow.show').click(function(){
		$( ".right-slide-panel" ).css('display','block'); 
		setTimeout(function(){  
	    	$('.slider-arrow.show').html('&raquo;').removeClass('show').addClass('hid'); 

	    }, 600);
	    $( " .right-slide-panel" ).animate({
          left: "-169px"
		  }, 400, function() {
            // Animation complete.
        });
	    $( " .slider-arrow" ).animate({
          left: "-230px"
		  }, 400, function() {
            // Animation complete.
        });
    });
    $(document).on('click', ".slider-arrow.hid", function() {
	   
    	setTimeout(function(){  
    		$( ".right-slide-panel" ).css('display','none'); 
	    	$('.slider-arrow.hid').html('&laquo;').removeClass('hid').addClass('show'); 
	    }, 700);
	    $( ".right-slide-panel" ).animate({
          left: "100px"
		  }, 400, function() {
            // Animation complete.
        });
	    $( " .slider-arrow" ).animate({
          left: "73px"
		  }, 400, function() {
            // Animation complete.
        }); 
	});

	$('#calendar').fullCalendar("getView").calendar.defaultTimedEventDuration = moment.duration("01:00:00");

	$(document).on('click', ".todat-btn", function() {
	 	$(".fc-today-button").click();
	});
});

function toggleSelectAll(control) {
    var allOptionIsSelected = (control.val() || []).indexOf("All") > -1;
    function valuesOf(elements) {
        return $.map(elements, function(element) {
            return element.value;
        });
    }
    if(control.data('allOptionIsSelected') != allOptionIsSelected) {
        // User clicked 'All' option
        if (allOptionIsSelected) {
            // Can't use .selectpicker('selectAll') because multiple "change" events will be triggered
            control.selectpicker('val', valuesOf(control.find('option')));
            DayData();
        } else {
            control.selectpicker('val', []);
            DayData();
        }
    } else {
    	DayData();
    } 
    control.data('allOptionIsSelected', allOptionIsSelected);
}
$('#AssignedTo').selectpicker().change(function(){toggleSelectAll($(this));}).trigger('change');

function DayData()
{ 
	$('#calendar').fullCalendar( 'removeEvents'); 
  	$.ajax({
        type:"GET",
        url: "<?=SITE_URL?>Calendar/CalendarView/Calendar/",
        dataType: 'json',
        data: $('#CalanderViewForm').serialize(),
        success: function(data){

            $.each(data, function(index, event){
              	$('#calendar').fullCalendar('renderEvent', event);
            });
        }
    });

    return false;

     // to stop the form from actually submitting
} 
</script>


