
<?php $this->load->view('Template/HeaderView');?>

<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10 pb-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['Calendar']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['Calendar']['title']?></span>
									<span class="panel-title txt-dark">Map View</span>
								</div>
							</div>
						</div>
					</ul>
					<div class="clearfix"></div>
				</div>
				
				<div  class="panel-wrapper collapse in">

				<form id="DayViewForm" method="post">
					<input type="hidden" name="ViewType" value="Week">

					<div class="panel-body">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-md-1">
								<label class="control-label mt-10">View</label>
							</div>
							<div class="btn-group col-md-7">
								<div class="form-group">
									<a href="<?=SITE_URL?>Calendar/ListView/" class="btn btn-default btn-outline">List</a>
									<a href="<?=SITE_URL?>Calendar/CalendarView/" class="btn btn-default btn-outline">Calendar</a>
									<a href="<?=SITE_URL?>Calendar/GanttView/" class="btn btn-default btn-outline">Gantt</a>
									<a href="<?=SITE_URL?>Calendar/MapView/" class="btn btn-primary">Map</a>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group" style="float: right;">
									<button type="button" class="btn btn-primary btn-outline unassigned-btn" id="show-unassigned">Show Unassigned</button>
									<input class="btn btn-primary btn-outline unassigned-btn" type="checkbox" name="Unassigned" id="unassigned-chk" value="1" style="display: none">
									<button type="button" class="btn btn-primary btn-outline remove-marker unscheduled-btn" id="show-unscheduled">Show Unscheduled</button>
									<input type="checkbox" name="Unscheduled" id="unscheduled-chk" value="1" style="display: none">
								</div>
							</div> 
						</div>

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-10">
							<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
								<label class="control-label mt-10">Show</label>
							</div>
							<div class="btn-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
								 <div class="form-group">
									<a href="<?=SITE_URL?>Calendar/MapView/" class="btn btn-default btn-outline" id="dayView">Day</a>
									<a href="<?=SITE_URL?>Calendar/MapView/Week/" class="btn btn-primary" id="weekView">Week</a>
								</div> 
							</div>
							<div class="btn-group col-lg-2 col-md-2 col-sm-4 col-xs-12">
								<div class="form-group">
									<div class='input-group date'>
										<input type='text' class="form-control" name="Date" id="showweeklydate" value="" />
										<span class="input-group-addon showweeklydatepicker">
											<span class="fa fa-calendar"></span>
										</span>
									</div>
								</div>
							</div>
							<div class="col-lg-1 col-md-1 col-sm-2 col-xs-6">
								<div class="form-group">
									<button type="button" class="btn btn-default btn-outline" id="TodayDate">Today</button>
								</div>
							</div>
							<div class="btn-group col-lg-2 col-md-2 col-sm-2 col-xs-6">
								<div class="form-group">
									<button type="button" class="btn btn-default btn-outline" data-toggle="modal" data-target="#list-view-filter" ><i class="fa fa-filter"></i></button>
									<?php $this->load->view('Calendar/Modal/MapViewFilter')?>
								</div>
							</div>
							<div class="btn-group col-lg-2 col-md-2 col-sm-3 col-xs-12">
								 <select id="AssignedTo" name="AssignedTo[]" class="selectpicker" multiple data-style="form-control btn-default btn-outline">
								  <option value="All" selected="selected">Select All</option>
									<?php $Users = $this->MapViewModel->GetUsers();
								 foreach ($Users as $key => $User) { ?>
								 		<option value="<?=$User->UserID?>" selected><?=$User->FirstName?> <?=$User->LastName?></option>
								 <?php } ?>
								</select> 
							</div>

						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default card-view">
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="col-md-12">
							<div id="map_canvas" style="height:800px;"></div>
						</div>
					</div>
				</div>
			</div>	
		</div>	
	</div>	
	<!-- /Row -->

 
	<!-- Row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Legend</h6>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
					<div class="col-lg-12">
						<div class="table-wrap sm-data-box-2">
							<div class="table-responsive">
								<table class="table top-countries mb-0">
									<thead>
										<tr>
											<th>Status / Priority</th>
											<th class="text-center">Low</th>
											<th class="text-center">Medium</th>
											<th class="text-center">High</th>
											<th>Status / Priority</th>
											<th class="text-center">Low</th>
											<th class="text-center">Medium</th>
											<th class="text-center">High</th>
										</tr>
									</thead>
									<tbody>
									<?php foreach ($WOStatus as $key => $value) { ?>
										<?php if($key % 2 == 0) {?>
											<tr>
												<td width="20%">
													<span class="country-name txt-dark"><?=$value->Status?></span>
												</td>
												<td width="10%" class="text-center"> 
													  <i class="fa fa-circle fa-2x" style="color:<?=$value->ColorCode?>;"></i>
												</td>
												<td width="10%" class="text-center"> 
													  <i class="fa fa-square fa-2x" style="color:<?=$value->ColorCode?>;"></i>
												</td>
												<td width="10%" class="text-center"> 
													  <i class="fa fa-star fa-2x" style="color:<?=$value->ColorCode?>;"></i>
												</td>
											<?php } ?>
										<?php if($key % 2 != 0) {?>
												<td width="20%">
													<span class="country-name txt-dark"><?=$value->Status?></span>
												</td>
												<td width="10%" class="text-center"> 
													  <i class="fa fa-circle fa-2x" style="color:<?=$value->ColorCode?>;"></i>
												</td>
												<td width="10%" class="text-center"> 
													  <i class="fa fa-square fa-2x" style="color:<?=$value->ColorCode?>;"></i>
												</td>
												<td width="10%" class="text-center"> 
													  <i class="fa fa-star fa-2x" style="color:<?=$value->ColorCode?>;"></i>
												</td>
											</tr>
										 <?php } ?>
									<?php } ?>
									<tr>
										<td width="20%" style="border-top-color: gray;">
											<span class="country-name txt-dark"><strong>Unassigned</strong></span>
										</td>
										<td width="10%" class="text-center" colspan="3" style="border-top-color: gray;"> 
											  <i class="fa fa-dot-circle-o fa-2x" style="color:#FF00FF;"></i>
										</td>
										<td width="20%" style="border-top-color: gray;">
											<span class="country-name txt-dark"><strong>Unscheduled</strong></span>
										</td>
										<td width="10%" class="text-center" colspan="3" style="border-top-color: gray;"> 
											  <i class="fa fa-clock-o fa-2x" style="color:#00FFFF;"></i>
										</td>
									</tr>
									<tr>
										<td width="20%" style="border-top-color: gray;">
											<span class="country-name txt-dark"><strong>Technicians Current Location</strong></span>
										</td>
										<td width="10%" class="text-center" colspan="3" style="border-top-color: gray;"> 
											  <i class="fa fa-user fa-2x" style="color:#4a235a;"></i>
										</td>
										<td width="20%" style="border-top-color: gray;">
											<span class="country-name txt-dark"><strong></strong></span>
										</td>
										<td width="10%" class="text-center" colspan="3" style="border-top-color: gray;"> 
											  
										</td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>	
						</div>	
					</div>
				</div>
			</div>	
		</div>	
	</div>	
	<!-- /Row -->

</div>

	
<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>


<!-- Bootstrap Datepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<!-- Google Map JavaScript -->
<script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_JS_KEY?>"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/fontawesome-markers@4.6.1/fontawesome-markers.min.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>


<script type="text/javascript">
$(function(){

function toggleSelectAll(control) { 
    var allOptionIsSelected = (control.val() || []).indexOf("All") > -1;
    function valuesOf(elements) {
        return $.map(elements, function(element) {
            return element.value;
        });
    }
    if(control.data('allOptionIsSelected') != allOptionIsSelected) {
        // User clicked 'All' option
        if (allOptionIsSelected) {
            // Can't use .selectpicker('selectAll') because multiple "change" events will be triggered
            control.selectpicker('val', valuesOf(control.find('option')));
            SetWorkorderOnMap();SetTechniciansOnMap();
        } else {
            control.selectpicker('val', []);
            SetWorkorderOnMap(); SetTechniciansOnMap();
        }
    } else {
    	SetWorkorderOnMap(); SetTechniciansOnMap();
    } 
    control.data('allOptionIsSelected', allOptionIsSelected);
}
$('#AssignedTo').selectpicker().change(function(){toggleSelectAll($(this));}).trigger('change');

	if( $('#map_canvas').length > 0 ){	
	
		var markers = []; var technicianmarkers = [];
		var marker, i;
		var infowindow = new google.maps.InfoWindow();
	    var map = new google.maps.Map(document.getElementById('map_canvas'), {
	      zoom: 4,
	      center: new google.maps.LatLng(49.78462436709735, -100.78511989999998),
	      mapTypeId: google.maps.MapTypeId.ROADMAP,
	    });

	  	var PriorityIcon = {"Low": fontawesome.markers.CIRCLE, "Normal": fontawesome.markers.SQUARE, "High": fontawesome.markers.STAR};

        SetWorkorderOnMap();

        if($("#unscheduled-chk").is(":checked")){
        	$(".unscheduled-btn").toggleClass("btn-outline");
        }
        if($("#unassigned-chk").is(":checked")){
        	$(".unassigned-btn").toggleClass("btn-outline");
        }
	}

    function SetWorkorderOnMap() {

    	$.ajax({
            url: '<?=SITE_URL?>Calendar/MapView/Map/',
            data: $("#DayViewForm").serialize(),
            method: "post",
            dataType: 'html',
            success: function (result) {
       
				var WorkOrderObj = JSON.parse(result);

				DeleteMarkers();
				//console.log(result);
				$.each(WorkOrderObj, function(WorkOrderObjKey, WorkOrderObjVal) {

						var fillColor = WorkOrderObj[WorkOrderObjKey]["Color"];
						var path = PriorityIcon[WorkOrderObj[WorkOrderObjKey]["Priority"]];
						if(WorkOrderObj[WorkOrderObjKey]["AssignedTo"] == null)
						{ 
							var fillColor = "#FF00FF";
							var path = fontawesome.markers.DOT_CIRCLE_O;
						}

						if(WorkOrderObj[WorkOrderObjKey]["StartDate"] == null && WorkOrderObj[WorkOrderObjKey]["IsClosed"] == 0)
						{  
							var fillColor = "#00FFFF";
							var path = fontawesome.markers.CLOCK_O;
						}
	 
			    	  	marker = new google.maps.Marker({
				        position: new google.maps.LatLng(WorkOrderObj[WorkOrderObjKey]["Lat"], WorkOrderObj[WorkOrderObjKey]["Long"]),
				        map: map,
				        icon: {
						        path: path,
						        scale: 0.5,
						        strokeWeight: 0.2,
						        strokeColor: 'black',
						        strokeOpacity: 1,
						        fillColor: fillColor,
						        fillOpacity: 8,
						    },
					    });

					    google.maps.event.addListener(marker, 'click', (function(marker, i) {
					        return function() {
					          infowindow.setContent(WorkOrderObj[WorkOrderObjKey]["Title"]);
					          infowindow.open(map, marker);
					        }
					    })(marker, i));

					    markers.push(marker);
				});
            }
        });
    }
    function SetTechniciansOnMap() {

    	$.ajax({
            url: '<?=SITE_URL?>Calendar/MapView/GetTechnicians/',
            data: $("#DayViewForm").serialize(),
            method: "post",
            dataType: 'html',
            success: function (result) {
       
       			DeleteTechniciansMarkers();
				var TechniciansObj = JSON.parse(result);
				$.each(TechniciansObj, function(WorkOrderObjKey, WorkOrderObjVal) {

						var fillColor = TechniciansObj[WorkOrderObjKey]["Color"];
						var path = fontawesome.markers.USER;
			    	  	marker = new google.maps.Marker({
				        position: new google.maps.LatLng(TechniciansObj[WorkOrderObjKey]["Lat"], TechniciansObj[WorkOrderObjKey]["Long"]),
				        map: map,
				        icon: {
						        path: path,
						        scale: 0.5,
						        strokeWeight: 0.2,
						        strokeColor: 'black',
						        strokeOpacity: 1,
						        fillColor: fillColor,
						        fillOpacity: 8,
						    },
					    });

					    google.maps.event.addListener(marker, 'click', (function(marker, i) {
					        return function() {
					          infowindow.setContent(TechniciansObj[WorkOrderObjKey]["Title"]);
					          infowindow.open(map, marker);
					        }
					    })(marker, i));

					    technicianmarkers.push(marker);
				});
            }
        });
    }

    function DeleteMarkers() {
        //Loop through all the markers and remove
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];
    };

    function DeleteTechniciansMarkers() {
        //Loop through all the markers and remove
        for (var i = 0; i < technicianmarkers.length; i++) {
            technicianmarkers[i].setMap(null);
        }
        technicianmarkers = [];
    };

	$('#show-unassigned').on('click', function() {
		$(".unassigned-btn").toggleClass("btn-outline");
		$("#unassigned-chk").click();
		SetWorkorderOnMap();
	});

	$('#show-unscheduled').on('click', function() {
		$(".unscheduled-btn").toggleClass("btn-outline");
		$("#unscheduled-chk").click();
		SetWorkorderOnMap();
	});

	$(document).on('click', "#MapViewFilterBtn", function () { 
		SetWorkorderOnMap();
	});

	// For Week
	$(document).on('click', "#TodayDate", function () { 
		var value = '<?=date("m/d/Y")?>';
	    var firstDate = moment(value, "MM/DD/YYYY").day(0).format("MM/DD/YYYY");
	    var lastDate =  moment(value, "MM/DD/YYYY").day(6).format("MM/DD/YYYY");
	    $("#showweeklydate").val(firstDate + " - " + lastDate);
		SetWorkorderOnMap();
	});

	// For Week
	setTimeout(function(){ 
		$("#TodayDate").click();
	}, 1000);

	$(".showweeklydatepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true,
        todayBtn: "linked"
	}).datepicker('update', new Date());

	$('.showweeklydatepicker').datepicker().on('changeDate', function(e) { 
	    var value = $('.showweeklydatepicker').datepicker("getDate"); 
	    var firstDate = moment(value, "MM/DD/YYYY").day(0).format("MM/DD/YYYY");
	    var lastDate =  moment(value, "MM/DD/YYYY").day(6).format("MM/DD/YYYY");
	    $("#showweeklydate").val(firstDate + " - " + lastDate);

	    SetWorkorderOnMap();
	});


});
</script>
