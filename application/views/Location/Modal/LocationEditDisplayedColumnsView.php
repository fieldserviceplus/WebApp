<!-- modal -->
<div class="modal fade" id="account-edit-displayed-columns-view-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Edit Displayed Columns</h5>
			</div>
			<form class="form-horizontal" method="post" action="<?=SITE_URL;?>Location/View/EditDisplayedColumnsCustomView/<?=$SelectedLocation?>">
			<div class="modal-body">
				<div class="row">
							<div class="col-md-12">
					            <div class="col-md-4">
					                <select name="from[]" id="multiselect" class="form-control" multiple="multiple" size="8" >
					                  <?php $SpecifyFieldsDisplay = explode(",", $LocationCustomViewData['LocationViewData']->SpecifyFieldsDisplay); 
									      	$Fields = $this->ViewModel->LocationViewFields(); 
									      	foreach($Fields as $Field){ 

											if(!in_array($Field->FieldName, $SpecifyFieldsDisplay)){?>

												<option value="<?=$Field->FieldName;?>"><?=$Field->FieldName;?></option>
											<?php } ?>
										<?php } ?>
					                </select>
					            </div>
					            
					            <div class="col-md-2 btn-group btn-group-vertical">
					                <button type="button" id="multiselect_rightAll" class="btn btn-default btn-icon-anim btn-circle ml-15"><i class="fa fa-angle-double-right"></i></button>
					                <button type="button" id="multiselect_rightSelected" class="btn btn-default btn-icon-anim btn-circle ml-15"><i class="fa fa-chevron-circle-right"></i></button>
					                <button type="button" id="multiselect_leftSelected" class="btn btn-default btn-icon-anim btn-circle ml-15"><i class="fa fa-chevron-circle-left"></i></button>
					                <button type="button" id="multiselect_leftAll" class="btn btn-default btn-icon-anim btn-circle ml-15"><i class="fa fa-angle-double-left"></i></button>
					            </div>
					            
					            <div class="col-md-4">
					                <select name="SpecifyFieldsDisplay[]" id="multiselect_to" class="form-control" size="8" multiple="multiple">
					                	  <?php foreach($SpecifyFieldsDisplay as $Field){ ?>
												<option value="<?=$Field;?>"><?=$Field;?></option>
										<?php } ?>
					                </select>
					            </div>

					            <div class="col-md-2 btn-group btn-group-vertical text-center">
					                <button type="button" id="multiselect_rightAll" class="btn btn-default btn-icon-anim btn-circle" onclick="moveTop();"><i class="fa fa-angle-double-up"></i></button>
					                <button type="button" id="multiselect_move_up" class="btn btn-default btn-icon-anim btn-circle"><i class="fa fa-chevron-circle-up"></i></button>
					                <button type="button" id="multiselect_move_down" class="btn btn-default btn-icon-anim btn-circle"><i class="fa fa-chevron-circle-down"></i></button>
					                <button type="button" id="multiselect_leftAll" class="btn btn-default btn-icon-anim btn-circle" onclick="moveBottom();"><i class="fa fa-angle-double-down"></i></button>
					            </div>
					        </div>
						</div>
					
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>