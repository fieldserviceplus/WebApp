
<!-- modal -->
<div class="modal fade" id="RecurringModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="RecurrenceClose"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Recurrence Settings</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" id="RecurringForm">
				<input id="WORecurrenceID" name="WORecurrenceID" type="hidden" class="WORecurrenceID" value="<?=isset($WORecurrence->WORecurrenceID)?$WORecurrence->WORecurrenceID:"";?>">
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
						    <div class="form-group">
								<label class="control-label col-md-3">Repeats</label>
								<div class="col-md-9">
									<select name="RepeatEvery" class="form-control" onchange="RepeatsChange(this.value)" required="">
										<option value="Daily" <?=isset($WORecurrence->RepeatEvery) && ($WORecurrence->RepeatEvery == 'Daily')?"Selected=''":"";?>>Daily</option>
										<option value="Weekly" <?=isset($WORecurrence->RepeatEvery) && ($WORecurrence->RepeatEvery == 'Weekly')?"Selected=''":"";?>>Weekly</option>
										<option value="Monthly" <?=isset($WORecurrence->RepeatEvery) && ($WORecurrence->RepeatEvery == 'Monthly')?"Selected=''":"";?>>Monthly</option>
										<option value="Yearly" <?=isset($WORecurrence->RepeatEvery) && ($WORecurrence->RepeatEvery == 'Yearly')?"Selected=''":"";?>>Yearly</option>
										<option value="Periodically" <?=isset($WORecurrence->RepeatEvery) && ($WORecurrence->RepeatEvery == 'Periodically')?"Selected=''":"";?>>Periodically</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Interval every</label>
								<div class="col-md-3">
										<input class="vertical-spin" type="text" data-bts-button-down-class="btn btn-default" data-bts-button-up-class="btn btn-default" data-plugin="touchSpin" name="IntervalEvery" required="" value="<?=isset($WORecurrence->IntervalEvery)?$WORecurrence->IntervalEvery:""?>">
								</div>
								<label class="control-label col-md-4 interval_every_txt" style="text-align:left;">day on</label>
							</div>

							<div class="form-group" id="days-cb" >
							   <label class="control-label col-md-3">Repeat on</label>
								<div class="col-md-1">
									<div class="checkbox checkbox-primary">
										<input id="sun_chb" type="checkbox" name="RepeatOn[]" value="SUN" checked="" value="<?=isset($WORecurrence->RepeatOn) && in_array("SUN", explode(",", $WORecurrence->RepeatOn))?"checked=''":""?>">
										<label for="sun_chb">S</label>
									</div>
								</div>
								<div class="col-md-1">
									<div class="checkbox checkbox-primary">
										<input id="mon_chb" type="checkbox" name="RepeatOn[]" value="MON" value="MON" value="<?=(isset($WORecurrence->RepeatOn) && in_array("MON", explode(",", $WORecurrence->RepeatOn)))?"checked=''":""?>">
										<label for="mon_chb">M</label>
									</div>
								</div>
								<div class="col-md-1">
									<div class="checkbox checkbox-primary">
										<input id="tue_chb" type="checkbox" name="RepeatOn[]" value="TUE" value="<?=(isset($WORecurrence->RepeatOn) && in_array("TUE", explode(",", $WORecurrence->RepeatOn)))?"checked=''":""?>">
										<label for="tue_chb">T</label>
									</div>
								</div>
								<div class="col-md-1">
									<div class="checkbox checkbox-primary">
										<input id="wed_chb" type="checkbox" name="RepeatOn[]" value="WED" value="<?=(isset($WORecurrence->RepeatOn) && in_array("WED", explode(",", $WORecurrence->RepeatOn)))?"checked=''":""?>">
										<label for="wed_chb">W</label>
									</div>
								</div>
								<div class="col-md-1">
									<div class="checkbox checkbox-primary">
										<input id="thu_chb" type="checkbox" name="RepeatOn[]" value="THU" value="<?=(isset($WORecurrence->RepeatOn) && in_array("THU", explode(",", $WORecurrence->RepeatOn)))?"checked=''":""?>">
										<label for="thu_chb">T</label>
									</div>
								</div>
								<div class="col-md-1">
									<div class="checkbox checkbox-primary">
										<input id="fri_chb" type="checkbox" name="RepeatOn[]" value="FRI" value="<?=(isset($WORecurrence->RepeatOn) && in_array("FRI", explode(",", $WORecurrence->RepeatOn)))?"checked=''":""?>">
										<label for="fri_chb">F</label>
									</div>
								</div>
								<div class="col-md-1">
									<div class="checkbox checkbox-primary">
										<input id="sat_chb" type="checkbox" name="RepeatOn[]" value="SAT" value="<?=(isset($WORecurrence->RepeatOn) && in_array("SAT", explode(",", $WORecurrence->RepeatOn)))?"checked=''":""?>">
										<label for="sat_chb">S</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Ends</label>
								<div class="col-md-9">
									<div class="radio radio-primary">
										<input id="radio1" type="radio" name="Ends" value="Never" <?=(isset($WORecurrence->Ends) && $WORecurrence->Ends == 'Never')?"checked=''":""?>>
										<label for="radio1">Never</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"></label>
								<div class="col-md-9">
									<div class="radio radio-primary col-md-2">
										<input id="radio2" type="radio" name="Ends" value="On" <?=(isset($WORecurrence->Ends) && $WORecurrence->Ends == 'On')?"checked=''":""?>>
										<label for="radio2">On</label>
									</div>
									<div class="col-md-4">
										<input type="text" id="EndsOnDate" name="EndsOnDate" class="form-control showdatepicker" value="<?=(isset($WORecurrence->EndsOnDate)?$WORecurrence->EndsOnDate:date("m/d/Y"));?>">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3"></label>
								<div class="col-md-9">
									<div class="radio radio-primary col-md-2">
										<input id="radio3" type="radio" name="Ends" value="After" <?=(isset($WORecurrence->Ends) && $WORecurrence->Ends == 'After')?"checked=''":""?>>
										<label for="radio3">After</label>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<input class="vertical-spin-occurrence" type="text" data-bts-button-down-class="btn btn-default" data-bts-button-up-class="btn btn-default" data-plugin="touchSpin" name="EndsAfterOccurrences" value="<?=isset($WORecurrence->EndsAfterOccurrences)?$WORecurrence->EndsAfterOccurrences:"";?>">
										</div>
									</div>
									<div class="col-md-3">
										<label class="control-label" style="text-align:left;">occurences</label>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Start Time</label>
								<div class="col-md-9">
									<input type="text" name="StartTime" class="form-control timepicker" required="" value="<?=isset($WORecurrence->StartTime)?$WORecurrence->StartTime:"";?>">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">End Time</label>
								<div class="col-md-9">
									<input type="text" name="EndTime" class="form-control timepicker" required="" value="<?=isset($WORecurrence->EndTime)?$WORecurrence->EndTime:"";?>">
								</div>
							</div>

						</div>
					</div>
				</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" id="RecurrenceClose">Close</button>
				<button type="button" class="btn btn-primary" id="RecurrenceSave">Save</button>
			</div>
		</div>
	</div>
</div>

