
<?php $this->load->view('Template/HeaderView')?>
<!-- Bootstrap Datetimepicker CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_JS_KEY?>&libraries=places"></script>
 
<div class="container-fluid pt-25">
	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Create Location</h6>
					</div>
					<div class="pull-right">
						<span style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong></span>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Location/Actions/CreateLocation" class="form-horizontal" id="CreateLocation" method="post">

										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>Location Details</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Name<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Name" id="Name" value="<?=isset($LocationData->Name)?$LocationData->Name:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Account<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="Account" id="Account" class="form-control select2" OnChange="GetPrimaryContactOptions(this.value);GetAccountDetails(this.value);">
														<option value="">Select Account</option>
														<?php $Accounts = $this->ActionsModel->GetAccounts();
															foreach ($Accounts as $Account) { 
																if((isset($LocationData->Account) && $LocationData->Account == $Account->AccountID) || (isset($_GET['Account']) && $_GET['Account'] == $Account->AccountID)){ ?>
																	<option value="<?=$Account->AccountID?>" selected="selected"><?=$Account->AccountName;?></option>
																<?php } else { ?>
																	<option value="<?=$Account->AccountID?>"><?=$Account->AccountName;?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('Account');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Primary Contact</label>
														<div class="col-md-9">
														<?php $Contacts = $this->ActionsModel->GetContacts(); ?>
															<select class="form-control select2 SelectPrimaryContact" name="PrimaryContact" id="PrimaryContact" OnChange="GetContactDetails(this.value);">
															<option value="">Select Primary Contact</option>
															<?php echo form_error('PrimaryContact');  ?>
														</select>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Parent Location</label>
														<div class="col-md-9">
														<select name="ParentLocation" id="ParentLocation" class="form-control select2">
														<option value="">Select Location</option>
														<?php $Locations = $this->ActionsModel->GetLocations(); 
															foreach ($Locations as $Location) { 
															if(isset($LocationData->ParentLocation) && $LocationData->ParentLocation == $Location->LocationID){ ?>
																	<option value="<?=$Location->LocationID?>" selected="selected"><?=$Location->Name?></option>
																<?php } else { ?>
																<option value="<?=$Location->LocationID?>"><?=$Location->Name?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('ParentLocation');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Access Notes</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="AccessNotes" id="AccessNotes" value="<?=isset($LocationData->AccessNotes)?$LocationData->AccessNotes:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Notes</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Notes" id="Notes" value="<?=isset($LocationData->Notes)?$LocationData->Notes:"";?>">
														</div>
													</div>
												</div>

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Owner<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="Owner" id="Owner" class="form-control select2">
														<option value="">Select Owner</option>
														<?php $Users = $this->ActionsModel->GetUsers(); 
															foreach ($Users as $User) { 
															if(isset($LocationData->Owner) && $LocationData->Owner == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else { ?>
																<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('Owner');  ?>
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-3">Location Type<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="LocationType" id="LocationType" class="form-control select2">
														<option value="">Select Location Type</option>
														<?php $LocationTypes = $this->ActionsModel->GetLocationTypes(); 
															foreach ($LocationTypes as $LocationType) { 
															if(isset($LocationData->LocationType) && $LocationData->LocationType == $LocationType->LocationTypeID){ ?>
																	<option value="<?=$LocationType->LocationTypeID?>" selected="selected"><?=$LocationType->LocationType?></option>
																<?php } else { ?>
																<option value="<?=$LocationType->LocationTypeID?>"><?=$LocationType->LocationType?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('LocationType');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Preferred Technician</label>
														<div class="col-md-9">
														<select name="PreferredTechnician" id="PreferredTechnician" class="form-control select2">
														<option value="">Select Preferred Technician</option>
														<?php $PreferredTechnicians = $this->ActionsModel->GetUsers(); 
															foreach ($PreferredTechnicians as $PreferredTechnician){
																if(isset($LocationData->PreferredTechnician) && $LocationData->PreferredTechnician == $PreferredTechnician->UserID){ ?>
																		<option value="<?=$PreferredTechnician->UserID?>" selected="selected"><?=$PreferredTechnician->FirstName?> <?=$PreferredTechnician->LastName?></option>
																	<?php } else { ?>
																		<option value="<?=$PreferredTechnician->UserID?>"><?=$PreferredTechnician->FirstName?> <?=$PreferredTechnician->LastName?></option>
																	<?php } ?>
																<?php } ?>
														</select>
														<?php echo form_error('PreferredTechnician');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Is Active</label>
														<div class="col-md-9">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="IsActive" id="IsActive" value="1" checked="">
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

												</div>
											</div>

											<div class="col-md-12">
												<div class="txt-dark capitalize-font"><strong>Address Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Address</label>
														<div class="col-md-9">
															<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-search"></i>Search Address</button>
														</div>
													</div>
												
													<div class="form-group">
														<label class="col-sm-3 control-label">Street<span class="required-sign">*</span></label>
														<div class="col-sm-9">
																<input type="text" class="form-control" name="Address" id="Address" placeholder="" data-toggle="modal" data-target="#location-picker-modal" value="<?=isset($LocationData->Address)?$LocationData->Address:"";?>">
															<?php echo form_error('Address');  ?>
															<input type="hidden" name="Latitude" id="Latitude" value="<?=isset($LocationData->Latitude)?$LocationData->Latitude:"";?>">
															<input type="hidden" name="Latitude" id="Latitude" value="<?=isset($LocationData->Latitude)?$LocationData->Latitude:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">City<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="City" id="City" value="<?=isset($LocationData->City)?$LocationData->City:"";?>">
															<?php echo form_error('City');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">State<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="State" id="State" value="<?=isset($LocationData->State)?$LocationData->State:"";?>">
															<?php echo form_error('State');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-3">Country<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Country" id="Country" value="<?=isset($LocationData->Country)?$LocationData->Country:"";?>">
															<?php echo form_error('Country');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Postal Code<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="PostalCode" id="PostalCode" value="<?=isset($LocationData->PostalCode)?$LocationData->PostalCode:"";?>">
															<?php echo form_error('PostalCode');  ?>
														</div>
													</div>
												</div>
											
											<!-- /Row -->
										</div>

										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="CreateLocation">Save</button>
															<a href="<?=SITE_URL;?>Location/Actions" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->
</div>

<?php $this->load->view('Location/Modal/LocationPickerView'); ?>
<?php $this->load->view('Location/Modal/RecurringTaskView'); ?>

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>

<script type="text/javascript">

$(document).ready(function () {
 
    $('#CreateLocation').validate({ 
        rules: {
            Name: {
                required: true
            },
            Account: {
                required: true
            },
            Owner: {
                required: true
            },
            Address: {
                required: true
            },
            City: {
                required: true
            },
            State: {
                required: true
            },
            Country: {
                required: true
            },
            PostalCode: {
                required: true
            },
            LocationType: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
        submitHandler: function (form) { 
        
        	return true;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    $('.showdatetimepicker').datetimepicker({
			/*useCurrent: true,*/
			icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
		});
});
</script>

<script type="text/javascript">
function GetPrimaryContactOptions(AccountID){
	$.ajax({
        url: '<?=SITE_URL?>Location/Actions/GetPrimaryContactOptions/',
        method: "post",
        data: { AccountID : AccountID},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$(".SelectPrimaryContact").html(result);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}

function GetAccountDetails(AccountID){
	$.ajax({
        url: '<?=SITE_URL?>Location/Actions/GetAccountDetails/'+AccountID,
        method: "post",
      /*  data: { AccountID : AccountID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$("#Address").val(result.BillingAddress);
            	$("#City").val(result.BillingCity);
            	$("#State").val(result.BillingState);
            	$("#Country").val(result.BillingCountry);
            	$("#PostalCode").val(result.BillingPostalCode);
            	$("#Latitude").val(result.BillingLatitude);
            	$("#Longitude").val(result.BillingLongitude);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}
</script>

<!-- Set Location -->
<script src="<?=SITE_URL?>dist/js/location-picker.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	
	set_location_in_map();
});
</script>