
<?php $this->load->view('Template/HeaderView');?>
 
<style type="text/css">.dataTables_filter { display: none; }</style>
    <div class="container-fluid pt-25">

        <!-- Row -->
	    <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-default card-view">
					<div  class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-lg-3 col-md-3 col-sm-5 col-xs-12 pr-0 pl-0">
								
								<div class="form-group">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
										<label class="control-label mt-10"><strong>View:</strong></label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 pr-0">
									<form method="post" action="<?=SITE_URL?>Location/View/" id="SelectedAcForm">
										<select class="selectpicker" name="SelectedLocation" data-style="form-control btn-default btn-outline" onchange="ViewLocations(this.value);">
											<option disabled="" selected="">Select Views</option>
											<optgroup label="Default Views">
											<option value="MyLocations" <?=(isset($SelectedLocation) && $SelectedLocation=='MyLocations')?"selected":""?>>My Locations</option>
											<option value="AllLocations" <?=(isset($SelectedLocation) && $SelectedLocation=='AllLocations')?"selected":""?>>All Locations</option>
											<option value="LocationsCreatedThisWeek" <?=(isset($SelectedLocation) && $SelectedLocation=='LocationsCreatedThisWeek')?"selected":""?>>Locations Created This Week</option>
											</optgroup>
											<optgroup label="My Custom Views">
												<?php $Views = $this->ViewModel->GetViews();
												foreach ($Views as $View) { ?>
												 	<option value="<?=$View->LocationViewID?>"><?=$View->LocationViewName?></option> 
											<?php } ?>
											</optgroup>
										</select>
										</form>
									</div>
								</div>	
								
							</div>	
							<div class="col-lg-5 col-md-6 col-sm-7 col-xs-12">
								<div class="form-group" >
								<!-- <div class="btn-group">
									<button class="btn btn-primary btn-outline">Go</button>
								</div> -->
								<div class="btn-group">
									<div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button"> <i class="fa fa-pencil"></i> <span class="caret"></span></button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li>
												<a><span><strong>LIST VIEW ACTIONS</strong></span></a>
											</li>
											<li> 
												<a href="<?=SITE_URL?>Location/View/CreateNewView"><span>Create New View</span></a>
											</li>
										</ul>
									</div>
								</div>
								<div class="btn-group">
									<!-- <button class="btn btn-default btn-outline" data-toggle="modal" data-target="#exampleModal" title="Edit Filters"><i class="fa fa-filter"></i></button> -->
									<button class="btn btn-default btn-outline" title="Refresh" ><i class="fa fa-refresh"></i></button>
									<button class="btn btn-default btn-outline" title="Export" id="Export-Excel"><i class="fa fa-file-excel-o"></i></button>
								</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-1 col-sm-6 col-xs-12">
								<div class="pull-right">
									<div class="form-group">
										<a href="<?=SITE_URL?>Location/Actions/CreateLocation" class="btn btn-primary btn-outline">New Location</a>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
								<div class="pull-right">
									<div class="input-group">
										<input id="accountglobalfilter" class="form-control accountglobalfilter" placeholder="Search" type="text">
										<span class="input-group-addon"><i class="zmdi zmdi-search"></i></span> 
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">

								<div class="table-responsive">
									<table id="estimate_tbl" class="table table-hover display  pb-30" width="100%">
										<thead>
											<tr>
												<th class="text-center">Edit</th>
												<th class="text-center">LC # </th>
												<th class="text-center">Name</th>
												<th class="text-center">Location Type</th>
												<th class="text-center">Account</th>
												<th class="text-center">Primary Contact</th>
												<th class="text-center">Is Active</th>
												<th class="text-center">Preferred Technician</th>
												<th class="text-center">City</th>
												<th class="text-center">State</th>
												<th class="text-center">Created Date</th>
												<th class="text-center">Created By</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->

	</div>	

<?php $this->load->view('Template/FooterView')?>


<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<script type="text/javascript">
 
 	function ViewLocations(flag){ 

 		if(flag != 'MyLocations' && flag != 'AllLocations' && flag != 'LocationsCreatedThisWeek'){ 
 			$('#SelectedAcForm').submit();
 		}

		var colarr = [
	        { className: "text-center" },
	        { className: "text-center" },
	        { className: "text-center" },
	        { className: "text-center" },
	        { className: "text-center" },
	        { className: "text-center" },
	        { className: "text-center" },
	        { className: "text-center" },
	        { className: "text-center" },
	        { className: "text-center" },
	        { className: "text-center" },
	        { className: "text-center" },
	     ];
		var dataTable = $('#estimate_tbl').DataTable({
			 destroy: true,
	       	"responsive": true,
	       	"paging": true,
	       	"bDestroy": true,
	       	"processing": true, //Feature control the processing indicator.
	        "serverSide": true, //Feature control DataTables' server-side processing mode.
	        "sAjaxSource": "<?=SITE_URL?>Location/View/ViewLocations?flag="+flag,
	       	"columnDefs": [ {
		        sortable: false,
		        targets: 'no-sort',
		        class: "index",
		        targets: 0
	       	} ],
	       	"aaSorting": [],
	       	"order": [[ 1, 'asc' ]],
	       	"fixedColumns": true, 
	       	"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 ] }],
	       	"columns": colarr, 
	        "dom": 'Bfrt<"bottom estimate_tbl"ilp><"clear">',
			buttons: [
				'excel'
			],
			lengthMenu: [
				[ 10, 25, 50, -1 ],
				[ '10', '25', '50', 'All' ]
			]
	    });
		$('.buttons-excel').hide();
	    $('#accountglobalfilter').keyup(function(){ 
	    	var dataTable = $('#estimate_tbl').DataTable();
	      	dataTable.search($(this).val()).draw() ;
	    }); 
    }

    $(document).ready(function(){

    	<?php if(isset($SelectedLocation) && !empty($SelectedLocation)){ ?>
    		ViewLocations('<?=$SelectedLocation?>');
    	<?php } else {?>
			ViewLocations('MyLocations');	
    	<?php } ?>
	    
 	});

 	function GoToNewView()
 	{
 		$('#SelectedAcForm').submit();
 	}

 	$(document).ready(function() {
	$('.buttons-excel').hide();
	$("#Export-Excel").on("click", function() {
	    $( '.buttons-excel' ).click();
	});
});
</script>



