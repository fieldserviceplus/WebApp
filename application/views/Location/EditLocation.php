<?php $this->load->view('Template/HeaderView'); ?>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

 <link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_JS_KEY?>&libraries=places"></script>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['Location']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['Location']['title']?></span>
									<span class="panel-title txt-dark"><?=$LocationData->Name;?></span>
								</div>
							</div>
						</div>
					</ul>
					<div class="btn-group pull-right">
						<!-- <div class="dropdown">
							<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
							<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
								<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
									<a><span><strong>Location Actions</strong></span></a>
								</li>
								<li> 
									<a href="<?=SITE_URL;?>Location/Actions/CreateLocation/<?=$LocationData->LocationID?>/"><span>Copy Location</span></a>
								</li>
								<li>
									<a href="#" onclick="DeleteLocation();"><span>Delete Location</span></a>
								</li>
							</ul>
						</div> -->
					</div>
					<div class="btn-group pull-right">
						<a href="<?=SITE_URL;?>Location/Actions/LocationDetails/<?=$LocationData->LocationID?>" class="btn btn-default">Cancel</a>
						<a href="#" class="btn btn-primary" id="EditLocationLink">Save</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
			 	<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Location Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>  -->
				<div class="panel-wrapper collapse in">
					<div class="panel-body"> 
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Location/Actions/EditLocation/<?=$LocationData->LocationID?>" class="form-horizontal" id="EditLocation" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>Location Details</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Name<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Name" id="Name" value="<?=isset($LocationData->Name)?$LocationData->Name:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Account<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="Account" id="Account" class="form-control select2" OnChange="GetPrimaryContactOptions(this.value);GetAccountDetails(this.value);">
														<option value="">Select Account</option>
														<?php $Accounts = $this->ActionsModel->GetAccounts();
															foreach ($Accounts as $Account) { 
																if((isset($LocationData->Account) && $LocationData->Account == $Account->AccountID) || (isset($_GET['Account']) && $_GET['Account'] == $Account->AccountID)){ ?>
																	<option value="<?=$Account->AccountID?>" selected="selected"><?=$Account->AccountName;?></option>
																<?php } else { ?>
																	<option value="<?=$Account->AccountID?>"><?=$Account->AccountName;?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('Account');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Primary Contact</label>
														<div class="col-md-9">
														<?php $Contacts = $this->ActionsModel->GetContacts(); ?>
															<select class="form-control select2 SelectPrimaryContact" name="PrimaryContact" id="PrimaryContact" OnChange="GetContactDetails(this.value);">
															<option value="">Select Primary Contact</option>
															<?php echo form_error('PrimaryContact');  ?>
														</select>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Parent Location</label>
														<div class="col-md-9">
														<select name="ParentLocation" id="ParentLocation" class="form-control select2">
														<option value="">Select Location</option>
														<?php $Locations = $this->ActionsModel->GetLocations(); 
															foreach ($Locations as $Location) { 
															if(isset($LocationData->ParentLocation) && $LocationData->ParentLocation == $Location->LocationID){ ?>
																	<option value="<?=$Location->LocationID?>" selected="selected"><?=$Location->Name?></option>
																<?php } else { ?>
																<option value="<?=$Location->LocationID?>"><?=$Location->Name?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('ParentLocation');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Access Notes</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="AccessNotes" id="AccessNotes" value="<?=isset($LocationData->AccessNotes)?$LocationData->AccessNotes:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Notes</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Notes" id="Notes" value="<?=isset($LocationData->Notes)?$LocationData->Notes:"";?>">
														</div>
													</div>
												</div>

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Owner<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="Owner" id="Owner" class="form-control select2">
														<option value="">Select Owner</option>
														<?php $Users = $this->ActionsModel->GetUsers(); 
															foreach ($Users as $User) { 
															if(isset($LocationData->Owner) && $LocationData->Owner == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else { ?>
																<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('Owner');  ?>
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-3">Location Type<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="LocationType" id="LocationType" class="form-control select2">
														<option value="">Select Location Type</option>
														<?php $LocationTypes = $this->ActionsModel->GetLocationTypes(); 
															foreach ($LocationTypes as $LocationType) { 
															if(isset($LocationData->LocationType) && $LocationData->LocationType == $LocationType->LocationTypeID){ ?>
																	<option value="<?=$LocationType->LocationTypeID?>" selected="selected"><?=$LocationType->LocationType?></option>
																<?php } else { ?>
																<option value="<?=$LocationType->LocationTypeID?>"><?=$LocationType->LocationType?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('LocationType');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Preferred Technician</label>
														<div class="col-md-9">
														<select name="PreferredTechnician" id="PreferredTechnician" class="form-control select2">
														<option value="">Select Preferred Technician</option>
														<?php $PreferredTechnicians = $this->ActionsModel->GetUsers(); 
															foreach ($PreferredTechnicians as $PreferredTechnician){
																if(isset($LocationData->PreferredTechnician) && $LocationData->PreferredTechnician == $PreferredTechnician->UserID){ ?>
																		<option value="<?=$PreferredTechnician->UserID?>" selected="selected"><?=$PreferredTechnician->FirstName?> <?=$PreferredTechnician->LastName?></option>
																	<?php } else { ?>
																		<option value="<?=$PreferredTechnician->UserID?>"><?=$PreferredTechnician->FirstName?> <?=$PreferredTechnician->LastName?></option>
																	<?php } ?>
																<?php } ?>
														</select>
														<?php echo form_error('PreferredTechnician');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Is Active</label>
														<div class="col-md-9">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="IsActive" id="IsActive" value="1" checked="">
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

												</div>
											</div>

											<div class="col-md-12">
												<div class="txt-dark capitalize-font"><strong>Address Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Address</label>
														<div class="col-md-9">
															<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-search"></i>Search Address</button>
														</div>
													</div>
												
													<div class="form-group">
														<label class="col-sm-3 control-label">Street<span class="required-sign">*</span></label>
														<div class="col-sm-9">
																<input type="text" class="form-control" name="Address" id="Address" placeholder="" data-toggle="modal" data-target="#location-picker-modal" value="<?=isset($LocationData->Address)?$LocationData->Address:"";?>">
															<?php echo form_error('Address');  ?>
															<input type="hidden" name="Latitude" id="Latitude" value="<?=isset($LocationData->Latitude)?$LocationData->Latitude:"";?>">
															<input type="hidden" name="Latitude" id="Latitude" value="<?=isset($LocationData->Latitude)?$LocationData->Latitude:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">City<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="City" id="City" value="<?=isset($LocationData->City)?$LocationData->City:"";?>">
															<?php echo form_error('City');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">State<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="State" id="State" value="<?=isset($LocationData->State)?$LocationData->State:"";?>">
															<?php echo form_error('State');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-3">Country<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Country" id="Country" value="<?=isset($LocationData->Country)?$LocationData->Country:"";?>">
															<?php echo form_error('Country');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Postal Code<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="PostalCode" id="PostalCode" value="<?=isset($LocationData->PostalCode)?$LocationData->PostalCode:"";?>">
															<?php echo form_error('PostalCode');  ?>
														</div>
													</div>
												</div>
											
											<!-- /Row -->
											</div>

											<div class="col-md-12">

											<div class="txt-dark capitalize-font"><strong>System Information</strong></div>
											<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-4">Create Date</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$LocationData->CreatedDate?>" readonly="">
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-4">Created By</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$LocationData->CreatedBy?>" readonly="">
														</div>
													</div>

												</div>
												<!--/span-->

												<div class="col-md-6">

												   <div class="form-group">
														<label class="control-label col-md-4">Last Modified Date</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$LocationData->LastModifiedDate?>" readonly="">
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-4">Last Modified By</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$LocationData->LastModifiedBy?>" readonly="">
														</div>
													</div>
												</div>
											</div>


											</div>
											<!-- /Row -->
										</div>

										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="EditLocation" id="EditLocationBtn">Save</button>
															<a href="<?=SITE_URL;?>Location/Actions" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			</div>
			<div id="profile_13" class="tab-pane fade" role="tabpanel">
				
				<?php //$this->load->view('Location/Include/RelatedInformation'); ?>

			</div>
		</div>
		
		</div>

		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">

			<?php //$this->load->view('Location/Include/SchedulingAndNotes'); ?>
			
		</div>

	</div>
	<!-- /Row -->

</div>

<?php //$this->load->view('Location/Modal/CreateNoteView'); ?>

<?php $this->load->view('Template/FooterView')?>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
$('.selectpicker').selectpicker();
</script>

<script type="text/javascript">

$(document).ready(function () {
 
    $('#EditLocation').validate({ 
        rules: {
             Name: {
                required: true
            },
            Account: {
                required: true
            },
            Owner: {
                required: true
            },
            Address: {
                required: true
            },
            City: {
                required: true
            },
            State: {
                required: true
            },
            Country: {
                required: true
            },
            PostalCode: {
                required: true
            },
            LocationType: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
        submitHandler: function (form) { 
        	return true;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    $('#CreateNoteForm').validate({ 
        rules: {
            Subject: {
                required: true
            },
            Body: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    $('.showdatetimepicker').datetimepicker({
		useCurrent: true,
		icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
	});
});

$(document).on('click', "#EditLocationLink", function () { 
	$('#EditLocationBtn').click(); 
});

function DeleteLocation(LocationID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#5fc55f",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>Location/Actions/DeleteLocation/<?=$LocationData->LocationID;?>/";
		}, 500);
    });
	return false;
}

function GetAccountDetails(AccountID){
	$.ajax({
        url: '<?=SITE_URL?>Location/Actions/GetAccountDetails/'+AccountID,
        method: "post",
      /*  data: { AccountID : AccountID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$("#Address").val(result.BillingAddress);
            	$("#City").val(result.BillingCity);
            	$("#State").val(result.BillingState);
            	$("#Country").val(result.BillingCountry);
            	$("#PostalCode").val(result.BillingPostalCode);
            	$("#Latitude").val(result.BillingLatitude);
            	$("#Longitude").val(result.BillingLongitude);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}

function GetContactDetails(ContactID){
	$.ajax({
        url: '<?=SITE_URL?>Location/Actions/GetContactDetails/'+ContactID,
        method: "post",
      /*  data: { AccountID : AccountID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$("#Phone").val(result.PhoneNo);
            	$("#Email").val(result.Email);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}
</script>
<script type="text/javascript">

function GetPrimaryContactOptions(AccountID){
	$.ajax({
        url: '<?=SITE_URL?>Location/Actions/GetPrimaryContactOptions/',
        method: "post",
        data: { AccountID : AccountID, PrimaryContact:'<?=$LocationData->PrimaryContact;?>'},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$(".SelectPrimaryContact").html(result);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}

$(document).ready(function() { 
	GetPrimaryContactOptions(<?=$LocationData->Account?>);
});
</script>

<!-- Set Location -->
<script src="<?=SITE_URL?>dist/js/location-picker.js"></script>
<script type="text/javascript">
$(document).ready(function () {  
	
	<?php if(!empty($LocationData->Latitude) && !empty($LocationData->Longitude)){ ?>
		set_location_in_map("", <?=$LocationData->Latitude;?>, <?=$LocationData->Longitude;?>);
	<?php } else { ?>
		set_location_in_map("");
	<?php } ?>
});
</script>