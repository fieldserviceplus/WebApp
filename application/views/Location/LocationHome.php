
<?php $this->load->view('Template/HeaderView')?>

 <div class="container-fluid pt-25">

		<!-- Row -->
        <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-default card-view">
					<div  class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

								<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 pr-0 pl-0">
								<form method="post" action="<?=SITE_URL?>Location/View/" id="SelectedWoForm">
								
										<div class="col-lg-2 col-md-3 col-sm-3 col-xs-3">
											<label class="control-label mt-10"><strong>View:</strong></label>
										</div>
										<div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 pr-0">
											<select name="SelectedLocation" class="selectpicker" data-style="form-control btn-default btn-outline" onchange="GoToNewView()"> 
												<option disabled="" selected="">Select Views</option>
												<optgroup label="Default Views">
													<option value="MyLocations">My Locations</option>
													<option value="AllLocations">All Locations</option>
													<option value="LocationsCreatedThisWeek">Locations Created This Week</option> 
												</optgroup>
												<optgroup label="My Custom Views">
													<?php $Views = $this->ActionsModel->GetViews();
													foreach ($Views as $View) { ?>
														 <option value="<?=$View->LocationViewID?>"><?=$View->LocationViewName?></option> 
												<?php } ?>
												</optgroup> 
											</select>
										</div>	
								</form>
							</div>		
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="form-group">
								<!-- <div class="btn-group">
									<a href="#" class="btn btn-primary btn-outline" onclick="GoToNewView()">Go</a>
								</div> -->
								<div class="btn-group">
									<div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button"> <i class="fa fa-pencil"></i> <span class="caret"></span></button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li>
												<a><strong>LIST VIEW ACTIONS</strong></a>
											</li>
											<li> 
												<a href="<?=SITE_URL?>Location/View/CreateNewView"><span>Create New View</span></a> 
											</li>
										</ul>
									</div>
								</div>
								</div>
							</div>
							<div class="col-lg-5 col-md-4 col-sm-3 col-xs-12">
								<div class="pull-right">
									<a href="<?=SITE_URL?>Location/Actions/CreateLocation" class="btn btn-primary btn-outline">New Location</a>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-heading">
						<div class="pull-left col-lg-10 col-sm-8 col-md-8">
							<h6 class="panel-title txt-dark">Recent Locations</h6>
						</div>
						<div class="pull-right col-lg-2 col-sm-4 col-md-4">
							<div class="input-group">
								<input id="accountglobalfilter" class="form-control" placeholder="Search" type="text">
								<span class="input-group-addon"><i class="zmdi zmdi-search"></i></span> 
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">

								<div class="table-responsive">
									<table id="invoicetable" class="table table-hover display  pb-30" width="100%">
										<thead>
											<tr>
												<th class="text-center">LC # </th>
												<th class="text-center">Location No</th>
												<th class="text-center">Name</th>
												<th class="text-center">Location Type</th>
												<th class="text-center">Account</th>
												<th class="text-center">Primary Contact</th>
												<th class="text-center">Is Active</th>
												<th class="text-center">Preferred Technician</th>
												<th class="text-center">City</th>
												<th class="text-center">State</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->

	</div>	

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<script type="text/javascript">

    $(document).ready(function(){

    	var dataTable = $('#invoicetable').DataTable({
	       	"responsive": true,
	       	"paging": true,
	       	"bDestroy": true,
	       	"processing": true, //Feature control the processing indicator.
	        "serverSide": true, //Feature control DataTables' server-side processing mode.
	        "sAjaxSource": "<?=SITE_URL?>Location/Actions/RecentLocations",
	       	"columnDefs": [ {
		        sortable: false,
		        targets: 'no-sort',
		        class: "index",
		        targets: 0
	       	} ],
	       	"aaSorting": [],
	       	/*"order": [[ 2, 'asc' ]],*/
	       	"fixedColumns": true, 
	       	"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 ] }],
	       	"columns": [
	       		{ className: "text-center" },
	            { className: "text-left" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" }
	         ], 
	         "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {  
	                var oSettings = $("#invoicetable").dataTable().fnSettings(); 
	                $("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
	                //$(nRow).attr("id",'row_' + aData.id); 
	                //return nRow;
	          },
	          "dom": 'rt<"bottom"ilp><"clear">' 
	    });
	    $('#accountglobalfilter').keyup(function(){
	    	var dataTable = $('#invoicetable').DataTable();
	      	dataTable.search($(this).val()).draw() ;
	    }); 
 	});

 	function GoToNewView()
 	{
 		$('#SelectedWoForm').submit();
 	}



</script>


