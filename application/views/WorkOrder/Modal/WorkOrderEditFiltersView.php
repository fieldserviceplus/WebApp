<!-- modal --> 
<div class="modal fade" id="account-edit-filters-view-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Edit Filters</h5>
			</div>
			<form class="form-horizontal" id="FiltersViewForm" method="post" action="<?=SITE_URL;?>WorkOrder/View/EditFiltersCustomView/<?=$SelectedWorkOrder?>">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<?php foreach ($WorkOrderCustomViewData['WorkOrderFilterData'] as $key => $value) { ?>
														
							<div class="form-group">
								<div class="col-md-4">
									<select class="form-control" name="FilterFields[]">
									<?php $Fields = $this->ViewModel->WorkOrderViewFields(); 

									foreach($Fields as $Field){ 

										if($value->FilterField == $Field->FieldName){?>

											<option value="<?=$Field->FieldName;?>" selected=""><?=$Field->FieldName;?></option>
										<?php } else {?>
											<option value="<?=$Field->FieldName;?>"><?=$Field->FieldName;?></option>
										<?php } ?>
									<?php } ?>
										
									</select>
								</div>
								<div class="col-md-3">
									<select class="form-control" name="FilterConditions[]">
										<option value="Equals" <?=($value->FilterCondition == "Equals")?"selected=''":"";?>>Equals</option>
										<option value="NotEqualTo" <?=($value->FilterCondition == "NotEqualTo")?"selected=''":"";?>>Not Equal To</option>
										<option value="LessThan" <?=($value->FilterCondition == "LessThan")?"selected=''":"";?>>Less Than</option>
										<option value="GreaterThan" <?=($value->FilterCondition == "GreaterThan")?"selected=''":"";?>>Greater Than</option>
										<option value="LessOREqualTo" <?=($value->FilterCondition == "LessOREqualTo")?"selected=''":"";?>>Less OR Equal To</option>
										<option value="GreaterOREqualTo" <?=($value->FilterCondition == "GreaterOREqualTo")?"selected=''":"";?>>Greater OR Equal To</option>
										<option value="Contains" <?=($value->FilterCondition == "Contains")?"selected=''":"";?>>Contains</option>
										<option value="StartsWith" <?=($value->FilterCondition == "StartsWith")?"selected=''":"";?>>Starts With</option>
										<option value="DoesNotContain" <?=($value->FilterCondition == "DoesNotContain")?"selected=''":"";?>>Does Not Contain</option>
									</select>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" name="FilterValues[]" value="<?=$value->FilterValue?>">
								</div>
								<?php if($key != 0) {?>
								<div class="col-md-1 btn-group btn-group-vertical pl-0">
									<button type="button" id="" class="btn btn-default btn-icon-anim btn-circle remove_field_filter_criteria"><i class="fa fa-close"></i></button>
								</div>
								<?php } ?>
							</div>

						<?php } ?>

						<div class="input_fields_wrap" >
						</div>

						<div class="col-md-12 pb-15">
							<button type="button" class="btn btn-default btn-outline add_field_button">Add Filter +</button>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>