<!-- modal -->
<div class="modal fade" id="account-edit-sharing-view-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Edit Sharing</h5>
			</div>
			<form class="form-horizontal" method="post" action="<?=SITE_URL;?>WorkOrder/View/EditSharingCustomView/<?=$SelectedWorkOrder?>">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group" >
							<div class="col-sm-12">
								<div class="radio radio-primary">
									<input name="RestrictVisibility" id="radio11" value="VisibleOnlyToMe" <?=($WorkOrderCustomViewData['WorkOrderViewData']->RestrictVisibility == "VisibleOnlyToMe")?"checked=''":"";?> type="radio" >
									<label for="radio11"> Visible only to me </label>
								</div>
								<div class="radio radio-primary">
									<input name="RestrictVisibility" id="radio12" value="VisibleToEveryone" type="radio" <?=($WorkOrderCustomViewData['WorkOrderViewData']->RestrictVisibility == "VisibleToEveryone")?"checked=''":"";?>>
									<label for="radio12"> Visible to everyone </label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>