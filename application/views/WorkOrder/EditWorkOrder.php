<?php $this->load->view('Template/HeaderView'); ?>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

 <link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_JS_KEY?>&libraries=places"></script>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<!--Signature Pad CSS -->
<!-- <link href="<?=SITE_URL?>dist/css/signature_pad.css" rel="stylesheet" type="text/css"> -->

<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10 pb-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['WorkOrder']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['WorkOrder']['title']?></span>
									<span class="panel-title txt-dark"><?=$WorkOrderData->Subject;?></span>
								</div>
							</div>
						</div>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div  class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="col-lg-7 col-md-5 col-sm-4 col-xs-12">
							<!-- <div class="form-group"> -->
								<ul role="tablist" class="nav nav-pills" id="myTabs_13">
									<li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_13" href="#home_13">Detail</a></li>
									<li role="presentation" class=""><a data-toggle="tab" id="profile_tab_13" role="tab" href="#profile_13" aria-expanded="false">Related</a></li>
								</ul>
							<!-- </div> -->
						</div>

						<div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">

							<div class="btn-group pull-right">
									<!-- <div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li>
												<a><span><strong>Add New Actions</strong></span></a>
											</li>
											<li>
												<a href="#" class="add_new_scheduling_line"><span>Schedule a Time</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#create-line-items-modal"><span>New Line Item</span></a>
											</li>
											<?php if(MY_Model::getChemicalSettings()){ ?>
											<li>
												<a href="#" data-toggle="modal" data-target="#create-chemical-line-items-modal"><span>New Chemical</span></a>
											</li>
											<?php } ?>
											<li> 
												<a href="#" data-toggle="modal" data-target="#CreateNewFile"><span>New File</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>Task/Actions/CreateTask/?WorkOrder=<?=$WorkOrderData->WorkOrderID?>"><span>New Task</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#CreateNewNote"><span>New Note</span></a>
											</li>
											<li>
												<a href="#"><span>Generate Document</span></a>
											</li>
											<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
												<a><span><strong>WorkOrder Actions</strong></span></a>
											</li>
											<li> 
												<a href="<?=SITE_URL;?>WorkOrder/Actions/CreateWorkOrder"><span>Create Recurring Work Order</span></a>
											</li>
											<li> 
												<a href="<?=SITE_URL;?>WorkOrder/Actions/CreateWorkOrder/<?=$WorkOrderData->WorkOrderID?>/"><span>Copy Work Order</span></a>
											</li>
											<li>
												<a href="#" onclick="DeleteWorkOrder(<?=$WorkOrderData->WorkOrderID?>);"><span>Delete Work Order</span></a>
											</li>
										</ul>
									</div> -->
								</div>
								<div class="btn-group pull-right">
									<a href="<?=SITE_URL;?>WorkOrder/Actions/WorkOrderDetails/<?=$WorkOrderData->WorkOrderID?>" class="btn btn-default">Cancel</a>
									<a href="#" class="btn btn-primary" id="EditWorkOrderLink">Save</a>

								</div>	
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
			 	<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">WorkOrder Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>  -->
				<div class="panel-wrapper collapse in">
					<div class="panel-body"> 
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>WorkOrder/Actions/EditWorkOrder/<?=$WorkOrderData->WorkOrderID?>" class="form-horizontal" id="EditWorkOrder" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font">
													<strong>Work Order Information</strong>
													<span class="pull-right" style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong>
													</span>
												</div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-4">Work Order No</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$WorkOrderData->WorkOrderNo;?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Assigned To<span class="required-sign">*</span></label>
														<div class="col-md-8">
														<select name="AssignedTo" id="AssignedTo" class="form-control select2">
														<option value="">Select Assigned To</option>
														<?php $Users = $this->ActionsModel->GetUsers();
															foreach ($Users as $User) { 
																if($WorkOrderData->AssignedTo == $User->UserID){?>
																	<option value="<?=$User->UserID?>" selected=""><?=$User->FirstName.' '.$User->LastName?></option>
															<?php } else {?>
																	<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
															<?php }
															} ?>
														</select>
														<?php echo form_error('AssignedTo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Account<span class="required-sign">*</span></label>
														<div class="col-md-8">
														<select name="Account" id="Account" class="form-control select2" OnChange="GetPrimaryContactOptions(this.value);">
														<option value="">Select Account</option>
														<?php $Accounts = $this->ActionsModel->GetAccounts();
															foreach ($Accounts as $Account) { 
																if($WorkOrderData->Account == $Account->AccountID){ ?>
																	<option value="<?=$Account->AccountID?>" selected=""><?=$Account->AccountName;?></option>
																	<?php } else { ?>
																	<option value="<?=$Account->AccountID?>"><?=$Account->AccountName;?></option>
																<?php }
															} ?>
														</select>
														<?php echo form_error('Account');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Primary Contact</label>
														<div class="col-md-8">

														<?php //$Contacts = $this->ActionsModel->GetContacts(); ?>
															<select class="form-control select2 SelectPrimaryContact" name="PrimaryContact" id="PrimaryContact">
															<option value="">Select Contact</option>
															<?php /*foreach ($Contacts as $Contact) { 
																if($WorkOrderData->PrimaryContact == $Contact->ContactID){?>
																		<option value="<?=$Contact->ContactID?>" selected=""><?=$Contact->FirstName.' '.$Contact->LastName?></option>
																	<?php } else {?>
																		<option value="<?=$Contact->ContactID?>"><?=$Contact->FirstName.' '.$Contact->LastName?></option>
															<?php }}*/ ?>
														</select>
														<?php echo form_error('PrimaryContact');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Parent Work Order</label>
														<div class="col-md-8">

														<?php $GetWorkOrders = $this->ActionsModel->GetWorkOrders(); ?>
															<select class="form-control select2" name="ParentWorkOrder" id="ParentWorkOrder">
															<option value="">Select Work Order</option>
															<?php foreach ($GetWorkOrders as $GetWorkOrder) { 
															if($WorkOrderData->ParentWorkOrder == $GetWorkOrder->WorkOrderID){ ?>
																	<option value="<?=$GetWorkOrder->WorkOrderID?>" selected=""><?=$GetWorkOrder->Subject?></option>
															<?php } else {?>
																<option value="<?=$GetWorkOrder->WorkOrderID?>"><?=$GetWorkOrder->Subject?></option>
															<?php } } ?>
															<?php echo form_error('ParentWorkOrder');  ?>
														</select>
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-4">Subject<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Subject" id="Subject" value="<?=$WorkOrderData->Subject;?>">
															<?php echo form_error('Subject');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Description<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Description" id="Description" value="<?=$WorkOrderData->Description;?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Pop Up Reminder</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="PopUpReminder" id="PopUpReminder" value="<?=$WorkOrderData->PopUpReminder;?>">
															<?php echo form_error('PopUpReminder');  ?>
														</div>
													</div>

												</div>
												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-4">Type<span class="required-sign">*</span></label>
														<div class="col-md-8">

														<?php $WorkOrderTypes = $this->ActionsModel->GetWorkOrderTypes(); ?>
															<select class="form-control" name="WorkOrderType" id="WorkOrderType">
															<option value="">Select Work Order Type</option>
															<?php foreach ($WorkOrderTypes as $WorkOrderType) { 
																if($WorkOrderData->WorkOrderType == $WorkOrderType->WorkOrderTypeID){ ?>
																	<option value="<?=$WorkOrderType->WorkOrderTypeID?>" selected=""><?=$WorkOrderType->WorkOrderType?></option>
																<?php } else {?>
																	<option value="<?=$WorkOrderType->WorkOrderTypeID?>"><?=$WorkOrderType->WorkOrderType?></option>
															<?php } } ?>
														</select>
														<?php echo form_error('WorkOrderType');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Status<span class="required-sign">*</span></label>
														<div class="col-md-8">

														<?php $GetWOStatus = $this->ActionsModel->GetWOStatus(); ?>
															<select class="form-control" name="WOStatus" id="WOStatus">
															<option value="">Select Status</option>
															<?php foreach ($GetWOStatus as $WOStatus) { 
																if($WorkOrderData->WOStatus == $WOStatus->WOStatusID){ ?>
																	<option value="<?=$WOStatus->WOStatusID?>" selected=""><?=$WOStatus->Status?></option>
																<?php } else { ?>
																	<option value="<?=$WOStatus->WOStatusID?>"><?=$WOStatus->Status?></option>
															<?php }
															 } ?>
														</select>
														<?php echo form_error('WOStatus');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-4">Priority<span class="required-sign">*</span></label>
														<div class="col-md-8">

														<?php $WOPriorities = $this->ActionsModel->WOPriorities(); ?>
															<select class="form-control" name="WOPriority" id="WOPriority">
															<option value="">Select Priority</option>
															<?php foreach ($WOPriorities as $WOPriority) { 
																if($WorkOrderData->WOPriority == $WOPriority->WOPriorityID){ ?>
																	<option value="<?=$WOPriority->WOPriorityID?>" selected=""><?=$WOPriority->Priority?></option>
																<?php } else { ?>
																	<option value="<?=$WOPriority->WOPriorityID?>"><?=$WOPriority->Priority?></option>
																<?php } } ?>
														</select>
														<?php echo form_error('WOPriority');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Category<span class="required-sign">*</span></label>
														<div class="col-md-8">

														<?php $WOCategories = $this->ActionsModel->WOCategories(); ?>
															<select class="form-control" name="WOCategory" id="WOCategory">
															<option value="">Select Category</option>
															<?php foreach ($WOCategories as $WOCategory) { 
															if($WorkOrderData->WOCategory == $WOCategory->WOCategoryID){ ?>
																	<option value="<?=$WOCategory->WOCategoryID?>" selected=""><?=$WOCategory->CategoryName?></option>
															<?php } else { ?>
																	<option value="<?=$WOCategory->WOCategoryID?>"><?=$WOCategory->CategoryName?></option>
															<?php } } ?>
														</select>
														<?php echo form_error('WOCategory');  ?>
														</div>
													</div>

												</div>
											</div>
											<div class="col-md-12">

											<div class="txt-dark capitalize-font"><strong>Address Information</strong></div>
											<hr class="light-grey-hr">

												<!--<div class="col-md-6">	

													 <div class="form-group">
														<label class="control-label col-md-4">Start Date Time</label>
														<div class="col-md-8">
															<input type="text" class="form-control showdatetimepicker" name="StartDateTime" id="StartDateTime" value="<?=$WorkOrderData->StartDate;?>">
															<?php echo form_error('StartDateTime'); ?>
														</div>
													</div> -->

													<!-- <div class="form-group">
														<label class="control-label col-md-4">End Date Time</label>
														<div class="col-md-8">
															<input type="text" class="form-control showdatetimepicker" name="EndDateTime" id="EndDateTime" value="<?=$WorkOrderData->EndDate;?>">
															<?php echo form_error('EndDateTime');?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Is Recurring?</label>
														<div class="col-md-8">
															<div class="checkbox checkbox-primary">
																<input id="IsRecurring" name="IsRecurring" type="checkbox" value="1" <?=($WorkOrderData->IsRecurring==1)?"checked=''":""?> >
																<label for="checkbox2"></label>
																<input id="WORecurrenceID" name="WORecurrenceID" class="WORecurrenceID" type="hidden" value="<?=$WorkOrderData->WORecurrenceID;?>">
															</div>
														</div>
													</div> 

												</div>-->
												<div class="col-md-6">	
													<div class="form-group">
														<label class="control-label col-md-4">Address</label>
														<div class="col-md-8">
																<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-search"></i>Search Address</button>
														</div>
													</div>

													<div class="form-group">
														<label class="col-md-4 control-label">Street<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Address" id="Address" placeholder="" value="<?=$WorkOrderData->Address;?>">
															<?php echo form_error('Address');  ?>
															<input type="hidden" name="Latitude" id="Latitude" value="<?=$WorkOrderData->Latitude;?>">
															<input type="hidden" name="Longitude" id="Longitude" value="<?=$WorkOrderData->Longitude;?>">
														</div>
													</div>

													<!-- <div class="form-group">
														<label class="control-label col-md-4">Street</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Street" id="Street" value="<?=$WorkOrderData->Street;?>">
															<?php //echo form_error('Street');  ?>
														</div>
													</div> -->

													<div class="form-group">
														<label class="control-label col-md-4">City<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="City" id="City"  value="<?=$WorkOrderData->City;?>">
															<?php echo form_error('City');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">State<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="State" id="State"  value="<?=$WorkOrderData->State;?>">
															<?php echo form_error('State');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-4">Country<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Country" id="Country" value="<?=$WorkOrderData->Country;?>">
															<?php echo form_error('Country');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Postal Code<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="PostalCode" id="PostalCode" value="<?=$WorkOrderData->PostalCode;?>">
															<?php echo form_error('PostalCode');  ?>
														</div>
													</div>

												</div>
												<!--/span-->

											</div>
											<div class="col-md-12">

											<div class="txt-dark capitalize-font"><strong>Financials</strong></div>
											<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-4">Sub Total</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="SubTotal" value="<?=number_format((double)$WOFinancials['SubTotal'], 2, '.', '');?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Discount</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Discount" value="<?=number_format((double)$WOFinancials['Discount'], 2, '.', '');?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Tax</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Tax" value="<?=number_format((double)$WOFinancials['Tax'], 2, '.', '');?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Total Price</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="TotalPrice" value="<?=number_format((double)$WOFinancials['TotalPrice'], 2, '.', '');?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Grand Total</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="GrandTotal" value="<?=number_format((double)$WOFinancials['GrandTotal'], 2, '.', '');?>" readonly="">
														</div>
													</div>

												</div>
												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-4">Signature</label>
														<div class="col-md-8">
															<?php if(!empty($WorkOrderData->Signature)){?>
					                                            <img id="collection_signature_img" style="width: 256px;height: 76px;" src="<?=$WorkOrderData->Signature;?>" />
					                                          <?php } ?>
														</div>
													</div>

													<!-- <div class="form-group">
														<label class="control-label col-md-4">Signature</label>
				                                       <div class="col-md-6">
					                                        <div id="signature-pad-1" class="m-signature-pad">
					                                            <img id="collection_signature_img" src="<?=$WorkOrderData->Signature;?>" />
					                                            <div id="collection_signature_pad" class="m-signature-pad--body" >
					                                                <canvas></canvas> 
					                                            </div> 
					                                            <div id="collection_signature_hidden">
					                                                <input type="hidden" name="Signature" id="Signature">
					                                            </div>
					                                        </div>
				                                        </div>
			                                            <div class="col-md-1 pull-left">
			                                        	 	<button type="button" id="collection_signature_clear_btn" class="btn btn-default btn-outline" data-action="clear1"><i class="fa fa-eraser"></i></button>
			                                        	 	<button type="button" id="collection_signature_close" class="btn btn-default btn-outline"><i class="fa fa-close"></i></button> 
			                                        	</div>
													</div> -->

													<div class="form-group">
														<label class="control-label col-md-4">Line Item Cnt</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="LineItemCount" id="LineItemCount" value="<?=count($WOLineItems);?>" readonly="">
														</div>
													</div>


												</div>

											</div>

											<!-- Start Custom Fields Row -->
											<?php $this->load->view('Setup/Customize/Fields/CustomFieldsForm', array('Object' => 'WorkOrder', 'ObjectID' => $WorkOrderData->WorkOrderID)); ?>
											<!-- End Custom Fields Row -->

											<div class="col-md-12">

											<div class="txt-dark capitalize-font"><strong>System Information</strong></div>
											<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-4">Create Date</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$WorkOrderData->CreatedDate?>" readonly="">
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-4">Created By</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$WorkOrderData->CreatedByName?>" readonly="">
														</div>
													</div>

												</div>
												<!--/span-->

												<div class="col-md-6">

												   <div class="form-group">
														<label class="control-label col-md-4">Last Modified Date</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$WorkOrderData->LastModifiedDate?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Last Modified By</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$WorkOrderData->LastModifiedByName?>" readonly="">
														</div>
													</div>
												</div>
											</div>


											</div>
											<!-- /Row -->
										</div>

										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="EditWorkOrder" id="EditWorkOrderBtn">Save</button>
															<a href="<?=SITE_URL;?>WorkOrder/Actions" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			</div>
			<div id="profile_13" class="tab-pane fade" role="tabpanel">
				
				<?php $this->load->view('WorkOrder/Include/RelatedInformation'); ?>

			</div>
		</div>
		
		</div>

		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">

			<?php $this->load->view('WorkOrder/Include/SchedulingAndNotes'); ?>

		</div>

	</div>
	<!-- /Row -->

</div>

<?php //$this->load->view('WorkOrder/Modal/CreateNoteView'); ?>
<?php $this->load->view('WorkOrder/Modal/LocationPickerView'); ?>
<?php $this->load->view('WorkOrder/Modal/RecurringTaskView'); ?>
<?php $this->load->view('WorkOrder/Modal/CreateLineItemsView'); ?>
<?php $this->load->view('WorkOrder/Modal/CreateChemicalLineItems'); ?>
<?php 
$data =  array('data' => array('RelatedTo' => 'WorkOrder', 'What' => $WorkOrderData->WorkOrderID, 'AssignedTo' => $WorkOrderData->AssignedTo));
$this->load->view('Common/Modal/CreateNewFile', $data); 
$this->load->view('Common/Modal/CreateNewNote', $data); ?>

<?php $this->load->view('Template/FooterView')?>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Use for common object  -->
<script src="<?=SITE_URL?>dist/js/common.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<!-- Signature Pad JavaScript Start -->
<!-- <script type="text/javascript" src="<?=SITE_URL?>dist/js/signature_pad.js"></script>
<script type="text/javascript">
var wrapper1 = document.getElementById("signature-pad-1"),
        canvas1 = wrapper1.querySelector("canvas"),
        signaturePad1;

function resizeCanvas(canvas) {
    var ratio = Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}

resizeCanvas(canvas1);
signaturePad1 = new SignaturePad(canvas1);
canvas1.addEventListener('mouseup', function (e) {
    document.getElementById("Signature").value = signaturePad1.toDataURL();
});
var clearButton1 = document.querySelector("[data-action=clear1]");
clearButton1.addEventListener("click", function (event) {
    signaturePad1.clear();
    document.getElementById("Signature").value = '';
});

function setSignImg(){
  if (!signaturePad1.isEmpty()) { 
       document.getElementById("Signature").value = signaturePad1.toDataURL();
  }
} 

$(document).ready(function () {
	$( "#collection_signature_pad" ).css('display','none');  
	$( "#collection_signature_clear_btn" ).css('display','none');           
	$( "#collection_signature_close" ).click(function() {
	    $( "#collection_signature_pad" ).css('display','block');
	    $( "#collection_signature_clear_btn" ).css('display','block');

	    $( "#collection_signature_close" ).css('display','none');
	    $( "#collection_signature_img" ).css('display','none');
	});

	<?php if($WorkOrderData->Signature==''){ ?>
	        $( "#collection_signature_pad" ).css('display','block');  
	        $( "#collection_signature_clear_btn" ).css('display','block');   
	        $( "#collection_signature_close" ).css('display','none');
	        $( "#collection_signature_img" ).css('display','none');
	<?php } ?>
});
</script> -->
<!-- Signature Pad JavaScript End -->

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
$('.selectpicker').selectpicker();
</script>

<!-- Bootstrap Touchspin JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
<script type="text/javascript">
	$(".vertical-spin").TouchSpin({
		min: 1,
        max: 150,
		verticalbuttons: true,
		verticalupclass: 'ti-plus',
		verticaldownclass: 'ti-minus'
	});
	var vspinTrue = $(".vertical-spin").TouchSpin({
		verticalbuttons: true
	});
	if (vspinTrue) {
		$('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
	}

	$(".vertical-spin-occurrence").TouchSpin({
		min: 1,
		verticalbuttons: true,
		verticalupclass: 'ti-plus',
		verticaldownclass: 'ti-minus'
	});

	/* Recurring Setting */
function RepeatsChange(Repeats){ 
	if(Repeats == 'Daily'){ 
			
		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 150});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('day on');
	} else if(Repeats == 'Weekly'){ 

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 104});
	
		$( "#days-cb" ).show();
		$("input[name*='RepeatOn[]']").attr("required",true);
		$('.interval_every_txt').html('week on');
	} else if(Repeats == 'Monthly'){

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 60});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('month on');
	} else if(Repeats == 'Yearly'){

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 10});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('year on');
	} else if(Repeats == 'Periodically'){

		$(".vertical-spin").trigger("touchspin.updatesettings", {max: 150});

		$( "#days-cb" ).hide();
		$("input[name*='RepeatOn[]']").attr("required",false);
		$('.interval_every_txt').html('periodical on');
	}
}
$("input[name='Ends']").change(function() { 
	if($("input[name='Ends']:checked").val()=='On') {
       $("#EndsOnDate").attr("required",true);
 	} else {
 		$("#EndsOnDate").attr("required",false);
 	}
}); 

$('#IsRecurring').change(function() { 
     if(this.checked) {
        $('#RecurringModal').modal({
		    backdrop: 'static',
		    keyboard: false  // to prevent closing with Esc button (if you want this too)
		}); 
         
     } else {

     	if($('.WORecurrenceID').val() != ''){

     		var WORecurrenceID = $('.WORecurrenceID').val();
	     	$.ajax({
	            url: '<?=SITE_URL?>Account/Actions/removeRecurrenceSetting/',
	            data: { WORecurrenceID:WORecurrenceID },
	            method: "post",
	            dataType: 'html',
	            success: function (result) {
	                if (result != '') {
	                   $('.WORecurrenceID').val(''); 
	                   return true;  	
	                } else {
	                    alert("Something went wrong.Please try again!");
	                    return false;
	                }
	            }
	        });	
	     }
     }
});

$('#RecurringForm').validate({
	errorPlacement: function(){
	    return false;
	},
	/* submitHandler: function (form) { 
	    return false; 
	},*/
	highlight: function(element) {
		$(element).closest('.form-group').addClass('has-error');
	},
	unhighlight: function(element) {
	    $(element).closest('.form-group').removeClass('has-error');
	}
});	

$(document).on('click', "#RecurrenceSave", function () {
    if ($("#RecurringForm").valid()) {	
        $.ajax({
            url: '<?=SITE_URL?>WorkOrder/Actions/saveRecurrenceSetting/',
            data: $("#RecurringForm").serialize(),
            method: "post",
            dataType: 'html',
            success: function (result) {
                if (result != '') {
                   	$('.WORecurrenceID').val(result); 
                   
                   	$('#RecurringModal').modal('hide'); 
                   	return true;  	
                } else {
                    alert("Something went wrong.Please try again!");
                    return false;
                }
            }
        });
    }
    return false;
});

$(document).on('click', "#RecurrenceClose", function () { 
   	$('#IsRecurring').prop('checked', false);
});
</script>

<script type="text/javascript">

$(document).ready(function () {
 
    $('#EditWorkOrder').validate({ 
        rules: {
            AssignedTo: {
                required: true
            },
            Account: {
                required: true
            },
            /*ParentWorkOrder: {
                required: true
            },*/
            Subject: {
                required: true
            },
            Description: {
                required: true
            },
            Address: {
                required: true
            },
           /* Street: {
                required: true
            },*/
            City: {
                required: true
            },
            State: {
                required: true
            },
            Country: {
                required: true
            },
            PostalCode: {
                required: true
            },
            WorkOrderType: {
                required: true
            },
            WOStatus: {
                required: true
            },
            WOPriority: {
                required: true
            },
            WOCategory: {
                required: true
            },
            /*PopUpReminder: {
                required: true
            },*/
            StartDateTime: {
                required: true
            },
            EndDateTime: {
                required: true
            },
           /* PrimaryContact: {
                required: true
            },*/
        },
        errorPlacement: function(){
            return false;
        },
        submitHandler: function (form) { 
            setSignImg();

        	return true;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    /*$('#CreateNoteForm').validate({ 
        rules: {
            Subject: {
                required: true
            },
            Body: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });*/


    /*$(".datepicker").datepicker();*/
     /*$('.datepicker').datepicker({
	    format: 'mm/dd/yyyy',
	    startDate: '-3d'
	});*/


    $('.showdatetimepicker').datetimepicker({
			useCurrent: true,
			icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
		});

	$('.timepicker').datetimepicker({
			format: 'LT',
			useCurrent: false,
			icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
		}).data("DateTimePicker").date(moment());

});

$(document).on('click', "#EditWorkOrderLink", function () { 
	$('#EditWorkOrderBtn').click(); 
});

/*function DeleteWorkOrder(WorkOrderID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#f8b32d",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>WorkOrder/Actions/DeleteWorkOrder/<?=$WorkOrderData->WorkOrderID;?>/";
		}, 500);
    });
	return false;
}*/

function DeleteWorkOrder(WorkOrderID)
{ 
	<?php if($WorkOrderData->IsRecurring == 0){ ?>

	    swal({   
	        title: "Are you sure?",   
	        text: "You will not be able to recover this data!",   
	        type: "warning",   
	        showCancelButton: true,   
	        confirmButtonColor: "#5fc55f",   
	        confirmButtonText: "Yes, delete it!",   
	        closeOnConfirm: false,
	        showLoaderOnConfirm: true,
	    }, function(){   
	        swal("Deleted!", "Your data has been deleted.", "success"); 
	        setTimeout(function () {
			    window.location.href = "<?=SITE_URL;?>WorkOrder/Actions/DeleteWorkOrder/<?=$WorkOrderData->WorkOrderID;?>/";
			}, 500);
	    });
		return false;

	<?php } else { ?>

		var inviteDevInput = '<div class="text-left pl-10">'
				+'<div class="form-group mb-30">'
				+	'<div class="radio radio-primary">'
				+		'<input type="radio" name="dltWorkOrder" id="radio1" value="OnlyThisOne" checked="">'
				+		'<label for="radio1">'
				+			'This Work Order only'
				+		'</label>'
				+	'</div>'
				+	'<div class="radio radio-primary">'
				+		'<input type="radio" name="dltWorkOrder" id="radio2" value="ThisAndFutureThisSeries">'
				+		'<label for="radio2">'
				+			'This and all future Work Orders in this series'
				+		'</label>'
				+	'</div>'
				+	'<div class="radio radio-primary">'
				+		'<input type="radio" name="dltWorkOrder" id="radio3" value="ThisAndOpenThisSeries">'
				+		'<label for="radio3">'
				+			'This and all open Work Orders in this series'
				+		'</label>'
				+	'</div>'
				+	'<div class="radio radio-primary">'
				+		'<input type="radio" name="dltWorkOrder" id="radio4" value="AllThisSeries">'
				+		'<label for="radio4">'
				+			'All Work Orders in this series'
				+		'</label>'
				+	'</div>'
				+'</div></div>';
		var recordType;
		swal({
					title: "Delete recurring Work Order",
					text: inviteDevInput,
					html: true,
					showCancelButton: true,
					confirmButtonColor: "#5fc55f",
					confirmButtonText: "OK",
					cancelButtonText: "Cancel",
					closeOnConfirm: false,
					closeOnCancel: true
				}, // Function that validates email address through a regular expression.
				function () {
					var Action = $("input[name=dltWorkOrder]:checked").val();
					swal("Deleted!", "Your data has been deleted.", "success"); 
			        setTimeout(function () {
					    window.location.href = "<?=SITE_URL;?>WorkOrder/Actions/DeleteWorkOrder/<?=$WorkOrderData->WorkOrderID;?>/"+Action;
					}, 500);
		    	});

    <?php } ?>
}
</script>

<!-- Set Location -->
<script src="<?=SITE_URL?>dist/js/location-picker.js"></script>
<script type="text/javascript">
$(document).ready(function () {

	<?php if(!empty($WorkOrderData->Latitude) && !empty($WorkOrderData->Longitude)){ ?>
		set_location_in_map("", <?=$WorkOrderData->Latitude;?>, <?=$WorkOrderData->Longitude;?>);
	<?php } else { ?>
		set_location_in_map("");
	<?php } ?>
});
</script>

<script type="text/javascript">
/* Scheduling Start */ 
$(document).ready(function() {

    var max_fields      = 20; //maximum input boxes allowed
    var wrapper         = $(".new_scheduling_line_wrap"); //Fields wrapper
   
    var x = 1;//<?=count($Events);?>; //initlal text box count
    $(".add_new_scheduling_line").click(function(e){ //on add input button click

        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div id="SchedulingLine'+x+'"><div class="form-wrap">'
					+	'<form action="#" class="form-horizontal schedulingfrmclss" id="SchedulingForm'+x+'">'
					+   '<input type="hidden" name="Subject" value="<?=$WorkOrderData->Subject;?>">'
					+   '<input type="hidden" name="WorkOrderNo" value="<?=$WorkOrderData->WorkOrderNo;?>">'
					+   '<input type="hidden" name="WorkOrderID" value="<?=$WorkOrderData->WorkOrderID;?>">'
					+   '<input type="hidden" name="Who" value="<?=$WorkOrderData->PrimaryContact;?>">'
					+   '<input type="hidden" name="Description" value="<?=$WorkOrderData->Description;?>">'
					+	'<div class="form-body">'
					+		'<div class="form-group">'
					+			'<label class="col-md-3 control-label text-left">Assigned To</label>'
					+			'<div class="col-md-6">'
					+				'<select name="AssignedTo" id="AssignedTo" class="form-control select2">'
					+				'<option value="">Select Owner</option>'
									<?php $Users = $this->ActionsModel->GetUsers();
										foreach ($Users as $User) { 
											if($WorkOrderData->AssignedTo == $User->UserID){?>
					+							'<option value="<?=$User->UserID?>" selected=""><?=$User->FirstName.' '.$User->LastName?></option>'
										<?php } else {?>
					+							'<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>'
										<?php }
										} ?>
					+				'</select>'
					+			'</div>'
					+		'</div>'
					+		'<div class="form-group">'
					+			'<label class="col-md-3 control-label text-left">Start</label>'
					+			'<div class="col-md-6">'
					+				'<input type="text" class="form-control showdatetimepicker" name="EventStartDate" value="<?=date("m/d/Y")?>" required>'
					+			'</div>'
					+		'</div>'
					+		'<div class="form-group">'
					+			'<label class="col-md-3 control-label text-left">End</label>'
					+			'<div class="col-md-6">'
					+				'<input type="text" class="form-control showdatetimepicker" name="EventEndDate" value="<?=date("m/d/Y")?>" required>'
					+			'</div>'
					+		'</div>'
					+	'</div>'
					+	'<div class="pull-right">'
					+		'<button type="button" class="btn btn-default remove_new_scheduling_line" data-dismiss="modal">Close</button> '
					+		'<button type="button" class="btn btn-primary" OnClick="SaveScheduling('+x+')">Save</button>'
					+	'</div>'
					+	'</form>'
					+ '<div class="clearfix"></div>'
					+ '<hr class="light-grey-hr">'
					+ '</div>'
					+ '</div>'); //add input box
		}
		$(".select2").select2();
		$('.showdatetimepicker').datetimepicker({
			useCurrent: true,
			icons: {
	                time: "fa fa-clock-o",
	                date: "fa fa-calendar",
	                up: "fa fa-arrow-up",
	                down: "fa fa-arrow-down"
	            }
		});

		SetViewMoreBtnInScheduling();
    });
    $(wrapper).on("click",".remove_new_scheduling_line", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.form-wrap').remove(); x--;

        SetViewMoreBtnInScheduling();
        
    })
});

function SetViewMoreBtnInScheduling(){
	if($(".schedulingfrmclss").length > 5){
		$(".scheduling-view-more-btn").html('<div class="pull-right">'
			+ '<a href="<?=SITE_URL?>WorkOrder/Actions/RelatedObject/<?=$WorkOrderData->WorkOrderID;?>/Events" type="button" class="btn btn-primary btn-outline">View All</a>'
			+ '</div>');
	} else {
		$(".scheduling-view-more-btn").html('');
	}
}

function SaveScheduling(x)
{
 	 $.ajax({
            url: '<?=SITE_URL?>WorkOrder/Actions/SaveScheduling/',
            data: $("#SchedulingForm"+x).serialize(),
            method: "post",
            dataType: 'html',
            success: function (result) {
                if (result != '') {
                	$(".new_scheduling_line_wrap").html(result);

                if($(".schedulingfrmclss").length > 0)
            		$("button.add_new_scheduling_line").html('<i class="fa fa-plus"></i> Add another time');

                  return true;  	
                } else {
                    alert("Something went wrong.Please try again!");
                    return false;
                }
            }
        });
}

function EditScheduling(EventID)
{
	$.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/GetScheduling/'+EventID,
        method: "post",
        dataType: 'html',
        success: function (result) {
            if (result != '') {

            	$("#Event"+EventID).html(result); //add input box
            	$(".select2").select2();
				$('.showdatetimepicker').datetimepicker({
					useCurrent: true,
					icons: {
			                time: "fa fa-clock-o",
			                date: "fa fa-calendar",
			                up: "fa fa-arrow-up",
			                down: "fa fa-arrow-down"
			            }
				});
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}	

function UpdateScheduling(EventID)
{
 	 $.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/UpdateScheduling/'+EventID,
        data: $("#SchedulingForm"+EventID).serialize(),
        method: "post",
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$(".new_scheduling_line_wrap").html(result);
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}

function DeleteScheduling(EventID, WorkOrderID)
{ 
	$.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/DeleteScheduling/'+EventID,
        method: "post",
        data: { EventID : EventID, WorkOrderID : WorkOrderID},
        dataType: 'html',
        success: function (result) {
            
        	$(".new_scheduling_line_wrap").html(result);

        	if($(".schedulingfrmclss").length == 0)
        		$("button.add_new_scheduling_line").html('Schedule a Time');

        	SetViewMoreBtnInScheduling();

           	return true;  	
        }
    });
}

function EditSchAssignedTo(EventID, AssignedTo)
{ 
	$.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/EditSchAssignedTo/',
        method: "post",
        data: { EventID : EventID, AssignedTo : AssignedTo},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$("#AssignedToDropDown"+EventID).html(result);
            	$("#AssignedToBtn"+EventID).html('<a onclick="UpdateAssignedTo('+EventID+')">Save</a>');
            	$(".select2").select2();
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}

function UpdateAssignedTo(EventID)
{ 
	var AssignedTo = $("#AssignedToVal"+EventID).val();
	$.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/UpdateAssignedTo/',
        method: "post",
        data: { EventID : EventID, AssignedTo : AssignedTo},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$("#AssignedToDropDown"+EventID).html(result);
            	$("#AssignedToBtn"+EventID).html('<a onclick="EditSchAssignedTo('+EventID+', '+AssignedTo+')">Change</a>');
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}
/* Scheduling End */ 
</script>


<script type="text/javascript">

function GetPrimaryContactOptions(AccountID){
	$.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/GetPrimaryContactOptions/',
        method: "post",
        data: { AccountID : AccountID, PrimaryContact:'<?=$WorkOrderData->PrimaryContact;?>'},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$(".SelectPrimaryContact").html(result);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}

$(document).ready(function() { 
	GetPrimaryContactOptions(<?=$WorkOrderData->Account?>);
});
</script>

<script type="text/javascript">
/* Line Items Start */ 
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".line_item_fields_wrap"); //Fields wrapper
   
    var x = <?=count($WOLineItems)-1;?>; //initlal text box count
    $(".add_line_item_field_button").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="col-md-12 pt-10 product-line-items">'
					+	'<div class="col-md-4">'
					+	'<select name="Product[]" id="Product" class="selectpicker" data-style="form-control btn-default btn-outline" data-show-subtext="true" data-live-search="true" OnChange="GetProductDetails(this.value, '+x+');">'
					+		'<option value="">Search Chemical</option>'
							<?php $Products = $this->ActionsModel->GetProducts();
								foreach ($Products as $Product) {  ?>
					+				'<option value="<?=$Product->ProductID?>" data-subtext="<?=$Product->ProductCode?> - $<?=number_format($Product->ListPrice, 2, '.', ',');?>"><?=$Product->ProductName?></option>'
							<?php } ?>
					+		'</select>'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="ListPrice'+x+'" name="ListPrice[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="Discount'+x+'" name="Discount[]" class="form-control" type="text" value="0" OnChange="CalculatePriceTotal('+x+')">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="UnitPrice'+x+'" name="UnitPrice[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+		'<input type="text" name="Quantity[]" id="Quantity'+x+'" class="form-control" value="1" OnChange="CalculatePriceTotal('+x+')">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="SubTotal'+x+'" name="SubTotal[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+	'</div>'
					+	'<div class="col-md-1 text-center pt-10">'
					+		'<div class="checkbox checkbox-primary pt-0">'
					+			'<input id="Taxable'+x+'" name="Taxable[]" type="checkbox" value="1" OnChange="CalculatePriceTotal('+x+')">'
					+			'<label for="checkbox2"></label>'
					+		'</div>'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="NetTotal'+x+'" name="NetTotal[]" class="form-control NetTotal" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+	'</div>'
					+	'<div class="col-md-1 text-center">'
					+		'<a href="#" class="btn btn-default remove_line_item_field blank-new-field"><i class="fa fa-trash-o"></i></a>'
					+	'</div>'
					+'</div>'); //add input box
		}
		$(".select2").select2();
		$('.selectpicker').selectpicker();
    });
   
    $(document).on("click",".remove_line_item_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.product-line-items').remove(); x--;

        CalculateGrandTotal();
    });

    CalculateGrandTotal();
});

$(document).on("click",".add-new-line-item", function(e){ //user click on remove text
   $(".add_line_item_field_button").click();
});

$(document).on("click",".edit-line-items", function(e){ //user click on remove text
   $(".blank-new-field.remove_line_item_field").click();
});

function GetProductDetails(ProductID, x)
{ 
	$.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/GetProductDetails/'+ProductID,
        method: "post",
       /* data: { ProductID : ProductID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$('#ListPrice'+x).val(result.ListPrice);
            	if(result.Taxable){ 
            		$( "#Taxable"+x ).prop( "checked", true);
            	}

            	if(result.IsListPriceEditable == 0){ 
            		$( "#ListPrice"+x ).prop( "readonly", true);
            	} else {
            		$( "#ListPrice"+x ).removeAttr( "readonly");
            	}

            	CalculatePriceTotal(x);
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });

}

function CalculatePriceTotal(x)
{ 
	var ListPrice = parseFloat($('#ListPrice'+x).val().replace(',', ''));
	var Discount = parseFloat($('#Discount'+x).val().replace(',', ''));
	var Quantity = parseFloat($('#Quantity'+x).val().replace(',', ''));

	var UnitPrice = ListPrice - (ListPrice * Discount) / 100;
	var SubTotal = UnitPrice * Quantity;
	if($('#Taxable'+x).is(':checked'))
		var NetTotal = SubTotal + (SubTotal * <?=$WorkOrderData->Tax;?>) / 100;
	else 
		var NetTotal = SubTotal;
	 
	$('#ListPrice'+x).val(NumberFormat(ListPrice));
	$('#UnitPrice'+x).val(NumberFormat(UnitPrice));
	$('#SubTotal'+x).val(NumberFormat(SubTotal));
	$('#NetTotal'+x).val(NumberFormat(NetTotal));

	CalculateGrandTotal();
}

function CalculateGrandTotal()
{ 
	var GrandTotal = 0;

    $(".NetTotal").each(function(){

    	GrandTotal = parseFloat(GrandTotal) + parseFloat($(this).val().replace(',', ''));
    });
	$('#GrandTotal').val(NumberFormat(GrandTotal));
}

function NumberFormat(n) {
  return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}

/* Line Items End */ 


/* Chemical Line Items Start*/

$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".chemical_line_item_fields_wrap"); //Fields wrapper
   
    var x = <?=count($Chemicals)-1;?>; //initlal text box count
    $(".add_chemical_line_item_field_button").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="col-md-12 chemical-line-items">'
						+	'<div class="panel-group accordion-struct accordion-style-1" id="accordion_2" role="tablist" aria-multiselectable="true">'
						+		 '<div class="panel panel-default pt-10">'
						+		 	'<div class="panel-heading activestate text-center col-md-1" role="tab" id="heading_10" style="border-bottom: none;">'
						+				'<a role="button" class="btn btn-default btn-outline" data-toggle="collapse" data-parent="#accordion_2" href="#collapse_'+x+'" aria-expanded="true" ><div class="icon-ac-wrap"><span class="plus-ac"><i class="ti-plus"></i></span><span class="minus-ac"><i class="ti-minus"></i></span></div></a>' 
						+			'</div>'
						+			'<div class="col-md-11">'
						+				'<div class="col-md-3">'
						+				'<select name="Product[]" id="Product" class="form-control select2" required>'
						+					'<option value="">Select Product</option>'
											<?php $Chemicals = $this->ActionsModel->GetChemicals();
												foreach ($Chemicals as $Chemical) {  ?>
						+							'<option value="<?=$Chemical->ProductID?>"><?=$Chemical->ProductName?></option>'
											<?php  } ?>
						+					'</select>'
						+				'</div>'
						+				'<div class="col-md-4">'
						+					'<div class="col-md-1"></div>'
						+					'<div class="col-md-4">' 
						+						'<input id="TestConcentration" name="TestConcentration[]" class="form-control" type="text" required>'
						+					'</div>'
						+					'<div class="col-md-6">' 
						+						'<select name="TestedUnitOfMeasure[]" id="TestedUnitOfMeasure" class="form-control" required>'
						+							'<option value="">Select</option>'
													<?php $UnitOfMeasurements = $this->ActionsModel->GetUnitOfMeasurement();
														foreach ($UnitOfMeasurements as $UnitOfMeasurement) { ?>
						+									'<option value="<?=$UnitOfMeasurement->UnitOfMeasurementID?>"><?=$UnitOfMeasurement->UnitOfMeasurement?></option>'
													<?php } ?>
						+						'</select>'
						+					'</div>'
						+				'</div>'
						+				'<div class="col-md-4">'
						+					'<div class="col-md-1"></div>'
						+					'<div class="col-md-4">' 
						+						'<input id="ApplicationAmount" name="ApplicationAmount[]" class="form-control" type="text" required>'
						+					'</div>'
						+					'<div class="col-md-6">' 
						+						'<select name="ApplicationUnitOfMeasure[]" id="ApplicationUnitOfMeasure" class="form-control" required>'
						+							'<option value="">Select</option>'
													<?php $UnitOfMeasurements = $this->ActionsModel->GetUnitOfMeasurement();
														foreach ($UnitOfMeasurements as $UnitOfMeasurement) { ?>
						+									'<option value="<?=$UnitOfMeasurement->UnitOfMeasurementID?>"><?=$UnitOfMeasurement->UnitOfMeasurement?></option>'
													<?php } ?>
						+						'</select>'
						+					'</div>'
						+				'</div>'
						+				'<div class="col-md-1 text-center">'
						+					'<a href="#" class="btn btn-default remove_chemical_line_item_field blank-chemical-new-field"><i class="fa fa-trash-o"></i></a>'
						+				'</div>'
						+			'</div>'
						+			'<div id="collapse_'+x+'" class="panel-collapse collapse in col-md-12 pt-10" role="tabpanel">'
						+				'<div class="col-md-2">'
						+					'<strong>Application Area</strong>'
						+				'</div>'
						+				'<div class="col-md-4">' 
						+					'<textarea class="form-control" name="ApplicationArea[]"></textarea>'
						+				'</div>'
						+				'<div class="col-md-2">' 
						+					'<strong>Additional Notes</strong>'
						+				'</div>'
						+				'<div class="col-md-4">' 
						+					'<textarea class="form-control" name="AdditionalNotes[]"></textarea>'
						+				'</div>'
						+			'</div>'
						+			'</div>'
						+		'</div><div class="clearfix"></div><hr class="light-grey-hr">'
						+	'</div>'); //add input box
		}
		$(".select2").select2();
    });
   
    $(document).on("click",".remove_chemical_line_item_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.chemical-line-items').remove(); x--;
    });

    $(document).on("click",".add-chemical-new-line-item", function(e){ //user click on remove text
	   $(".add_chemical_line_item_field_button").click();
	});

	$(document).on("click",".edit-chemical-line-items", function(e){ //user click on remove text
	   $(".blank-chemical-new-field.remove_chemical_line_item_field").click();
	});
});
/* Chemocal Line Items End*/
</script>