
<?php $this->load->view('Template/HeaderView')?>

<!-- multi-select CSS -->
<!-- <link href="<?=SITE_URL?>vendors/bower_components/multiselect/css/multi-select.css" rel="stylesheet" type="text/css"/> -->

<!-- Bootstrap Switches CSS -->
<!-- <link href="<?=SITE_URL?>vendors/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/> -->

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<style type="text/css">.dataTables_filter { display: none; }</style>
    <div class="container-fluid pt-25">

       <!-- Row -->
	    <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-default card-view">
					<div  class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-lg-3 col-md-3 col-sm-5 col-xs-12 pr-0 pl-0">
								<form method="post" action="<?=SITE_URL?>WorkOrder/View/" id="SelectedAcForm">
								
										<div class="col-lg-2 col-md-3 col-sm-3 col-xs-3">
											<label class="control-label mt-10"><strong>View:</strong></label>
										</div>
										<div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 pr-0">
										<select class="selectpicker" name="SelectedWorkOrder" data-style="form-control btn-default btn-outline" onchange="ViewWorkOrders(this.value);">
											<option disabled="" selected="">Select Views</option>
											<optgroup label="Default Views">
											<option value="MyOpenWorkOrdersToday" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='MyOpenWorkOrdersToday')?"selected":""?>>My Open Work Orders Today</option>
											<option value="MyOpenWorkOrdersThisWeek" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='MyOpenWorkOrdersThisWeek')?"selected":""?>>My Open Work Orders This Week</option>
											<option value="AllOpenWorkOrdersToday" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='AllOpenWorkOrdersToday')?"selected":""?>>All Open Work Orders Today</option>
											<option value="AllWorkOrdersToday" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='AllWorkOrdersToday')?"selected":""?>>All Work Orders Today</option>
											<option value="AllOpenWorkOrdersThisWeek" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='AllOpenWorkOrdersThisWeek')?"selected":""?>>All Open Work Orders This Week</option>
											<option value="WorkOrdersCreatedToday" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='WorkOrdersCreatedToday')?"selected":""?>>Work Orders Created Today</option>
											<option value="WorkOrdersCreatedThisWeek" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='WorkOrdersCreatedThisWeek')?"selected":""?>>Work Orders Created This Week</option>
											<option value="UnscheduledWorkOrders" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='UnscheduledWorkOrders')?"selected":""?>>Unscheduled Work Orders</option>
											<option value="UnassignedWorkOrders" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='UnassignedWorkOrders')?"selected":""?>>Unassigned Work Orders</option>
											<option value="UnassignedWorkOrders" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='UnassignedWorkOrders')?"selected":""?>>Unassigned Work Orders</option>
											<option value="MyOpenWorkOrdersPastDue" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='MyOpenWorkOrdersPastDue')?"selected":""?>>My Open Work Orders Past Due</option>
											<option value="AllOpenWorkOrdersPastDue" <?=(isset($SelectedWorkOrder) && $SelectedWorkOrder=='AllOpenWorkOrdersPastDue')?"selected":""?>>All Open Work Orders Past Due</option>
											</optgroup>
											<optgroup label="My Custom Views">
												<?php $Views = $this->ViewModel->GetViews();
												foreach ($Views as $View) { 
													if($View->WorkOrderViewID == $SelectedWorkOrder){?>
														<option value="<?=$View->WorkOrderViewID?>" selected=""><?=$View->WorkOrderViewName?></option>
													<?php } else { ?>
														<option value="<?=$View->WorkOrderViewID?>"><?=$View->WorkOrderViewName?></option>
													<?php } ?>
											<?php } ?>
											</optgroup>
										</select>
										</div>	
								
								</form>	
							</div>	
							<div class="col-lg-5 col-md-6 col-sm-7 col-xs-12">
								<div class="form-group" >
								<!-- <div class="btn-group">
									<button class="btn btn-primary btn-outline">Go</button>
								</div> --> 
								<div class="btn-group">
									<div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button"> <i class="fa fa-pencil"></i> <span class="caret"></span></button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li>
												<a><span><strong>LIST VIEW ACTIONS</strong></span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>WorkOrder/View/CreateNewView"><span>Create New View</span></a>
											</li>
											<li>
												<a href="#" OnClick="CopyCustomView(<?=$SelectedWorkOrder?>)"><span>Copy</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#account-rename-view-modal"><span>Rename</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#account-edit-sharing-view-modal"><span>Edit Sharing</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#account-edit-filters-view-modal"><span>Edit Filters</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#account-edit-displayed-columns-view-modal"><span>Edit Displayed Columns</span></a>
											</li> 
											 <li>
												<a href="#" OnClick="DeleteCustomView(<?=$SelectedWorkOrder?>)"><span>Delete View</span></a>
											</li>
										</ul>
									</div>
								</div>
								<div class="btn-group">
									<button class="btn btn-default btn-outline" title="Refresh" ><i class="fa fa-refresh"></i></button>
									<button class="btn btn-default btn-outline" title="Export" id="Export-Excel"><i class="fa fa-file-excel-o"></i></button>
								</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-1 col-sm-6 col-xs-12">
								<div class="pull-right">
									<div class="form-group">
										<a href="<?=SITE_URL?>WorkOrder/Actions/CreateWorkOrder" class="btn btn-primary btn-outline">New WorkOrder</a>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
								<div class="pull-right">
									<div class="input-group">
										<input id="myTable1globalfilter" class="form-control myTable1globalfilter" placeholder="Search" type="text">
										<span class="input-group-addon"><i class="zmdi zmdi-search"></i></span> 
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view loading-view" style="min-height: 700px;">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">

								<div class="cus-view-loading-img text-center">
									<img src="<?=SITE_URL?>/img/loader.gif">
								</div>
								<input type="hidden" id="cur_page" value="1">
							
								<div class="table-responsive">
									<table id="postsList" class="table table-hover display pb-30" width="100%">
										<thead>
											<tr> 
												<?php /*foreach ($WorkOrderData['ColumnsNames'] as $key => $value) { ?>
														<th class="text-center"><?=$value?></th>
												<?php }*/ ?>
											</tr>
										</thead>
										<tbody>
										<?php /*foreach ($WorkOrderData['ColumnsValues'] as $key1 => $WorkOrder) {

										 ?> 
											<tr> 
											<?php foreach ($WorkOrderData['ColumnsNames'] as $value) {
													if($value == 'Subject' || $value == 'WorkOrderNo'){ ?>
														<td class="text-center"><a href="<?=SITE_URL?>WorkOrder/Actions/WorkOrderDetails/<?=$WorkOrder->WorkOrderID?>"><?=$WorkOrder->$value?></a></td>
													<?php } else if($value == 'AssignedTo'){ ?>  
														<td class="text-center"><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/<?=$WorkOrder->AssignedToID?>"><?=$WorkOrder->$value?></a></td>
													<?php } else if($value == 'Account'){ ?>  
														<td class="text-center"><a href="<?=SITE_URL?>Account/Actions/AccountDetails/<?=$WorkOrder->AccountID?>"><?=$WorkOrder->$value?></a></td>
													<?php } else if($value == 'ParentWorkOrder'){ ?>  
														<td class="text-center"><a href="<?=SITE_URL?>WorkOrder/Actions/WorkOrderDetails/<?=$WorkOrder->ParentWorkOrderID?>"><?=$WorkOrder->$value?></a></td>
													<?php } else if($value == 'PrimaryContact'){ ?>  
														<td class="text-center"><a href="<?=SITE_URL?>Contact/Actions/ContactDetails/<?=$WorkOrder->ContactID?>"><?=$WorkOrder->$value?></a></td>
													<?php } else if($value == 'CreatedBy'){ ?>  
														<td class="text-center"><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/<?=$WorkOrder->CreatedByID?>"><?=$WorkOrder->$value?></a></td>
													<?php } else if($value == 'LastModifiedBy'){ ?>  
														<td class="text-center"><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/<?=$WorkOrder->LastModifiedByID?>"><?=$WorkOrder->$value?></a></td>
													<?php } else {?>
														<td class="text-center"><?=$WorkOrder->$value?></td>
													<?php } ?>
											<?php } ?>
											</tr>
										<?php }*/ ?>
										</tbody>
									</table>
								</div>
								<div class="dataTables_wrapper no-footer row-per-page-div">
									<div class="bottom">
										<div class="dataTables_info result-count" id="accounttable_info" role="status" aria-live="polite"></div>
										<div class="dataTables_length" id="accounttable_length">
											<label>Show 
												<select name="accounttable_length" class="rowperpage">
													<option value="10">10</option>
													<option value="25">25</option>
													<option value="50">50</option>
													<option value="100">100</option>
												</select> entries</label>
										</div>
										<div id='pagination' class="dataTables_paginate paging_simple_numbers" id="accounttable_paginate">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
		</div>
		<!-- /Row -->
	</div>	

<!-- Start Modal -->

<?php $this->load->view('WorkOrder/Modal/WorkOrderEditSharingView'); ?>
<?php $this->load->view('WorkOrder/Modal/WorkOrderEditFiltersView'); ?>
<?php $this->load->view('WorkOrder/Modal/WorkOrderEditDisplayedColumnsView'); ?>
<?php $this->load->view('WorkOrder/Modal/WorkOrderRenameView'); ?>

<?php $this->load->view('Template/FooterView')?>

<!-- Multiselect JavaScript -->
<!-- <script src="<?=SITE_URL?>vendors/bower_components/multiselect/js/jquery.multi-select.js"></script> -->
<!-- Bootstrap Switch JavaScript -->
<!-- <script src="<?=SITE_URL?>vendors/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script> -->

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript" src="<?=SITE_URL?>dist/js/multiselect.min.js"></script>


<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>
	
<script type="text/javascript">

 	/*var dataTable = $('#myTable1').DataTable();

 	$('#accountglobalfilter').keyup(function(){
    	var dataTable = $('#accounttable').DataTable();
      	dataTable.search($(this).val()).draw() ;
    }); */
    function ViewWorkOrders()
 	{
 		$('#SelectedAcForm').submit();
 	}
</script>
<script type="text/javascript">
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
   
    var x = 1; //initlal text box count
    $(".add_field_button").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="form-group" >'
					+'<div class="col-md-4">'
					+'	<select class="form-control" name="FilterFields[]">'
					+'		<option value="">Select Field</option>'
					<?php $Fields = $this->ViewModel->WorkOrderViewFields(); 
					foreach($Fields as $Field){  ?>
						+'	<option value="<?=$Field->FieldName;?>"><?=$Field->FieldName;?></option>'	
					<?php } ?>
					+'	</select>'
					+'</div>'
					+'<div class="col-md-3">'
					+'		<select class="form-control" name="FilterConditions[]">'
					+'	<option value="Equals">Equals</option>'
					+'	<option value="NotEqualTo">Not Equal To</option>'
					+'	<option value="LessThan">Less Than</option>'
					+'	<option value="GreaterThan">Greater Than</option>'
					+'	<option value="LessOREqualTo">Less OR Equal To</option>'
					+'	<option value="GreaterOREqualTo">Greater OR Equal To</option>'
					+'	<option value="Contains">Contains</option>'
					+'	<option value="StartsWith">Starts With</option>'
					+'	<option value="DoesNotContain">Does Not Contain</option>'
					+'</select>'
					+'</div>'
					+'<div class="col-md-4">'
					+'	<input type="text" class="form-control" name="FilterValues[]">'
					+'</div>'
					+'<div class="col-md-1 btn-group btn-group-vertical pl-0">'
					+	 '<button type="button" id="" class="btn btn-default btn-icon-anim btn-circle remove_field_filter_criteria"><i class="fa fa-close"></i></button>'
					+'</div>'
				+'</div>'); //add input box
		}
    });
   
    $(document).on("click",".remove_field_filter_criteria", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.form-group').remove(); x--;
    });
});

/* Multiselect Init*/     
/*$('#my-select').multiSelect({ selectableOptgroup: true, selectableHeader: '<div style="text-align: center;">Available Fields</div>', selectionHeader: '<div style="text-align: center;">Displayed Fields</div>' });*/


function DeleteCustomView(WorkOrderViewID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#f8b32d",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>WorkOrder/View/DeleteCustomView/<?=$SelectedWorkOrder?>/";
		}, 500);
    });
	return false;
}

function CopyCustomView(WorkOrderViewID)
{ 
	swal({
	  title: "Nice!",
	  text: "View copy successfully.",
	  type: "success",
	  showConfirmButton: false, 
	});

	setTimeout(function () {
	    window.location.href = "<?=SITE_URL;?>WorkOrder/View/CopyCustomView/<?=$SelectedWorkOrder?>/";
	}, 1000);
}
</script>
<script type="text/javascript">
$(document).ready(function() {

     $('#multiselect').multiselect({ keepRenderingSort: true });

});
function moveTop() {
    $("#multiselect_to option:selected").each(function () {
        var listItem = $(this);
        listItem.insertBefore(listItem.siblings().first());
    });
}

function moveBottom() {
    var itemsCount = $("#multiselect_to option").length;

    $($("#multiselect_to option:selected").get().reverse()).each(function () {
        var listItem = $(this);
        listItem.insertAfter(listItem.siblings().last());
    });
}

$(document).ready(function() {
	$('.buttons-excel').hide();
	$("#Export-Excel").on("click", function() {
	    $( '.buttons-excel' ).click();
	});
});
</script>

<script type="text/javascript">
	
$(document).ready(function () {

    $('#FiltersViewForm').validate({ 
        rules: {
            "FilterFields[]": {
                required: true
            },
            "FilterValues[]": {
                required: true
            },
            "FilterConditions[]": {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    $('#RenameViewForm').validate({ 
        rules: {
            WorkOrderViewName: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    $('#FieldsDisplayViewForm').validate({ 
        rules: {
            "SpecifyFieldsDisplay[]": {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    $('#pagination').on('click','a',function(e){

       e.preventDefault(); 
       var pageno = $(this).attr('data-ci-pagination-page');
       loadPagination(pageno);
     });

    $('.rowperpage').on('change',function(e){

       e.preventDefault(); 

       var pageno = $("#cur_page").val();
       var rowperpage = $(this).val();
       loadPagination(pageno,'','',rowperpage);
    });

    $(document).on('click','th.sorting a',function(e){
 
       e.preventDefault(); 
       var sorting_col = $(this).attr('data-ci-sorting-col');
       var sorting_val = $(this).attr('data-ci-sorting-val');

       sorting_val = (sorting_val == '') ? "desc" : sorting_val;

       loadPagination(0, sorting_col, sorting_val);
     });

    loadPagination(0);

    function loadPagination(pagno, sortby='', order='',rowperpage=10){

       if(pagno === undefined){
       		var pagno = $("#cur_page").val();
       }
       $("#cur_page").val(pagno);
       $('.dataTables_length').hide();
       
       $(".cus-view-loading-img").show();
       $(".loading-view").css('opacity','0.5');
       $.ajax({
         url: '<?=SITE_URL?>WorkOrder/View/List/<?=$SelectedWorkOrder?>/'+pagno+'/'+rowperpage+'/'+sortby+'/'+order,
         type: 'get',
         dataType: 'json',
         success: function(response){ 
            $('#pagination').html(response.pagination);
            $('.result-count').html(response.result_count);
            createTable(response);

            $('.dataTables_length').show();
            $(".cus-view-loading-img").hide();
            $(".loading-view").css('opacity','1');

            if(response.WorkOrderData.TotalRows == 0){
            	$('#postsList tbody').html("<tr><td colspan="+response.WorkOrderData.ColumnsNames.length+" align='center'>No records found.</td></tr>");
            }
            else {
            	$(".row-per-page-div").show();
            }
         }
       });
    }

    function createTable(result){

	       $('#postsList tbody').empty();
	       
	        $.each(result.WorkOrderData.ColumnsValues, function( key1, WorkOrder ) {

	        	var tr = "<tr>";

	        	$.each(result.WorkOrderData.ColumnsNames, function( key2, value ) {

	        		var workorder_val = (WorkOrder[value] == null) ? "" : WorkOrder[value];

			  		if(value == 'Subject' || value == 'WorkOrderNo'){ 
			  			tr += '<td class="text-center"><a href="<?=SITE_URL?>WorkOrder/Actions/WorkOrderDetails/'+WorkOrder.WorkOrderID+'">'+workorder_val+'</a></td>';

			  		} else if(value == 'AssignedTo'){ 
			  			
			  			tr += '<td class="text-center"><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/'+WorkOrder.AssignedToID+'">'+workorder_val+'</a></td>';

			  		} else if(value == 'Account'){ 
			  			
			  			tr += '<td class="text-center"><a href="<?=SITE_URL?>Account/Actions/AccountDetails/'+WorkOrder.AccountID+'">'+WorkOrder.Account+'</a></td>';

			  		} else if(value == 'ParentWorkOrder'){ 
			  			
			  			tr += '<td class="text-center"><a href="<?=SITE_URL?>Account/Actions/AccountDetails/'+WorkOrder.ParentWorkOrderID+'">'+workorder_val+'</a></td>';

			  		} else if(value == 'PrimaryContact'){ 
			  			
			  			tr += '<td class="text-center"><a href="<?=SITE_URL?>Account/Actions/AccountDetails/'+WorkOrder.ContactID+'">'+workorder_val+'</a></td>';
			  		} else if(value == 'CreatedBy'){ 
			  			
			  			tr += '<td class="text-center"><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/'+WorkOrder.CreatedByID+'">'+workorder_val+'</a></td>';
			  		} else if(value == 'LastModifiedBy'){ 
			  			
			  			tr += '<td class="text-center"><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/'+WorkOrder.LastModifiedByID+'">'+workorder_val+'</a></td>';

			  		} else { 
			  			
			  			tr += '<td class="text-center">'+workorder_val+'</td>';
			  		}
			  	});

			  	tr += "</tr>";
			  	$('#postsList tbody').append(tr);
			});

	   
	        var tr = "<tr>";
			$.each(result.WorkOrderData.ColumnsNames, function( key2, value ) {
			 
			  	var sort_val = (typeof result.order[value] === 'undefined') ? "" : result.order[value];
			  	tr += '<th class="sorting text-center"><a href="#" class="sort'+sort_val+'" data-ci-sorting-col="'+value+'" data-ci-sorting-val="'+sort_val+'">'+value+'</a></th>';
		  	});
		  	tr += "</tr>";

		  	$('#postsList thead').html(tr);
	}
});
</script>


