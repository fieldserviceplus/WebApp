
<?php $this->load->view('Template/HeaderView'); ?>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10 pb-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['WorkOrder']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['WorkOrder']['title']?></span>
									<span class="panel-title txt-dark"><?=$WorkOrderData->Subject;?></span>
								</div>
							</div>
						</div>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div  class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="col-lg-7 col-md-5 col-sm-4 col-xs-12">
							<!-- <div class="form-group"> -->
								<ul role="tablist" class="nav nav-pills" id="myTabs_13">
									<li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_13" href="#home_13">Detail</a></li>
									<li role="presentation" class=""><a data-toggle="tab" id="profile_tab_13" role="tab" href="#profile_13" aria-expanded="false">Related</a></li>
								</ul>
							<!-- </div> -->
						</div>

						<div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">
							<div class="btn-group pull-right">
									<div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li>
												<a><span><strong>Communication</strong></span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>Common/Actions/SendEmail/WorkOrder/<?=$WorkOrderData->WorkOrderID;?>"><span>Send Email</span></a>
											</li>
											<li>
												<a><span><strong>Add New Actions</strong></span></a>
											</li>
											<li>
												<a href="#" class="add_new_scheduling_line"><span>Schedule a Time</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#create-line-items-modal"><span>New Line Item</span></a>
											</li>
											<?php if(MY_Model::getChemicalSettings()){ ?>
											<li>
												<a href="#" data-toggle="modal" data-target="#create-chemical-line-items-modal"><span>New Chemical</span></a>
											</li>
											<?php } ?>
											<li> 
												<a href="#" data-toggle="modal" data-target="#CreateNewFile"><span>New File</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>Task/Actions/CreateTask/?WorkOrder=<?=$WorkOrderData->WorkOrderID?>"><span>New Task</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#CreateNewNote"><span>New Note</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>Invoice/Actions/CreateInvoice?WorkOrder=<?=$WorkOrderData->WorkOrderID?>"><span>New Invoice</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>WorkOrder/Actions/ConvertToInvoice/<?=$WorkOrderData->WorkOrderID?>"><span>Convert to Invoice</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>WorkOrder/Actions/GenerateDocument/<?=$WorkOrderData->WorkOrderID?>/"><span>Generate Document</span></a>
											</li>
											<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
												<a><span><strong>Work Order Actions</strong></span></a>
											</li>
											<li> 
												<a href="<?=SITE_URL;?>WorkOrder/Actions/CreateWorkOrder/<?=$WorkOrderData->WorkOrderID?>/"><span>Copy Work Order</span></a>
											</li>
											<li>
												<a href="#" onclick="DeleteWorkOrder();"><span>Delete Work Order</span></a>
											</li>
										</ul>
									</div>
								</div>
								<div class="btn-group pull-right">
									<a href="<?=SITE_URL?>WorkOrder/Actions/EditWorkOrder/<?=$WorkOrderData->WorkOrderID;?>" class="btn btn-primary btn-outline">Edit</a>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
			 	<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">WorkOrder Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>  -->
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>WorkOrder/Actions/EditWorkOrder/<?=$WorkOrderData->WorkOrderID?>" class="form-horizontal" id="EditWorkOrder" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>WorkOrder Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">
												
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Work Order No
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->WorkOrderNo;?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Assigned To
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/<?=$WorkOrderData->AssignedTo?>"><?=$WorkOrderData->AssignedToName;?></a>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Account
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Account/Actions/AccountDetails/<?=$WorkOrderData->Account?>"><?=$WorkOrderData->AccountName;?></a>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Primary Contact
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Contact/Actions/ContactDetails/<?=$WorkOrderData->PrimaryContact?>"><?=$WorkOrderData->PrimaryContactName;?></a>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Parent Work Order
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>WorkOrder/Actions/WorkOrderDetails/<?=$WorkOrderData->ParentWorkOrder?>"><?=$WorkOrderData->ParentWorkOrderName;?></a>
														</div>
													</div>
												
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Subject
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->Subject;?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Description
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->Description;?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Pop Up Reminder
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->PopUpReminder;?>
														</div>
													</div>

												</div>
												<div class="col-md-6">

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Type
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->WorkOrderTypeName;?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Status
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->Status;?>
														</div>
													</div>


													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Priority
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->Priority;?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Category
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->CategoryName;?>
														</div>
													</div>
											</div>
											</div>

											<div class="col-md-12">
											<div class="txt-dark capitalize-font pt-10"><strong>Address Information</strong></div>
											<hr class="light-grey-hr">

												<!-- <div class="col-md-6">	

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Start Date Time
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->StartDate;?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															End Date Time
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->EndDate;?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Is Recurring?
														</div>
														<div class="col-md-8">
															<div class="checkbox checkbox-primary pt-0">
																<input id="IsRecurring" name="IsRecurring" type="checkbox" value="1" <?=($WorkOrderData->IsRecurring==1)?"checked=''":""?>>
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

												</div> -->
												<div class="col-md-6">	

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Address
														</div>
														<div class="col-md-8">
															<div><?=$WorkOrderData->Address;?></div>
															<div><?=$WorkOrderData->City;?>, <?=$WorkOrderData->State;?> <?=$WorkOrderData->PostalCode;?></div>
															<div><?=$WorkOrderData->Country;?></div>
														</div>
													</div>
												</div>
												<!--/span-->
											</div>

											<div class="col-md-12">
											<div class="txt-dark capitalize-font pt-10"><strong>Financials</strong></div>
											<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Sub Total
														</div>
														<div class="col-md-8">
															<i class="fa fa-usd"></i> <?=number_format($WOFinancials['SubTotal'], 2, '.', ',');?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Discount
														</div>
														<div class="col-md-8">
															<?=number_format((double)$WOFinancials['Discount'], 2, '.', '');?>%
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Tax
														</div>
														<div class="col-md-8">
															<i class="fa fa-usd"></i> <?=number_format((double)$WOFinancials['Tax'], 2, '.', ',');?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Total Price
														</div>
														<div class="col-md-8">
															<i class="fa fa-usd"></i> <?=number_format((double)$WOFinancials['TotalPrice'], 2, '.', ',');?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Grand Total
														</div>
														<div class="col-md-8">
															<i class="fa fa-usd"></i> <?=number_format((double)$WOFinancials['GrandTotal'], 2, '.', ',');?>
														</div>
													</div>

												</div>
												<div class="col-md-6">

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Signature
														</div>
														<div class="col-md-8">
														<?php if(!empty($WorkOrderData->Signature)){?>
															 <img style="width: 256px;height: 76px;" src="<?=$WorkOrderData->Signature;?>" />
														<?php } ?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Line Item Cnt
														</div>
														<div class="col-md-8">
															<?=count($WOLineItems);?>
														</div>
													</div>
												</div>
											</div>

											<!-- Start Custom Fields Row -->
											<?php $this->load->view('Setup/Customize/Fields/CustomFieldsView', array('Object' => 'WorkOrder', 'ObjectID' => $WorkOrderData->WorkOrderID)); ?>
											<!-- End Custom Fields Row -->

											<div class="col-md-12">

												<div class="txt-dark capitalize-font pt-10"><strong>System Information</strong></div>
												<hr class="light-grey-hr">
												
												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Create Date
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->CreatedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Created By
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/<?=$WorkOrderData->CreatedBy?>"><?=$WorkOrderData->CreatedByName;?></a>
														</div>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified Date
														</div>
														<div class="col-md-8">
															<?=$WorkOrderData->LastModifiedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified By
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/<?=$WorkOrderData->LastModifiedBy?>"><?=$WorkOrderData->LastModifiedByName;?></a>
														</div>
													</div>
												</div>
											</div>


										</div>
											<!-- /Row -->
									</div>

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		
			</div>
			<div id="profile_13" class="tab-pane fade" role="tabpanel">

				<?php $this->load->view('WorkOrder/Include/RelatedInformation'); ?>
				
			</div>
		</div>
		
		</div>
 
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">

			<?php $this->load->view('WorkOrder/Include/SchedulingAndNotes'); ?>
			
		</div>

	</div>
	<!-- /Row -->

</div>


<!-- Start Modal -->
<?php //$this->load->view('WorkOrder/Modal/CreateTaskView'); ?>

<?php //$this->load->view('WorkOrder/Modal/CreateEventView'); ?>

<?php //$this->load->view('WorkOrder/Modal/RecurringTaskView'); ?>

<!-- modal -->
 <div class="modal fade" id="location-picker-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Select Location</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
				<div id="Billingpopupmap" style="height: 400px;"></div><br />
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-2">Location</label>
								<div class="col-md-10">
									<input id="BillingLocation" type="text" name="" class="form-control" readonly="">
								</div>
							</div>
						</div>
					</div>
				</div>
				</form>
				
			</div>
		</div>
	</div>
</div> 

 <div class="modal fade" id="location-picker-modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Select Location</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
				<div id="Shippingpopupmap" style="height: 400px;"></div><br />
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-2">Location</label>
								<div class="col-md-10">
									<input id="ShippingLocation" type="text" name="" class="form-control" readonly="">
								</div>
							</div>
						</div>
					</div>
				</div>
				</form>
				
			</div>
		</div>
	</div>
</div> 

<?php $this->load->view('WorkOrder/Modal/CreateLineItemsView'); ?>
<?php $this->load->view('WorkOrder/Modal/CreateChemicalLineItems'); ?>
<?php //$this->load->view('WorkOrder/Modal/CreateNoteView'); ?>

<?php $data = array('data' => array('RelatedTo' => 'WorkOrder', 'What' => $WorkOrderData->WorkOrderID, 'AssignedTo' => $WorkOrderData->AssignedTo));
$this->load->view('Common/Modal/CreateNewFile', $data);
$this->load->view('Common/Modal/CreateNewNote', $data); ?>

<?php $this->load->view('Template/FooterView')?>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<!-- Use for common object  -->
<script src="<?=SITE_URL?>dist/js/common.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	$(".select2").select2();
	$('.selectpicker').selectpicker();

});
</script>

<script type="text/javascript">

$(document).ready(function () {

    $('.showdatetimepicker').datetimepicker({
		useCurrent: true,
		icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
	});

   /* $('.timepicker').datetimepicker({
			format: 'LT',
			useCurrent: false,
			icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
	}).data("DateTimePicker").date(moment());*/

    /*$('#CreateNoteForm').validate({ 
        rules: {
            Subject: {
                required: true
            },
            Body: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });*/

}); 
function DeleteWorkOrder(WorkOrderID)
{ 
	<?php if($WorkOrderData->IsRecurring == 0){ ?>

	    swal({   
	        title: "Are you sure?",   
	        text: "You will not be able to recover this data!",   
	        type: "warning",   
	        showCancelButton: true,   
	        confirmButtonColor: "#5fc55f",   
	        confirmButtonText: "Yes, delete it!",   
	        closeOnConfirm: false,
	        showLoaderOnConfirm: true,
	        iconColor: '#000',
	    }, function(){   
	        swal("Deleted!", "Your data has been deleted.", "success"); 
	        setTimeout(function () {
			    window.location.href = "<?=SITE_URL;?>WorkOrder/Actions/DeleteWorkOrder/<?=$WorkOrderData->WorkOrderID;?>/";
			}, 500);
	    });
		return false;

	<?php } else { ?>

		var inviteDevInput = '<div class="text-left pl-10">'
				+'<div class="form-group mb-30">'
				+	'<div class="radio radio-primary">'
				+		'<input type="radio" name="dltWorkOrder" id="radio1" value="OnlyThisOne" checked="">'
				+		'<label for="radio1">'
				+			'This Work Order only'
				+		'</label>'
				+	'</div>'
				+	'<div class="radio radio-primary">'
				+		'<input type="radio" name="dltWorkOrder" id="radio2" value="ThisAndFutureThisSeries">'
				+		'<label for="radio2">'
				+			'This and all future Work Orders in this series'
				+		'</label>'
				+	'</div>'
				+	'<div class="radio radio-primary">'
				+		'<input type="radio" name="dltWorkOrder" id="radio3" value="ThisAndOpenThisSeries">'
				+		'<label for="radio3">'
				+			'This and all open Work Orders in this series'
				+		'</label>'
				+	'</div>'
				+	'<div class="radio radio-primary">'
				+		'<input type="radio" name="dltWorkOrder" id="radio4" value="AllThisSeries">'
				+		'<label for="radio4">'
				+			'All Work Orders in this series'
				+		'</label>'
				+	'</div>'
				+'</div></div>';
		var recordType;
		swal({
					title: "Delete recurring Work Order",
					text: inviteDevInput,
					html: true,
					showCancelButton: true,
					confirmButtonColor: "#5fc55f",
					confirmButtonText: "OK",
					cancelButtonText: "Cancel",
					closeOnConfirm: false,
					closeOnCancel: true
				}, // Function that validates email address through a regular expression.
				function () {
					var Action = $("input[name=dltWorkOrder]:checked").val();
					swal("Deleted!", "Your data has been deleted.", "success"); 
			        setTimeout(function () {
					    window.location.href = "<?=SITE_URL;?>WorkOrder/Actions/DeleteWorkOrder/<?=$WorkOrderData->WorkOrderID;?>/"+Action;
					}, 500);
		    	});

    <?php } ?>
}
</script>


<script type="text/javascript">
/* Scheduling Start */ 
$(document).ready(function() {

    var max_fields      = 20; //maximum input boxes allowed
    var wrapper         = $(".new_scheduling_line_wrap"); //Fields wrapper
   
    var x = 1;//<?=count($Events);?>; //initlal text box count
    $(".add_new_scheduling_line").click(function(e){ //on add input button click

        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div id="SchedulingLine'+x+'"><div class="form-wrap">'
					+	'<form action="#" class="form-horizontal schedulingfrmclss" id="SchedulingForm'+x+'">'
					+   '<input type="hidden" name="Subject" value="<?=$WorkOrderData->Subject;?>">'
					+   '<input type="hidden" name="WorkOrderNo" value="<?=$WorkOrderData->WorkOrderNo;?>">'
					+   '<input type="hidden" name="WorkOrderID" value="<?=$WorkOrderData->WorkOrderID;?>">'
					+   '<input type="hidden" name="Who" value="<?=$WorkOrderData->PrimaryContact;?>">'
					+   '<input type="hidden" name="Description" value="<?=$WorkOrderData->Description;?>">'
					+	'<div class="form-body">'
					+		'<div class="form-group">'
					+			'<label class="col-md-3 control-label text-left">Assigned To</label>'
					+			'<div class="col-md-6">'
					+				'<select name="AssignedTo" id="AssignedTo" class="form-control select2">'
					+				'<option value="">Select Owner</option>'
									<?php $Users = $this->ActionsModel->GetUsers();
										foreach ($Users as $User) { 
											if($WorkOrderData->AssignedTo == $User->UserID){?>
					+							'<option value="<?=$User->UserID?>" selected=""><?=$User->FirstName.' '.$User->LastName?></option>'
										<?php } else {?>
					+							'<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>'
										<?php }
										} ?>
					+				'</select>'
					+			'</div>'
					+		'</div>'
					+		'<div class="form-group">'
					+			'<label class="col-md-3 control-label text-left">Start</label>'
					+			'<div class="col-md-6">'
					+				'<input type="text" class="form-control showdatetimepicker" name="EventStartDate" value="<?=date("m/d/Y")?>" required>'
					+			'</div>'
					+		'</div>'
					+		'<div class="form-group">'
					+			'<label class="col-md-3 control-label text-left">End</label>'
					+			'<div class="col-md-6">'
					+				'<input type="text" class="form-control showdatetimepicker" name="EventEndDate" value="<?=date("m/d/Y")?>" required>'
					+			'</div>'
					+		'</div>'
					+	'</div>'
					+	'<div class="pull-right">'
					+		'<button type="button" class="btn btn-default remove_new_scheduling_line" data-dismiss="modal">Close</button> '
					+		'<button type="button" class="btn btn-primary" OnClick="SaveScheduling('+x+')">Save</button>'
					+	'</div>'
					+	'</form>'
					+ '<div class="clearfix"></div>'
					+ '<hr class="light-grey-hr">'
					+ '</div>'
					+ '</div>'); //add input box
		}
		$(".select2").select2();
		$('.showdatetimepicker').datetimepicker({
			useCurrent: true,
			icons: {
	                time: "fa fa-clock-o",
	                date: "fa fa-calendar",
	                up: "fa fa-arrow-up",
	                down: "fa fa-arrow-down"
	            }
		});

		SetViewMoreBtnInScheduling();
    });
    $(wrapper).on("click",".remove_new_scheduling_line", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.form-wrap').remove(); x--;

        SetViewMoreBtnInScheduling();
        
    })
});

function SetViewMoreBtnInScheduling(){
	if($(".schedulingfrmclss").length > 5){
		$(".scheduling-view-more-btn").html('<div class="pull-right">'
			+ '<a href="<?=SITE_URL?>WorkOrder/Actions/RelatedObject/<?=$WorkOrderData->WorkOrderID;?>/Events" type="button" class="btn btn-primary btn-outline">View All</a>'
			+ '</div>');
	} else {
		$(".scheduling-view-more-btn").html('');
	}
}

function SaveScheduling(x)
{
 	 $.ajax({
            url: '<?=SITE_URL?>WorkOrder/Actions/SaveScheduling/',
            data: $("#SchedulingForm"+x).serialize(),
            method: "post",
            dataType: 'html',
            success: function (result) {
                if (result != '') {
                	$(".new_scheduling_line_wrap").html(result);

                if($(".schedulingfrmclss").length > 0)
            		$("button.add_new_scheduling_line").html('<i class="fa fa-plus"></i> Add another time');

                  return true;  	
                } else {
                    alert("Something went wrong.Please try again!");
                    return false;
                }
            }
        });
}

function EditScheduling(EventID)
{
	$.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/GetScheduling/'+EventID,
        method: "post",
        dataType: 'html',
        success: function (result) {
            if (result != '') {

            	$("#Event"+EventID).html(result); //add input box
            	$(".select2").select2();
				$('.showdatetimepicker').datetimepicker({
					useCurrent: true,
					icons: {
			                time: "fa fa-clock-o",
			                date: "fa fa-calendar",
			                up: "fa fa-arrow-up",
			                down: "fa fa-arrow-down"
			            }
				});
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}	

function UpdateScheduling(EventID)
{
 	 $.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/UpdateScheduling/'+EventID,
        data: $("#SchedulingForm"+EventID).serialize(),
        method: "post",
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$(".new_scheduling_line_wrap").html(result);
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}

function DeleteScheduling(EventID, WorkOrderID)
{ 
	$.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/DeleteScheduling/'+EventID,
        method: "post",
        data: { EventID : EventID, WorkOrderID : WorkOrderID},
        dataType: 'html',
        success: function (result) {
            
        	$(".new_scheduling_line_wrap").html(result);

        	if($(".schedulingfrmclss").length == 0)
        		$("button.add_new_scheduling_line").html('Schedule a Time');

        	SetViewMoreBtnInScheduling();

           	return true;  	
        }
    });
}

function EditSchAssignedTo(EventID, AssignedTo)
{ 
	$.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/EditSchAssignedTo/',
        method: "post",
        data: { EventID : EventID, AssignedTo : AssignedTo},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$("#AssignedToDropDown"+EventID).html(result);
            	$("#AssignedToBtn"+EventID).html('<a onclick="UpdateAssignedTo('+EventID+')">Save</a>');
            	$(".select2").select2();
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}

function UpdateAssignedTo(EventID)
{ 
	var AssignedTo = $("#AssignedToVal"+EventID).val();
	$.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/UpdateAssignedTo/',
        method: "post",
        data: { EventID : EventID, AssignedTo : AssignedTo},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$("#AssignedToDropDown"+EventID).html(result);
            	$("#AssignedToBtn"+EventID).html('<a onclick="EditSchAssignedTo('+EventID+', '+AssignedTo+')">Change</a>');
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}
/* Scheduling End */ 
</script>

<script type="text/javascript">
/* Line Items Start */ 
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".line_item_fields_wrap"); //Fields wrapper
   
    var x = <?=count($WOLineItems)-1;?>; //initlal text box count
    $(".add_line_item_field_button").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="col-md-12 pt-10 product-line-items">'
					+	'<div class="col-md-4">'
					+	'<select name="Product[]" id="Product" class="selectpicker" data-style="form-control btn-default btn-outline" data-show-subtext="true" data-live-search="true" OnChange="GetProductDetails(this.value, '+x+');">'
					+		'<option value="">Search Product</option>'
							<?php $Products = $this->ActionsModel->GetProducts();
								foreach ($Products as $Product) {  ?>
					+				'<option value="<?=$Product->ProductID?>" data-subtext="<?=$Product->ProductCode?> - $<?=number_format($Product->ListPrice, 2, '.', ',')?>"><?=$Product->ProductName?></option>'
							<?php } ?>
					+		'</select>'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="ListPrice'+x+'" name="ListPrice[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="Discount'+x+'" name="Discount[]" class="form-control" type="text" value="0" OnChange="CalculatePriceTotal('+x+')">'
					
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="UnitPrice'+x+'" name="UnitPrice[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+		'<input type="text" name="Quantity[]" id="Quantity'+x+'" class="form-control" value="1" OnChange="CalculatePriceTotal('+x+')">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="SubTotal'+x+'" name="SubTotal[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+	'</div>'
					+	'<div class="col-md-1 text-center pt-10">'
					+		'<div class="checkbox checkbox-primary pt-0">'
					+			'<input id="Taxable'+x+'" name="Taxable[]" type="checkbox" value="1" OnChange="CalculatePriceTotal('+x+')">'
					+			'<label for="checkbox2"></label>'
					+		'</div>'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="NetTotal'+x+'" name="NetTotal[]" class="form-control NetTotal" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+	'</div>'
					+	'<div class="col-md-1 text-center">'
					+		'<a href="#" class="btn btn-default remove_line_item_field blank-new-field"><i class="fa fa-trash-o"></i></a>'
					+	'</div>'
					+'</div>'); //add input box
		}
		//$(".select2").select2();
		$('.selectpicker').selectpicker();
    });
   
    $(document).on("click",".remove_line_item_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.product-line-items').remove(); x--;

        CalculateGrandTotal();
    });

    CalculateGrandTotal();
});

$(document).on("click",".add-new-line-item", function(e){ //user click on remove text
   $(".add_line_item_field_button").click();
});

$(document).on("click",".edit-line-items", function(e){ //user click on remove text
   $(".blank-new-field.remove_line_item_field").click();
});

function GetProductDetails(ProductID, x)
{ 
	$.ajax({
        url: '<?=SITE_URL?>WorkOrder/Actions/GetProductDetails/'+ProductID,
        method: "post",
       /* data: { ProductID : ProductID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$('#ListPrice'+x).val(result.ListPrice);
            	if(result.Taxable){ 
            		$( "#Taxable"+x ).prop( "checked", true);
            	}
         
            	if(result.IsListPriceEditable == 0){ 
            		$( "#ListPrice"+x ).prop( "readonly", true);
            	} else {
            		$( "#ListPrice"+x ).removeAttr( "readonly");
            	}
            	CalculatePriceTotal(x);
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });

}

function CalculatePriceTotal(x)
{ 
	var ListPrice = parseFloat($('#ListPrice'+x).val().replace(',', ''));
	var Discount = parseFloat($('#Discount'+x).val().replace(',', ''));
	var Quantity = parseFloat($('#Quantity'+x).val().replace(',', ''));

	var UnitPrice = ListPrice - (ListPrice * Discount) / 100;
	var SubTotal = UnitPrice * Quantity;
	if($('#Taxable'+x).is(':checked'))
		var NetTotal = SubTotal + (SubTotal * <?=$WorkOrderData->Tax;?>) / 100;
	else 
		var NetTotal = SubTotal;
	 
	$('#ListPrice'+x).val(NumberFormat(ListPrice));
	$('#UnitPrice'+x).val(NumberFormat(UnitPrice));
	$('#SubTotal'+x).val(NumberFormat(SubTotal));
	$('#NetTotal'+x).val(NumberFormat(NetTotal));

	CalculateGrandTotal();
}

function CalculateGrandTotal()
{ 
	var GrandTotal = 0;

    $(".NetTotal").each(function(){

    	GrandTotal = parseFloat(GrandTotal) + parseFloat($(this).val().replace(',', ''));
    });
	$('#GrandTotal').val(NumberFormat(GrandTotal));
}

function NumberFormat(n) {
  return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}

/* Line Items End */ 


/* Chemical Line Items Start*/

$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".chemical_line_item_fields_wrap"); //Fields wrapper
   
    var x = <?=count($Chemicals)-1;?>; //initlal text box count
    $(".add_chemical_line_item_field_button").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="col-md-12 chemical-line-items">'
						+	'<div class="panel-group accordion-struct accordion-style-1" id="accordion_2" role="tablist" aria-multiselectable="true">'
						+		 '<div class="panel panel-default pt-10">'
						+		 	'<div class="panel-heading activestate text-center col-md-1" role="tab" id="heading_10" style="border-bottom: none;">'
						+				'<a role="button" class="btn btn-default btn-outline" data-toggle="collapse" data-parent="#accordion_2" href="#collapse_'+x+'" aria-expanded="true" ><div class="icon-ac-wrap"><span class="plus-ac"><i class="ti-plus"></i></span><span class="minus-ac"><i class="ti-minus"></i></span></div></a>' 
						+			'</div>'
						+			'<div class="col-md-11">'
						+				'<div class="col-md-3">'
						+				'<select name="Product[]" id="Product" class="form-control select2" required>'
						+					'<option value="">Select Chemical</option>'
											<?php $Chemicals = $this->ActionsModel->GetChemicals();
												foreach ($Chemicals as $Chemical) {  ?>
						+							'<option value="<?=$Chemical->ProductID?>"><?=$Chemical->ProductName?></option>'
											<?php  } ?>
						+					'</select>'
						+				'</div>'
						+				'<div class="col-md-4">'
						+					'<div class="col-md-1"></div>'
						+					'<div class="col-md-4">' 
						+						'<input id="TestedConcentration" name="TestConcentration[]" class="form-control" type="text" required>'
						+					'</div>'
						+					'<div class="col-md-6">' 
						+						'<select name="TestedUnitOfMeasure[]" id="TestedUnitOfMeasure" class="form-control" required>'
						+							'<option value="">Select</option>'
													<?php $UnitOfMeasurements = $this->ActionsModel->GetUnitOfMeasurement();
														foreach ($UnitOfMeasurements as $UnitOfMeasurement) { ?>
						+									'<option value="<?=$UnitOfMeasurement->UnitOfMeasurementID?>"><?=$UnitOfMeasurement->UnitOfMeasurement?></option>'
													<?php } ?>
						+						'</select>'
						+					'</div>'
						+				'</div>'
						+				'<div class="col-md-4">'
						+					'<div class="col-md-1"></div>'
						+					'<div class="col-md-4">' 
						+						'<input id="ApplicationAmount" name="ApplicationAmount[]" class="form-control" type="text" required>'
						+					'</div>'
						+					'<div class="col-md-6">' 
						+						'<select name="ApplicationUnitOfMeasure[]" id="ApplicationUnitOfMeasure" class="form-control" required>'
						+							'<option value="">Select</option>'
													<?php $UnitOfMeasurements = $this->ActionsModel->GetUnitOfMeasurement();
														foreach ($UnitOfMeasurements as $UnitOfMeasurement) { ?>
						+									'<option value="<?=$UnitOfMeasurement->UnitOfMeasurementID?>"><?=$UnitOfMeasurement->UnitOfMeasurement?></option>'
													<?php } ?>
						+						'</select>'
						+					'</div>'
						+				'</div>'
						+				'<div class="col-md-1 text-center">'
						+					'<a href="#" class="btn btn-default remove_chemical_line_item_field blank-chemical-new-field"><i class="fa fa-trash-o"></i></a>'
						+				'</div>'
						+			'</div>'
						+			'<div id="collapse_'+x+'" class="panel-collapse collapse in col-md-12 pt-10" role="tabpanel">'
						+				'<div class="col-md-2">'
						+					'<strong>Application Area</strong>'
						+				'</div>'
						+				'<div class="col-md-4">' 
						+					'<textarea class="form-control" name="ApplicationArea[]"></textarea>'
						+				'</div>'
						+				'<div class="col-md-2">' 
						+					'<strong>Additional Notes</strong>'
						+				'</div>'
						+				'<div class="col-md-4">' 
						+					'<textarea class="form-control" name="AdditionalNotes[]"></textarea>'
						+				'</div>'
						+			'</div>'
						+			'</div>'
						+		'</div><div class="clearfix"></div><hr class="light-grey-hr">'
						+	'</div>'); //add input box
		}
		$(".select2").select2();
    });
   
    $(document).on("click",".remove_chemical_line_item_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.chemical-line-items').remove(); x--;
    });

    $(document).on("click",".add-chemical-new-line-item", function(e){ //user click on remove text
	   $(".add_chemical_line_item_field_button").click();
	});

	$(document).on("click",".edit-chemical-line-items", function(e){ //user click on remove text
	   $(".blank-chemical-new-field.remove_chemical_line_item_field").click();
	});
});

/* Chemocal Line Items End*/
</script>


