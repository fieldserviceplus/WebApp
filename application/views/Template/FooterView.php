
			
			<!-- Footer -->
			<footer class="footer container-fluid pl-30 pr-30">
				<div class="row">
					<div class="col-sm-12">
						<p><?=date("Y");?> &copy; <?=MAINTITLE;?>. Pampered by <?=MAINTITLE;?></p>
					</div>
				</div>
			</footer>
			<!-- /Footer -->
			
		</div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
	
	<!-- JavaScript -->
	
    <!-- jQuery -->
    <script src="<?=SITE_URL;?>vendors/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=SITE_URL;?>vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    
	
	<!-- Slimscroll JavaScript -->
	 <script src="<?=SITE_URL;?>dist/js/jquery.slimscroll.js"></script> 
	 
	<!-- Fancy Dropdown JS -->
	<!-- <script src="dist/js/dropdown-bootstrap-extended.js"></script> -->
	
	<!-- Sparkline JavaScript -->
	<!-- <script src="vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script> -->
	
	<!-- Owl JavaScript -->
	<!-- <script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script> -->
	
	<!-- Toast JavaScript -->
	<!-- <script src="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script> -->
	
	<!-- Switchery JavaScript -->
	<!-- <script src="vendors/bower_components/switchery/dist/switchery.min.js"></script> -->

	
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>	
	<!-- Init JavaScript -->
	<script src="<?=SITE_URL;?>dist/js/init.js"></script>


	<!-- simpleWeather JavaScript -->
	<!-- <script src="vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
	<script src="dist/js/simpleweather-data.js"></script> -->
	
	<!-- Progressbar Animation JavaScript -->
	<!-- <script src="vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
	<script src="vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script> -->


	<!-- Bootstrap Datepicker JavaScript -->
	<script src="<?=SITE_URL;?>vendors/bower_components/bootstrap-datepicker/ajax/libs/bootstrap-datepicker.js"></script> 
	<script type="text/javascript">
	$(function () {
	  $(".showdatepicker").datepicker({ 
	        autoclose: true, 
	        todayHighlight: true,
	        todayBtn: "linked",
	  }); // .datepicker('update', new Date())
	});
	</script>

	<!-- Moment JavaScript -->
	<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
	<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript">
	$('.showdatetimepicker').datetimepicker({
		useCurrent: true,
		icons: {
	            time: "fa fa-clock-o",
	            date: "fa fa-calendar",
	            up: "fa fa-arrow-up",
	            down: "fa fa-arrow-down"
	        }
	});
	

	/* Common Validation */
	$('.allow-number-dot').keypress(function(evt){
	    return (/^[0-9]*\.?[0-9]*$/).test($(this).val()+evt.key);
	});

	$('.allow-url').keypress(function(evt){
	    return (/^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/).test($(this).val()+evt.key);
	});

</script>

	<!-- Toast JavaScript -->
	<script src="<?=SITE_URL?>vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
	<!-- For Toast Message Start -->
	<script type="text/javascript">
	<?php if($this->session->flashdata('SuccessMsg')){ ?>
		$.toast().reset('all');
		$("body").removeAttr('class');
		$.toast({
	        heading: 'Yay! Success',
	        text: '<?=$this->session->flashdata('SuccessMsg');?>',
	        position: 'top-right',
	        loaderBg:'#5fc55f',
	        icon: 'success',
	        hideAfter: 4000
	    });
	<?php } else if($this->session->flashdata('ErrorMsg')){ ?>
		$.toast().reset('all');
		$("body").removeAttr('class');
		$.toast({
	        heading: 'Opps! Fail',
	        text: '<?=$this->session->flashdata('ErrorMsg');?>',
	        position: 'top-right',
	        loaderBg:'#5fc55f',
	        icon: 'error',
	        hideAfter: 4000
	    });
	 <?php } ?>
	 </script>
	 <!-- For Toast Message End -->

</body>
</html>