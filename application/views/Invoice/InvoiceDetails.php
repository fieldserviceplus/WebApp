
<?php $this->load->view('Template/HeaderView'); ?>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">


<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10 pb-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['Invoice']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['Invoice']['title']?></span>
									<span class="panel-title txt-dark"><?=$InvoiceData->InvoiceNo;?></span>
								</div>
							</div>
						</div>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div  class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="col-lg-7 col-md-5 col-sm-4 col-xs-12">
							<div class="form-group">
								<ul role="tablist" class="nav nav-pills" id="myTabs_13">
									<li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_13" href="#home_13">Detail</a></li>
									<li role="presentation" class=""><a data-toggle="tab" id="profile_tab_13" role="tab" href="#profile_13" aria-expanded="false">Related</a></li>
								</ul>
							</div>
						</div>

						<div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">
							<div class="btn-group pull-right">
									<div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
												<a><span><strong>Communication</strong></span></a>
											</li>
											<li> 
												<a href="<?=SITE_URL;?>Common/Actions/SendEmail/Invoice/<?=$InvoiceData->InvoiceID;?>"><span>Send Email</span></a>
											</li>
											<li> 
												<a href="<?=SITE_URL;?>Invoice/Actions/GenerateDocument/<?=$InvoiceData->InvoiceID?>/"><span>Generate Document</span></a>
											</li>
											<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
												<a><span><strong>Add New Actions</strong></span></a>
											</li>
											<li> 
												<a href="#" class="add-new-line-item" data-toggle="modal" data-target="#create-line-items-modal"><span>New Line Item</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>Task/Actions/CreateTask/?Invoice=<?=$InvoiceData->InvoiceID?>"><span>New Task</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>Event/Actions/CreateEvent/?Invoice=<?=$InvoiceData->InvoiceID?>"><span>New Event</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#CreateNewFile" ><span>New File</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#CreateNewNote"><span>New Note</span></a>
											</li>
											<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
												<a><span><strong>Invoice Actions</strong></span></a>
											</li>
											<li> 
												<a href="<?=SITE_URL;?>Invoice/Actions/CreateInvoice/<?=$InvoiceData->InvoiceID?>/"><span>Copy Invoice</span></a>
											</li>
											<li>
												<a href="#" onclick="DeleteInvoice();"><span>Delete Invoice</span></a>
											</li>
										</ul>
									</div>
								</div>
								<div class="btn-group pull-right">
									<a href="<?=SITE_URL?>Invoice/Actions/EditInvoice/<?=$InvoiceData->InvoiceID;?>" class="btn btn-primary btn-outline">Edit</a>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
			 	<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Invoice Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>  -->
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Invoice/Actions/EditInvoice/<?=$InvoiceData->InvoiceID?>" class="form-horizontal" id="EditInvoice" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>Invoice Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">
												
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Invoice No
														</div>
														<div class="col-md-8">
															<?=$InvoiceData->InvoiceNo;?>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Assigned To
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$InvoiceData->AssignedTo?>"><?=$InvoiceData->AssignedToName;?></a>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Account
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Account/Actions/AccountDetails/<?=$InvoiceData->Account?>"><?=$InvoiceData->AccountName;?></a>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Work Order
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>WorkOrder/Actions/WorkOrderDetails/<?=$InvoiceData->WorkOrder?>"><?=$InvoiceData->WOSubject;?></a>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Contact
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Contact/Actions/ContactDetails/<?=$InvoiceData->Contact?>"><?=$InvoiceData->ContactName;?></a>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Description
														</div>
														<div class="col-md-8">
															<?=$InvoiceData->Description;?>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Additional Information
														</div>
														<div class="col-md-8">
															<?=$InvoiceData->AdditionalInformation;?>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Invoice Status
														</div>
														<div class="col-md-8">
															<?=$InvoiceData->Status;?>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Invoice Date
														</div>
														<div class="col-md-8">
															<?=$InvoiceData->InvoiceDate;?>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Payment Terms
														</div>
														<div class="col-md-8">
															<?=$InvoiceData->PaymentTerms;?>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Due Date
														</div>
														<div class="col-md-8">
															<?=$InvoiceData->DueDate;?>
														</div>
													</div>
											</div>
											</div>

											<div class="col-md-12">
											<div class="txt-dark capitalize-font pt-10"><strong>Financials</strong></div>
											<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Sub Total
														</div>
														<div class="col-md-8">
															<i class="fa fa-usd"></i> <?=number_format($InvoiceFinancials['SubTotal'], 2, '.', ',');?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Discount
														</div>
														<div class="col-md-8">
															<?=number_format((double)$InvoiceFinancials['Discount'], 2, '.', '');?>%
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Tax
														</div>
														<div class="col-md-8">
															<i class="fa fa-usd"></i> <?=number_format((double)$InvoiceFinancials['Tax'], 2, '.', ',');?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Total Price
														</div>
														<div class="col-md-8">
															<i class="fa fa-usd"></i> <?=number_format((double)$InvoiceFinancials['TotalPrice'], 2, '.', ',');?>
														</div>
													</div>

													<!-- <div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Grand Total
														</div>
														<div class="col-md-8">
															<i class="fa fa-usd"></i> <?=number_format((double)$InvoiceFinancials['GrandTotal'], 2, '.', ',');?>
														</div>
													</div> -->

												</div>
												<div class="col-md-6">

													<!-- <div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Line Item Cnt
														</div>
														<div class="col-md-8">
															<?=count($InvoiceLineItems);?>
														</div>
													</div> -->
												</div>
											</div>

											<div class="col-md-12">
											<div class="txt-dark capitalize-font pt-10"><strong>Address Information</strong></div>
											<hr class="light-grey-hr">
												<div class="col-md-6">	
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Address
														</div>
														<div class="col-md-8">
															<div><?=$InvoiceData->Address;?></div>
															<div><?=$InvoiceData->City;?>, <?=$InvoiceData->State;?> <?=$InvoiceData->PostalCode;?></div>
															<div><?=$InvoiceData->Country;?></div>
														</div>
													</div>
												</div>
												<!--/span-->
											</div>

											<!-- Start Custom Fields Row -->
											<?php $this->load->view('Setup/Customize/Fields/CustomFieldsView', array('Object' => 'Invoice', 'ObjectID' => $InvoiceData->InvoiceID)); ?>
											<!-- End Custom Fields Row -->

											<div class="col-md-12">

												<div class="txt-dark capitalize-font pt-10"><strong>System Information</strong></div>
												<hr class="light-grey-hr">
												
												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Create Date
														</div>
														<div class="col-md-8">
															<?=$InvoiceData->CreatedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Created By
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$InvoiceData->CreatedBy?>"><?=$InvoiceData->CreatedByName;?></a>
														</div>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified Date
														</div>
														<div class="col-md-8">
															<?=$InvoiceData->LastModifiedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified By
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$InvoiceData->LastModifiedBy?>"><?=$InvoiceData->LastModifiedByName;?></a>
														</div>
													</div>
												</div>
											</div>


										</div>
											<!-- /Row -->
									</div>

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			</div>
			<div id="profile_13" class="tab-pane fade" role="tabpanel">

				<?php $this->load->view('Invoice/Include/RelatedInformation'); ?>
				
			</div>
		</div>
		</div>
 
		<!-- <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">

			<?php //$this->load->view('Invoice/Include/SchedulingAndNotes'); ?>
			
		</div> -->

	</div>
	<!-- /Row -->
</div>

<!-- Start Modal -->

<?php //$this->load->view('Invoice/Modal/CreateNoteView'); ?>
<?php $this->load->view('Invoice/Modal/CreateLineItemsView'); ?>
<?php $data = array('data' => array('RelatedTo' => 'Invoice', 'What' => $InvoiceData->InvoiceID, 'AssignedTo' => $InvoiceData->AssignedTo));
$this->load->view('Common/Modal/CreateNewFile', $data);
$this->load->view('Common/Modal/CreateNewNote', $data); ?>

<?php $this->load->view('Template/FooterView');?>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Use for common object  -->
<script src="<?=SITE_URL?>dist/js/common.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	$(".select2").select2();
	$('.selectpicker').selectpicker();

});
</script>

<script type="text/javascript">

$(document).ready(function () {

    $('.showdatetimepicker').datetimepicker({
		useCurrent: true,
		icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
	});

   /* $('.timepicker').datetimepicker({
			format: 'LT',
			useCurrent: false,
			icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
	}).data("DateTimePicker").date(moment());*/

    /*$('#CreateNoteForm').validate({ 
        rules: {
            Subject: {
                required: true
            },
            Body: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });*/

}); 
function DeleteInvoice(InvoiceID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#5fc55f",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>Invoice/Actions/DeleteInvoice/<?=$InvoiceData->InvoiceID;?>/";
		}, 500);
    });
	return false;
}
</script>

<script type="text/javascript">
/* Line Items Start */ 
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".line_item_fields_wrap"); //Fields wrapper
  
    var x = <?=count($InvoiceLineItems)-1;?>; //initlal text box count
    $(".add_line_item_field_button").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="col-md-12 pt-10 product-line-items">'
					+	'<div class="col-md-4">'
					+	'<select name="Product[]" id="Product" class="selectpicker" data-style="form-control btn-default btn-outline" data-show-subtext="true" data-live-search="true" OnChange="GetProductDetails(this.value, '+x+');">'
					+		'<option value="">Search Product</option>'
							<?php $Products = $this->ActionsModel->GetProducts();
								foreach ($Products as $Product) {  ?>
					+				'<option value="<?=$Product->ProductID?>" data-subtext="<?=$Product->ProductCode?> - $<?=number_format($Product->ListPrice, 2, '.', ',')?>"><?=$Product->ProductName?></option>'
							<?php } ?>
					+		'</select>'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="ListPrice'+x+'" name="ListPrice[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="Discount'+x+'" name="Discount[]" class="form-control" type="text" value="0" OnChange="CalculatePriceTotal('+x+')">'
					
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="UnitPrice'+x+'" name="UnitPrice[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+		'<input type="text" name="Quantity[]" id="Quantity'+x+'" class="form-control" value="1" OnChange="CalculatePriceTotal('+x+')">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="SubTotal'+x+'" name="SubTotal[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+	'</div>'
					+	'<div class="col-md-1 text-center pt-10">'
					+		'<div class="checkbox checkbox-primary pt-0">'
					+			'<input id="Taxable'+x+'" name="Taxable[]" type="checkbox" value="1" OnChange="CalculatePriceTotal('+x+')">'
					+			'<label for="checkbox2"></label>'
					+		'</div>'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="TotalPrice'+x+'" name="TotalPrice[]" class="form-control TotalPrice" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+	'</div>'
					+	'<div class="col-md-1 text-center">'
					+		'<a href="#" class="btn btn-default remove_line_item_field blank-new-field"><i class="fa fa-trash-o"></i></a>'
					+	'</div>'
					+'</div>'); //add input box
		}
		//$(".select2").select2();
		$('.selectpicker').selectpicker();
    });
   
    $(document).on("click",".remove_line_item_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.product-line-items').remove(); x--;

        CalculateGrandTotal();
    });

    CalculateGrandTotal();
});

$(document).on("click",".add-new-line-item", function(e){ //user click on remove text
   $(".add_line_item_field_button").click();
});

$(document).on("click",".edit-line-items", function(e){ //user click on remove text
   $(".blank-new-field.remove_line_item_field").click();
});

function GetProductDetails(ProductID, x)
{ 
	$.ajax({
        url: '<?=SITE_URL?>Invoice/Actions/GetProductDetails/'+ProductID,
        method: "post",
       /* data: { ProductID : ProductID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$('#ListPrice'+x).val(result.ListPrice);
            	if(result.Taxable){ 
            		$( "#Taxable"+x ).prop( "checked", true);
            	}
         
            	if(result.IsListPriceEditable == 0){ 
            		$( "#ListPrice"+x ).prop( "readonly", true);
            	} else {
            		$( "#ListPrice"+x ).removeAttr( "readonly");
            	}
            	CalculatePriceTotal(x);
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });

}

function CalculatePriceTotal(x)
{ 
	var ListPrice = parseFloat($('#ListPrice'+x).val().replace(',', ''));
	var Discount = parseFloat($('#Discount'+x).val().replace(',', ''));
	var Quantity = parseFloat($('#Quantity'+x).val().replace(',', ''));

	var UnitPrice = ListPrice - (ListPrice * Discount) / 100;
	var SubTotal = UnitPrice * Quantity;
	if($('#Taxable'+x).is(':checked'))
		var TotalPrice = SubTotal + (SubTotal * <?=$InvoiceData->Tax;?>) / 100;
	else 
		var TotalPrice = SubTotal;
	 
	$('#ListPrice'+x).val(NumberFormat(ListPrice));
	$('#UnitPrice'+x).val(NumberFormat(UnitPrice));
	$('#SubTotal'+x).val(NumberFormat(SubTotal));
	$('#TotalPrice'+x).val(NumberFormat(TotalPrice));

	CalculateGrandTotal();
}

function CalculateGrandTotal()
{ 
	var GrandTotal = 0;

    $(".TotalPrice").each(function(){

    	GrandTotal = parseFloat(GrandTotal) + parseFloat($(this).val().replace(',', ''));
    });
	$('#GrandTotal').val(NumberFormat(GrandTotal));
}

function NumberFormat(n) {
  return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}

/* Line Items End */ 
</script>



