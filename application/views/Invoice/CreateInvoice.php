
<?php $this->load->view('Template/HeaderView')?>
<!-- Bootstrap Datetimepicker CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_JS_KEY?>&libraries=places"></script>
 
 <!--Signature Pad CSS -->
<!-- <link href="<?=SITE_URL?>dist/css/signature_pad.css" rel="stylesheet" type="text/css"> -->


<div class="container-fluid pt-25">
	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Create Invoice</h6>
					</div>
					<div class="pull-right">
						<span style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong></span>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Invoice/Actions/CreateInvoice" class="form-horizontal" id="CreateInvoice" method="post">

									<?php if(isset($_GET['Account']) && !empty($_GET['Account'])){ ?>
											<input type="hidden" name="Redirect" value="Account">
									<?php } ?>
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>Invoice Details</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Assigned To<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="AssignedTo" id="AssignedTo" class="form-control select2">
														<option value="">Select Assigned To</option>
														<?php $Users = $this->ActionsModel->GetUsers(); 
															foreach ($Users as $User) { 
															if(isset($InvoiceData->AssignedTo) && $InvoiceData->AssignedTo == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else { ?>
																<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('AssignedTo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Account<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="Account" id="Account" class="form-control select2" OnChange="GetPrimaryContactOptions(this.value);GetAccountDetails(this.value);">
														<option value="">Select Account</option>
														<?php $Accounts = $this->ActionsModel->GetAccounts();
															foreach ($Accounts as $Account) { 
																if((isset($InvoiceData->Account) && $InvoiceData->Account == $Account->AccountID) || (isset($_GET['Account']) && $_GET['Account'] == $Account->AccountID)){ ?>
																	<option value="<?=$Account->AccountID?>" selected="selected"><?=$Account->AccountName;?></option>
																<?php } else { ?>
																	<option value="<?=$Account->AccountID?>"><?=$Account->AccountName;?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('Account');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Work Order<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="WorkOrder" id="WorkOrder" class="form-control select2">
														<option value="">Select Work Order</option>
														<?php $WorkOrders = $this->ActionsModel->GetWorkOrders(); 
															foreach ($WorkOrders as $WorkOrder) { 
															if((isset($InvoiceData->WorkOrder) && $InvoiceData->WorkOrder == $WorkOrder->WorkOrderID) || (isset($_GET['WorkOrder']) && $_GET['WorkOrder'] == $WorkOrder->WorkOrderID)){ ?>
																	<option value="<?=$WorkOrder->WorkOrderID?>" selected="selected"><?=$WorkOrder->WorkOrderNo?> - <?=$WorkOrder->Subject?></option>
																<?php } else { ?>
																<option value="<?=$WorkOrder->WorkOrderID?>"><?=$WorkOrder->WorkOrderNo?> - <?=$WorkOrder->Subject?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('WorkOrder');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Contact</label>
														<div class="col-md-9">
														<?php $Contacts = $this->ActionsModel->GetContacts(); ?>
															<select class="form-control select2 SelectPrimaryContact" name="Contact" id="Contact" OnChange="GetContactDetails(this.value);">
															<option value="">Select Contact</option>
															<?php echo form_error('Contact');  ?>
														</select>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Description<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Description" id="Description" value="<?=isset($InvoiceData->Description)?$InvoiceData->Description:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Additional Information</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="AdditionalInformation" id="AdditionalInformation" value="<?=isset($InvoiceData->AdditionalInformation)?$InvoiceData->AdditionalInformation:"";?>">
														</div>
													</div>


												</div>

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Invoice Status<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<?php $GetInvoiceStatus = $this->ActionsModel->GetInvoiceStatus(); ?>
															<select class="form-control" name="InvoiceStatus" id="InvoiceStatus">
															<option value="">Select Status</option>
															<?php foreach ($GetInvoiceStatus as $InvoiceStatus) { 
																if(isset($InvoiceData->InvoiceStatus) && $InvoiceData->InvoiceStatus == $InvoiceStatus->InvoiceStatusID){ ?>
																	<option value="<?=$InvoiceStatus->InvoiceStatusID?>" selected="selected"><?=$InvoiceStatus->InvoiceStatus?></option>
																<?php } else { ?>
																	<option value="<?=$InvoiceStatus->InvoiceStatusID?>"><?=$InvoiceStatus->InvoiceStatus?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('InvoiceStatus');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Invoice Date<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control showdatepicker" name="InvoiceDate" id="InvoiceDate" value="<?=isset($InvoiceData->InvoiceDate)?$InvoiceData->InvoiceDate:date('m/d/Y');?>">
															<?php echo form_error('InvoiceDate');  ?>
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-3">Payment Terms<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="PaymentTerms" id="PaymentTerms" class="form-control select2">
														<?php $PaymentTerms = $this->ActionsModel->GetInvoicePaymentTerms(); 
															foreach ($PaymentTerms as $PaymentTerm) { 
															if(isset($InvoiceData->PaymentTerms) && $InvoiceData->PaymentTerms == $PaymentTerm->InvoicePaymentTermID){ ?>
																	<option value="<?=$PaymentTerm->InvoicePaymentTermID?>" selected="selected"><?=$PaymentTerm->PaymentTerms?></option>
																<?php } else { ?>
																<option value="<?=$PaymentTerm->InvoicePaymentTermID?>"><?=$PaymentTerm->PaymentTerms?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('PaymentTerms');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Due Date<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control showdatepicker" name="DueDate" id="DueDate" value="<?=isset($InvoiceData->DueDate)?$InvoiceData->DueDate:date('m/d/Y');?>">
															<?php echo form_error('DueDate');  ?>
														</div>
													</div>

												</div>
											</div>

											<div class="col-md-12">
												<div class="txt-dark capitalize-font"><strong>Address Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Address</label>
														<div class="col-md-9">
															<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-search"></i>Search Address</button>
														</div>
													</div>
												
													<div class="form-group">
														<label class="col-sm-3 control-label">Street<span class="required-sign">*</span></label>
														<div class="col-sm-9">
																<input type="text" class="form-control" name="Address" id="Address" placeholder="" data-toggle="modal" data-target="#location-picker-modal" value="<?=isset($InvoiceData->Address)?$InvoiceData->Address:"";?>">
															<?php echo form_error('Address');  ?>
															<input type="hidden" name="Latitude" id="Latitude" value="<?=isset($InvoiceData->Latitude)?$InvoiceData->Latitude:"";?>">
															<input type="hidden" name="Latitude" id="Latitude" value="<?=isset($InvoiceData->Latitude)?$InvoiceData->Latitude:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">City<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="City" id="City" value="<?=isset($InvoiceData->City)?$InvoiceData->City:"";?>">
															<?php echo form_error('City');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">State<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="State" id="State" value="<?=isset($InvoiceData->State)?$InvoiceData->State:"";?>">
															<?php echo form_error('State');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-3">Country<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Country" id="Country" value="<?=isset($InvoiceData->Country)?$InvoiceData->Country:"";?>">
															<?php echo form_error('Country');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Postal Code<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="PostalCode" id="PostalCode" value="<?=isset($InvoiceData->PostalCode)?$InvoiceData->PostalCode:"";?>">
															<?php echo form_error('PostalCode');  ?>
														</div>
													</div>
												</div>
											
											<!-- /Row -->
										</div>

										<!-- Start Custom Fields Row -->
										<?php $this->load->view('Setup/Customize/Fields/CustomFieldsForm', array('Object' => 'Invoice', 'ObjectID' => 0)); ?>
										<!-- End Custom Fields Row -->

										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="CreateInvoice">Save</button>
															<a href="<?=SITE_URL;?>Invoice/Actions" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->
</div>

<?php $this->load->view('Invoice/Modal/LocationPickerView'); ?>
<?php $this->load->view('Invoice/Modal/RecurringTaskView'); ?>

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>

<script type="text/javascript">

$(document).ready(function () {
 
    $('#CreateInvoice').validate({ 
        rules: {
            AssignedTo: {
                required: true
            },
            Account: {
                required: true
            },
            WorkOrder: {
                required: true
            },
            Description: {
                required: true
            },
            Address: {
                required: true
            },
            City: {
                required: true
            },
            State: {
                required: true
            },
            Country: {
                required: true
            },
            PostalCode: {
                required: true
            },
            InvoiceStatus: {
                required: true
            },
            InvoiceDate: {
                required: true
            },
            DueDate: {
                required: true
            },
            PaymentTerms: {
                required: true
            },
        },
        errorPlacement: function(){
            return false;
        },
        submitHandler: function (form) { 
        
        	return true;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    $('.showdatetimepicker').datetimepicker({
			/*useCurrent: true,*/
			icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
		});
});
</script>

<script type="text/javascript">
function GetPrimaryContactOptions(AccountID, PrimaryContact = 0){
	$.ajax({
        url: '<?=SITE_URL?>Invoice/Actions/GetPrimaryContactOptions/',
        method: "post",
        data: { AccountID : AccountID, PrimaryContact: PrimaryContact},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$(".SelectPrimaryContact").html(result);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}

function GetAccountDetails(AccountID){
	$.ajax({
        url: '<?=SITE_URL?>Invoice/Actions/GetAccountDetails/'+AccountID,
        method: "post",
      /*  data: { AccountID : AccountID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$("#Address").val(result.BillingAddress);
            	$("#City").val(result.BillingCity);
            	$("#State").val(result.BillingState);
            	$("#Country").val(result.BillingCountry);
            	$("#PostalCode").val(result.BillingPostalCode);
            	$("#Latitude").val(result.BillingLatitude);
            	$("#Longitude").val(result.BillingLongitude);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}

$(document).ready(function () {
<?php if(isset($_GET['Account']) && !empty($_GET['Account'])){ ?>
		GetAccountDetails(<?=$_GET['Account']?>);
<?php } ?>
 });

$(document).ready(function () {
	<?php if(isset($_GET['Account']) && !empty($_GET['Account'])){ ?>
			GetAccountDetails(<?=$_GET['Account']?>);
			GetPrimaryContactOptions(<?=$_GET['Account']?>);
	<?php } if(isset($_GET['Contact']) && $_GET['Contact']){ ?>
			GetPrimaryContactOptions(<?=$_GET['Account']?>, <?=$_GET['Contact']?>);
			GetAccountDetails(<?=$_GET['Account']?>);
	<?php } ?>
 });
</script>

<!-- Set Location -->
<script src="<?=SITE_URL?>dist/js/location-picker.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	
	set_location_in_map();
});
</script>