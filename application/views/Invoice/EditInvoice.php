<?php $this->load->view('Template/HeaderView'); ?>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

 <link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_JS_KEY?>&libraries=places"></script>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<!--Signature Pad CSS -->
<!-- <link href="<?=SITE_URL?>dist/css/signature_pad.css" rel="stylesheet" type="text/css"> -->

<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10 pb-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['Invoice']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['Invoice']['title']?></span>
									<span class="panel-title txt-dark"><?=$InvoiceData->InvoiceNo;?></span>
								</div>
							</div>
						</div>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div  class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="col-lg-7 col-md-5 col-sm-4 col-xs-12">
							<!-- <div class="form-group"> -->
								<ul role="tablist" class="nav nav-pills" id="myTabs_13">
									<li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_13" href="#home_13">Detail</a></li>
									<li role="presentation" class=""><a data-toggle="tab" id="profile_tab_13" role="tab" href="#profile_13" aria-expanded="false">Related</a></li>
								</ul>
							<!-- </div> -->
						</div>
						<div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">
							<div class="btn-group pull-right">
								<!-- <div class="dropdown">
									<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
									<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
										<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
											<a><span><strong>Communication</strong></span></a>
										</li>
										<li> 
											<a href="<?=SITE_URL;?>Common/Actions/SendEmail/Invoice/<?=$InvoiceData->InvoiceID;?>"><span>Send Email</span></a>
										</li>
										<li> 
											<a href="#"><span>Generate Document</span></a>
										</li>
										<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
											<a><span><strong>Add New Actions</strong></span></a>
										</li>
										<li> 
											<a href="#" data-toggle="modal" data-target="#create-line-items-modal"><span>New Line Item</span></a>
										</li>
										<li>
											<a href="#"><span>New Task</span></a>
										</li>
										<li>
											<a href="#"><span>New Event</span></a>
										</li>
										<li> 
											<a href="#" data-toggle="modal" data-target="#CreateNewFile"><span>New File</span></a>
										</li>
										<li>
											<a href="#" data-toggle="modal" data-target="#CreateNewNote"><span>New Note</span></a>
										</li>
										<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
											<a><span><strong>Invoice Actions</strong></span></a>
										</li>
										<li> 
											<a href="<?=SITE_URL;?>Invoice/Actions/CreateInvoice/<?=$InvoiceData->InvoiceID?>/"><span>Copy Invoice</span></a>
										</li>
										<li>
											<a href="#" onclick="DeleteInvoice(<?=$InvoiceData->InvoiceID?>);"><span>Delete Invoice</span></a>
										</li>
									</ul>
								</div> -->
							</div>
							<div class="btn-group pull-right">
								<a href="<?=SITE_URL;?>Invoice/Actions/InvoiceDetails/<?=$InvoiceData->InvoiceID?>" class="btn btn-default">Cancel</a>
								<a href="#" class="btn btn-primary" id="EditInvoiceLink">Save</a>

							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
			 	<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Invoice Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>  -->
				<div class="panel-wrapper collapse in">
					<div class="panel-body"> 
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Invoice/Actions/EditInvoice/<?=$InvoiceData->InvoiceID?>" class="form-horizontal" id="EditInvoice" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font">
													<strong>Invoice Information</strong>
													<span class="pull-right" style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong>
													</span>
												</div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-4">Invoice No</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$InvoiceData->InvoiceNo;?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Assigned To<span class="required-sign">*</span></label>
														<div class="col-md-8">
														<select name="AssignedTo" id="AssignedTo" class="form-control select2">
														<option value="">Select Assigned To</option>
														<?php $Users = $this->ActionsModel->GetUsers(); 
															foreach ($Users as $User) { 
															if(isset($InvoiceData->AssignedTo) && $InvoiceData->AssignedTo == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else { ?>
																<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('AssignedTo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Account<span class="required-sign">*</span></label>
														<div class="col-md-8">
														<select name="Account" id="Account" class="form-control select2" OnChange="GetPrimaryContactOptions(this.value);GetAccountDetails(this.value);">
														<option value="">Select Account</option>
														<?php $Accounts = $this->ActionsModel->GetAccounts();
															foreach ($Accounts as $Account) { 
																if((isset($InvoiceData->Account) && $InvoiceData->Account == $Account->AccountID) || (isset($_GET['Account']) && $_GET['Account'] == $Account->AccountID)){ ?>
																	<option value="<?=$Account->AccountID?>" selected="selected"><?=$Account->AccountName;?></option>
																<?php } else { ?>
																	<option value="<?=$Account->AccountID?>"><?=$Account->AccountName;?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('Account');  ?>
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-4">Work Order<span class="required-sign">*</span></label>
														<div class="col-md-8">
														<select name="WorkOrder" id="WorkOrder" class="form-control select2">
														<option value="">Select Work Order</option>
														<?php $WorkOrders = $this->ActionsModel->GetWorkOrders(); 
															foreach ($WorkOrders as $WorkOrder) { 
															if(isset($InvoiceData->WorkOrder) && $InvoiceData->WorkOrder == $WorkOrder->WorkOrderID){ ?>
																	<option value="<?=$WorkOrder->WorkOrderID?>" selected="selected"><?=$WorkOrder->WorkOrderNo?> - <?=$WorkOrder->Subject?></option>
																<?php } else { ?>
																<option value="<?=$WorkOrder->WorkOrderID?>"><?=$WorkOrder->WorkOrderNo?> - <?=$WorkOrder->Subject?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('WorkOrder');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Contact</label>
														<div class="col-md-8">
														<?php $Contacts = $this->ActionsModel->GetContacts(); ?>
															<select class="form-control select2 SelectPrimaryContact" name="Contact" id="Contact" OnChange="GetContactDetails(this.value);">
															<option value="">Select Contact</option>
															<?php echo form_error('Contact');  ?>
														</select>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Description<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Description" id="Description" value="<?=isset($InvoiceData->Description)?$InvoiceData->Description:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Additional Information</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="AdditionalInformation" id="AdditionalInformation" value="<?=isset($InvoiceData->AdditionalInformation)?$InvoiceData->AdditionalInformation:"";?>">
														</div>
													</div>

												</div>
												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-4">Invoice Status<span class="required-sign">*</span></label>
														<div class="col-md-8">
														<?php $GetInvoiceStatus = $this->ActionsModel->GetInvoiceStatus(); ?>
															<select class="form-control" name="InvoiceStatus" id="InvoiceStatus">
															<option value="">Select Status</option>
															<?php foreach ($GetInvoiceStatus as $InvoiceStatus) { 
																if(isset($InvoiceData->InvoiceStatus) && $InvoiceData->InvoiceStatus == $InvoiceStatus->InvoiceStatusID){ ?>
																	<option value="<?=$InvoiceStatus->InvoiceStatusID?>" selected="selected"><?=$InvoiceStatus->InvoiceStatus?></option>
																<?php } else { ?>
																	<option value="<?=$InvoiceStatus->InvoiceStatusID?>"><?=$InvoiceStatus->InvoiceStatus?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('InvoiceStatus');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Invoice Date<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control showdatepicker" name="InvoiceDate" id="InvoiceDate" value="<?=isset($InvoiceData->InvoiceDate)?$InvoiceData->InvoiceDate:date('m/d/Y');?>">
															<?php echo form_error('InvoiceDate');  ?>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-4">Payment Terms<span class="required-sign">*</span></label>
														<div class="col-md-8">
														<select name="PaymentTerms" id="PaymentTerms" class="form-control select2">
														<?php $PaymentTerms = $this->ActionsModel->GetInvoicePaymentTerms(); 
															foreach ($PaymentTerms as $PaymentTerm) { 
															if(isset($InvoiceData->InvoicePaymentTermID) && $InvoiceData->InvoicePaymentTermID == $PaymentTerm->InvoicePaymentTermID){ ?>
																	<option value="<?=$PaymentTerm->InvoicePaymentTermID?>" selected="selected"><?=$PaymentTerm->PaymentTerms?></option>
																<?php } else { ?>
																<option value="<?=$PaymentTerm->InvoicePaymentTermID?>"><?=$PaymentTerm->PaymentTerms?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('PaymentTerms');  ?>
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-4">Due Date<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control showdatepicker" name="DueDate" id="DueDate" value="<?=isset($InvoiceData->DueDate)?$InvoiceData->DueDate:date('m/d/Y');?>">
															<?php echo form_error('DueDate');  ?>
														</div>
													</div>

												</div>
											</div>
											<div class="col-md-12">

											<div class="txt-dark capitalize-font"><strong>Address Information</strong></div>
											<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-4">Address</label>
														<div class="col-md-8">
															<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-search"></i>Search Address</button>
														</div>
													</div>
												
													<div class="form-group">
														<label class="col-sm-4 control-label">Street<span class="required-sign">*</span></label>
														<div class="col-sm-8">
																<input type="text" class="form-control" name="Address" id="Address" placeholder="" data-toggle="modal" data-target="#location-picker-modal" value="<?=isset($InvoiceData->Address)?$InvoiceData->Address:"";?>">
															<?php echo form_error('Address');  ?>
															<input type="hidden" name="Latitude" id="Latitude" value="<?=isset($InvoiceData->Latitude)?$InvoiceData->Latitude:"";?>">
															<input type="hidden" name="Latitude" id="Latitude" value="<?=isset($InvoiceData->Latitude)?$InvoiceData->Latitude:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">City<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="City" id="City" value="<?=isset($InvoiceData->City)?$InvoiceData->City:"";?>">
															<?php echo form_error('City');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">State<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="State" id="State" value="<?=isset($InvoiceData->State)?$InvoiceData->State:"";?>">
															<?php echo form_error('State');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-4">Country<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Country" id="Country" value="<?=isset($InvoiceData->Country)?$InvoiceData->Country:"";?>">
															<?php echo form_error('Country');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Postal Code<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="PostalCode" id="PostalCode" value="<?=isset($InvoiceData->PostalCode)?$InvoiceData->PostalCode:"";?>">
															<?php echo form_error('PostalCode');  ?>
														</div>
													</div>
												</div>

												<!-- <div class="col-md-6">
													
												</div> -->

												<!--/span-->

											</div>
											<div class="col-md-12">

											<div class="txt-dark capitalize-font"><strong>Financials</strong></div>
											<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-4">Sub Total</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="SubTotal" value="<?=number_format((double)$InvoiceFinancials['SubTotal'], 2, '.', '');?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Discount</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Discount" value="<?=number_format((double)$InvoiceFinancials['Discount'], 2, '.', '');?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Tax</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Tax" value="<?=number_format((double)$InvoiceFinancials['Tax'], 2, '.', '');?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Total Price</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="TotalPrice" value="<?=number_format((double)$InvoiceFinancials['TotalPrice'], 2, '.', '');?>" readonly="">
														</div>
													</div>
												</div>
												<div class="col-md-6">

												</div>
											</div>

											<!-- Start Custom Fields Row -->
											<?php $this->load->view('Setup/Customize/Fields/CustomFieldsForm', array('Object' => 'Invoice', 'ObjectID' => $InvoiceData->InvoiceID)); ?>
											<!-- End Custom Fields Row -->

											<div class="col-md-12">

											<div class="txt-dark capitalize-font"><strong>System Information</strong></div>
											<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-4">Create Date</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$InvoiceData->CreatedDate?>" readonly="">
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-4">Created By</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$InvoiceData->CreatedByName?>" readonly="">
														</div>
													</div>

												</div>
												<!--/span-->

												<div class="col-md-6">

												   <div class="form-group">
														<label class="control-label col-md-4">Last Modified Date</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$InvoiceData->LastModifiedDate?>" readonly="">
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-4">Last Modified By</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$InvoiceData->LastModifiedByName?>" readonly="">
														</div>
													</div>
												</div>
											</div>

											</div>
											<!-- /Row -->
										</div>

										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="EditInvoice" id="EditInvoiceBtn">Save</button>
															<a href="<?=SITE_URL;?>Invoice/Actions" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			</div>
			<div id="profile_13" class="tab-pane fade" role="tabpanel">
				
				<?php $this->load->view('Invoice/Include/RelatedInformation'); ?>

			</div>
		</div>
		
		</div>

		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">

			<?php //$this->load->view('Invoice/Include/SchedulingAndNotes'); ?>
			
		</div>

	</div>
	<!-- /Row -->

</div>

<?php //$this->load->view('Invoice/Modal/CreateNoteView'); ?>
<?php $this->load->view('Invoice/Modal/CreateLineItemsView'); ?>
<?php $data = array('data' => array('RelatedTo' => 'Invoice', 'What' => $InvoiceData->InvoiceID, 'AssignedTo' => $InvoiceData->AssignedTo));
$this->load->view('Common/Modal/CreateNewFile', $data);
$this->load->view('Common/Modal/CreateNewNote', $data); ?>

<?php $this->load->view('Template/FooterView')?>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<!-- Use for common object  -->
<script src="<?=SITE_URL?>dist/js/common.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
$('.selectpicker').selectpicker();
</script>

<script type="text/javascript">

$(document).ready(function () {
 
    $('#EditInvoice').validate({ 
        rules: {
            AssignedTo: {
                required: true
            },
            Account: {
                required: true
            },
            WorkOrder: {
                required: true
            },
            Description: {
                required: true
            },
            Address: {
                required: true
            },
            City: {
                required: true
            },
            State: {
                required: true
            },
            Country: {
                required: true
            },
            PostalCode: {
                required: true
            },
            InvoiceStatus: {
                required: true
            },
            InvoiceDate: {
                required: true
            },
            DueDate: {
                required: true
            },
            PaymentTerms: {
                required: true
            },
        },
        errorPlacement: function(){
            return false;
        },
        submitHandler: function (form) { 
        	return true;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    /*$('#CreateNoteForm').validate({ 
        rules: {
            Subject: {
                required: true
            },
            Body: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });*/

    $('.showdatetimepicker').datetimepicker({
		useCurrent: true,
		icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
	});
});

$(document).on('click', "#EditInvoiceLink", function () { 
	$('#EditInvoiceBtn').click(); 
});

function DeleteInvoice(InvoiceID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#5fc55f",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>Invoice/Actions/DeleteInvoice/<?=$InvoiceData->InvoiceID;?>/";
		}, 500);
    });
	return false;
}

function GetAccountDetails(AccountID){
	$.ajax({
        url: '<?=SITE_URL?>Invoice/Actions/GetAccountDetails/'+AccountID,
        method: "post",
      /*  data: { AccountID : AccountID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$("#Address").val(result.BillingAddress);
            	$("#City").val(result.BillingCity);
            	$("#State").val(result.BillingState);
            	$("#Country").val(result.BillingCountry);
            	$("#PostalCode").val(result.BillingPostalCode);
            	$("#Latitude").val(result.BillingLatitude);
            	$("#Longitude").val(result.BillingLongitude);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}

function GetContactDetails(ContactID){
	$.ajax({
        url: '<?=SITE_URL?>Invoice/Actions/GetContactDetails/'+ContactID,
        method: "post",
      /*  data: { AccountID : AccountID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$("#Phone").val(result.PhoneNo);
            	$("#Email").val(result.Email);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}
</script>
<script type="text/javascript">

function GetPrimaryContactOptions(AccountID){
	$.ajax({
        url: '<?=SITE_URL?>Invoice/Actions/GetPrimaryContactOptions/',
        method: "post",
        data: { AccountID : AccountID, PrimaryContact:'<?=$InvoiceData->Contact;?>'},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$(".SelectPrimaryContact").html(result);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}

$(document).ready(function() { 
	GetPrimaryContactOptions(<?=$InvoiceData->Account?>);
});
</script>

<script type="text/javascript">
/* Line Items Start */ 
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".line_item_fields_wrap"); //Fields wrapper
   
    var x = 1; //initlal text box count
    $(".add_line_item_field_button").click(function(e){ //on add input button click
        e.preventDefault();

        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="col-md-12 pt-10 product-line-items">'
					+	'<div class="col-md-4">'
					+	'<select name="Product[]" id="Product" class="selectpicker" data-style="form-control btn-default btn-outline" data-show-subtext="true" data-live-search="true" OnChange="GetProductDetails(this.value, '+x+');">'
					+		'<option value="">Search Product</option>'
							<?php $Products = $this->ActionsModel->GetProducts();
								foreach ($Products as $Product) {  ?>
					+				'<option value="<?=$Product->ProductID?>" data-subtext="<?=$Product->ProductCode?> - $<?=number_format($Product->ListPrice, 2, '.', ',')?>"><?=$Product->ProductName?></option>'
							<?php } ?>
					+		'</select>'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="ListPrice'+x+'" name="ListPrice[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="Discount'+x+'" name="Discount[]" class="form-control" type="text" value="0" OnChange="CalculatePriceTotal('+x+')">'
					
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="UnitPrice'+x+'" name="UnitPrice[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+		'<input type="text" name="Quantity[]" id="Quantity'+x+'" class="form-control" value="1" OnChange="CalculatePriceTotal('+x+')">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="SubTotal'+x+'" name="SubTotal[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+	'</div>'
					+	'<div class="col-md-1 text-center pt-10">'
					+		'<div class="checkbox checkbox-primary pt-0">'
					+			'<input id="Taxable'+x+'" name="Taxable[]" type="checkbox" value="1" OnChange="CalculatePriceTotal('+x+')">'
					+			'<label for="checkbox2"></label>'
					+		'</div>'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="TotalPrice'+x+'" name="TotalPrice[]" class="form-control TotalPrice" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+	'</div>'
					+	'<div class="col-md-1 text-center">'
					+		'<a href="#" class="btn btn-default remove_line_item_field blank-new-field"><i class="fa fa-trash-o"></i></a>'
					+	'</div>'
					+'</div>'); //add input box
		}
		//$(".select2").select2();
		$('.selectpicker').selectpicker();
    });
   
    $(document).on("click",".remove_line_item_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.product-line-items').remove(); x--;

        CalculateGrandTotal();
    });

    CalculateGrandTotal();
});

$(document).on("click",".add-new-line-item", function(e){ //user click on remove text
   $(".add_line_item_field_button").click();
});

$(document).on("click",".edit-line-items", function(e){ //user click on remove text
   $(".blank-new-field.remove_line_item_field").click();
});

function GetProductDetails(ProductID, x)
{ 
	$.ajax({
        url: '<?=SITE_URL?>Invoice/Actions/GetProductDetails/'+ProductID,
        method: "post",
       /* data: { ProductID : ProductID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$('#ListPrice'+x).val(result.ListPrice);
            	if(result.Taxable){ 
            		$( "#Taxable"+x ).prop( "checked", true);
            	}
         
            	if(result.IsListPriceEditable == 0){ 
            		$( "#ListPrice"+x ).prop( "readonly", true);
            	} else {
            		$( "#ListPrice"+x ).removeAttr( "readonly");
            	}
            	CalculatePriceTotal(x);
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });

}

function CalculatePriceTotal(x)
{ 
	var ListPrice = parseFloat($('#ListPrice'+x).val().replace(',', ''));
	var Discount = parseFloat($('#Discount'+x).val().replace(',', ''));
	var Quantity = parseFloat($('#Quantity'+x).val().replace(',', ''));

	var UnitPrice = ListPrice - (ListPrice * Discount) / 100;
	var SubTotal = UnitPrice * Quantity;
	if($('#Taxable'+x).is(':checked'))
		var TotalPrice = SubTotal + (SubTotal * <?=$InvoiceData->Tax;?>) / 100;
	else 
		var TotalPrice = SubTotal;
	 
	$('#ListPrice'+x).val(NumberFormat(ListPrice));
	$('#UnitPrice'+x).val(NumberFormat(UnitPrice));
	$('#SubTotal'+x).val(NumberFormat(SubTotal));
	$('#TotalPrice'+x).val(NumberFormat(TotalPrice));

	CalculateGrandTotal();
}

function CalculateGrandTotal()
{ 
	var GrandTotal = 0;

    $(".TotalPrice").each(function(){

    	GrandTotal = parseFloat(GrandTotal) + parseFloat($(this).val().replace(',', ''));
    });
	$('#GrandTotal').val(NumberFormat(GrandTotal));
}

function NumberFormat(n) {
  return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}

/* Line Items End */ 
</script>


<!-- Set Location -->
<script src="<?=SITE_URL?>dist/js/location-picker.js"></script>
<script type="text/javascript">
$(document).ready(function () {  
	
	<?php if(!empty($InvoiceData->Latitude) && !empty($InvoiceData->Longitude)){ ?>
		set_location_in_map("", <?=$InvoiceData->Latitude;?>, <?=$InvoiceData->Longitude;?>);
	<?php } else { ?>
		set_location_in_map("");
	<?php } ?>
});
</script>