<!-- modal -->
<div class="modal fade" id="account-rename-view-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Rename View</h5>
			</div>
			<form class="form-horizontal" id="RenameViewForm" method="post" action="<?=SITE_URL;?>Invoice/View/RenameCustomView/<?=$SelectedInvoice?>">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-3">View Name</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="InvoiceViewName" name="InvoiceViewName" value="<?=$InvoiceCustomViewData['InvoiceViewData']->InvoiceViewName?>">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>