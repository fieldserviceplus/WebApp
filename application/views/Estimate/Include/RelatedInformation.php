<div class="panel panel-default card-view">
	 <div class="panel-heading">
		<div class="pull-left">
			<h6 class="panel-title txt-dark">Related Information</h6>
		</div>
		<div class="clearfix"></div>
	</div> 
	<div class="panel-wrapper collapse in">
		<div class="panel-body">

		<div class="row">

			 <div class="col-md-12">
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0 pb-10">
						<div class="txt-dark capitalize-font mt-20"><strong>Estimate Line Item</strong> </div>
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="table-wrap">
				<div class="table-responsive">
				<table id="ContactsTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>ET Line # </th>
							<th>Product</th>
							<th>Unit Price</th>
							<th>Quantity</th>
							<th>Net Total</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($EstimateLines as $key => $EstimateLineItem) { ?>
						<tr>
							<td><?=$EstimateLineItem->EstimateLineNo?></td>
							<td><a href="#"><?=$EstimateLineItem->ProductName?></a></td>
							<td><i class="fa fa-usd"> <?=number_format($EstimateLineItem->UnitPrice, 2, '.', ',');?></td>
							<td><?=$EstimateLineItem->Quantity?></td>
							<td><i class="fa fa-usd"> <?=number_format($EstimateLineItem->TotalPrice, 2, '.', ',');?></td>
						</tr>
						<?php } ?> 
					</tbody>
				</table>
				</div>
				<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<a href="#" class="btn btn-primary btn-outline mb-10 mt-15 add-new-line-item"  data-toggle="modal" data-target="#create-line-items-modal" ><i class="fa fa-plus"></i> Add Line Item</a>
					 	<button type="button" class="btn btn-primary btn-outline mb-10 mt-15 edit-line-items" data-toggle="modal" data-target="#create-line-items-modal"> Edit Lines</button> 
				</div>
				<?php if(count($EstimateLines) >= 10){?>
					<div class="pull-right">
						<a href="<?=SITE_URL?>Estimate/Actions/RelatedObject/<?=$EstimateData->EstimateID;?>/WOLineItems" type="button" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
					</div>
				<?php } ?>
				</div>
			</div>  

			<!-- <div class="col-md-12">
				<div class="clearfix"></div>
				
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
						<div class="txt-dark capitalize-font mt-20"><strong>Invoices</strong></div>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<button type="button" class="btn btn-primary btn-outline pull-right mb-10 mt-15"><i class="fa fa-plus"></i> New Invoice</button>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="table-wrap">
				<div class="table-responsive">
				<table id="InvoicesTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>INV # </th>
							<th>Invoice Date</th>
							<th>Due Date</th>
							<th>Status</th>
							<th>Sub Total</th>
							<th>Total Price</th>
						</tr>
					</thead>
					<tbody>
						<?php /*foreach ($Invoices as $key => $Invoice) { ?>
							<tr>
								<td><a href="#"><?=$Invoice->InvoiceNumber?></a></td>
								<td><?=$Invoice->InvoiceDate?></td>
								<td><?=$Invoice->DueDate?></td>
								<td><?=$Invoice->InvoiceStatus?></td>
								<td><?=$Invoice->SubTotal?></td>
								<td><?=$Invoice->TotalPrice?></td>
							</tr>
						<?php }*/ ?>
					</tbody>
				</table>
				</div>
				<?php /*if(count($Invoices) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Estimate/Actions/RelatedObject/<?=$EstimateData->EstimateID;?>/Invoices" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
				<?php }*/ ?>
				</div>
			</div>  -->

			<div class="col-md-12">
			<div class="clearfix"></div>
			
			<div class="col-md-12 pl-0 pr-0">
				<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
					<div class="txt-dark capitalize-font mt-20"><strong>Tasks</strong> </div>
				</div>
				<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
					<a href="<?=SITE_URL?>Task/Actions/CreateTask/?Estimate=<?=$EstimateData->EstimateID?>" class="btn btn-primary btn-outline pull-right mb-10 mt-15"><i class="fa fa-plus"></i> New Task</a>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="table-wrap">
			<div class="table-responsive">
			<table id="EstimatesTblEAD" class="table table-hover display table-bordered" width="99%">
				<thead>
					<tr>
						<th>Subject </th>
						<th>Name </th>
						<th>Type </th>
						<th>Assigned To </th>
						<th>Date</th>
						<th>Status</th>
						<th>Priority</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($Tasks as $key => $Task) { ?>
						<tr>
							<td><a href="<?=SITE_URL?>Task/Actions/TaskDetails/<?=$Task->TaskID?>"><?=$Task->Subject?></a></td>
							<td><a href="<?=SITE_URL?>Contact/Actions/ContactDetails/<?=$Task->Who?>"><?=$Task->ContactName?></a></td>
							<td><?=$Task->TaskType?></td>
							<td><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/<?=$Task->AssignedTo?>"><?=$Task->AssignedToName?></a></td>
							<td><?=$Task->DueDate?></td>
							<td><?=$Task->TaskStatus?></td>
							<td><?=$Task->Priority?></td>
						
						</tr>
					<?php } ?>
				</tbody>
			</table>
			</div>
			<?php if(count($Tasks) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Estimate/Actions/RelatedObject/<?=$EstimateData->EstimateID;?>/Tasks" type="button" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
			<?php } ?>
			</div>
			</div>

			<div class="col-md-12">
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
						<div class="txt-dark capitalize-font mt-20"><strong>Events</strong> </div>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<a href="<?=SITE_URL?>Task/Actions/CreateTask/?Estimate=<?=$EstimateData->EstimateID?>" class="btn btn-primary btn-outline pull-right mb-10 mt-15"><i class="fa fa-plus"></i> New Event</a>
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="table-wrap">
				<div class="table-responsive">
				<table id="ContactsTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>Subject </th>
							<th>Start Date</th>
							<th>End Date</th>
							<th>Assigned To</th>
							<th>Name</th>
							<th>Created Date</th>
							<th>Created By</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($Events as $key => $Event) { ?>
						<tr>
							<td><a href="<?=SITE_URL?>Event/Actions/EventDetails/<?=$Event->EventID?>"><?=$Event->Subject?></a></td>
							<td><?=$Event->EventStartDate?></td>
							<td><?=$Event->EventEndDate?></td>
							<td><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/<?=$Event->AssignedTo?>"><?=$Event->AssignedToName?></a></td>
							<td><a href="<?=SITE_URL?>Contact/Actions/ContactDetails/<?=$Event->Who?>"><?=$Event->ContactName?></a></td>
							<td><?=$Event->CreatedDate?></td>
							<td><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/<?=$Event->CreatedBy?>"><?=$Event->CreatedByName?></a></td>
						</tr>
						<?php } ?> 
					</tbody>
				</table>
				</div>
				<?php if(count($Events) >= 10){?>
					<div class="pull-right">
						<a href="<?=SITE_URL?>Estimate/Actions/RelatedObject/<?=$EstimateData->EstimateID;?>/Events" type="button" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
					</div>
				<?php } ?>
				</div>
			</div> 

			<div class="col-md-12">
			<div class="clearfix"></div>
			<div class="col-md-12 pl-0 pr-0">
				<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
					<div class="txt-dark capitalize-font mt-20"><strong>Files</strong> </div>
				</div>
				<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
					<button type="button" class="btn btn-primary btn-outline pull-right mb-10 mt-15" data-toggle="modal" data-target="#CreateNewFile"><i class="fa fa-plus"></i> New File</button>
				</div>
			</div>
			<div class="clearfix"></div>

			<div class="table-wrap">
			<div class="table-responsive">
			<table id="LocationsTblEAD" class="table table-hover display table-bordered" width="99%">
				<thead>
					<tr>
						<th>File Name </th>
						<th>Content Type</th>
						<th>Subject</th>
						<th>Created Date</th>
						<th>Created By</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($Files as $key => $File) { ?>
						<tr>
							<td><?=$File->FileName?></td>
							<td><?=$File->ContentType?></td>
							<td><a href="<?=SITE_URL?>File/Actions/FileDetails/<?=$File->FileID?>"><?=$File->Subject?></a></td>
							<td><?=$File->CreatedDate?></td>
							<td><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/<?=$File->CreatedBy?>"><?=$File->CreatedByName?></a></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
			</div>
			<?php if(count($Files) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Estimate/Actions/RelatedObject/<?=$EstimateData->EstimateID;?>/Files" type="button" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
			<?php } ?>
			</div>
			</div> 

			<div class="col-md-12">
			<div class="clearfix"></div>
			
			<div class="col-md-12 pl-0 pr-0">
				<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
					<div class="txt-dark capitalize-font mt-20"><strong>Notes</strong> </div>
				</div>
				<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
					<button type="button" class="btn btn-primary btn-outline pull-right mb-10 mt-15" data-toggle="modal" data-target="#CreateNewNote"><i class="fa fa-plus"></i> New Note</button>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="table-wrap">
			<div class="table-responsive">
			<table id="EstimatesTblEAD" class="table table-hover display table-bordered" width="99%">
				<thead>
					<tr>
						<th>Subject</th>
						<th>Owner</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($Notes as $key => $Note) { ?>
						<tr>
							<td><a href="<?=SITE_URL?>Note/Actions/NoteDetails/<?=$Note->NoteID?>"><?=$Note->Subject?></a></td>
							<td><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/<?=$Note->Owner?>"><?=$Note->OwnerName?></a></td>
							<td><?=$Note->CreatedDate?></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
			</div>
			<?php if(count($Notes) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Estimate/Actions/RelatedObject/<?=$EstimateData->EstimateID;?>/Notes" type="button" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
			<?php } ?>
			</div>
			</div> 

			</div>
		</div>
	</div>
</div>