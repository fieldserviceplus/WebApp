<div class="panel panel-default card-view">
	<div class="panel-heading">
		<div class="pull-left">
			<h6 class="panel-title txt-dark">Scheduling</h6>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="panel-wrapper collapse in">
		<div class="panel-body">
			<div class="row">

				<div class="col-md-12 new_scheduling_line_wrap">

				<?php foreach ($Events as $key => $Event) { ?>

					<div id="Event<?=$Event->EventID?>">	
					<div class="form-wrap">
						<form action="#" class="form-horizontal schedulingfrmclss" id="SchedulingForm<?=$Event->EventID?>">
						<div class="form-body">
							<div class="form-group">
								<div class="col-md-3 text-left txt-dark">
									Assigned To
								</div>
								<div class="col-md-4" id="AssignedToDropDown<?=$Event->EventID?>">
									<?=$Event->AssignedToName;?>
								</div>
								<div class="col-md-4" id="AssignedToBtn<?=$Event->EventID?>">
									<a OnClick="EditSchAssignedTo('<?=$Event->EventID?>', '<?=$Event->AssignedTo;?>')">Change</a>
								</div>
								<div class="col-md-1 pl-10">
									<div class="dropdown" >
										<a data-toggle="dropdown"><i class="fa fa-toggle-down"></i></a>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu" style="right:0;left: unset;">
											<li>
												<a>View in Calendar</a>
												<a OnClick="EditScheduling('<?=$Event->EventID?>')">Edit</a>
												<a OnClick="DeleteScheduling('<?=$Event->EventID?>','<?=$EstimateData->EstimateID?>')">Delete</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="form-group pt-5">
								<div class="col-md-3 text-left txt-dark">
									Start
								</div>
								<div class="col-md-6">
									<?=$Event->EventStartDate?>
								</div>
							</div>
							<div class="form-group pt-5">
								<div class="col-md-3 text-left txt-dark">
									End
								</div>
								<div class="col-md-6">
									<?=$Event->EventEndDate?>
								</div>
							</div>
						</div>
						</form>
					</div>
					<div class="clearfix"></div>
                    <hr class="light-grey-hr">
                    </div>
				<?php if($key == 4) break; } ?> 
				
				</div>

				<div class="col-md-12 col-sm-12 mt-10">
				<?php if(count($Events) > 0){?>
					<div class="pull-left">
						<button type="button" class="btn btn-primary btn-outline mr-15 mb-10 pull-left add_new_scheduling_line"><i class="fa fa-plus"></i> Add another time</button>
					</div>
				<?php } else { ?>
					 <div class="col-md-12 text-center">
						<button type="button" class="btn btn-primary btn-outline mr-15 mb-10 pull-left add_new_scheduling_line">Schedule a Time</button>
					</div>
				<?php } ?>

					<div class="pull-right scheduling-view-more-btn">
					<?php if(count($Events) >= 5){?>
						<a href="<?=SITE_URL?>Estimate/Actions/RelatedObject/<?=$EstimateData->EstimateID;?>/Events" type="button" class="btn btn-primary btn-outline">View All</a>
					<?php } ?>	
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<div class="panel panel-default card-view">
	<div class="panel-heading">
		<div class="pull-left">
			<h6 class="panel-title txt-dark">Notes</h6>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="panel-wrapper collapse in">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12 text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-primary btn-outline" data-toggle="modal" data-target="#CreateNewNote">New Note</button>
						</div>
				</div>

				<div class="col-md-12 mt-10">
					<div class="clearfix"></div>
					<table class="table table-striped table-bordered mb-0 pt-5">
						<thead>
							<tr>
								<th>Subject</th>
								<th>Owner</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($Notes as $key => $Note) { ?>
						<tr>
							<td><a href="<?=SITE_URL?>Note/Actions/NoteDetails/<?=$Note->NoteID?>"><?=$Note->Subject?></a></td>
							<td><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/<?=$Note->Owner?>"><?=$Note->OwnerName?></a></td>
							<td><?=$Note->CreatedDate?></td>
						</tr>
						<?php } ?> 
					</tbody>
				</table>
				
				<?php if(count($Notes) >= 10){?>
					<div class="pull-right">
						<a href="<?=SITE_URL?>Estimate/Actions/RelatedObject/<?=$EstimateData->EstimateID;?>/Notes" type="button" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
					</div>
				<?php } ?>
				</div>
			
			</div>
		</div>
	</div>
</div>