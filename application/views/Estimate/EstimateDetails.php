
<?php $this->load->view('Template/HeaderView'); ?>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">


<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10 pb-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['Estimate']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['Estimate']['title']?></span>
									<span class="panel-title txt-dark"><?=$EstimateData->EstimateName;?></span>
								</div>
							</div>
						</div>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div  class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="col-lg-7 col-md-5 col-sm-4 col-xs-12">
							<!-- <div class="form-group"> -->
								<ul role="tablist" class="nav nav-pills" id="myTabs_13">
									<li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_13" href="#home_13">Detail</a></li>
									<li role="presentation" class=""><a data-toggle="tab" id="profile_tab_13" role="tab" href="#profile_13" aria-expanded="false">Related</a></li>
								</ul>
							<!-- </div> -->
						</div>

						<div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">
							<div class="btn-group pull-right">
									<div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
												<a><span><strong>Communication</strong></span></a>
											</li>
											<li> 
												<a href="<?=SITE_URL;?>Common/Actions/SendEmail/Estimate/<?=$EstimateData->EstimateID;?>"><span>Send Email</span></a>
											</li>
											<li> 
												<a href="<?=SITE_URL;?>Estimate/Actions/GenerateDocument/<?=$EstimateData->EstimateID?>/"><span>Generate Document</span></a>
											</li>
											<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
												<a><span><strong>Add New Actions</strong></span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#create-line-items-modal" class="add-new-line-item"><span>New Line Item</span></a>
											</li>
											<li>
												<a href="#" class="add_new_scheduling_line"><span>Schedule a Time</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>Task/Actions/CreateTask/?Estimate=<?=$EstimateData->EstimateID;?>"><span>New Task</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>Event/Actions/CreateEvent/?Estimate=<?=$EstimateData->EstimateID;?>"><span>New Event</span></a>
											</li>
											<li> 
												<a href="#" data-toggle="modal" data-target="#CreateNewFile"><span>New File</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#CreateNewNote"><span>New Note</span></a>
											</li>
											<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
												<a><span><strong>Estimate Actions</strong></span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>Estimate/Actions/ConvertToWorkOrder/<?=$EstimateData->EstimateID?>"><span>Convert to Work Order</span></a>
											</li>
											<li> 
												<a href="<?=SITE_URL;?>Estimate/Actions/CreateEstimate/<?=$EstimateData->EstimateID?>/"><span>Copy Estimate</span></a>
											</li>
											<li>
												<a href="#" onclick="DeleteEstimate();"><span>Delete Estimate</span></a>
											</li>
										</ul>
									</div>
								</div>
								<div class="btn-group pull-right">
									<a href="<?=SITE_URL?>Estimate/Actions/EditEstimate/<?=$EstimateData->EstimateID;?>" class="btn btn-primary btn-outline">Edit</a>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
			 	<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Estimate Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>  -->
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Estimate/Actions/EditEstimate/<?=$EstimateData->EstimateID?>" class="form-horizontal" id="EditEstimate" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>Estimate Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">
												
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Estimate No
														</div>
														<div class="col-md-8">
															<?=$EstimateData->EstimateNo;?>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Estimate Name
														</div>
														<div class="col-md-8">
															<?=$EstimateData->EstimateName;?>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Account
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Account/Actions/AccountDetails/<?=$EstimateData->Account?>"><?=$EstimateData->AccountName;?></a>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Contact
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Contact/Actions/ContactDetails/<?=$EstimateData->Contact?>"><?=$EstimateData->ContactName;?></a>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Description
														</div>
														<div class="col-md-8">
															<?=$EstimateData->Description;?>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Owner
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$EstimateData->Owner?>"><?=$EstimateData->OwnerName;?></a>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Status
														</div>
														<div class="col-md-8">
															<?=$EstimateData->Status;?>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Expiration Date
														</div>
														<div class="col-md-8">
															<?=$EstimateData->ExpirationDate;?>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Phone
														</div>
														<div class="col-md-8">
															<?=$EstimateData->Phone;?>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Email
														</div>
														<div class="col-md-8">
															<?=$EstimateData->Email;?>
														</div>
													</div>
											</div>
											</div>

											<div class="col-md-12">
											<div class="txt-dark capitalize-font pt-10"><strong>Financials</strong></div>
											<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Sub Total
														</div>
														<div class="col-md-8">
															<i class="fa fa-usd"></i> <?=number_format($EstimateFinancials['SubTotal'], 2, '.', ',');?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Discount
														</div>
														<div class="col-md-8">
															<?=number_format((double)$EstimateFinancials['Discount'], 2, '.', '');?>%
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Tax
														</div>
														<div class="col-md-8">
															<i class="fa fa-usd"></i> <?=number_format((double)$EstimateFinancials['Tax'], 2, '.', ',');?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Total Price
														</div>
														<div class="col-md-8">
															<i class="fa fa-usd"></i> <?=number_format((double)$EstimateFinancials['TotalPrice'], 2, '.', ',');?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Grand Total
														</div>
														<div class="col-md-8">
															<i class="fa fa-usd"></i> <?=number_format((double)$EstimateFinancials['GrandTotal'], 2, '.', ',');?>
														</div>
													</div>

												</div>
												<div class="col-md-6">

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Signature
														</div>
														<div class="col-md-8">
														<?php if(!empty($EstimateData->Signature)){?>
															 <img style="width: 349px;height: 102px;" src="<?=$EstimateData->Signature;?>" />
														<?php } ?>
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Line Item Cnt
														</div>
														<div class="col-md-8">
															<?=count($EstimateLines);?>
														</div>
													</div>
												</div>
											</div>

											<div class="col-md-12">
											<div class="txt-dark capitalize-font pt-10"><strong>Address Information</strong></div>
											<hr class="light-grey-hr">
												<div class="col-md-6">	
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Billing Name
														</div>
														<div class="col-md-8">
															<?=$EstimateData->BillingName;?>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Address
														</div>
														<div class="col-md-8">
															<div><?=$EstimateData->BillingAddress;?></div>
															<div><?=$EstimateData->BillingCity;?>, <?=$EstimateData->BillingState;?> <?=$EstimateData->BillingPostalCode;?></div>
															<div><?=$EstimateData->BillingCountry;?></div>
														</div>
													</div>
												</div>
												<!--/span-->
												<div class="col-md-6">	
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Shipping Name
														</div>
														<div class="col-md-8">
															<?=$EstimateData->ShippingName;?>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-4 text-left txt-dark">
															Address
														</div>
														<div class="col-md-8">
															<div><?=$EstimateData->ShippingAddress;?></div>
															<div><?=$EstimateData->ShippingCity;?>, <?=$EstimateData->ShippingState;?> <?=$EstimateData->ShippingPostalCode;?></div>
															<div><?=$EstimateData->ShippingCountry;?></div>
														</div>
													</div>
												</div>
											</div>

											<!-- Start Custom Fields Row -->
											<?php $this->load->view('Setup/Customize/Fields/CustomFieldsView', array('Object' => 'Estimate', 'ObjectID' => $EstimateData->EstimateID)); ?>
											<!-- End Custom Fields Row -->

											<div class="col-md-12">

												<div class="txt-dark capitalize-font pt-10"><strong>System Information</strong></div>
												<hr class="light-grey-hr">
												
												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Create Date
														</div>
														<div class="col-md-8">
															<?=$EstimateData->CreatedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Created By
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$EstimateData->CreatedBy?>"><?=$EstimateData->CreatedByName;?></a>
														</div>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified Date
														</div>
														<div class="col-md-8">
															<?=$EstimateData->LastModifiedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified By
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$EstimateData->LastModifiedBy?>"><?=$EstimateData->LastModifiedByName;?></a>
														</div>
													</div>
												</div>
											</div>

										</div>
											<!-- /Row -->
									</div>

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			</div>
			<div id="profile_13" class="tab-pane fade" role="tabpanel">

				<?php $this->load->view('Estimate/Include/RelatedInformation'); ?>
				
			</div>
		</div>
		</div>
 
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">

			<?php $this->load->view('Estimate/Include/SchedulingAndNotes'); ?>
			
		</div>

	</div>
	<!-- /Row -->
</div>

<!-- modal -->
 <div class="modal fade" id="location-picker-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Select Location</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
				<div id="Billingpopupmap" style="height: 400px;"></div><br />
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-2">Location</label>
								<div class="col-md-10">
									<input id="BillingLocation" type="text" name="" class="form-control" readonly="">
								</div>
							</div>
						</div>
					</div>
				</div>
				</form>
				
			</div>
		</div>
	</div>
</div> 

 <div class="modal fade" id="location-picker-modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Select Location</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
				<div id="Shippingpopupmap" style="height: 400px;"></div><br />
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-2">Location</label>
								<div class="col-md-10">
									<input id="ShippingLocation" type="text" name="" class="form-control" readonly="">
								</div>
							</div>
						</div>
					</div>
				</div>
				</form>
				
			</div>
		</div>
	</div>
</div> 

<!-- Start Modal -->

<?php //$this->load->view('Estimate/Modal/CreateNoteView'); ?>
<?php $this->load->view('Estimate/Modal/CreateLineItemsView'); ?>
<?php $data = array('data' => array('RelatedTo' => 'Estimate', 'What' => $EstimateData->EstimateID, 'AssignedTo' => $EstimateData->Owner));
$this->load->view('Common/Modal/CreateNewFile', $data);
$this->load->view('Common/Modal/CreateNewNote', $data); ?>

<?php $this->load->view('Template/FooterView')?>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Use for common object  -->
<script src="<?=SITE_URL?>dist/js/common.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	$(".select2").select2();
	$('.selectpicker').selectpicker();

});
</script>

<script type="text/javascript">

$(document).ready(function () {

    $('.showdatetimepicker').datetimepicker({
		useCurrent: true,
		icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
	});

   /* $('.timepicker').datetimepicker({
			format: 'LT',
			useCurrent: false,
			icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
	}).data("DateTimePicker").date(moment());*/

    /*$('#CreateNoteForm').validate({ 
        rules: {
            Subject: {
                required: true
            },
            Body: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });*/

}); 
function DeleteEstimate(EstimateID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#5fc55f",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>Estimate/Actions/DeleteEstimate/<?=$EstimateData->EstimateID;?>/";
		}, 500);
    });
	return false;
}
</script>

<script type="text/javascript">
/* Line Items Start */ 
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".line_item_fields_wrap"); //Fields wrapper
   
    var x = <?=count($EstimateLines)-1;?>; //initlal text box count
    $(".add_line_item_field_button").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="col-md-12 pt-10 product-line-items">'
					+	'<div class="col-md-4">'
					+	'<select name="Product[]" id="Product" class="selectpicker" data-style="form-control btn-default btn-outline" data-show-subtext="true" data-live-search="true" OnChange="GetProductDetails(this.value, '+x+');">'
					+		'<option value="">Search Product</option>'
							<?php $Products = $this->ActionsModel->GetProducts();
								foreach ($Products as $Product) {  ?>
					+				'<option value="<?=$Product->ProductID?>" data-subtext="<?=$Product->ProductCode?> - $<?=number_format($Product->ListPrice, 2, '.', ',')?>"><?=$Product->ProductName?></option>'
							<?php } ?>
					+		'</select>'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="ListPrice'+x+'" name="ListPrice[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="Discount'+x+'" name="Discount[]" class="form-control" type="text" value="0" OnChange="CalculatePriceTotal('+x+')">'
					
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="UnitPrice'+x+'" name="UnitPrice[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+		'<input type="text" name="Quantity[]" id="Quantity'+x+'" class="form-control" value="1" OnChange="CalculatePriceTotal('+x+')">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="SubTotal'+x+'" name="SubTotal[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+	'</div>'
					+	'<div class="col-md-1 text-center pt-10">'
					+		'<div class="checkbox checkbox-primary pt-0">'
					+			'<input id="Taxable'+x+'" name="Taxable[]" type="checkbox" value="1" OnChange="CalculatePriceTotal('+x+')">'
					+			'<label for="checkbox2"></label>'
					+		'</div>'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="TotalPrice'+x+'" name="TotalPrice[]" class="form-control TotalPrice" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+	'</div>'
					+	'<div class="col-md-1 text-center">'
					+		'<a href="#" class="btn btn-default remove_line_item_field blank-new-field"><i class="fa fa-trash-o"></i></a>'
					+	'</div>'
					+'</div>'); //add input box
		}
		//$(".select2").select2();
		$('.selectpicker').selectpicker();
    });
   
    $(document).on("click",".remove_line_item_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.product-line-items').remove(); x--;

        CalculateGrandTotal();
    });

    CalculateGrandTotal();
});

$(document).on("click",".add-new-line-item", function(e){ //user click on remove text
   $(".add_line_item_field_button").click();
});

$(document).on("click",".edit-line-items", function(e){ //user click on remove text
   $(".blank-new-field.remove_line_item_field").click();
});

function GetProductDetails(ProductID, x)
{ 
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/GetProductDetails/'+ProductID,
        method: "post",
       /* data: { ProductID : ProductID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$('#ListPrice'+x).val(result.ListPrice);
            	if(result.Taxable){ 
            		$( "#Taxable"+x ).prop( "checked", true);
            	}
         
            	if(result.IsListPriceEditable == 0){ 
            		$( "#ListPrice"+x ).prop( "readonly", true);
            	} else {
            		$( "#ListPrice"+x ).removeAttr( "readonly");
            	}
            	CalculatePriceTotal(x);
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });

}

function CalculatePriceTotal(x)
{ 
	var ListPrice = parseFloat($('#ListPrice'+x).val().replace(',', ''));
	var Discount = parseFloat($('#Discount'+x).val().replace(',', ''));
	var Quantity = parseFloat($('#Quantity'+x).val().replace(',', ''));

	var UnitPrice = ListPrice - (ListPrice * Discount) / 100;
	var SubTotal = UnitPrice * Quantity;
	if($('#Taxable'+x).is(':checked'))
		var TotalPrice = SubTotal + (SubTotal * <?=$EstimateData->Tax;?>) / 100;
	else 
		var TotalPrice = SubTotal;
	 
	$('#ListPrice'+x).val(NumberFormat(ListPrice));
	$('#UnitPrice'+x).val(NumberFormat(UnitPrice));
	$('#SubTotal'+x).val(NumberFormat(SubTotal));
	$('#TotalPrice'+x).val(NumberFormat(TotalPrice));

	CalculateGrandTotal();
}

function CalculateGrandTotal()
{ 
	var GrandTotal = 0;

    $(".TotalPrice").each(function(){

    	GrandTotal = parseFloat(GrandTotal) + parseFloat($(this).val().replace(',', ''));
    });
	$('#GrandTotal').val(NumberFormat(GrandTotal));
}

function NumberFormat(n) {
  return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}

/* Line Items End */ 
</script>

<script type="text/javascript">
/* Scheduling Start */ 
$(document).ready(function() {

    var max_fields      = 20; //maximum input boxes allowed
    var wrapper         = $(".new_scheduling_line_wrap"); //Fields wrapper
   
    var x = 1;//<?=count($Events);?>; //initlal text box count
    $(".add_new_scheduling_line").click(function(e){ //on add input button click

        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div id="SchedulingLine'+x+'"><div class="form-wrap">'
					+	'<form action="#" class="form-horizontal schedulingfrmclss" id="SchedulingForm'+x+'">'
					+   '<input type="hidden" name="EstimateName" value="<?=$EstimateData->EstimateName;?>">'
					+   '<input type="hidden" name="EstimateNo" value="<?=$EstimateData->EstimateNo;?>">'
					+   '<input type="hidden" name="EstimateID" value="<?=$EstimateData->EstimateID;?>">'
					+   '<input type="hidden" name="Who" value="<?=$EstimateData->Contact;?>">'
					+   '<input type="hidden" name="Description" value="<?=$EstimateData->Description;?>">'
					+	'<div class="form-body">'
					+		'<div class="form-group">'
					+			'<label class="col-md-3 control-label text-left">Assigned To</label>'
					+			'<div class="col-md-6">'
					+				'<select name="AssignedTo" id="AssignedTo" class="form-control select2">'
					+				'<option value="">Select Owner</option>'
									<?php $Users = $this->ActionsModel->GetUsers();
										foreach ($Users as $User) { 
											if($EstimateData->Owner == $User->UserID){?>
					+							'<option value="<?=$User->UserID?>" selected=""><?=$User->FirstName.' '.$User->LastName?></option>'
										<?php } else {?>
					+							'<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>'
										<?php }
										} ?>
					+				'</select>'
					+			'</div>'
					+		'</div>'
					+		'<div class="form-group">'
					+			'<label class="col-md-3 control-label text-left">Start</label>'
					+			'<div class="col-md-6">'
					+				'<input type="text" class="form-control showdatetimepicker" name="EventStartDate" value="<?=date("m/d/Y")?>" required>'
					+			'</div>'
					+		'</div>'
					+		'<div class="form-group">'
					+			'<label class="col-md-3 control-label text-left">End</label>'
					+			'<div class="col-md-6">'
					+				'<input type="text" class="form-control showdatetimepicker" name="EventEndDate" value="<?=date("m/d/Y")?>" required>'
					+			'</div>'
					+		'</div>'
					+	'</div>'
					+	'<div class="pull-right">'
					+		'<button type="button" class="btn btn-default remove_new_scheduling_line" data-dismiss="modal">Close</button> '
					+		'<button type="button" class="btn btn-primary" OnClick="SaveScheduling('+x+')">Save</button>'
					+	'</div>'
					+	'</form>'
					+ '<div class="clearfix"></div>'
					+ '<hr class="light-grey-hr">'
					+ '</div>'
					+ '</div>'); //add input box
		}
		$(".select2").select2();
		$('.showdatetimepicker').datetimepicker({
			useCurrent: true,
			icons: {
	                time: "fa fa-clock-o",
	                date: "fa fa-calendar",
	                up: "fa fa-arrow-up",
	                down: "fa fa-arrow-down"
	            }
		});

		SetViewMoreBtnInScheduling();
    });
    $(wrapper).on("click",".remove_new_scheduling_line", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.form-wrap').remove(); x--;

        SetViewMoreBtnInScheduling();
        
    })
});

function SetViewMoreBtnInScheduling(){
	if($(".schedulingfrmclss").length > 5){
		$(".scheduling-view-more-btn").html('<div class="pull-right">'
			+ '<a href="<?=SITE_URL?>Estimate/Actions/RelatedObject/<?=$EstimateData->EstimateID;?>/Events" type="button" class="btn btn-primary btn-outline">View All</a>'
			+ '</div>');
	} else {
		$(".scheduling-view-more-btn").html('');
	}
}

function SaveScheduling(x)
{
 	 $.ajax({
            url: '<?=SITE_URL?>Estimate/Actions/SaveScheduling/',
            data: $("#SchedulingForm"+x).serialize(),
            method: "post",
            dataType: 'html',
            success: function (result) {
                if (result != '') {
                	$(".new_scheduling_line_wrap").html(result);

                if($(".schedulingfrmclss").length > 0)
            		$("button.add_new_scheduling_line").html('<i class="fa fa-plus"></i> Add another time');

                  return true;  	
                } else {
                    alert("Something went wrong.Please try again!");
                    return false;
                }
            }
        });
}

function EditScheduling(EventID)
{
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/GetScheduling/'+EventID,
        method: "post",
        dataType: 'html',
        success: function (result) {
            if (result != '') {

            	$("#Event"+EventID).html(result); //add input box
            	$(".select2").select2();
				$('.showdatetimepicker').datetimepicker({
					useCurrent: true,
					icons: {
			                time: "fa fa-clock-o",
			                date: "fa fa-calendar",
			                up: "fa fa-arrow-up",
			                down: "fa fa-arrow-down"
			            }
				});
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}	

function UpdateScheduling(EventID)
{
 	 $.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/UpdateScheduling/'+EventID,
        data: $("#SchedulingForm"+EventID).serialize(),
        method: "post",
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$(".new_scheduling_line_wrap").html(result);
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}

function DeleteScheduling(EventID, EstimateID)
{ 
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/DeleteScheduling/'+EventID,
        method: "post",
        data: { EventID : EventID, EstimateID : EstimateID},
        dataType: 'html',
        success: function (result) {
            
        	$(".new_scheduling_line_wrap").html(result);

        	if($(".schedulingfrmclss").length == 0)
        		$("button.add_new_scheduling_line").html('Schedule a Time');

        	SetViewMoreBtnInScheduling();

           	return true;  	
        }
    });
}

function EditSchAssignedTo(EventID, AssignedTo)
{ 
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/EditSchAssignedTo/',
        method: "post",
        data: { EventID : EventID, AssignedTo : AssignedTo},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$("#AssignedToDropDown"+EventID).html(result);
            	$("#AssignedToBtn"+EventID).html('<a onclick="UpdateAssignedTo('+EventID+')">Save</a>');
            	$(".select2").select2();
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}

function UpdateAssignedTo(EventID)
{ 
	var AssignedTo = $("#AssignedToVal"+EventID).val();
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/UpdateAssignedTo/',
        method: "post",
        data: { EventID : EventID, AssignedTo : AssignedTo},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$("#AssignedToDropDown"+EventID).html(result);
            	$("#AssignedToBtn"+EventID).html('<a onclick="EditSchAssignedTo('+EventID+', '+AssignedTo+')">Change</a>');
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}
/* Scheduling End */ 
</script>


