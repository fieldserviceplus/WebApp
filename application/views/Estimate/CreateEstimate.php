
<?php $this->load->view('Template/HeaderView')?>
<!-- Bootstrap Datetimepicker CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_JS_KEY?>&libraries=places"></script>
 
 <!--Signature Pad CSS -->
<!-- <link href="<?=SITE_URL?>dist/css/signature_pad.css" rel="stylesheet" type="text/css"> -->


<div class="container-fluid pt-25">
	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Create Estimate</h6>
					</div>
					<div class="pull-right">
						<span style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong></span>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Estimate/Actions/CreateEstimate" class="form-horizontal" id="CreateEstimate" method="post">

									<?php if(isset($_GET['Account']) && !empty($_GET['Account'])){ ?>
											<input type="hidden" name="Redirect" value="Account">
									<?php } ?>
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>Estimate Details</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Estimate Name<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="EstimateName" id="EstimateName" value="<?=isset($EstimateData->EstimateName)?$EstimateData->EstimateName:"";?>">
															<?php echo form_error('EstimateName');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Account<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="Account" id="Account" class="form-control select2" OnChange="GetPrimaryContactOptions(this.value);GetAccountDetails(this.value);">
														<option value="">Select Account</option>
														<?php $Accounts = $this->ActionsModel->GetAccounts();
															foreach ($Accounts as $Account) { 
																if((isset($EstimateData->Account) && $EstimateData->Account == $Account->AccountID) || (isset($_GET['Account']) && $_GET['Account'] == $Account->AccountID)){ ?>
																	<option value="<?=$Account->AccountID?>" selected="selected"><?=$Account->AccountName;?></option>
																<?php } else { ?>
																	<option value="<?=$Account->AccountID?>"><?=$Account->AccountName;?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('Account');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Contact</label>
														<div class="col-md-9">
														<?php $Contacts = $this->ActionsModel->GetContacts(); ?>
															<select class="form-control select2 SelectPrimaryContact" name="Contact" id="Contact">
															<option value="">Select Contact</option>
															<?php echo form_error('Contact');  ?>
														</select>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Description<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Description" id="Description" value="<?=isset($EstimateData->Description)?$EstimateData->Description:"";?>">
														</div>
													</div>

												</div>


												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Owner<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="Owner" id="Owner" class="form-control select2">
														<option value="">Select Owner</option>
														<?php $Users = $this->ActionsModel->GetUsers(); 
															foreach ($Users as $User) { 
															if(isset($EstimateData->Owner) && $EstimateData->Owner == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else { ?>
																<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('Owner');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Status<span class="required-sign">*</span></label>
														<div class="col-md-9">

														<?php $GetEstimateStatus = $this->ActionsModel->GetEstimateStatus(); ?>
															<select class="form-control" name="EstimateStatus" id="EstimateStatus">
															<option value="">Select Status</option>
															<?php foreach ($GetEstimateStatus as $EstimateStatus) { 
																if(isset($EstimateData->EstimateStatus) && $EstimateData->EstimateStatus == $EstimateStatus->EstimateStatusID){ ?>
																	<option value="<?=$EstimateStatus->EstimateStatusID?>" selected="selected"><?=$EstimateStatus->Status?></option>
																<?php } else { ?>
																	<option value="<?=$EstimateStatus->EstimateStatusID?>"><?=$EstimateStatus->Status?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('WOStatus');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Expiration Date<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control showdatepicker" name="ExpirationDate" id="ExpirationDate" value="<?=isset($EstimateData->ExpirationDate)?$EstimateData->ExpirationDate:date('m/d/Y',strtotime('+30 days',strtotime(date("m/d/Y"))));?>">
															<?php echo form_error('ExpirationDate');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Phone</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Phone" id="Phone" value="<?=isset($EstimateData->Phone)?$EstimateData->Phone:"";?>">
															<?php echo form_error('Phone');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Email</label>
														<div class="col-md-9">
															<input type="email" class="form-control" name="Email" id="Email" value="<?=isset($EstimateData->Email)?$EstimateData->Email:"";?>">
															<?php echo form_error('Email');  ?>
														</div>
													</div>
												</div>
											</div>

											<div class="col-md-12">
												<div class="txt-dark capitalize-font"><strong>Address Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Billing Name<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="BillingName" id="BillingName" value="<?=isset($EstimateData->BillingName)?$EstimateData->BillingName:"";?>">
															<?php echo form_error('BillingName');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Billing Address</label>
														<div class="col-md-9">
															<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-search"></i>Search Address</button>
														</div>
													</div>
												
													<div class="form-group">
														<label class="col-sm-3 control-label">Billing Street<span class="required-sign">*</span></label>
														<div class="col-sm-9">
																<input type="text" class="form-control" name="BillingAddress" id="BillingAddress" placeholder="" data-toggle="modal" data-target="#location-picker-modal" value="<?=isset($EstimateData->BillingAddress)?$EstimateData->BillingAddress:"";?>">
															<?php echo form_error('BillingAddress');  ?>
															<input type="hidden" name="BillingLatitude" id="BillingLatitude" value="<?=isset($EstimateData->BillingLatitude)?$EstimateData->BillingLatitude:"";?>">
															<input type="hidden" name="BillingLongitude" id="BillingLongitude" value="<?=isset($EstimateData->BillingLongitude)?$EstimateData->BillingLongitude:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Billing City<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="BillingCity" id="BillingCity" value="<?=isset($EstimateData->BillingCity)?$EstimateData->BillingCity:"";?>">
															<?php echo form_error('BillingCity');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Billing State<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="BillingState" id="BillingState" value="<?=isset($EstimateData->BillingState)?$EstimateData->BillingState:"";?>">
															<?php echo form_error('BillingState');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-3">Billing Country<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="BillingCountry" id="BillingCountry" value="<?=isset($EstimateData->BillingCountry)?$EstimateData->BillingCountry:"";?>">
															<?php echo form_error('BillingCountry');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Billing Postal Code<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="BillingPostalCode" id="BillingPostalCode" value="<?=isset($EstimateData->BillingPostalCode)?$EstimateData->BillingPostalCode:"";?>">
															<?php echo form_error('BillingPostalCode');  ?>
														</div>
													</div>
												</div>

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Shipping Name<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="ShippingName" id="ShippingName" value="<?=isset($EstimateData->ShippingName)?$EstimateData->ShippingName:"";?>">
															<?php echo form_error('ShippingName');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Shipping Address</label>
														<div class="col-md-9">
															<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal2"><i class="fa fa-search"></i>Search Address</button>
														</div>
													</div>
												
													<div class="form-group">
														<label class="col-sm-3 control-label">Shipping Street<span class="required-sign">*</span></label>
														<div class="col-sm-9">
															<input type="text" class="form-control" name="ShippingAddress" id="ShippingAddress" placeholder="" value="<?=isset($EstimateData->ShippingAddress)?$EstimateData->ShippingAddress:"";?>">
															<?php echo form_error('ShippingAddress');  ?>
															<input type="hidden" name="ShippingLatitude" id="ShippingLatitude" value="<?=isset($EstimateData->ShippingLatitude)?$EstimateData->ShippingLatitude:"";?>">
															<input type="hidden" name="ShippingLongitude" id="ShippingLongitude" value="<?=isset($EstimateData->ShippingLongitude)?$EstimateData->ShippingLongitude:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Shipping City<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="ShippingCity" id="ShippingCity" value="<?=isset($EstimateData->ShippingCity)?$EstimateData->ShippingCity:"";?>">
															<?php echo form_error('ShippingCity');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Shipping State<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="ShippingState" id="ShippingState" value="<?=isset($EstimateData->ShippingState)?$EstimateData->ShippingState:"";?>">
															<?php echo form_error('ShippingState');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-3">Shipping Country<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="ShippingCountry" id="ShippingCountry" value="<?=isset($EstimateData->ShippingCountry)?$EstimateData->ShippingCountry:"";?>">
															<?php echo form_error('ShippingCountry');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Shipping Postal Code<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="ShippingPostalCode" id="ShippingPostalCode" value="<?=isset($EstimateData->ShippingPostalCode)?$EstimateData->ShippingPostalCode:"";?>">
															<?php echo form_error('ShippingPostalCode');  ?>
														</div>
													</div>
													
												</div>
											
											<!-- /Row -->
										</div>

										<!-- Start Custom Fields Row -->
										<?php $this->load->view('Setup/Customize/Fields/CustomFieldsForm', array('Object' => 'Estimate', 'ObjectID' => 0)); ?>
										<!-- End Custom Fields Row -->

										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="CreateEstimate">Save</button>
															<a href="<?=SITE_URL;?>Estimate/Actions" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->
</div>

<!-- modal -->
 <div class="modal fade" id="location-picker-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Address Search</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" action="javascript:void(0)">
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-2">Location</label>
								<div class="col-md-10">
									<input id="BillingLocation" type="text" name="" class="form-control" placeholder="Enter Address" autocomplete="false">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="Billingpopupmap" style="height: 400px;"></div><br />
				</form>
				
			</div>
		</div>
	</div>
</div> 

 <div class="modal fade" id="location-picker-modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Address Search</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" action="javascript:void(0)">
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-2">Location</label>
								<div class="col-md-10">
									<input id="ShippingLocation" type="text" name="" class="form-control" placeholder="Enter Address" autocomplete="false">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="Shippingpopupmap" style="height: 400px;"></div><br />
				
				</form>
				
			</div>
		</div>
	</div>
</div> 

<?php //$this->load->view('Estimate/Modal/LocationPickerView'); ?>
<?php $this->load->view('Estimate/Modal/RecurringTaskView'); ?>

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>

<script type="text/javascript">

$(document).ready(function () {
 
    $('#CreateEstimate').validate({ 
        rules: {
            Owner: {
                required: true
            },
            Account: {
                required: true
            },
            EstimateName: {
                required: true
            },
            Description: {
                required: true
            },
            BillingName: {
                required: true
            },
            BillingAddress: {
                required: true
            },
            BillingCity: {
                required: true
            },
            BillingState: {
                required: true
            },
            BillingCountry: {
                required: true
            },
            BillingPostalCode: {
                required: true
            },
            ShippingName: {
                required: true
            },
            ShippingAddress: {
                required: true
            },
            ShippingCity: {
                required: true
            },
            ShippingState: {
                required: true
            },
            ShippingCountry: {
                required: true
            },
            ShippingPostalCode: {
                required: true
            },
            EstimateStatus: {
                required: true
            },
            ExpirationDate: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
        submitHandler: function (form) { 
        
        	return true;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    /*$('.showdatetimepicker').datetimepicker({
			useCurrent: true,
			icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
		});

	$('.timepicker').datetimepicker({
			format: 'LT',
			useCurrent: false,
			icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
		}).data("DateTimePicker").date(moment());*/

});
</script>

<script type="text/javascript">
function GetPrimaryContactOptions(AccountID, PrimaryContact = 0){
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/GetPrimaryContactOptions/',
        method: "post",
        data: { AccountID : AccountID, PrimaryContact : PrimaryContact},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$(".SelectPrimaryContact").html(result);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}

function GetAccountDetails(AccountID){
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/GetAccountDetails/'+AccountID,
        method: "post",
      /*  data: { AccountID : AccountID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$("#BillingName").val(result.AccountName);
            	$("#ShippingName").val(result.AccountName);
            	$("#BillingAddress").val(result.BillingAddress);
            	$("#BillingCity").val(result.BillingCity);
            	$("#BillingState").val(result.BillingState);
            	$("#BillingCountry").val(result.BillingCountry);
            	$("#BillingPostalCode").val(result.BillingPostalCode);
            	$("#BillingLatitude").val(result.BillingLatitude);
            	$("#BillingLongitude").val(result.BillingLongitude);
            	$("#ShippingAddress").val(result.ShippingAddress);
            	$("#ShippingCity").val(result.ShippingCity);
            	$("#ShippingState").val(result.ShippingState);
            	$("#ShippingCountry").val(result.ShippingCountry);
            	$("#ShippingPostalCode").val(result.ShippingPostalCode);
            	$("#ShippingLatitude").val(result.ShippingLatitude);
            	$("#ShippingLongitude").val(result.ShippingLongitude);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}

$(document).ready(function () {
<?php if(isset($_GET['Account']) && !empty($_GET['Account'])){ ?>
		GetAccountDetails(<?=$_GET['Account']?>);
		GetPrimaryContactOptions(<?=$_GET['Account']?>);
<?php } 
	if(isset($_GET['Contact']) && $_GET['Contact']){ ?>
		GetPrimaryContactOptions(<?=$_GET['Account']?>, <?=$_GET['Contact']?>);
		GetContactDetails(<?=$_GET['Contact']?>);
<?php } ?>
 });

function GetContactDetails(ContactID){
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/GetContactDetails/'+ContactID,
        method: "post",
      /*  data: { AccountID : AccountID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$("#Phone").val(result.PhoneNo);
            	$("#Email").val(result.Email);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}
</script>

<!-- Set Location -->
<script src="<?=SITE_URL?>dist/js/location-picker.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	
	set_location_in_map("Billing");

	set_location_in_map("Shipping");
});
</script>