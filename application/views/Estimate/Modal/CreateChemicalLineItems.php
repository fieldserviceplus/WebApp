
<!-- modal --> 
<div class="modal fade" id="create-chemical-line-items-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog modal-lg" role="document" style="width: 90%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Edit Chemicals</h5>
			</div>
			<form class="form-horizontal" id="CreateWOChemicalLineItem" method="post" action="<?=SITE_URL;?>WorkOrder/Actions/CreateWOChemicalLineItems/<?=$WorkOrderData->WorkOrderID?>">
			<input type="hidden" name="Account" value="<?=$WorkOrderData->Account;?>">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-1"></div>
						<div class="col-md-11">
						<div class="col-md-3 text-center">
							<strong>Chemical</strong>
						</div>
						<div class="col-md-4 text-center">
							<strong>Tested Concentration</strong>
						</div>
						<div class="col-md-4 text-center">
							<strong>Application</strong>
						</div>
						<div class="col-md-1 text-center">
							<strong>Delete</strong>
						</div>
						</div>
					</div>

					<?php foreach ($Chemicals as $key => $WOChemicalLineItem) { ?>
						<div class="col-md-12 chemical-line-items">
							<div class="panel-group accordion-struct accordion-style-1" id="accordion_2" role="tablist" aria-multiselectable="true">
								 <div class="panel panel-default pt-10">
								 	<div class="panel-heading text-center col-md-1" role="tab" id="heading_10" style="border-bottom: none;">
										<a role="button" class="btn btn-default btn-outline" data-toggle="collapse" data-parent="#accordion_2" href="#collapse_<?=$key?>" aria-expanded="true" ><div class="icon-ac-wrap"><span class="plus-ac"><i class="ti-plus"></i></span><span class="minus-ac"><i class="ti-minus"></i></span></div></a> 
									</div> 
									<div class="col-md-11">
										<div class="col-md-3">
										<select name="Product[]" id="Product<?=$key?>" class="form-control select2" required>
											<option value="">Select Chemical</option>
											<?php $ChemicalProducts = $this->ActionsModel->GetChemicals();
												foreach ($ChemicalProducts as $Chemical) { 
												if($WOChemicalLineItem->Product == $Chemical->ProductID) {?>
													<option value="<?=$Chemical->ProductID?>" selected=""><?=$Chemical->ProductName?></option>
												<?php } else {?>
													<option value="<?=$Chemical->ProductID?>"><?=$Chemical->ProductName?></option>
												<?php } ?>
											<?php } ?>
											</select>
										</div>
										<div class="col-md-4">
											<div class="col-md-1"></div>
											<div class="col-md-4"> 
												<input id="TestConcentration<?=$key?>" name="TestConcentration[]" class="form-control" type="text"  value="<?=$WOChemicalLineItem->TestConcentration?>" required>
											</div>
											<div class="col-md-6"> 
												<select name="TestedUnitOfMeasure[]" id="TestedUnitOfMeasure<?=$key?>" class="form-control" required>
													<option value="">Select</option>
													<?php $UnitOfMeasurements = $this->ActionsModel->GetUnitOfMeasurement();
														foreach ($UnitOfMeasurements as $UnitOfMeasurement) { 
														if($UnitOfMeasurement->UnitOfMeasurementID == $WOChemicalLineItem->TestedUnitOfMeasure) {?>
															<option value="<?=$UnitOfMeasurement->UnitOfMeasurementID?>" selected=""><?=$UnitOfMeasurement->UnitOfMeasurement?></option>
														<?php } else {?>
															<option value="<?=$UnitOfMeasurement->UnitOfMeasurementID?>"><?=$UnitOfMeasurement->UnitOfMeasurement?></option>
														<?php } ?>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="col-md-1"></div>
											<div class="col-md-4"> 
												<input id="Application<?=$key?>" name="ApplicationAmount[]" class="form-control" type="text" value="<?=$WOChemicalLineItem->ApplicationAmount?>" required>
											</div>
											<div class="col-md-6"> 
												<select name="ApplicationUnitOfMeasure[]" id="ApplicationUnitOfMeasure<?=$key?>" class="form-control" required>
													<option value="">Select</option>
													<?php $UnitOfMeasurements = $this->ActionsModel->GetUnitOfMeasurement();
														foreach ($UnitOfMeasurements as $UnitOfMeasurement) { 
														if($UnitOfMeasurement->UnitOfMeasurementID == $WOChemicalLineItem->ApplicationUnitOfMeasure) {?>
															<option value="<?=$UnitOfMeasurement->UnitOfMeasurementID?>" selected=""><?=$UnitOfMeasurement->UnitOfMeasurement?></option>
														<?php } else {?>
															<option value="<?=$UnitOfMeasurement->UnitOfMeasurementID?>"><?=$UnitOfMeasurement->UnitOfMeasurement?></option>
														<?php } ?>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="col-md-1 text-center">
											<a href="#" class="btn btn-default remove_chemical_line_item_field "><i class="fa fa-trash-o"></i></a>
										</div>
									</div>
									<div id="collapse_<?=$key?>" class="panel-collapse collapse col-md-12 pt-10" role="tabpanel">
										<div class="col-md-2"> 
											<strong>Application Area</strong>
										</div>
										<div class="col-md-4"> 
											<textarea class="form-control" name="ApplicationArea[]"><?=$WOChemicalLineItem->ApplicationArea?></textarea>
										</div>
										<div class="col-md-2"> 
											<strong>Additional Notes</strong>
										</div>
										<div class="col-md-4"> 
											<textarea class="form-control" name="AdditionalNotes[]"><?=$WOChemicalLineItem->AdditionalNotes?></textarea>
										</div>
									</div>
									</div>
								</div> 
								<div class="clearfix"></div><hr class="light-grey-hr">
							</div>
					<?php } ?> 

					<div class="chemical_line_item_fields_wrap">
						
					</div>
					<div class="col-md-12">
						<a href="#" class="btn btn-primary btn-outline mb-10 mt-15 add_chemical_line_item_field_button"><i class="fa fa-plus"></i> Add Chemical</a>
					</div>
					
				</div>
			</div>
			<div class="modal-footer" style="text-align: center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
			</form>
		</div>
	</div>
</div> 

