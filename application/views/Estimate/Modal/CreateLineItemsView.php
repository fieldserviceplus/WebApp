
<!-- modal --> 
<div class="modal fade" id="create-line-items-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog modal-lg" role="document" style="width: 100%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Edit Line Items</h5>
			</div>
			<form class="form-horizontal" id="EstimateLine" method="post" action="<?=SITE_URL;?>Estimate/Actions/EstimateLine/<?=$EstimateData->EstimateID?>">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<strong>Product</strong>
						</div>
						<div class="col-md-1 text-center">
							<strong>List Price ($)</strong>
						</div>
						<div class="col-md-1 text-center">
							<strong>Discount (%)</strong>
						</div>
						<div class="col-md-1 text-center">
							<strong>Unit Price ($)</strong>
						</div>
						<div class="col-md-1 text-center">
							<strong>Quantity</strong>
						</div>
						<div class="col-md-1 text-center">
							<strong>Subtotal ($)</strong>
						</div>
						<div class="col-md-1 text-center">
							<strong>Taxable</strong>
						</div>
						<div class="col-md-1 text-center">
							<strong>Net Total ($)</strong>
						</div>
						<div class="col-md-1 text-center">
							<strong>Delete</strong>
						</div>
					</div>

					<?php foreach ($EstimateLines as $key => $EstimateLine) { ?>
						<div class="col-md-12 pt-10 product-line-items">
						<div class="col-md-4">
						<select name="Product[]" id="Product<?=$key?>" class="selectpicker" data-style="form-control btn-default btn-outline"  data-show-subtext="true" data-live-search="true" OnChange="GetProductDetails(this.value, <?=$key?>);">
							<option value="">Select Product</option>
							<?php $Products = $this->ActionsModel->GetProducts();
								$IsListPriceEditable = 0;
								foreach ($Products as $Product) { 
								if($EstimateLine->Product == $Product->ProductID) {?>
									<option value="<?=$Product->ProductID?>" selected="" data-subtext="<?=$Product->ProductCode?> - $<?=number_format($Product->ListPrice, 2, '.', ',')?>"><?=$Product->ProductName?></option>
								<?php $IsListPriceEditable = $Product->IsListPriceEditable;
								} else {?>
									<option value="<?=$Product->ProductID?>" data-subtext="<?=$Product->ProductCode?> - $<?=number_format($Product->ListPrice, 2, '.', ',')?>"><?=$Product->ProductName?></option>
								<?php 
								} ?>
							<?php } ?>
							</select>
						</div>
						<div class="col-md-1">
							<input id="ListPrice<?=$key?>" name="ListPrice[]" class="form-control" type="text" value="<?=number_format($EstimateLine->ListPrice, 2, '.', ',')?>" OnChange="CalculatePriceTotal(<?=$key?>);" <?=($IsListPriceEditable==0?"readonly":"");?> >
						</div>
						<div class="col-md-1">
							<input id="Discount<?=$key?>" name="Discount[]" class="form-control" type="text" value="<?=number_format($EstimateLine->Discount, 2, '.', ',')?>" OnChange="CalculatePriceTotal(<?=$key?>);"> 
						</div>
						<div class="col-md-1">
							<input id="UnitPrice<?=$key?>" name="UnitPrice[]" class="form-control" type="text" value="<?=number_format($EstimateLine->UnitPrice, 2, '.', ',')?>" OnChange="CalculatePriceTotal(<?=$key?>);" readonly="">
						</div>
						<div class="col-md-1">
							<input type="text" name="Quantity[]" id="Quantity<?=$key?>" class="form-control" value="<?=$EstimateLine->Quantity?>" OnChange="CalculatePriceTotal(<?=$key?>);">
						</div>
						<div class="col-md-1">
							<input id="SubTotal<?=$key?>" name="SubTotal[]" class="form-control" type="text" value="<?=number_format($EstimateLine->SubTotal, 2, '.', ',')?>" OnChange="CalculatePriceTotal(<?=$key?>);" readonly="">
						</div>
						<div class="col-md-1 text-center pt-10">
							<div class="checkbox checkbox-primary pt-0">
								<input id="Taxable<?=$key?>" name="Taxable[]" type="checkbox" value="1" <?=$EstimateLine->Taxable==1?"checked":"";?> OnChange="CalculatePriceTotal(<?=$key?>);">
								<label for="checkbox2"></label>
							</div>
						</div>
						<div class="col-md-1">
							<input id="TotalPrice<?=$key?>" name="TotalPrice[]" class="form-control TotalPrice" type="text" value="<?=number_format($EstimateLine->TotalPrice, 2, '.', ',')?>" OnChange="CalculatePriceTotal(<?=$key?>);" readonly="">
						</div>
						<div class="col-md-1 text-center">
							<a href="#" class="btn btn-default remove_line_item_field"><i class="fa fa-trash-o"></i></a>
						</div>
					</div>
					<?php } ?> 

					<div class="line_item_fields_wrap">
						
					</div>
					<div class="col-md-12 pt-10">
						<div class="col-md-10"><strong class="pull-right"><label class="control-label"> Grand Total</label></strong></div>
						<div class="col-md-1">
							<input id="GrandTotal" name="GrandTotal" class="form-control" type="text">
						</div>
					</div>
					<div class="col-md-12">
						<a href="#" class="btn btn-primary btn-outline mb-10 mt-15 add_line_item_field_button"><i class="fa fa-plus"></i> Add Line Item</a>
					</div>
					
				</div>
			</div>
			<div class="modal-footer" style="text-align: center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="CreateNote">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>

