
<?php $this->load->view('Template/HeaderView')?>

<!-- multi-select CSS -->
<!-- <link href="<?=SITE_URL?>vendors/bower_components/multiselect/css/multi-select.css" rel="stylesheet" type="text/css"/> -->

<!-- Bootstrap Switches CSS -->
<!-- <link href="<?=SITE_URL?>vendors/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/> -->

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<style type="text/css">.dataTables_filter { display: none; }</style>
    <div class="container-fluid pt-25">

       <!-- Row -->
	    <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-default card-view">
					<div  class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-lg-3 col-md-3 col-sm-5 col-xs-12 pr-0 pl-0">
								<form method="post" action="<?=SITE_URL?>Estimate/View/" id="SelectedAcForm">
								
										<div class="col-lg-2 col-md-3 col-sm-3 col-xs-3">
											<label class="control-label mt-10"><strong>View:</strong></label>
										</div>
										<div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 pr-0">
										<select class="selectpicker" name="SelectedEstimate" data-style="form-control btn-default btn-outline" onchange="ViewEstimates(this.value);">
											<option disabled="" selected="">Select Views</option>
											<optgroup label="Default Views">
											<option value="MyOpenEstimates" <?=(isset($SelectedEstimate) && $SelectedEstimate=='MyOpenEstimates')?"selected":""?>>My Open Estimates</option>
											<option value="AllOpenEstimates" <?=(isset($SelectedEstimate) && $SelectedEstimate=='AllOpenEstimates')?"selected":""?>>All Open Estimates</option>
											<option value="EstimatesCreatedThisWeek" <?=(isset($SelectedEstimate) && $SelectedEstimate=='EstimatesCreatedThisWeek')?"selected":""?>>Estimates Created This Week</option>
											</optgroup>
											<optgroup label="My Custom Views">
												<?php $Views = $this->ViewModel->GetViews();
												foreach ($Views as $View) { 
													if($View->EstimateViewID == $SelectedEstimate){?>
														<option value="<?=$View->EstimateViewID?>" selected=""><?=$View->EstimateViewName?></option>
													<?php } else { ?>
														<option value="<?=$View->EstimateViewID?>"><?=$View->EstimateViewName?></option>
													<?php } ?>
											<?php } ?>
											</optgroup>
										</select>
										</div>	
								
								</form>	
							</div>	
							<div class="col-lg-5 col-md-6 col-sm-7 col-xs-12">
								<div class="form-group" >
								<!-- <div class="btn-group">
									<button class="btn btn-primary btn-outline">Go</button>
								</div> --> 
								<div class="btn-group">
									<div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button"> <i class="fa fa-pencil"></i> <span class="caret"></span></button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li>
												<a><span><strong>LIST VIEW ACTIONS</strong></span></a>
											</li>
											<li>
												<a href="<?=SITE_URL?>Estimate/View/CreateNewView"><span>Create New View</span></a>
											</li>
											<li>
												<a href="#" OnClick="CopyCustomView(<?=$SelectedEstimate?>)"><span>Copy</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#account-rename-view-modal"><span>Rename</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#account-edit-sharing-view-modal"><span>Edit Sharing</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#account-edit-filters-view-modal"><span>Edit Filters</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#account-edit-displayed-columns-view-modal"><span>Edit Displayed Columns</span></a>
											</li> 
											 <li>
												<a href="#" OnClick="DeleteCustomView(<?=$SelectedEstimate?>)"><span>Delete View</span></a>
											</li>
										</ul>
									</div>
								</div>
								<div class="btn-group">
									<button class="btn btn-default btn-outline" title="Refresh" ><i class="fa fa-refresh"></i></button>
									<button class="btn btn-default btn-outline" title="Export" id="Export-Excel"><i class="fa fa-file-excel-o"></i></button>
								</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-1 col-sm-6 col-xs-12">
								<div class="pull-right">
									<div class="form-group">
										<a href="<?=SITE_URL?>Estimate/Actions/CreateEstimate" class="btn btn-primary btn-outline">New Estimate</a>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
								<div class="pull-right">
									<div class="input-group">
										<input id="myTable1globalfilter" class="form-control myTable1globalfilter" placeholder="Search" type="text">
										<span class="input-group-addon"><i class="zmdi zmdi-search"></i></span> 
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							
								<div class="table-responsive">
									<table id="myTable1" class="table table-hover display pb-30" width="100%">
										<thead>
											<tr> 
												<?php foreach ($EstimateData['ColumnsNames'] as $key => $value) { ?>
														<th class="text-center"><?=$value?></th>
												<?php } ?>
											</tr>
										</thead>
										<tbody>
										<?php foreach ($EstimateData['ColumnsValues'] as $key1 => $Estimate) {

										 ?>
											<tr> 
											<?php foreach ($EstimateData['ColumnsNames'] as $value) {
													if($value == 'EstimateNo' || $value == 'EstimateName'){ ?>
														<td class="text-center"><a href="<?=SITE_URL?>Estimate/Actions/EstimateDetails/<?=$Estimate->EstimateID?>"><?=$Estimate->$value?></a></td>
													<?php } else if($value == 'Owner'){ ?>  
														<td class="text-center"><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/<?=$Estimate->OwnerID?>"><?=$Estimate->$value?></a></td>
													<?php } else if($value == 'Account'){ ?>  
														<td class="text-center"><a href="<?=SITE_URL?>Account/Actions/AccountDetails/<?=$Estimate->AccountID?>"><?=$Estimate->$value?></a></td>
													<?php } else if($value == 'Contact'){ ?>  
														<td class="text-center"><a href="<?=SITE_URL?>Contact/Actions/ContactDetails/<?=$Estimate->ContactID?>"><?=$Estimate->$value?></a></td>
													<?php } else if($value == 'CreatedBy'){ ?>  
														<td class="text-center"><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/<?=$Estimate->CreatedByID?>"><?=$Estimate->$value?></a></td>
													<?php } else if($value == 'LastModifiedBy'){ ?>  
														<td class="text-center"><a href="<?=SITE_URL?>Setup/Administration/Actions/UserDetails/<?=$Estimate->LastModifiedByID?>"><?=$Estimate->$value?></a></td>
													<?php } else {?>
														<td class="text-center"><?=$Estimate->$value?></td>
													<?php } ?>
											<?php } ?>
											</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
		</div>
		<!-- /Row -->
	</div>	

<!-- Start Modal -->

<?php $this->load->view('Estimate/Modal/EstimateEditSharingView'); ?>
<?php $this->load->view('Estimate/Modal/EstimateEditFiltersView'); ?>
<?php $this->load->view('Estimate/Modal/EstimateEditDisplayedColumnsView'); ?>
<?php $this->load->view('Estimate/Modal/EstimateRenameView'); ?>

<?php $this->load->view('Template/FooterView')?>

<!-- Multiselect JavaScript -->
<!-- <script src="<?=SITE_URL?>vendors/bower_components/multiselect/js/jquery.multi-select.js"></script> -->
<!-- Bootstrap Switch JavaScript -->
<!-- <script src="<?=SITE_URL?>vendors/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script> -->

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript" src="<?=SITE_URL?>dist/js/multiselect.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>
	
<script type="text/javascript">

 	/*var dataTable = $('#myTable1').DataTable();

 	$('#accountglobalfilter').keyup(function(){
    	var dataTable = $('#accounttable').DataTable();
      	dataTable.search($(this).val()).draw() ;
    }); */
    function ViewEstimates()
 	{
 		$('#SelectedAcForm').submit();
 	}
</script>
<script type="text/javascript">
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
   
    var x = 1; //initlal text box count
    $(".add_field_button").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="form-group" >'
					+'<div class="col-md-4">'
					+'	<select class="form-control" name="FilterFields[]">'
					+'		<option value="">Select Field</option>'
					<?php $Fields = $this->ViewModel->EstimateViewFields(); 
					foreach($Fields as $Field){  ?>
						+'	<option value="<?=$Field->FieldName;?>"><?=$Field->FieldName;?></option>'	
					<?php } ?>
					+'	</select>'
					+'</div>'
					+'<div class="col-md-3">'
					+'		<select class="form-control" name="FilterConditions[]">'
					+'	<option value="Equals">Equals</option>'
					+'	<option value="NotEqualTo">Not Equal To</option>'
					+'	<option value="LessThan">Less Than</option>'
					+'	<option value="GreaterThan">Greater Than</option>'
					+'	<option value="LessOREqualTo">Less OR Equal To</option>'
					+'	<option value="GreaterOREqualTo">Greater OR Equal To</option>'
					+'	<option value="Contains">Contains</option>'
					+'	<option value="StartsWith">Starts With</option>'
					+'	<option value="DoesNotContain">Does Not Contain</option>'
					+'</select>'
					+'</div>'
					+'<div class="col-md-4">'
					+'	<input type="text" class="form-control" name="FilterValues[]">'
					+'</div>'
					+'<div class="col-md-1 btn-group btn-group-vertical pl-0">'
					+	 '<button type="button" id="" class="btn btn-default btn-icon-anim btn-circle remove_field_filter_criteria"><i class="fa fa-close"></i></button>'
					+'</div>'
				+'</div>'); //add input box
		}
    });
   
    $(document).on("click",".remove_field_filter_criteria", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.form-group').remove(); x--;
    });
});

/* Multiselect Init*/     
/*$('#my-select').multiSelect({ selectableOptgroup: true, selectableHeader: '<div style="text-align: center;">Available Fields</div>', selectionHeader: '<div style="text-align: center;">Displayed Fields</div>' });*/


function DeleteCustomView(EstimateViewID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#f8b32d",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>Estimate/View/DeleteCustomView/<?=$SelectedEstimate?>/";
		}, 500);
    });
	return false;
}

function CopyCustomView(EstimateViewID)
{ 
	swal({
	  title: "Nice!",
	  text: "View copy successfully.",
	  type: "success",
	  showConfirmButton: false, 
	});

	setTimeout(function () {
	    window.location.href = "<?=SITE_URL;?>Estimate/View/CopyCustomView/<?=$SelectedEstimate?>/";
	}, 1000);
}
</script>
<script type="text/javascript">
$(document).ready(function() {

     $('#multiselect').multiselect({ keepRenderingSort: true });

});
function moveTop() {
    $("#multiselect_to option:selected").each(function () {
        var listItem = $(this);
        listItem.insertBefore(listItem.siblings().first());
    });
}

function moveBottom() {
    var itemsCount = $("#multiselect_to option").length;

    $($("#multiselect_to option:selected").get().reverse()).each(function () {
        var listItem = $(this);
        listItem.insertAfter(listItem.siblings().last());
    });
}

$(document).ready(function() {
	$('.buttons-excel').hide();
	$("#Export-Excel").on("click", function() {
	    $( '.buttons-excel' ).click();
	});
});
</script>

<script type="text/javascript">
	
$(document).ready(function () {

    $('#FiltersViewForm').validate({ 
        rules: {
            "FilterFields[]": {
                required: true
            },
            "FilterValues[]": {
                required: true
            },
            "FilterConditions[]": {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    $('#RenameViewForm').validate({ 
        rules: {
            EstimateViewName: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    $('#FieldsDisplayViewForm').validate({ 
        rules: {
            "SpecifyFieldsDisplay[]": {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
});
</script>

