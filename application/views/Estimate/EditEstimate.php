<?php $this->load->view('Template/HeaderView'); ?>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

 <link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_JS_KEY?>&libraries=places"></script>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<!--Signature Pad CSS -->
<!-- <link href="<?=SITE_URL?>dist/css/signature_pad.css" rel="stylesheet" type="text/css"> -->

<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10 pb-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['Estimate']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['Estimate']['title']?></span>
									<span class="panel-title txt-dark"><?=$EstimateData->EstimateName;?></span>
								</div>
							</div>
						</div>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div  class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="col-lg-7 col-md-5 col-sm-4 col-xs-12">
							<!-- <div class="form-group"> -->
								<ul role="tablist" class="nav nav-pills" id="myTabs_13">
									<li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_13" href="#home_13">Detail</a></li>
									<li role="presentation" class=""><a data-toggle="tab" id="profile_tab_13" role="tab" href="#profile_13" aria-expanded="false">Related</a></li>
								</ul>
							<!-- </div> -->
						</div>

						<div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">

							<div class="btn-group pull-right">
									<!-- <div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
												<a><span><strong>Communication</strong></span></a>
											</li>
											<li> 
												<a href="<?=SITE_URL;?>Common/Actions/SendEmail/Estimate/<?=$EstimateData->EstimateID;?>"><span>Send Email</span></a>
											</li>
											<li> 
												<a href="#"><span>Generate Document</span></a>
											</li>
											<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
												<a><span><strong>Add New Actions</strong></span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#create-line-items-modal"><span>New Line Item</span></a>
											</li>
											<li>
												<a href="#" class="add_new_scheduling_line"><span>Schedule a Time</span></a>
											</li>
											<li>
												<a href="#"><span>New Task</span></a>
											</li>
											<li>
												<a href="#"><span>New Event</span></a>
											</li>
											<li> 
												<a href="#" data-toggle="modal" data-target="#CreateNewFile" ><span>New File</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#CreateNewNote"><span>New Note</span></a>
											</li>
											<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
												<a><span><strong>Estimate Actions</strong></span></a>
											</li>
											<li>
												<a href="#"><span>Convert to Work Order</span></a>
											</li>
											<li> 
												<a href="<?=SITE_URL;?>Estimate/Actions/CreateEstimate/<?=$EstimateData->EstimateID?>/"><span>Copy Estimate</span></a>
											</li>
											<li>
												<a href="#" onclick="DeleteEstimate(<?=$EstimateData->EstimateID?>);"><span>Delete Estimate</span></a>
											</li>
										</ul>
									</div>
								</div> -->
								<div class="btn-group pull-right">
									<a href="<?=SITE_URL;?>Estimate/Actions/EstimateDetails/<?=$EstimateData->EstimateID?>" class="btn btn-default">Cancel</a>
									<a href="#" class="btn btn-primary" id="EditEstimateLink">Save</a>

								</div>	
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
			 	<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Estimate Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>  -->
				<div class="panel-wrapper collapse in">
					<div class="panel-body"> 
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Estimate/Actions/EditEstimate/<?=$EstimateData->EstimateID?>" class="form-horizontal" id="EditEstimate" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font">
													<strong>Estimate Information</strong>
													<span class="pull-right" style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong>
													</span>
												</div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-4">Estimate No</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$EstimateData->EstimateNo;?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Estimate Name<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="EstimateName" value="<?=$EstimateData->EstimateName;?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Account<span class="required-sign">*</span></label>
														<div class="col-md-8">
														<select name="Account" id="Account" class="form-control select2" OnChange="GetPrimaryContactOptions(this.value);GetAccountDetails(this.value);">
														<option value="">Select Account</option>
														<?php $Accounts = $this->ActionsModel->GetAccounts();
															foreach ($Accounts as $Account) { 
																if($EstimateData->Account == $Account->AccountID){ ?>
																	<option value="<?=$Account->AccountID?>" selected=""><?=$Account->AccountName;?></option>
																	<?php } else { ?>
																	<option value="<?=$Account->AccountID?>"><?=$Account->AccountName;?></option>
																<?php }
															} ?>
														</select>
														<?php echo form_error('Account');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Contact</label>
														<div class="col-md-8">

														<?php $Contacts = $this->ActionsModel->GetContacts(); ?>
															<select class="form-control select2 SelectPrimaryContact" name="Contact" id="Contact" OnChange="GetContactDetails(this.value);">
															<option value="">Select Contact</option>
															<?php foreach ($Contacts as $Contact) { 
																if($EstimateData->Contact == $Contact->ContactID){?>
																		<option value="<?=$Contact->ContactID?>" selected=""><?=$Contact->FirstName.' '.$Contact->LastName?></option>
																	<?php } else {?>
																		<option value="<?=$Contact->ContactID?>"><?=$Contact->FirstName.' '.$Contact->LastName?></option>
															<?php }} ?>
														</select>
														<?php echo form_error('Contact');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Description<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Description" id="Description" value="<?=$EstimateData->Description;?>">
														</div>
													</div>
												</div>
												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-4">Owner<span class="required-sign">*</span></label>
														<div class="col-md-8">
														<select name="Owner" id="Owner" class="form-control select2">
														<option value="">Select Owner</option>
														<?php $Users = $this->ActionsModel->GetUsers();
															foreach ($Users as $User) { 
																if($EstimateData->Owner == $User->UserID){?>
																	<option value="<?=$User->UserID?>" selected=""><?=$User->FirstName.' '.$User->LastName?></option>
															<?php } else {?>
																	<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
															<?php }
															} ?>
														</select>
														<?php echo form_error('Owner');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Status<span class="required-sign">*</span></label>
														<div class="col-md-8">
														<?php $GetEstimateStatus = $this->ActionsModel->GetEstimateStatus(); ?>
															<select class="form-control" name="EstimateStatus" id="EstimateStatus">
															<option value="">Select Status</option>
															<?php foreach ($GetEstimateStatus as $EstimateStatus) { 
																if(isset($EstimateData->EstimateStatus) && $EstimateData->EstimateStatus == $EstimateStatus->EstimateStatusID){ ?>
																	<option value="<?=$EstimateStatus->EstimateStatusID?>" selected="selected"><?=$EstimateStatus->Status?></option>
																<?php } else { ?>
																	<option value="<?=$EstimateStatus->EstimateStatusID?>"><?=$EstimateStatus->Status?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('WOStatus');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Expiration Date<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control showdatepicker" name="ExpirationDate" id="ExpirationDate" value="<?=$EstimateData->ExpirationDate;?>">
															<?php echo form_error('ExpirationDate');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Phone</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Phone" id="Phone" value="<?=$EstimateData->Phone;?>">
															<?php echo form_error('Phone');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Email</label>
														<div class="col-md-8">
															<input type="email" class="form-control" name="Email" id="Email" value="<?=$EstimateData->Email;?>">
															<?php echo form_error('Email');  ?>
														</div>
													</div>

												</div>
											</div>
											<div class="col-md-12">

											<div class="txt-dark capitalize-font"><strong>Address Information</strong></div>
											<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-4">Billing Name<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="BillingName" id="BillingName" value="<?=isset($EstimateData->BillingName)?$EstimateData->BillingName:"";?>">
															<?php echo form_error('BillingName');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Billing Address</label>
														<div class="col-md-8">
															<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-search"></i>Search Address</button>
														</div>
													</div>
												
													<div class="form-group">
														<label class="col-sm-4 control-label">Billing Street<span class="required-sign">*</span></label>
														<div class="col-sm-8">
																<input type="text" class="form-control" name="BillingAddress" id="BillingAddress" placeholder="" data-toggle="modal" data-target="#location-picker-modal" value="<?=isset($EstimateData->BillingAddress)?$EstimateData->BillingAddress:"";?>">
															<?php echo form_error('BillingAddress');  ?>
															<input type="hidden" name="BillingLatitude" id="BillingLatitude" value="<?=isset($EstimateData->BillingLatitude)?$EstimateData->BillingLatitude:"";?>">
															<input type="hidden" name="BillingLongitude" id="BillingLongitude" value="<?=isset($EstimateData->BillingLongitude)?$EstimateData->BillingLongitude:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Billing City<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="BillingCity" id="BillingCity" value="<?=isset($EstimateData->BillingCity)?$EstimateData->BillingCity:"";?>">
															<?php echo form_error('BillingCity');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Billing State<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="BillingState" id="BillingState" value="<?=isset($EstimateData->BillingState)?$EstimateData->BillingState:"";?>">
															<?php echo form_error('BillingState');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-4">Billing Country<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="BillingCountry" id="BillingCountry" value="<?=isset($EstimateData->BillingCountry)?$EstimateData->BillingCountry:"";?>">
															<?php echo form_error('BillingCountry');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Billing Postal Code<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="BillingPostalCode" id="BillingPostalCode" value="<?=isset($EstimateData->BillingPostalCode)?$EstimateData->BillingPostalCode:"";?>">
															<?php echo form_error('BillingPostalCode');  ?>
														</div>
													</div>
												</div>

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-4">Shipping Name<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="ShippingName" id="ShippingName" value="<?=isset($EstimateData->ShippingName)?$EstimateData->ShippingName:"";?>">
															<?php echo form_error('ShippingName');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Shipping Address</label>
														<div class="col-md-8">
															<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal2"><i class="fa fa-search"></i>Search Address</button>
														</div>
													</div>
												
													<div class="form-group">
														<label class="col-sm-4 control-label">Shipping Street<span class="required-sign">*</span></label>
														<div class="col-sm-8">
															<input type="text" class="form-control" name="ShippingAddress" id="ShippingAddress" placeholder="" value="<?=isset($EstimateData->ShippingAddress)?$EstimateData->ShippingAddress:"";?>">
															<?php echo form_error('ShippingAddress');  ?>
															<input type="hidden" name="ShippingLatitude" id="ShippingLatitude" value="<?=isset($EstimateData->ShippingLatitude)?$EstimateData->ShippingLatitude:"";?>">
															<input type="hidden" name="ShippingLongitude" id="ShippingLongitude" value="<?=isset($EstimateData->ShippingLongitude)?$EstimateData->ShippingLongitude:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Shipping City<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="ShippingCity" id="ShippingCity" value="<?=isset($EstimateData->ShippingCity)?$EstimateData->ShippingCity:"";?>">
															<?php echo form_error('ShippingCity');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Shipping State<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="ShippingState" id="ShippingState" value="<?=isset($EstimateData->ShippingState)?$EstimateData->ShippingState:"";?>">
															<?php echo form_error('ShippingState');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-4">Shipping Country<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="ShippingCountry" id="ShippingCountry" value="<?=isset($EstimateData->ShippingCountry)?$EstimateData->ShippingCountry:"";?>">
															<?php echo form_error('ShippingCountry');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Shipping Postal Code<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="ShippingPostalCode" id="ShippingPostalCode" value="<?=isset($EstimateData->ShippingPostalCode)?$EstimateData->ShippingPostalCode:"";?>">
															<?php echo form_error('ShippingPostalCode');  ?>
														</div>
													</div>
												</div>
												<!--/span-->

											</div>
											<div class="col-md-12">

											<div class="txt-dark capitalize-font"><strong>Financials</strong></div>
											<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-4">Sub Total</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="SubTotal" value="<?=number_format((double)$EstimateFinancials['SubTotal'], 2, '.', '');?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Discount</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Discount" value="<?=number_format((double)$EstimateFinancials['Discount'], 2, '.', '');?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Tax</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Tax" value="<?=number_format((double)$EstimateFinancials['Tax'], 2, '.', '');?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Total Price</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="TotalPrice" value="<?=number_format((double)$EstimateFinancials['TotalPrice'], 2, '.', '');?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Grand Total</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="GrandTotal" value="<?=number_format((double)$EstimateFinancials['GrandTotal'], 2, '.', '');?>" readonly="">
														</div>
													</div>

												</div>
												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-4">Signature</label>
														<div class="col-md-8">
															<?php if(!empty($EstimateData->Signature)){?>
					                                            <img id="collection_signature_img" style="width: 349px;height: 102px;" src="<?=$EstimateData->Signature;?>" />
					                                          <?php } ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Line Item Cnt</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="LineItemCount" id="LineItemCount" value="<?=count($EstimateLines);?>" readonly="">
														</div>
													</div>
												</div>
											</div>

											<!-- Start Custom Fields Row -->
											<?php $this->load->view('Setup/Customize/Fields/CustomFieldsForm', array('Object' => 'Estimate', 'ObjectID' => $EstimateData->EstimateID)); ?>
											<!-- End Custom Fields Row -->

											<div class="col-md-12">

											<div class="txt-dark capitalize-font"><strong>System Information</strong></div>
											<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-4">Create Date</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$EstimateData->CreatedDate?>" readonly="">
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-4">Created By</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$EstimateData->CreatedByName?>" readonly="">
														</div>
													</div>

												</div>
												<!--/span-->

												<div class="col-md-6">

												   <div class="form-group">
														<label class="control-label col-md-4">Last Modified Date</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$EstimateData->LastModifiedDate?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Last Modified By</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$EstimateData->LastModifiedByName?>" readonly="">
														</div>
													</div>
												</div>
											</div>


											</div>
											<!-- /Row -->
										</div>

										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="EditEstimate" id="EditEstimateBtn">Save</button>
															<a href="<?=SITE_URL;?>Estimate/Actions" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			</div>
			<div id="profile_13" class="tab-pane fade" role="tabpanel">
				
				<?php $this->load->view('Estimate/Include/RelatedInformation'); ?>

			</div>
		</div>
		
		</div>

		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">

			<?php $this->load->view('Estimate/Include/SchedulingAndNotes'); ?>
			
		</div>

	</div>
	<!-- /Row -->

</div>

<!-- modal -->
 <div class="modal fade" id="location-picker-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Address Search</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" action="javascript:void(0)">
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-2">Location</label>
								<div class="col-md-10">
									<input id="BillingLocation" type="text" name="" class="form-control" placeholder="Enter Address" autocomplete="false">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="Billingpopupmap" style="height: 400px;"></div><br />
				</form>
				
			</div>
		</div>
	</div>
</div> 

 <div class="modal fade" id="location-picker-modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Address Search</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" action="javascript:void(0)">
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-2">Location</label>
								<div class="col-md-10">
									<input id="ShippingLocation" type="text" name="" class="form-control" placeholder="Enter Address" autocomplete="false">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="Shippingpopupmap" style="height: 400px;"></div><br />
				
				</form>
				
			</div>
		</div>
	</div>
</div> 

<?php //$this->load->view('Estimate/Modal/CreateNoteView'); ?>
<?php $this->load->view('Estimate/Modal/CreateLineItemsView'); ?>
<?php $data = array('data' => array('RelatedTo' => 'Estimate', 'What' => $EstimateData->EstimateID, 'AssignedTo' => $EstimateData->Owner));
$this->load->view('Common/Modal/CreateNewFile', $data); 
$this->load->view('Common/Modal/CreateNewNote', $data); ?>

<?php $this->load->view('Template/FooterView')?>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<!-- Use for common object  -->
<script src="<?=SITE_URL?>dist/js/common.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
$('.selectpicker').selectpicker();
</script>

<script type="text/javascript">

$(document).ready(function () {
 
    $('#EditEstimate').validate({ 
        rules: {
            Owner: {
                required: true
            },
            Account: {
                required: true
            },
            EstimateName: {
                required: true
            },
            Description: {
                required: true
            },
            BillingName: {
                required: true
            },
            BillingAddress: {
                required: true
            },
            BillingCity: {
                required: true
            },
            BillingState: {
                required: true
            },
            BillingCountry: {
                required: true
            },
            BillingPostalCode: {
                required: true
            },
            ShippingName: {
                required: true
            },
            ShippingAddress: {
                required: true
            },
            ShippingCity: {
                required: true
            },
            ShippingState: {
                required: true
            },
            ShippingCountry: {
                required: true
            },
            ShippingPostalCode: {
                required: true
            },
            EstimateStatus: {
                required: true
            },
            ExpirationDate: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
        submitHandler: function (form) { 
        	return true;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });

    /*$('#CreateNoteForm').validate({ 
        rules: {
            Subject: {
                required: true
            },
            Body: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });*/

   /* $('.showdatetimepicker').datetimepicker({
		useCurrent: true,
		icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
	});*/
});

$(document).on('click', "#EditEstimateLink", function () { 
	$('#EditEstimateBtn').click(); 
});

function DeleteEstimate(EstimateID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#5fc55f",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>Estimate/Actions/DeleteEstimate/<?=$EstimateData->EstimateID;?>/";
		}, 500);
    });
	return false;
}

function GetAccountDetails(AccountID){
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/GetAccountDetails/'+AccountID,
        method: "post",
      /*  data: { AccountID : AccountID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$("#BillingName").val(result.AccountName);
            	$("#ShippingName").val(result.AccountName);
            	$("#BillingAddress").val(result.BillingAddress);
            	$("#BillingCity").val(result.BillingCity);
            	$("#BillingState").val(result.BillingState);
            	$("#BillingCountry").val(result.BillingCountry);
            	$("#BillingPostalCode").val(result.BillingPostalCode);
            	$("#BillingLatitude").val(result.BillingLatitude);
            	$("#BillingLongitude").val(result.BillingLongitude);
            	$("#ShippingAddress").val(result.ShippingAddress);
            	$("#ShippingCity").val(result.ShippingCity);
            	$("#ShippingState").val(result.ShippingState);
            	$("#ShippingCountry").val(result.ShippingCountry);
            	$("#ShippingPostalCode").val(result.ShippingPostalCode);
            	$("#ShippingLatitude").val(result.ShippingLatitude);
            	$("#ShippingLongitude").val(result.ShippingLongitude);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}

function GetContactDetails(ContactID){
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/GetContactDetails/'+ContactID,
        method: "post",
      /*  data: { AccountID : AccountID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$("#Phone").val(result.PhoneNo);
            	$("#Email").val(result.Email);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}
</script>

<!-- Set Location -->
<script src="<?=SITE_URL?>dist/js/location-picker.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	
	<?php if(!empty($EstimateData->BillingLatitude) && !empty($EstimateData->BillingLongitude)){ ?>
		set_location_in_map("Billing", <?=$EstimateData->BillingLatitude;?>, <?=$EstimateData->BillingLongitude;?>);
	<?php } else { ?>
		set_location_in_map("Billing");
	<?php } ?>

	<?php if(!empty($EstimateData->ShippingLatitude) && !empty($EstimateData->ShippingLongitude)){ ?>
		set_location_in_map("Shipping", <?=$EstimateData->ShippingLatitude;?>, <?=$EstimateData->ShippingLongitude;?>);
	<?php } else { ?>
		set_location_in_map("Shipping");
	<?php } ?>
});
</script>
<script type="text/javascript">

function GetPrimaryContactOptions(AccountID){
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/GetPrimaryContactOptions/',
        method: "post",
        data: { AccountID : AccountID, PrimaryContact:'<?=$EstimateData->Contact;?>'},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$(".SelectPrimaryContact").html(result);

               	return true;  	
            } else {
                $(".SelectPrimaryContact").html('');
                return false;
            }
        }
    });
}

$(document).ready(function() { 
	GetPrimaryContactOptions(<?=$EstimateData->Account?>);
});
</script>

<script type="text/javascript">
/* Line Items Start */ 
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".line_item_fields_wrap"); //Fields wrapper
   
    var x = <?=count($EstimateLines)-1;?>; //initlal text box count
    $(".add_line_item_field_button").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="col-md-12 pt-10 product-line-items">'
					+	'<div class="col-md-4">'
					+	'<select name="Product[]" id="Product" class="selectpicker" data-style="form-control btn-default btn-outline" data-show-subtext="true" data-live-search="true" OnChange="GetProductDetails(this.value, '+x+');">'
					+		'<option value="">Search Product</option>'
							<?php $Products = $this->ActionsModel->GetProducts();
								foreach ($Products as $Product) {  ?>
					+				'<option value="<?=$Product->ProductID?>" data-subtext="<?=$Product->ProductCode?> - $<?=number_format($Product->ListPrice, 2, '.', ',')?>"><?=$Product->ProductName?></option>'
							<?php } ?>
					+		'</select>'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="ListPrice'+x+'" name="ListPrice[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="Discount'+x+'" name="Discount[]" class="form-control" type="text" value="0" OnChange="CalculatePriceTotal('+x+')">'
					
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="UnitPrice'+x+'" name="UnitPrice[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+		'<input type="text" name="Quantity[]" id="Quantity'+x+'" class="form-control" value="1" OnChange="CalculatePriceTotal('+x+')">'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="SubTotal'+x+'" name="SubTotal[]" class="form-control" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+	'</div>'
					+	'<div class="col-md-1 text-center pt-10">'
					+		'<div class="checkbox checkbox-primary pt-0">'
					+			'<input id="Taxable'+x+'" name="Taxable[]" type="checkbox" value="1" OnChange="CalculatePriceTotal('+x+')">'
					+			'<label for="checkbox2"></label>'
					+		'</div>'
					+	'</div>'
					+	'<div class="col-md-1">'
					+			'<input id="TotalPrice'+x+'" name="TotalPrice[]" class="form-control TotalPrice" type="text" value="0.00" OnChange="CalculatePriceTotal('+x+')" readonly="">'
					+	'</div>'
					+	'<div class="col-md-1 text-center">'
					+		'<a href="#" class="btn btn-default remove_line_item_field blank-new-field"><i class="fa fa-trash-o"></i></a>'
					+	'</div>'
					+'</div>'); //add input box
		}
		//$(".select2").select2();
		$('.selectpicker').selectpicker();
    });
   
    $(document).on("click",".remove_line_item_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.product-line-items').remove(); x--;

        CalculateGrandTotal();
    });

    CalculateGrandTotal();
});

$(document).on("click",".add-new-line-item", function(e){ //user click on remove text
   $(".add_line_item_field_button").click();
});

$(document).on("click",".edit-line-items", function(e){ //user click on remove text
   $(".blank-new-field.remove_line_item_field").click();
});

function GetProductDetails(ProductID, x)
{ 
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/GetProductDetails/'+ProductID,
        method: "post",
       /* data: { ProductID : ProductID},*/
        dataType: 'json',
        success: function (result) {
            if (result != '') {
            	$('#ListPrice'+x).val(result.ListPrice);
            	if(result.Taxable){ 
            		$( "#Taxable"+x ).prop( "checked", true);
            	}
         
            	if(result.IsListPriceEditable == 0){ 
            		$( "#ListPrice"+x ).prop( "readonly", true);
            	} else {
            		$( "#ListPrice"+x ).removeAttr( "readonly");
            	}
            	CalculatePriceTotal(x);
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });

}

function CalculatePriceTotal(x)
{ 
	var ListPrice = parseFloat($('#ListPrice'+x).val().replace(',', ''));
	var Discount = parseFloat($('#Discount'+x).val().replace(',', ''));
	var Quantity = parseFloat($('#Quantity'+x).val().replace(',', ''));

	var UnitPrice = ListPrice - (ListPrice * Discount) / 100;
	var SubTotal = UnitPrice * Quantity;
	if($('#Taxable'+x).is(':checked'))
		var TotalPrice = SubTotal + (SubTotal * <?=$EstimateData->Tax;?>) / 100;
	else 
		var TotalPrice = SubTotal;
	 
	$('#ListPrice'+x).val(NumberFormat(ListPrice));
	$('#UnitPrice'+x).val(NumberFormat(UnitPrice));
	$('#SubTotal'+x).val(NumberFormat(SubTotal));
	$('#TotalPrice'+x).val(NumberFormat(TotalPrice));

	CalculateGrandTotal();
}

function CalculateGrandTotal()
{ 
	var GrandTotal = 0;

    $(".TotalPrice").each(function(){

    	GrandTotal = parseFloat(GrandTotal) + parseFloat($(this).val().replace(',', ''));
    });
	$('#GrandTotal').val(NumberFormat(GrandTotal));
}

function NumberFormat(n) {
  return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}

/* Line Items End */  
</script>

<script type="text/javascript">
/* Scheduling Start */ 
$(document).ready(function() {

    var max_fields      = 20; //maximum input boxes allowed
    var wrapper         = $(".new_scheduling_line_wrap"); //Fields wrapper
   
    var x = 1;//<?=count($Events);?>; //initlal text box count
    $(".add_new_scheduling_line").click(function(e){ //on add input button click

        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div id="SchedulingLine'+x+'"><div class="form-wrap">'
					+	'<form action="#" class="form-horizontal schedulingfrmclss" id="SchedulingForm'+x+'">'
					+   '<input type="hidden" name="EstimateName" value="<?=$EstimateData->EstimateName;?>">'
					+   '<input type="hidden" name="EstimateNo" value="<?=$EstimateData->EstimateNo;?>">'
					+   '<input type="hidden" name="EstimateID" value="<?=$EstimateData->EstimateID;?>">'
					+   '<input type="hidden" name="Who" value="<?=$EstimateData->Contact;?>">'
					+   '<input type="hidden" name="Description" value="<?=$EstimateData->Description;?>">'
					+	'<div class="form-body">'
					+		'<div class="form-group">'
					+			'<label class="col-md-3 control-label text-left">Assigned To</label>'
					+			'<div class="col-md-6">'
					+				'<select name="AssignedTo" id="AssignedTo" class="form-control select2">'
					+				'<option value="">Select Owner</option>'
									<?php $Users = $this->ActionsModel->GetUsers();
										foreach ($Users as $User) { 
											if($EstimateData->Owner == $User->UserID){?>
					+							'<option value="<?=$User->UserID?>" selected=""><?=$User->FirstName.' '.$User->LastName?></option>'
										<?php } else {?>
					+							'<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>'
										<?php }
										} ?>
					+				'</select>'
					+			'</div>'
					+		'</div>'
					+		'<div class="form-group">'
					+			'<label class="col-md-3 control-label text-left">Start</label>'
					+			'<div class="col-md-6">'
					+				'<input type="text" class="form-control showdatetimepicker" name="EventStartDate" value="<?=date("m/d/Y")?>" required>'
					+			'</div>'
					+		'</div>'
					+		'<div class="form-group">'
					+			'<label class="col-md-3 control-label text-left">End</label>'
					+			'<div class="col-md-6">'
					+				'<input type="text" class="form-control showdatetimepicker" name="EventEndDate" value="<?=date("m/d/Y")?>" required>'
					+			'</div>'
					+		'</div>'
					+	'</div>'
					+	'<div class="pull-right">'
					+		'<button type="button" class="btn btn-default remove_new_scheduling_line" data-dismiss="modal">Close</button> '
					+		'<button type="button" class="btn btn-primary" OnClick="SaveScheduling('+x+')">Save</button>'
					+	'</div>'
					+	'</form>'
					+ '<div class="clearfix"></div>'
					+ '<hr class="light-grey-hr">'
					+ '</div>'
					+ '</div>'); //add input box
		}
		$(".select2").select2();
		$('.showdatetimepicker').datetimepicker({
			useCurrent: true,
			icons: {
	                time: "fa fa-clock-o",
	                date: "fa fa-calendar",
	                up: "fa fa-arrow-up",
	                down: "fa fa-arrow-down"
	            }
		});

		SetViewMoreBtnInScheduling();
    });
    $(wrapper).on("click",".remove_new_scheduling_line", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.form-wrap').remove(); x--;

        SetViewMoreBtnInScheduling();
        
    })
});

function SetViewMoreBtnInScheduling(){
	if($(".schedulingfrmclss").length > 5){
		$(".scheduling-view-more-btn").html('<div class="pull-right">'
			+ '<a href="<?=SITE_URL?>Estimate/Actions/RelatedObject/<?=$EstimateData->EstimateID;?>/Events" type="button" class="btn btn-primary btn-outline">View All</a>'
			+ '</div>');
	} else {
		$(".scheduling-view-more-btn").html('');
	}
}

function SaveScheduling(x)
{
 	 $.ajax({
            url: '<?=SITE_URL?>Estimate/Actions/SaveScheduling/',
            data: $("#SchedulingForm"+x).serialize(),
            method: "post",
            dataType: 'html',
            success: function (result) {
                if (result != '') {
                	$(".new_scheduling_line_wrap").html(result);

                if($(".schedulingfrmclss").length > 0)
            		$("button.add_new_scheduling_line").html('<i class="fa fa-plus"></i> Add another time');

                  return true;  	
                } else {
                    alert("Something went wrong.Please try again!");
                    return false;
                }
            }
        });
}

function EditScheduling(EventID)
{
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/GetScheduling/'+EventID,
        method: "post",
        dataType: 'html',
        success: function (result) {
            if (result != '') {

            	$("#Event"+EventID).html(result); //add input box
            	$(".select2").select2();
				$('.showdatetimepicker').datetimepicker({
					useCurrent: true,
					icons: {
			                time: "fa fa-clock-o",
			                date: "fa fa-calendar",
			                up: "fa fa-arrow-up",
			                down: "fa fa-arrow-down"
			            }
				});
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}	

function UpdateScheduling(EventID)
{
 	 $.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/UpdateScheduling/'+EventID,
        data: $("#SchedulingForm"+EventID).serialize(),
        method: "post",
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$(".new_scheduling_line_wrap").html(result);
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}

function DeleteScheduling(EventID, EstimateID)
{ 
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/DeleteScheduling/'+EventID,
        method: "post",
        data: { EventID : EventID, EstimateID : EstimateID},
        dataType: 'html',
        success: function (result) {
            
        	$(".new_scheduling_line_wrap").html(result);

        	if($(".schedulingfrmclss").length == 0)
        		$("button.add_new_scheduling_line").html('Schedule a Time');

        	SetViewMoreBtnInScheduling();

           	return true;  	
        }
    });
}

function EditSchAssignedTo(EventID, AssignedTo)
{ 
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/EditSchAssignedTo/',
        method: "post",
        data: { EventID : EventID, AssignedTo : AssignedTo},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$("#AssignedToDropDown"+EventID).html(result);
            	$("#AssignedToBtn"+EventID).html('<a onclick="UpdateAssignedTo('+EventID+')">Save</a>');
            	$(".select2").select2();
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}

function UpdateAssignedTo(EventID)
{ 
	var AssignedTo = $("#AssignedToVal"+EventID).val();
	$.ajax({
        url: '<?=SITE_URL?>Estimate/Actions/UpdateAssignedTo/',
        method: "post",
        data: { EventID : EventID, AssignedTo : AssignedTo},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$("#AssignedToDropDown"+EventID).html(result);
            	$("#AssignedToBtn"+EventID).html('<a onclick="EditSchAssignedTo('+EventID+', '+AssignedTo+')">Change</a>');
               	return true;  	
            } else {
                alert("Something went wrong.Please try again!");
                return false;
            }
        }
    });
}
/* Scheduling End */ 
</script>