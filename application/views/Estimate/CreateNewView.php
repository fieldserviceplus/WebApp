
<?php $this->load->view('Template/HeaderView')?>


<div class="container-fluid pt-25">

	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Create New View</h6>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Estimate/View/CreateNewView" class="form-horizontal" id="CreateNewView" method="post">
										<div class="form-body">

											<!-- /Row -->
											<div class="row">
												<div class="col-md-12">

													<div class="txt-dark capitalize-font" style="font-weight: bold;">Enter View Name</div>
													<hr class="light-grey-hr"/>

													<div class="form-group">
														<label class="control-label col-md-2">View Name</label>
														<div class="col-md-4">
															<input type="text" name="EstimateViewName" class="form-control" placeholder="Enter View Name Here">
														</div>
													</div>

													<div class="txt-dark capitalize-font" style="font-weight: bold;">Specify Filter Criteria</div>
													<hr class="light-grey-hr"/>
												
													<div class="col-md-12 pb-10">Enter Additional Filters Criteria (optional):</div>

													<div class="form-group">
														<div class="col-md-1"></div>
														<div class="col-md-2">
															<select class="form-control" name="FilterFields[]">
																<option value="">Select Filter Field</option>
																<?php foreach($EstimateViewFields as $Field){ ?>
																		<option value="<?=$Field->FieldName;?>"><?=$Field->FieldName;?></option>
																<?php } ?>
															</select>
														</div>
														<div class="col-md-2">
															<select class="form-control" name="FilterConditions[]">
																<option value="Equals">Equals</option>
																<option value="NotEqualTo">Not Equal To</option>
																<option value="LessThan">Less Than</option>
																<option value="GreaterThan">Greater Than</option>
																<option value="LessOREqualTo">Less OR Equal To</option>
																<option value="GreaterOREqualTo">Greater OR Equal To</option>
																<option value="Contains">Contains</option>
																<option value="StartsWith">Starts With</option>
																<option value="DoesNotContain">Does Not Contain</option>
															</select>
														</div>
														<div class="col-md-3">
															<input type="text" class="form-control" name="FilterValues[]">
														</div>
														<div class="col-md-1 btn-group btn-group-vertical pl-0">
															<!-- <button type="button" id="" class="btn btn-default btn-icon-anim btn-circle remove_field_filter_criteria"><i class="fa fa-close"></i></button> -->
														</div>
													</div>
												
													<!-- <div class="form-group">
														<div class="col-md-1"></div>
														<div class="col-md-2">
															<select class="form-control" name="FilterFields[]">
																<option value="" disabled="">Select Filter Field</option>
																<?php /*foreach($EstimateViewFields as $Field){ ?>
																		<option value="<?=$Field->FieldName;?>"><?=$Field->FieldName;?></option>
																<?php }*/ ?>
															</select>
														</div> 
														<div class="col-md-2">
															<select class="form-control" name="FilterConditions[]">
																<option value="Equals">Equals</option>
																<option value="NotEqualTo">Not Equal To</option>
																<option value="LessThan">Less Than</option>
																<option value="GreaterThan">Greater Than</option>
																<option value="LessOREqualTo">Less OR Equal To</option>
																<option value="GreaterOREqualTo">Greater OR Equal To</option>
																<option value="Contains">Contains</option>
																<option value="StartsWith">Starts With</option>
																<option value="DoesNotContain">Does Not Contain</option>
															</select>
														</div>
														<div class="col-md-3">
															<input type="text" class="form-control" name="FilterValues[]">
														</div>
														<div class="col-md-1 btn-group btn-group-vertical pl-0">
															<button type="button" id="" class="btn btn-default btn-icon-anim btn-circle remove_field_filter_criteria"><i class="fa fa-close"></i></button>
														</div>
													</div> -->

													<!-- <div class="form-group" >
														<div class="col-md-1"></div>
														<div class="col-md-2">
															<select class="form-control" name="FilterFields[]">
																<option value="" disabled="">Select Filter Field</option>
																<?php /*foreach($EstimateViewFields as $Field){ ?>
																		<option value="<?=$Field->FieldName;?>"><?=$Field->FieldName;?></option>
																<?php }*/ ?>
															</select>
														</div>
														<div class="col-md-2">
															<select class="form-control" name="FilterConditions[]">
																<option value="Equals">Equals</option>
																<option value="NotEqualTo">Not Equal To</option>
																<option value="LessThan">Less Than</option>
																<option value="GreaterThan">Greater Than</option>
																<option value="LessOREqualTo">Less OR Equal To</option>
																<option value="GreaterOREqualTo">Greater OR Equal To</option>
																<option value="Contains">Contains</option>
																<option value="StartsWith">Starts With</option>
																<option value="DoesNotContain">Does Not Contain</option>
														</select>
														</div>
														<div class="col-md-3">
															<input type="text" class="form-control" name="FilterValues[]">
														</div>
														<div class="col-md-1 btn-group btn-group-vertical pl-0">
															<button type="button" id="" class="btn btn-default btn-icon-anim btn-circle remove_field_filter_criteria"><i class="fa fa-close"></i></button>
														</div>
													</div>-->

													<div class="input_fields_wrap" >
													</div>

													<div class="col-md-12 pb-15">
														<button type="button" class="btn btn-default btn-outline add_field_button">Add Filter +</button>
													</div> 

													<div class="txt-dark capitalize-font" style="font-weight: bold;">Specify Fields to Display</div>
													<hr class="light-grey-hr"/>
													<!-- <div class="col-md-12">
														<div class="col-md-2"></div>
														<div class="col-md-6 pb-15">
															<select multiple="multiple" id="my-select" name="SpecifyFieldsDisplay[]">
																<?php foreach($EstimateViewFields as $Field){ ?>
																		<option value="<?=$Field->FieldName;?>"><?=$Field->FieldName;?></option>
																<?php } ?>
														    </select>
														</div>
														<div class="col-md-4"></div>
													</div> -->
        
											        <div class="col-md-12">
											        	<div class="col-md-2"></div>
											            <div class="col-md-3">
											                <select name="from[]" id="multiselect" class="form-control" multiple="multiple" size="8" >
											                   <?php foreach($EstimateViewFields as $Field){ ?>
																	<option value="<?=$Field->FieldName;?>"><?=$Field->FieldName;?></option>
																<?php } ?>
											                </select>
											            </div>
											            
											            <div class="col-md-1 btn-group btn-group-vertical">
											                <button type="button" id="multiselect_rightAll" class="btn btn-default btn-icon-anim btn-circle ml-40"><i class="fa fa-angle-double-right"></i></button>
											                <button type="button" id="multiselect_rightSelected" class="btn btn-default btn-icon-anim btn-circle ml-40"><i class="fa fa-chevron-circle-right"></i></button>
											                <button type="button" id="multiselect_leftSelected" class="btn btn-default btn-icon-anim btn-circle ml-40"><i class="fa fa-chevron-circle-left"></i></button>
											                <button type="button" id="multiselect_leftAll" class="btn btn-default btn-icon-anim btn-circle ml-40"><i class="fa fa-angle-double-left"></i></button>
											            </div>
											            
											            <div class="col-md-3">
											                <select name="SpecifyFieldsDisplay[]" id="multiselect_to" class="form-control" size="8" multiple="multiple"></select>
											            </div>

											            <div class="col-md-1 btn-group btn-group-vertical text-center">
											                <button type="button" id="multiselect_rightAll" class="btn btn-default btn-icon-anim btn-circle" onclick="moveTop();"><i class="fa fa-angle-double-up"></i></button>
											                <button type="button" id="multiselect_move_up" class="btn btn-default btn-icon-anim btn-circle"><i class="fa fa-chevron-circle-up"></i></button>
											                <button type="button" id="multiselect_move_down" class="btn btn-default btn-icon-anim btn-circle"><i class="fa fa-chevron-circle-down"></i></button>
											                <button type="button" id="multiselect_leftAll" class="btn btn-default btn-icon-anim btn-circle" onclick="moveBottom();"><i class="fa fa-angle-double-down"></i></button>
											            </div>
											        </div>


													<div class="txt-dark capitalize-font" style="font-weight: bold;">Restrict Visibility</div>
													<hr class="light-grey-hr"/>
													<div class="col-md-12">
														<div class="form-group" >
															<div class="col-sm-12">
																<div class="radio radio-primary">
																	<input name="RestrictVisibility" id="radio11" value="VisibleOnlyToMe" checked="" type="radio">
																	<label for="radio11"> Visible only to me </label>
																</div>
																<div class="radio radio-primary">
																	<input name="RestrictVisibility" id="radio12" value="VisibleToEveryone" type="radio">
																	<label for="radio12"> Visible to everyone </label>
																</div>
															</div>
														</div>
													</div>

												</div>
												<!--/span-->

											</div>
											<!-- /Row -->
										</div>
										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="CreateNewView">Save</button>
															<button type="button" class="btn btn-default" onclick="window.history.go(-1); return false;">Cancel</button>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->


</div>

<?php $this->load->view('Template/FooterView')?>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>dist/js/multiselect.min.js"></script>

<script type="text/javascript">
	
$(document).ready(function () {

    $('#CreateNewView').validate({ 
        rules: {
            EstimateViewName: {
                required: true
            },
            "SpecifyFieldsDisplay[]": {
                required: true
            },
            "FilterFields[]": {
                required: true
            },
            "FilterValues[]": {
                required: true
            },
            "FilterConditions[]": {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
   
    var x = 1; //initlal text box count
    $(".add_field_button").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="form-group" >'
					+'<div class="col-md-1"></div>'
					+'<div class="col-md-2">'
					+'	<select class="form-control" name="FilterFields[]">'
					+'		<option value="">Select Filter Field</option>'
						<?php foreach($EstimateViewFields as $Field){  ?>
							+'	<option value="<?=$Field->FieldName;?>"><?=$Field->FieldName;?></option>'	
						<?php } ?>
					+'	</select>'
					+'</div>'
					+'<div class="col-md-2">'
					+'		<select class="form-control" name="FilterConditions[]">'
					+'	<option value="Equals">Equals</option>'
					+'	<option value="NotEqualTo">Not Equal To</option>'
					+'	<option value="LessThan">Less Than</option>'
					+'	<option value="GreaterThan">Greater Than</option>'
					+'	<option value="LessOREqualTo">Less OR Equal To</option>'
					+'	<option value="GreaterOREqualTo">Greater OR Equal To</option>'
					+'	<option value="Contains">Contains</option>'
					+'	<option value="StartsWith">Starts With</option>'
					+'	<option value="DoesNotContain">Does Not Contain</option>'
					+'</select>'
					+'</div>'
					+'<div class="col-md-3">'
					+'	<input type="text" class="form-control" name="FilterValues[]">'
					+'</div>'
					+'<div class="col-md-1 btn-group btn-group-vertical pl-0">'
					+	 '<button type="button" id="" class="btn btn-default btn-icon-anim btn-circle remove_field_filter_criteria"><i class="fa fa-close"></i></button>'
					+'</div>'
				+'</div>'); //add input box
		}
    });
   
    $(document).on("click",".remove_field_filter_criteria", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.form-group').remove(); x--;
    });
});
</script>

<script type="text/javascript">
$(document).ready(function() {

     $('#multiselect').multiselect({ keepRenderingSort: true });

});
function moveTop() {
    $("#multiselect_to option:selected").each(function () {
        var listItem = $(this);
        listItem.insertBefore(listItem.siblings().first());
    });
}

function moveBottom() {
    var itemsCount = $("#multiselect_to option").length;

    $($("#multiselect_to option:selected").get().reverse()).each(function () {
        var listItem = $(this);
        listItem.insertAfter(listItem.siblings().last());
    });
}
</script>
