<?php $this->load->view('Template/HeaderView')?>

<div class="container-fluid pt-25">

	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Estimate <?=$EstimateData->EstimateNo;?></h6>
						<a href="<?=SITE_URL;?>Estimate/Actions/EstimateDetails/<?=$EstimateData->EstimateID;?>">&lt; Back to Estimate</a>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
							<?php if(isset($PDFOutputFile) && !empty($PDFOutputFile)){ ?>
								<object
								  data="<?=$PDFOutputFile;?>"
								  type="application/pdf"
								  width="100%"
								  height="1475px">
								 <p>Your browser does not support PDFs.
								      <a href="http://localhost/fieldserviceplus/doc_templates/Generated_Estimate.pdf">Download the PDF</a>.</p>
								</object>
							<?php } else { ?>
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Estimate/Actions/GenerateDocument/<?=$EstimateData->EstimateID;?>" class="form-horizontal" id="CreateNewView" method="post">
										<div class="form-body">

											<!-- /Row -->
											<div class="row">
												<div class="col-md-12">

													<div class="txt-dark capitalize-font" style="font-weight: bold;">Generate Document</div>
													<hr class="light-grey-hr"/>

													<div class="form-group">
														<label class="control-label col-md-2">Template</label>
														<div class="col-md-2">
														<?php $Templates = $this->ActionsModel->GenDocTemplates("Estimate"); ?>
															<select class="form-control" name="GenDocTemplateID" id="GenDocTemplateID" required="">
															<option value="">Select Template</option>
															<?php foreach ($Templates as $Template) { ?>
																	<option value="<?=$Template->GenDocTemplateID?>"><?=$Template->TemplateName?></option>
															<?php } ?>
															</select>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-2">Output Format</label>
														<div class="col-md-2">
															<select name="OutputFormat" id="OutputFormat" class="form-control" required="">
																<option value="PDF">PDF</option>
																<option value="Word">Word</option>
															</select>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-2">Save to Estimate</label>
														<div class="col-md-4">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="SaveToEstimate" id="SaveToEstimate" value="1" checked="">
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

												</div>
												<!--/span-->

											</div>
											<!-- /Row -->
										</div>
										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="GenerateDocument">Save</button>
															<button type="button" class="btn btn-default" onclick="window.history.go(-1); return false;">Cancel</button>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->
</div>

<?php $this->load->view('Template/FooterView')?>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<script type="text/javascript">
	
$(document).ready(function () {

    $('#CreateNewView').validate({ 
        rules: {
            EstimateViewName: {
                required: true
            },
            "SpecifyFieldsDisplay[]": {
                required: true
            },
            "FilterFields[]": {
                required: true
            },
            "FilterValues[]": {
                required: true
            },
            "FilterConditions[]": {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
});
</script>
