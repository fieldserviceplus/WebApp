<?php $this->load->view('Template/HeaderView'); ?>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
<script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_JS_KEY?>&libraries=places"></script>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10 pb-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['Contact']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['Contact']['title']?></span>
									<span class="panel-title txt-dark"><?=$ContactData->FirstName;?> <?=$ContactData->LastName;?></span>
								</div>
							</div>
						</div>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div  class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="col-lg-7 col-md-5 col-sm-4 col-xs-12">
							<!-- <div class="form-group"> -->
								<ul role="tablist" class="nav nav-pills" id="myTabs_13">
									<li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_13" href="#home_13">Detail</a></li>
									<li role="presentation" class=""><a data-toggle="tab" id="profile_tab_13" role="tab" href="#profile_13" aria-expanded="false">Related</a></li>
								</ul>
							<!-- </div> -->
						</div>

						<div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">

							<div class="btn-group pull-right">
									<!-- <div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li>
												<a><span><strong>Communication</strong></span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>Common/Actions/SendEmail/Contact/<?=$ContactData->ContactID;?>"><span>Send Email</span></a>
											</li>
											<li>
												<a><span><strong>Add New Actions</strong></span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>WorkOrder/Actions/CreateWorkOrder/?Contact=<?=$ContactData->ContactID;?>&Account=<?=$ContactData->Account;?>"><span>New Work Order</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>Estimate/Actions/CreateEstimate/?Contact=<?=$ContactData->ContactID;?>&Account=<?=$ContactData->Account;?>"><span>New Estimate</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>Invoice/Actions/CreateInvoice/?Contact=<?=$ContactData->ContactID;?>&Account=<?=$ContactData->Account;?>"><span>New Invoice</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>Event/Actions/CreateEvent/?Contact=<?=$ContactData->ContactID;?>&Account=<?=$ContactData->Account;?>"><span>New Event</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>Task/Actions/CreateTask/?Contact=<?=$ContactData->ContactID;?>&Account=<?=$ContactData->Account;?>"><span>New Task</span></a>
											</li>
											<li> 
												<a href="JavaScript:void(0);" data-toggle="modal" data-target="#CreateNewFile"><span>New File</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#CreateNewNote"><span>New Note</span></a>
											</li>
											<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
												<a><span><strong>Contact Actions</strong></span></a>
											</li>
											<li> 
												<a href="<?=SITE_URL;?>Contact/Actions/CreateContact/<?=$ContactData->ContactID?>/"><span>Copy Contact</span></a>
											</li>
											<li>
												<a href="#" onclick="DeleteContact();"><span>Delete Contact</span></a>
											</li>
										</ul>
									</div>
								</div> -->
								<div class="btn-group pull-right">
									<a href="<?=SITE_URL;?>Contact/Actions/ContactDetails/<?=$ContactData->ContactID?>" class="btn btn-default">Cancel</a>
									<a href="#" class="btn btn-primary" id="EditContactLink">Save</a>
								</div>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
			 	<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Contact Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>  -->
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Contact/Actions/EditContact/<?=$ContactData->ContactID?>" class="form-horizontal" id="EditContact" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font">
													<strong>Contact Information</strong>
													<span class="pull-right" style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong>
													</span>
												</div>
												
												<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-4">Assigned To<span class="required-sign">*</span></label>
														<div class="col-md-8">
														<select name="AssignedTo" id="AssignedTo" class="form-control select2">
														<option value="">Select Assigned To</option>
														<?php $Users = $this->ActionsModel->GetUsers();
															foreach ($Users as $User) { 
																if(isset($ContactData->AssignedTo) && $ContactData->AssignedTo == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else { ?>
																	<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('AssignedTo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Name<span class="required-sign">*</span></label>
														<div class="col-md-2">
															<select name="Salutation" id="Salutation" class="form-control">
																<?php $Salutations = $this->ActionsModel->GetSalutation();
																	foreach ($Salutations as $Salutation) { 
																		if(isset($ContactData->Salutation) && $ContactData->Salutation == $Salutation->Salutation){ ?>
																			<option value="<?=$Salutation->Salutation?>" selected="selected"><?=$Salutation->Salutation?></option>
																		<?php } else { ?>
																			<option value="<?=$Salutation->Salutation?>"><?=$Salutation->Salutation?></option>
																		<?php } ?>
																	<?php } ?>
															</select>
															<?php echo form_error('Salutation');  ?>
														</div>
														<div class="col-md-3">
															<input type="text" class="form-control" name="FirstName" id="FirstName" value="<?=isset($ContactData->FirstName)?$ContactData->FirstName:"";?>" placeholder="First Name">
															<?php echo form_error('FirstName');  ?>
														</div>
														<div class="col-md-3">
															<input type="text" class="form-control" name="LastName" id="LastName" value="<?=isset($ContactData->LastName)?$ContactData->LastName:"";?>" placeholder="Last Name">
															<?php echo form_error('LastName');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Title<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Title" id="Title" value="<?=isset($ContactData->Title)?$ContactData->Title:"";?>">
															<?php echo form_error('Title');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Email</label>
														<div class="col-md-8">
															<input type="email" class="form-control" name="Email" id="Email" value="<?=isset($ContactData->Email)?$ContactData->Email:"";?>">
															<?php echo form_error('Email');  ?>
														</div>
													</div>
											
													<div class="form-group">
														<label class="control-label col-md-4">Phone #</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="PhoneNo" id="PhoneNo" value="<?=isset($ContactData->PhoneNo)?$ContactData->PhoneNo:"";?>">
															<?php echo form_error('PhoneNo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Mobile #</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="MobileNo" id="MobileNo" value="<?=isset($ContactData->MobileNo)?$ContactData->MobileNo:"";?>">
															<?php echo form_error('MobileNo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Do Not Call</label>
														<div class="col-md-8">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="DoNotCall" id="DoNotCall" value="0" <?=($ContactData->DoNotCall==1)?"checked=''":"";?>>
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>
													
												</div>
												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-4">Account<span class="required-sign">*</span></label>
														<div class="col-md-8">
														<select name="Account" id="Account" class="form-control select2">
														<option value="">Select Account</option>
														<?php $Accounts = $this->ActionsModel->GetAccounts();
															foreach ($Accounts as $Account) { 
																if(isset($ContactData->Account) && $ContactData->Account == $Account->AccountID){ ?>
																	<option value="<?=$Account->AccountID?>" selected="selected"><?=$Account->AccountName?></option>
																<?php } else { ?>
																	<option value="<?=$Account->AccountID?>"><?=$Account->AccountName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('Account');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Birth Date</label>
														<div class="col-md-8">
															<input type="text" class="form-control showdatepicker" name="BirthDate" id="BirthDate" value="<?=isset($ContactData->BirthDate)?$ContactData->BirthDate:"";?>">
															<?php echo form_error('BirthDate');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Lead Source<span class="required-sign">*</span></label>
														<div class="col-md-8">
														<select name="LeadSource" id="LeadSource" class="form-control">
														<option value="">Select Lead Source</option>
														<?php $LeadSources = $this->ActionsModel->GetLeadSources();
															foreach ($LeadSources as $LeadSource) { 
																if(isset($ContactData->LeadSource) && $ContactData->LeadSource == $LeadSource->LeadSourceID){ ?>
																	<option value="<?=$LeadSource->LeadSourceID?>" selected="selected"><?=$LeadSource->LeadSource?></option>
																<?php } else { ?>
																	<option value="<?=$LeadSource->LeadSourceID?>"><?=$LeadSource->LeadSource?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('LeadSource');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Is Active</label>
														<div class="col-md-8">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="IsActive" id="IsActive" value="1" checked="">
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Notes</label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="Notes" id="Notes" value="<?=isset($ContactData->Notes)?$ContactData->Notes:"";?>">
															<?php echo form_error('Notes');  ?>
														</div>
													</div> 

												</div>
											</div>

											<div class="col-md-12">

												<div class="txt-dark capitalize-font pt-10"><strong>Address Information</strong></div>
												<hr class="light-grey-hr">
												

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-4">Mailing Address</label>
														<div class="col-md-8">
																<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-search"></i>Search Address</button>
														</div>
													</div>
												
													<div class="form-group">
														<label class="col-sm-4 control-label">Mailing Street<span class="required-sign">*</span></label>
														<div class="col-sm-8">
															<input type="text" class="form-control" name="MailingAddress" id="MailingAddress" value="<?=isset($ContactData->MailingAddress)?$ContactData->MailingAddress:"";?>">
															<?php echo form_error('MailingAddress');  ?>
															<input type="hidden" name="MailingLatitude" id="MailingLatitude" value="<?=isset($ContactData->MailingLatitude)?$ContactData->MailingLatitude:"";?>">
															<input type="hidden" name="MailingLongitude" id="MailingLongitude" value="<?=isset($ContactData->MailingLongitude)?$ContactData->MailingLongitude:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Mailing City<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="MailingCity" id="MailingCity" value="<?=isset($ContactData->MailingCity)?$ContactData->MailingCity:"";?>">
															<?php echo form_error('MailingCity');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Mailing State<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="MailingState" id="MailingState" value="<?=isset($ContactData->MailingState)?$ContactData->MailingState:"";?>">
															<?php echo form_error('MailingState');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-4">Mailing Country<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="MailingCountry" id="MailingCountry" value="<?=isset($ContactData->MailingCountry)?$ContactData->MailingCountry:"";?>">
															<?php echo form_error('MailingCountry');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Mailing Postal Code<span class="required-sign">*</span></label>
														<div class="col-md-8">
															<input type="text" class="form-control" name="MailingPostalCode" id="MailingPostalCode" value="<?=isset($ContactData->MailingPostalCode)?$ContactData->MailingPostalCode:"";?>">
															<?php echo form_error('MailingPostalCode');  ?>
														</div>
													</div>
												</div>

											</div>

											<!-- Start Custom Fields Row -->
											<?php $this->load->view('Setup/Customize/Fields/CustomFieldsForm', array('Object' => 'Contact', 'ObjectID' => $ContactData->ContactID)); ?>
											<!-- End Custom Fields Row -->

											<div class="col-md-12">

												<div class="txt-dark capitalize-font pt-10"><strong>System Information</strong></div>
												<hr class="light-grey-hr">
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-4">Create Date</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$ContactData->CreatedDate?>" readonly="">
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-4">Created By</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$ContactData->CreatedByName?>" readonly="">
														</div>
													</div>
												</div>
												<div class="col-md-6">

												   <div class="form-group">
														<label class="control-label col-md-4">Last Modified Date</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$ContactData->LastModifiedDate?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-4">Last Modified By</label>
														<div class="col-md-8">
															<input type="text" class="form-control" value="<?=$ContactData->LastModifiedByName?>" readonly="">
														</div>
													</div>
												</div>
											</div>


										</div>
											<!-- /Row -->
									</div>

									<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="EditContact" id="EditContactBtn">Save</button>
															<a href="<?=SITE_URL;?>Contact/Actions/ContactDetails/<?=$ContactData->ContactID?>" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			</div>
			<div id="profile_13" class="tab-pane fade" role="tabpanel">
				
				<?php $this->load->view('Contact/Include/RelatedInformation'); ?>
				
			</div>
		</div>
		
		</div>

		<!-- <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
			<?php //$this->load->view('Contact/Include/ActivityTimeline'); ?>
		</div> -->

	</div>
	<!-- /Row -->
</div>

<?php //$this->load->view('Contact/Modal/CreateNoteView'); ?>
<?php $this->load->view('Contact/Modal/LocationPickerView'); ?> 
<?php $data = array('data' => array('RelatedTo' => 'Contact', 'What' => $ContactData->ContactID, 'AssignedTo' => $ContactData->AssignedTo));
$this->load->view('Common/Modal/CreateNewFile', $data);
$this->load->view('Common/Modal/CreateNewNote', $data); ?>
<!-- End Modal -->

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<!-- <script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>
 -->
<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Use for common object  -->
<script src="<?=SITE_URL?>dist/js/common.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>
		
<script type="text/javascript">

$(document).ready(function () {

$(document).on('click', "#EditContactLink", function () {
	$('#EditContactBtn').click(); 
});

    $('#EditContact').validate({ 
        rules: {
            Salutation: {
                required: true
            },
            FirstName: {
                required: true
            },
            LastName: {
                required: true
            },
            Title: {
                required: true
            },
            AssignedTo: {
                required: true
            },
            Account: {
                required: true
            },
            LeadSource: {
                required: true
            },
            MailingAddress: {
                required: true
            },
            MailingCity: {
                required: true
            },
            MailingState: {
                required: true
            },
            MailingCountry: {
                required: true
            },
            MailingPostalCode: {
                required: true
            },
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
}); 

function DeleteContact(ContactID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#5fc55f",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>Contact/Actions/DeleteContact/<?=$ContactData->ContactID;?>/";
		}, 500);
    });
	return false;
}
</script>

<!-- Set Location -->
<script src="<?=SITE_URL?>dist/js/location-picker.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	set_location_in_map("Mailing");
});
</script>




