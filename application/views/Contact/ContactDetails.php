<?php $this->load->view('Template/HeaderView'); ?>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10 pb-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['Contact']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['Contact']['title']?></span>
									<span class="panel-title txt-dark"><?=$ContactData->FirstName;?> <?=$ContactData->LastName;?></span>
								</div>
							</div>
						</div>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div  class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="col-lg-7 col-md-5 col-sm-4 col-xs-12">
							<!-- <div class="form-group"> -->
								<ul role="tablist" class="nav nav-pills" id="myTabs_13">
									<li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_13" href="#home_13">Detail</a></li>
									<li role="presentation" class=""><a data-toggle="tab" id="profile_tab_13" role="tab" href="#profile_13" aria-expanded="false">Related</a></li>
								</ul>
							<!-- </div> -->
						</div>

						<div class="col-lg-5 col-md-7 col-sm-8 col-xs-12">
							
							<div class="btn-group pull-right">
									<div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li>
												<a><span><strong>Communication</strong></span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>Common/Actions/SendEmail/Contact/<?=$ContactData->ContactID;?>"><span>Send Email</span></a>
											</li>
											<li>
												<a><span><strong>Add New Actions</strong></span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>WorkOrder/Actions/CreateWorkOrder/?Contact=<?=$ContactData->ContactID;?>&Account=<?=$ContactData->Account;?>"><span>New Work Order</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>Estimate/Actions/CreateEstimate/?Contact=<?=$ContactData->ContactID;?>&Account=<?=$ContactData->Account;?>"><span>New Estimate</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>Invoice/Actions/CreateInvoice/?Contact=<?=$ContactData->ContactID;?>&Account=<?=$ContactData->Account;?>"><span>New Invoice</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>Event/Actions/CreateEvent/?Contact=<?=$ContactData->ContactID;?>"><span>New Event</span></a>
											</li>
											<li>
												<a href="<?=SITE_URL;?>Task/Actions/CreateTask/?Contact=<?=$ContactData->ContactID;?>"><span>New Task</span></a>
											</li>
											<li> 
												<a href="JavaScript:void(0);" data-toggle="modal" data-target="#CreateNewFile"><span>New File</span></a>
											</li>
											<li>
												<a href="#" data-toggle="modal" data-target="#CreateNewNote"><span>New Note</span></a>
											</li>
											<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
												<a><span><strong>Contact Actions</strong></span></a>
											</li>
											<li> 
												<a href="<?=SITE_URL;?>Contact/Actions/CreateContact/<?=$ContactData->ContactID?>/"><span>Copy Contact</span></a>
											</li>
											<li>
												<a href="#" onclick="DeleteContact();"><span>Delete Contact</span></a>
											</li>
										</ul>
									</div>
								</div>
								<div class="btn-group pull-right">
									<a href="<?=SITE_URL?>Contact/Actions/EditContact/<?=$ContactData->ContactID;?>" class="btn btn-primary btn-outline">Edit</a>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
			 	<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Contact Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>  -->
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Contact/Actions/EditContact/<?=$ContactData->ContactID?>" class="form-horizontal" id="EditContact" method="post">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>Contact Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Assigned To
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$ContactData->AssignedTo?>"><?=$ContactData->AssignedToName; ?></a>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
														 Name
														</div>
														<div class="col-md-8">
															<?=$ContactData->FirstName;?> <?=$ContactData->LastName;?>
														</div>
													</div>
												
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Title
														</div>
														<div class="col-md-8">
															<?=$ContactData->Title;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Email
														</div>
														<div class="col-md-8">
															<?=$ContactData->Email;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Phone #
														</div>
														<div class="col-md-8">
															<?=$ContactData->PhoneNo;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Mobile #
														</div>
														<div class="col-md-8">
															<?=$ContactData->MobileNo;?>
														</div>
													</div>

													<div class="form-group pt-5">
														<div class="col-md-4 text-left txt-dark">
															Do Not Call
														</div>
														<div class="col-md-8">
															<div class="checkbox checkbox-primary pt-0 pb-0">
																<input id="checkbox12" disabled="" <?=$ContactData->DoNotCall==1?'checked':'';?> type="checkbox">
																<label for="checkbox12"></label>
															</div>
														</div>
													</div>
													
												</div>

												<div class="col-md-6">

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Account
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Account/Actions/AccountDetails/<?=$ContactData->Account;?>"><?=$ContactData->AccountName;?></a>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Birth Date
														</div>
														<div class="col-md-8">
															<?=$ContactData->BirthDate;?>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Lead Source
														</div>
														<div class="col-md-8">
															<?=$ContactData->LeadSourceName;?>
														</div>
													</div>

													<div class="form-group pt-5">
														<div class="col-md-4 text-left txt-dark">
															Is Active
														</div>
														<div class="col-md-8">
															<div class="checkbox checkbox-primary pt-0 pb-0">
																<input id="checkbox12" disabled="" <?=$ContactData->IsActive==1?'checked':'';?> type="checkbox">
																<label for="checkbox12"></label>
															</div>
														</div>
													</div>

													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Notes
														</div>
														<div class="col-md-8" style="word-wrap: break-word;">
															<?=$ContactData->Notes;?>
														</div>
													</div>

												</div>
											</div>

											<div class="col-md-12">
											<div class="txt-dark capitalize-font pt-10"><strong>Address Information</strong></div>
											<hr class="light-grey-hr">
												

												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Mailing Address
														</div>
														<div class="col-md-8">
															<div><?=$ContactData->MailingAddress;?></div>
															<div><?=$ContactData->MailingCity;?>, <?=$ContactData->MailingState;?> <?=$ContactData->MailingPostalCode;?></div>
															<div><?=$ContactData->MailingCountry;?></div>
														</div>
													</div>
												</div>

											</div>

											<!-- Start Custom Fields Row -->
											<?php $this->load->view('Setup/Customize/Fields/CustomFieldsView', array('Object' => 'Contact', 'ObjectID' => $ContactData->ContactID)); ?>
											<!-- End Custom Fields Row -->

											<div class="col-md-12">

												<div class="txt-dark capitalize-font pt-10"><strong>System Information</strong></div>
												<hr class="light-grey-hr">
												
												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Create Date
														</div>
														<div class="col-md-8">
															<?=$ContactData->CreatedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Created By
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$ContactData->CreatedBy?>"><?=$ContactData->CreatedByName;?></a>
														</div>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified Date
														</div>
														<div class="col-md-8">
															<?=$ContactData->LastModifiedDate;?>
														</div>
													</div>
													<div class="form-group pt-5 pb-5">
														<div class="col-md-4 text-left txt-dark">
															Last Modified By
														</div>
														<div class="col-md-8">
															<a href="<?=SITE_URL;?>Setup/Administration/Actions/UserDetails/<?=$ContactData->LastModifiedBy?>"><?=$ContactData->LastModifiedByName;?></a>
														</div>
													</div>
												</div>
											</div>


										</div>
											<!-- /Row -->
									</div>

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		
			</div>
			<div id="profile_13" class="tab-pane fade" role="tabpanel">

				<?php $this->load->view('Contact/Include/RelatedInformation'); ?>
				
			</div>
		</div>
		
		</div>
 
		<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">

			<?php //$this->load->view('Contact/Include/ActivityTimeline'); ?>
			
		</div>

	</div>
	<!-- /Row -->
</div>

<?php //$this->load->view('Contact/Modal/CreateNoteView'); ?>
<?php $this->load->view('Contact/Modal/SendEmailView'); ?>
<?php $data = array('data' => array('RelatedTo' => 'Contact', 'What' => $ContactData->ContactID, 'AssignedTo' => $ContactData->AssignedTo));
$this->load->view('Common/Modal/CreateNewFile', $data); 
$this->load->view('Common/Modal/CreateNewNote', $data); ?>

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<!-- Use for common object  -->
<script src="<?=SITE_URL?>dist/js/common.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">
function DeleteContact(ContactID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#5fc55f",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>Contact/Actions/DeleteContact/<?=$ContactData->ContactID;?>/";
		}, 500);
    });
	return false;
}
</script>


