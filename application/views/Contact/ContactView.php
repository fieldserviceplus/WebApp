
<?php $this->load->view('Template/HeaderView'); ?>
 
<style type="text/css">.dataTables_filter { display: none; }</style>
    <div class="container-fluid pt-25">

        <!-- Row -->
	    <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-default card-view">
					<div  class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-lg-3 col-md-3 col-sm-5 col-xs-12 pr-0 pl-0">
								
								<div class="form-group">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
										<label class="control-label mt-10"><strong>View:</strong></label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 pr-0">
									<form method="post" action="<?=SITE_URL?>Contact/View/" id="SelectedAcForm">
										<select class="selectpicker" name="SelectedContact" data-style="form-control btn-default btn-outline" onchange="ViewContacts(this.value);">
											<option disabled="" selected="">Select Views</option>
											<optgroup label="Default Views">
											<option value="MyActiveContacts" <?=(isset($SelectedContact) && $SelectedContact=='MyActiveContacts')?"selected":""?>>My Active Contacts</option>
											<option value="AllActiveContacts" <?=(isset($SelectedContact) && $SelectedContact=='AllActiveContacts')?"selected":""?>>All Active Contacts</option>
											<option value="ContactsCreatedThisWeek" <?=(isset($SelectedContact) && $SelectedContact=='ContactsCreatedThisWeek')?"selected":""?>>Contacts Created This Week</option>
											</optgroup>
											<optgroup label="My Custom Views">
												<?php $Views = $this->ViewModel->GetViews();
												foreach ($Views as $View) { ?>
												<option value="<?=$View->ContactViewID?>"><?=$View->ContactViewName?></option>
											<?php } ?>
											</optgroup>
										</select>
										</form>
									</div>
								</div>	
								
							</div>	
							<div class="col-lg-5 col-md-6 col-sm-7 col-xs-12">
								<div class="form-group" >
								<!-- <div class="btn-group">
									<button class="btn btn-primary btn-outline">Go</button>
								</div> -->
								<div class="btn-group">
									<div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button"> <i class="fa fa-pencil"></i> <span class="caret"></span></button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li>
												<a><span><strong>LIST VIEW ACTIONS</strong></span></a>
											</li>
											<li> 
												<a href="<?=SITE_URL?>Contact/View/CreateNewView"><span>Create New View</span></a>
											</li>
										</ul>
									</div>
								</div>
								<div class="btn-group">
									<!-- <button class="btn btn-default btn-outline" data-toggle="modal" data-target="#exampleModal" title="Edit Filters"><i class="fa fa-filter"></i></button> -->
									<button class="btn btn-default btn-outline" title="Refresh" ><i class="fa fa-refresh"></i></button>
									<button class="btn btn-default btn-outline" title="Export" id="Export-Excel"><i class="fa fa-file-excel-o"></i></button>
								</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-1 col-sm-6 col-xs-12">
								<div class="pull-right">
									<div class="form-group">
										<a href="<?=SITE_URL?>Contact/Actions/CreateContact" class="btn btn-primary btn-outline">New Contact</a>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
								<div class="pull-right">
									<div class="input-group">
										<input id="accountglobalfilter" class="form-control accountglobalfilter" placeholder="Search" type="text">
										<span class="input-group-addon"><i class="zmdi zmdi-search"></i></span> 
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">

								<div class="table-responsive">
								<!-- 	<table id="accounts" class="table table-hover display  pb-30" width="100%">
										<thead>
											<tr>
												<th class="text-center">Edit</th>
												<th class="text-center">Assigned To</th>
												<th class="text-center">Contact Name</th>
												<th class="text-center">Account</th>
												<th class="text-center">Phone No</th>
												<th class="text-center">Mailing City</th>
												<th class="text-center">Mailing State</th>
												<th class="text-center">Birth Date</th>
												<th class="text-center">Notes</th>
												<th class="text-center">Description</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table> -->
									<table id="accountsweek" class="table table-hover display  pb-30" width="100%">
										<thead>
											<tr>
												<th class="text-center">Edit</th>
												<th class="text-center">Assigned To</th>
												<th class="text-center">Contact Name</th>
												<th class="text-center">Account</th>
												<th class="text-center">Phone No</th>
												<th class="text-center">Mailing City</th>
												<th class="text-center">Mailing State</th>
												<th class="text-center">Birth Date</th>
												<th class="text-center">Notes</th>
												<th class="text-center">Created Date</th>
												<th class="text-center">Created By</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->

	</div>	

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<form class="form-horizontal" action="<?=SITE_URL?>Contact/View/ViewResult" method="post">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">View Filters</h5>
			</div>
			<div class="modal-body">
				
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
						    
						    <?php 
						    /*foreach($ContactFilters as $ContactFilter){ ?>
						    <div class="form-group">
						    	<div class="col-md-3">
									<select class="form-control" name="FilterFields[]">
										<option value="ContactName" <?=$ContactFilter->FilterField=='ContactName'?"Selected":""?> >Ac Name</option>
										<option value="CityName" <?=$ContactFilter->FilterField=='CityName'?"Selected":""?> >City</option>
										<option value="StateName" <?=$ContactFilter->FilterField=='StateName'?"Selected":""?> >State</option>
										<option value="Type" <?=$ContactFilter->FilterField=='Type'?"Selected":""?> >Type</option>
									</select>
								</div>
								<div class="col-md-4">
									<select class="form-control" name="FilterConditions[]">
										<option value="Equals" <?=$ContactFilter->FilterCondition=='Equals'?"Selected":""?> >Equals</option>
										<option value="Contains" <?=$ContactFilter->FilterCondition=='Contains'?"Selected":""?> >Contains</option>
										<option value="StartsWith" <?=$ContactFilter->FilterCondition=='StartsWith'?"Selected":""?> >Starts With</option>
										<option value="DoesNotContain" <?=$ContactFilter->FilterCondition=='DoesNotContain'?"Selected":""?> >Does Not Contain</option>
									</select>
								</div>
								<div class="col-md-5">
									<input type="text" class="form-control" name="FilterValues[]" value="<?=$ContactFilter->FilterValue?>" required>
								</div>
							</div>
							<?php }*/ ?>
							
						</div>
						<button type="button" class="btn btn-default btn-outline add_field_button">Add Filter +</button>
					</div>
				</div>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="FilterForm">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>



<?php $this->load->view('Template/FooterView')?>


<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/jszip/dist/jszip.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<script type="text/javascript">
 
 	function ViewContacts(flag){ 

 		if(flag != 'MyActiveContacts' && flag != 'AllActiveContacts' && flag != 'ContactsCreatedThisWeek'){ 
 			$('#SelectedAcForm').submit();
 		}

 		/*if(flag == 'ContactsCreatedThisWeek'){ */

 			//$("#accounttable thead tr").append('<th class="text-center set-flag">Created Date</th><th class="text-center set-flag">Created By</th>');

 			var tbl = 'accountsweek';

 			$('#accounts,.accounts').hide();
 			$('#accountsweek,.accountsweek').show();

 			var colarr = [
				{ className: "text-center" },
	            { className: "text-center" },
	            { className: "text-left" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center set-flag" },
	            { className: "text-center set-flag" },
	         ];
 		/*} else {

 			var tbl = 'accounts';

 			$('#accountsweek,.accountsweek').hide();
 			$('#accounts,.accounts').show();

 			//$(".set-flag").remove();

 			var colarr = [
				{ className: "text-center" },
				{ className: "text-center" },
	            { className: "text-center" },
	            { className: "text-left" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" },
	            { className: "text-center" }
	         ];
	       
 		}*/

		var dataTable = $('#'+tbl).DataTable({
			 destroy: true,
	       	"responsive": true,
	       	"paging": true,
	       	"bDestroy": true,
	       	"processing": true, //Feature control the processing indicator.
	        "serverSide": true, //Feature control DataTables' server-side processing mode.
	        "sAjaxSource": "<?=SITE_URL?>Contact/View/ViewContacts?flag="+flag,
	       	"columnDefs": [ {
		        sortable: false,
		        targets: 'no-sort',
		        class: "index",
		        targets: 0
	       	} ],
	       	"aaSorting": [],
	       	"order": [[ 1, 'asc' ]],
	       	"fixedColumns": true, 
	       	"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 ] }],
	       	"columns": colarr, 
	        "dom": 'Bfrt<"bottom '+tbl+'"ilp><"clear">',
			buttons: [
				'excel'
			],
			lengthMenu: [
				[ 10, 25, 50, -1 ],
				[ '10', '25', '50', 'All' ]
			]
	    });
		$('.buttons-excel').hide();
	    $('#accountglobalfilter').keyup(function(){ 
	    	var dataTable = $('#'+tbl).DataTable();
	      	dataTable.search($(this).val()).draw() ;
	    }); 
    }

    $(document).ready(function(){

    	<?php if(isset($SelectedContact) && !empty($SelectedContact)){ ?>
    		ViewContacts('<?=$SelectedContact?>');
    	<?php } else {?>
			ViewContacts('MyActiveContacts');	
    	<?php } ?>
	    
 	});

 	function GoToNewView()
 	{
 		$('#SelectedAcForm').submit();
 	}

 	$(document).ready(function() {
	$('.buttons-excel').hide();
	$("#Export-Excel").on("click", function() {
	    $( '.buttons-excel' ).click();
	});
});
</script>



