
<!-- modal --> 
<div class="modal fade" id="send-email-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog modal-lg" role="document" >
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Send Email</h5>
			</div>
			<form class="form-horizontal" id="SendEmailForm" method="post" action="<?=SITE_URL;?>Contact/Actions/SendEmail/<?=$ContactData->ContactID?>">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-2">Subject</label>
							<div class="col-md-10">
								<input type="text" name="Subject" class="form-control">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-2">Body</label>
							<div class="col-md-10">
								<textarea class="form-control" id="CKEditor1" name="Body"></textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-2">Attach File</label>
							<div class="col-md-10">
								<div action="#" class="dropzone" id="my-awesome-dropzone">
									<div class="fallback">
										<input name="file" type="file" multiple />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer" style="text-align: center;">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="CreateNote">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>

