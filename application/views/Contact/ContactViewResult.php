
<?php $this->load->view('Template/HeaderView')?>
 
<style type="text/css">.dataTables_filter { display: none; }</style>
    <div class="container-fluid pt-25">

        <!-- Row -->
	    <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-default card-view">
					<div  class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-lg-3 col-md-3 col-sm-5 col-xs-12 pr-0 pl-0">
								<div class="form-group">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
										<label class="control-label mt-10"><strong>View:</strong></label>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 pr-0">
										<select class="form-control" data-style="form-control btn-default btn-outline">
											<option>My Contacts</option>
											<option>Contacts Created This Week</option>
											<option>Customer Contacts</option>
											<option>Customer Contacts - Active</option>
										</select>
									</div>
								</div>	
							</div>	
							<div class="col-lg-5 col-md-6 col-sm-7 col-xs-12">
								<div class="form-group" >
								<div class="btn-group">
									<button class="btn btn-primary btn-outline">Go</button>
								</div>
								<div class="btn-group">
									<div class="dropdown">
										<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button"> <i class="fa fa-pencil"></i> <span class="caret"></span></button>
										<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
											<li>
												<a><span>LIST VIEW ACTIONS</span></a>
											</li>
											<li>
												<a href="#"><span>New View</span></a>
											</li>
											<li>
												<a href="#"><span>Copy</span></a>
											</li>
											<li>
												<a href="#"><span>Rename</span></a>
											</li>
											<li>
												<a href="#"><span>Edit Sharing</span></a>
											</li>
											<li>
												<a href="#"><span>Edit Filters</span></a>
											</li>
											<li>
												<a href="#"><span>Edit Displayed Columns</span></a>
											</li>
											<li>
												<a href="#"><span>Delete View</span></a>
											</li>
										</ul>
									</div>
								</div>
								<div class="btn-group">
									<button class="btn btn-default btn-outline" data-toggle="modal" data-target="#exampleModal" title="Edit Filters"><i class="fa fa-filter"></i></button>
									<button class="btn btn-default btn-outline" title="Refresh" ><i class="fa fa-refresh"></i></button>
									<button class="btn btn-default btn-outline" title="Export"><i class="fa fa-file-excel-o"></i></button>
								</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-1 col-sm-6 col-xs-12">
								<div class="pull-right">
									<div class="form-group">
										<a href="create_new_account.php" class="btn btn-primary btn-outline">New Contact</a>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
								<div class="pull-right">
									<div class="input-group">
										<input id="accountglobalfilter" class="form-control accountglobalfilter" placeholder="Search" type="text">
										<span class="input-group-addon"><i class="zmdi zmdi-search"></i></span> 
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">

								<div class="table-responsive">
									<table id="myTable1" class="table table-hover display  pb-30" width="100%">
										<thead>
											<tr>
												<th class="text-center">Assigned To</th>
												<th class="text-center">Contact Name</th>
												<th class="text-center">Contact Type</th>
												<th class="text-center">Phone No</th>
												<th class="text-center">Billing City</th>
												<th class="text-center">BIlling State</th>
												<th class="text-center">Last Service Date</th>
												<th class="text-center">Last Activity Date</th>
												<th class="text-center">Access Notes</th>
												<th class="text-center">Preferred Technician</th>
												<th class="text-center">PopUp Reminder</th>
											</tr>
										</thead>
										<tbody>
											<?php 
											foreach ($FilteredContacts as $key => $FilteredContact) { ?>
												<tr>
													<td class="text-center"><?=$FilteredContact->OwnerName?></td>
													<td class="text-left"><?=$FilteredContact->ContactName?></td>
													<td class="text-center"><?=$FilteredContact->ContactType?></td>
													<td class="text-center"><?=$FilteredContact->PhoneNo?></td>
													<td class="text-center"><?=$FilteredContact->BillingCity?></td>
													<td class="text-center"><?=$FilteredContact->BillingState?></td>
													<td class="text-center"><?=$FilteredContact->LastServiceDate?></td>
													<td class="text-center"><?=$FilteredContact->LastActivityDate?></td>
													<td class="text-center"><?=$FilteredContact->AccessNotes?></td>
													<td class="text-center"><?=$FilteredContact->PreferredTechnician?></td>
													<td class="text-center"><?=$FilteredContact->PopUpReminder?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->

	</div>	

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<form class="form-horizontal" action="<?=SITE_URL?>Contact/View/ViewResult" method="post">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">View Filters</h5>
			</div>
			<div class="modal-body">
				
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
						    
						    <?php 
						    foreach($ContactFilters as $ContactFilter){ ?>
						    <div class="form-group">
						    	<div class="col-md-3">
									<select class="form-control" >
										<option <?=$ContactFilter->FilterField=='ContactName'?"Selected":""?> >Ac Name</option>
										<option <?=$ContactFilter->FilterField=='CityName'?"Selected":""?> >City</option>
										<option <?=$ContactFilter->FilterField=='StateName'?"Selected":""?> >State</option>
										<option <?=$ContactFilter->FilterField=='Type'?"Selected":""?> >Type</option>
									</select>
								</div>
								<div class="col-md-4">
									<select class="form-control" >
										<option <?=$ContactFilter->FilterCondition=='Equals'?"Selected":""?> >Equals</option>
										<option <?=$ContactFilter->FilterCondition=='Contains'?"Selected":""?> >Contains</option>
										<option <?=$ContactFilter->FilterCondition=='StartsWith'?"Selected":""?> >Starts With</option>
										<option <?=$ContactFilter->FilterCondition=='DoesNotContain'?"Selected":""?> >Does Not Contain</option>
									</select>
								</div>
								<div class="col-md-5">
									<input type="text" class="form-control" value="<?=$ContactFilter->FilterValue?>" required>
								</div>
							</div>
							<?php } ?>
							
						</div>
						<button type="button" class="btn btn-default btn-outline add_field_button">Add Filter +</button>
					</div>
				</div>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" name="FilterForm">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<script type="text/javascript">
 
 	function ViewContacts(flag){

		//var dataTable = $('#myTable1').DataTable();
    }

    /*$(document).ready(function(){

    	ViewContacts('MyContacts');
	    $('#accountglobalfilter').keyup(function(){
	    	var dataTable = $('#accounttable').DataTable();
	      	dataTable.search($(this).val()).draw() ;
	    }); 
 	});

 	function GoToNewView()
 	{
 		$('#SelectedAcForm').submit();
 	}*/
</script>


