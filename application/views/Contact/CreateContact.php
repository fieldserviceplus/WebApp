<?php $this->load->view('Template/HeaderView')?>

<script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_JS_KEY?>&libraries=places"></script>
 
<div class="container-fluid pt-25">

	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Create Contact</h6>
					</div>
					<div class="pull-right">
						<span style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong></span>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>Contact/Actions/CreateContact" class="form-horizontal" id="CreateContact" method="post">
										<div class="form-body">

										<?php if(isset($_GET['Account']) && !empty($_GET['Account'])){ ?>
											<input type="hidden" name="Redirect" value="Account">
										<?php } ?>
									
											<!-- /Row -->
											<div class="row">

												<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>Contact Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Assigned To<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="AssignedTo" id="AssignedTo" class="form-control select2">
														<option value="">Select Assigned To</option>
														<?php $Users = $this->ActionsModel->GetUsers();
															foreach ($Users as $User) { 
																if(isset($ContactData->AssignedTo) && $ContactData->AssignedTo == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else { ?>
																	<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('AssignedTo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Name<span class="required-sign">*</span></label>
														<div class="col-md-2">
															<select name="Salutation" id="Salutation" class="form-control">
																<?php $Salutations = $this->ActionsModel->GetSalutation();
																	foreach ($Salutations as $Salutation) { 
																		if(isset($ContactData->Salutation) && $ContactData->Salutation == $Salutation->Salutation){ ?>
																			<option value="<?=$Salutation->Salutation?>" selected="selected"><?=$Salutation->Salutation?></option>
																		<?php } else { ?>
																			<option value="<?=$Salutation->Salutation?>"><?=$Salutation->Salutation?></option>
																		<?php } ?>
																	<?php } ?>
															</select>
															<?php echo form_error('Salutation');  ?>
														</div>
														<div class="col-md-4">
															<input type="text" class="form-control" name="FirstName" id="FirstName" value="<?=isset($ContactData->FirstName)?$ContactData->FirstName:"";?>" placeholder="First Name">
															<?php echo form_error('FirstName');  ?>
														</div>
														<div class="col-md-3">
															<input type="text" class="form-control" name="LastName" id="LastName" value="<?=isset($ContactData->LastName)?$ContactData->LastName:"";?>" placeholder="Last Name">
															<?php echo form_error('LastName');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Title<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Title" id="Title" value="<?=isset($ContactData->Title)?$ContactData->Title:"";?>">
															<?php echo form_error('Title');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Email</label>
														<div class="col-md-9">
															<input type="email" class="form-control" name="Email" id="Email" value="<?=isset($ContactData->Email)?$ContactData->Email:"";?>">
															<?php echo form_error('Email');  ?>
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-3">Phone #</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="PhoneNo" id="PhoneNo" value="<?=isset($ContactData->PhoneNo)?$ContactData->PhoneNo:"";?>">
															<?php echo form_error('PhoneNo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Mobile #</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="MobileNo" id="MobileNo" value="<?=isset($ContactData->MobileNo)?$ContactData->MobileNo:"";?>">
															<?php echo form_error('MobileNo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Do Not Call</label>
														<div class="col-md-9">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="DoNotCall" id="DoNotCall" value="1">
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

												</div>

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Account<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="Account" id="Account" class="form-control select2">
														<option value="">Select Account</option>
														<?php $Accounts = $this->ActionsModel->GetAccounts();
															foreach ($Accounts as $Account) { 
																if(isset($ContactData->Account) && ($ContactData->Account == $Account->AccountID) || (isset($_GET['Account']) && $_GET['Account'] == $Account->AccountID)){ ?>
																	<option value="<?=$Account->AccountID?>" selected="selected"><?=$Account->AccountName?></option>
																<?php } else { ?>
																	<option value="<?=$Account->AccountID?>"><?=$Account->AccountName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('Account');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Birth Date</label>
														<div class="col-md-9">
															<input type="text" class="form-control showdatepicker" name="BirthDate" id="BirthDate" value="<?=isset($ContactData->BirthDate)?$ContactData->BirthDate:"";?>">
															<?php echo form_error('BirthDate');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Lead Source<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="LeadSource" id="LeadSource" class="form-control">
														<option value="">Select Lead Source</option>
														<?php $LeadSources = $this->ActionsModel->GetLeadSources();
															foreach ($LeadSources as $LeadSource) { 
																if(isset($ContactData->LeadSource) && $ContactData->LeadSource == $LeadSource->LeadSourceID){ ?>
																	<option value="<?=$LeadSource->LeadSourceID?>" selected="selected"><?=$LeadSource->LeadSource?></option>
																<?php } else { ?>
																	<option value="<?=$LeadSource->LeadSourceID?>"><?=$LeadSource->LeadSource?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('LeadSource');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Is Active</label>
														<div class="col-md-9">
															<div class="checkbox checkbox-primary">
																<input type="checkbox" name="IsActive" id="IsActive" value="1" checked="">
																<label for="checkbox2"></label>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Notes</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Notes" id="Notes" value="<?=isset($ContactData->Notes)?$ContactData->Notes:"";?>">
															<?php echo form_error('Notes');  ?>
														</div>
													</div> 
											
												</div>
												<!--/span-->
												</div>

												<div class="col-md-12">
												<div class="txt-dark capitalize-font"><strong>Address Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Mailing Address</label>
														<div class="col-md-9">
																<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-search"></i>Search Address</button>
														</div>
													</div>
												
													<div class="form-group">
														<label class="col-sm-3 control-label">Mailing Street<span class="required-sign">*</span></label>
														<div class="col-sm-9">
															<input type="text" class="form-control" name="MailingAddress" id="MailingAddress" value="<?=isset($ContactData->MailingAddress)?$ContactData->MailingAddress:"";?>">
															<?php echo form_error('MailingAddress');  ?>
															<input type="hidden" name="MailingLatitude" id="MailingLatitude" value="<?=isset($ContactData->MailingLatitude)?$ContactData->MailingLatitude:"";?>">
															<input type="hidden" name="MailingLongitude" id="MailingLongitude" value="<?=isset($ContactData->MailingLongitude)?$ContactData->MailingLongitude:"";?>">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Mailing City<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="MailingCity" id="MailingCity" value="<?=isset($ContactData->MailingCity)?$ContactData->MailingCity:"";?>">
															<?php echo form_error('MailingCity');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Mailing State<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="MailingState" id="MailingState" value="<?=isset($ContactData->MailingState)?$ContactData->MailingState:"";?>">
															<?php echo form_error('MailingState');  ?>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-md-3">Mailing Country<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="MailingCountry" id="MailingCountry" value="<?=isset($ContactData->MailingCountry)?$ContactData->MailingCountry:"";?>">
															<?php echo form_error('MailingCountry');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Mailing Postal Code<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="MailingPostalCode" id="MailingPostalCode" value="<?=isset($ContactData->MailingPostalCode)?$ContactData->MailingPostalCode:"";?>">
															<?php echo form_error('MailingPostalCode');  ?>
														</div>
													</div>
												</div>

											</div>
											
										<!-- Start Custom Fields Row -->
										<?php $this->load->view('Setup/Customize/Fields/CustomFieldsForm', array('Object' => 'Contact', 'ObjectID' => 0)); ?>
										<!-- End Custom Fields Row -->

										<!-- /Row -->
										</div>

										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="CreateContact">Save</button>
															<a href="<?=SITE_URL;?>Contact/View" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->

<?php $this->load->view('Contact/Modal/LocationPickerView')?>

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>

<script type="text/javascript">
$(document).ready(function () {
 
    $('#CreateContact').validate({ 
        rules: {
            Salutation: {
                required: true
            },
            FirstName: {
                required: true
            },
            LastName: {
                required: true
            },
            Title: {
                required: true
            },
            AssignedTo: {
                required: true
            },
            Account: {
                required: true
            },
            LeadSource: {
                required: true
            },
            MailingAddress: {
                required: true
            },
            MailingCity: {
                required: true
            },
            MailingState: {
                required: true
            },
            MailingCountry: {
                required: true
            },
            MailingPostalCode: {
                required: true
            },
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    }); 
});
</script>
<!-- Set Location -->
<script src="<?=SITE_URL?>dist/js/location-picker.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	set_location_in_map("Mailing");
});
</script>




