<div class="panel panel-default card-view"> 
	 <div class="panel-heading">
		<div class="pull-left">
			<h6 class="panel-title txt-dark">Related Information</h6>
		</div>
		<div class="clearfix"></div>
	</div> 
	<div class="panel-wrapper collapse in">
		<div class="panel-body">

		<div class="row">
			
			<div class="col-md-12">
				<div class="clearfix"></div>
				
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
						<div class="txt-dark capitalize-font mt-20"><strong>Work Orders</strong></div>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<a href="<?=SITE_URL?>WorkOrder/Actions/CreateWorkOrder/?Contact=<?=$ContactData->ContactID?>" class="btn btn-primary btn-outline pull-right mb-10 mt-15"><i class="fa fa-plus"></i> New Work Order</a>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="table-wrap">
				<div class="table-responsive">
				<table id="WorkOrdersTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>WO # </th>
							<th>Subject</th>
							<th>Type</th>
							<th>Date</th>
							<th>Status</th>
							<th>Priority</th>
							<th>Assigned To</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($WorkOrders as $key => $WorkOrder) { ?>
							<tr>
								<td><a href="<?=SITE_URL?>WorkOrder/Actions/WorkOrderDetails/<?=$WorkOrder->WorkOrderID?>"><?=$WorkOrder->WorkOrderNo?></a></td>
								<td><a href="<?=SITE_URL?>WorkOrder/Actions/WorkOrderDetails/<?=$WorkOrder->WorkOrderID?>"><?=$WorkOrder->Subject?></a></td>
								<td><?=$WorkOrder->WorkOrderType?></td>
								<td><?=$WorkOrder->CreatedDate?></td>
								<td><?=$WorkOrder->Status?></td>
								<td><?=$WorkOrder->Priority?></td>
								<td><a href="#"><?=$WorkOrder->AssignedToName?></a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				</div>
				<?php if(count($WorkOrders) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Contact/Actions/RelatedObject/<?=$ContactData->ContactID;?>/WorkOrders" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
				<?php } ?>
				</div>
			</div>

			<div class="col-md-12">
				<div class="clearfix"></div>
				
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
						<div class="txt-dark capitalize-font mt-20"><strong>Estimates</strong></div>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<a href="<?=SITE_URL;?>Estimate/Actions/CreateEstimate/?Contact=<?=$ContactData->ContactID?>" class="btn btn-primary btn-outline pull-right mb-10 mt-15"><i class="fa fa-plus"></i> New Estimate</a>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="table-wrap">
				<div class="table-responsive">
				<table id="EstimatesTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>ET # </th>
							<th>Estimate Name</th>
							<th>Expiration Date</th>
							<th>Status</th>
							<th>Grand Total</th>
							<th>Owner</th>
							<th>Created Date</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($Estimates as $key => $Estimate) { ?>
							<tr>
								<td><a href="<?=SITE_URL?>Estimate/Actions/EstimateDetails/<?=$Estimate->EstimateID?>"><?=$Estimate->EstimateNo?></a></td>
								<td><a href="<?=SITE_URL?>Estimate/Actions/EstimateDetails/<?=$Estimate->EstimateID?>"><?=$Estimate->EstimateName?></a></td>
								<td><?=$Estimate->ExpirationDate?></td>
								<td><?=$Estimate->Status?></td>
								<td><?=$Estimate->GrandTotal?></td>
								<td><a href="#"><?=$Estimate->Owner?></a></td>
								<td><?=$Estimate->CreatedDate?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				</div>
				<?php if(count($Estimates) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Contact/Actions/RelatedObject/<?=$ContactData->ContactID;?>/Estimates" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
				<?php } ?>
				</div>
			</div>

			<div class="col-md-12">
				<div class="clearfix"></div>
				
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
						<div class="txt-dark capitalize-font mt-20"><strong>Invoices</strong></div>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<button type="button" class="btn btn-primary btn-outline pull-right mb-10 mt-15"><i class="fa fa-plus"></i> New Invoice</button>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="table-wrap">
				<div class="table-responsive">
				<table id="InvoicesTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>INV # </th>
							<th>Invoice Date</th>
							<th>Due Date</th>
							<th>Status</th>
							<th>Sub Total</th>
							<th>Total Price</th>
							<th>Work Order</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($Invoices as $key => $Invoice) { ?>
							<tr>
								<td><a href="#"><?=$Invoice->InvoiceNo?></a></td>
								<td><?=$Invoice->InvoiceDate?></td>
								<td><?=$Invoice->DueDate?></td>
								<td><?=$Invoice->InvoiceStatus?></td>
								<td><?=$Invoice->SubTotal?></td>
								<td><?=$Invoice->TotalPrice?></td>
								<td><a href="#"><?=$Invoice->WorkOrderName?></a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				</div>
				<?php if(count($Invoices) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Contact/Actions/RelatedObject/<?=$ContactData->ContactID;?>/Invoices" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
				<?php } ?>
				</div>
			</div>

			<div class="col-md-12">
				<div class="clearfix"></div>
				
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
						<div class="txt-dark capitalize-font mt-20"><strong>Events</strong></div>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<button type="button" class="btn btn-primary btn-outline pull-right mb-10 mt-15" data-toggle="modal" data-target="#CreateNewEvent"><i class="fa fa-plus" ></i> New Event</button>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="table-wrap">
				<div class="table-responsive">
				<table id="EventsTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>Subject </th>
							<th>Start Date</th>
							<th>End Date</th>
							<th>Assigned To</th>
							<th>Name</th>
							<th>Created Date</th>
							<th>Created By</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($Events as $key => $Event) { ?>
							<tr>
								<td><a href="#"><?=$Event->Subject?></a></td>
								<td><?=$Event->EventStartDate?></td>
								<td><?=$Event->EventEndDate?></td>
								<td><a href="#"><?=$Event->AssignedTo?></a></td>
								<td><a href="#"><?=$Event->Name?></a></td>
								<td><?=$Event->CreatedDate?></td>
								<td><a href="#"><?=$Event->CreatedBy?></a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				</div>
				<?php if(count($Events) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Contact/Actions/RelatedObject/<?=$ContactData->ContactID;?>/Events" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
				<?php } ?>
				</div>
			</div>

			<div class="col-md-12">
				<div class="clearfix"></div>
				
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
						<div class="txt-dark capitalize-font mt-20"><strong>Tasks</strong></div>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<button type="button" class="btn btn-primary btn-outline pull-right mb-10 mt-15" data-toggle="modal" data-target="#CreateNewTask"><i class="fa fa-plus" ></i> New Task</button>
					</div>
				</div> 
				<div class="clearfix"></div>
				<div class="table-wrap">
				<div class="table-responsive">
				<table id="TasksTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>Subject </th>
							<th>Name </th>
							<th>Type </th>
							<th>Assigned To </th>
							<th>Date</th>
							<th>Status</th>
							<th>Priority</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($Tasks as $key => $Task) { ?>
							<tr>
								<td><a href="#"><?=$Task->Subject?></a></td>
								<td><a href="#"><?=$Task->Name?></a></td>
								<td><?=$Task->TaskType?></td>
								<td><a href="#"><?=$Task->AssignedTo?></a></td>
								<td><?=$Task->DueDate?></td>
								<td><?=$Task->TaskStatus?></td>
								<td><?=$Task->Priority?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				</div>
				<?php if(count($Tasks) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Contact/Actions/RelatedObject/<?=$ContactData->ContactID;?>/Tasks" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
				<?php } ?>
				</div>
			</div> 

			<div class="col-md-12">
				<div class="clearfix"></div>
				
				<div class="col-md-12 pl-0 pr-0">
					<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
						<div class="txt-dark capitalize-font mt-20"><strong>Files</strong></div>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
						<button type="button" class="btn btn-primary btn-outline pull-right mb-10 mt-15"><i class="fa fa-plus"></i> New File</button>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="table-wrap">
				<div class="table-responsive">
				<table id="FilesTblEAD" class="table table-hover display table-bordered" width="99%">
					<thead>
						<tr>
							<th>File Name </th>
							<th>Content Type</th>
							<th>Subject</th>
							<th>Created Date</th>
							<th>Created By</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($Files as $key => $File) { ?>
							<tr>
								<td><?=$File->FileName?></td>
								<td><?=$File->ContentType?></td>
								<td><a href="#"><?=$File->Subject?></a></td>
								<td><?=$File->CreatedDate?></td>
								<td><a href="#"><?=$File->CreatedBy?></a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				</div>
				<?php if(count($Files) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Contact/Actions/RelatedObject/<?=$ContactData->ContactID;?>/Files" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
				<?php } ?>
				</div>
			</div>

			<div class="col-md-12">
			<div class="clearfix"></div>
			
			<div class="col-md-12 pl-0 pr-0">
				<div class="col-md-6 col-lg-6 col-sm-6 pl-0">
					<div class="txt-dark capitalize-font mt-20"><strong>Notes</strong> </div>
				</div>
				<div class="col-md-6 col-lg-6 col-sm-6 pr-0">
					<button type="button" class="btn btn-primary btn-outline pull-right mb-10 mt-15" data-toggle="modal" data-target="#create-note-modal"><i class="fa fa-plus"></i> New Note</button>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="table-wrap">
			<div class="table-responsive">
			<table id="WorkOrdersTblEAD" class="table table-hover display table-bordered" width="99%">
				<thead>
					<tr>
						<th>Subject</th>
						<th>Owner</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($Notes as $key => $Note) { ?>
						<tr>
							<td><a href="#"><?=$Note->Subject?></a></td>
							<td><a href="#"><?=$Note->OwnerName?></a></td>
							<td><?=$Note->CreatedDate?></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
			</div>
			<?php if(count($Notes) >= 10){?>
				<div class="pull-right">
					<a href="<?=SITE_URL?>Contact/Actions/RelatedObject/<?=$ContactData->ContactID;?>/Notes" type="button" class="btn btn-primary btn-outline mb-10 mt-10">View All</a>
				</div>
			<?php } ?>
			</div>
			</div>

			</div>
		</div>
	</div>
</div>