<?php $this->load->view('Template/HeaderView'); ?>

<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<!--alerts CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<div class="container-fluid pt-25">

	<!-- Row -->
    <div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading pt-10">
					<ul class="chat-list-wrap">
						<div class="chat-body">
							<div class="chat-data">
								<i class="<?=$this->OBJECT_DETAILS['File']['icon']?> user-img fa-3x txt-dark"></i>
								<div class="user-data">
									<span class="name block capitalize-font txt-dark"><?=$this->OBJECT_DETAILS['File']['title']?></span>
									<span class="panel-title txt-dark"><?=$FileData->Subject;?></span>
								</div>
							</div>
						</div>
					</ul>
					<div class="btn-group pull-right">
						<!-- <div class="dropdown">
							<button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle " type="button">Actions</button>
							<ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu">
								<li style="border-top: 1px solid gainsboro;padding-top: 6px;">
									<a><span><strong>File Actions</strong></span></a>
								</li>
								<li> 
									<a href="<?=SITE_URL;?>File/Actions/CreateFile/<?=$FileData->FileID?>/"><span>Copy File</span></a>
								</li>
								<li>
									<a href="#" onclick="DeleteFile();"><span>Delete File</span></a>
								</li>
							</ul>
						</div> -->
					</div>
					<div class="btn-group pull-right">
						<a href="<?=SITE_URL;?>File/Actions/FileDetails/<?=$FileData->FileID?>" class="btn btn-default">Cancel</a>
						<a href="#" class="btn btn-primary" id="EditFileLink">Save</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Row -->
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content" id="myTabContent_13">
			<div  id="home_13" class="tab-pane fade active in" role="tabpanel">
						
			<div class="panel panel-default card-view">
			 	<!-- <div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">File Details</h6>
					</div>
					<div class="clearfix"></div>
				</div>  -->
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>File/Actions/EditFile/<?=$FileData->FileID?>" class="form-horizontal" id="EditFile" method="post" enctype="multipart/form-data">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

											<div class="col-md-12">

												<div class="txt-dark capitalize-font">
													<strong>File Information</strong>
													<span class="pull-right" style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong>
													</span>
												</div>
												
												<hr class="light-grey-hr">

												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-3">Owner<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="AssignedTo" id="AssignedTo" class="form-control select2">
														<option value="">Select Owner</option>
														<?php $Users = $this->ActionsModel->GetUsers();
															foreach ($Users as $User) { 
																if(isset($FileData->AssignedTo) && $FileData->AssignedTo == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else if($this->session->userdata('UserID') == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else { ?>
																	<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('AssignedTo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Subject<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Subject" id="Subject" value="<?=isset($FileData->Subject)?$FileData->Subject:"";?>">
															<?php echo form_error('Subject');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Related To<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<select name="RelatedTo" id="RelatedTo" class="form-control" OnChange="GetRelatedToObjectData(this.value)">
																<option value="">Select Related To</option>
																<option value="Account" <?=($FileData->RelatedTo=='Account')?"selected=''":"";?>>Account</option>
																<option value="WorkOrder" <?=($FileData->RelatedTo=='WorkOrder')?"selected=''":"";?>>WorkOrder</option>
																<option value="Estimate" <?=($FileData->RelatedTo=='Estimate')?"selected=''":"";?>>Estimate</option>
																<option value="Invoice" <?=($FileData->RelatedTo=='Invoice')?"selected=''":"";?>>Invoice</option>
																<option value="Contact" <?=($FileData->RelatedTo=='Contact')?"selected=''":"";?>>Contact</option>
															</select>
															<?php echo form_error('RelatedTo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">What<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<select name="What" id="What" class="form-control SelectWhat select2">
																<option value="" selected="selected">Select What</option>
															</select>
															<?php echo form_error('What');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Upload<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="file" name="Upload" id="Upload" style="display: none;">
															<button type="button" id="UploadBtn" class="btn btn-default col-md-12">Browse</button>
															<?php echo form_error('Upload');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">File Name<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="FileName" id="FileName" value="<?=isset($FileData->FileName)?$FileData->FileName:"";?>" readonly="">
															<?php echo form_error('FileName');  ?>
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-3">Description</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Description" id="Description" value="<?=isset($FileData->Description)?$FileData->Description:"";?>">
															<?php echo form_error('Description');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">File Size<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="FileSize" id="FileSize" value="<?=isset($FileData->FileSize)?$FileData->FileSize.' MB':"";?>" readonly="">
															<?php echo form_error('FileSize');  ?>
														</div>
													</div>
												</div>
											</div>

											<div class="col-md-12">

												<div class="txt-dark capitalize-font pt-10"><strong>System Information</strong></div>
												<hr class="light-grey-hr">
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label col-md-3">Create Date</label>
														<div class="col-md-9">
															<input type="text" class="form-control" value="<?=$FileData->CreatedDate?>" readonly="">
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-3">Created By</label>
														<div class="col-md-9">
															<input type="text" class="form-control" value="<?=$FileData->CreatedByName?>" readonly="">
														</div>
													</div>
												</div>
												<div class="col-md-6">

												   <div class="form-group">
														<label class="control-label col-md-3">Last Modified Date</label>
														<div class="col-md-9">
															<input type="text" class="form-control" value="<?=$FileData->LastModifiedDate?>" readonly="">
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Last Modified By</label>
														<div class="col-md-9">
															<input type="text" class="form-control" value="<?=$FileData->LastModifiedByName?>" readonly="">
														</div>
													</div>
												</div>
											</div>


										</div>
											<!-- /Row -->
									</div>

									<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="EditFile" id="EditFileBtn">Save</button>
															<a href="<?=SITE_URL;?>File/Actions/FileDetails/<?=$FileData->FileID?>" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			</div>
			<div id="profile_13" class="tab-pane fade" role="tabpanel">
				
				<?php //$this->load->view('File/Include/RelatedInformation'); ?>
				
			</div>
		</div>
		
		</div>

		<!-- <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
			<?php //$this->load->view('File/Include/ActivityTimeline'); ?>
		</div> -->
	</div>
	<!-- /Row -->
</div>


<!-- End Modal -->

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<!-- <script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>
 -->
<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=SITE_URL?>dist/js/responsive-datatable-data.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?=SITE_URL?>vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>
		
<script type="text/javascript">

$(document).ready(function () {

$(document).on('click', "#EditFileLink", function () {
	$('#EditFileBtn').click(); 
});

    $('#EditFile').validate({ 
        rules: {
            Subject: {
                required: true
            },
            RelatedTo: {
                required: true
            },
            What: {
                required: true
            },
            AssignedTo: {
                required: true
            },
            FileName: {
                required: true
            },
            FileSize: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    });
}); 

function DeleteFile(FileID)
{ 
    swal({   
        title: "Are you sure?",   
        text: "You will not be able to recover this data!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#5fc55f",   
        confirmButtonText: "Yes, delete it!",   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function(){   
        swal("Deleted!", "Your data has been deleted.", "success"); 
        setTimeout(function () {
		    window.location.href = "<?=SITE_URL;?>File/Actions/DeleteFile/<?=$FileData->FileID;?>/";
		}, 500);
    });
	return false;
}

function GetRelatedToObjectData(RelatedTo, What = 0){
	$.ajax({
        url: '<?=SITE_URL?>File/Actions/GetRelatedToObjectOptions/'+RelatedTo+'/'+What,
        method: "post",
        data: { RelatedTo : RelatedTo},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$(".SelectWhat").html(result);

               	return true;  	
            } else {
                $(".SelectWhat").html('');
                return false;
            }
        }
    });
}
$(document).ready(function () {
	GetRelatedToObjectData('<?=$FileData->RelatedTo?>', '<?=$FileData->What?>');
	$('#What').val('<?=$FileData->What?>');

	$("#UploadBtn").click(function () { 
		$("#Upload").trigger("click");
	});
	var inputElement = document.getElementById("Upload");
		inputElement.addEventListener("change", handleFiles, false);
			function handleFiles() {
			  	var file = this.files; 
				var FileName = file[0].name;
				var ContentType = file[0].type;
				var Size = file[0].size/1024/1024;
				var FileSize = Size.toFixed(2);
				$("#FileName").val(FileName);	
				$("#FileSize").val(FileSize+' MB');	
			}
});
</script>




