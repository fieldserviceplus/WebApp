<?php $this->load->view('Template/HeaderView')?>

<!-- Bootstrap Datetimepicker CSS -->
<link href="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

<div class="container-fluid pt-25">

	<!-- Row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Create File</h6>
					</div>
					<div class="pull-right">
						<span style="font-style:italic;"><span class="required-sign">*</span> <strong>Denotes required field</strong></span>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									<form action="<?=SITE_URL;?>File/Actions/CreateFile" class="form-horizontal" id="CreateFile" method="post" enctype="multipart/form-data">
										<div class="form-body">
									
											<!-- /Row -->
											<div class="row">

												<div class="col-md-12">

												<div class="txt-dark capitalize-font"><strong>File Information</strong></div>
												<hr class="light-grey-hr">

												<div class="col-md-6">

													<div class="form-group">
														<label class="control-label col-md-3">Owner<span class="required-sign">*</span></label>
														<div class="col-md-9">
														<select name="AssignedTo" id="AssignedTo" class="form-control select2">
														<option value="">Select Owner</option>
														<?php $Users = $this->ActionsModel->GetUsers();
															foreach ($Users as $User) { 
																if(isset($FileData->AssignedTo) && $FileData->AssignedTo == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else if($this->session->userdata('UserID') == $User->UserID){ ?>
																	<option value="<?=$User->UserID?>" selected="selected"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } else { ?>
																	<option value="<?=$User->UserID?>"><?=$User->FirstName.' '.$User->LastName?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php echo form_error('AssignedTo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Subject<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Subject" id="Subject" value="<?=isset($FileData->Subject)?$FileData->Subject:"";?>">
															<?php echo form_error('Subject');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Related To<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<select name="RelatedTo" id="RelatedTo" class="form-control" OnChange="GetRelatedToObjectData(this.value)">
																<option value="">Select Related To</option>
																<option value="Account" <?=((isset($FileData->RelatedTo) && $FileData->RelatedTo=='Account'))?"selected=''":"";?>>Account</option>
																<option value="WorkOrder" <?=(isset($FileData->RelatedTo) && ($FileData->RelatedTo=='WorkOrder'))?"selected=''":"";?>>WorkOrder</option>
																<option value="Estimate" <?=(isset($FileData->RelatedTo) && ($FileData->RelatedTo=='Estimate'))?"selected=''":"";?>>Estimate</option>
																<option value="Invoice" <?=(isset($FileData->RelatedTo) && ($FileData->RelatedTo=='Invoice'))?"selected=''":"";?>>Invoice</option>
																<option value="Contact" <?=(isset($FileData->RelatedTo) && ($FileData->RelatedTo=='Contact'))?"selected=''":"";?>>Contact</option>
															</select>
															<?php echo form_error('RelatedTo');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">What<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<select name="What" id="What" class="form-control SelectWhat select2">
																<option value="" selected="selected">Select What</option>
															</select>
															<?php echo form_error('What');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">Upload<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="file" name="Upload" id="Upload" style="display: none;">
															<button type="button" id="UploadBtn" class="btn btn-default col-md-12">Browse</button>
															<?php echo form_error('Upload');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">File Name<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="FileName" id="FileName" value="" readonly="">
															<?php echo form_error('FileName');  ?>
														</div>
													</div>
												
													<div class="form-group">
														<label class="control-label col-md-3">Description</label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="Description" id="Description" value="<?=isset($FileData->Description)?$FileData->Description:"";?>">
															<?php echo form_error('Description');  ?>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-md-3">File Size<span class="required-sign">*</span></label>
														<div class="col-md-9">
															<input type="text" class="form-control" name="FileSize" id="FileSize" value="" readonly="">
															<?php echo form_error('FileSize');  ?>
														</div>
													</div>

												</div>

												<!--/span-->
												</div>

											<!-- /Row -->
										</div>

										<div class="form-actions mt-10">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-offset-5 col-md-6">
															<button type="submit" class="btn btn-primary  mr-10" name="CreateFile">Save</button>
															<a href="<?=SITE_URL;?>File/View" class="btn btn-default">Cancel</a>
														</div>
													</div>
												</div>
												<div class="col-md-12"> </div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->

<?php $this->load->view('Template/FooterView')?>

<!-- Data table JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?=SITE_URL?>dist/js/dataTables-data.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Jquery validate -->
<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

<!-- Moment JavaScript -->
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?=SITE_URL?>vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Select2 JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
/* Select2 Init*/
$(".select2").select2();
</script>

<script type="text/javascript">
$(document).ready(function () {
 
    $('#CreateFile').validate({ 
        rules: {
            Subject: {
                required: true
            },
            RelatedTo: {
                required: true
            },
            What: {
                required: true
            },
            AssignedTo: {
                required: true
            },
            FileName: {
                required: true
            },
            FileSize: {
                required: true
            }
        },
        errorPlacement: function(){
            return false;
        },
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
    }); 
});

function GetRelatedToObjectData(RelatedTo){
	$.ajax({
        url: '<?=SITE_URL?>File/Actions/GetRelatedToObjectOptions/'+RelatedTo,
        method: "post",
        data: { RelatedTo : RelatedTo},
        dataType: 'html',
        success: function (result) {
            if (result != '') {
            	$(".SelectWhat").html(result);

               	return true;  	
            } else {
                $(".SelectWhat").html('');
                return false;
            }
        }
    });
}

$(document).ready(function () {

<?php if(isset($FileData->RelatedTo) && isset($FileData->What)){ ?>
	GetRelatedToObjectData('<?=$FileData->RelatedTo?>');
	$('#What').val('<?=$FileData->What?>');
<?php } ?>

$("#UploadBtn").click(function () { 
	$("#Upload").trigger("click");
});
var inputElement = document.getElementById("Upload");
	inputElement.addEventListener("change", handleFiles, false);
		function handleFiles() {
		  	var file = this.files; 
			var FileName = file[0].name;
			var ContentType = file[0].type;
			var Size = file[0].size/1024/1024;
			var FileSize = Size.toFixed(2);
			$("#FileName").val(FileName);	
			$("#FileSize").val(FileSize+' MB');	
		}
});
</script>




