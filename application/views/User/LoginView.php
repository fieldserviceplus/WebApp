<!DOCTYPE html>
<html lang="en"> 
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title><?=MAINTITLE?></title>
		<meta name="description" content="<?=MAINTITLE?>" />
		<meta name="keywords" content="<?=MAINTITLE?>" />
		<meta name="author" content="hencework"/>
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="<?=SITE_URL?>img/favicon.ico" type="image/x-icon">
		
		<!-- vector map CSS -->
		<link href="<?=SITE_URL?>vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
		
		
		
		<!-- Custom CSS -->
		<link href="<?=SITE_URL?>dist/css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->
		
		<div class="wrapper pa-0">
			<header class="sp-header">
				<div class="navbar">
					    <div class="navbar-header">
							 <div class="sp-logo-wrap pull-left">
								<a href="<?=SITE_URL?>blog/">
									<img class="brand-img mr-10" src="<?=SITE_LOGO_URL?>" alt="brand" width="213"/>
									<!-- <span class="brand-text"><?=MAINTITLE?></span> -->
									<h6 class="text-center nonecase-font txt-grey">#1 Field Service Software</h6>
								</a>
							</div>
						<button type="button" class="navbar-toggle navbar-toggle-mob-btn" data-toggle="collapse" data-target=".navbar-collapse">
					        <span class="icon-bar" style="background-color: white;"></span>
					        <span class="icon-bar" style="background-color: white;"></span>
					        <span class="icon-bar" style="background-color: white;"></span>
					      </button>
					    </div>
					    <div class="collapse navbar-collapse pull-right collapse-mob">
					      <ul class="nav navbar-nav">
					        <li class="active"><a href="<?=SITE_URL?>blog/">Home</a></li>
					        <li><a href="<?=SITE_URL?>blog/solutions/">Solutions</a></li>
					        <li><a href="<?=SITE_URL?>blog/industries/">Industries</a></li>
					        <li><a href="<?=SITE_URL?>blog/pricing/">Pricing</a></li>
					        <li><a href="<?=SITE_URL?>blog/blog/">Blog</a></li>
					        <li><a href="<?=SITE_URL?>blog/about/">About</a></li>
					        <li><a href="<?=SITE_URL?>User/Auth/Login">Login</a></li>
					        <li class="signup-link"><a href="<?=SITE_URL?>User/Auth/SignUp">Sign Up</a></li>
					      </ul>
					    </div><!--/.nav-collapse -->
					 
					</div>
					<div class="clearfix"></div>
				</header>
			
			<!-- Main Content -->
		    <div class="contain_data"> 
				<div class="container-fluid">
					<!-- Row -->
					<div class="row">
					<div class="col-md-12">
					
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<h3 class="text-center txt-dark mb-10">Login </h3>
											<!-- <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6> -->
										</div>	
										<div class="form-wrap">

											<span><?=$this->session->flashdata('message');?></span>


											<form id="LoginForm" action="<?=SITE_URL?>User/Auth/Login" method="post">
												<div class="form-group">
													<label class="control-label mb-10" for="exampleInputEmail_2">Email address</label>
													<input type="email" class="form-control"  id="exampleInputEmail_2" placeholder="Enter email" name="Email">
													<?php echo form_error('Email');  ?>
												</div>
												<div class="form-group">
													<label class="pull-left control-label mb-10" for="exampleInputpwd_2" >Password</label>
													<a class="capitalize-font txt-primary block mb-10 pull-right font-12" href="<?=SITE_URL?>User/Auth/ForgotPassword">forgot password ?</a>
													<div class="clearfix"></div>
													<input type="password" class="form-control" id="exampleInputpwd_2" placeholder="Enter password" name="Password">
													<?php echo form_error('Password');  ?>
												</div>
												
												<!-- <div class="form-group">
													<div class="checkbox checkbox-primary pr-10 pull-left">
														<input id="checkbox_2" required="" type="checkbox">
														<label for="checkbox_2"> Keep me logged in</label>
													</div>
													<div class="clearfix"></div>
												</div> -->
												<div class="form-group text-center">
													<button type="submit" name="Login" class="btn btn-primary  btn-rounded">Login</button>
												</div>

												<div class="form-group">
													<span class="inline-block pr-10">Don't have an account?</span>
													<a class="inline-block btn btn-primary  btn-rounded btn-outline" href="<?=SITE_URL?>User/Auth/SignUp">Sign Up</a>
												</div>

											</form>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
				<!-- /Row -->	
				
				</div>
				
			</div>
			<!-- /Main Content -->
		
		</div>
		<!-- /#wrapper -->
		
		<!-- JavaScript -->
		
		<!-- jQuery -->
		<script src="<?=SITE_URL?>vendors/bower_components/jquery/dist/jquery.min.js"></script>
		
		<!-- Bootstrap Core JavaScript -->
		<script src="<?=SITE_URL?>vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="<?=SITE_URL?>vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
		
		<!-- Slimscroll JavaScript -->
		<script src="<?=SITE_URL?>dist/js/jquery.slimscroll.js"></script>
		
		<!-- Init JavaScript -->
		<script src="<?=SITE_URL?>dist/js/init.js"></script>

		<!-- Jquery validate -->
		<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

		<script type="text/javascript">
			$(document).ready(function () {

		    $('#LoginForm').validate({ 
		        rules: {
		            Email: {
		                required: true
		            },
		            Password: {
		                required: true,
		                minlength: 6
		            }
		        },
		    	errorElement: 'span',
        		errorClass: 'help-block',
		        highlight: function(element) {
		        	$(element).closest('.form-group').addClass('has-error');
			    },
			    unhighlight: function(element) {
			        $(element).closest('.form-group').removeClass('has-error');
			    },
		    });
		});
		</script>
	</body>
</html>
