 <div class="modal fade" id="location-picker-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title" id="exampleModalLabel1">Address Search</h5>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" action="javascript:void(0)">
				<div class="row">
					<div class="col-md-12">
						<div class="input_fields_wrap">
							<div class="form-group">
								<label class="control-label col-md-2">Location</label>
								<div class="col-md-10">
									<input id="BillingLocation" type="text" name="" class="form-control" placeholder="Enter Address">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="Billingpopupmap" style="height: 400px;"></div><br />
				
				</form>
				
			</div>
		</div>
	</div>
</div> 