<!DOCTYPE html>
<html lang="en"> 
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title><?=MAINTITLE?></title>
		<meta name="description" content="<?=MAINTITLE?>" />
		<meta name="keywords" content="<?=MAINTITLE?>" />
		<meta name="author" content="hencework"/>
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="<?=SITE_URL?>img/favicon.ico" type="image/x-icon">
		
		<!-- vector map CSS -->
		<link href="<?=SITE_URL?>vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
		
		
		
		<!-- Custom CSS -->
		<link href="<?=SITE_URL?>dist/css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->
		
		<div class="wrapper pa-0">
			<header class="sp-header">
				<div class="navbar">
					    <div class="navbar-header">
							 <div class="sp-logo-wrap pull-left">
								<a href="#">
									<img class="brand-img mr-10" src="<?=SITE_LOGO_URL?>" alt="brand"/>
									<span class="brand-text"><?=MAINTITLE?></span>
									<h6 class="text-center nonecase-font txt-grey">#1 Field Service Software</h6>
								</a>
							</div>
						<button type="button" class="navbar-toggle navbar-toggle-mob-btn" data-toggle="collapse" data-target=".navbar-collapse">
					        <span class="icon-bar" style="background-color: white;"></span>
					        <span class="icon-bar" style="background-color: white;"></span>
					        <span class="icon-bar" style="background-color: white;"></span>
					      </button>
					    </div>
					    <div class="collapse navbar-collapse pull-right collapse-mob">
					      <ul class="nav navbar-nav">
					        <li class="active"><a href="#">Home</a></li>
					        <li><a href="#">Features</a></li>
					        <li><a href="#">Pricing</a></li>
					        <li><a href="#">Blog</a></li>
					        <li><a href="#">About</a></li>
					        <li><a href="<?=SITE_URL?>User/Auth/Login">Login</a></li>
					        <li class="signup-link"><a href="<?=SITE_URL?>User/Auth/SignUp">Sign Up</a></li>
					      </ul>
					    </div><!--/.nav-collapse -->
					 
					</div>
					<div class="clearfix"></div>
				</header>
			
				<!-- Main Content -->
		    <div class="contain_data"> 
				<div class="container-fluid">
					<!-- Row -->
					<div class="row">
					<div class="col-md-12">
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<h3 class="text-center txt-dark mb-10">Sign up Form</h3>
											<!-- <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6> -->
										</div>	
										<div class="form-wrap">

										     <span><?=$this->session->flashdata('message');?></span>

											<form id="SignUpForm" action="<?=SITE_URL?>User/Auth/SignUp" method="post">
												<div class="form-group">
													<label class="control-label mb-10" for="exampleInputName_1">First Name</label>
													<input type="text" class="form-control"  id="FirstName" placeholder="First Name" name="FirstName" value="<?php echo set_value('FirstName'); ?>">
													<?php echo form_error('FirstName');  ?>
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="exampleInputName_1">Last Name</label>
													<input type="text" class="form-control"  id="LastName" placeholder="Last Name" name="LastName" value="<?php echo set_value('LastName'); ?>">
													<?php echo form_error('LastName');  ?>
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="exampleInputName_1">Company</label>
													<input type="text" class="form-control"  id="CompanyName" placeholder="Company" name="CompanyName" value="<?php echo set_value('CompanyName'); ?>">
													<?php echo form_error('CompanyName');  ?>
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="exampleInputName_1">Phone</label>
													<input type="text" class="form-control" id="PhoneNo" placeholder="Phone" name="PhoneNo" value="<?php echo set_value('PhoneNo'); ?>">
													<?php echo form_error('PhoneNo');  ?>
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="exampleInputName_1">Country</label>
													<?php $Countries = MY_Model::getCountry();
													?>
													<select class="form-control" data-style="form-control btn-default btn-outline" name="Country" id="Country" onchange="setState(this.value)">
													<option value="">Select Country</option>
													<?php foreach($Countries as $Country){ ?>

														<option value="<?=$Country->CountryID?>"><?=$Country->CountryName?></option>
													<?php } ?>
													</select>
													<?php echo form_error('Country');  ?>
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="exampleInputName_1">State</label>
													<select class="form-control" data-style="form-control btn-default btn-outline" name="State" id="State" onchange="getCity(this.value)">
														<option value="">Select State</option>
													</select>
													<?php echo form_error('State');  ?>
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="exampleInputName_1">City</label>
													<select class="form-control" data-style="form-control btn-default btn-outline" name="City" id="City">
														<option value="">Select City</option>
													</select>
													<?php echo form_error('City');  ?>
												</div>
												<div class="form-group <?php if(form_error('Email')) { echo "has-error"; } ?>" >
													<label class="control-label mb-10" for="exampleInputEmail_2">Email</label>
													<input type="email" class="form-control"  id="Email" placeholder="Email" name="Email" value="<?php echo set_value('Email'); ?>">
													<div class="help-block"><?php echo form_error('Email'); ?></div>
												</div>
												<div class="form-group">
													<label class="pull-left control-label mb-10" for="exampleInputpwd_2">Password</label>
													<input type="password" class="form-control" id="Password" placeholder="Enter Password" name="Password" value="<?php echo set_value('Password'); ?>">
													<?php echo form_error('Password');  ?>
												</div>
												<div class="form-group">
													<label class="pull-left control-label mb-10" for="exampleInputpwd_3">Confirm Password</label>
													<input type="password" class="form-control" id="ConfirmPassword" placeholder="Enter Confirm Password" name="ConfirmPassword" value="<?php echo set_value('ConfirmPassword'); ?>">
													<?php echo form_error('ConfirmPassword');  ?>
												</div>
												<div class="form-group">
													<div class="checkbox checkbox-primary pr-10 pull-left">
														<input id="TermsConditions" name="TermsConditions" type="checkbox">
														<label for="TermsConditions"> I accept these <span class="txt-primary">terms and conditions</span></label>
														<?php echo form_error('TermsConditions');  ?> 
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="form-group text-center">
													<button type="submit" class="btn btn-primary btn-rounded" name="SignUp">sign Up</button>
												</div>
												<div class="form-group">
												 <span class="inline-block pr-10">Already have an account?</span> 
												 <a class="inline-block btn btn-primary btn-rounded btn-outline" href="<?=SITE_URL?>User/Auth/Login">Login</a>
												</div>
											</form>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->	
				</div>

			</div>

			</div>
				
			</div>
			<!-- /Main Content -->
		
		</div>
		<!-- /#wrapper -->
		
		<!-- JavaScript -->
		
		<!-- jQuery -->
		<script src="<?=SITE_URL?>vendors/bower_components/jquery/dist/jquery.min.js"></script>
		
		<!-- Bootstrap Core JavaScript -->
		<script src="<?=SITE_URL?>vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="<?=SITE_URL?>vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
		
		<!-- Slimscroll JavaScript -->
		<script src="<?=SITE_URL?>dist/js/jquery.slimscroll.js"></script>
		
		<!-- Init JavaScript -->
		<script src="<?=SITE_URL?>dist/js/init.js"></script>

		<!-- Jquery validate -->
		<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

		<script type="text/javascript">
		function setState(CountryID)
		{   
	        $.ajax({
                url: '<?=SITE_URL?>/User/Auth/getState/',
                type: "POST",
                data: {CountryID : CountryID},
                dataType: 'html',
                async: true,
                success: function(data)
                {  
                    $("#State").html(data);
                    $("#City").empty();
                }
	        });
		}

		function getCity(StateID)
		{   
	        $.ajax({
                url: '<?=SITE_URL?>/User/Auth/getCity/',
                type: "POST",
                data: {StateID : StateID},
                dataType: 'html',
                async: true,
                success: function(data)
                {  
                    $("#City").html(data);
                }
	        });
		}
		$(document).ready(function () {

		    $('#SignUpForm').validate({ 
		        rules: {
		            FirstName: {
		                required: true
		            },
		            LastName: {
		                required: true
		            },
		            CompanyName: {
		                required: true
		            },
		            PhoneNo: {
		                required: true
		            },
		            Country: {
		                required: true
		            },
		            State: {
		                required: true
		            },
		            City: {
		                required: true
		            },
		            Email: {
		                required: true
		            },
		            Password: {
		                required: true,
		                minlength: 6
		            },
		            ConfirmPassword: {
		                required: true,
		                minlength: 6,
		                equalTo: "#Password"
		            },
		            TermsConditions: {
		                required: true,
		            }
		        },
		        messages: {
		        	TermsConditions: "",
		    	},
		    	errorElement: 'span',
        		errorClass: 'help-block',
		        errorPlacement: function(error, element) { 
				    if(element.context.id == 'TermsConditions'){  
				    	return false;
				    } else {
				    	element.after(error);
				    }
				},
		        /*submitHandler: function (form) { 
		            return false; 
		        },*/
		        highlight: function(element) {
		        	$(element).closest('.form-group').addClass('has-error');
			    },
			    unhighlight: function(element) {
			        $(element).closest('.form-group').removeClass('has-error');
			    },
		    });


		});
		</script>
	</body>
</html>
