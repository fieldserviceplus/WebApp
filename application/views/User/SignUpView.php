
<!DOCTYPE html>
<html lang="en"> 
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title><?=MAINTITLE?></title>
		<meta name="description" content="<?=MAINTITLE?>" />
		<meta name="keywords" content="<?=MAINTITLE?>" />
		<meta name="author" content="hencework"/>
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="<?=SITE_URL?>img/favicon.ico" type="image/x-icon">
		
		<!-- vector map CSS -->
		<link href="<?=SITE_URL?>vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
		
		<!-- jquery-steps css -->
		<link rel="stylesheet" href="<?=SITE_URL?>vendors/bower_components/jquery.steps/demo/css/jquery.steps.css">
		
		<!-- Custom CSS -->
		<link href="<?=SITE_URL?>dist/css/style.css" rel="stylesheet" type="text/css">

		<script src="https://maps.googleapis.com/maps/api/js?key=<?=MAP_JS_KEY?>&libraries=places"></script>
 
	</head>
	<body>
		<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->
		
		<div class="wrapper pa-0">
			<header class="sp-header">
				<div class="navbar">
					    <div class="navbar-header">
							 <div class="sp-logo-wrap pull-left">
								<a href="<?=SITE_URL?>blog/">
									<img class="brand-img mr-10" src="<?=SITE_LOGO_URL?>" width="213" alt="brand"/>
									<!-- <span class="brand-text"><?=MAINTITLE?></span> -->
									<h6 class="text-center nonecase-font txt-grey">#1 Field Service Software</h6>
								</a>
							</div>
						<button type="button" class="navbar-toggle navbar-toggle-mob-btn" data-toggle="collapse" data-target=".navbar-collapse">
					        <span class="icon-bar" style="background-color: white;"></span>
					        <span class="icon-bar" style="background-color: white;"></span>
					        <span class="icon-bar" style="background-color: white;"></span>
					      </button>
					    </div>
					    <div class="collapse navbar-collapse pull-right collapse-mob">
					      <ul class="nav navbar-nav">
					        <li class="active"><a href="<?=SITE_URL?>blog/">Home</a></li>
					        <li><a href="<?=SITE_URL?>blog/solutions/">Solutions</a></li>
					        <li><a href="<?=SITE_URL?>blog/industries/">Industries</a></li>
					        <li><a href="<?=SITE_URL?>blog/pricing/">Pricing</a></li>
					        <li><a href="<?=SITE_URL?>blog/blog/">Blog</a></li>
					        <li><a href="<?=SITE_URL?>blog/about/">About</a></li>
					        <li><a href="<?=SITE_URL?>User/Auth/Login">Login</a></li>
					        <li class="signup-link"><a href="<?=SITE_URL?>User/Auth/SignUp">Sign Up</a></li>
					      </ul>
					    </div><!--/.nav-collapse -->
					 
					</div>
					<div class="clearfix"></div>
				</header>
			
				<!-- Main Content -->
		    <div class="contain_data"> 
				<div class="container-fluid">
					<!-- Row -->
					<div class="row mt-50">
						<div class="col-md-12">
							<div class="row">
								<div class="col-sm-12 col-xs-12">
									<div class="mb-30">
										<h3 class="text-center txt-dark mb-10">Signup</h3>
									</div>	
									<div class="form-wrap">

										<form id="SignUpForm" action="<?=SITE_URL?>User/Auth/SignUp" method="post" class="form-horizontal">

										<div class="col-md-1">
										</div>
										<div class="col-md-10">

										<div><?=$this->session->flashdata('message');?></div>

										<div class="col-md-12">
										<div class="txt-dark capitalize-font pt-10">
											<strong>User Details</strong>
										</div>
										<hr class="light-grey-hr">	

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">First Name<span class="required-sign">*</span></label>
												<div class="col-md-9">
													<input type="text" class="form-control" name="FirstName" id="FirstName" value="<?=set_value('FirstName'); ?>">
													<?php echo form_error('FirstName');  ?>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Last Name<span class="required-sign">*</span></label>
												<div class="col-md-9">
													<input type="text" class="form-control" name="LastName" id="LastName" value="<?=set_value('LastName'); ?>">
													<?php echo form_error('LastName');  ?>
												</div>
											</div>
											<div class="form-group <?php if(form_error('Email')) { echo "has-error"; } ?>">
												<label class="control-label col-md-3">Email<span class="required-sign">*</span></label>
												<div class="col-md-9">
													<input type="text" class="form-control" name="Email" id="Email" value="<?=set_value('Email'); ?>">
													<?php echo form_error('Email');  ?>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Password<span class="required-sign">*</span></label>
												<div class="col-md-9">
													<input type="Password" class="form-control" name="Password" id="Password" value="<?=set_value('Password'); ?>">
													<?php echo form_error('Password');  ?>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Confirm Password<span class="required-sign">*</span></label>
												<div class="col-md-9">
													<input type="Password" class="form-control" name="ConfirmPassword" id="ConfirmPassword" value="<?=set_value('ConfirmPassword'); ?>">
													<?php echo form_error('ConfirmPassword');  ?>
												</div>
											</div>
										</div>
										</div>

										<div class="col-md-12">
										<div class="txt-dark capitalize-font pt-50">
										<strong>Company Details</strong>
										</div>
										<hr class="light-grey-hr">	
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Company Name<span class="required-sign">*</span></label>
												<div class="col-md-9">
													<input type="text" class="form-control" name="CompanyName" id="CompanyName" value="<?=set_value('CompanyName');?>">
													<?php echo form_error('CompanyName');  ?>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Phone #<span class="required-sign">*</span></label>
												<div class="col-md-9">
													<input type="text" class="form-control" name="Phone" id="Phone" value="<?=set_value('Phone');?>">
													<?php echo form_error('Phone');  ?>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Website<span class="required-sign">*</span></label>
												<div class="col-md-9">
													<input type="text" class="form-control" name="Website" id="Website" value="<?=set_value('Website');?>">
													<?php echo form_error('Website');  ?>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Edition<span class="required-sign">*</span></label>
												<div class="col-md-9">
													<input type="text" class="form-control" name="CompanyEdition" id="CompanyEdition" value="<?=set_value('CompanyEdition');?>">
													<?php echo form_error('CompanyEdition');  ?>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Billing Address</label>
												<div class="col-md-9">
													<button type="button" class="btn btn-primary btn-outline btn-icon left-icon col-md-12" data-toggle="modal" data-target="#location-picker-modal"><i class="fa fa-search"></i>Search Address</button>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Billing Street<span class="required-sign">*</span></label>
												<div class="col-md-9">
													<input type="text" class="form-control" name="BillingAddress" id="BillingAddress" value="<?=set_value('BillingAddress');?>">
													<?php echo form_error('BillingAddress');  ?>
													<input type="hidden" name="BillingLatitude" id="BillingLatitude">
													<input type="hidden" name="BillingLongitude" id="BillingLongitude">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Billing City<span class="required-sign">*</span></label>
												<div class="col-md-9">
													<input type="text" class="form-control" name="BillingCity" id="BillingCity" value="<?=set_value('BillingCity');?>">
													<?php echo form_error('BillingCity');  ?>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Billing State<span class="required-sign">*</span></label>
												<div class="col-md-9">
													<input type="text" class="form-control" name="BillingState" id="BillingState" value="<?=set_value('BillingState');?>">
													<?php echo form_error('BillingState');  ?>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Billing Country<span class="required-sign">*</span></label>
												<div class="col-md-9">
													<input type="text" class="form-control" name="BillingCountry" id="BillingCountry" value="<?=set_value('BillingCountry');?>">
													<?php echo form_error('BillingCountry');  ?>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Billing Postal Code<span class="required-sign">*</span></label>
												<div class="col-md-9">
													<input type="text" class="form-control" name="BillingPostalCode" id="BillingPostalCode" value="<?=set_value('BillingPostalCode');?>">
													<?php echo form_error('BillingPostalCode');  ?>
												</div>
											</div>

										</div>
										</div>
										<div class="col-md-12">
										<div class="txt-dark capitalize-font pt-50">
										<strong>Plan</strong>
										</div>
										<hr class="light-grey-hr">	
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Plan Details<span class="required-sign">*</span></label>
												<div class="col-md-9">
													<select name="Plan" id="Plan" class="form-control select2 FetchPlan">
														<?php $Plans = $this->AuthModel->GetPlans();
															foreach ($Plans as $Plan) { ?>
																<option value="<?=$Plan->OrgPlanID?>"><?=$Plan->PlanName?></option>
															<?php } ?>
													</select>
													<?php echo form_error('Plan');  ?>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3">Contract Type<span class="required-sign">*</span></label>
												<div class="col-md-9">
													<select name="ContractType" id="ContractType" class="form-control select2 FetchPlan">
														<option value="Annual">Annual</option>
														<option value="Monthly">Monthly</option>
													</select>
													<?php echo form_error('ContractType');  ?>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div id="PlanPrice" class="pt-10"></div>
											<div id="ContractRenewalDate" class="pt-10"></div>
										</div>
										</div>

										<div class="col-md-12">
										<div class="form-group pt-10">
											<div class="checkbox checkbox-primary pr-10 text-center">
												<input id="TermsConditions" name="TermsConditions" type="checkbox">
												<label for="TermsConditions"> I accept these <span class="txt-primary">terms and conditions</span></label>
												<?php echo form_error('TermsConditions');  ?> 
											</div>
										</div>
										<div class="form-group text-center">
											<button type="submit" class="btn btn-primary btn-rounded" name="SignUp">sign Up</button>
										</div>
										<div class="form-group text-center">
										 <span class="inline-block pr-10">Already have an account?</span> 
										 <a class="inline-block btn btn-primary btn-rounded btn-outline" href="<?=SITE_URL?>User/Auth/Login">Login</a>
										</div>
										</div>
										</div>
									</form>
									</div>
								</div>	
							</div>
					
						</div>
					</div>
			</div>
				
			</div>
			<!-- /Main Content -->
		
		</div>
		<!-- /#wrapper -->

		<?php $this->load->view('User/Modal/LocationPickerView');?>
		
		<!-- JavaScript -->
		
		<!-- jQuery -->
		<script src="<?=SITE_URL?>vendors/bower_components/jquery/dist/jquery.min.js"></script>
		
		<!-- Bootstrap Core JavaScript -->
		<script src="<?=SITE_URL?>vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="<?=SITE_URL?>vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
		
		<!-- Slimscroll JavaScript -->
		<script src="<?=SITE_URL?>dist/js/jquery.slimscroll.js"></script>
		
		<!-- Init JavaScript -->
		<script src="<?=SITE_URL?>dist/js/init.js"></script>

		<!-- Jquery validate -->
		<script src="<?=SITE_URL?>dist/js/jquery.validate.min.js"></script>

		<script type="text/javascript">

		$(document).ready(function () {

		    $('#SignUpForm').validate({ 
		        rules: {
		            FirstName: {
		                required: true
		            },
		            LastName: {
		                required: true
		            },
		            CompanyName: {
		                required: true
		            },
		            Phone: {
		                required: true
		            },
		            Website: {
		                required: true
		            },
		            Edition: {
		                required: true
		            },
		            BillingAddress: {
		                required: true
		            },
		            BillingCity: {
		                required: true
		            },
		            BillingState: {
		                required: true
		            },
		            BillingCountry: {
		                required: true
		            },
		            BillingPostalCode: {
		                required: true
		            },
		            CompanyEdition: {
		                required: true
		            },
		            Email: {
		                required: true
		            },
		            Password: {
		                required: true,
		                minlength: 6
		            },
		            ConfirmPassword: {
		                required: true,
		                minlength: 6,
		                equalTo: "#Password"
		            },
		            TermsConditions: {
		                required: true,
		            }
		        },
		        messages: {
		        	TermsConditions: "",
		    	},
		    	errorElement: 'span',
        		errorClass: 'help-block',
		        errorPlacement: function(error, element) { 
				    if(element.context.id == 'TermsConditions'){  
				    	return false;
				    } else {
				    	element.after(error);
				    }
				},
		        highlight: function(element) {
		        	$(element).closest('.form-group').addClass('has-error');
			    },
			    unhighlight: function(element) {
			        $(element).closest('.form-group').removeClass('has-error');
			    },
		    });
		});
		
		$('.FetchPlan').on('change', function() {

			var Plan = $("#Plan").val();
			var ContractType = $("#ContractType").val();
			$.ajax({
		        url: '<?=SITE_URL?>User/Auth/GetPlanDetails/'+Plan,
		        type: "POST",
		       /* data: {Plan : Plan, ContractType : ContractType},*/
		        dataType: 'json',
		        async: true,
		        success: function(data)
		        {  
		        	var d = new Date();
				    var year = d.getFullYear();
				    var month = d.getMonth();
				    var day = d.getDate();
		        	if(ContractType == 'Annual'){

		        		var date = new Date(year + 1, month, day);

		        		$("#PlanPrice").html('<div class="form-group pt-5 pb-5"><div class="col-md-3 text-right txt-dark">Plan Price</div><div class="col-md-9">$ '+data.UserPriceAnnual+'</div></div>');

		        	} else if(ContractType == 'Monthly'){

		        		var date = new Date(year, month + 1, day);
				    	
		        		$("#PlanPrice").html('<div class="form-group pt-5 pb-5"><div class="col-md-3 text-right txt-dark">Plan Price</div><div class="col-md-9">$ '+data.UserPriceMonthly+'</div></div>');
		        	}
		        	var ContractDate = date.toLocaleDateString('en-GB', {
					  day: 'numeric', month: 'numeric', year: 'numeric'
					}).replace(/ /g, '-');
		            $("#ContractRenewalDate").html('<div class="form-group pt-5 pb-5"><div class="col-md-3 text-right txt-dark">Contract End Date</div><div class="col-md-9">'+ContractDate+'</div></div>');
		        }
		    });
		});
		</script>
		<!-- Set Location -->
		<script src="<?=SITE_URL?>dist/js/location-picker.js"></script>
		<script type="text/javascript">
		$(document).ready(function () {
			set_location_in_map("Billing");
		});
		</script>
	</body>
</html>
