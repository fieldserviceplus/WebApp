<?php $this->load->view('Template/HeaderView');?>
 <style type="text/css">
.dot {
  background-color: #5fc55f;
  width: 8px;
  height: 8px;
  margin-right: 2px;
  border-radius: 50%;
  display: inline-block;
}
.fc-more-cell, .fc-event-container{
	text-align: center;
}
</style>
<link href="<?=SITE_URL?>vendors/bower_components/fullcalendar/dist/fullcalendar.css" rel="stylesheet" type="text/css"/>

            <div class="container-fluid pt-25">
				<!-- Row -->
				<div class="row">
					<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
							<div class="panel panel-default card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark">My Tasks and Events</h6>
									</div>
									<div class="pull-right col-lg-3 col-md-6 col-sm-6 col-xs-12">
										<select class="selectpicker" data-style="form-control col-lg-2" onchange="GetTaskData(this.value);">
											<optgroup>
										        <option value="Today">Today</option>
												<option value="TodayOverdue">Today + Overdue</option>
												<option value="Overdue">Overdue</option>
										    </optgroup>
										    <optgroup>
										        <option value="JumpToCalendar">Jump to Calendar</option>
												<option value="JumpToTasks">Jump to Tasks</option>
										    </optgroup>
										</select>
									</div>	
									<div class="clearfix"></div>
								</div>
								<div  class="panel-wrapper collapse in">
									<div  class="panel-body" style="min-height:367px;">
										<ul class="list-icons">
										  <!-- <li class="mb-10"><i class="fa fa-angle-double-right text-info mr-5"></i> 1/3/2018 PM Visit to Frank Wright</li>
										  <li class="mb-10"><i class="fa fa-angle-double-right text-info mr-5"></i> 1/2/2018 PM Visit to Joe Olson</li>
										  <li class="mb-10"><i class="fa fa-angle-double-right text-info mr-5"></i> 1/1/2018	New Installation of Heating Unit - Sue White </li>
										  <li class="mb-10"><i class="fa fa-angle-double-right text-info mr-5"></i> 12/31/2017 Call John Smith</li>
										  <li class="mb-10"><i class="fa fa-angle-double-right text-info mr-5"></i> 12/23/2017 Repair HVAC</li>
										  <li class="mb-10"><i class="fa fa-angle-double-right text-info mr-5"></i> 12/10/2017 Repair HVAC</li> -->
										</ul>
									</div>
								</div>
						</div>
					</div>
					
					<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
						<div class="panel panel-default card-view">
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="calendar-wrap">
									  <div id="calendar_small" class="small-calendar"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /Row -->


				<!-- <div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">Dashboard</h6>
								</div>
								<div class="pull-right">
									<a href="#" class="pull-left inline-block full-screen mr-15">
										<i class="zmdi zmdi-fullscreen"></i>
									</a>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div id="e_chart_3" class="" style="height:330px;"></div>
								</div>
							</div>
							</div>
							<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div id="bar_chart_horizontal" class="" style="height:346px;"></div>
								</div>
							</div>
							</div>
							<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<canvas id="bar_chart_vertical" height="200"></canvas>
								</div>	
							</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>

				</div> -->
				
			
			</div>
			

 <?php $this->load->view('Template/FooterView')?>
 
<!-- Calender JavaScripts -->
<script src="<?=SITE_URL?>vendors/bower_components/moment/min/moment.min.js"></script>
<script src="<?=SITE_URL?>vendors/jquery-ui.min.js"></script>
<script src="<?=SITE_URL?>vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="<?=SITE_URL?>dist/js/fullcalendar-data.js"></script>

<!-- EChartJS JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/echarts/dist/echarts-en.min.js"></script>
<script src="<?=SITE_URL?>vendors/echarts-liquidfill.min.js"></script>
<script src="<?=SITE_URL?>vendors/ecStat.min.js"></script>

<!-- ChartJS JavaScript -->
<script src="<?=SITE_URL?>vendors/chart.js/Chart.min.js"></script>

<!-- Init JavaScript -->
<script src="<?=SITE_URL?>dist/js/home.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="<?=SITE_URL?>vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<script type="text/javascript">

function GetTaskData(Filter){

	if(Filter == 'Today' || Filter == 'TodayOverdue' || Filter == 'Overdue')
	{
	    $.ajax({
	        url: '<?=SITE_URL?>User/Home/GetTaskData',
	        data: { Filter : Filter},
	        method: "post",
	        dataType: 'html',
	        success: function (result) {
	            if (result != '') {
	            	$(".list-icons").html(result);
	               	return true;  	
	            } else {
	            	$(".list-icons").html('');
	            }
	        }
	    });
	} else if(Filter == 'JumpToCalendar'){
		window.location.href = "<?=SITE_URL?>Calendar/ListView/";
	} else if(Filter == 'JumpToTasks'){
		window.location.href = "<?=SITE_URL?>Task/Actions";
	}
}

GetTaskData('Today');

function DayData(start, end)
{ 
	//$('#calendar').fullCalendar( 'removeEvents'); 
  	$.ajax({
        type:"GET",
        url: "<?=SITE_URL?>Calendar/CalendarView/Calendar/",
        dataType: 'json',
        data: { ViewType:'Day', start : start, end: end, IsDashboardCal: 1, ShowTasks: 1},
        success: function(data){

            $.each(data, function(index, event){
              	$('#calendar_small').fullCalendar('renderEvent', event);
            });
        }
    });

    return false;
    // to stop the form from actually submitting
} 

$('#calendar_small').fullCalendar({
	header: {
	    left: 'title',
	    right: 'prev,next',
	},
	height: 'auto',
	editable: false,
	droppable: false, // this allows things to be dropped onto the calendar
	eventLimit: true, 
	eventMouseover: function (data, event, view) {
		var tooltip = '<div class="tooltiptopicevent tooltip tooltip-inner" style="width:auto;height:auto;position:absolute;z-index:10001;">10:00 AM ' + data.title + '</div>';
		$("body").append(tooltip);
	    $(this).mouseover(function (e) {
	        $(this).css('z-index', 10000);
	        $('.tooltiptopicevent').fadeIn('500');
	        $('.tooltiptopicevent').fadeTo('10', 1.9);
	    }).mousemove(function (e) {
	        $('.tooltiptopicevent').css('top', e.pageY + 10);
	        $('.tooltiptopicevent').css('left', e.pageX + 20);
	    });
	},
	eventMouseout: function (data, event, view) {
	    //$(this).css('z-index', 8);
	    //$('.tooltiptopicevent').remove();
	},
	dayClick: function () {
	    //tooltip.hide()
	},
	eventResizeStart: function () {
	    //tooltip.hide()
	},
	eventDragStart: function () {
	    //tooltip.hide()
	},
	eventAfterRender: function(event, element) {
	  
	   $('.fc-more').html('<div class="dot"></div>');
	   $('.fc-event-container').html('<div class="dot" title="'+event.title+'"></div>');

	}, 
	events: function(start, end, timezone, callback) {
				
	    var start = start.format("YYYY-MM-DD");
	    var end = end.format("YYYY-MM-DD");
	    DayData(start, end);	
	},
	viewDisplay: function () {
	    //tooltip.hide()
	},
});

</script>