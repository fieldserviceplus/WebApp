<?php

class MY_Model extends CI_Model
{  
    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $CI=&get_instance();
        $CI->LoginUserID = $CI->session->userdata('UserID');
        $CI->LoginOrganizationID = $CI->session->userdata('OrganizationID');
    }

    static function insertCreatedModifiedByAndDt($UserID, $TableName, $Where)
    { 
        $CI=&get_instance();
        $data = array(
            'CreatedDate' => date('Y-m-d H:i:s'),
            'LastModifiedDate' => date('Y-m-d H:i:s'),
            'CreatedBy' => $UserID,
            'LastModifiedBy' => $UserID
        ); 
        $result = $CI->db->update($TableName, $data, $Where);
    }
 
    static function updateCreatedModifiedByAndDt($UserID, $TableName, $Where)
    {
        $CI=&get_instance();
        $data = array(
            'LastModifiedDate' => date('Y-m-d H:i:s'),
            'LastModifiedBy' => $UserID
        ); 
        $result = $CI->db->update($TableName, $data, $Where);
    }

    static function getCountry()
    {
        $CI=&get_instance();
        $query = $CI->db->select('CountryID, CountryName')->get('Country');
        return $query->result(); 
    }

    static function getState($CountryID)
    {
        $CI=&get_instance();
        $statestr = "";
        $query = $CI->db->select('StateID, StateName')->get_where('State',array('Country'=>$CountryID));
        $results = $query->result();

        foreach ($results as $key => $value) {
           $statestr .= '<option value="'.$value->StateID.'">'.$value->StateName.'</option>';
        }
        return $statestr; 
    }

    static function getCity($StateID)
    {
        $CI=&get_instance();
        $citystr = "";
        $query = $CI->db->select('CityID, CityName')->get_where('City',array('State'=>$StateID));
        $results = $query->result();

        foreach ($results as $key => $value) {
           $citystr .= '<option value="'.$value->CityID.'">'.$value->CityName.'</option>';
        }
        return $citystr; 
    }

    static function getAllUsers()
    { 
        $CI=&get_instance();
        $query = $CI->db->get_where('User', array('OrganizationID' => $CI->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    static function GetEmailTemplates()
    {
        $CI=&get_instance();
        $query = $CI->db->get_where('EmailTemplates', array('OrganizationID' => $CI->LoginOrganizationID, 'IsDeleted' => 0, 'AvailableForUse' => 1)); 
        
        return $query->result();
    }

    static function getRelatedToObjectOptions($RelatedTo, $ObjectID)
    { 
        $CI=&get_instance();

        $OptionStr = "";
        if(isset($RelatedTo) && $RelatedTo == 'Account'){

            $query = $CI->db->get_where('Account', array('OrganizationID' => $CI->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
            $Accounts = $query->result();
            foreach ($Accounts as $key => $Account) {
                if($ObjectID == $Account->AccountID)
                    $OptionStr .= "<option value='".$Account->AccountID."' selected=''>".$Account->AccountName."</option>";
                else 
                    $OptionStr .= "<option value='".$Account->AccountID."'>".$Account->AccountName."</option>";
            }

        } else if(isset($RelatedTo) && $RelatedTo == 'WorkOrder'){

            $query = $CI->db->get_where('WorkOrder', array('OrganizationID' => $CI->LoginOrganizationID, 'IsDeleted' => 0)); 
            $WorkOrders = $query->result();
            foreach ($WorkOrders as $key => $WorkOrder) {
                if($ObjectID == $WorkOrder->WorkOrderID)
                    $OptionStr .= "<option value='".$WorkOrder->WorkOrderID."' selected=''>".$WorkOrder->WorkOrderNo." - ".$WorkOrder->Subject."</option>";
                else 
                    $OptionStr .= "<option value='".$WorkOrder->WorkOrderID."'>".$WorkOrder->WorkOrderNo." - ".$WorkOrder->Subject."</option>";
            }
            
        } else if(isset($RelatedTo) && $RelatedTo == 'Estimate'){

            $query = $CI->db->get_where('Estimate', array('OrganizationID' => $CI->LoginOrganizationID, 'IsDeleted' => 0)); 
            $Estimates = $query->result();
            foreach ($Estimates as $key => $Estimate) {
                if($ObjectID == $Estimate->EstimateID)
                    $OptionStr .= "<option value='".$Estimate->EstimateID."' selected=''>".$Estimate->EstimateName."</option>";
                else 
                    $OptionStr .= "<option value='".$Estimate->EstimateID."'>".$Estimate->EstimateName."</option>";
            }
            
        } else if(isset($RelatedTo) && $RelatedTo == 'Invoice'){

            $query = $CI->db->get_where('Invoice', array('OrganizationID' => $CI->LoginOrganizationID, 'IsDeleted' => 0)); 
            $Invoices = $query->result();
            foreach ($Invoices as $key => $Invoice) {
                if($ObjectID == $Invoice->InvoiceID)
                    $OptionStr .= "<option value='".$Invoice->InvoiceID."' selected=''>".$Invoice->InvoiceNo."</option>";
                else
                    $OptionStr .= "<option value='".$Invoice->InvoiceID."'>".$Invoice->InvoiceNo."</option>";
            }
            
        } else if(isset($RelatedTo) && $RelatedTo == 'Contact'){

            $query = $CI->db->get_where('Contact', array('OrganizationID' => $CI->LoginOrganizationID, 'IsDeleted' => 0)); 
            $Contacts = $query->result();
            foreach ($Contacts as $key => $Contact) {
                if($ObjectID == $Contact->ContactID)
                    $OptionStr .= "<option value='".$Contact->ContactID."' selected=''>".$Contact->FirstName." ".$Contact->LastName."</option>";
                else
                    $OptionStr .= "<option value='".$Contact->ContactID."'>".$Contact->FirstName." ".$Contact->LastName."</option>";
            }
        }

        return $OptionStr;
    }

    static function getCustomFieldsByObject($Object, $ObjectID)
    {  
        $CI=&get_instance();
        $CI->db->select('acf.*');
        $CI->db->from('CustomFields acf');
        $CI->db->where(array('acf.AddToPageLayout' => 1,'acf.Object' => $Object,'acf.OrganizationID' => $CI->LoginOrganizationID, 'IsDeleted' => 0));
        $query = $CI->db->get();
        $data['Fields'] = $query->result();

        $CI->db->select('*, '.$Object.'CustomFieldValueID as CustomFieldValueID');
        $CI->db->from($Object.'CustomFieldValues');
        $CI->db->where(array('OrganizationID' => $CI->LoginOrganizationID, $Object.'ID' => $ObjectID)); 
        $query = $CI->db->get();
        $result = $query->result();  

         foreach ($result as $key => $value) {
             $data['FieldValues'][$value->CustomFieldID] = $value;
         }
         return $data;
    }
 
    static function insertCustomFieldData($Object, $ObjectID)
    {
        $CI = &get_instance();

        if(isset($_POST['CustomFieldIDs']) && !empty($_POST['CustomFieldIDs'])){

            foreach ($_POST['CustomFieldIDs'] as $key => $CustomFieldID) {
           
                $query = $CI->db->get_where('CustomFields', array('OrganizationID' => $CI->LoginOrganizationID, 'CustomFieldID' => $CustomFieldID)); 
                $result = $query->row(); 
                
                if($result->FieldType == 'Checkbox'){
                    $FieldValue = implode(",", $_POST[$result->FieldName]);
                } else {
                    $FieldValue = $_POST[$result->FieldName];
                }

                if($FieldValue == "" || $FieldValue == NULL)
                    $FieldValue = $result->DefaultValue;

                $data = array(
                    'OrganizationID' =>  $CI->LoginOrganizationID,
                    $Object.'ID' => $ObjectID,
                    'FieldValue' => $FieldValue,
                    'CustomFieldID' => $CustomFieldID,
                ); 
                $result = $CI->db->insert($Object.'CustomFieldValues', $data);
            }
        }
    }

    static function updateCustomFieldData($Object, $ObjectID)
    {
        $CI = &get_instance();

       // if(isset($_POST['CustomFieldValueIDs']) && !empty($_POST['CustomFieldValueIDs'])){

         //   foreach ($_POST['CustomFieldValueIDs'] as $key => $CustomFieldValueID) {
        if(isset($_POST['CustomFieldIDs']) && !empty($_POST['CustomFieldIDs'])){

            foreach ($_POST['CustomFieldIDs'] as $key => $CustomFieldID) {

                $query = $CI->db->get_where('CustomFields', array('OrganizationID' => $CI->LoginOrganizationID, 'CustomFieldID' => $CustomFieldID)); 
                $result = $query->row(); 
                
                if($result->FieldType == 'Checkbox'){
                    $FieldValue = implode(",", $_POST[$result->FieldName]);
                } else {
                    $FieldValue = $_POST[$result->FieldName];
                }

                if($FieldValue == "" || $FieldValue == NULL)
                    $FieldValue = $result->DefaultValue;

                if(isset($_POST['CustomFieldValueIDs'][$CustomFieldID]) && !empty($_POST['CustomFieldValueIDs'][$CustomFieldID])){

                    $data = array(
                        'OrganizationID' =>  $CI->LoginOrganizationID,
                        $Object.'ID' => $ObjectID,
                        'FieldValue' => $FieldValue,
                        'CustomFieldID' => $CustomFieldID
                    ); 
                    $result = $CI->db->update($Object.'CustomFieldValues', $data, array($Object.'CustomFieldValueID' => $_POST['CustomFieldValueIDs'][$CustomFieldID]));

                } else {

                    $data = array(
                        'OrganizationID' =>  $CI->LoginOrganizationID,
                        $Object.'ID' => $ObjectID,
                        'FieldValue' => $FieldValue,
                        'CustomFieldID' => $CustomFieldID,
                    ); 
                    $result = $CI->db->insert($Object.'CustomFieldValues', $data);
                }
            }
        }
    }

    static function getRelatedData()
    {
        $CI=&get_instance();

        $query = $CI->db->select('wo.WorkOrderID as ID, wo.Subject as Name, wo.WorkOrderNo as No, wos.ColorCode, wop.Priority, wos.Status')
        ->from('WorkOrder wo')
        ->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left')
        ->join('WOPriority wop','wop.WOPriorityID = wo.WOPriority', 'left')
        ->where(array('wo.OrganizationID' => $CI->LoginOrganizationID))
        ->get();
        $results = $query->result();
        $WorkOrder = $results;

        foreach ($WorkOrder as $key => $value) {
            $ResData['WorkOrder'][$value->ID] = $value;
        }

        $query = $CI->db->select('AccountID as ID, AccountName as Name, AccountNo as No')
                ->from('Account')
                ->where(array('OrganizationID' => $CI->LoginOrganizationID))
                ->get();
        $results = $query->result();
        $Account = $results;

        foreach ($Account as $key => $value) {
            $ResData['Account'][$value->ID] = $value;
        }

        $query = $CI->db->select('e.EstimateID as ID, e.EstimateName as Name, e.EstimateNo as No, es.ColorCode, es.Status')
            ->from('Estimate e')
            ->join('EstimateStatus es','es.EstimateStatusID = e.EstimateStatus', 'left')
            ->where(array('e.OrganizationID' => $CI->LoginOrganizationID))
            ->get();
        $results = $query->result();
        $Estimate = $results;

        foreach ($Estimate as $key => $value) {
            $ResData['Estimate'][$value->ID] = $value;
        }

        $query = $CI->db->select('ContactID as ID, CONCAT(FirstName, " ", LastName) as Name, ContactNo as No')
                ->from('Contact')
                ->where(array('OrganizationID' => $CI->LoginOrganizationID))
                ->get();
        $results = $query->result();
        $Contact = $results;

        foreach ($Contact as $key => $value) {
            $ResData['Contact'][$value->ID] = $value;
        }

        $query = $CI->db->select('i.InvoiceID as ID, i.InvoiceNo as Name, i.InvoiceNo as No, is.InvoiceStatus as Status')
                ->from('Invoice i')
                ->join('InvoiceStatus is','is.InvoiceStatusID = i.InvoiceStatus', 'left')
                ->where(array('i.OrganizationID' => $CI->LoginOrganizationID))
                ->get();
        $results = $query->result();
        $Invoice = $results;

        foreach ($Invoice as $key => $value) {
            $ResData['Invoice'][$value->ID] = $value;
        }
        
        return $ResData; 
    }

    static function getRelatedDataByName()
    {
        $CI=&get_instance();

        $query = $CI->db->select('wo.WorkOrderID as ID, wo.Subject as Name')
        ->from('WorkOrder wo')
        ->where(array('wo.OrganizationID' => $CI->LoginOrganizationID))
        ->get();
        $results = $query->result();
        $WorkOrder = $results;

        foreach ($WorkOrder as $key => $value) {
            $ResData['WorkOrder'][preg_replace('/\s*/', '', strtoupper($value->Name))] = $value;
        }

        $query = $CI->db->select('AccountID as ID, AccountName as Name, AccountNo as No')
                ->from('Account')
                ->where(array('OrganizationID' => $CI->LoginOrganizationID))
                ->get();
        $results = $query->result();
        $Account = $results;

        foreach ($Account as $key => $value) {
            $ResData['Account'][preg_replace('/\s*/', '', strtoupper($value->Name))] = $value;
        }

        $query = $CI->db->select('e.EstimateID as ID, e.EstimateName as Name, e.EstimateNo as No')
            ->from('Estimate e')
            ->where(array('e.OrganizationID' => $CI->LoginOrganizationID))
            ->get();
        $results = $query->result();
        $Estimate = $results;

        foreach ($Estimate as $key => $value) {
            $ResData['Estimate'][preg_replace('/\s*/', '', strtoupper($value->Name))] = $value;
        }

        $query = $CI->db->select('ContactID as ID, CONCAT(FirstName, " ", LastName) as Name, ContactNo as No')
                ->from('Contact')
                ->where(array('OrganizationID' => $CI->LoginOrganizationID))
                ->get();
        $results = $query->result();
        $Contact = $results;

        foreach ($Contact as $key => $value) {
            $ResData['Contact'][preg_replace('/\s*/', '', strtoupper($value->Name))] = $value;
        }

        $query = $CI->db->select('i.InvoiceID as ID, i.InvoiceNo as Name, i.InvoiceNo as No')
                ->from('Invoice i')
                ->where(array('i.OrganizationID' => $CI->LoginOrganizationID))
                ->get();
        $results = $query->result();
        $Invoice = $results;

        foreach ($Invoice as $key => $value) {
            $ResData['Invoice'][preg_replace('/\s*/', '', strtoupper($value->Name))] = $value;
        }
        
        return $ResData; 
    }
 
    static function getChemicalSettings()
    {
        $CI=&get_instance();
        $query = $CI->db->select('EnableChemicals')
            ->from('ChemicalSettings')
            ->where(array('OrganizationID' => $CI->LoginOrganizationID))
            ->get();
        $result = $query->row();

        return empty($result) ? 0 : $result->EnableChemicals;
    }

    static function generateDocs($GenDocTemplateID, $TemplateContainsData, $ObjectID, $ObjectNo, $AssignedTo, $SaveToObject, $OutputFormat, $RelatedTo)
    {
        $CI = &get_instance();

        include_once APPPATH.'/libraries/DocxTemplate/src/DocxTemplate.php';

        $DOC_TEMPLATE_PATH = str_replace('{org_id}', $CI->LoginOrganizationID, DOC_TEMPLATE_PATH);
        $DOC_TEMPLATE_URL = str_replace('{org_id}', $CI->LoginOrganizationID, DOC_TEMPLATE_URL);

        extract($_POST);

        $query = $CI->db->get_where('GenDocTemplates', array('OrganizationID' => $CI->LoginOrganizationID, 'GenDocTemplateID' => $GenDocTemplateID)); 
        $GenDocTemplate = $query->row();

        if(!empty($GenDocTemplate)){

            // Start Code for Generate Docx File
            $FileName = pathinfo($ObjectNo.'-'.$GenDocTemplate->TemplateFile, PATHINFO_FILENAME);
            $TemplatePath = $DOC_TEMPLATE_PATH.$GenDocTemplate->TemplateFile;
            $OutputPath = $DOC_TEMPLATE_PATH.$FileName.".docx";
            $PDFOutputURL = $DOC_TEMPLATE_URL.$FileName.'.pdf';

            $docxTemplate = new DocxTemplate($TemplatePath); 
            
            file_exists($OutputPath)?unlink($OutputPath):"";
            
            //$CI->assertFalse(file_exists($OutputPath));

            $docxTemplate->merge($TemplateContainsData, $OutputPath);

            //$CI->assertTrue(file_exists($OutputPath)); 

            chmod($OutputPath, 0777);

            // End Code for Generate Docx File

            // Start Code for Generate PDF File 

            //require_once APPPATH.'/libraries/PdfGear/vendor/autoload.php';

            $PDFOutputPath = $DOC_TEMPLATE_PATH.$FileName.'.pdf';

            //Gears\Pdf::convert($OutputPath, $PDFOutputPath);
            MY_Controller::convertDocxToPDF($OutputPath, $PDFOutputPath); 

            chmod($PDFOutputPath, 0777);
            // End Code for Generate PDF File

            // Start Code for Save file into File Object 
            if(isset($SaveToObject) && $SaveToObject == 1){

                $FILE_PATH = str_replace('{org_id}', $CI->LoginOrganizationID, FILE_PATH);

                if($OutputFormat == "PDF"){

                    $NewFileName = $FileName.'_'.date('YmdHis').".pdf";
                    copy($PDFOutputPath, $FILE_PATH.$NewFileName);
                    $MimeType = mime_content_type($FILE_PATH.$NewFileName);
                    $FileSize = round(filesize($FILE_PATH.$NewFileName)/1048576, 2);

                } else if($OutputFormat == "Word"){

                    $NewFileName = $FileName.'_'.date('YmdHis').".docx";
                    copy($OutputPath, $FILE_PATH.$NewFileName);
                    $MimeType = mime_content_type($FILE_PATH.$NewFileName);
                    $FileSize = round(filesize($FILE_PATH.$NewFileName)/1048576, 2);
                }

                $data = array(
                    'OrganizationID' =>  $CI->LoginOrganizationID,
                    'Subject' => "Generated doc for ".$ObjectNo,
                    'RelatedTo' => $RelatedTo,
                    'What' => $ObjectID,
                    'AssignedTo' => $AssignedTo,
                    'ContentType' => $MimeType,
                    'Description' => 'Generated document.',
                    'FileName' => $NewFileName,
                    'FileSize' => $FileSize
                ); 
                
                $result = $CI->db->insert('File', $data); 
                  
                $FileID = $CI->db->insert_id();

                MY_Model::insertCreatedModifiedByAndDt($CI->LoginUserID, 'File', array('FileID' => $FileID));
            }
            // End Code for Save file into File Object 

            return $PDFOutputURL;
        } else {
            return 2;
        }
    }

    static function getViewFields($Object)
    {
        $CI=&get_instance();

        $query = $CI->db->get_where($Object.'ViewFields'); 
        
        return $query->result();
    }

    static function checkFilterConditions($FilterConditions, $FilterValues, $WhereField)
    {
        $CI = &get_instance();

        if($FilterConditions == 'Equals'){
            return $CI->db->where($WhereField.' =',  $FilterValues);  
        } else if($FilterConditions == 'NotEqualTo'){
            return $CI->db->where($WhereField.' !=', $FilterValues);   
        } else if($FilterConditions == 'LessThan'){
            return $CI->db->where($WhereField.' <', $FilterValues);   
        } else if($FilterConditions == 'GreaterThan'){
            return $CI->db->where($WhereField.' >', $FilterValues);   
        } else if($FilterConditions == 'LessOREqualTo'){
            return $CI->db->where($WhereField.' <=', $FilterValues);   
        } else if($FilterConditions == 'GreaterOREqualTo'){
            return $CI->db->where($WhereField.' >=', $FilterValues);   
        } else if($FilterConditions == 'Contains'){
            return $CI->db->like($WhereField, $FilterValues);   
        } else if($FilterConditions == 'StartsWith'){ 
            return $CI->db->like($WhereField, $FilterValues, 'after');   
        } else if($FilterConditions == 'DoesNotContain'){
            return $CI->db->not_like($WhereField, $FilterValues);   
        } 
    }

    /*static function getObjectWithCustomFieldData($Object)
    {

        // Pivot table - vertical data to horizontal
        //   SELECT ac.*, accf.* FROM
        //       (SELECT a.AccountID, a.FieldValue "Text", b.FieldValue "Date", c.FieldValue "Checkbox"
        //      FROM AccountCustomFieldValues a, AccountCustomFieldValues b, AccountCustomFieldValues c
        //        WHERE a.CustomFieldID=1
        //        AND b.CustomFieldID=2
        //       AND c.CustomFieldID=10
        //        AND a.AccountID=b.AccountID
        //       AND a.AccountID=c.AccountID)  accf
        //        RIGHT JOIN Account ac ON accf.AccountID = ac.AccountID
        

        $CI=&get_instance();

        $query = $CI->db->get_where('CustomFields', array('OrganizationID' => $CI->LoginOrganizationID, 'IsDeleted' => 0, 'Object' => $Object)); 
        
        $CustomFields = $query->result();


        $FieldName = "";
        $TableAlias = "";
        $Where = "";
        $Where2 = "";

        foreach($CustomFields as $key => $CustomField){
            $FieldName .= " , ac".$key.".FieldValue '".$CustomField->FieldName."'";

            $TableAlias .= ", ".$Object."CustomFieldValues ac".$key;

            $Where .= " AND ac".$key.".CustomFieldID=".$CustomField->CustomFieldID;

            $Where2 .= ($key != 0) ?  " AND ac0.".$Object."ID = ac".($key).".".$Object."ID" : "";
        }

        $query = "SELECT ac.*, accf.* FROM
                ( 
                    SELECT ac".$key.".".$Object."ID $FieldName 
                    FROM ".$Object."CustomFieldValues $TableAlias
                    WHERE ac".$key.".OrganizationID = ".$CI->LoginOrganizationID."
                    $Where
                    $Where2
                )  accf
                RIGHT JOIN ".$Object." ac ON accf.".$Object."ID = ac.".$Object."ID";
        
        $query = $CI->db->query($query);  
        return $query->result();
    }*/
}