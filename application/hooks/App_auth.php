<?php
class App_auth
{
    private $CI;

    function __construct() { 
    	$this->CI = &get_instance();
    }

    function index()
    { 
	    if ($this->CI->session->userdata('UserID') == "")  // If no session found redirect to login page.
	    { 
	    	redirect('User/Auth/');
	    }
	}
}