<?php

class ViewModel extends MY_Model{
 
    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }
 
    function ViewContacts() 
    {
        /*if(isset($_GET['flag']) && $_GET['flag'] == 'ContactsCreatedThisWeek'){*/
            $aColumns = array('c.ContactID','CONCAT(ast.FirstName, " ", ast.LastName)','CONCAT(c.FirstName, " ", c.LastName)', 'a.AccountName','c.PhoneNo', 'c.MailingCity', 'c.MailingState', 'DATE_FORMAT(c.BirthDate, "'.RES_DATE.'")','c.Notes','DATE_FORMAT(c.CreatedDate, "'.RES_DATETIME.'")','CONCAT(crby.FirstName, " ", crby.LastName)', 'c.Account', 'c.AssignedTo', 'c.CreatedBy');

            $bColumns = array('ContactID','CONCAT(ast.FirstName, " ", ast.LastName)', 'CONCAT(c.FirstName, " ", c.LastName)', 'AccountName', 'PhoneNo', 'MailingCity', 'MailingState','DATE_FORMAT(c.BirthDate, "'.RES_DATE.'")','Notes','DATE_FORMAT(c.CreatedDate, "'.RES_DATETIME.'")','CONCAT(crby.FirstName, " ", crby.LastName)', 'Account', 'AssignedTo', 'CreatedBy');
       /* } else {

            $aColumns = array('c.ContactID','CONCAT(ast.FirstName, " ", ast.LastName)','CONCAT(c.FirstName, " ", c.LastName)', 'a.AccountName', 'c.PhoneNo', 'c.MailingCity', 'c.MailingState', 'DATE_FORMAT(c.BirthDate, "'.RES_DATE.'")','c.Notes','c.Description', 'c.Account');

            $bColumns = array('ContactID','CONCAT(ast.FirstName, " ", ast.LastName)', 'CONCAT(c.FirstName, " ", c.LastName)', 'AccountName', 'PhoneNo', 'MailingCity', 'MailingState','DATE_FORMAT(c.BirthDate, "'.RES_DATE.'")','Notes','Description', 'Account');
        }*/
      
        
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "c.ContactID";

        /* DB table to use */
        $sTable = "Contact c
        LEFT JOIN User ast ON ast.UserID = c.AssignedTo
        LEFT JOIN User crby ON crby.UserID = c.CreatedBy
        LEFT JOIN Account a ON a.AccountID = c.Account";

        /* Limit */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".( $_GET['iDisplayStart'] ).", ".
                        ( $_GET['iDisplayLength'] );
        }

        /* Ordering */
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                    {
                            $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                                    ".( $_GET['sSortDir_'.$i] ) .", ";
                    }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                    $sOrder = "";
            }
        }

        /* Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited */
        $sWhere = "";
        if ( $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                        $sWhere .= $aColumns[$i]." LIKE '%".( $_GET['sSearch'] )."%' OR ";
                }
                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ')';
        }
        
        /* Where Condition */
        $UserID = $this->session->userdata('UserID');
        if(isset($_GET['flag']) && $_GET['flag'] == 'MyActiveContacts')
            $where_str = "c.AssignedTo = $UserID and c.OrganizationID='".$this->LoginOrganizationID."' and c.IsDeleted=0 and c.IsActive=1";
        else if(isset($_GET['flag']) && $_GET['flag'] == 'ContactsCreatedThisWeek'){
            $where_str = "c.IsDeleted=0 and (YEARWEEK(c.CreatedDate, 1) = YEARWEEK(CURDATE(), 1)) and c.OrganizationID='".$this->LoginOrganizationID."' and c.IsActive=1";
        } else {
            $where_str = "c.IsDeleted=0 and c.OrganizationID='".$this->LoginOrganizationID."' and c.IsActive=1";
        }
        
        if ( $sWhere == "" )
        {
                $sWhere = "WHERE $where_str";
        }
        else
        {
                $sWhere .= "AND $where_str";
        }
        
        /* SQL queries Get data to display */
        $sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit";  
        
        $rResult = $this->db->query($sQuery); 
        $rResult = $rResult->result_array(); 
      
        /* Data set length after filtering */
        $sQuery = "SELECT FOUND_ROWS() as cnt";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iFilteredTotal = $result->cnt;

        /* Total data set length */
        $sQuery = "SELECT COUNT(".$sIndexColumn.") as cnt
                FROM   $sTable  $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iTotal = $result->cnt;

        /* Output */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );

        foreach ( $rResult as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($bColumns) ; $i++ )
            {
                if($i == 0)
                {
                    $row[] = '<a href="'.SITE_URL.'Contact/Actions/EditContact/'.$aRow[$bColumns[0]].'"><i class="fa fa-pencil-square-o"></i></a>';
                } 
                else if($i == 1)
                {
                     $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['AssignedTo'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($i == 10)
                {
                     $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['CreatedBy'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($bColumns[$i] == 'AccountName')
                {
                    $row[] = '<a href="'.SITE_URL.'Account/Actions/AccountDetails/'.$aRow['Account'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($i == 2)
                {
                    $row[] = '<a href="'.SITE_URL.'Contact/Actions/ContactDetails/'.$aRow[$bColumns[0]].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if ( $bColumns[$i] != ' ' )
                {
                    $row[] = $aRow[$bColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode( $output );
    } 

    function CreateNewView()
    {
        extract($_POST);

        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'ContactViewName' => $ContactViewName,
            'UserID' => $this->LoginUserID,
            'RestrictVisibility' => $RestrictVisibility,
            'SpecifyFieldsDisplay' => implode(",", $SpecifyFieldsDisplay),
            'CreatedDate' => date("Y-m-d H:i:s"),
            'LastModifiedDate' => date("Y-m-d H:i:s")
        );     
        $query = $this->db->insert('ContactView', $data);  
        $ContactViewID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($this->LoginUserID, 'ContactView', array('ContactViewID' => $ContactViewID));

        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'ContactViewID' => $ContactViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('ContactFilter', $data);  
                }
            } 
 
        }
        return $ContactViewID;
    }

    function EditCustomView($ContactViewID)
    {  
        extract($_POST);
  
        $data = array(
            'ContactViewName' => $ContactViewName,
            'UserID' => $this->LoginUserID,
            'RestrictVisibility' => $RestrictVisibility,
            'SpecifyFieldsDisplay' => implode(",", $SpecifyFieldsDisplay),
            'CreatedDate' => date("Y-m-d H:i:s"),
            'LastModifiedDate' => date("Y-m-d H:i:s")
        );     
        $query = $this->db->update('ContactView', $data, array('ContactViewID' => $ContactViewID));  

        $this->db->delete('ContactFilter', array('ContactViewID' => $ContactViewID));  
        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'ContactViewID' => $ContactViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('ContactFilter', $data);  
                }
            } 
 
        }
        return $query;
    }

    function GetContactViewData($ContactViewID){

        $query = $this->db->get_where('ContactView', array('ContactViewID' => $ContactViewID)); 
        $ContactViewData = $query->row();

        $query = $this->db->get_where('ContactFilter', array('ContactViewID' => $ContactViewID)); 
        $ContactFilterData = $query->result_array();

        $ColName = "c.ContactID, c.Account, c.AssignedTo as AssignedToID, c.CreatedBy as CreatedByID, c.LastModifiedBy as LastModifiedByID, ";
        if(!empty($ContactViewData->SpecifyFieldsDisplay)){
            $SpecifyFieldsDisplays = explode(",", $ContactViewData->SpecifyFieldsDisplay);
            foreach ($SpecifyFieldsDisplays as $key => $value) {
                if($value == 'AssignedTo'){
                    $ColName .= "CONCAT(ast.FirstName, ' ', ast.LastName) as AssignedTo";
                } else if($value == 'Account'){
                    $ColName .= "a.AccountName";
                } else if($value == 'OrganizationID'){
                    $ColName .= "o.CompanyName";
                } else if($value == 'LeadSource'){
                    $ColName .= "ls.LeadSource";
                } else if($value == 'CreatedBy'){
                    $ColName .= "CONCAT(cr.FirstName, ' ', cr.LastName) as CreatedBy";
                } else if($value == 'LastModifiedBy'){
                    $ColName .= "CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedBy";
                } else if($value == 'BirthDate'){
                    $ColName .= "DATE_FORMAT(c.BirthDate, '".RES_DATE."') as BirthDate";
                } else {
                    $ColName .= "c.".$value;
                }

                if($key != count($SpecifyFieldsDisplays)){
                    $ColName .= ", ";
                }
            }
        }
              
         /* Filter Contact Data Start */
            $this->db->select($ColName);
            $this->db->from('Contact c');
            $this->db->join('User ast','ast.UserID = c.AssignedTo', 'left');
            $this->db->join('Account a','a.AccountID = c.Account', 'left');
            $this->db->join('User cr','cr.UserID = c.CreatedBy', 'left');
            $this->db->join('User lmb','lmb.UserID = c.LastModifiedBy', 'left');
            $this->db->join('LeadSource ls','ls.LeadSourceID = c.LeadSource', 'left');
            $this->db->join('Organization o','o.OrganizationID = c.OrganizationID', 'left');
            $this->db->where(array('c.OrganizationID' => $this->LoginOrganizationID, 'c.IsActive' => 1, 'c.IsDeleted' => 0));

            foreach ($ContactFilterData as $key => $value) {

                $WhereField = "";
                if($value['FilterField'] == 'AssignedTo'){
                    $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                } else if($value['FilterField'] == 'Account') {
                    $WhereField = "a.AccountName";
                } else if($value['FilterField'] == 'CreatedBy') {
                    $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                } else if($value['FilterField'] == 'LastModifiedBy') {
                    $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                } else if($value['FilterField'] == 'OrganizationID') {
                    $WhereField = "o.CompanyName";
                } else if($value['FilterField'] == 'LeadSource') {
                    $WhereField = "ls.LeadSource";
                } else if($value['FilterField'] == 'BirthDate'){
                     $WhereField = "DATE_FORMAT(c.BirthDate, '".RES_DATE."')";
                } else {
                    $WhereField = "c.".$value['FilterField'];
                }

                if($value['FilterCondition'] == 'Equals'){
                    $this->db->where($WhereField.' =',  $value['FilterValue']);  
                } else if($value['FilterCondition'] == 'NotEqualTo'){
                    $this->db->where($WhereField.' !=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'LessThan'){
                    $this->db->where($WhereField.' <', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'GreaterThan'){
                    $this->db->where($WhereField.' >', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'LessOREqualTo'){
                    $this->db->where($WhereField.' <=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'GreaterOREqualTo'){
                    $this->db->where($WhereField.' >=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'Contains'){
                    $this->db->like($WhereField, $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'StartsWith'){ 
                    $this->db->like($WhereField, $value['FilterValue'], 'after');   
                } else if($value['FilterCondition'] == 'DoesNotContain'){
                    $this->db->not_like($WhereField, $value['FilterValue']);   
                } 
            } 
            $query = $this->db->get();  
           // echo $this->db->last_query();exit;
            $data['ColumnsNames'] = $SpecifyFieldsDisplays;
            $data['ColumnsValues'] = $query->result();
            return $data; 
    }

    function GetCustomView($ContactViewID)
    {
        $query = $this->db->get_where('ContactView', array('ContactViewID' => $ContactViewID)); 
        
        $data['ContactViewData'] = $query->row();

        $query = $this->db->get_where('ContactFilter', array('ContactViewID' => $ContactViewID)); 
        
        $data['ContactFilterData'] = $query->result();

        return $data;
    }

    /*function GetContactFilter()
    {
        $query = $this->db->get_where('ContactFilter', array('UserID' => $this->LoginUserID)); 
        
        return $query->result();
    }*/

    function GetViews(){

        $query = $this->db->query("SELECT * FROM ContactView WHERE ((RestrictVisibility = 'VisibleOnlyToMe' AND UserID = '".$this->LoginUserID."') OR (RestrictVisibility = 'VisibleToEveryone')) AND OrganizationID='".$this->LoginOrganizationID."' ORDER BY ContactViewName ASC");
        
        return $query->result();
    }

    function ContactViewFields()
    {
        $query = $this->db->get_where('ContactViewFields'); 
        
        return $query->result();
    }

    function DeleteCustomView($ContactViewID)
    {
        return $this->db->delete('ContactView', array('ContactViewID' => $ContactViewID)); 
    }

    function CopyCustomView($ContactViewID)
    {
       $query = $this->db->get_where('ContactView', array('ContactViewID' => $ContactViewID)); 
      
        foreach ($query->result() as $row){   
           foreach($row as $key=>$val){ 
                if($key != 'ContactViewID'){    
                    $this->db->set($key, $val); 
                }                
           }
        }
        $this->db->insert('ContactView');
        $NewContactViewID = $this->db->insert_id();

        $query = $this->db->get_where('ContactFilter', array('ContactViewID' => $ContactViewID)); 
       
        foreach ($query->result() as $row){   
           foreach($row as $key => $val){ 
                if($key == 'ContactViewID'){  
                    $this->db->set($key, $NewContactViewID);  
                } else if($key != 'ContactFilterID'){    
                    $this->db->set($key, $val); 
                }             
           }
           $this->db->insert('ContactFilter');
        }

        return 1; 
    }

    function RenameCustomView($ContactViewID){

        $data = array(
            'ContactViewName' => $_POST['ContactViewName']
        );

        return $this->db->update('ContactView', $data, array('ContactViewID' => $ContactViewID));  
    }

    function EditSharingCustomView($ContactViewID){

        $data = array(
            'RestrictVisibility' => $_REQUEST['RestrictVisibility']
        );

        return $this->db->update('ContactView', $data, array('ContactViewID' => $ContactViewID));  
    }

    function EditFiltersCustomView($ContactViewID){ 

        extract($_POST);

        $this->db->delete('ContactFilter', array('ContactViewID' => $ContactViewID));  
        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'ContactViewID' => $ContactViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('ContactFilter', $data);  
                }
            } 
 
        }
        return $query; 
    }

    function EditDisplayedColumnsCustomView($ContactViewID){

        $data = array(
            'SpecifyFieldsDisplay' => implode(",", $_REQUEST['SpecifyFieldsDisplay'])
        );

        return $this->db->update('ContactView', $data, array('ContactViewID' => $ContactViewID));  
    }
    
}

?>
