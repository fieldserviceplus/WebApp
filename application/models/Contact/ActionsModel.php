<?php
 
class ActionsModel extends MY_Model{

    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }
 
    function RecentContacts() 
    {
        $aColumns = array('c.ContactID','CONCAT(c.FirstName, " ", c.LastName)', 'c.PhoneNo', 'a.AccountName', 'c.MailingCity', 'c.MailingState', 'CONCAT(ast.FirstName, " ", ast.LastName)', 'c.AssignedTo');
        $bColumns = array('ContactID','CONCAT(c.FirstName, " ", c.LastName)', 'PhoneNo' , 'AccountName', 'MailingCity', 'MailingState', 'CONCAT(ast.FirstName, " ", ast.LastName)', 'AssignedTo');
        
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "c.ContactID";

        /* DB table to use */
        $sTable = "Contact c
        LEFT JOIN User ast ON ast.UserID = c.AssignedTo
        LEFT JOIN Account a ON a.AccountID = c.Account";

        /* Limit */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".( $_GET['iDisplayStart'] ).", ".
                        ( $_GET['iDisplayLength'] );
        }

        /* Ordering */
        $sOrder = "ORDER BY c.LastModifiedDate DESC";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                    {
                            $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                                    ".( $_GET['sSortDir_'.$i] ) .", ";
                    }
            }
           
            $sOrder = substr_replace( $sOrder, "", -2 );

            if ( $sOrder == "ORDER BY" )
            {
                    $sOrder = "";
            } 
        }

        /* Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited */
        $sWhere = "";
        if ( $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                        $sWhere .= $aColumns[$i]." LIKE '%".( $_GET['sSearch'] )."%' OR ";
                }
                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ')';
        }
        
        /* Where Condition */

        $where_str = "c.OrganizationID='".$this->LoginOrganizationID."' and c.IsActive=1 and c.IsDeleted=0";
        if ( $sWhere == "" )
        {
                $sWhere = "WHERE $where_str";
        }
        else
        {
                $sWhere .= "AND $where_str";
        }
        
        /* SQL queries Get data to display */
        $sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit"; 
        
        $rResult = $this->db->query($sQuery); 
        $rResult = $rResult->result_array(); 
      
        /* Data set length after filtering */
        $sQuery = "SELECT FOUND_ROWS() as cnt";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iFilteredTotal = $result->cnt;

        /* Total data set length */
        $sQuery = "SELECT COUNT(".$sIndexColumn.") as cnt
                FROM   $sTable  $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iTotal = $result->cnt;

        /* Output */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );

        foreach ( $rResult as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($bColumns) ; $i++ )
            {
                if($i == 0)
                {
                    $row[] = '';
                } 
                else if($i == 1)
                {
                    $row[] = '<a href="'.SITE_URL.'Contact/Actions/ContactDetails/'.$aRow[$bColumns[0]].'">'.$aRow[$bColumns[$i]].'</a>';
                }
                else if($bColumns[$i] == 'AccountName')
                {
                    $row[] = '<a href="'.SITE_URL.'Account/Actions/AccountDetails/'.$aRow[$bColumns[0]].'">'.$aRow[$bColumns[$i]].'</a>';
                }
                else if($i == 6)
                {
                    $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['AssignedTo'].'">'.$aRow[$bColumns[$i]].'</a>';
                }
                else if ( $bColumns[$i] != ' ' )
                {
                    $row[] = $aRow[$bColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode( $output );
    } 

    function CreateContact() {
               
        extract($_POST); 

        $UserID = $this->LoginUserID;
       
        $data = array(
            'OrganizationID' =>  $this->LoginOrganizationID,
            'Salutation' => $Salutation,
            'FirstName' => $FirstName,
            'LastName' => $LastName,
            'Name' => $FirstName.' '.$LastName,
            'Title' => $Title,
            'AssignedTo' => $AssignedTo,
            'Account' => $Account,
            'PhoneNo' => $PhoneNo,
            'MobileNo' => $MobileNo,
            'Email' => $Email,
            'LeadSource' => $LeadSource,
            'DoNotCall' => !empty($DoNotCall)?$DoNotCall:0,
            'IsActive' => !empty($IsActive)?$IsActive:0,
            'Notes' => $Notes,
            'MailingAddress' => $MailingAddress,
            'MailingCity' => $MailingCity,
            'MailingState' => $MailingState,
            'MailingCountry' => $MailingCountry,
            'MailingPostalCode' => $MailingPostalCode,
            'MailingLatitude' => (double)$MailingLatitude,
            'MailingLongitude' => (double)$MailingLongitude,
            'BirthDate' => !empty($BirthDate)?date("Y-m-d",strtotime($BirthDate)):NULL
        ); 
        $result = $this->db->insert('Contact', $data);    

        $ContactID = $this->db->insert_id();

        $ContactNo = MY_Controller::genCode('CT', $ContactID);
        $this->db->update('Contact', array('ContactNo' => $ContactNo), array('ContactID' => $ContactID));  

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Contact', array('ContactID' => $ContactID));

        /* For insert custom fields */ 
        MY_Model::insertCustomFieldData('Contact', $ContactID);
    
        return $ContactID;
    } 

    function EditContact($ContactID) {

        extract($_POST);          

        $UserID = $this->LoginUserID;

        $data = array(
            'Salutation' => $Salutation,
            'FirstName' => $FirstName,
            'LastName' => $LastName,
            'Name' => $FirstName.' '.$LastName,
            'Title' => $Title,
            'AssignedTo' => $AssignedTo,
            'Account' => $Account,
            'PhoneNo' => $PhoneNo,
            'MobileNo' => $MobileNo,
            'Email' => $Email,
            'LeadSource' => $LeadSource,
            'DoNotCall' => !empty($DoNotCall)?$DoNotCall:0,
            'IsActive' => !empty($IsActive)?$IsActive:0,
            'Notes' => $Notes,
            'MailingAddress' => $MailingAddress,
            'MailingCity' => $MailingCity,
            'MailingState' => $MailingState,
            'MailingCountry' => $MailingCountry,
            'MailingPostalCode' => $MailingPostalCode,
            'MailingLatitude' => (double)$MailingLatitude,
            'MailingLongitude' => (double)$MailingLongitude,
            'BirthDate' => !empty($BirthDate)?date("Y-m-d",strtotime($BirthDate)):NULL
        ); 
       
        $result = $this->db->update('Contact', $data, array('ContactID' => $ContactID));    
   
        MY_Model::updateCreatedModifiedByAndDt($UserID,'Contact', array('ContactID' => $ContactID));
    
        /* For insert custom fields */ 
        MY_Model::updateCustomFieldData('Contact', $ContactID);
        
        return $result;
    } 

    function getContactData($ContactID, $RelatedObjName = 'AllObject')
    {

        $result = $this->db->update('Contact', array('LastModifiedDate' => date("Y-m-d H:i:s")), array('OrganizationID' => $this->LoginOrganizationID,'ContactID' => $ContactID));  

        $query = $this->db->select('c.*, CONCAT(c.FirstName, " ", c.LastName) as Title, CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName, ls.LeadSource as LeadSourceName, a.AccountName, DATE_FORMAT(c.BirthDate, "'.RES_DATE.'") as BirthDate, DATE_FORMAT(c.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,DATE_FORMAT(c.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName, c.LastModifiedBy, c.CreatedBy')
        ->from('Contact c')
        ->join('User cb','cb.UserID = c.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = c.LastModifiedBy', 'left')
        ->join('User ast','ast.UserID = c.AssignedTo', 'left')
        ->join('Account a','a.AccountID = c.Account', 'left')
        ->join('LeadSource ls','ls.LeadSourceID = c.LeadSource', 'left')
        ->where(array('c.OrganizationID' => $this->LoginOrganizationID, 'c.ContactID' => $ContactID)) 
        ->get();

        $data['ContactData'] = $query->row();

        if($RelatedObjName == 'WorkOrders'  || $RelatedObjName == 'AllObject'){
            $this->db->select('wo.WorkOrderID, wot.WorkOrderType, wo.WorkOrderNo, wo.Subject, wop.Priority, wos.Status, DATE_FORMAT(wo.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, wo.AssignedTo');
            $this->db->from('WorkOrder wo');
            $this->db->join('WorkOrderType wot','wot.WorkOrderTypeID = wo.WorkOrderType', 'left');
            $this->db->join('User at','at.UserID = wo.AssignedTo', 'left');
            $this->db->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left');
            $this->db->join('WOPriority wop','wop.WOPriorityID = wo.WOPriority', 'left');
            $this->db->where(array('wo.OrganizationID' => $this->LoginOrganizationID, 'wo.PrimaryContact' => $ContactID, 'wo.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get();   
            $data['WorkOrders'] = $query->result();
        }

        if($RelatedObjName == 'Estimates'  || $RelatedObjName == 'AllObject'){
            $this->db->select('e.EstimateID ,e.EstimateNo, e.EstimateName, DATE_FORMAT(e.ExpirationDate, "'.RES_DATE.'") as ExpirationDate, (Select SUM(TotalPrice) FROM EstimateLine WHERE Estimate=e.EstimateID) as GrandTotal, es.Status, CONCAT(o.FirstName, " ", o.LastName) as OwnerName, DATE_FORMAT(e.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, e.Owner');
            $this->db->from('Estimate e');
            $this->db->join('User o','o.UserID = e.Owner', 'left');
            $this->db->join('EstimateStatus es','es.EstimateStatusID = e.EstimateStatus', 'left');
            $this->db->where(array('e.OrganizationID' => $this->LoginOrganizationID, 'e.Contact' => $ContactID, 'e.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                    $this->db->limit(10);
            $query = $this->db->get();   
            $data['Estimates'] = $query->result();
        }

        if($RelatedObjName == 'Invoices'  || $RelatedObjName == 'AllObject'){
            $this->db->select('i.InvoiceID, i.InvoiceNo, (Select SUM(TotalPrice) FROM InvoiceLineItem WHERE Invoice=i.InvoiceID) as TotalPrice, (Select SUM(SubTotal) FROM InvoiceLineItem WHERE Invoice=i.InvoiceID) as SubTotal, is.InvoiceStatus, DATE_FORMAT(i.InvoiceDate, "'.RES_DATE.'") as InvoiceDate, DATE_FORMAT(i.DueDate, "'.RES_DATE.'") as DueDate, wo.Subject as WorkOrderName, i.WorkOrder');
            $this->db->from('Invoice i');
            $this->db->join('InvoiceStatus is','is.InvoiceStatusID = i.InvoiceStatus', 'left');
            $this->db->join('WorkOrder wo','wo.WorkOrderID = i.WorkOrder', 'left');
            $this->db->where(array('i.OrganizationID' => $this->LoginOrganizationID, 'i.Contact' => $ContactID, 'i.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                    $this->db->limit(10);
            $query = $this->db->get();   
            $data['Invoices'] = $query->result();
        }

        if($RelatedObjName == 'Events'  || $RelatedObjName == 'AllObject'){
            $this->db->select('e.EventID, e.Who, e.Subject, DATE_FORMAT(e.EventStartDate, "'.RES_DATETIME.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATETIME.'") as EventEndDate, DATE_FORMAT(e.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, CONCAT(cr.FirstName, " ", cr.LastName) as CreatedByName, CONCAT(cn.FirstName, " ", cn.LastName) as ContactName, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, e.AssignedTo, e.CreatedBy');
            $this->db->from('Event e'); 
            $this->db->join('User cr','cr.UserID = e.CreatedBy', 'left');
            $this->db->join('Contact cn','cn.ContactID = e.Who', 'left');
            $this->db->join('User at','at.UserID = e.AssignedTo', 'left');
            $this->db->where(array('e.RelatedTo' => 'Contact', 'e.What' => $ContactID, 'e.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                    $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Events'] = $query->result(); 
        }

        if($RelatedObjName == 'Tasks'  || $RelatedObjName == 'AllObject'){
            $this->db->select('t.TaskID, t.Who, t.Subject, DATE_FORMAT(t.DueDate, "'.RES_DATETIME.'") as DueDate, ts.TaskStatus, tp.Priority, CONCAT(cn.FirstName, " ", cn.LastName) as ContactName, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, tt.TaskType, t.Who, t.AssignedTo');
            $this->db->from('Task t');
            $this->db->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left');
            $this->db->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left');
            $this->db->join('Contact cn','cn.ContactID = t.Who', 'left');
            $this->db->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left');
            $this->db->join('User at','at.UserID = t.AssignedTo', 'left');
            $this->db->group_start();
                $this->db->or_group_start();
                    $this->db->where(array('t.RelatedTo' => 'Contact', 't.What' => $ContactID, 't.IsDeleted' => 0));
                $this->db->group_end();
                $this->db->or_group_start();
                         $this->db->where(array('t.Who' => $ContactID, 't.IsDeleted' => 0));
                $this->db->group_end();
            $this->db->group_end() ;
            if($RelatedObjName == 'AllObject')
                    $this->db->limit(10);
            $query = $this->db->get();
            $data['Tasks'] = $query->result();
        }  

        if($RelatedObjName == 'Files'  || $RelatedObjName == 'AllObject'){
            $this->db->select('f.FileID, f.FileName, f.Subject, DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, f.ContentType, CONCAT(cr.FirstName, " ", cr.LastName) as CreatedByName, f.CreatedBy');
            $this->db->from('File f'); 
            $this->db->join('User cr','cr.UserID = f.CreatedBy', 'left');
            $this->db->where(array('f.OrganizationID' => $this->LoginOrganizationID, 'f.RelatedTo' => 'Contact', 'f.What' => $ContactID, 'f.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                    $this->db->limit(10);
            $query = $this->db->get();   
            $data['Files'] = $query->result();
        }

        if($RelatedObjName == 'Notes'  || $RelatedObjName == 'AllObject'){
           $this->db->select('n.NoteID, n.Subject, DATE_FORMAT(n.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, CONCAT(o.FirstName, " ", o.LastName) as OwnerName, n.Owner');
            $this->db->from('Note n'); 
            $this->db->join('User o','o.UserID = n.Owner', 'left');
            $this->db->where(array('n.What' => $ContactID,'n.RelatedTo' => 'Contact', 'n.OrganizationID' => $this->LoginOrganizationID, 'n.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Notes'] = $query->result();
        }

        return $data;
    }

    /*function CreateNote($ContactID)
    {
        extract($_POST); 

        $UserID = $this->LoginUserID;
  
        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'Owner' => $UserID,
            'What' => $ContactID,
            'Subject' => $Subject,
            'Body' => $Body,
            'RelatedTo' => 'Contact'
        );
        $result = $this->db->insert('Note', $data);    
        $NoteID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Note', array('NoteID' => $NoteID));
    
        return $result;
    }*/

    /*function SendEmail($ContactID)
    { 
        extract($_POST); 

        $UserID = $this->LoginUserID;

        if(!empty($EmailTemplate)){
            $query = $this->db->get_where('EmailTemplates', array('EmailTemplateID' => $EmailTemplate, 'IsDeleted' => 0)); 
            if($query->num_rows() > 0){
                $result = $query->row();
                $NewEmailBody = str_replace("{{EMAIL_CONTAINS}}", $EmailBody, $result->Body);
            }
        }
       
        $this->load->library('email');
        $this->email->set_newline("\r\n");
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['smtp_user'] = SMTP_USER;
        $config['smtp_pass'] = SMTP_PASS;
        $config['smtp_crypto'] = SMTP_CRYPTO; 
        $config['protocol'] = PROTOCOL; 
        $config['mailtype'] = 'html'; 
        $config['send_multipart'] = FALSE;
        $this->email->initialize($config);
        $this->email->from(ADMIN_EMAIL); 
        $this->email->to($ToEmailID);
        $this->email->subject($EmailSubject);
        $this->email->message($NewEmailBody);
       
        if(!empty($_FILES["EmailAttachment"]['name'][0])){

            $files  = $_FILES;
            $file_count    = count($_FILES['EmailAttachment']['name']);
            for($i = 0; $i < $file_count; $i++)
            {
                $_FILES['EmailAttachment']['name']        = $files['EmailAttachment']['name'][$i];
                $_FILES['EmailAttachment']['type']        = $files['EmailAttachment']['type'][$i];
                $_FILES['EmailAttachment']['tmp_name']    = $files['EmailAttachment']['tmp_name'][$i];
                $_FILES['EmailAttachment']['error']        = $files['EmailAttachment']['error'][$i];
                $_FILES['EmailAttachment']['size']        = $files['EmailAttachment']['size'][$i];

                $new_image_name = 'file_'. uniqid().'_'.date('YmdHis');
                $config = array(
                 'upload_path' => 'tmp_mail_images/',
                 'file_name' => $new_image_name,
                 'allowed_types' => "jpg|jpeg|png|doc|pptx|xlsx|docx|zip",
                 'overwrite' => TRUE,
                 'max_size' => "2048000"   //'max_height' => "768", 'max_width' => "1024" // Can be set to particular file size , here it is 2 MB(2048 Kb)
                );
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
               
                if($this->upload->do_upload('EmailAttachment')){
                    $fileData = $this->upload->data();
                    chmod('tmp_mail_images/'.$fileData['file_name'], 0777);
                    $file_name = $fileData['file_name'];
                    $this->email->attach('tmp_mail_images/'.$file_name);
                }
            }
        }
        return $this->email->send();
    }*/

    function GetUsers()
    { 
        $query = $this->db->get_where('User', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetAccounts()
    { 
        $query = $this->db->get_where('Account', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetSalutation()
    {
        $query = $this->db->get_where('Salutation', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetLeadSources()
    {
        $query = $this->db->get_where('LeadSource', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    /*function GetEmailTemplates()
    {
        $query = $this->db->get_where('EmailTemplates', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }*/

    function GetViews(){

        $query = $this->db->query("SELECT * FROM ContactView WHERE ((RestrictVisibility = 'VisibleOnlyToMe' AND UserID = '".$this->LoginUserID."') OR (RestrictVisibility = 'VisibleToEveryone')) AND OrganizationID='".$this->LoginOrganizationID."' ORDER BY ContactViewName ASC");
        
        return $query->result();
    }

    function DeleteContact($ContactID)
    {
        return $this->db->update('Contact', array('IsDeleted' => 1), array('ContactID' => $ContactID));  
    }
}

?>
