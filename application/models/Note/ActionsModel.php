<?php
 
class ActionsModel extends MY_Model{

    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }

    function EditNote($NoteID) {

        extract($_POST);          

        $UserID = $this->LoginUserID;
    
        $data = array(
            'OrganizationID' =>  $this->LoginOrganizationID,
            'Subject' => $Subject,
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            'Owner' => $Owner,
            'Body' => $Body
        ); 
        
        $result = $this->db->update('Note', $data, array('NoteID' => $NoteID));    

        MY_Model::updateCreatedModifiedByAndDt($UserID,'Note', array('NoteID' => $NoteID));

        return $result;
    } 

    function GetNoteData($NoteID)
    {
        $query = $this->db->select('n.*, n.Subject as Title, CONCAT(o.FirstName, " ", o.LastName) as OwnerName, DATE_FORMAT(n.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,DATE_FORMAT(n.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName')
        ->from('Note n')
        ->join('User cb','cb.UserID = n.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = n.LastModifiedBy', 'left')
        ->join('User o','o.UserID = n.Owner', 'left')
        ->where(array('n.OrganizationID' => $this->LoginOrganizationID, 'n.NoteID' => $NoteID)) 
        ->get();
        
        $data['NoteData'] = $query->row();

        if(isset($data['NoteData'])){
            $RelatedObjData = MY_Model::getRelatedData();
            $RelatedObjData = $RelatedObjData[$data['NoteData']->RelatedTo][$data['NoteData']->What];
            $data['NoteData']->RelatedObjNo = $RelatedObjData->No;
        }

        return $data;
    }

    function DeleteNote($NoteID)
    {
        return $this->db->update('Note', array('IsDeleted' => 1), array('NoteID' => $NoteID));  
    }
}

?>
