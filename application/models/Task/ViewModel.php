<?php


class ViewModel extends MY_Model{
 
    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }
 
    function ViewTasks() 
    {
        $mainColumns = array('TaskID', 'AssignedToName', 'Subject', 'ContactName', 'ObjName', 'DueDate', 'TaskStatus', 'Priority', 'TaskType', 'CreatedDate', 'CreatedByName', 'RelatedTo', 'Who', 'What', 'CreatedBy', 'AssignedTo');

        $aColumns = array('t.TaskID', 'CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName', 't.Subject', 'CONCAT(c.FirstName, " ", c.LastName) as ContactName', 'a.AccountName as ObjName', 'DATE_FORMAT(t.DueDate, "'.RES_DATE.'") as DueDate', 'ts.TaskStatus', 'tp.Priority', 'tt.TaskType', 't.LastModifiedDate', 'DATE_FORMAT(t.CreatedDate, "'.RES_DATETIME.'") as CreatedDate', 'CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName','t.RelatedTo', 't.Who', 't.What', 't.CreatedBy', 't.AssignedTo');

        $wColumns = array('t.TaskID', 'CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName', 't.Subject', 'CONCAT(c.FirstName, " ", c.LastName) as ContactName', 'wo.Subject as ObjName', 'DATE_FORMAT(t.DueDate, "'.RES_DATE.'") as DueDate', 'ts.TaskStatus', 'tp.Priority', 'tt.TaskType', 't.LastModifiedDate', 'DATE_FORMAT(t.CreatedDate, "'.RES_DATETIME.'") as CreatedDate', 'CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName', 't.RelatedTo', 't.Who', 't.What', 't.CreatedBy', 't.AssignedTo');

        $eColumns = array('t.TaskID', 'CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName', 't.Subject', 'CONCAT(c.FirstName, " ", c.LastName) as ContactName', 'e.EstimateName as ObjName', 'DATE_FORMAT(t.DueDate, "'.RES_DATE.'") as DueDate', 'ts.TaskStatus', 'tp.Priority', 'tt.TaskType', 't.LastModifiedDate', 'DATE_FORMAT(t.CreatedDate, "'.RES_DATETIME.'") as CreatedDate', 'CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName', 't.RelatedTo', 't.Who', 't.What', 't.CreatedBy', 't.AssignedTo');

        $cColumns = array('t.TaskID', 'CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName', 't.Subject', 'CONCAT(c.FirstName, " ", c.LastName) as ContactName', 'CONCAT(ct.FirstName, " ", ct.LastName) as ObjName', 'DATE_FORMAT(t.DueDate, "'.RES_DATE.'") as DueDate', 'ts.TaskStatus', 'tp.Priority', 'tt.TaskType', 't.LastModifiedDate', 'DATE_FORMAT(t.CreatedDate, "'.RES_DATETIME.'") as CreatedDate', 'CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName', 't.RelatedTo', 't.Who', 't.What', 't.CreatedBy', 't.AssignedTo');

        $iColumns = array('t.TaskID', 'CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName', 't.Subject', 'CONCAT(c.FirstName, " ", c.LastName) as ContactName', 'i.InvoiceNo as ObjName', 'DATE_FORMAT(t.DueDate, "'.RES_DATE.'") as DueDate', 'ts.TaskStatus', 'tp.Priority', 'tt.TaskType', 't.LastModifiedDate', 'DATE_FORMAT(t.CreatedDate, "'.RES_DATETIME.'") as CreatedDate', 'CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName', 't.RelatedTo', 't.Who', 't.What', 't.CreatedBy', 't.AssignedTo');

        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "t.TaskID";

        /* DB table to use */
        $sTable = "Task t
        LEFT JOIN Contact c ON c.ContactID = t.Who
        LEFT JOIN TaskStatus ts ON ts.TaskStatusID = t.TaskStatus
        LEFT JOIN TaskPriority tp ON tp.TaskPriorityID = t.TaskPriority
        LEFT JOIN TaskType tt ON tt.TaskTypeID = t.TaskType
        LEFT JOIN User ast ON ast.UserID = t.AssignedTo
        LEFT JOIN User cb ON cb.UserID = t.CreatedBy";

        /* Limit */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".( $_GET['iDisplayStart'] ).", ".
                        ( $_GET['iDisplayLength'] );
        }

        /* Ordering */
        $sOrder = "ORDER BY LastModifiedDate DESC";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                    {
                            $sOrder .= $mainColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                                    ".( $_GET['sSortDir_'.$i] ) .", ";
                    }
            }
           
            $sOrder = substr_replace( $sOrder, "", -2 );

            if ( $sOrder == "ORDER BY" )
            {
                    $sOrder = "";
            } 
        }

        /* Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited */
        $sWhere = "";
        if ( $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
                for ( $i=0 ; $i<count($mainColumns) ; $i++ )
                {
                        $sWhere .= $mainColumns[$i]." LIKE '%".( $_GET['sSearch'] )."%' OR ";
                }
                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ')';
        }
        
        /* Where Condition */
        $UserID = $this->session->userdata('UserID');
        if(isset($_GET['flag']) && $_GET['flag'] == 'MyOpenTasksToday'){

            $Where = "Where t.AssignedTo = $UserID AND ts.IsClosed=0 AND t.OrganizationID='".$this->LoginOrganizationID."' AND t.IsDeleted=0 AND DATE(t.CreatedDate) = CURDATE()";
        }
        else if(isset($_GET['flag']) && $_GET['flag'] == 'MyOpenTasksThisWeek'){

            $Where = "Where t.AssignedTo = $UserID AND YEARWEEK(t.CreatedDate, 1) = YEARWEEK(CURDATE(), 1) AND ts.IsClosed=0 AND t.OrganizationID='".$this->LoginOrganizationID."' AND t.IsDeleted=0";

        } else if(isset($_GET['flag']) && $_GET['flag'] == 'AllMyOpenTasks'){

            $Where = "Where t.AssignedTo = $UserID AND ts.IsClosed=0 AND t.OrganizationID='".$this->LoginOrganizationID."' AND t.IsDeleted=0";

        } else if(isset($_GET['flag']) && $_GET['flag'] == 'MyCompletedTasksThisWeek'){

            $Where = "Where t.AssignedTo = $UserID AND YEARWEEK(t.CreatedDate, 1) = YEARWEEK(CURDATE(), 1) AND ts.TaskStatus='Completed' AND t.OrganizationID='".$this->LoginOrganizationID."' AND t.IsDeleted=0";

        } else {
            $Where = "Where t.IsDeleted=0 AND t.OrganizationID='".$this->LoginOrganizationID."'";
        }

        $sqlUnionQuery = "(SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
        FROM $sTable
        JOIN Account a ON t.What = a.AccountID 
        $Where AND t.RelatedTo = 'Account')
        UNION ALL
        (SELECT ".str_replace(" , ", " ", implode(", ", $wColumns))."
        FROM $sTable
        JOIN WorkOrder wo ON t.What = wo.WorkOrderID
        $Where AND t.RelatedTo = 'WorkOrder')
        UNION ALL
        (SELECT ".str_replace(" , ", " ", implode(", ", $eColumns))."
        FROM $sTable
        JOIN Estimate e ON t.What = e.EstimateID
        $Where AND t.RelatedTo = 'Estimate')
        UNION ALL
        (SELECT ".str_replace(" , ", " ", implode(", ", $cColumns))."
        FROM $sTable
        JOIN Contact ct ON t.What = ct.ContactID
        $Where AND t.RelatedTo = 'Contact')
        UNION ALL
        (SELECT ".str_replace(" , ", " ", implode(", ", $iColumns))."
        FROM $sTable
        JOIN Invoice i ON t.What = i.InvoiceID
        $Where AND t.RelatedTo = 'Invoice')";

        /* SQL queries Get data to display */
        $sQuery = "SELECT ".str_replace(" , ", " ", implode(", ", $mainColumns))." FROM
        (".$sqlUnionQuery.") results
        $sWhere
        $sOrder
        $sLimit";   
        
        $rResult = $this->db->query($sQuery); 
        $rResult = $rResult->result_array(); 
      
        /* Data set length after filtering */
        $sQuery = "SELECT COUNT(*) as cnt FROM (".$sqlUnionQuery.") results $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iFilteredTotal = $result->cnt;

        /* Total data set length */
        $sQuery = "SELECT COUNT(*) as cnt FROM (".$sqlUnionQuery.") results $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iTotal = $result->cnt;

        /* Output */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );

        foreach ( $rResult as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($mainColumns) ; $i++ )
            {
                if($i == 0)
                {   
                    $row[] = '<a href="'.SITE_URL.'Task/Actions/EditTask/'.$aRow[$mainColumns[0]].'"><i class="fa fa-pencil-square-o"></i></a>';
                } 
                else if($mainColumns[$i] == 'AssignedToName')
                {
                    $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['AssignedTo'].'">'.$aRow[$mainColumns[$i]].'</a>';
                }
                else if($mainColumns[$i] == 'CreatedByName')
                {
                    $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['CreatedBy'].'">'.$aRow[$mainColumns[$i]].'</a>';
                }
                else if($mainColumns[$i] == 'Subject')
                {
                    $row[] = '<a href="'.SITE_URL.'Task/Actions/TaskDetails/'.$aRow[$mainColumns[0]].'">'.$aRow[$mainColumns[$i]].'</a>';
                }
                else if($mainColumns[$i] == 'ContactName')
                {
                    $row[] = '<a href="'.SITE_URL.'Contact/Actions/ContactDetails/'.$aRow['Who'].'">'.$aRow[$mainColumns[$i]].'</a>';
                }
                else if($mainColumns[$i] == 'ObjName')
                {
                    $row[] = '<a href="'.SITE_URL.$aRow['RelatedTo'].'/Actions/'.$aRow['RelatedTo'].'Details/'.$aRow['What'].'">'.$aRow[$mainColumns[$i]].'</a>';
                }
                else if ( $mainColumns[$i] != ' ' )
                {
                    $row[] = $aRow[$mainColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode( $output );
    } 

    function CreateNewView()
    {
        extract($_POST);

        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'TaskViewName' => $TaskViewName,
            'UserID' => $this->LoginUserID,
            'RestrictVisibility' => $RestrictVisibility,
            'SpecifyFieldsDisplay' => implode(",", $SpecifyFieldsDisplay),
            'CreatedDate' => date("Y-m-d H:i:s"),
            'LastModifiedDate' => date("Y-m-d H:i:s")
        );     
        $query = $this->db->insert('TaskView', $data);  
        $TaskViewID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($this->LoginUserID, 'TaskView', array('TaskViewID' => $TaskViewID));

        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'TaskViewID' => $TaskViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('TaskFilter', $data);  
                }
            } 
 
        }
        return $TaskViewID;
    }

    function EditCustomView($TaskViewID)
    {  
        extract($_POST);
  
        $data = array(
            'TaskViewName' => $TaskViewName,
            'UserID' => $this->LoginUserID,
            'RestrictVisibility' => $RestrictVisibility,
            'SpecifyFieldsDisplay' => implode(",", $SpecifyFieldsDisplay),
            'CreatedDate' => date("Y-m-d H:i:s"),
            'LastModifiedDate' => date("Y-m-d H:i:s")
        );     
        $query = $this->db->update('TaskView', $data, array('TaskViewID' => $TaskViewID));  

        $this->db->delete('TaskFilter', array('TaskViewID' => $TaskViewID));  
        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'TaskViewID' => $TaskViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('TaskFilter', $data);  
                }
            } 
 
        }
        return $query;
    }

    function GetTaskViewData($TaskViewID){

        $query = $this->db->get_where('TaskView', array('TaskViewID' => $TaskViewID)); 
        $TaskViewData = $query->row();

        $query = $this->db->get_where('TaskFilter', array('TaskViewID' => $TaskViewID)); 
        $TaskFilterData = $query->result_array();

        $ColName = "t.TaskID, c.ContactID, t.What, t.AssignedTo as AssignedToID, t.CreatedBy as CreatedByID, t.LastModifiedBy as LastModifiedByID, ";
        if(!empty($TaskViewData->SpecifyFieldsDisplay)){
            $SpecifyFieldsDisplays = explode(",", $TaskViewData->SpecifyFieldsDisplay);
            foreach ($SpecifyFieldsDisplays as $key => $value) {
                if($value == 'AssignedTo'){
                    $ColName .= "CONCAT(ast.FirstName, ' ', ast.LastName) as AssignedTo";
                } else if($value == 'Who'){
                    $ColName .= "CONCAT(c.FirstName, ' ', c.LastName) as Who";
                } else if($value == 'OrganizationID'){
                    $ColName .= "o.CompanyName";
                } else if($value == 'TaskStatus'){
                    $ColName .= "ts.TaskStatus";
                } else if($value == 'TaskPriority'){
                    $ColName .= "tp.Priority as TaskPriority";
                } else if($value == 'TaskType'){
                    $ColName .= "tt.TaskType";
                } else if($value == 'RecurrenceRegeneratedType'){
                    $ColName .= "rrt.RecurrenceRegeneratedType";
                } else if($value == 'CreatedBy'){
                    $ColName .= "CONCAT(cr.FirstName, ' ', cr.LastName) as CreatedBy";
                } else if($value == 'LastModifiedBy'){
                    $ColName .= "CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedBy";
                } else if($value == 'DueDate'){
                    $ColName .= "DATE_FORMAT(t.DueDate, '".RES_DATE."') as DueDate";
                } else {
                    $ColName .= "t.".$value;
                }

                if($key != count($SpecifyFieldsDisplays)){
                    $ColName .= ", ";
                }
            }
        }
        
         /* Filter Task Data Start */
            $this->db->select($ColName);
            $this->db->from('Task t');
            $this->db->join('User ast','ast.UserID = t.AssignedTo', 'left');
            $this->db->join('Contact c','c.ContactID = t.Who', 'left');
            $this->db->join('User cr','cr.UserID = t.CreatedBy', 'left');
            $this->db->join('User lmb','lmb.UserID = t.LastModifiedBy', 'left');
            $this->db->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left');
            $this->db->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left');
            $this->db->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left');
            $this->db->join('RecurrenceRegeneratedType rrt','rrt.RecurrenceRegeneratedTypeID = t.RecurrenceRegeneratedType', 'left');
            $this->db->join('Organization o','o.OrganizationID = t.OrganizationID', 'left');
            $this->db->where(array('t.OrganizationID' => $this->LoginOrganizationID, 't.IsDeleted' => 0));

            foreach ($TaskFilterData as $key => $value) {

                $WhereField = "";
                if($value['FilterField'] == 'AssignedTo'){
                    $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                } else if($value['FilterField'] == 'Who') {
                    $WhereField = "CONCAT(c.FirstName, ' ', c.LastName)";
                } else if($value['FilterField'] == 'CreatedBy') {
                    $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                } else if($value['FilterField'] == 'LastModifiedBy') {
                    $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                } else if($value['FilterField'] == 'OrganizationID') {
                    $WhereField = "o.CompanyName";
                } else if($value['FilterField'] == 'TaskStatus') {
                    $WhereField = "ts.TaskStatus";
                } else if($value['FilterField'] == 'TaskPriority') {
                    $WhereField = "tp.Priority";
                } else if($value['FilterField'] == 'TaskType') {
                    $WhereField = "tt.TaskType";
                } else if($value['FilterField'] == 'DueDate'){
                     $WhereField = "DATE_FORMAT(t.DueDate, '".RES_DATE."')";
                } else {
                    $WhereField = "t.".$value['FilterField'];
                }

                if($value['FilterCondition'] == 'Equals'){
                    $this->db->where($WhereField.' =',  $value['FilterValue']);  
                } else if($value['FilterCondition'] == 'NotEqualTo'){
                    $this->db->where($WhereField.' !=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'LessThan'){
                    $this->db->where($WhereField.' <', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'GreaterThan'){
                    $this->db->where($WhereField.' >', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'LessOREqualTo'){
                    $this->db->where($WhereField.' <=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'GreaterOREqualTo'){
                    $this->db->where($WhereField.' >=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'Contains'){
                    $this->db->like($WhereField, $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'StartsWith'){ 
                    $this->db->like($WhereField, $value['FilterValue'], 'after');   
                } else if($value['FilterCondition'] == 'DoesNotContain'){
                    $this->db->not_like($WhereField, $value['FilterValue']);   
                } 
            } 
            $query = $this->db->get();  
            //$this->db->last_query();exit;
            $data['ColumnsNames'] = $SpecifyFieldsDisplays;

            $RelatedData = MY_Model::getRelatedData();

            $result = $query->result_array();
            $Task = array();
            $TaskData = array();
            $FinalData = array();
            foreach ($result as $key => $task) {
                foreach ($task as $key => $taskdetails) {  

                    $tmpdata = $task;
                    if(isset($task['RelatedTo']) && $task['What']){
                        $RelatedDt = $RelatedData[$task['RelatedTo']][$task['What']]; 
                        $tmpdata['RelatedToNo']  = $RelatedDt->No;
                    }
                    $FinalData[] = $tmpdata;
                } 
            } 

            $data['ColumnsValues'] = $FinalData;
            return $data; 
    }

    function GetCustomView($TaskViewID)
    {
        $query = $this->db->get_where('TaskView', array('TaskViewID' => $TaskViewID)); 
        
        $data['TaskViewData'] = $query->row();

        $query = $this->db->get_where('TaskFilter', array('TaskViewID' => $TaskViewID)); 
        
        $data['TaskFilterData'] = $query->result();

        return $data;
    }

    /*function GetTaskFilter()
    {
        $query = $this->db->get_where('TaskFilter', array('UserID' => $this->LoginUserID)); 
        
        return $query->result();
    }*/

    function GetViews(){

        $query = $this->db->query("SELECT * FROM TaskView WHERE ((RestrictVisibility = 'VisibleOnlyToMe' AND UserID = '".$this->LoginUserID."') OR (RestrictVisibility = 'VisibleToEveryone')) AND OrganizationID='".$this->LoginOrganizationID."' ORDER BY TaskViewName ASC");
        
        return $query->result();
    }

    function TaskViewFields()
    {
        $query = $this->db->get_where('TaskViewFields'); 
        
        return $query->result();
    }

    function DeleteCustomView($TaskViewID)
    {
        return $this->db->delete('TaskView', array('TaskViewID' => $TaskViewID)); 
    }

    function CopyCustomView($TaskViewID)
    {
       $query = $this->db->get_where('TaskView', array('TaskViewID' => $TaskViewID)); 
      
        foreach ($query->result() as $row){   
           foreach($row as $key=>$val){ 
                if($key != 'TaskViewID'){    
                    $this->db->set($key, $val); 
                }                
           }
        }
        $this->db->insert('TaskView');
        $NewTaskViewID = $this->db->insert_id();

        $query = $this->db->get_where('TaskFilter', array('TaskViewID' => $TaskViewID)); 
       
        foreach ($query->result() as $row){   
           foreach($row as $key => $val){ 
                if($key == 'TaskViewID'){  
                    $this->db->set($key, $NewTaskViewID);  
                } else if($key != 'TaskFilterID'){    
                    $this->db->set($key, $val); 
                }             
           }
           $this->db->insert('TaskFilter');
        }

        return 1; 
    }

    function RenameCustomView($TaskViewID){

        $data = array(
            'TaskViewName' => $_POST['TaskViewName']
        );

        return $this->db->update('TaskView', $data, array('TaskViewID' => $TaskViewID));  
    }

    function EditSharingCustomView($TaskViewID){

        $data = array(
            'RestrictVisibility' => $_REQUEST['RestrictVisibility']
        );

        return $this->db->update('TaskView', $data, array('TaskViewID' => $TaskViewID));  
    }

    function EditFiltersCustomView($TaskViewID){ 

        extract($_POST);

        $this->db->delete('TaskFilter', array('TaskViewID' => $TaskViewID));  
        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'TaskViewID' => $TaskViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('TaskFilter', $data);  
                }
            } 
 
        }
        return $query; 
    }

    function EditDisplayedColumnsCustomView($TaskViewID){

        $data = array(
            'SpecifyFieldsDisplay' => implode(",", $_REQUEST['SpecifyFieldsDisplay'])
        );

        return $this->db->update('TaskView', $data, array('TaskViewID' => $TaskViewID));  
    }
    
}

?>
