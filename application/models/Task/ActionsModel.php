<?php
 
class ActionsModel extends MY_Model{

    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }

    function RecentTasks() 
    {

        $mainColumns = array('TaskID', 'Subject', 'ContactName', 'ObjName', 'DueDate', 'TaskStatus', 'Priority', 'TaskType', 'RelatedTo', 'Who', 'What');

        $aColumns = array('t.TaskID','t.Subject', 'CONCAT(c.FirstName, " ", c.LastName) as ContactName', 'a.AccountName as ObjName', 'DATE_FORMAT(t.DueDate, "'.RES_DATETIME.'") as DueDate', 'ts.TaskStatus', 'tp.Priority', 'tt.TaskType', 't.LastModifiedDate', 't.RelatedTo', 't.Who', 't.What');

        $wColumns = array('t.TaskID','t.Subject', 'CONCAT(c.FirstName, " ", c.LastName) as ContactName', 'wo.Subject as ObjName', 'DATE_FORMAT(t.DueDate, "'.RES_DATETIME.'") as DueDate', 'ts.TaskStatus', 'tp.Priority', 'tt.TaskType', 't.LastModifiedDate', 't.RelatedTo', 't.Who', 't.What');

        $eColumns = array('t.TaskID','t.Subject', 'CONCAT(c.FirstName, " ", c.LastName) as ContactName', 'e.EstimateName as ObjName', 'DATE_FORMAT(t.DueDate, "'.RES_DATETIME.'") as DueDate', 'ts.TaskStatus', 'tp.Priority', 'tt.TaskType', 't.LastModifiedDate', 't.RelatedTo', 't.Who', 't.What');

        $cColumns = array('t.TaskID','t.Subject', 'CONCAT(c.FirstName, " ", c.LastName) as ContactName', 'CONCAT(ct.FirstName, " ", ct.LastName) as ObjName', 'DATE_FORMAT(t.DueDate, "'.RES_DATETIME.'") as DueDate', 'ts.TaskStatus', 'tp.Priority', 'tt.TaskType', 't.LastModifiedDate', 't.RelatedTo', 't.Who', 't.What');

        $iColumns = array('t.TaskID','t.Subject', 'CONCAT(c.FirstName, " ", c.LastName) as ContactName', 'i.InvoiceNo as ObjName', 'DATE_FORMAT(t.DueDate, "'.RES_DATETIME.'") as DueDate', 'ts.TaskStatus', 'tp.Priority', 'tt.TaskType', 't.LastModifiedDate', 't.RelatedTo', 't.Who', 't.What');

        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "t.TaskID";

        /* DB table to use */
        $sTable = "Task t
        LEFT JOIN Contact c ON c.ContactID = t.Who
        LEFT JOIN TaskStatus ts ON ts.TaskStatusID = t.TaskStatus
        LEFT JOIN TaskPriority tp ON tp.TaskPriorityID = t.TaskPriority
        LEFT JOIN TaskType tt ON tt.TaskTypeID = t.TaskType";

        /* Limit */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".( $_GET['iDisplayStart'] ).", ".
                        ( $_GET['iDisplayLength'] );
        }

        /* Ordering */
        $sOrder = "ORDER BY LastModifiedDate DESC";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                    {
                            $sOrder .= $mainColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                                    ".( $_GET['sSortDir_'.$i] ) .", ";
                    }
            }
           
            $sOrder = substr_replace( $sOrder, "", -2 );

            if ( $sOrder == "ORDER BY" )
            {
                    $sOrder = "";
            } 
        }

        /* Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited */
        $sWhere = "";
        if ( $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
                for ( $i=0 ; $i<count($mainColumns) ; $i++ )
                {
                        $sWhere .= $mainColumns[$i]." LIKE '%".( $_GET['sSearch'] )."%' OR ";
                }
                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ')';
        }
        
        /* Where Condition */

        $Where = "Where t.OrganizationID='".$this->LoginOrganizationID."' and t.IsDeleted=0 AND";

        $sqlUnionQuery = "(SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
        FROM $sTable
        JOIN Account a ON t.What = a.AccountID 
        $Where t.RelatedTo = 'Account')
        UNION ALL
        (SELECT ".str_replace(" , ", " ", implode(", ", $wColumns))."
        FROM $sTable
        JOIN WorkOrder wo ON t.What = wo.WorkOrderID
        $Where t.RelatedTo = 'WorkOrder')
        UNION ALL
        (SELECT ".str_replace(" , ", " ", implode(", ", $eColumns))."
        FROM $sTable
        JOIN Estimate e ON t.What = e.EstimateID
        $Where t.RelatedTo = 'Estimate')
        UNION ALL
        (SELECT ".str_replace(" , ", " ", implode(", ", $cColumns))."
        FROM $sTable
        JOIN Contact ct ON t.What = ct.ContactID
        $Where t.RelatedTo = 'Contact')
        UNION ALL
        (SELECT ".str_replace(" , ", " ", implode(", ", $iColumns))."
        FROM $sTable
        JOIN Invoice i ON t.What = i.InvoiceID
        $Where t.RelatedTo = 'Invoice')";

        /* SQL queries Get data to display */
        $sQuery = "SELECT ".str_replace(" , ", " ", implode(", ", $mainColumns))." FROM
        (".$sqlUnionQuery.") results
        $sWhere
        $sOrder
        $sLimit";   
        
        $rResult = $this->db->query($sQuery); 
        $rResult = $rResult->result_array(); 
      
        /* Data set length after filtering */
        $sQuery = "SELECT COUNT(*) as cnt FROM (".$sqlUnionQuery.") results $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iFilteredTotal = $result->cnt;

        /* Total data set length */
        $sQuery = "SELECT COUNT(*) as cnt FROM (".$sqlUnionQuery.") results $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iTotal = $result->cnt;

        /* Output */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );

        foreach ( $rResult as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($mainColumns) ; $i++ )
            {
                if($i == 0)
                {
                    $row[] = '';
                } 
                else if($i == 1)
                {   
                    $row[] = '<a href="'.SITE_URL.'Task/Actions/TaskDetails/'.$aRow[$mainColumns[0]].'">'.$aRow[$mainColumns[$i]].'</a>';
                }
                else if($mainColumns[$i] == 'ContactName')
                {
                    $row[] = '<a href="'.SITE_URL.'Contact/Actions/ContactDetails/'.$aRow['Who'].'">'.$aRow[$mainColumns[$i]].'</a>';
                }
                else if($mainColumns[$i] == 'ObjName')
                {
                    $row[] = '<a href="'.SITE_URL.$aRow['RelatedTo'].'/Actions/'.$aRow['RelatedTo'].'Details/'.$aRow['What'].'">'.$aRow[$mainColumns[$i]].'</a>';
                }
                else if ( $mainColumns[$i] != ' ' )
                {
                    $row[] = $aRow[$mainColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode( $output );
    } 

    function CreateTask() {
               
        extract($_POST); 

        $UserID = $this->LoginUserID;
       
        $data = array(
            'OrganizationID' =>  $this->LoginOrganizationID,
            'Subject' => $Subject,
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            'AssignedTo' => $AssignedTo,
            'Who' => !empty($Who)?$Who:NULL,
            'Description' => $Description,
            'Email' => $Email,
            'TaskStatus' => $TaskStatus,
            'Phone' => $Phone,
            'TaskPriority' => $TaskPriority,
            'RecurrenceID' => !empty($RecurrenceID)?$RecurrenceID:NULL,
            'IsRecurrence' => isset($IsRecurring)?$IsRecurring:0,
            'TaskType' => $TaskType, 
            'DueDate' => !empty($DueDate)?date("Y-m-d",strtotime($DueDate)):NULL
        ); 
        
        if(isset($IsRecurring) && $IsRecurring == 1 && isset($RecurrenceID) && !empty($RecurrenceID)){

            $TaskID = $this->CreateRecurringTask($RecurrenceID, $data);

        } else {

            $result = $this->db->insert('Task', $data);    
           //echo $this->db->last_query();exit;
            $TaskID = $this->db->insert_id();

            MY_Model::insertCreatedModifiedByAndDt($UserID,'Task', array('TaskID' => $TaskID));
        }

        /* For insert custom fields */ 
        MY_Model::insertCustomFieldData('Task', $TaskID);
    
        return $TaskID;
    } 

    function CreateRecurringTask($WORecurrenceID, $TaskData)
    {
        $query = $this->db->get_where('WORecurrence', array('WORecurrenceID' => $WORecurrenceID));  
        $result = $query->row();

        $StartOnDate = date("Y-m-d", strtotime($result->StartOn));
        $EndsAfterOccurrences = isset($result->EndsAfterOccurrences) ? ($result->EndsAfterOccurrences * $result->IntervalEvery) : (100 * $result->IntervalEvery);

        if(isset($result->RepeatEvery) && ($result->RepeatEvery == "Daily" || $result->RepeatEvery == "Monthly" || $result->RepeatEvery == "Yearly")){

            if($result->RepeatEvery == "Daily"){
                $IntervalUnit = 'D'; 
                $IntervalType = 'day'; 
            } else if($result->RepeatEvery == "Monthly"){
                $IntervalUnit = 'M'; 
                $IntervalType = 'months'; 
            } else if($result->RepeatEvery == "Yearly"){
                $IntervalUnit = 'Y'; 
                $IntervalType = 'years'; 
            }

            if($result->Ends == 'On'){
                $EndsOnDate = date("Y-m-d", strtotime($result->EndsOnDate));
            } else{
                $EndsOnDate = date('Y-m-d', strtotime("+$EndsAfterOccurrences $IntervalType", strtotime($StartOnDate)));
            }

            $start = new DateTime( $StartOnDate );
            $end = new DateTime( $EndsOnDate );
            $interval = new DateInterval('P'.$result->IntervalEvery.''.$IntervalUnit.'');
            $flag = 1;
            $period = new DatePeriod($start, $interval, $end);
            foreach ($period as $date) {
 
                //print $date->format('Y-m-d H:i:s') . '<br/>';
                //$StartDateTime = $date->format('Y-m-d').' '.$result->StartTime;
                //$EndDateTime = $date->format('Y-m-d').' '.$result->EndTime;
                $Date = $date->format('Y-m-d');
                $RecurringTaskData = $this->InsertRecurringTask($TaskData, $Date);
                
                if((isset($result->EndsAfterOccurrences) && $flag == $result->EndsAfterOccurrences) || $flag == 100)
                        break;
                $flag++;
            }

        } else if(isset($result->RepeatEvery) && $result->RepeatEvery == "Weekly"){

            $RepeatOn = explode(",", $result->RepeatOn);
            if($result->Ends == 'On'){
                $EndsOnDate = date("Y-m-d", strtotime($result->EndsOnDate));
            } else{
                $EndsOnDate = date('Y-m-d', strtotime("+$EndsAfterOccurrences weeks", strtotime($StartOnDate)));
            }

            $start = new DateTime( $StartOnDate );
            $end = new DateTime( $EndsOnDate );
            $interval = new DateInterval('P1D');

            $period = new DatePeriod($start, $interval, $end);

            // only trigger every interval weeks...
            $weekInterval = $result->IntervalEvery;

            $fakeWeek = 0;
            $currentWeek = $start->format('W');
            $flag = 1;
            foreach ($period as $date) {

                if ($date->format('W') !== $currentWeek) {
                    $currentWeek = $date->format('W');
                    $fakeWeek++; 
                    //print ' WEEK ' . $currentWeek . '<br/>';
                }

                if ($fakeWeek % $weekInterval !== 0) {
                    continue;
                }

                $dayOfWeek = $date->format('D');

                if (in_array($dayOfWeek, $RepeatOn)) {
                    //print $date->format('Y-m-d H:i:s') . '   ' . $dayOfWeek . '<br/>';
                    //$StartDateTime = $date->format('Y-m-d').' '.$result->StartTime;
                    //$EndDateTime = $date->format('Y-m-d').' '.$result->EndTime;
                    $Date = $date->format('Y-m-d');
                    $RecurringTaskData = $this->InsertRecurringTask($TaskData, $Date);

                    if((isset($result->EndsAfterOccurrences) && $flag == $result->EndsAfterOccurrences) || ($flag == 100)){
                        break;
                    }
                    $flag++;
                }
            }
        } /*else if(isset($result->RepeatEvery) && $result->RepeatEvery == "Periodically"){
            
        }*/

        return $RecurringTaskData;
    }

    function InsertRecurringTask($TaskData, $Date)
    {

        $UserID = $this->LoginUserID;
        $data = array(
            'OrganizationID' =>  $this->LoginOrganizationID,
            'Subject' => $TaskData['Subject'],
            'RelatedTo' => $TaskData['RelatedTo'],
            'What' => $TaskData['What'],
            'AssignedTo' => $TaskData['AssignedTo'],
            'Who' => $TaskData['Who'],
            'Description' => $TaskData['Description'],
            'Email' => $TaskData['Email'],
            'TaskStatus' => $TaskData['TaskStatus'],
            'Phone' => $TaskData['Phone'],
            'TaskPriority' => $TaskData['TaskPriority'],
            'TaskType' => $TaskData['TaskType'], 
            /*'DueDate' => !empty($TaskData['DueDate'])?date("Y-m-d",strtotime($TaskData['DueDate'])):NULL,*/
            'DueDate' => !empty($Date)?date("Y-m-d",strtotime($Date)):NULL,
            'RecurrenceID' => $TaskData['RecurrenceID'],
            'IsRecurrence' => $TaskData['IsRecurrence'],
        );   
        $result = $this->db->insert('Task', $data);  
        //echo $this->db->last_query(); exit;
        $TaskID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Task', array('TaskID' => $TaskID));

        return $TaskID;
    }

    function EditTask($TaskID) {

        extract($_POST);          

        $UserID = $this->LoginUserID;

        $data = array(
            'OrganizationID' =>  $this->LoginOrganizationID,
            'Subject' => $Subject,
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            'AssignedTo' => $AssignedTo,
            'Who' => !empty($Who)?$Who:NULL,
            'Description' => $Description,
            'Email' => $Email,
            'TaskStatus' => $TaskStatus,
            'Phone' => $Phone,
            'TaskPriority' => $TaskPriority,
            'RecurrenceID' => !empty($RecurrenceID)?$RecurrenceID:NULL,
            'IsRecurrence' => isset($IsRecurring)?$IsRecurring:0,
            'TaskType' => $TaskType, 
            'DueDate' => !empty($DueDate)?date("Y-m-d",strtotime($DueDate)):NULL
        ); 
       
        $result = $this->db->update('Task', $data, array('TaskID' => $TaskID));    
        //echo $this->db->last_query();exit;
        MY_Model::updateCreatedModifiedByAndDt($UserID,'Task', array('TaskID' => $TaskID));
    
        /* For insert custom fields */ 
        MY_Model::updateCustomFieldData('Task', $TaskID);
        
        return $result;
    } 

    function getTaskData($TaskID, $RelatedObjName = 'AllObject')
    {

        $result = $this->db->update('Task', array('LastModifiedDate' => date("Y-m-d H:i:s")), array('OrganizationID' => $this->LoginOrganizationID,'TaskID' => $TaskID));  

        $query = $this->db->select('t.*, t.Subject as Title, CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName, CONCAT(c.FirstName, " ", c.LastName) as ContactName, ts.TaskStatus, ts.TaskStatusID, tp.Priority, tt.TaskType, tt.TaskTypeID, rrt.RecurrenceRegeneratedType, DATE_FORMAT(t.DueDate, "'.RES_DATE.'") as DueDate, DATE_FORMAT(t.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,DATE_FORMAT(t.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName')
        ->from('Task t')
        ->join('User cb','cb.UserID = t.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = t.LastModifiedBy', 'left')
        ->join('User ast','ast.UserID = t.AssignedTo', 'left')
        ->join('Contact c','c.ContactID = t.Who', 'left')
        ->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left')
        ->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left')
        ->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left')
        ->join('RecurrenceRegeneratedType rrt','rrt.RecurrenceRegeneratedTypeID = t.RecurrenceRegeneratedType', 'left')
        ->where(array('t.OrganizationID' => $this->LoginOrganizationID, 't.TaskID' => $TaskID)) 
        ->get();

        $data['TaskData'] = $query->row();
        
        if(isset($data['TaskData'])){
            $RelatedObjData = MY_Model::getRelatedData();
            $RelatedObjData = $RelatedObjData[$data['TaskData']->RelatedTo][$data['TaskData']->What];
            $data['TaskData']->RelatedObjNo = $RelatedObjData->Name; 
        }

        return $data;
    }

    function CreateNote($TaskID)
    {
        extract($_POST); 

        $UserID = $this->LoginUserID;
  
        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'Owner' => $UserID,
            'What' => $TaskID,
            'Subject' => $Subject,
            'Body' => $Body,
            'RelatedTo' => 'Task'
        );
        $result = $this->db->insert('Note', $data);    
        $NoteID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Note', array('NoteID' => $NoteID));
    
        return $result;
    }

    /*function GetRelatedToObjectData()
    {
        $OptionStr = "";
        if(isset($_POST['RelatedTo']) && $_POST['RelatedTo'] == 'Account'){

            $query = $this->db->get_where('Account', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
            $Accounts = $query->result();
            foreach ($Accounts as $key => $Account) {
                if(isset($_POST['What']) && $Account->AccountID == $_POST['What'])
                    $OptionStr .= "<option value='".$Account->AccountID."' selected='selected'>".$Account->AccountName."</option>";
                else
                    $OptionStr .= "<option value='".$Account->AccountID."'>".$Account->AccountName."</option>";
            }

        } else if(isset($_POST['RelatedTo']) && $_POST['RelatedTo'] == 'WorkOrder'){

            $query = $this->db->get_where('WorkOrder', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
            $WorkOrders = $query->result();
            foreach ($WorkOrders as $key => $WorkOrder) {
                if(isset($_POST['What']) && $WorkOrder->WorkOrderID == $_POST['What'])
                    $OptionStr .= "<option value='".$WorkOrder->WorkOrderID."' selected='selected'>".$WorkOrder->Subject."</option>";
                else
                    $OptionStr .= "<option value='".$WorkOrder->WorkOrderID."'>".$WorkOrder->Subject."</option>";
            }
            
        } else if(isset($_POST['RelatedTo']) && $_POST['RelatedTo'] == 'Estimate'){

            $query = $this->db->get_where('Estimate', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
            $Estimates = $query->result();
            foreach ($Estimates as $key => $Estimate) {
                if(isset($_POST['What']) && $Estimate->EstimateID == $_POST['What'])
                    $OptionStr .= "<option value='".$Estimate->EstimateID."' selected='selected'>".$Estimate->EstimateName."</option>";
                else
                    $OptionStr .= "<option value='".$Estimate->EstimateID."'>".$Estimate->EstimateName."</option>";
            }
            
        } else if(isset($_POST['RelatedTo']) && $_POST['RelatedTo'] == 'Invoice'){

            $query = $this->db->get_where('Invoice', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
            $Invoices = $query->result();
            foreach ($Invoices as $key => $Invoice) { 
                if(isset($_POST['What']) && $Invoice->InvoiceID == $_POST['What'])
                    $OptionStr .= "<option value='".$Invoice->InvoiceID."' selected='selected'>".$Invoice->InvoiceNo."</option>";
                else
                    $OptionStr .= "<option value='".$Invoice->InvoiceID."'>".$Invoice->InvoiceNo."</option>";
            }
            
        } else if(isset($_POST['RelatedTo']) && $_POST['RelatedTo'] == 'Contact'){

            $query = $this->db->get_where('Contact', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
            $Contacts = $query->result();
            foreach ($Contacts as $key => $Contact) {
                if(isset($_POST['What']) && $Contact->ContactID == $_POST['What'])
                    $OptionStr .= "<option value='".$Contact->ContactID."' selected='selected'>".$Contact->FirstName." ".$Contact->LastName."</option>";
                else
                    $OptionStr .= "<option value='".$Contact->ContactID."'>".$Contact->FirstName." ".$Contact->LastName."</option>";
            }
        }

        return $OptionStr;
    }*/

    function SaveRecurrenceSetting()
    { 
        extract($_POST);

        if(isset($WORecurrenceID) && !empty($WORecurrenceID))
        {
            $data = array(
                'StartOn' => date("Y-m-d",strtotime($StartOn)),
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'RepeatOn' => ($RepeatEvery=='Weekly')?implode(',', $RepeatOn):NULL,
                'Ends' => $Ends,
                'EndsOnDate' => ($Ends=='On')?date("Y-m-d",strtotime($EndsOnDate)):NULL,
                'EndsAfterOccurrences' => ($Ends=='After')?$EndsAfterOccurrences:NULL,
                'StartTime' => !empty($StartTime)?date("H:i:s",strtotime($StartTime)):NULL,
                'EndTime' => !empty($EndTime)?date("H:i:s",strtotime($EndTime)):NULL,
                'LastModifiedDate' => date("Y-m-d H:i:s")
            ); 
            $result = $this->db->update('WORecurrence', $data, array('WORecurrenceID'=>$WORecurrenceID));    

        } else {
            $data = array(
                'StartOn' => date("Y-m-d",strtotime($StartOn)),
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'RepeatOn' => ($RepeatEvery=='Weekly')?implode(',', $RepeatOn):NULL,
                'Ends' => $Ends,
                'EndsOnDate' => ($Ends=='On')?date("Y-m-d",strtotime($EndsOnDate)):NULL,
                'EndsAfterOccurrences' => ($Ends=='After')?$EndsAfterOccurrences:NULL,
                'StartTime' => !empty($StartTime)?date("H:i:s",strtotime($StartTime)):NULL,
                'EndTime' => !empty($EndTime)?date("H:i:s",strtotime($EndTime)):NULL,
                'CreatedDate' => date("Y-m-d H:i:s"),
                'LastModifiedDate' => date("Y-m-d H:i:s")
            ); 
            $result = $this->db->insert('WORecurrence', $data);    
            $WORecurrenceID = $this->db->insert_id();
        }
        return $WORecurrenceID;
    }

    function removeRecurrenceSetting()
    {
        $WORecurrenceID = $_REQUEST['WORecurrenceID'];
        $WorkOrderID = $_REQUEST['WorkOrderID'];
        $this->db->update('WorkOrder',  array('IsRecurring' => 0), array('WorkOrderID' => $WorkOrderID));  
        return $this->db->delete('WORecurrence', array('WORecurrenceID' => $WORecurrenceID));
    }

    function GetContacts()
    { 
        $query = $this->db->get_where('Contact', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetTaskTypes()
    { 
        $query = $this->db->get_where('TaskType', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetUsers()
    { 
        $query = $this->db->get_where('User', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetTaskStatus()
    { 
        $query = $this->db->get_where('TaskStatus', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetTaskPriority()
    { 
        $query = $this->db->get_where('TaskPriority', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetViews(){

        $query = $this->db->query("SELECT * FROM TaskView WHERE ((RestrictVisibility = 'VisibleOnlyToMe' AND UserID = '".$this->LoginUserID."') OR (RestrictVisibility = 'VisibleToEveryone')) AND OrganizationID='".$this->LoginOrganizationID."' ORDER BY TaskViewName ASC");
        
        return $query->result();
    }

    function DeleteTask($TaskID)
    {
        return $this->db->update('Task', array('IsDeleted' => 1), array('TaskID' => $TaskID));  
    }
}

?>
