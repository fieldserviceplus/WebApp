<?php
require_once(APPPATH.'models/API/API_Model.php');

class AuthModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }

    function checkUserAuthentication($UserID, $token) {
        $data = array(
            'UserID' => $UserID,
            'Token' => $token
        );

        $query = $this->db->get_where('User', $data);
        return $query->num_rows();
    }
   
    function SignUp($key, $FirstName, $LastName, $CompanyName, $PhoneNo, $City, $Email, $Password, $DeviceUDID, $DeviceType, $generateToken) {

        $data = array(
            'Email' => $Email
        );
        $query = $this->db->get_where('User', $data);
 
        if (AUTH_KEY != $key) {

            return 2;

        } else if ($query->num_rows() > 0) {
 
            return 3;

        } else {

            $data = array(
                'FirstName' => $FirstName,
                'LastName' => $LastName,
                'OrganizationID' => 1,
                /*'CompanyName' => $CompanyName,*/
                'PhoneNo' => $PhoneNo,
                /*'City' => $City,*/
                'Email' => $Email,
                'Password' => md5($Password),
                'DeviceUDID' => $DeviceUDID,
                'DeviceType' => $DeviceType,
                'Token' => $generateToken
            );
            $query = $this->db->insert('User', $data);

            API_Model::checkQuery($query); 

            $UserID = $this->db->insert_id();

            API_Model::insertCreatedModifiedByAndDt($UserID,'User', array('UserID' => $UserID));
        
            return $this->getUserData($UserID);
        }
    }
 
    function Login($key, $Email, $Password, $generateToken, $DeviceUDID, $DeviceType){

        if (AUTH_KEY != $key) {
            return 2;
        }

        $where = array(
            'Email' => $Email,
            'Password' => md5($Password)
        );
        $query = $this->db->select('UserID')->get_where('User', $where);
    
        if ($query->num_rows() > 0) {

            $result = $query->row();
            $UserID = $result->UserID;
            $data = array(
                'DeviceUDID' => $DeviceUDID,
                'DeviceType' => $DeviceType,
                'Token' => $generateToken
            );

            $this->db->where('UserID', $UserID);
            $query = $this->db->update('User',$data);

            API_Model::checkQuery($query); 
 
            return $this->getUserData($UserID);

        } else {
            return 3; 
        }
    }

    function getUserData($UserID)
    { 
        $query = $this->db->select('u.UserID, u.OrganizationID, u.Salutation, u.FirstName, u.LastName, u.Email, u.AboutMe, u.Manager, u.ReceiveAdminEmails, u.Alias, u.MobileNo, u.PhoneNo, o.CompanyName, u.DefaultGrpNotificationFreq, d.DepartmentName, ds.DivisionName, u.Address, u.City, u.State, u.Country, u.PostalCode, cb.FirstName as CreatedBy, mb.FirstName as LastModifiedBy')
        ->from('User u')
        ->join('Organization o','o.OrganizationID = u.OrganizationID', 'left')
        //->join('Profile p','p.ProfileID = u.Profile', 'left')
        ->join('Department d','d.DepartmentID = u.Department', 'left')
        ->join('Division ds','ds.DivisionID = u.Division', 'left')
        ->join('City c','c.CityID = u.City', 'left')
        ->join('State st','st.StateID = c.State', 'left')
        ->join('Country cn','cn.CountryID = st.Country', 'left')
        ->join('User cb','cb.UserID = u.CreatedBy', 'left')
        ->join('User mb','mb.UserID = u.LastModifiedBy', 'left') 
        ->where('u.UserID', $UserID)
        ->get(); 

        API_Model::checkQuery($query);  
          
        return $query->row_array();
    }

    function ForgotPassword($key, $Email, $generateToken){
        if (AUTH_KEY != $key) { 
            return 2;
        }

        $where = array(
            'Email' => $Email
        );
        $query = $this->db->select('UserID')->get_where('User', $where);
     
        if ($query->num_rows() > 0) {

            $result = $query->row();
            
            $Password = API_Controller::genNumRandCode(6);

            $message = "Hello, <br /><br /> Your new password is $Password <br /><br /> Thank You";  
 
            API_Controller::sendMail($Email, 'Forgot Password', $message);

            $data = array('Password' => md5($Password));

            $result = $this->db->update('User', $data, array('UserID' => $result->UserID));

            API_Model::checkQuery($result);  
 
            return $result;

        } else { 
            return 2; 
        }
    }

}

?>
