<?php
class API_Model extends CI_Model
{   
    function __construct() {
        parent::__construct();
    }

    static function checkQuery($Query)
    {
        $CI=&get_instance();
        try {

            if( !$Query )
            {
                $error = $CI->db->error();
                throw new Exception(API_Controller::responseMessage($error['code'], $error['message'], "False"));
            }
        }
        catch (Exception $e){ exit;}  
        
    }

    static function insertCreatedModifiedByAndDt($UserID, $TableName, $Where)
    { 
        $CI=&get_instance();
        $data = array(
            'CreatedDate' => date('Y-m-d H:i:s'),
            'LastModifiedDate' => date('Y-m-d H:i:s'),
            'CreatedBy' => $UserID,
            'LastModifiedBy' => $UserID
        ); 
        $result = $CI->db->update($TableName, $data, $Where);
    }

    static function updateCreatedModifiedByAndDt($UserID, $TableName, $Where)
    {
        $CI=&get_instance();
        $data = array(
            'LastModifiedDate' => date('Y-m-d H:i:s'),
            'LastModifiedBy' => $UserID
        ); 
        $result = $CI->db->update($TableName, $data, $Where);
    }

    static function getCountry()
    {
        $CI=&get_instance();
        $query = $CI->db->select('CountryID, CountryName')->get('Country');
        return $query->result(); 
    }  

    static function getRelatedData()
    {
        $CI=&get_instance();

        $query = $CI->db->select('wo.WorkOrderID as ID, wo.Subject as Name, wo.WorkOrderNo as No, wos.ColorCode, wo.Latitude, wo.Longitude, wop.Priority')
        ->from('WorkOrder wo')
        ->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left')
        ->join('WOPriority wop','wop.WOPriorityID = wo.WOPriority', 'left')
        ->get();
        $results = $query->result();
        $WorkOrder = $results;

        foreach ($WorkOrder as $key => $value) {
            $ResData['WorkOrder'][$value->ID] = $value;
        }

        $query = $CI->db->select('AccountID as ID, AccountName as Name, AccountNo as No')->get('Account');
        $results = $query->result();
        $Account = $results;

        foreach ($Account as $key => $value) {
            $ResData['Account'][$value->ID] = $value;
        }

        $query = $CI->db->select('e.EstimateID as ID, e.EstimateName as Name, e.EstimateNo as No, es.ColorCode')
            ->from('Estimate e')
            ->join('EstimateStatus es','es.EstimateStatusID = e.EstimateStatus', 'left')
            ->get();
        $results = $query->result();
        $Estimate = $results;

        foreach ($Estimate as $key => $value) {
            $ResData['Estimate'][$value->ID] = $value;
        }

        $query = $CI->db->select('ContactID as ID, CONCAT(FirstName, " ", LastName) as Name, ContactNo as No')->get('Contact');
        $results = $query->result();
        $Contact = $results;

        foreach ($Contact as $key => $value) {
            $ResData['Contact'][$value->ID] = $value;
        }

        $query = $CI->db->select('InvoiceID as ID, InvoiceNo as Name, InvoiceNo as No')->get('Invoice');
        $results = $query->result();
        $Invoice = $results;

        foreach ($Invoice as $key => $value) {
            $ResData['Invoice'][$value->ID] = $value;
        }
        
        return $ResData; 
    }

    static function getCustomFields($RelatedToID, $RelatedTo, $OrganizationID)
    {
        $CI=&get_instance();

        $query = $CI->db->select('cf.CustomFieldID, cf.FieldName, cf.FieldLabel, cf.FieldLength, cf.IsRequired, cf.DefaultValue, cf.FieldType, cf.OptionValues, cf.OptionLabels')
        ->from('CustomFields cf')
        ->where(array('cf.Object' => $RelatedTo, 'cf.OrganizationID' => $OrganizationID, 'cf.AddToPageLayout' => 1, 'cf.IsDeleted' => 0))
        ->get();
        foreach ($query->result() as $key => $value) {
            $CustomFields[$value->CustomFieldID]['CustomFieldID'] = $value->CustomFieldID;
            $CustomFields[$value->CustomFieldID]['FieldName'] = $value->FieldName;
            $CustomFields[$value->CustomFieldID]['FieldLabel'] = $value->FieldLabel;
            $CustomFields[$value->CustomFieldID]['IsRequired'] = $value->IsRequired;
            $CustomFields[$value->CustomFieldID]['DefaultValue'] = $value->DefaultValue;
            $CustomFields[$value->CustomFieldID]['FieldType'] = $value->FieldType;
            $CustomFields[$value->CustomFieldID]['OptionValues'] = $value->OptionValues;
            $CustomFields[$value->CustomFieldID]['OptionLabels'] = $value->OptionLabels;
            $CustomFields[$value->CustomFieldID]['AccountCustomFieldValueID'] = "";
            $CustomFields[$value->CustomFieldID]['FieldValue'] = "";
        }

        $query = $CI->db->select('*')
        ->get_where($RelatedTo.'CustomFieldValues', array($RelatedTo.'ID' => $RelatedToID, 'OrganizationID' => $OrganizationID));
        foreach ($query->result() as $key => $value) {
            if(in_array($value->CustomFieldID, array_keys($CustomFields))){
                $CustomFields[$value->CustomFieldID][$RelatedTo.'CustomFieldValueID'] = $value->AccountCustomFieldValueID;
                $CustomFields[$value->CustomFieldID]['FieldValue'] = $value->FieldValue;
            }
        }
        $data = API_Controller::removeNullFromMultiDimArr(array_values($CustomFields));
       
        return $data;
    }

    static function insertCustomFieldData($Object, $ObjectID, $OrganizationID)
    {

        /*$CI = &get_instance();
       
        if(isset($_POST['CustomFieldIDs']) && !empty($_POST['CustomFieldIDs'])){

            foreach ($_POST['CustomFieldIDs'] as $key => $CustomFieldID) {
           
                $query = $CI->db->get_where('CustomFields', array('OrganizationID' => $OrganizationID, 'CustomFieldID' => $CustomFieldID)); 
                $result = $query->row(); 
                
                if($result->FieldType == 'Checkbox'){
                    $FieldValue = implode(",", $_POST[$result->FieldName]);
                } else {
                    $FieldValue = $_POST[$result->FieldName];
                }

                if($FieldValue == "" || $FieldValue == NULL)
                    $FieldValue = $result->DefaultValue;

                $data = array(
                    'OrganizationID' =>  $OrganizationID,
                    $Object.'ID' => $ObjectID,
                    'FieldValue' => $FieldValue,
                    'CustomFieldID' => $CustomFieldID,
                ); 
                $result = $CI->db->insert($Object.'CustomFieldValues', $data);
            }
        }*/

        $CI = &get_instance();
       
        if(isset($_POST['CustomFieldIDs']) && !empty($_POST['CustomFieldIDs'])){

            foreach ($_POST['CustomFieldIDs'] as $CustomFieldID => $CustomFieldVal) {
           
                $query = $CI->db->get_where('CustomFields', array('OrganizationID' => $OrganizationID, 'CustomFieldID' => $CustomFieldID)); 
                $result = $query->row(); 
                
                /*if($result->FieldType == 'Checkbox'){
                    $FieldValue = implode(",", $CustomFieldVal);
                } else {
                    $FieldValue = $CustomFieldVal;
                }*/
                $FieldValue = $CustomFieldVal;

                if($FieldValue == "" || $FieldValue == NULL)
                    $FieldValue = $result->DefaultValue;

                $data = array(
                    'OrganizationID' =>  $OrganizationID,
                    $Object.'ID' => $ObjectID,
                    'FieldValue' => $FieldValue,
                    'CustomFieldID' => $CustomFieldID,
                ); 
                $result = $CI->db->insert($Object.'CustomFieldValues', $data);
            }
        }
    }

    static function updateCustomFieldData($Object, $ObjectID, $OrganizationID)
    {
        $CI = &get_instance();

        /*if(isset($_POST['CustomFieldIDs']) && !empty($_POST['CustomFieldIDs'])){

            foreach ($_POST['CustomFieldIDs'] as $key => $CustomFieldID) {

                $query = $CI->db->get_where('CustomFields', array('OrganizationID' => $OrganizationID, 'CustomFieldID' => $CustomFieldID)); 
                $result = $query->row(); 
                
                if($result->FieldType == 'Checkbox'){
                    $FieldValue = implode(",", $_POST[$result->FieldName]);
                } else {
                    $FieldValue = $_POST[$result->FieldName];
                }

                if($FieldValue == "" || $FieldValue == NULL)
                    $FieldValue = $result->DefaultValue;

                if(isset($_POST['CustomFieldValueIDs'][$CustomFieldID]) && !empty($_POST['CustomFieldValueIDs'][$CustomFieldID])){

                    $data = array(
                        'OrganizationID' =>  $OrganizationID,
                        $Object.'ID' => $ObjectID,
                        'FieldValue' => $FieldValue,
                        'CustomFieldID' => $CustomFieldID
                    ); 
                    $result = $CI->db->update($Object.'CustomFieldValues', $data, array($Object.'CustomFieldValueID' => $_POST['CustomFieldValueIDs'][$CustomFieldID]));

                } else {

                    $data = array(
                        'OrganizationID' =>  $OrganizationID,
                        $Object.'ID' => $ObjectID,
                        'FieldValue' => $FieldValue,
                        'CustomFieldID' => $CustomFieldID,
                    ); 
                    $result = $CI->db->insert($Object.'CustomFieldValues', $data);
                }
            }
        }*/

        if(isset($_POST['CustomFieldIDs']) && !empty($_POST['CustomFieldIDs'])){

            foreach ($_POST['CustomFieldIDs'] as $CustomFieldID => $CustomFieldVal) {

                $query = $CI->db->get_where('CustomFields', array('OrganizationID' => $OrganizationID, 'CustomFieldID' => $CustomFieldID)); 
                $result = $query->row(); 
                
                $FieldValue = $CustomFieldVal;
                if(empty($FieldValue) || $FieldValue == NULL)
                    $FieldValue = $result->DefaultValue;

                $query2 = $CI->db->get_where($Object.'CustomFieldValues', array('OrganizationID' => $OrganizationID, 'CustomFieldID' => $CustomFieldID, $Object.'ID' => $ObjectID)); 
                
                if($query2->num_rows() > 0){
                    $result2 = $query2->row_array(); 
                   
                    $data = array(
                        'OrganizationID' =>  $OrganizationID,
                        $Object.'ID' => $ObjectID,
                        'FieldValue' => $FieldValue,
                        'CustomFieldID' => $CustomFieldID
                    ); 
                    $result = $CI->db->update($Object.'CustomFieldValues', $data, array($Object.'CustomFieldValueID' => $result2[$Object.'CustomFieldValueID']));

                } else {

                    $data = array(
                        'OrganizationID' =>  $OrganizationID,
                        $Object.'ID' => $ObjectID,
                        'FieldValue' => $FieldValue,
                        'CustomFieldID' => $CustomFieldID,
                    ); 
                    $result = $CI->db->insert($Object.'CustomFieldValues', $data);
                }
            }
        }
    }
}