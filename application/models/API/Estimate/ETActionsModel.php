<?php
require_once(APPPATH.'models/API/API_Model.php');

class ETActionsModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function CreateEstimate() {

        extract($_POST);

        $data = array(
            'OrganizationID' => $OrganizationID,
            'EstimateName' => $EstimateName,
            'Account' => $Account,
            'Owner' => $Owner,
            'Contact' => !empty($Contact)?$Contact:NULL,
            'BillingAddress' => $BillingAddress,
            'BillingCity' => $BillingCity,
            'BillingState' => $BillingState,
            'BillingCountry' => $BillingCountry,
            'BillingPostalCode' => $BillingPostalCode,
            'BillingLatitude' => $BillingLatitude,
            'BillingLongitude' => $BillingLongitude,
            'BillingName' => $BillingName,
            'Description' => $Description,
            'ExpirationDate' => date("Y-m-d H:i:s",strtotime($ExpirationDate)),
            'Phone' => isset($Phone)?$Phone:NULL,
            'Email' => isset($Email)?$Email:NULL,
            'ShippingAddress' => $ShippingAddress,
            'ShippingCity' => $ShippingCity,
            'ShippingState' => $ShippingState,
            'ShippingCountry' => $ShippingCountry,
            'ShippingPostalCode' => $ShippingPostalCode,
            'ShippingLatitude' => $ShippingLatitude,
            'ShippingLongitude' => $ShippingLongitude,
            'ShippingName' => $ShippingName,
            'EstimateStatus' => $EstimateStatus
        );   
        $query = $this->db->insert('Estimate', $data);  

        API_Model::checkQuery($query);  

        $EstimateID = $this->db->insert_id();

        $EstimateNo = API_Controller::genCode('ET',$EstimateID);

        $query = $this->db->update('Estimate', array('EstimateNo'=>$EstimateNo), array('EstimateID' => $EstimateID));   

        API_Model::checkQuery($query);  

        API_Model::insertCreatedModifiedByAndDt($UserID,'Estimate', array('EstimateID' => $EstimateID));

        API_Model::insertCustomFieldData('Estimate', $EstimateID, $OrganizationID);

        $ResData['EstimateID'] = $EstimateID;
        $ResData['EstimateNo'] = $EstimateNo;
        return $ResData;
    }

    function RecentEstimates($UserID, $OrganizationID)
    {  
        $query = $this->db->select('et.EstimateID, et.EstimateNo as ET#, et.EstimateName, a.AccountName, es.Status, CONCAT(o.FirstName, " ", o.LastName) as Owner')
        ->from('Estimate et')
        ->join('EstimateStatus es','es.EstimateStatusID = et.EstimateStatus', 'left') 
        ->join('Account a','a.AccountID = et.Account', 'left') 
        ->join('User o','o.UserID = et.Owner', 'left') 
        ->where(array('et.OrganizationID' => $OrganizationID, 'et.IsDeleted' => 0))
        ->order_by("et.LastModifiedDate", "desc")
        ->get();  

        API_Model::checkQuery($query);  
        
        return $query->result();
    }

    function EstimateDetails($UserID, $EstimateID, $OrganizationID)
    { 
        $query = $this->db->select('et.*,ac.AccountName, CONCAT(pc.FirstName, " ", pc.LastName) as PrimaryContactName, es.Status, CONCAT(o.FirstName, " ", o.LastName) as OwnerName, DATE_FORMAT(et.ExpirationDate, "'.RES_DATETIME.'") as ExpirationDate, DATE_FORMAT(et.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,DATE_FORMAT(et.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy, org.CompanyName, org.Website, org.Phone as OrgPhone, org.BillingAddress as OrgBillingAddress, org.BillingCity as OrgBillingCity, org.BillingState as OrgBillingState,org.BillingCountry as OrgBillingCountry, org.BillingPostalCode as OrgBillingPostalCode, ac.PhoneNo as AcPhoneNo, org.Tax as TaxRate, org.Tax as Tax')
        ->from('Estimate et')
        ->join('User cb','cb.UserID = et.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = et.LastModifiedBy', 'left')
        ->join('User o','o.UserID = et.Owner', 'left') 
        ->join('EstimateStatus es','es.EstimateStatusID = et.EstimateStatus', 'left') 
        ->join('Contact pc','pc.ContactID = et.Contact', 'left')
        ->join('Account ac','ac.AccountID = et.Account', 'left')
        ->join('Organization org','org.OrganizationID = et.OrganizationID', 'left')
        ->where(array('et.EstimateID' => $EstimateID, 'et.OrganizationID' => $OrganizationID)) // , 'et.IsDeleted' => 0
        ->get();

        API_Model::checkQuery($query); 
        $EstimateDetails = $query->row_array();

        $this->db->select('SUM(SubTotal) as SubTot, SUM(TotalPrice) as TotalPrice, SUM(ListPrice) as ListTot, SUM(UnitPrice) as UnitTot, COUNT(*) as LineItemCnt');
        $this->db->from('EstimateLine');
        $this->db->where(array('Estimate' => $EstimateID, 'OrganizationID' => $OrganizationID, 'IsDeleted' => 0));
        $query = $this->db->get(); 
        $result = $query->row();
        if(!empty($result->SubTot) && !empty($result->TotalPrice) && !empty($result->ListTot) && !empty($result->UnitTot)){
            $Financials['SubTotal'] = $result->SubTot;
            $Financials['Discount'] = (100 - ($result->UnitTot / $result->ListTot) * 100);
            $Financials['Tax'] = ($result->TotalPrice - $result->SubTot);
            $Financials['TotalPrice'] = $result->ListTot;
            $Financials['GrandTotal'] = $result->TotalPrice;
            $Financials['LineItemCount'] = $result->LineItemCnt;
        } else {
            $Financials['SubTotal'] = 0.00;
            $Financials['Discount'] = 0.00;
            $Financials['Tax'] = 0.00;
            $Financials['TotalPrice'] = 0.00;
            $Financials['GrandTotal'] = 0.00;
            $Financials['LineItemCount'] = 0;
        }
     
        return array_merge($EstimateDetails, $Financials); 
    }

    function EstimateRelatedList($UserID, $EstimateID, $OrganizationID)
    { 
        $this->db->select('Count(*) as LineItemCnt');
        $query = $this->db->get_where('EstimateLine el', array('el.Estimate' => $EstimateID, 'el.OrganizationID' => $OrganizationID, 'el.IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['EstimateLine']['title'] = 'Estimate Line Items ('.$result->LineItemCnt.')';    

/*        $this->db->select('Count(*) as ChemicalCnt');
        $query = $this->db->get_where('Chemical c', array('c.Estimate' => $EstimateID, 'c.IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Chemical']['title'] = 'Chemicals ('.$result->ChemicalCnt.')'; 
*/
        $query = $this->db->select('Count(*) as EventCnt')
        ->get_where('Event', array('OrganizationID' => $OrganizationID, 'RelatedTo' => 'Estimate', 'What' => $EstimateID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Event']['title'] = 'Events ('.$result->EventCnt.')';

        $query = $this->db->select('Count(*) as TaskCnt')
        ->get_where('Task', array('OrganizationID' => $OrganizationID, 'RelatedTo' => 'Estimate', 'What' => $EstimateID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Task']['title'] = 'Tasks ('.$result->TaskCnt.')';
 
        $this->db->select('Count(*) as NoteCnt');
        $query = $this->db->get_where('Note n', array('n.OrganizationID' => $OrganizationID, 'n.What' => $EstimateID,'n.RelatedTo' => 'Estimate', 'n.IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Note']['title'] = 'Notes ('.$result->NoteCnt.')';

        $query = $this->db->select('Count(*) as FileCnt')
        ->get_where('File', array('OrganizationID' => $OrganizationID, 'RelatedTo' => 'Estimate', 'What' => $EstimateID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['File']['title'] = 'Files ('.$result->FileCnt.')';

        return $data;
    }

    function EstimateRelatedLineItem($UserID, $EstimateID, $OrganizationID)
    {
        $query = $this->db->select('el.*, p.ProductName, p.IsListPriceEditable, p.IsQuantityEditable, org.Tax, et.ShippingHandling');
        $this->db->from('EstimateLine el');
        $this->db->join('Product p','p.ProductID = el.Product', 'left');
        $this->db->join('Estimate et','et.EstimateID = el.Estimate', 'left');
        $this->db->join('Organization org','org.OrganizationID = el.OrganizationID', 'left');
        $this->db->where(array('el.Estimate' => $EstimateID, 'el.OrganizationID' => $OrganizationID, 'el.IsDeleted' => 0));
        $query = $this->db->get(); 

        API_Model::checkQuery($query);

        return $query->result_array();
    }

  /*  function EstimateRelatedChemical($UserID, $EstimateID)
    {
        $query = $this->db->select('c.*, p.ProductName, tum.UnitOfMeasurement as TestedUnitOfMeasure, aum.UnitOfMeasurement as ApplicationUnitOfMeasure');
        $this->db->from('Chemical c');
        $this->db->join('Product p','p.ProductID = c.Product', 'left');
        $this->db->join('UnitOfMeasurement tum','tum.UnitOfMeasurementID = c.TestedUnitOfMeasure', 'left');
        $this->db->join('UnitOfMeasurement aum','aum.UnitOfMeasurementID = c.ApplicationUnitOfMeasure', 'left');
        $this->db->where(array('c.Estimate' => $EstimateID, 'c.IsDeleted' => 0));
        $query = $this->db->get(); 

        API_Model::checkQuery($query);
        
        return $query->result_array();
    }*/

    function EstimateRelatedEvent($UserID, $EstimateID, $OrganizationID)
    { 
        $query = $this->db->select('e.EventID, e.Subject, es.EventStatus, et.EventTypeName, DATE_FORMAT(e.EventStartDate, "'.RES_DATE.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATE.'") as EventEndDate, DATE_FORMAT(e.EventStartTime, "'.RES_TIME.'") as EventStartTime, DATE_FORMAT(e.EventEndTime, "'.RES_TIME.'") as EventEndTime')
        ->from('Event e') 
        ->join('User cb','cb.UserID = e.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = e.LastModifiedBy', 'left')
        ->join('EventStatus es','es.EventStatusID = e.EventStatus', 'left')
        ->join('EventType et','et.EventTypeID = e.EventType', 'left')
        ->where(array('e.What' => $EstimateID, 'e.OrganizationID' => $OrganizationID, 'e.RelatedTo' => 'Estimate', 'e.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function EstimateRelatedTask($UserID, $EstimateID, $OrganizationID)
    {  
        $query = $this->db->select('t.TaskID, t.Subject, tt.TaskType, tp.Priority, ts.TaskStatus')
        ->from('Task t') 
        ->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left')
        ->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left')
        ->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left')
        ->where(array('t.What' => $EstimateID, 't.OrganizationID' => $OrganizationID, 't.RelatedTo' => 'Estimate', 't.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function EstimateRelatedNote($UserID, $EstimateID, $OrganizationID)
    {  
        $this->db->select('n.NoteID, n.Subject, DATE_FORMAT(n.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, CONCAT(o.FirstName, " ", o.LastName) as OwnerName');
        $this->db->from('Note n'); 
        $this->db->join('User o','o.UserID = n.Owner', 'left');
        $this->db->where(array('n.OrganizationID' => $OrganizationID, 'n.What' => $EstimateID,'n.RelatedTo' => 'Estimate', 'n.IsDeleted' => 0));
        $query = $this->db->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function EstimateRelatedFile($UserID, $EstimateID, $OrganizationID)
    {  
        $query = $this->db->select('f.FileID, f.FileName, f.Subject, f.ContentType')
        ->from('File f') 
        ->where(array('f.OrganizationID' => $OrganizationID, 'f.What' => $EstimateID,'f.RelatedTo' => 'Estimate', 'f.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function GetProducts()
    {
        extract($_POST);

        $Products = array();

        $query = $this->db->select('p.ProductID, p.ProductCode, p.ProductName, p.Description, p.ListPrice, p.DefaultQuantity, p.Taxable, p.IsListPriceEditable, p.IsQuantityEditable');
        $this->db->from('Product p');
        $this->db->where(array('p.OrganizationID' => $OrganizationID, 'p.IsDeleted' => 0, 'p.IsActive' => 1));
        if(isset($ProductFamily) && !empty($ProductFamily)){
            $this->db->where('ProductFamily',  $ProductFamily);  
        }
        if(isset($ProductCode) && !empty($ProductCode)){
            $this->db->like('ProductCode',  $ProductCode);  
        }
        $query = $this->db->get(); 
        $result = $query->result();

        API_Model::checkQuery($query);

        $query = $this->db->get_where('Estimate', array('EstimateID'=>$EstimateID));  
        $EstimateTax = $query->row();

        $query = $this->db->get_where('Organization', array('OrganizationID'=>$OrganizationID));  
        $OrganizationTax = $query->row();

        foreach ($result as $key => $value) {
            $Product = $value;
            $Product->Tax = $OrganizationTax->Tax;
            $Product->ShippingHandling = $EstimateTax->ShippingHandling;
            
            $Products[] = $Product;
            
        }

        return $Products;
    }

    /*function GetChemicals()
    {
        $this->db->select('p.ProductID, p.ProductCode, p.ProductName, p.Description, p.ListPrice, p.DefaultQuantity, p.Taxable');
        $this->db->from('Product p');
        $this->db->where(array('p.IsChemical' => 1, 'p.IsDeleted' => 0, 'p.IsActive' => 1));
        $query = $this->db->get(); 

        API_Model::checkQuery($query);

        return $query->result();
    }*/

    function EditEstimate() {
                
        extract($_POST);

        $data = array(
            'EstimateName' => $EstimateName,
            'Account' => $Account,
            'Owner' => $Owner,
            'Contact' => !empty($Contact)?$Contact:NULL,
            'BillingAddress' => $BillingAddress,
            'BillingCity' => $BillingCity,
            'BillingState' => $BillingState,
            'BillingCountry' => $BillingCountry,
            'BillingPostalCode' => $BillingPostalCode,
            'BillingLatitude' => $BillingLatitude,
            'BillingLongitude' => $BillingLongitude,
            'BillingName' => $BillingName,
            'Description' => $Description,
            'ExpirationDate' => date("Y-m-d H:i:s",strtotime($ExpirationDate)),
            'Phone' => isset($Phone)?$Phone:NULL,
            'Email' => isset($Email)?$Email:NULL,
            'ShippingAddress' => $ShippingAddress,
            'ShippingCity' => $ShippingCity,
            'ShippingState' => $ShippingState,
            'ShippingCountry' => $ShippingCountry,
            'ShippingPostalCode' => $ShippingPostalCode,
            'ShippingLatitude' => $ShippingLatitude,
            'ShippingLongitude' => $ShippingLongitude,
            'ShippingName' => $ShippingName,
            'EstimateStatus' => $EstimateStatus
        );    
        $query = $this->db->update('Estimate', $data, array('OrganizationID' => $OrganizationID, 'EstimateID' => $EstimateID));   

        API_Model::checkQuery($query);

        API_Model::updateCreatedModifiedByAndDt($UserID,'Estimate', array('EstimateID' => $EstimateID));

        API_Model::updateCustomFieldData('Estimate', $EstimateID, $OrganizationID);
    
        return $query;
    }

    function SaveEstimateSignature($UserID, $EstimateID, $Signature, $OrganizationID)
    {
        $data = array(
            'Signature' => $Signature
        );
        $query = $this->db->update('Estimate', $data, array('OrganizationID' => $OrganizationID, 'EstimateID' => $EstimateID));   

        API_Model::checkQuery($query);

        API_Model::updateCreatedModifiedByAndDt($UserID,'Estimate', array('EstimateID' => $EstimateID));
    
        return $query;
    }
 
    function SaveEstimateLineItems()
    { 
        extract($_POST);

        $query = $this->db->delete('EstimateLine', array('Estimate' => $EstimateID));
        
        if(isset($Product) && count($Product) > 0 && is_array($Product)){ 

            foreach ($Product as $key => $value) {
               
               if(!empty($Product[$key])){
                    $data = array(
                        'OrganizationID' => $OrganizationID,
                        'Estimate' => $EstimateID,
                        'Product' => $Product[$key],
                        'ListPrice' => $ListPrice[$key],
                        'Discount' => $Discount[$key],
                        'UnitPrice' => $UnitPrice[$key],
                        'Quantity' => $Quantity[$key],
                        'SubTotal' => $SubTotal[$key],
                        'Taxable' => empty($Taxable[$key])?0:1,
                        'TotalPrice' => $TotalPrice[$key]
                    );     
                    $query = $this->db->insert('EstimateLine', $data);  
                    $EstimateLineID = $this->db->insert_id();

                    $EstimateLineNo = MY_Controller::genCode('ETL' ,$EstimateLineID);
                    $this->db->update('EstimateLine', array('EstimateLineNo'=>$EstimateLineNo), array('EstimateLineID' => $EstimateLineID));  
           
                    API_Model::insertCreatedModifiedByAndDt($UserID,'EstimateLine', array('EstimateLineID' => $EstimateLineID));
                }
            }
        }

        return $query;
    }

    function ConvertToWorkOrder()
    {
        extract($_POST);

        $query = $this->db->get_where('Estimate', array('OrganizationID' => $OrganizationID, 'EstimateID' => $EstimateID)); 
        
        if($query->num_rows() > 0){ 

            $result = $query->row(); 

            $data = array(
                'OrganizationID' => $OrganizationID,
                'AssignedTo' => $result->Owner,
                'Account' => $result->Account,
                'Subject' => $result->EstimateName,
                'PrimaryContact' => $result->Contact,
                'Description' => $result->Description,
                'Address' => $result->BillingAddress,
                'City' => $result->BillingCity,
                'State' => $result->BillingState,
                'Country' => $result->BillingCountry,
                'PostalCode' => $result->BillingPostalCode,
                'Latitude' => $result->BillingLatitude,
                'Longitude' => $result->BillingLongitude,
            );     
            $result = $this->db->insert('WorkOrder', $data); 
            $WorkOrderID = $this->db->insert_id();

            $WorkOrderNo = API_Controller::genCode('WO', $WorkOrderID);
            $this->db->update('WorkOrder', array('WorkOrderNo' => $WorkOrderNo), array('WorkOrderID' => $WorkOrderID));  

            API_Model::insertCreatedModifiedByAndDt($UserID,'WorkOrder', array('WorkOrderID' => $WorkOrderID));

            $query = $this->db->get_where('EstimateLine', array('OrganizationID' => $OrganizationID, 'Estimate' => $EstimateID));
            if($query->num_rows() > 0){ 

                $LineItemsResult = $query->result(); 

                foreach ($LineItemsResult as $key => $LineItem) {
                    
                    $data = array(
                        'OrganizationID' => $OrganizationID,
                        'WorkOrder' => $WorkOrderID,
                        'Product' => $LineItem->Product,
                        'ListPrice' => $LineItem->ListPrice,
                        'Discount' => $LineItem->Discount,
                        'UnitPrice' => $LineItem->UnitPrice,
                        'Quantity' => $LineItem->Quantity,
                        'SubTotal' => $LineItem->SubTotal,
                        'Taxable' => $LineItem->Taxable,
                        'NetTotal' => $LineItem->TotalPrice,
                    );     
                    $query = $this->db->insert('WOLineItem', $data);  
                    $WOLineItemID = $this->db->insert_id();

                    $LineItemNo = API_Controller::genCode('WOLI' ,$WOLineItemID);
                    $this->db->update('WOLineItem', array('LineItemNo'=>$LineItemNo), array('WOLineItemID' => $WOLineItemID));  
           
                    API_Model::insertCreatedModifiedByAndDt($UserID,'WOLineItem', array('WOLineItemID' => $WOLineItemID));
                }
            }

            return $WorkOrderID;
        }

        return 0;
    }

    function GetTemplateContainsData($UserID, $EstimateID, $OrganizationID){

        $EstimateData = $this->EstimateDetails($UserID, $EstimateID, $OrganizationID);
        $EstimateLineItemsData = $this->ETActionsModel->EstimateRelatedLineItem($UserID, $EstimateID, $OrganizationID);
        
        $data = array();

        $data['AssignedTo'] = $EstimateData['Owner'];
        $data['EstimateNo'] = $EstimateData['EstimateNo'];
        
        $company = array();
        $company["name"] = $EstimateData['CompanyName'];
        $company["street_address"] = $EstimateData['OrgBillingAddress'];
        $company["city_state_zip"] = $EstimateData['OrgBillingCity'].', '.$EstimateData['OrgBillingState'].', '.$EstimateData['OrgBillingCountry'].', '.$EstimateData['OrgBillingPostalCode'];
        $company["phone"] = $EstimateData['OrgPhone'];
        $company["fax"] = "";
        $company["website"] = $EstimateData['Website'];
        $data["company"] = $company;

       /* $host = array();
        $host["logo"] = dirname(__FILE__).'/logo.png';
        $data["host"] = $host;*/

        $et = array();
        $et["no"] = $EstimateData['EstimateNo'];
        $et["date"] = date("m/d/Y", strtotime($EstimateData['ExpirationDate']));
        $et["description"] = $EstimateData['Description'];
        $et["subject"] = $EstimateData['EstimateName'];
        $data["et"] = $et;

        $customer = array();
        $customer["name"] = "-";
        $customer["id"] = "-";
        $data["customer"] = $customer;

        $bill = array();
        $bill["name"] = "john jojo";
        $bill["company_name"] = $EstimateData['CompanyName'];
        $bill["street_address"] = $EstimateData['BillingAddress'];
        $bill["city_state_zip"] = $EstimateData['BillingCity'].','.$EstimateData['BillingState'].','.$EstimateData['BillingCountry'].','.$EstimateData['BillingPostalCode'];
        $bill["phone"] = $EstimateData['AcPhoneNo'];
        $data["bill"] = $bill;

        $items = array();
        $taxable = 0;

        foreach ($EstimateLineItemsData as $key => $EstimateLineItem) {
            
            $item = array();
            $item["qty"] = $EstimateLineItem['Quantity'];
            $item["description"] = $EstimateLineItem['ProductName'];
            $item["tax"] = ($EstimateLineItem['Taxable'] == 1) ? "Y" : "N";
            $item["price"] = number_format((double)$EstimateLineItem['UnitPrice'], 2, '.', ',');
            $item["line_total"] = number_format((double)$EstimateLineItem['TotalPrice'], 2, '.', ',');
            $items[] = $item;

            $taxable += ($EstimateLineItem['Taxable'] == 1) ? $EstimateLineItem['TotalPrice'] : 0;
        } 
        $data["items"] = $items;

        $invoice = array();
        $invoice["subtotal"] = number_format($EstimateData['SubTotal'], 2, '.', ',');
        $invoice["taxable"] = number_format($taxable, 2, '.', ',');
        $invoice["tax_rate"] = $EstimateData['TaxRate'].'%';
        $invoice["tax"] = number_format((double)$EstimateData['Tax'], 2, '.', ',');
        $invoice["s_h"] = "-";
        $invoice["other"] = "-";
        $invoice["total"] = number_format((double)$EstimateData['GrandTotal'], 2, '.', ',');
        $data["invoice"] = $invoice;

        return $data; 
    }
}

?>
