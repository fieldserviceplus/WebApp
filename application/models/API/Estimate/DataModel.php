<?php
require_once(APPPATH.'models/API/API_Model.php');

class DataModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }

    function GetEstimateStatus($UserID, $OrganizationID)
    { 
        $query = $this->db->select('EstimateStatusID, Status, IsDefault, IsClosed')
        ->from('EstimateStatus')
        ->where(array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }
}

?>
