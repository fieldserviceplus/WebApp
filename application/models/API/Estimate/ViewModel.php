<?php
require_once(APPPATH.'models/API/API_Model.php');

class ViewModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function GetViews($UserID, $OrganizationID)
    { 
        $EstimateDefaultView = array(
                array('EstimateViewID' => 'MyOpenEstimates', 'EstimateViewName' => 'My Open Estimates'),
                array('EstimateViewID' => 'AllOpenEstimates', 'EstimateViewName' => 'All Open Estimates'),
                array('EstimateViewID' => 'EstimatesCreatedThisWeek', 'EstimateViewName' => 'Estimates Created This Week')
            );

        $query = $this->db->select('EstimateViewID, EstimateViewName')
        ->from('EstimateView')
        ->where(array('OrganizationID' => $OrganizationID, 'UserID' => $UserID, 'RestrictVisibility'=>'VisibleOnlyToMe'))
        ->or_where(array('RestrictVisibility' => 'VisibleToEveryone'))
        ->order_by('EstimateViewName', 'asc')
        ->get();   

        API_Model::checkQuery($query); 
        
        return array_merge($EstimateDefaultView, $query->result());
    }

    function GetDefaultEstimateData($UserID, $OrganizationID, $EstimateViewID, $FilterData = array())
    {
            $WhereString = "";
            if($EstimateViewID == 'MyOpenEstimates'){

                $WhereString .= "e.Owner = $UserID AND es.IsClosed=0 AND e.IsDeleted=0 AND e.OrganizationID = '".$OrganizationID."'";
            }
            else if($EstimateViewID == 'AllOpenEstimates'){

                $WhereString .= "es.IsClosed=0 AND e.IsDeleted=0 AND e.OrganizationID = '".$OrganizationID."'";

            } else if($EstimateViewID == 'EstimatesCreatedThisWeek'){

                $WhereString .= "YEARWEEK(e.CreatedDate, 1) = YEARWEEK(CURDATE(), 1) AND e.IsDeleted=0 AND e.OrganizationID = '".$OrganizationID."'";

            } else {
                 $WhereString .= "e.IsDeleted=0 AND e.OrganizationID = '".$OrganizationID."'";
            }

            if(!empty($FilterData)){
                foreach ($FilterData['FilterField'] as $key => $value) {

                    $WhereField = "";
                    if($FilterData['FilterField'][$key] == 'Owner'){
                        $WhereField = "CONCAT(ow.FirstName, ' ', ow.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'Account') {
                        $WhereField = "ac.AccountName";
                    } else if($FilterData['FilterField'][$key] == 'OrganizationID') {
                        $WhereField = "o.CompanyName";
                    } else if($FilterData['FilterField'][$key] == 'EstimateStatus') {
                        $WhereField = "es.Status as EstimateStatus";
                    } else if($FilterData['FilterField'][$key] == 'CreatedBy') {
                        $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'LastModifiedBy') {
                        $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'Contact') {
                        $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'ExpirationDate'){
                         $WhereField = "DATE_FORMAT(e.ExpirationDate, '".RES_DATETIME."')";
                    } else {
                        $WhereField = "e.".$FilterData['FilterField'][$key];
                    }

                    if($FilterData['FilterCondition'][$key] == 'Equals'){
                         $WhereString .= " AND $WhereField = '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'NotEqualTo'){
                        $WhereString .= " AND $WhereField != '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'LessThan'){
                        $WhereString .= " AND $WhereField < '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'GreaterThan'){
                        $WhereString .= " AND $WhereField > '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'LessOREqualTo'){
                        $WhereString .= " AND $WhereField <= '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'GreaterOREqualTo'){
                        $WhereString .= " AND $WhereField >= '".$FilterData['FilterValue'][$key]."'";
                    } else if($FilterData['FilterCondition'][$key] == 'Contains'){
                        $WhereString .= " AND $WhereField LIKE '%".$FilterData['FilterValue'][$key]."%'";
                    } else if($FilterData['FilterCondition'][$key] == 'StartsWith'){ 
                        $WhereString .= " AND $WhereField LIKE '".$FilterData['FilterValue'][$key]."%'"; 
                    } else if($FilterData['FilterCondition'][$key] == 'DoesNotContain'){
                        $WhereString .= " AND $WhereField NOT LIKE '%".$FilterData['FilterValue'][$key]."%'";  
                    } 
                }
            }
            $OrderByString = "";
            if(isset($FilterData['SortByField']) && isset($FilterData['SortByValue']) && !empty($FilterData['SortByField']) && !empty($FilterData['SortByValue']) && ($FilterData['SortByValue'] == 'asc' || $FilterData['SortByValue'] == 'desc')){ 
                $OrderByString .= "ORDER BY ".$FilterData['SortByField']." ".$FilterData['SortByValue']."";
            }

            $query = "SELECT e.EstimateID, IF(e.Owner IS NOT NULL, CONCAT(ow.FirstName, ' ', ow.LastName), '') as Owner, e.EstimateNo, ac.AccountName, e.EstimateName, IF(cnt.FirstName IS NOT NULL,CONCAT(cnt.FirstName, ' ', cnt.LastName), '') as ContactName, es.Status, e.BillingCity, e.BillingState
            FROM Estimate e
            LEFT JOIN User ow ON ow.UserID = e.Owner
            LEFT JOIN User cb ON cb.UserID = e.CreatedBy
            LEFT JOIN Account ac ON ac.AccountID = e.Account
            LEFT JOIN Contact cnt ON cnt.ContactID = e.Contact
            LEFT JOIN Organization o ON o.OrganizationID = e.OrganizationID
            LEFT JOIN EstimateStatus es ON es.EstimateStatusID = e.EstimateStatus
            WHERE $WhereString $OrderByString"; 
            $query = $this->db->query($query);
     
            API_Model::checkQuery($query); 

            return $query->result_array();
    }

    function ViewEstimateList($UserID, $EstimateViewID, $OrganizationID)
    {    
        if($EstimateViewID == 'MyOpenEstimates' 
            || $EstimateViewID == 'AllOpenEstimates' 
            || $EstimateViewID == 'EstimatesCreatedThisWeek'
        ){

            return $this->GetDefaultEstimateData($UserID, $OrganizationID, $EstimateViewID);

        } else {

            $data = array();
            $query = $this->db->get_where('EstimateView', array('OrganizationID' => $OrganizationID, 'EstimateViewID' => $EstimateViewID)); 
            $EstimateViewData = $query->row();

            if($query->num_rows() > 0){
                $query = $this->db->get_where('EstimateFilter', array('EstimateViewID' => $EstimateViewID)); 
                $EstimateFilterData = $query->result_array();

                $ColName = "e.EstimateID, ac.AccountID, ";
                if(!empty($EstimateViewData->SpecifyFieldsDisplay)){
                    $SpecifyFieldsDisplays = explode(",", $EstimateViewData->SpecifyFieldsDisplay);
                    foreach ($SpecifyFieldsDisplays as $key => $value) {
                        if($value == 'Owner'){
                            $ColName .= "CONCAT(ow.FirstName, ' ', ow.LastName) as Owner";
                        } else if($value == 'Account'){
                            $ColName .= "ac.AccountName as Account";
                        } else if($value == 'OrganizationID'){
                            $ColName .= "o.CompanyName as OrganizationID";
                        } else if($value == 'EstimateStatus'){
                            $ColName .= "es.Status as EstimateStatus";
                        } else if($value == 'Contact'){
                            $ColName .= "CONCAT(pc.FirstName, ' ', pc.LastName) as Contact";
                        } else if($value == 'CreatedBy'){
                            $ColName .= "CONCAT(cr.FirstName, ' ', cr.LastName) as CreatedBy";
                        } else if($value == 'LastModifiedBy'){
                            $ColName .= "CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedBy";
                        } else if($value == 'ExpirationDate'){
                            $ColName .= "DATE_FORMAT(e.ExpirationDate, '".RES_DATETIME."') as ExpirationDate";
                        } else {
                            $ColName .= "e.".$value;
                        }

                        if($key != count($SpecifyFieldsDisplays)){
                            $ColName .= ", ";
                        }
                    }
                }
                      
                /* Filter Estimate Data Start */
                $this->db->select($ColName);
                $this->db->from('Estimate e');
                $this->db->join('User ow','ow.UserID = e.Owner', 'left');
                $this->db->join('Account ac','ac.AccountID = e.Account', 'left');
                $this->db->join('Organization o','o.OrganizationID = e.OrganizationID', 'left');
                $this->db->join('EstimateStatus es','es.EstimateStatusID = e.EstimateStatus', 'left');
                $this->db->join('User cr','cr.UserID = e.CreatedBy', 'left');
                $this->db->join('User lmb','lmb.UserID = e.LastModifiedBy', 'left');
                $this->db->join('Contact pc','pc.ContactID = e.Contact', 'left');
                $this->db->where(array('e.OrganizationID' => $OrganizationID, 'e.IsDeleted' => 0));

                foreach ($EstimateFilterData as $key => $value) {

                    $WhereField = "";
                    if($value['FilterField'] == 'Owner'){
                        $WhereField = "CONCAT(ow.FirstName, ' ', ow.LastName)";
                    } else if($value['FilterField'] == 'Account') {
                        $WhereField = "ac.AccountName";
                    } else if($value['FilterField'] == 'OrganizationID') {
                        $WhereField = "o.CompanyName";
                    } else if($value['FilterField'] == 'EstimateStatus') {
                        $WhereField = "es.Status as EstimateStatus";
                    } else if($value['FilterField'] == 'CreatedBy') {
                        $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                    } else if($value['FilterField'] == 'LastModifiedBy') {
                        $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                    } else if($value['FilterField'] == 'Contact') {
                        $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                    } else if($value['FilterField'] == 'ExpirationDate'){
                         $WhereField = "DATE_FORMAT(e.ExpirationDate, '".RES_DATETIME."')";
                    } else {
                        $WhereField = "e.".$value['FilterField'];
                    }

                    if($value['FilterCondition'] == 'Equals'){
                        $this->db->where($WhereField.' =',  $value['FilterValue']);  
                    } else if($value['FilterCondition'] == 'NotEqualTo'){
                        $this->db->where($WhereField.' !=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'LessThan'){
                        $this->db->where($WhereField.' <', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'GreaterThan'){
                        $this->db->where($WhereField.' >', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'LessOREqualTo'){
                        $this->db->where($WhereField.' <=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'GreaterOREqualTo'){
                        $this->db->where($WhereField.' >=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'Contains'){
                        $this->db->like($WhereField, $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'StartsWith'){ 
                        $this->db->like($WhereField, $value['FilterValue'], 'after');   
                    } else if($value['FilterCondition'] == 'DoesNotContain'){
                        $this->db->not_like($WhereField, $value['FilterValue']);   
                    } 
                } 
                if(!empty($EstimateViewData->SortByField) && !empty($EstimateViewData->SortByValue)){
                    $this->db->order_by($EstimateViewData->SortByField, $EstimateViewData->SortByValue);
                }
                $query = $this->db->get();  
                //echo  $this->db->last_query();exit;
                API_Model::checkQuery($query);

                $data = $query->result_array();
            } 

            return $data;

        }
    }

    function EstimateFilter($UserID, $EstimateViewID, $FilterFields, $FilterConditions, $FilterValues, $SortByField, $SortByValue, $OrganizationID)
    { 

        $FilterData = array(
            'FilterField' => $FilterFields,
            'FilterCondition' => $FilterConditions,
            'FilterValue' => $FilterValues,
            'SortByField' => $SortByField,
            'SortByValue' => $SortByValue,
        );
     
        return $this->GetDefaultEstimateData($UserID, $OrganizationID, $EstimateViewID, $FilterData);
    }

    function GetEstimateViewFields()
    {
        $query = $this->db->select('FieldName')
        ->from('EstimateViewFields')
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }
}

?>
