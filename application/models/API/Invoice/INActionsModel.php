<?php

require_once(APPPATH.'models/API/API_Model.php');

class INActionsModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function CreateInvoice() {

        extract($_POST);

        $data = array(
            'OrganizationID' => $OrganizationID,
            'WorkOrder' => $WorkOrder,
            'AssignedTo' => $AssignedTo,
            'Account' => $Account,
            'Contact' => !empty($Contact)?$Contact:NULL,
            'Description' => $Description,
            'Address' => $Address,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => (double)$Latitude,
            'Longitude' => (double)$Longitude,
            'InvoiceStatus' => $InvoiceStatus,
            'InvoiceDate' => date("Y-m-d",strtotime($InvoiceDate)),
            'DueDate' => date("Y-m-d",strtotime($DueDate)),
            'PaymentTerms' => $PaymentTerms,
            'AdditionalInformation' => isset($AdditionalInformation)?$AdditionalInformation:NULL,
        );   
        $query = $this->db->insert('Invoice', $data);  

        API_Model::checkQuery($query);  

        $InvoiceID = $this->db->insert_id();

        $InvoiceNo = API_Controller::genCode('IN',$InvoiceID);

        $query = $this->db->update('Invoice', array('InvoiceNo'=>$InvoiceNo), array('InvoiceID' => $InvoiceID));   

        API_Model::checkQuery($query);  

        API_Model::insertCreatedModifiedByAndDt($UserID,'Invoice', array('InvoiceID' => $InvoiceID));

        API_Model::insertCustomFieldData('Invoice', $InvoiceID, $OrganizationID);

        $ResData['InvoiceID'] = $InvoiceID;
        $ResData['InvoiceNo'] = $InvoiceNo;
        return $ResData;
    }

    function RecentInvoices($UserID, $OrganizationID)
    {  
        $query = $this->db->select('i.InvoiceID, i.InvoiceNo as IN#, a.AccountName, es.InvoiceStatus, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName')
        ->from('Invoice i')
        ->join('InvoiceStatus es','es.InvoiceStatusID = i.InvoiceStatus', 'left') 
        ->join('Account a','a.AccountID = i.Account', 'left') 
        ->join('User at','at.UserID = i.AssignedTo', 'left') 
        ->where(array('i.OrganizationID' => $OrganizationID, 'i.IsDeleted' => 0))
        ->order_by("i.LastModifiedDate", "desc")
        ->get();  

        API_Model::checkQuery($query);  
        
        return $query->result();
    }

    function InvoiceDetails($UserID, $InvoiceID, $OrganizationID)
    {  
        $query = $this->db->select('i.*, wo.Subject as WorkOrderSubject, ac.AccountName, ac.PhoneNo, CONCAT(pc.FirstName, " ", pc.LastName) as ContactName, es.InvoiceStatus, es.InvoiceStatusID, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, pt.PaymentTerms, pt.InvoicePaymentTermID, DATE_FORMAT(i.InvoiceDate, "'.RES_DATE.'") as InvoiceDate, DATE_FORMAT(i.DueDate, "'.RES_DATE.'") as DueDate, DATE_FORMAT(i.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,DATE_FORMAT(i.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy, org.CompanyName, org.Website, org.Phone as OrgPhone, org.BillingAddress as OrgBillingAddress, org.BillingCity as OrgBillingCity, org.BillingState as OrgBillingState,org.BillingCountry as OrgBillingCountry, org.BillingPostalCode as OrgBillingPostalCode, ac.PhoneNo as AcPhoneNo, org.Tax as TaxRate, org.Tax as Tax')
        ->from('Invoice i')
        ->join('User cb','cb.UserID = i.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = i.LastModifiedBy', 'left')
        ->join('User at','at.UserID = i.AssignedTo', 'left') 
        ->join('InvoiceStatus es','es.InvoiceStatusID = i.InvoiceStatus', 'left')
        ->join('InvoicePaymentTerm pt','pt.InvoicePaymentTermID = i.PaymentTerms', 'left') 
        ->join('Contact pc','pc.ContactID = i.Contact', 'left')
        ->join('Account ac','ac.AccountID = i.Account', 'left')
        ->join('WorkOrder wo','wo.WorkOrderID = i.WorkOrder', 'left')
        ->join('Organization org','org.OrganizationID = i.OrganizationID', 'left')
        ->where(array('i.InvoiceID' => $InvoiceID, 'i.OrganizationID' => $OrganizationID)) //, 'i.IsDeleted' => 0
        ->get();

        API_Model::checkQuery($query); 
        $InvoiceDetails = $query->row_array();

        $this->db->select('SUM(SubTotal) as SubTot, SUM(TotalPrice) as TotalPrice, SUM(ListPrice) as ListTot, SUM(UnitPrice) as UnitTot, COUNT(*) as LineItemCnt');
        $this->db->from('InvoiceLineItem');
        $this->db->where(array('Invoice' => $InvoiceID, 'OrganizationID' => $OrganizationID, 'IsDeleted' => 0));
        $query = $this->db->get(); 
        $result = $query->row();
        if(!empty($result->SubTot) && !empty($result->TotalPrice) && !empty($result->ListTot) && !empty($result->UnitTot)){
            $Financials['SubTotal'] = $result->SubTot;
            $Financials['Discount'] = (100 - ($result->UnitTot / $result->ListTot) * 100);
            $Financials['Tax'] = ($result->TotalPrice - $result->SubTot);
            $Financials['TotalPrice'] = $result->ListTot;
            $Financials['GrandTotal'] = $result->TotalPrice;
            $Financials['LineItemCount'] = $result->LineItemCnt;
        } else {
            $Financials['SubTotal'] = 0.00;
            $Financials['Discount'] = 0.00;
            $Financials['Tax'] = 0.00;
            $Financials['TotalPrice'] = 0.00;
            $Financials['GrandTotal'] = 0.00;
            $Financials['LineItemCount'] = 0;
        }
     
        return array_merge($InvoiceDetails, $Financials); 
    }

    function InvoiceRelatedList($UserID, $InvoiceID, $OrganizationID)
    { 
        $this->db->select('Count(*) as LineItemCnt');
        $query = $this->db->get_where('InvoiceLineItem il', array('il.Invoice' => $InvoiceID, 'il.OrganizationID' => $OrganizationID, 'il.IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['InvoiceLineItems']['title'] = 'Invoice Line Items ('.$result->LineItemCnt.')';    

/*        $this->db->select('Count(*) as ChemicalCnt');
        $query = $this->db->get_where('Chemical c', array('c.Invoice' => $InvoiceID, 'c.IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Chemical']['title'] = 'Chemicals ('.$result->ChemicalCnt.')'; 
*/
        $query = $this->db->select('Count(*) as EventCnt')
        ->get_where('Event', array('OrganizationID' => $OrganizationID, 'RelatedTo' => 'Invoice', 'What' => $InvoiceID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Event']['title'] = 'Events ('.$result->EventCnt.')';

        $query = $this->db->select('Count(*) as TaskCnt')
        ->get_where('Task', array('OrganizationID' => $OrganizationID, 'RelatedTo' => 'Invoice', 'What' => $InvoiceID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Task']['title'] = 'Tasks ('.$result->TaskCnt.')';
 
        $query = $this->db->select('Count(*) as FileCnt')
        ->get_where('File', array('OrganizationID' => $OrganizationID, 'RelatedTo' => 'Invoice', 'What' => $InvoiceID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['File']['title'] = 'Files ('.$result->FileCnt.')';

        $this->db->select('Count(*) as NoteCnt');
        $query = $this->db->get_where('Note n', array('n.OrganizationID' => $OrganizationID, 'n.RelatedTo' => 'Invoice', 'n.What' => $InvoiceID, 'n.IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Note']['title'] = 'Notes ('.$result->NoteCnt.')';

        return $data;
    }

    function InvoiceRelatedLineItem($UserID, $InvoiceID, $OrganizationID)
    {
        $query = $this->db->select('il.*, p.ProductName, p.IsListPriceEditable, p.IsQuantityEditable, org.Tax');
        $this->db->from('InvoiceLineItem il');
        $this->db->join('Product p','p.ProductID = il.Product', 'left');
        $this->db->join('Invoice et','et.InvoiceID = il.Invoice', 'left');
        $this->db->join('Organization org','org.OrganizationID = il.OrganizationID', 'left');
        $this->db->where(array('il.Invoice' => $InvoiceID, 'il.OrganizationID' => $OrganizationID, 'il.IsDeleted' => 0));
        $query = $this->db->get(); 

        API_Model::checkQuery($query);

        return $query->result_array();
    }

  /*  function InvoiceRelatedChemical($UserID, $InvoiceID)
    {
        $query = $this->db->select('c.*, p.ProductName, tum.UnitOfMeasurement as TestedUnitOfMeasure, aum.UnitOfMeasurement as ApplicationUnitOfMeasure');
        $this->db->from('Chemical c');
        $this->db->join('Product p','p.ProductID = c.Product', 'left');
        $this->db->join('UnitOfMeasurement tum','tum.UnitOfMeasurementID = c.TestedUnitOfMeasure', 'left');
        $this->db->join('UnitOfMeasurement aum','aum.UnitOfMeasurementID = c.ApplicationUnitOfMeasure', 'left');
        $this->db->where(array('c.Invoice' => $InvoiceID, 'c.IsDeleted' => 0));
        $query = $this->db->get(); 

        API_Model::checkQuery($query);
        
        return $query->result_array();
    }*/

    function InvoiceRelatedEvent($UserID, $InvoiceID, $OrganizationID)
    { 
        $query = $this->db->select('e.EventID, e.Subject, es.EventStatus, et.EventTypeName, DATE_FORMAT(e.EventStartDate, "'.RES_DATE.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATE.'") as EventEndDate, DATE_FORMAT(e.EventStartTime, "'.RES_TIME.'") as EventStartTime, DATE_FORMAT(e.EventEndTime, "'.RES_TIME.'") as EventEndTime')
        ->from('Event e') 
        ->join('User cb','cb.UserID = e.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = e.LastModifiedBy', 'left')
        ->join('EventStatus es','es.EventStatusID = e.EventStatus', 'left')
        ->join('EventType et','et.EventTypeID = e.EventType', 'left')
        ->where(array('e.What' => $InvoiceID, 'e.OrganizationID' => $OrganizationID, 'e.RelatedTo' => 'Invoice', 'e.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function InvoiceRelatedTask($UserID, $InvoiceID, $OrganizationID)
    {  
        $query = $this->db->select('t.TaskID, t.Subject, tt.TaskType, tp.Priority, ts.TaskStatus')
        ->from('Task t') 
        ->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left')
        ->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left')
        ->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left')
        ->where(array('t.What' => $InvoiceID, 't.OrganizationID' => $OrganizationID, 't.RelatedTo' => 'Invoice', 't.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function InvoiceRelatedNote($UserID, $InvoiceID, $OrganizationID)
    {  
        $this->db->select('n.NoteID, n.Subject, DATE_FORMAT(n.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, CONCAT(o.FirstName, " ", o.LastName) as OwnerName');
        $this->db->from('Note n'); 
        $this->db->join('User o','o.UserID = n.Owner', 'left');
        $this->db->where(array('n.OrganizationID' => $OrganizationID, 'n.What' => $InvoiceID,'n.RelatedTo' => 'Invoice', 'n.IsDeleted' => 0));
        $query = $this->db->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function InvoiceRelatedFile($UserID, $InvoiceID, $OrganizationID)
    {  
        $query = $this->db->select('f.FileID, f.FileName, f.Subject, f.ContentType')
        ->from('File f') 
        ->where(array('f.OrganizationID' => $OrganizationID, 'f.What' => $InvoiceID,'f.RelatedTo' => 'Invoice', 'f.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function GetProducts()
    {
        extract($_POST);

        $Products = array();

        $query = $this->db->select('p.ProductID, p.ProductCode, p.ProductName, p.Description, p.ListPrice, p.DefaultQuantity, p.Taxable, p.IsListPriceEditable, p.IsQuantityEditable');
        $this->db->from('Product p');
        $this->db->where(array('p.OrganizationID' => $OrganizationID, 'p.IsDeleted' => 0, 'p.IsActive' => 1));
        if(isset($ProductFamily) && !empty($ProductFamily)){
            $this->db->where('ProductFamily',  $ProductFamily);  
        }
        if(isset($ProductCode) && !empty($ProductCode)){
            $this->db->like('ProductCode',  $ProductCode);  
        }
        $query = $this->db->get(); 
        $result = $query->result();

        API_Model::checkQuery($query);

        /*$query = $this->db->get_where('Invoice', array('InvoiceID'=>$InvoiceID));  
        $InvoiceTax = $query->row();*/

        $query = $this->db->get_where('Organization', array('OrganizationID'=>$OrganizationID));  
        $OrganizationTax = $query->row();

        foreach ($result as $key => $value) {
            $Product = $value;
            $Product->Tax = $OrganizationTax->Tax;
            
            $Products[] = $Product;
            
        }

        return $Products;
    }

    /*function GetChemicals()
    {
        $this->db->select('p.ProductID, p.ProductCode, p.ProductName, p.Description, p.ListPrice, p.DefaultQuantity, p.Taxable');
        $this->db->from('Product p');
        $this->db->where(array('p.IsChemical' => 1, 'p.IsDeleted' => 0, 'p.IsActive' => 1));
        $query = $this->db->get(); 

        API_Model::checkQuery($query);

        return $query->result();
    }*/

    function EditInvoice() {
                
        extract($_POST);

        $data = array(
            'OrganizationID' => $OrganizationID,
            'WorkOrder' => $WorkOrder,
            'AssignedTo' => $AssignedTo,
            'Account' => $Account,
            'Contact' => !empty($Contact)?$Contact:NULL,
            'Description' => $Description,
            'Address' => $Address,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => (double)$Latitude,
            'Longitude' => (double)$Longitude,
            'InvoiceStatus' => $InvoiceStatus,
            'InvoiceDate' => date("Y-m-d",strtotime($InvoiceDate)),
            'DueDate' => date("Y-m-d",strtotime($DueDate)),
            'PaymentTerms' => $PaymentTerms,
            'AdditionalInformation' => isset($AdditionalInformation)?$AdditionalInformation:NULL,
        );   
        $query = $this->db->update('Invoice', $data, array('OrganizationID' => $OrganizationID, 'InvoiceID' => $InvoiceID));   

        API_Model::checkQuery($query);

        API_Model::updateCreatedModifiedByAndDt($UserID,'Invoice', array('InvoiceID' => $InvoiceID));

        API_Model::updateCustomFieldData('Invoice', $InvoiceID, $OrganizationID);
    
        return $query;
    }

    /*function SaveInvoiceSignature($UserID, $InvoiceID, $Signature, $OrganizationID)
    {
        $data = array(
            'Signature' => $Signature
        );
        $query = $this->db->update('Invoice', $data, array('OrganizationID' => $OrganizationID, 'InvoiceID' => $InvoiceID));   

        API_Model::checkQuery($query);

        API_Model::updateCreatedModifiedByAndDt($UserID,'Invoice', array('InvoiceID' => $InvoiceID));
    
        return $query;
    }*/
 
    function SaveInvoiceLineItems()
    { 
        extract($_POST);

        $query = $this->db->delete('InvoiceLineItem', array('Invoice' => $InvoiceID, 'OrganizationID' => $OrganizationID));
        
        if(isset($Product) && count($Product) > 0 && is_array($Product)){ 

            foreach ($Product as $key => $value) {
               
               if(!empty($Product[$key])){
                    $data = array(
                        'OrganizationID' => $OrganizationID,
                        'Invoice' => $InvoiceID,
                        'Product' => $Product[$key],
                        'ListPrice' => $ListPrice[$key],
                        'Discount' => $Discount[$key],
                        'UnitPrice' => $UnitPrice[$key],
                        'Quantity' => $Quantity[$key],
                        'SubTotal' => $SubTotal[$key],
                        'Taxable' => empty($Taxable[$key])?0:1,
                        'TotalPrice' => $TotalPrice[$key]
                    );     
                    $query = $this->db->insert('InvoiceLineItem', $data); 
                    
                    $InvoiceLineItemID = $this->db->insert_id();

                    $InvoiceLineNo = MY_Controller::genCode('INL' ,$InvoiceLineItemID);
                    $this->db->update('InvoiceLineItem', array('InvoiceLineNo'=>$InvoiceLineNo), array('InvoiceLineItemID' => $InvoiceLineItemID));  
           
                    API_Model::insertCreatedModifiedByAndDt($UserID,'InvoiceLineItem', array('InvoiceLineItemID' => $InvoiceLineItemID));
                }
            }
        }

        return $query;
    }

    function GetTemplateContainsData($UserID, $InvoiceID, $OrganizationID){

        $InvoiceData = $this->InvoiceDetails($UserID, $InvoiceID, $OrganizationID);
        $InvoiceLineItemsData = $this->INActionsModel->InvoiceRelatedLineItem($UserID, $InvoiceID, $OrganizationID);
        
        $data = array();

        $data['AssignedTo'] = $InvoiceData['AssignedTo'];
        $data['InvoiceNo'] = $InvoiceData['InvoiceNo'];
        
        $company = array();
        $company["name"] = $InvoiceData['CompanyName'];
        $company["street_address"] = $InvoiceData['OrgBillingAddress'];
        $company["city_state_zip"] = $InvoiceData['OrgBillingCity'].', '.$InvoiceData['OrgBillingState'].', '.$InvoiceData['OrgBillingCountry'].', '.$InvoiceData['OrgBillingPostalCode'];
        $company["phone"] = $InvoiceData['OrgPhone'];
        $company["fax"] = "";
        $company["website"] = $InvoiceData['Website'];
        $data["company"] = $company;

       /* $host = array();
        $host["logo"] = dirname(__FILE__).'/logo.png';
        $data["host"] = $host;*/

        $in = array();
        $in["no"] = $InvoiceData['InvoiceNo'];
        $in["date"] = date("m/d/Y", strtotime($InvoiceData['InvoiceDate']));
        $in["description"] = $InvoiceData['Description'];
        $data["in"] = $in;

        $customer = array();
        $customer["name"] = "-";
        $customer["id"] = "-";
        $data["customer"] = $customer;

        $bill = array();
        $bill["name"] = "john jojo";
        $bill["company_name"] = $InvoiceData['CompanyName'];
        $bill["street_address"] = $InvoiceData['Address'];
        $bill["city_state_zip"] = $InvoiceData['City'].','.$InvoiceData['State'].','.$InvoiceData['Country'].','.$InvoiceData['PostalCode'];
        $bill["phone"] = $InvoiceData['AcPhoneNo'];
        $data["bill"] = $bill;

        $items = array();
        $taxable = 0;

        foreach ($InvoiceLineItemsData as $key => $InvoiceLineItem) {
            
            $item = array();
            $item["qty"] = $InvoiceLineItem['Quantity'];
            $item["description"] = $InvoiceLineItem['ProductName'];
            $item["tax"] = ($InvoiceLineItem['Taxable'] == 1) ? "Y" : "N";
            $item["price"] = number_format((double)$InvoiceLineItem['UnitPrice'], 2, '.', ',');
            $item["line_total"] = number_format((double)$InvoiceLineItem['TotalPrice'], 2, '.', ',');
            $items[] = $item;

            $taxable += ($InvoiceLineItem['Taxable'] == 1) ? $InvoiceLineItem['TotalPrice'] : 0;
        } 
        $data["items"] = $items;

        $invoice = array();
        $invoice["subtotal"] = number_format($InvoiceData['SubTotal'], 2, '.', ',');
        $invoice["taxable"] = number_format($taxable, 2, '.', ',');
        $invoice["tax_rate"] = $InvoiceData['TaxRate'].'%';
        $invoice["tax"] = number_format((double)$InvoiceData['Tax'], 2, '.', ',');
        $invoice["s_h"] = "-";
        $invoice["other"] = "-";
        $invoice["total"] = number_format((double)$InvoiceData['GrandTotal'], 2, '.', ',');
        $data["invoice"] = $invoice;

        return $data; 
    }
}

?>
