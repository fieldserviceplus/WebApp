<?php
require_once(APPPATH.'models/API/API_Model.php');

class DataModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }

    function GetInvoiceStatus($UserID, $OrganizationID)
    { 
        $query = $this->db->select('InvoiceStatusID, InvoiceStatus, IsDefault, IsClosed, IsPaid')
        ->from('InvoiceStatus')
        ->where(array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetInvoicePaymentTerms($UserID, $OrganizationID)
    { 
        $query = $this->db->select('InvoicePaymentTermID, PaymentTerms, IsDefault')
        ->from('InvoicePaymentTerm')
        ->where(array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }
}

?>
