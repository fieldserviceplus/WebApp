<?php
require_once(APPPATH.'models/API/API_Model.php');

class ViewModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
  
    function GetViews($UserID, $OrganizationID)
    { 
        $InvoiceDefaultView = array(
                array('InvoiceViewID' => 'MyOpenInvoices', 'InvoiceViewName' => 'My Open Invoices'),
                array('InvoiceViewID' => 'AllOpenInvoices', 'InvoiceViewName' => 'All Open Invoices'),
                array('InvoiceViewID' => 'AllOpenInvoicesPastDue', 'InvoiceViewName' => 'All Open Invoices Past Due'),
                array('InvoiceViewID' => 'InvoicesCreatedThisWeek', 'InvoiceViewName' => 'Invoices Created This Week')
            );

        $query = $this->db->select('InvoiceViewID, InvoiceViewName')
        ->from('InvoiceView')
        ->where(array('OrganizationID' => $OrganizationID, 'UserID' => $UserID, 'RestrictVisibility'=>'VisibleOnlyToMe'))
        ->or_where(array('RestrictVisibility' => 'VisibleToEveryone'))
        ->order_by('InvoiceViewName', 'asc')
        ->get();   

        API_Model::checkQuery($query); 
        
        return array_merge($InvoiceDefaultView, $query->result());
    }

    function GetDefaultInvoiceData($UserID, $OrganizationID, $InvoiceViewID, $FilterData = array())
    {
            $WhereString = "";
            if($InvoiceViewID == 'MyOpenInvoices'){

                $WhereString .= "i.AssignedTo = $UserID AND es.IsClosed=0 AND i.IsDeleted=0 AND i.OrganizationID = '".$OrganizationID."'";
            }
            else if($InvoiceViewID == 'AllOpenInvoices'){

                $WhereString .= "es.IsClosed=0 AND i.IsDeleted=0 AND i.OrganizationID = '".$OrganizationID."'";

            } else if($InvoiceViewID == 'AllOpenInvoicesPastDue'){

                $WhereString .= "DATE(i.DueDate) < CURDATE() AND es.IsClosed=0 AND i.IsDeleted=0 AND i.OrganizationID = '".$OrganizationID."'";

            } else if($InvoiceViewID == 'InvoicesCreatedThisWeek'){

                $WhereString .= "YEARWEEK(i.CreatedDate, 1) = YEARWEEK(CURDATE(), 1) AND i.IsDeleted=0 AND i.OrganizationID = '".$OrganizationID."'";

            } else {
                 $WhereString .= "i.IsDeleted=0 AND i.OrganizationID = '".$OrganizationID."'";
            }

            if(!empty($FilterData)){
                foreach ($FilterData['FilterField'] as $key => $value) {

                    $WhereField = "";
                    if($FilterData['FilterField'][$key] == 'Owner'){
                        $WhereField = "CONCAT(ow.FirstName, ' ', ow.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'Account') {
                        $WhereField = "ac.AccountName";
                    } else if($FilterData['FilterField'][$key] == 'WorkOrder') {
                        $WhereField = "wo.Subject as WorkOrder";
                    } else if($FilterData['FilterField'][$key] == 'OrganizationID') {
                        $WhereField = "o.CompanyName";
                    } else if($FilterData['FilterField'][$key] == 'InvoiceStatus') {
                        $WhereField = "es.Status as InvoiceStatus";
                    } else if($FilterData['FilterField'][$key] == 'PaymentTerms') {
                        $WhereField = "pt.PaymentTerms";
                    } else if($FilterData['FilterField'][$key] == 'CreatedBy') {
                        $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'LastModifiedBy') {
                        $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'Contact') {
                        $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'InvoiceDate'){
                         $WhereField = "DATE_FORMAT(i.InvoiceDate, '".RES_DATETIME."')";
                    } else if($FilterData['FilterField'][$key] == 'DueDate'){
                         $WhereField = "DATE_FORMAT(i.DueDate, '".RES_DATETIME."')";
                    } else {
                        $WhereField = "i.".$FilterData['FilterField'][$key];
                    }

                    if($FilterData['FilterCondition'][$key] == 'Equals'){
                         $WhereString .= " AND $WhereField = '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'NotEqualTo'){
                        $WhereString .= " AND $WhereField != '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'LessThan'){
                        $WhereString .= " AND $WhereField < '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'GreaterThan'){
                        $WhereString .= " AND $WhereField > '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'LessOREqualTo'){
                        $WhereString .= " AND $WhereField <= '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'GreaterOREqualTo'){
                        $WhereString .= " AND $WhereField >= '".$FilterData['FilterValue'][$key]."'";
                    } else if($FilterData['FilterCondition'][$key] == 'Contains'){
                        $WhereString .= " AND $WhereField LIKE '%".$FilterData['FilterValue'][$key]."%'";
                    } else if($FilterData['FilterCondition'][$key] == 'StartsWith'){ 
                        $WhereString .= " AND $WhereField LIKE '".$FilterData['FilterValue'][$key]."%'"; 
                    } else if($FilterData['FilterCondition'][$key] == 'DoesNotContain'){
                        $WhereString .= " AND $WhereField NOT LIKE '%".$FilterData['FilterValue'][$key]."%'";  
                    } 
                }
            }
            $OrderByString = "";
            if(isset($FilterData['SortByField']) && isset($FilterData['SortByValue']) && !empty($FilterData['SortByField']) && !empty($FilterData['SortByValue']) && ($FilterData['SortByValue'] == 'asc' || $FilterData['SortByValue'] == 'desc')){ 
                $OrderByString .= "ORDER BY ".$FilterData['SortByField']." ".$FilterData['SortByValue']."";
            }
 
            $query = "SELECT i.InvoiceID, IF(i.AssignedTo IS NOT NULL, CONCAT(at.FirstName, ' ', at.LastName), '') as AssignedTo, i.InvoiceNo, ac.AccountName, IF(cnt.FirstName IS NOT NULL,CONCAT(cnt.FirstName, ' ', cnt.LastName), '') as ContactName, es.InvoiceStatus, i.City, i.State
            FROM Invoice i
            LEFT JOIN User at ON at.UserID = i.AssignedTo
            LEFT JOIN User cb ON cb.UserID = i.CreatedBy
            LEFT JOIN Account ac ON ac.AccountID = i.Account
            LEFT JOIN WorkOrder wo ON wo.WorkOrderID = i.WorkOrder
            LEFT JOIN Contact cnt ON cnt.ContactID = i.Contact
            LEFT JOIN Organization o ON o.OrganizationID = i.OrganizationID
            LEFT JOIN InvoiceStatus es ON es.InvoiceStatusID = i.InvoiceStatus
            LEFT JOIN InvoicePaymentTerm pt ON pt.InvoicePaymentTermID = i.PaymentTerms
            WHERE $WhereString $OrderByString"; 
            $query = $this->db->query($query);
     
            API_Model::checkQuery($query); 

            return $query->result_array();
    }

    function ViewInvoiceList($UserID, $InvoiceViewID, $OrganizationID)
    {    
        if($InvoiceViewID == 'MyOpenInvoices' 
            || $InvoiceViewID == 'AllOpenInvoices' 
            || $InvoiceViewID == 'AllOpenInvoicesPastDue' 
            || $InvoiceViewID == 'InvoicesCreatedThisWeek'
        ){

            return $this->GetDefaultInvoiceData($UserID, $OrganizationID, $InvoiceViewID);

        } else {

            $data = array();
            $query = $this->db->get_where('InvoiceView', array('OrganizationID' => $OrganizationID, 'InvoiceViewID' => $InvoiceViewID)); 
            $InvoiceViewData = $query->row();

            if($query->num_rows() > 0){
                $query = $this->db->get_where('InvoiceFilter', array('InvoiceViewID' => $InvoiceViewID)); 
                $InvoiceFilterData = $query->result_array();

                $ColName = "i.InvoiceID, ac.AccountID, wo.WorkOrderID, ";
                if(!empty($InvoiceViewData->SpecifyFieldsDisplay)){
                    $SpecifyFieldsDisplays = explode(",", $InvoiceViewData->SpecifyFieldsDisplay);
                    foreach ($SpecifyFieldsDisplays as $key => $value) {
                        if($value == 'AssignedTo'){
                            $ColName .= "CONCAT(at.FirstName, ' ', at.LastName) as AssignedTo";
                        } else if($value == 'Account'){
                            $ColName .= "ac.AccountName as Account";
                        } else if($value == 'WorkOrder'){
                            $ColName .= "wo.Subject as WorkOrder";
                        } else if($value == 'OrganizationID'){
                            $ColName .= "o.CompanyName as OrganizationID";
                        } else if($value == 'InvoiceStatus'){
                            $ColName .= "es.InvoiceStatus as InvoiceStatus";
                        } else if($value == 'PaymentTerms'){
                            $ColName .= "pt.PaymentTerms";
                        } else if($value == 'Contact'){
                            $ColName .= "CONCAT(pc.FirstName, ' ', pc.LastName) as Contact";
                        } else if($value == 'CreatedBy'){
                            $ColName .= "CONCAT(cr.FirstName, ' ', cr.LastName) as CreatedBy";
                        } else if($value == 'LastModifiedBy'){
                            $ColName .= "CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedBy";
                        } else if($value == 'InvoiceDate'){
                            $ColName .= "DATE_FORMAT(i.InvoiceDate, '".RES_DATETIME."') as InvoiceDate";
                        } else if($value == 'DueDate'){
                            $ColName .= "DATE_FORMAT(i.DueDate, '".RES_DATETIME."') as DueDate";
                        } else {
                            $ColName .= "i.".$value;
                        }

                        if($key != count($SpecifyFieldsDisplays)){
                            $ColName .= ", ";
                        }
                    }
                }
                      
                /* Filter Invoice Data Start */
                $this->db->select($ColName);
                $this->db->from('Invoice i');
                $this->db->join('User at','at.UserID = i.AssignedTo', 'left');
                $this->db->join('Account ac','ac.AccountID = i.Account', 'left');
                $this->db->join('WorkOrder wo','wo.WorkOrderID = i.WorkOrder', 'left');
                $this->db->join('Organization o','o.OrganizationID = i.OrganizationID', 'left');
                $this->db->join('InvoiceStatus es','es.InvoiceStatusID = i.InvoiceStatus', 'left');
                $this->db->join('InvoicePaymentTerm pt','pt.InvoicePaymentTermID = i.PaymentTerms', 'left');
                $this->db->join('User cr','cr.UserID = i.CreatedBy', 'left');
                $this->db->join('User lmb','lmb.UserID = i.LastModifiedBy', 'left');
                $this->db->join('Contact pc','pc.ContactID = i.Contact', 'left');
                $this->db->where(array('i.OrganizationID' => $OrganizationID, 'i.IsDeleted' => 0));

                foreach ($InvoiceFilterData as $key => $value) {

                    $WhereField = "";
                    if($value['FilterField'] == 'Owner'){
                        $WhereField = "CONCAT(ow.FirstName, ' ', ow.LastName)";
                    } else if($value['FilterField'] == 'Account') {
                        $WhereField = "ac.AccountName";
                    } else if($value['FilterField'] == 'WorkOrder') {
                        $WhereField = "wo.Subject as WorkOrder";
                    } else if($value['FilterField'] == 'OrganizationID') {
                        $WhereField = "o.CompanyName";
                    } else if($value['FilterField'] == 'InvoiceStatus') {
                        $WhereField = "es.InvoiceStatus as InvoiceStatus";
                    } else if($value['FilterField'] == 'PaymentTerms') {
                        $WhereField = "pt.PaymentTerms";
                    } else if($value['FilterField'] == 'CreatedBy') {
                        $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                    } else if($value['FilterField'] == 'LastModifiedBy') {
                        $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                    } else if($value['FilterField'] == 'Contact') {
                        $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                    } else if($value['FilterField'] == 'InvoiceDate'){
                         $WhereField = "DATE_FORMAT(i.InvoiceDate, '".RES_DATETIME."')";
                    } else if($value['FilterField'] == 'DueDate'){
                         $WhereField = "DATE_FORMAT(i.DueDate, '".RES_DATETIME."')";
                    } else {
                        $WhereField = "i.".$value['FilterField'];
                    }

                    if($value['FilterCondition'] == 'Equals'){
                        $this->db->where($WhereField.' =',  $value['FilterValue']);  
                    } else if($value['FilterCondition'] == 'NotEqualTo'){
                        $this->db->where($WhereField.' !=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'LessThan'){
                        $this->db->where($WhereField.' <', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'GreaterThan'){
                        $this->db->where($WhereField.' >', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'LessOREqualTo'){
                        $this->db->where($WhereField.' <=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'GreaterOREqualTo'){
                        $this->db->where($WhereField.' >=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'Contains'){
                        $this->db->like($WhereField, $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'StartsWith'){ 
                        $this->db->like($WhereField, $value['FilterValue'], 'after');   
                    } else if($value['FilterCondition'] == 'DoesNotContain'){
                        $this->db->not_like($WhereField, $value['FilterValue']);   
                    } 
                } 
                if(!empty($InvoiceViewData->SortByField) && !empty($InvoiceViewData->SortByValue)){
                    $this->db->order_by($InvoiceViewData->SortByField, $InvoiceViewData->SortByValue);
                }
                $query = $this->db->get();  
                //echo  $this->db->last_query();exit;
                API_Model::checkQuery($query);

                $data = $query->result_array();
            } 

            return $data;

        }
    }

    function InvoiceFilter($UserID, $InvoiceViewID, $FilterFields, $FilterConditions, $FilterValues, $SortByField, $SortByValue, $OrganizationID)
    { 

        $FilterData = array(
            'FilterField' => $FilterFields,
            'FilterCondition' => $FilterConditions,
            'FilterValue' => $FilterValues,
            'SortByField' => $SortByField,
            'SortByValue' => $SortByValue,
        );
     
        return $this->GetDefaultInvoiceData($UserID, $OrganizationID, $InvoiceViewID, $FilterData);
    }

    function GetInvoiceViewFields()
    {
        $query = $this->db->select('FieldName')
        ->from('InvoiceViewFields')
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }
}

?>
