<?php
require_once(APPPATH.'models/API/API_Model.php');

class AcActionsModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function CreateAccount($OrganizationID, $UserID, $AssignedTo, $AccountName, $PhoneNo, $BillingAddress, $BillingCity, $BillingState, $BillingCountry, $BillingPostalCode, $BillingLatitude, $BillingLongitude, $ShippingAddress, $ShippingCity, $ShippingState, $ShippingCountry, $ShippingPostalCode, $ShippingLatitude, $ShippingLongitude, $AccessNotes, $PopUpReminder, $Notes, $AccountType, $PreferredTechnician, $IsActive) {
                 
        $data = array(
            'OrganizationID' => $OrganizationID,
            'AssignedTo' => $AssignedTo,
            'AccountName' => $AccountName,
            'PhoneNo' => $PhoneNo,
            'PreferredTechnician' => !empty($PreferredTechnician)?$PreferredTechnician:NULL,
            'BillingAddress' => $BillingAddress,
            'BillingState' => $BillingState,
            'BillingCity' => $BillingCity,
            'BillingCountry' => $BillingCountry,
            'BillingPostalCode' => $BillingPostalCode,
            'BillingLatitude' => $BillingLatitude,
            'BillingLongitude' => $BillingLongitude,
            'ShippingAddress' => $ShippingAddress,
            'ShippingState' => $ShippingState,
            'ShippingCity' => $ShippingCity,
            'ShippingCountry' => $ShippingCountry,
            'ShippingPostalCode' => $ShippingPostalCode,
            'ShippingLatitude' => $ShippingLatitude,
            'ShippingLongitude' => $ShippingLongitude,
            'AccessNotes' => $AccessNotes,
            'PopUpReminder' => $PopUpReminder,
            'Notes' => $Notes,
            'AccountType' => $AccountType,
            'IsActive' => $IsActive
        ); 
        $query = $this->db->insert('Account', $data);  

        API_Model::checkQuery($query);   

        $AccountID = $this->db->insert_id();

        $AccountNo = 'AC'.'-'.sprintf("%05d", $AccountID);
        $query = $this->db->update('Account', array('AccountNo' => $AccountNo), array('AccountID' => $AccountID)); 

        API_Model::checkQuery($query);    

        API_Model::insertCreatedModifiedByAndDt($UserID,'Account', array('AccountID' => $AccountID));

        API_Model::insertCustomFieldData('Account', $AccountID, $OrganizationID);
    
        return array('AccountID' => $AccountID, 'AccountNo' => $AccountNo);
    }

    function RecentAccounts($UserID, $OrganizationID)
    { 
        $query = $this->db->select('a.AccountID, a.AccountName, a.PhoneNo, at.AccountType as Type, a.BillingCity as City, a.BillingState as State, CONCAT(ast.FirstName, " ", ast.LastName) as AssignedTo')
        ->from('Account a')
        ->join('User ast','ast.UserID = a.AssignedTo', 'left') 
        ->join('AccountType at','at.AccountTypeID = a.AccountType', 'left') 
        ->where(array('a.OrganizationID' => $OrganizationID, 'a.IsActive' => 1, 'a.IsDeleted' => 0))
        ->order_by("a.LastModifiedDate", "desc")
        ->get();   
 
        API_Model::checkQuery($query); 

        return $query->result();
    }

    function AccountDetails($UserID, $AccountID, $OrganizationID)
    { 

        $query = $this->db->select('a.*, CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName, CONCAT(pc.FirstName, " ", pc.LastName) as PrimaryContactName, CONCAT(pmt.FirstName, " ", pmt.LastName) as PreferredTechnicianName, at.AccountType as AccountTypeName, DATE_FORMAT(a.LastActivityDate, "'.RES_DATE.'") as LastActivityDate, DATE_FORMAT(a.LastServiceDate, "'.RES_DATE.'") as LastServiceDate, DATE_FORMAT(a.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,DATE_FORMAT(a.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy, a.AccountName as BillingName, a.AccountName as ShippingName')
        ->from('Account a')
        ->join('User cb','cb.UserID = a.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = a.LastModifiedBy', 'left')
        ->join('AccountType at','at.AccountTypeID = a.AccountType', 'left')
        ->join('User ast','ast.UserID = a.AssignedTo', 'left')
        ->join('User pmt','pmt.UserID = a.PreferredTechnician', 'left')
        ->join('Contact pc','pc.ContactID = a.PrimaryContact', 'left')
        ->where(array('a.OrganizationID' => $OrganizationID, 'a.AccountID' => $AccountID,'a.IsActive' => 1)) // , 'a.IsDeleted' => 0
        ->get();

        API_Model::checkQuery($query); 

        $data = $query->row_array(); 

        return $data;
    }

    function AccountRelatedList($UserID, $AccountID, $OrganizationID)
    { 
        
        $query = $this->db->select('Count(*) as ContactCnt')
        ->get_where('Contact', array('Account' => $AccountID, 'OrganizationID' => $OrganizationID, 'IsActive' => 1, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Contact']['title'] = 'Contacts ('.$result->ContactCnt.')';

       /* $query = $this->db->select('Count(*) as LocationCnt')
        ->get_where('Location', array('Account' => $AccountID, 'OrganizationID' => $OrganizationID, 'IsActive' => 1, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Location']['title'] = 'Locations ('.$result->LocationCnt.')';*/

        $query = $this->db->select('Count(*) as WorkOrderCnt')
        ->get_where('WorkOrder', array('Account' => $AccountID, 'OrganizationID' => $OrganizationID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['WorkOrder']['title'] = 'Work Orders ('.$result->WorkOrderCnt.')';

        $query = $this->db->select('Count(*) as EstimateCnt')
        ->get_where('Estimate', array('Account' => $AccountID, 'OrganizationID' => $OrganizationID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Estimate']['title'] = 'Estimates ('.$result->EstimateCnt.')';

        $query = $this->db->select('Count(*) as InvoiceCnt')
        ->get_where('Invoice', array('Account' => $AccountID, 'OrganizationID' => $OrganizationID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Invoice']['title'] = 'Invoices ('.$result->InvoiceCnt.')';

        $query = $this->db->select('Count(*) as FileCnt')
         ->get_where('File', array('RelatedTo' => 'Account', 'OrganizationID' => $OrganizationID, 'What' => $AccountID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['File']['title'] = 'Files ('.$result->FileCnt.')';

        $query = $this->db->select('Count(*) as EventCnt')
        ->get_where('Event', array('RelatedTo' => 'Account', 'OrganizationID' => $OrganizationID, 'What' => $AccountID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Event']['title'] = 'Events ('.$result->EventCnt.')';

        $query = $this->db->select('Count(*) as TaskCnt')
        ->get_where('Task', array('RelatedTo' => 'Account', 'OrganizationID' => $OrganizationID, 'What' => $AccountID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Task']['title'] = 'Tasks ('.$result->TaskCnt.')';

        /*$query = $this->db->select('Count(*) as ChemicalCnt')
        ->get_where('Chemical', array('Account' => $AccountID, 'OrganizationID' => $OrganizationID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Chemical']['title'] = 'Chemicals ('.$result->ChemicalCnt.')';

        $query = $this->db->select('Count(*) as ProductCnt')
        ->get_where('Product', array('Account' => $AccountID, 'OrganizationID' => $OrganizationID, 'IsDeleted' => 0, 'TrackInstalledProduct' => 1));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Product']['title'] = 'Installed Products ('.$result->ProductCnt.')';*/

        return $data;
    }

    function AccountRelatedContact($UserID, $AccountID, $OrganizationID)
    { 
        $query = $this->db->select('c.ContactID, c.FirstName, c.LastName, c.Title, c.PhoneNo, c.Email, CONCAT(c.FirstName, " ", c.LastName) as FullName')
        ->from('Contact c') 
        ->join('User cb','cb.UserID = c.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = c.LastModifiedBy', 'left')
        ->where(array('c.Account' => $AccountID, 'c.OrganizationID' => $OrganizationID, 'c.IsActive' => 1, 'c.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function AccountRelatedLocation($UserID, $AccountID, $OrganizationID)
    { 
        $query = $this->db->select('l.LocationID, l.Name, l.Address, l.City, l.State')
        ->from('Location l') 
        ->where(array('l.Account' => $AccountID, 'l.OrganizationID' => $OrganizationID, 'l.IsActive' => 1, 'l.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function AccountRelatedWorkOrder($UserID, $AccountID, $OrganizationID)
    { 
        $query = $this->db->select('wo.WorkOrderID, wo.WorkOrderNo, wo.Subject, wop.Priority, wos.Status, woc.CategoryName, DATE_FORMAT(wo.StartDate, "'.RES_DATETIME.'") as StartDate') 
        ->from('WorkOrder wo') 
        ->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left')
        ->join('WOCategory woc','woc.WOCategoryID = wo.WOCategory', 'left')
        ->join('WOPriority wop','wop.WOPriorityID = wo.WOPriority', 'left')
        ->where(array('wo.Account' => $AccountID, 'wo.OrganizationID' => $OrganizationID, 'wo.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function AccountRelatedEstimate($UserID, $AccountID, $OrganizationID)
    { 
        $this->db->select('e.EstimateID ,e.EstimateNo, e.EstimateName, DATE_FORMAT(e.ExpirationDate, "'.RES_DATE.'") as ExpirationDate, (Select SUM(TotalPrice) FROM EstimateLine WHERE Estimate=e.EstimateID) as GrandTotal, es.Status, CONCAT(o.FirstName, " ", o.LastName) as OwnerName, DATE_FORMAT(e.CreatedDate, "'.RES_DATETIME.'") as CreatedDate');
        $this->db->from('Estimate e');
        $this->db->join('User o','o.UserID = e.Owner', 'left');
        $this->db->join('EstimateStatus es','es.EstimateStatusID = e.EstimateStatus', 'left');
        $this->db->where(array('e.Account' => $AccountID, 'e.OrganizationID' => $OrganizationID, 'e.IsDeleted' => 0));
        $query = $this->db->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function AccountRelatedInvoice($UserID, $AccountID, $OrganizationID)
    { 
        $query = $this->db->select('i.InvoiceID, i.InvoiceNo as InvoiceNumber, DATE_FORMAT(i.InvoiceDate, "'.RES_DATE.'") as DueDate, DATE_FORMAT(i.DueDate, "'.RES_DATE.'") as DueDate, is.InvoiceStatus, (Select SUM(TotalPrice) FROM InvoiceLineItem WHERE Invoice=i.InvoiceID) as TotalPrice, (Select SUM(SubTotal) FROM InvoiceLineItem WHERE Invoice=i.InvoiceID) as SubTotal, wo.Subject') 
        ->from('Invoice i') 
        ->join('InvoiceStatus is','is.InvoiceStatusID = i.InvoiceStatus', 'left')
        ->join('WorkOrder wo','wo.WorkOrderID = i.WorkOrder', 'left')
        ->where(array('i.Account' => $AccountID, 'i.OrganizationID' => $OrganizationID, 'i.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function AccountRelatedTask($UserID, $AccountID, $OrganizationID)
    { 
        $query = $this->db->select('t.TaskID, t.Subject, CONCAT(c.FirstName, " ", c.LastName) as Name, tt.TaskType, DATE_FORMAT(t.ActivityDate, "'.RES_DATE.'") as Date, CONCAT(u.FirstName, " ", u.LastName) as AssignedTo, ts.TaskStatus, tp.Priority') 
        ->from('Task t') 
        ->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left')
        ->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left')
        ->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left')
        ->join('Contact c','c.ContactID = t.Who', 'left')
        ->join('User u','u.UserID = t.AssignedTo', 'left')
        ->where(array('t.OrganizationID' => $OrganizationID, 't.RelatedTo' => 'Account', 't.What' => $AccountID, 't.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function AccountRelatedEvent($UserID, $AccountID, $OrganizationID)
    { 
        $query = $this->db->select('e.EventID, e.Subject, CONCAT(c.FirstName, " ", c.LastName) as Name, DATE_FORMAT(e.EventStartDate, "'.RES_DATE.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATETIME.'") as EventEndDate, CONCAT(u.FirstName, " ", u.LastName) as AssignedTo, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, DATE_FORMAT(e.CreatedDate, "'.RES_DATETIME.'") as CreatedDate') 
        ->from('Event e') 
        ->join('Contact c','c.ContactID = e.Who', 'left')
        ->join('User u','u.UserID = e.AssignedTo', 'left')
        ->join('User cb','cb.UserID = e.CreatedBy', 'left')
        ->where(array('e.OrganizationID' => $OrganizationID, 'e.RelatedTo' => 'Account', 'e.What' => $AccountID, 'e.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function AccountRelatedFile($UserID, $AccountID, $OrganizationID)
    { 
        $query = $this->db->select('f.FileID, f.FileName, f.ContentType, f.Subject, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate') 
        ->from('File f') 
        ->join('User u','u.UserID = f.AssignedTo', 'left')
        ->join('User cb','cb.UserID = f.CreatedBy', 'left')
        ->where(array('f.OrganizationID' => $OrganizationID, 'f.RelatedTo' => 'Account', 'f.What' => $AccountID, 'f.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function AccountRelatedChemical($UserID, $AccountID, $OrganizationID)
    {  
        $query = $this->db->select('p.ProductID, c.ChemicalNo, p.ProductName, p.Account, wo.Subject as WorkOrder, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, DATE_FORMAT(c.CreatedDate, "'.RES_DATETIME.'") as CreatedDate') 
        ->from('Chemical c') 
        ->join('User cb','cb.UserID = c.CreatedBy', 'left')
        ->join('Product p','p.ProductID = c.Product', 'left')
        ->join('WorkOrder wo','wo.WorkOrderID = c.WorkOrder', 'left')
        ->where(array('c.OrganizationID' => $OrganizationID, 'c.Account' => $AccountID, 'c.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function AccountRelatedProduct($UserID, $AccountID, $OrganizationID)
    {  
        $query = $this->db->select('p.ProductID, p.ProductCode, p.ProductName, p.DefaultQuantity, p.ListPrice, p.DatePurchased, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, DATE_FORMAT(p.CreatedDate, "'.RES_DATETIME.'") as CreatedDate') 
        ->from('Product p')  
        ->join('User cb','cb.UserID = p.CreatedBy', 'left')
        ->where(array('p.OrganizationID' => $OrganizationID, 'p.Account' => $AccountID, 'p.IsDeleted' => 0, 'TrackInstalledProduct' => 1))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function EditAccount($OrganizationID, $AccountID, $UserID, $AssignedTo, $AccountName, $PhoneNo, $BillingAddress, $BillingCity, $BillingState, $BillingCountry, $BillingPostalCode, $BillingLatitude, $BillingLongitude, $ShippingAddress, $ShippingCity, $ShippingState, $ShippingCountry, $ShippingPostalCode, $ShippingLatitude, $ShippingLongitude, $AccessNotes, $PopUpReminder, $Notes, $AccountType, $PreferredTechnician,$LastActivityDate, $LastServiceDate, $PrimaryContact, $IsActive) {
           
        $data = array(
            'OrganizationID' => $OrganizationID,
            'AssignedTo' => $AssignedTo,
            'AccountName' => $AccountName,
            'PhoneNo' => $PhoneNo,
            'PreferredTechnician' => !empty($PreferredTechnician)?$PreferredTechnician:NULL,
            'BillingAddress' => $BillingAddress,
            'BillingState' => $BillingState,
            'BillingCity' => $BillingCity,
            'BillingCountry' => $BillingCountry,
            'BillingPostalCode' => $BillingPostalCode,
            'BillingLatitude' => $BillingLatitude,
            'BillingLongitude' => $BillingLongitude,
            'ShippingAddress' => $ShippingAddress,
            'ShippingState' => $ShippingState,
            'ShippingCity' => $ShippingCity,
            'ShippingCountry' => $ShippingCountry,
            'ShippingPostalCode' => $ShippingPostalCode,
            'ShippingLatitude' => $ShippingLatitude,
            'ShippingLongitude' => $ShippingLongitude,
            'AccessNotes' => $AccessNotes,
            'PopUpReminder' => $PopUpReminder,
            'Notes' => $Notes,
            'AccountType' => $AccountType,
            /*'LastActivityDate' => date("Y-m-d",strtotime($LastActivityDate)),
            'LastServiceDate' => date("Y-m-d",strtotime($LastServiceDate)),*/
            'PrimaryContact' => !empty($PrimaryContact)?$PrimaryContact:NULL,
            'IsActive' => $IsActive
        );       
       
        $result = $this->db->update('Account', $data, array('AccountID' => $AccountID)); 

        API_Model::checkQuery($result); 

        API_Model::updateCreatedModifiedByAndDt($UserID,'Account', array('AccountID' => $AccountID));

        API_Model::updateCustomFieldData('Account', $AccountID, $OrganizationID);
    
        return $result;
    }
}


?>
