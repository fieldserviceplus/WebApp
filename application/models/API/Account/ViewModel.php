<?php
require_once(APPPATH.'models/API/API_Model.php');

class ViewModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function GetViews($UserID, $OrganizationID)
    { 
        $AccountDefaultView = array(
                array('AccountViewID' => 'MyAccounts', 'AccountViewName' => 'My Accounts'),
                array('AccountViewID' => 'AllAccounts', 'AccountViewName' => 'All Accounts'),
                array('AccountViewID' => 'NewAccountsThisWeek', 'AccountViewName' => 'New Accounts This Week')
            );

        $query = $this->db->select('AccountViewID, AccountViewName')
        ->from('AccountView')
        ->where("OrganizationID = '".$OrganizationID."' AND ((UserID = '".$UserID."' AND RestrictVisibility = 'VisibleOnlyToMe') OR (RestrictVisibility = 'VisibleToEveryone'))")
        ->order_by('AccountViewName', 'asc')
        ->get();   

        API_Model::checkQuery($query); 
        
        return array_merge($AccountDefaultView, $query->result());
    }

    function GetDefaultAccountData($OrganizationID, $UserID, $AccountViewID, $FilterData = array())
    {
         if($AccountViewID == 'MyAccounts'){

                $query = $this->db->select('a.AccountID, CONCAT(ast.FirstName, " ", ast.LastName) as AssignedTo, a.AccountName, at.AccountType, a.PhoneNo, a.BillingCity as CityName, a.BillingState as StateName');
                $this->db->from('Account a');
                $this->db->join('User ast','ast.UserID = a.AssignedTo', 'left');
                $this->db->join('User pt','pt.UserID = a.PreferredTechnician', 'left');
                $this->db->join('User cr','cr.UserID = a.CreatedBy', 'left');
                $this->db->join('User lmb','lmb.UserID = a.LastModifiedBy', 'left');
                $this->db->join('Contact pc','pc.ContactID = a.PrimaryContact', 'left');
                $this->db->join('AccountType at','at.AccountTypeID = a.AccountType', 'left');
                $this->db->where(array('a.OrganizationID' => $OrganizationID, 'a.AssignedTo' => $UserID,'a.IsActive' => 1, 'a.IsDeleted' => 0));
                if(!empty($FilterData)){
                    foreach ($FilterData['FilterField'] as $key => $value) {
                    
                        $WhereField = "";
                        if($FilterData['FilterField'][$key] == 'AssignedTo'){
                            $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                        } else if($FilterData['FilterField'][$key] == 'PreferredTechnician') {
                            $WhereField = "CONCAT(pt.FirstName, ' ', pt.LastName)";
                        } else if($FilterData['FilterField'][$key] == 'CreatedBy') {
                            $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                        } else if($FilterData['FilterField'][$key] == 'LastModifiedBy') {
                            $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                        } else if($FilterData['FilterField'][$key] == 'PrimaryContact') {
                            $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                        } else if($FilterData['FilterField'][$key] == 'AccountType') {
                            $WhereField = "at.AccountType";
                        } else if($FilterData['FilterField'][$key] == 'LastActivityDate'){
                             $WhereField = "DATE_FORMAT(a.LastActivityDate, '".RES_DATE."')";
                        } else if($FilterData['FilterField'][$key] == 'LastServiceDate'){
                             $WhereField = "DATE_FORMAT(a.LastServiceDate, '".RES_DATE."')";
                        } else {
                            $WhereField = "a.".$FilterData['FilterField'][$key];
                        }

                        if($FilterData['FilterCondition'][$key] == 'Equals'){
                            $this->db->where($WhereField.' =',  $FilterData['FilterValue'][$key]);  
                        } else if($FilterData['FilterCondition'][$key] == 'NotEqualTo'){
                            $this->db->where($WhereField.' !=', $FilterData['FilterValue'][$key]);   
                        } else if($FilterData['FilterCondition'][$key] == 'LessThan'){
                            $this->db->where($WhereField.' <', $FilterData['FilterValue'][$key]);   
                        } else if($FilterData['FilterCondition'][$key] == 'GreaterThan'){
                            $this->db->where($WhereField.' >', $FilterData['FilterValue'][$key]);   
                        } else if($FilterData['FilterCondition'][$key] == 'LessOREqualTo'){
                            $this->db->where($WhereField.' <=', $FilterData['FilterValue'][$key]);   
                        } else if($FilterData['FilterCondition'][$key] == 'GreaterOREqualTo'){
                            $this->db->where($WhereField.' >=', $FilterData['FilterValue'][$key]);   
                        } else if($FilterData['FilterCondition'][$key] == 'Contains'){
                            $this->db->like($WhereField, $FilterData['FilterValue'][$key]);   
                        } else if($FilterData['FilterCondition'][$key] == 'StartsWith'){ 
                            $this->db->like($WhereField, $FilterData['FilterValue'][$key], 'after');   
                        } else if($FilterData['FilterCondition'][$key] == 'DoesNotContain'){
                            $this->db->not_like($WhereField, $FilterData['FilterValue'][$key]);   
                        } 
                    } 
                }
                if(isset($FilterData['SortByField']) && isset($FilterData['SortByValue']) && !empty($FilterData['SortByField']) && !empty($FilterData['SortByValue']) && ($FilterData['SortByValue'] == 'asc' || $FilterData['SortByValue'] == 'desc')){ 
                    $this->db->order_by($FilterData['SortByField'], $FilterData['SortByValue']);
                }
                $query = $this->db->get(); 
                //echo $this->db->last_query(); exit;
 
            } else if($AccountViewID == 'AllAccounts'){

                $query = $this->db->select('a.AccountID, CONCAT(ast.FirstName, " ", ast.LastName) as AssignedTo, a.AccountName, at.AccountType, a.PhoneNo, a.BillingCity as CityName, a.BillingState as StateName');
                $this->db->from('Account a');
                $this->db->join('User ast','ast.UserID = a.AssignedTo', 'left');
                $this->db->join('User pt','pt.UserID = a.PreferredTechnician', 'left');
                $this->db->join('User cr','cr.UserID = a.CreatedBy', 'left');
                $this->db->join('User lmb','lmb.UserID = a.LastModifiedBy', 'left');
                $this->db->join('Contact pc','pc.ContactID = a.PrimaryContact', 'left');
                $this->db->join('AccountType at','at.AccountTypeID = a.AccountType', 'left');
                $this->db->where(array('a.OrganizationID' => $OrganizationID, 'a.IsActive' => 1, 'a.IsDeleted' => 0));

                if(!empty($FilterData)){

                    foreach ($FilterData['FilterField'] as $key => $value) {
                    
                        $WhereField = "";
                        if($FilterData['FilterField'][$key] == 'AssignedTo'){
                            $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                        } else if($FilterData['FilterField'][$key] == 'PreferredTechnician') {
                            $WhereField = "CONCAT(pt.FirstName, ' ', pt.LastName)";
                        } else if($FilterData['FilterField'][$key] == 'CreatedBy') {
                            $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                        } else if($FilterData['FilterField'][$key] == 'LastModifiedBy') {
                            $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                        } else if($FilterData['FilterField'][$key] == 'PrimaryContact') {
                            $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                        } else if($FilterData['FilterField'][$key] == 'AccountType') {
                            $WhereField = "at.AccountType";
                        } else if($FilterData['FilterField'][$key] == 'LastActivityDate'){
                             $WhereField = "DATE_FORMAT(a.LastActivityDate, '".RES_DATE."')";
                        } else if($FilterData['FilterField'][$key] == 'LastServiceDate'){
                             $WhereField = "DATE_FORMAT(a.LastServiceDate, '".RES_DATE."')";
                        } else {
                            $WhereField = "a.".$FilterData['FilterField'][$key];
                        }

                        if($FilterData['FilterCondition'][$key] == 'Equals'){
                            $this->db->where($WhereField.' =',  $FilterData['FilterValue'][$key]);  
                        } else if($FilterData['FilterCondition'][$key] == 'NotEqualTo'){
                            $this->db->where($WhereField.' !=', $FilterData['FilterValue'][$key]);   
                        } else if($FilterData['FilterCondition'][$key] == 'LessThan'){
                            $this->db->where($WhereField.' <', $FilterData['FilterValue'][$key]);   
                        } else if($FilterData['FilterCondition'][$key] == 'GreaterThan'){
                            $this->db->where($WhereField.' >', $FilterData['FilterValue'][$key]);   
                        } else if($FilterData['FilterCondition'][$key] == 'LessOREqualTo'){
                            $this->db->where($WhereField.' <=', $FilterData['FilterValue'][$key]);   
                        } else if($FilterData['FilterCondition'][$key] == 'GreaterOREqualTo'){
                            $this->db->where($WhereField.' >=', $FilterData['FilterValue'][$key]);   
                        } else if($FilterData['FilterCondition'][$key] == 'Contains'){
                            $this->db->like($WhereField, $FilterData['FilterValue'][$key]);   
                        } else if($FilterData['FilterCondition'][$key] == 'StartsWith'){ 
                            $this->db->like($WhereField, $FilterData['FilterValue'][$key], 'after');   
                        } else if($FilterData['FilterCondition'][$key] == 'DoesNotContain'){
                            $this->db->not_like($WhereField, $FilterData['FilterValue'][$key]);   
                        } 
                    }
                }  
                if(isset($FilterData['SortByField']) && isset($FilterData['SortByValue']) && !empty($FilterData['SortByField']) && !empty($FilterData['SortByValue']) && ($FilterData['SortByValue'] == 'asc' || $FilterData['SortByValue'] == 'desc')){ 
                    $this->db->order_by($FilterData['SortByField'], $FilterData['SortByValue']);
                }
                $query = $this->db->get(); 

            } else if($AccountViewID == 'NewAccountsThisWeek'){

                /*$query = $this->db->select('a.AccountID, CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName, a.AccountName, at.AccountType, a.PhoneNo, a.BillingCity as CityName, a.BillingState as StateName');
                $this->db->from('Account a');
                $this->db->join('User ast','ast.UserID = a.AssignedTo', 'left');
                $this->db->join('User pt','pt.UserID = a.PreferredTechnician', 'left');
                $this->db->join('User cr','cr.UserID = a.CreatedBy', 'left');
                $this->db->join('User lmb','lmb.UserID = a.LastModifiedBy', 'left');
                $this->db->join('Contact pc','pc.ContactID = a.PrimaryContact', 'left');
                $this->db->join('AccountType at','at.AccountTypeID = a.AccountType', 'left');
                $this->db->where(array('a.IsActive' => 1, 'a.IsDeleted' => 0));*/
                $WhereString = "";
                if(!empty($FilterData)){
                    foreach ($FilterData['FilterField'] as $key => $value) {
                    
                        $WhereField = "";
                        if($FilterData['FilterField'][$key] == 'AssignedTo'){
                            $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                        } else if($FilterData['FilterField'][$key] == 'PreferredTechnician') {
                            $WhereField = "CONCAT(pt.FirstName, ' ', pt.LastName)";
                        } else if($FilterData['FilterField'][$key] == 'CreatedBy') {
                            $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                        } else if($FilterData['FilterField'][$key] == 'LastModifiedBy') {
                            $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                        } else if($FilterData['FilterField'][$key] == 'PrimaryContact') {
                            $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                        } else if($FilterData['FilterField'][$key] == 'AccountType') {
                            $WhereField = "at.AccountType";
                        } else if($FilterData['FilterField'][$key] == 'LastActivityDate'){
                             $WhereField = "DATE_FORMAT(a.LastActivityDate, '".RES_DATE."')";
                        } else if($FilterData['FilterField'][$key] == 'LastServiceDate'){
                             $WhereField = "DATE_FORMAT(a.LastServiceDate, '".RES_DATE."')";
                        } else {
                            $WhereField = "a.".$FilterData['FilterField'][$key];
                        }
                        $WhereString = "";
                        if($FilterData['FilterCondition'][$key] == 'Equals'){
                             $WhereString .= "AND $WhereField = '".$FilterData['FilterValue'][$key]."'";
                        } else if($FilterData['FilterCondition'][$key] == 'NotEqualTo'){
                            $this->db->where($WhereField.' !=', $FilterData['FilterValue'][$key]);   
                        } else if($FilterData['FilterCondition'][$key] == 'LessThan'){
                            $this->db->where($WhereField.' <', $FilterData['FilterValue'][$key]);   
                        } else if($FilterData['FilterCondition'][$key] == 'GreaterThan'){
                            $this->db->where($WhereField.' >', $FilterData['FilterValue'][$key]);   
                        } else if($FilterData['FilterCondition'][$key] == 'LessOREqualTo'){
                            $this->db->where($WhereField.' <=', $FilterData['FilterValue'][$key]);   
                        } else if($FilterData['FilterCondition'][$key] == 'GreaterOREqualTo'){
                            $this->db->where($WhereField.' >=', $FilterData['FilterValue'][$key]);   
                        } else if($FilterData['FilterCondition'][$key] == 'Contains'){
                            $WhereString .= "AND $WhereField LIKE '%".$FilterData['FilterValue'][$key]."%'";
                        } else if($FilterData['FilterCondition'][$key] == 'StartsWith'){ 
                            $WhereString .= "AND $WhereField LIKE '".$FilterData['FilterValue'][$key]."%'"; 
                        } else if($FilterData['FilterCondition'][$key] == 'DoesNotContain'){
                            $WhereString .= "AND $WhereField NOT LIKE '%".$FilterData['FilterValue'][$key]."%'";  
                        } 
                    }
                }
                $OrderByString = "";
                if(isset($FilterData['SortByField']) && isset($FilterData['SortByValue']) && !empty($FilterData['SortByField']) && !empty($FilterData['SortByValue']) && ($FilterData['SortByValue'] == 'asc' || $FilterData['SortByValue'] == 'desc')){ 
                    $OrderByString .= "ORDER BY ".$FilterData['SortByField']." ".$FilterData['SortByValue']."";
                }
               
                $query = $this->db->query("SELECT a.AccountID, a.AccountName, a.BillingCity as CityName, a.BillingState as StateName, at.AccountType, a.PhoneNo, CONCAT(ast.FirstName, ' ', ast.LastName) as AssignedTo 
                    FROM Account a 
                    LEFT JOIN User ast ON ast.UserID = a.AssignedTo
                    LEFT JOIN User pt ON pt.UserID = a.PreferredTechnician
                    LEFT JOIN User cr ON cr.UserID = a.CreatedBy
                    LEFT JOIN User lmb ON lmb.UserID = a.LastModifiedBy
                    LEFT JOIN Contact pc ON pc.ContactID = a.PrimaryContact
                    LEFT JOIN AccountType at ON at.AccountTypeID = a.AccountType 
                    WHERE (YEARWEEK(a.CreatedDate, 1) = YEARWEEK(CURDATE(), 1)) AND a.IsActive = 1 AND a.IsDeleted = 0 AND a.OrganizationID = '".$OrganizationID."' $WhereString $OrderByString");
                //echo $this->db->last_query(); exit;
            } 
     
            API_Model::checkQuery($query); 

            return $query->result_array();
    }

    function ViewAccountList($OrganizationID, $UserID, $AccountViewID)
    {  
        if($AccountViewID == 'MyAccounts' || $AccountViewID == 'AllAccounts' || $AccountViewID == 'NewAccountsThisWeek'){

            return $this->GetDefaultAccountData($OrganizationID, $UserID, $AccountViewID);

        } else {

            $data = array();
            $query = $this->db->get_where('AccountView', array('AccountViewID' => $AccountViewID)); 
            $AccountViewData = $query->row();

            if($query->num_rows() > 0){
                $query = $this->db->get_where('AccountFilter', array('AccountViewID' => $AccountViewID)); 
                $AccountFilterData = $query->result_array();

                $ColName = "a.AccountID, ";
                if(!empty($AccountViewData->SpecifyFieldsDisplay)){
                    $SpecifyFieldsDisplays = explode(",", $AccountViewData->SpecifyFieldsDisplay);
                    foreach ($SpecifyFieldsDisplays as $key => $value) {
                        if($value == 'AssignedTo'){
                            $ColName .= "CONCAT(ast.FirstName, ' ', ast.LastName) as AssignedTo";
                        } else if($value == 'AccountType'){
                            $ColName .= "at.AccountType";
                        } else if($value == 'PreferredTechnician'){
                            $ColName .= "CONCAT(pt.FirstName, ' ', pt.LastName) as PreferredTechnician";
                        } else if($value == 'PrimaryContact'){
                            $ColName .= "CONCAT(pc.FirstName, ' ', pc.LastName) as PrimaryContact";
                        } else if($value == 'CreatedBy'){
                            $ColName .= "CONCAT(cr.FirstName, ' ', cr.LastName) as CreatedBy";
                        } else if($value == 'LastModifiedBy'){
                            $ColName .= "CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedBy";
                        } else if($value == 'LastActivityDate'){
                            $ColName .= "DATE_FORMAT(a.LastActivityDate, '".RES_DATE."') as LastActivityDate";
                        } else if($value == 'LastServiceDate'){
                            $ColName .= "DATE_FORMAT(a.LastServiceDate, '".RES_DATE."') as LastServiceDate";
                        }  else {
                            $ColName .= "a.".$value;
                        }

                        if($key != count($SpecifyFieldsDisplays)){
                            $ColName .= ", ";
                        }
                    }
                }
                      
                /* Filter Account Data Start */
                $this->db->select($ColName);
                $this->db->from('Account a');
                $this->db->join('User ast','ast.UserID = a.AssignedTo', 'left');
                $this->db->join('User pt','pt.UserID = a.PreferredTechnician', 'left');
                $this->db->join('User cr','cr.UserID = a.CreatedBy', 'left');
                $this->db->join('User lmb','lmb.UserID = a.LastModifiedBy', 'left');
                $this->db->join('Contact pc','pc.ContactID = a.PrimaryContact', 'left');
                $this->db->join('AccountType at','at.AccountTypeID = a.AccountType', 'left');
                $this->db->where(array('a.OrganizationID' => $OrganizationID, 'a.IsActive' => 1, 'a.IsDeleted' => 0));

                foreach ($AccountFilterData as $key => $value) {

                    $WhereField = "";
                    if($value['FilterField'] == 'AssignedTo'){
                        $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                    } else if($value['FilterField'] == 'PreferredTechnician') {
                        $WhereField = "CONCAT(pt.FirstName, ' ', pt.LastName)";
                    } else if($value['FilterField'] == 'CreatedBy') {
                        $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                    } else if($value['FilterField'] == 'LastModifiedBy') {
                        $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                    } else if($value['FilterField'] == 'PrimaryContact') {
                        $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                    } else if($value['FilterField'] == 'AccountType') {
                        $WhereField = "at.AccountType";
                    } else if($value['FilterField'] == 'LastActivityDate'){
                         $WhereField = "DATE_FORMAT(a.LastActivityDate, '".RES_DATE."')";
                    } else if($value['FilterField'] == 'LastServiceDate'){
                         $WhereField = "DATE_FORMAT(a.LastServiceDate, '".RES_DATE."')";
                    } else {
                        $WhereField = "a.".$value['FilterField'];
                    }

                    if($value['FilterCondition'] == 'Equals'){
                        $this->db->where($WhereField.' =',  $value['FilterValue']);  
                    } else if($value['FilterCondition'] == 'NotEqualTo'){
                        $this->db->where($WhereField.' !=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'LessThan'){
                        $this->db->where($WhereField.' <', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'GreaterThan'){
                        $this->db->where($WhereField.' >', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'LessOREqualTo'){
                        $this->db->where($WhereField.' <=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'GreaterOREqualTo'){
                        $this->db->where($WhereField.' >=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'Contains'){
                        $this->db->like($WhereField, $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'StartsWith'){ 
                        $this->db->like($WhereField, $value['FilterValue'], 'after');   
                    } else if($value['FilterCondition'] == 'DoesNotContain'){
                        $this->db->not_like($WhereField, $value['FilterValue']);   
                    } 
                }
                if(!empty($AccountViewData->SortByField) && !empty($AccountViewData->SortByValue)){
                    $this->db->order_by($AccountViewData->SortByField, $AccountViewData->SortByValue);
                }
                $query = $this->db->get();  

                API_Model::checkQuery($query);

                $data = $query->result_array();
            } 

            return $data;

        }
    }

    function AccountFilter($OrganizationID, $UserID, $AccountViewID, $FilterFields, $FilterConditions, $FilterValues, $SortByField, $SortByValue)
    { 

        $FilterData = array(
            'FilterField' => $FilterFields,
            'FilterCondition' => $FilterConditions,
            'FilterValue' => $FilterValues,
            'SortByField' => $SortByField,
            'SortByValue' => $SortByValue,
        );
     
        return $this->GetDefaultAccountData($OrganizationID, $UserID, $AccountViewID, $FilterData);
    }

    function GetAccountViewFields($UserID, $OrganizationID)
    {
        $query = $this->db->select('FieldName')
        ->from('AccountViewFields')
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }
}

?>
