<?php
require_once(APPPATH.'models/API/API_Model.php');

class DataModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function GetAccountType($UserID, $OrganizationID)
    { 
        $query = $this->db->select('AccountTypeID, AccountType')
        ->from('AccountType')
        ->where(array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetAccounts($UserID, $OrganizationID)
    { 
        $query = $this->db->select('AccountID, AccountName')
        ->from('Account')
        ->where(array('OrganizationID' => $OrganizationID, 'IsActive' => 1, 'IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }
}

?>
