<?php
require_once(APPPATH.'models/API/API_Model.php');

class DataModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function GetSchedule($UserID, $OrganizationID)
    { 
        $FinalData = array();

        $this->db->select('e.EventID, e.Subject, e.EventStartDate as StartDate, e.EventEndDate as EndDate, e.EventEndDate as Time, es.EventStatus as Status, ep.Priority, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, e.City, e.State, e.RelatedTo, e.What');
        $this->db->from('Event e');
        $this->db->join('EventType et','et.EventTypeID = e.EventType', 'left');
        $this->db->join('EventStatus es','es.EventStatusID = e.EventStatus', 'left');
        $this->db->join('EventPriority ep','ep.EventPriorityID = e.EventPriority', 'left');
        $this->db->join('User at','at.UserID = e.AssignedTo', 'left');
        $this->db->where("('".date("Y-m-d H:i:s")."' BETWEEN e.EventStartDate AND e.EventEndDate)");
        $this->db->where(array('e.AssignedTo' => $UserID,'e.IsDeleted' => 0, 'e.OrganizationID' => $OrganizationID));
        $EventQuery =  $this->db->get();   

        API_Model::checkQuery($EventQuery); 

        $OpenActivities = $EventQuery->result_array();

         /* Fetch data from Related Object */
        $RelatedDataArr = API_Model::getRelatedData();

        foreach ($OpenActivities as $key => $OpenActivity) {

            $ObjRelatedData = $RelatedDataArr[$OpenActivity['RelatedTo']][$OpenActivity['What']];

            $sddata['EventID'] = $OpenActivity['EventID'];
            $sddata['Subject'] = $OpenActivity['Subject'];
            $sddata['Address'] = $OpenActivity['City'].', '.$OpenActivity['State'];
            if(date("H:i:s",strtotime($OpenActivity['StartDate'])) == '00:00:00' && date("H:i:s",strtotime($OpenActivity['EndDate'])) == '00:00:00'){
                $sddata['Time'] = "All-Day";
            } else {
                $sddata['Time'] = date("h:i A",strtotime($OpenActivity['StartDate'])).'-'.date("h:i A",strtotime($OpenActivity['EndDate']));
            }
            $sddata['RelatedTo'] = $OpenActivity['RelatedTo'];
            $sddata['RelatedObjID'] = $ObjRelatedData->ID;
            $sddata['RelatedObjNo'] = $ObjRelatedData->No;
            $FinalData[] = $sddata;
        }
        return $FinalData;
    }

    function GetTasks($UserID, $OrganizationID, $Filter)
    { 
        $FinalData = array();

        $this->db->select('t.TaskID, t.Subject, t.DueDate as Time, DATE_FORMAT(t.DueDate, "'.RES_DATE.'") as Date, ts.TaskStatus as Status, tp.Priority, t.Subject, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, t.City, t.State, t.RelatedTo, t.What');
        $this->db->from('Task t');
        $this->db->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left');
        $this->db->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left');
        $this->db->join('User at','at.UserID = t.AssignedTo', 'left');
        $this->db->where(array('ts.IsClosed' => 0, 't.IsDeleted' => 0, 't.OrganizationID' => $OrganizationID));
        if($Filter == 'Today')
            $this->db->where("(DATE(t.DueDate) = '".date("Y-m-d")."')");
        else if($Filter == 'TodayOverdue')
            $this->db->where("(DATE(t.DueDate) <= '".date("Y-m-d")."')");
        else if($Filter == 'Overdue')
            $this->db->where("(DATE(t.DueDate) < '".date("Y-m-d")."')");
        $this->db->where('t.AssignedTo', $UserID);
        $this->db->order_by("t.DueDate", "desc");
        $this->db->limit(10);
        $TaskQuery = $this->db->get(); 

        $OpenActivities = $TaskQuery->result_array();

         /* Fetch data from Related Object */
        $RelatedDataArr = API_Model::getRelatedData();

        foreach ($OpenActivities as $key => $OpenActivity) {

            $ObjRelatedData = $RelatedDataArr[$OpenActivity['RelatedTo']][$OpenActivity['What']];

            $sddata['TaskID'] = $OpenActivity['TaskID'];
            $sddata['Subject'] = $OpenActivity['Subject'];
            $sddata['Date'] = $OpenActivity['Date'];
            $sddata['RelatedTo'] = $OpenActivity['RelatedTo'];
            $sddata['RelatedObjID'] = $ObjRelatedData->ID;
            $sddata['RelatedObjNo'] = $ObjRelatedData->No;
            $FinalData[] = $sddata;
        }
        return $FinalData;
    }

    function GetRecents($UserID, $OrganizationID)
    { 
        $FinalData = array();

        $this->db->select('AccountID as ID, "Account" as Object, CONCAT(a.AccountNo, " ", a.AccountName) as Name, , a.CreatedDate as Date');
        $this->db->from('Account a');
        $this->db->where(array('a.CreatedBy' => $UserID, 'a.IsDeleted' => 0, 'a.OrganizationID' => $OrganizationID));
        $this->db->where("(DATE(a.CreatedDate) BETWEEN '".date('Y-m-d', strtotime("-5 days"))."' AND '".date("Y-m-d")."')");
        $this->db->order_by("a.CreatedDate", "desc");
        $this->db->limit(10);
        $AcRecents = $this->db->get(); 

        $this->db->select('WorkOrderID as ID, "WorkOrder" as Object, CONCAT(wo.WorkOrderNo, " ", wo.Subject) as Name, wo.CreatedDate as Date');
        $this->db->from('WorkOrder wo');
        $this->db->where(array('wo.CreatedBy' => $UserID, 'wo.IsDeleted' => 0, 'wo.OrganizationID' => $OrganizationID));
        $this->db->where("(DATE(wo.CreatedDate) BETWEEN '".date('Y-m-d', strtotime("-5 days"))."' AND '".date("Y-m-d")."')");
        $this->db->order_by("wo.CreatedDate", "desc");
        $this->db->limit(10);
        $WORecents = $this->db->get();

        $this->db->select('ContactID as ID, "Contact" as Object, CONCAT(c.ContactNo, " ", c.FirstName, " ", c.LastName) as Name, c.CreatedDate as Date');
        $this->db->from('Contact c');
        $this->db->where(array('c.CreatedBy' => $UserID, 'c.IsDeleted' => 0, 'c.OrganizationID' => $OrganizationID));
        $this->db->where("(DATE(c.CreatedDate) BETWEEN '".date('Y-m-d', strtotime("-5 days"))."' AND '".date("Y-m-d")."')");
        $this->db->order_by("c.CreatedDate", "desc");
        $this->db->limit(10);
        $CnRecents = $this->db->get();
        
        $this->db->select('EstimateID as ID, "Estimate" as Object, CONCAT(e.EstimateNo, " ", e.EstimateName) as Name, e.CreatedDate as Date');
        $this->db->from('Estimate e');
        $this->db->where(array('e.CreatedBy' => $UserID, 'e.IsDeleted' => 0, 'e.OrganizationID' => $OrganizationID));
        $this->db->where("(DATE(e.CreatedDate) BETWEEN '".date('Y-m-d', strtotime("-5 days"))."' AND '".date("Y-m-d")."')");
        $this->db->order_by("e.CreatedDate", "desc");
        $this->db->limit(10);
        $EtRecents = $this->db->get();

        $this->db->select('InvoiceID as ID, "Invoice" as Object, i.InvoiceNo as Name, i.CreatedDate as Date');
        $this->db->from('Invoice i');
        $this->db->where(array('i.CreatedBy' => $UserID, 'i.IsDeleted' => 0, 'i.OrganizationID' => $OrganizationID));
        $this->db->where("(DATE(i.CreatedDate) BETWEEN '".date('Y-m-d', strtotime("-5 days"))."' AND '".date("Y-m-d")."')");
        $this->db->order_by("i.CreatedDate", "desc");
        $this->db->limit(10);
        $InRecents = $this->db->get();

        $this->db->select('FileID as ID, "File" as Object, f.Subject as Name, f.CreatedDate as Date');
        $this->db->from('File f');
        $this->db->where(array('f.CreatedBy' => $UserID, 'f.IsDeleted' => 0, 'f.OrganizationID' => $OrganizationID));
        $this->db->where("(DATE(f.CreatedDate) BETWEEN '".date('Y-m-d', strtotime("-5 days"))."' AND '".date("Y-m-d")."')");
        $this->db->order_by("f.CreatedDate", "desc");
        $this->db->limit(10);
        $FlRecents = $this->db->get();

        $this->db->select('EventID as ID, "Event" as Object, e.Subject as Name, e.CreatedDate as Date');
        $this->db->from('Event e');
        $this->db->where(array('e.CreatedBy' => $UserID, 'e.IsDeleted' => 0, 'e.OrganizationID' => $OrganizationID));
        $this->db->where("(DATE(e.CreatedDate) BETWEEN '".date('Y-m-d', strtotime("-5 days"))."' AND '".date("Y-m-d")."')");
        $this->db->order_by("e.CreatedDate", "desc");
        $this->db->limit(10);
        $EvRecents = $this->db->get();

        $this->db->select('TaskID as ID, "Task" as Object, t.Subject as Name, t.CreatedDate as Date');
        $this->db->from('Task t');
        $this->db->where(array('t.CreatedBy' => $UserID, 't.IsDeleted' => 0, 't.OrganizationID' => $OrganizationID));
        $this->db->where("(DATE(t.CreatedDate) BETWEEN '".date('Y-m-d', strtotime("-5 days"))."' AND '".date("Y-m-d")."')");
        $this->db->order_by("t.CreatedDate", "desc");
        $this->db->limit(10);
        $TsRecents = $this->db->get();

        $Recents = array_merge($AcRecents->result_array(), $WORecents->result_array(), $CnRecents->result_array(), $EtRecents->result_array(), $InRecents->result_array(), $FlRecents->result_array(), $EvRecents->result_array(), $TsRecents->result_array()); 

        usort($Recents, array($this,'date_compare_desc'));

        $result = array();
        foreach ($Recents as $key => $Recent) {
            if($key >= 10)
                break;

            $result[] = $Recent;
            
        }
        return $result;
    }

    function date_compare_desc($a, $b)
    {
        $t1 = strtotime($a['Date']);
        $t2 = strtotime($b['Date']);
        return $t2 - $t1;
    } 
}

?>
