<?php
require_once(APPPATH.'models/API/API_Model.php');

class ViewModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
  
    function GetViews($UserID, $OrganizationID)
    { 
        $LocationDefaultView = array(
                array('LocationViewID' => 'MyLocations', 'LocationViewName' => 'My Locations'),
                array('LocationViewID' => 'AllLocations', 'LocationViewName' => 'All Locations'),
                array('LocationViewID' => 'LocationsCreatedThisWeek', 'LocationViewName' => 'Locations Created This Week')
            );

        $query = $this->db->select('LocationViewID, LocationViewName')
        ->from('LocationView')
        ->where(array('OrganizationID' => $OrganizationID, 'UserID' => $UserID, 'RestrictVisibility'=>'VisibleOnlyToMe'))
        ->or_where(array('RestrictVisibility' => 'VisibleToEveryone'))
        ->order_by('LocationViewName', 'asc')
        ->get();   

        API_Model::checkQuery($query); 
        
        return array_merge($LocationDefaultView, $query->result());
    }

    function GetDefaultLocationData($UserID, $OrganizationID, $LocationViewID, $FilterData = array())
    {
            $WhereString = "";
            if($LocationViewID == 'MyLocations'){

                $WhereString .= "l.Owner = $UserID AND l.IsDeleted=0 AND l.OrganizationID = '".$OrganizationID."'";
            }
            else if($LocationViewID == 'AllLocations'){

                $WhereString .= "l.IsDeleted=0 AND l.OrganizationID = '".$OrganizationID."'";

            } else if($LocationViewID == 'LocationsCreatedThisWeek'){

                $WhereString .= "YEARWEEK(l.CreatedDate, 1) = YEARWEEK(CURDATE(), 1) AND l.IsDeleted=0 AND l.OrganizationID = '".$OrganizationID."'";

            } else {
                 $WhereString .= "l.IsDeleted=0 AND l.OrganizationID = '".$OrganizationID."'";
            }

            if(!empty($FilterData)){
                foreach ($FilterData['FilterField'] as $key => $value) {

                    $WhereField = "";
                    if($FilterData['FilterField'][$key] == 'Owner'){
                        $WhereField = "CONCAT(ow.FirstName, ' ', ow.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'Account') {
                        $WhereField = "ac.AccountName";
                    } else if($FilterData['FilterField'][$key] == 'PreferredTechnician') {
                        $WhereField = "CONCAT(pt.FirstName, ' ', pt.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'OrganizationID') {
                        $WhereField = "o.CompanyName";
                    } else if($FilterData['FilterField'][$key] == 'LocationType') {
                        $WhereField = "lt.LocationType";
                    } else if($FilterData['FilterField'][$key] == 'ParentLocation') {
                        $WhereField = "pl.Name";
                    } else if($FilterData['FilterField'][$key] == 'CreatedBy') {
                        $WhereField = "CONCAT(cb.FirstName, ' ', cb.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'LastModifiedBy') {
                        $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'PrimaryContact') {
                        $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                    } else {
                        $WhereField = "l.".$FilterData['FilterField'][$key];
                    }

                    if($FilterData['FilterCondition'][$key] == 'Equals'){
                         $WhereString .= " AND $WhereField = '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'NotEqualTo'){
                        $WhereString .= " AND $WhereField != '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'LessThan'){
                        $WhereString .= " AND $WhereField < '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'GreaterThan'){
                        $WhereString .= " AND $WhereField > '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'LessOREqualTo'){
                        $WhereString .= " AND $WhereField <= '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'GreaterOREqualTo'){
                        $WhereString .= " AND $WhereField >= '".$FilterData['FilterValue'][$key]."'";
                    } else if($FilterData['FilterCondition'][$key] == 'Contains'){
                        $WhereString .= " AND $WhereField LIKE '%".$FilterData['FilterValue'][$key]."%'";
                    } else if($FilterData['FilterCondition'][$key] == 'StartsWith'){ 
                        $WhereString .= " AND $WhereField LIKE '".$FilterData['FilterValue'][$key]."%'"; 
                    } else if($FilterData['FilterCondition'][$key] == 'DoesNotContain'){
                        $WhereString .= " AND $WhereField NOT LIKE '%".$FilterData['FilterValue'][$key]."%'";  
                    } 
                }
            }
            $OrderByString = "";
            if(isset($FilterData['SortByField']) && isset($FilterData['SortByValue']) && !empty($FilterData['SortByField']) && !empty($FilterData['SortByValue']) && ($FilterData['SortByValue'] == 'asc' || $FilterData['SortByValue'] == 'desc')){ 
                $OrderByString .= "ORDER BY ".$FilterData['SortByField']." ".$FilterData['SortByValue']."";
            }
 
            $query = "SELECT l.LocationID, l.LocationNo, l.Name, lt.LocationType, ac.AccountName, IF(cnt.FirstName IS NOT NULL,CONCAT(cnt.FirstName, ' ', cnt.LastName), '') as PrimaryContactName, l.IsActive, IF(l.PreferredTechnician IS NOT NULL, CONCAT(pt.FirstName, ' ', pt.LastName), '') as PreferredTechnicianName, l.City, l.State, DATE_FORMAT(l.CreatedDate, '".RES_DATETIME."') as CreatedDate, CONCAT(cb.FirstName, ' ', cb.LastName) as CreatedBy
            FROM Location l
            LEFT JOIN User ow ON ow.UserID = l.Owner
            LEFT JOIN User cb ON cb.UserID = l.CreatedBy
            LEFT JOIN Account ac ON ac.AccountID = l.Account
            LEFT JOIN LocationType lt ON lt.LocationTypeID = l.LocationType
            LEFT JOIN Contact cnt ON cnt.ContactID = l.PrimaryContact
            LEFT JOIN Organization o ON o.OrganizationID = l.OrganizationID
            LEFT JOIN User pt ON pt.UserID = l.PreferredTechnician
            LEFT JOIN Location pl ON pl.LocationID = l.ParentLocation
            WHERE $WhereString $OrderByString"; 
            $query = $this->db->query($query);
     
            API_Model::checkQuery($query); 

            return $query->result_array();
    }

    function ViewLocationList($UserID, $LocationViewID, $OrganizationID)
    {    
        if($LocationViewID == 'MyLocations' 
            || $LocationViewID == 'AllLocations'  
            || $LocationViewID == 'LocationsCreatedThisWeek'
        ){

            return $this->GetDefaultLocationData($UserID, $OrganizationID, $LocationViewID);

        } else {

            $data = array();
            $query = $this->db->get_where('LocationView', array('OrganizationID' => $OrganizationID, 'LocationViewID' => $LocationViewID)); 
            $LocationViewData = $query->row();

            if($query->num_rows() > 0){
                $query = $this->db->get_where('LocationFilter', array('LocationViewID' => $LocationViewID)); 
                $LocationFilterData = $query->result_array();

                $ColName = "l.LocationID, ac.AccountID, ";
                if(!empty($LocationViewData->SpecifyFieldsDisplay)){
                    $SpecifyFieldsDisplays = explode(",", $LocationViewData->SpecifyFieldsDisplay);
                    foreach ($SpecifyFieldsDisplays as $key => $value) {
                        if($value == 'Owner'){
                            $ColName .= "CONCAT(ow.FirstName, ' ', ow.LastName) as Owner";
                        } else if($value == 'Account'){
                            $ColName .= "ac.AccountName as Account";
                        } else if($value == 'LocationType'){ 
                            $ColName .= "lt.LocationType";
                        } else if($value == 'OrganizationID'){
                            $ColName .= "o.CompanyName as OrganizationID";
                        } else if($value == 'ParentLocation'){
                            $ColName .= "pl.Name as ParentLocation";
                        } else if($value == 'PreferredTechnician'){
                            $ColName .= "CONCAT(pt.FirstName, ' ', pt.LastName) as PreferredTechnician";
                        } else if($value == 'PrimaryContact'){
                            $ColName .= "CONCAT(pc.FirstName, ' ', pc.LastName) as PrimaryContact";
                        } else if($value == 'CreatedBy'){
                            $ColName .= "CONCAT(cr.FirstName, ' ', cr.LastName) as CreatedBy";
                        } else if($value == 'LastModifiedBy'){
                            $ColName .= "CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedBy";
                        } else {
                            $ColName .= "l.".$value;
                        }

                        if($key != count($SpecifyFieldsDisplays)){
                            $ColName .= ", ";
                        }
                    }
                }
                      
                /* Filter Location Data Start */
                $this->db->select($ColName);
                $this->db->from('Location l');
                $this->db->join('User ow','ow.UserID = l.Owner', 'left');
                $this->db->join('Account ac','ac.AccountID = l.Account', 'left');
                $this->db->join('LocationType lt','lt.LocationTypeID = l.LocationType', 'left');
                $this->db->join('Organization o','o.OrganizationID = l.OrganizationID', 'left');
                $this->db->join('Location pl','pl.LocationID = l.ParentLocation', 'left');
                $this->db->join('User pt','pt.UserID = l.PreferredTechnician', 'left');
                $this->db->join('User cr','cr.UserID = l.CreatedBy', 'left');
                $this->db->join('User lmb','lmb.UserID = l.LastModifiedBy', 'left');
                $this->db->join('Contact pc','pc.ContactID = l.PrimaryContact', 'left');
                $this->db->where(array('l.OrganizationID' => $OrganizationID, 'l.IsDeleted' => 0));

                foreach ($LocationFilterData as $key => $value) {

                    $WhereField = "";
                    if($value['FilterField'] == 'Owner'){
                        $WhereField = "CONCAT(ow.FirstName, ' ', ow.LastName)";
                    } else if($value['FilterField'] == 'Account') {
                        $WhereField = "ac.AccountName";
                    } else if($value['FilterField'] == 'ParentLocation') {
                        $WhereField = "pl.Name";
                    } else if($value['FilterField'] == 'OrganizationID') {
                        $WhereField = "o.CompanyName";
                    } else if($value['FilterField'] == 'LocationType') {
                        $WhereField = "lt.LocationType";
                    } else if($value['FilterField'] == 'PreferredTechnician') {
                        $WhereField = "CONCAT(pt.FirstName, ' ', pt.LastName)";
                    } else if($value['FilterField'] == 'CreatedBy') {
                        $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                    } else if($value['FilterField'] == 'LastModifiedBy') {
                        $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                    } else if($value['FilterField'] == 'PrimaryContact') {
                        $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                    } else {
                        $WhereField = "l.".$value['FilterField'];
                    }

                    if($value['FilterCondition'] == 'Equals'){
                        $this->db->where($WhereField.' =',  $value['FilterValue']);  
                    } else if($value['FilterCondition'] == 'NotEqualTo'){
                        $this->db->where($WhereField.' !=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'LessThan'){
                        $this->db->where($WhereField.' <', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'GreaterThan'){
                        $this->db->where($WhereField.' >', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'LessOREqualTo'){
                        $this->db->where($WhereField.' <=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'GreaterOREqualTo'){
                        $this->db->where($WhereField.' >=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'Contains'){
                        $this->db->like($WhereField, $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'StartsWith'){ 
                        $this->db->like($WhereField, $value['FilterValue'], 'after');   
                    } else if($value['FilterCondition'] == 'DoesNotContain'){
                        $this->db->not_like($WhereField, $value['FilterValue']);   
                    } 
                } 
                $query = $this->db->get();  
                //echo  $this->db->last_query();exit;
                API_Model::checkQuery($query);

                $data = $query->result_array();
            } 

            return $data;

        }
    }

    function LocationFilter($UserID, $LocationViewID, $FilterFields, $FilterConditions, $FilterValues, $SortByField, $SortByValue, $OrganizationID)
    { 

        $FilterData = array(
            'FilterField' => $FilterFields,
            'FilterCondition' => $FilterConditions,
            'FilterValue' => $FilterValues,
            'SortByField' => $SortByField,
            'SortByValue' => $SortByValue,
        );
     
        return $this->GetDefaultLocationData($UserID, $OrganizationID, $LocationViewID, $FilterData);
    }

    function GetLocationViewFields()
    {
        $query = $this->db->select('FieldName')
        ->from('LocationViewFields')
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }
}

?>
