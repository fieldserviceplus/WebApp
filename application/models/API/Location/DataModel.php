<?php
require_once(APPPATH.'models/API/API_Model.php');

class DataModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }

    function GetLocationTypes($UserID, $OrganizationID)
    { 
        $query = $this->db->select('*')
        ->from('LocationType')
        ->where(array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetParentLocations($UserID, $OrganizationID)
    { 
        $query = $this->db->select('LocationID, Name')
        ->from('Location')
        ->where(array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0, 'IsActive' => 1))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }
}

?>
