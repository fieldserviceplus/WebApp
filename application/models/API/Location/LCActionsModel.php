<?php
require_once(APPPATH.'models/API/API_Model.php');

class LCActionsModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function CreateLocation() {

        extract($_POST);

        $data = array(
            'OrganizationID' => $OrganizationID,
            'Name' => $Name,
            'Account' => $Account,
            'Owner' => $Owner,
            'PrimaryContact' => isset($PrimaryContact)?$PrimaryContact:NULL,
            'PreferredTechnician' => isset($PreferredTechnician)?$PreferredTechnician:NULL,
            'ParentLocation' => isset($ParentLocation)?$ParentLocation:NULL,
            'Address' => $Address,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => (double)$Latitude,
            'Longitude' => (double)$Longitude,
            'LocationType' => $LocationType,
            'PostalCode' => $PostalCode,
            'Notes' => $Notes,
            'AccessNotes' => $AccessNotes,
            'IsActive' => isset($IsActive)?$IsActive:1,
        );   
        $query = $this->db->insert('Location', $data);  

        API_Model::checkQuery($query);  

        $LocationID = $this->db->insert_id();

        $LocationNo = API_Controller::genCode('LC',$LocationID);

        $query = $this->db->update('Location', array('LocationNo'=>$LocationNo), array('LocationID' => $LocationID));   

        API_Model::checkQuery($query);  

        API_Model::insertCreatedModifiedByAndDt($UserID,'Location', array('LocationID' => $LocationID));

        $ResData['LocationID'] = $LocationID;
        $ResData['LocationNo'] = $LocationNo;
        return $ResData;
    }

    function RecentLocations($UserID, $OrganizationID)
    {  
        $query = $this->db->select('l.LocationID, l.LocationNo as LC#, l.Name, lt.LocationType, a.AccountName, CONCAT(o.FirstName, " ", o.LastName) as OwnerName, l.IsActive, CONCAT(pt.FirstName, " ", pt.LastName) as PreferredTechnicianName, l.City, l.State')
        ->from('Location l')
        ->join('LocationType lt','lt.LocationTypeID = l.LocationType', 'left') 
        ->join('Account a','a.AccountID = l.Account', 'left') 
        ->join('User o','o.UserID = l.Owner', 'left') 
        ->join('User pt','pt.UserID = l.PreferredTechnician', 'left') 
        ->where(array('l.OrganizationID' => $OrganizationID, 'l.IsDeleted' => 0))
        ->order_by("l.LastModifiedDate", "desc")
        ->get();  

        API_Model::checkQuery($query);  
        
        return $query->result_array();
    }

    function LocationDetails($UserID, $LocationID, $OrganizationID)
    {  
        $query = $this->db->select('l.*, ac.AccountName, CONCAT(pc.FirstName, " ", pc.LastName) as PrimaryContactName, CONCAT(o.FirstName, " ", o.LastName) as OwnerName, lt.LocationType as LocationTypeName, CONCAT(pt.FirstName, " ", pt.LastName) as PreferredTechnicianName, pl.Name as ParentLocationName, DATE_FORMAT(l.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,DATE_FORMAT(l.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy')
        ->from('Location l')
        ->join('LocationType lt','lt.LocationTypeID = l.LocationType', 'left') 
        ->join('Account ac','ac.AccountID = l.Account', 'left') 
        ->join('User o','o.UserID = l.Owner', 'left') 
        ->join('User pt','pt.UserID = l.PreferredTechnician', 'left')
        ->join('Location pl','pl.LocationID = l.ParentLocation', 'left')
        ->join('Contact pc','pc.ContactID = l.PrimaryContact', 'left')
        ->join('User cb','cb.UserID = l.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = l.LastModifiedBy', 'left')
        ->where(array('l.LocationID' => $LocationID, 'l.OrganizationID' => $OrganizationID)) //, 'l.IsDeleted' => 0
        ->get();

        API_Model::checkQuery($query); 
        $LocationDetails = $query->row_array();

        return ($LocationDetails); 
    }

    /*function LocationRelatedList($UserID, $LocationID, $OrganizationID)
    { 
        $query = $this->db->select('Count(*) as EventCnt')
        ->get_where('Event', array('OrganizationID' => $OrganizationID, 'RelatedTo' => 'Location', 'What' => $LocationID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Event']['title'] = 'Events ('.$result->EventCnt.')';

        $query = $this->db->select('Count(*) as TaskCnt')
        ->get_where('Task', array('OrganizationID' => $OrganizationID, 'RelatedTo' => 'Location', 'What' => $LocationID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Task']['title'] = 'Tasks ('.$result->TaskCnt.')';
 
        $query = $this->db->select('Count(*) as FileCnt')
        ->get_where('File', array('OrganizationID' => $OrganizationID, 'RelatedTo' => 'Location', 'What' => $LocationID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['File']['title'] = 'Files ('.$result->FileCnt.')';

        $this->db->select('Count(*) as NoteCnt');
        $query = $this->db->get_where('Note n', array('n.OrganizationID' => $OrganizationID, 'n.RelatedTo' => 'Location', 'n.What' => $LocationID, 'n.IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Note']['title'] = 'Notes ('.$result->NoteCnt.')';

        return $data;
    }*/

    /*function LocationRelatedEvent($UserID, $LocationID, $OrganizationID)
    { 
        $query = $this->db->select('e.EventID, e.Subject, es.EventStatus, et.EventTypeName, DATE_FORMAT(e.EventStartDate, "'.RES_DATE.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATE.'") as EventEndDate, DATE_FORMAT(e.EventStartTime, "'.RES_TIME.'") as EventStartTime, DATE_FORMAT(e.EventEndTime, "'.RES_TIME.'") as EventEndTime')
        ->from('Event e') 
        ->join('User cb','cb.UserID = e.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = e.LastModifiedBy', 'left')
        ->join('EventStatus es','es.EventStatusID = e.EventStatus', 'left')
        ->join('EventType et','et.EventTypeID = e.EventType', 'left')
        ->where(array('e.What' => $LocationID, 'e.OrganizationID' => $OrganizationID, 'e.RelatedTo' => 'Location', 'e.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function LocationRelatedTask($UserID, $LocationID, $OrganizationID)
    {  
        $query = $this->db->select('t.TaskID, t.Subject, tt.TaskType, tp.Priority, ts.TaskStatus')
        ->from('Task t') 
        ->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left')
        ->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left')
        ->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left')
        ->where(array('t.What' => $LocationID, 't.OrganizationID' => $OrganizationID, 't.RelatedTo' => 'Location', 't.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function LocationRelatedNote($UserID, $LocationID, $OrganizationID)
    {  
        $this->db->select('n.NoteID, n.Subject, DATE_FORMAT(n.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, CONCAT(o.FirstName, " ", o.LastName) as OwnerName');
        $this->db->from('Note n'); 
        $this->db->join('User o','o.UserID = n.Owner', 'left');
        $this->db->where(array('n.OrganizationID' => $OrganizationID, 'n.What' => $LocationID,'n.RelatedTo' => 'Location', 'n.IsDeleted' => 0));
        $query = $this->db->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function LocationRelatedFile($UserID, $LocationID, $OrganizationID)
    {  
        $query = $this->db->select('f.FileID, f.FileName, f.Subject, f.ContentType')
        ->from('File f') 
        ->where(array('f.OrganizationID' => $OrganizationID, 'f.What' => $LocationID,'f.RelatedTo' => 'Location', 'f.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }*/

    function EditLocation() {
                
        extract($_POST);

        $data = array(
            'OrganizationID' => $OrganizationID,
            'Name' => $Name,
            'Account' => $Account,
            'Owner' => $Owner,
            'PrimaryContact' => isset($PrimaryContact)?$PrimaryContact:NULL,
            'PreferredTechnician' => isset($PreferredTechnician)?$PreferredTechnician:NULL,
            'ParentLocation' => isset($ParentLocation)?$ParentLocation:NULL,
            'Address' => $Address,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => (double)$Latitude,
            'Longitude' => (double)$Longitude,
            'LocationType' => $LocationType,
            'PostalCode' => $PostalCode,
            'Notes' => $Notes,
            'AccessNotes' => $AccessNotes,
            'IsActive' => isset($IsActive)?$IsActive:1,
        );   
        $query = $this->db->update('Location', $data, array('OrganizationID' => $OrganizationID, 'LocationID' => $LocationID));   

        API_Model::checkQuery($query);

        API_Model::updateCreatedModifiedByAndDt($UserID,'Location', array('LocationID' => $LocationID));
    
        return $query;
    }
}

?>
