<?php
require_once(APPPATH.'models/API/API_Model.php');

class CalendarViewModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function GetEventTaskByDates($UserID, $OrganizationID, $FromDate, $ToDate, $WorkOrderType, $WOStatus, $WOPriority, $AssignedTo, $ShowTasks)
    { 
        $FinalData = array(); 
        $TaskData = array(); 
        $WorkOrderIDs = array(); 

        if($ShowTasks)
        {
            $this->db->select('t.TaskID, t.Subject, t.DueDate as Time, t.DueDate as StartDate, t.DueDate as EndDate, ts.TaskStatus as Status, tp.Priority, t.Subject, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, t.City, t.State, t.RelatedTo, t.What');
            $this->db->from('Task t');
            $this->db->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left');
            $this->db->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left');
            $this->db->join('User at','at.UserID = t.AssignedTo', 'left');
            $this->db->where(array('ts.IsClosed' => 0, 't.IsDeleted' => 0, 't.OrganizationID' => $OrganizationID));
           /* $this->db->where('DATE(t.DueDate) >=', date("Y-m-d", strtotime($FromDate)));
            $this->db->where('DATE(t.DueDate) <=', date("Y-m-d", strtotime($ToDate)));*/
            $this->db->where("((DATE(t.DueDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."'))");
            if(isset($AssignedTo) && is_array($AssignedTo) && !empty($AssignedTo))
                $this->db->where_in('t.AssignedTo', $AssignedTo);
            else if(isset($AssignedTo) && !is_null($AssignedTo))
                $this->db->where_in('t.AssignedTo', '');
            $TaskQuery = $this->db->get(); 
            // echo $this->db->last_query(); exit;

            $TaskData = $TaskQuery->result_array();
        }  
       
        $this->db->select('WorkOrderID');
        $this->db->from('WorkOrder');
        if(isset($WorkOrderType) && !empty($WorkOrderType))
            $this->db->where_in('WorkOrderType', $WorkOrderType);
        else if(isset($WorkOrderType) && !is_null($WorkOrderType)) 
            $this->db->where_in('WorkOrderType', '');
        if(isset($WOStatus) && !empty($WOStatus))
            $this->db->where_in('WOStatus', $WOStatus);
        else if(isset($WOStatus) && !is_null($WOStatus)) 
            $this->db->where_in('WOStatus', '');
        if(isset($WOPriority) && !empty($WOPriority))
            $this->db->where_in('WOPriority', $WOPriority);
        else if(isset($WOPriority) && !is_null($WOPriority)) 
            $this->db->where_in('WOPriority', '');
        $this->db->where(array('IsDeleted' => 0, 'OrganizationID' => $OrganizationID));
        $WorkOrderQuery = $this->db->get(); 
        $WorkOrderRes = $WorkOrderQuery->result();
        foreach ($WorkOrderRes as $key => $value) {
           $WorkOrderIDs[] = $value->WorkOrderID; 
        }

        $this->db->select('e.EventID, e.Subject, e.EventStartDate as StartDate, e.EventEndDate as EndDate, e.EventEndDate as Time, es.EventStatus as Status, ep.Priority, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, e.City, e.State, e.RelatedTo, e.What');
        $this->db->from('Event e');
        $this->db->join('EventType et','et.EventTypeID = e.EventType', 'left');
        $this->db->join('EventStatus es','es.EventStatusID = e.EventStatus', 'left');
        $this->db->join('EventPriority ep','ep.EventPriorityID = e.EventPriority', 'left');
        $this->db->join('User at','at.UserID = e.AssignedTo', 'left');
        /*$this->db->where('DATE(e.EventStartDate) >=', date("Y-m-d", strtotime($FromDate)));
        $this->db->where('DATE(e.EventEndDate) >=', date("Y-m-d", strtotime($FromDate)));
        $this->db->where('DATE(e.EventStartDate) <=', date("Y-m-d", strtotime($ToDate)));
        $this->db->where('DATE(e.EventEndDate) <=', date("Y-m-d", strtotime($ToDate)));*/
        $this->db->where("((DATE(e.EventStartDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."') OR (DATE(e.EventEndDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."'))");
        
        $this->db->where("IF(e.RelatedTo = 'WorkOrder', IF(e.What IN ('".implode("','", $WorkOrderIDs)."'),1,0), 1)");

        if(isset($AssignedTo) && is_array($AssignedTo) && !empty($AssignedTo)) // && !empty($AssignedTo)
            $this->db->where_in('e.AssignedTo', $AssignedTo);
        else if(isset($AssignedTo) && !is_null($AssignedTo)) 
            $this->db->where_in('e.AssignedTo', '');
        $this->db->where(array('e.IsDeleted' => 0, 'e.OrganizationID' => $OrganizationID));
        $EventQuery =  $this->db->get();   
        //echo $this->db->last_query(); exit; 

        $OpenActivities = array_merge($TaskData, $EventQuery->result_array());

         /* Fetch data from Related Object */
        $RelatedDataArr = API_Model::getRelatedData();

        foreach ($OpenActivities as $key => $OpenActivity) {

            $ObjRelatedData = $RelatedDataArr[$OpenActivity['RelatedTo']][$OpenActivity['What']];

            if(isset($OpenActivity['TaskID'])){
                $data['ID'] = $OpenActivity['TaskID'];
                $data['Type'] = 'Task';
            } else if(isset($OpenActivity['EventID'])){
                $data['ID'] = $OpenActivity['EventID'];
                $data['Type'] = 'Event';
            }
            $data['Title'] = $OpenActivity['Subject'].' - '.$OpenActivity['City'].', '.$OpenActivity['State'];
            if(date("H:i:s",strtotime($OpenActivity['StartDate'])) == '00:00:00' && date("H:i:s",strtotime($OpenActivity['EndDate'])) == '00:00:00'){
                $data['Start'] = date("Y-m-d",strtotime($OpenActivity['StartDate']));
                $data['End'] = date("Y-m-d",strtotime($OpenActivity['EndDate']));
                $data['StartTime'] = '';
                $data['EndTime'] = '';
                $data['Time'] = "All-Day";
            } else {
                $data['Start'] = date("Y-m-d",strtotime($OpenActivity['StartDate']));
                $data['End'] = date("Y-m-d",strtotime($OpenActivity['EndDate']));
                $data['StartTime'] = date("H:i",strtotime($OpenActivity['StartDate']));
                $data['EndTime'] = date("H:i",strtotime($OpenActivity['EndDate']));
                $data['Time'] = date("H:i",strtotime($OpenActivity['StartDate'])).'-'.date("H:i",strtotime($OpenActivity['EndDate']));
            }
            
            if($OpenActivity['RelatedTo'] == 'WorkOrder'){
                $data['Latitude'] = $ObjRelatedData->Latitude;
                $data['Longitude'] = $ObjRelatedData->Longitude;
                if($ObjRelatedData->Priority == 'Low')
                    $data['ShapeType'] = 'CIRCLE';
                else if($ObjRelatedData->Priority == 'Normal')
                    $data['ShapeType'] = 'SQUARE';
                else if($ObjRelatedData->Priority == 'High')
                    $data['ShapeType'] = 'STAR';
            } else {
                $data['Latitude'] = "0";
                $data['Longitude'] = "0";
                $data['ShapeType'] = '';
            }
            
            $data['ColorCode'] = isset($ObjRelatedData->ColorCode)?$ObjRelatedData->ColorCode:"#FFFFFF";
            $data['RelatedTo'] = $OpenActivity['RelatedTo'];
            $data['RelatedObjID'] = $ObjRelatedData->ID;
            $data['RelatedObjNo'] = $ObjRelatedData->No;
            $FinalData[] = $data;
        }
         
        return $FinalData;
    }

    function CalendarFilterData($OrganizationID)
    {
        $this->db->select('wt.WorkOrderTypeID, wt.WorkOrderType');
        $this->db->from('WorkOrderType wt');
        $this->db->where(array('wt.OrganizationID' => $OrganizationID, 'wt.IsDeleted' => 0));
        $Type = $this->db->get();
        $data['WOType'] = $Type->result();

        $this->db->select('wos.WOStatusID, wos.Status');
        $this->db->from('WOStatus wos');
        $this->db->where(array('wos.OrganizationID' => $OrganizationID, 'wos.IsDeleted' => 0));
        $Status = $this->db->get();
        $data['WOStatus'] = $Status->result();

        $this->db->select('wop.WOPriorityID, wop.Priority');
        $this->db->from('WOPriority wop');
        $this->db->where(array('wop.OrganizationID' => $OrganizationID, 'wop.IsDeleted' => 0));
        $Priority = $this->db->get();
        $data['WOPriority'] = $Priority->result();
 
        return $data;
    }


    function GetWorkOrderByDate($UserID, $OrganizationID, $FromDate, $ToDate, $WorkOrderType, $WOStatus, $WOPriority, $AssignedTo)
    {  
        $FinalData = array(); 

        $this->db->select('wo.*, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, wos.Status, wos.ColorCode, wop.Priority, wo.StartDate, wos.IsClosed');
        $this->db->from('WorkOrder wo');
        $this->db->join('User at','at.UserID = wo.AssignedTo', 'left');
        $this->db->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left');
        $this->db->join('WOPriority wop','wop.WOPriorityID = wo.WOPriority', 'left');
        $this->db->where("((DATE(wo.StartDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."') OR (DATE(wo.EndDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."'))");

        if(isset($AssignedTo) && !empty($AssignedTo))
            $this->db->where_in('wo.AssignedTo', $AssignedTo);
        else if(isset($AssignedTo) && !is_null($AssignedTo)) 
            $this->db->where_in('wo.AssignedTo', '');
        
        if(isset($WorkOrderType) && !empty($WorkOrderType))
            $this->db->where_in('wo.WorkOrderType', $WorkOrderType);
        else if(isset($WorkOrderType) && !is_null($WorkOrderType)) 
            $this->db->where_in('wo.WorkOrderType', '');

        if(isset($WOStatus) && !empty($WOStatus))
            $this->db->where_in('wo.WOStatus', $WOStatus);
        else if(isset($WOStatus) && !is_null($WOStatus)) 
            $this->db->where_in('wo.WOStatus', '');

        if(isset($WOPriority) && !empty($WOPriority))
            $this->db->where_in('wo.WOPriority', $WOPriority);
        else if(isset($WOPriority) && !is_null($WOPriority)) 
            $this->db->where_in('wo.WOPriority', '');
       
        $this->db->where(array('wo.OrganizationID' => $OrganizationID, 'wo.IsDeleted' => 0));
        $WorkOrderQuery =  $this->db->get();  
        //echo $this->db->last_query(); exit; 
        $WorkOrders = $WorkOrderQuery->result_array();
 
        foreach ($WorkOrders as $key => $WorkOrder) {

            $data['WorkOrderID'] = $WorkOrder['WorkOrderID'];
            $data['Title'] = $WorkOrder['Subject'].' - '.$WorkOrder['City'].', '.$WorkOrder['State'];
            $data['Subject'] = $WorkOrder['Subject'];
            $data['AssignedTo'] = $WorkOrder['AssignedToName'];
            $data['RelatedTo'] = $WorkOrder['WorkOrderNo'];
            $data['Status'] = $WorkOrder['Status'];
            $data['Priority'] = $WorkOrder['Priority'];
            $data['Start'] = date("Y-m-d",strtotime($WorkOrder['StartDate']));
            $data['End'] = date("Y-m-d",strtotime($WorkOrder['EndDate']));
            $data['StartTime'] = date("H:i",strtotime($WorkOrder['StartDate']));
            $data['EndTime'] = date("H:i",strtotime($WorkOrder['EndDate']));
            $data['Latitude'] = $WorkOrder['Latitude'];
            $data['Longitude'] = $WorkOrder['Longitude'];
            if($WorkOrder['Priority'] == 'Low')
                $data['ShapeType'] = 'CIRCLE';
            else if($WorkOrder['Priority'] == 'Normal')
                $data['ShapeType'] = 'SQUARE';
            else if($WorkOrder['Priority'] == 'High')
                $data['ShapeType'] = 'STAR';
            $data['ColorCode'] = $WorkOrder['ColorCode'];
            $data['RelatedTo'] = 'WorkOrder';
            $data['RelatedObjID'] = $WorkOrder['WorkOrderID'];
            $data['RelatedObjNo'] = $WorkOrder['WorkOrderNo'];

            $FinalData[] = $data;
        }
         
        return $FinalData;
    }
}

?>
