<?php
require_once(APPPATH.'models/API/API_Model.php');

class ViewModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function CreateNewView()
    {
        extract($_POST);

        $data = array(
            'OrganizationID' => $OrganizationID,
            $Object.'ViewName' => $ViewName,
            'UserID' => $UserID,
            'RestrictVisibility' => $RestrictVisibility,
            'SpecifyFieldsDisplay' => implode(",", $SpecifyFieldsDisplay),
            'CreatedDate' => date("Y-m-d H:i:s"),
            'LastModifiedDate' => date("Y-m-d H:i:s")
        );     
        $query = $this->db->insert($Object.'View', $data);
        API_Model::checkQuery($query);     
        $ViewID = $this->db->insert_id();

        API_Model::insertCreatedModifiedByAndDt($UserID, $Object.'View', array($Object.'ViewID' => $ViewID));
 
        if(!empty($FilterValues) && !empty($ViewID)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        $Object.'ViewID' => $ViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert($Object.'Filter', $data);  
                    API_Model::checkQuery($query);   
                }
            } 
 
        }
        return $ViewID; 
    }

    function GetCustomViewDetails(){
 
        extract($_POST);

        $query = $this->db->select("".$Object."ViewID as ViewID, ".$Object."ViewName as ViewName, SpecifyFieldsDisplay, RestrictVisibility")->from($Object.'View')->where(array('OrganizationID' => $OrganizationID, $Object.'ViewID' => $ViewID))->get(); 

        API_Model::checkQuery($query);

        $ViewData = $query->row_array();

        $data['data'] = $ViewData;

        $SpecifyFields = array();
        $SpecifyFieldsDisplay = explode(",", $ViewData['SpecifyFieldsDisplay']);
        foreach ($SpecifyFieldsDisplay as $key => $value) { 
           $Field['FieldName'] = $value;
           $SpecifyFields[] = $Field;
        }

        $data['data']['DisplayedColumns'] = $SpecifyFields;

        $query = $this->db->select("".$Object."FilterID, FilterField, FilterCondition, FilterValue")->from($Object.'Filter')->where(array($Object.'ViewID' => $ViewID))->get(); 

        API_Model::checkQuery($query);

        $data['data']['Filters'] = $query->result();

        return $data; 
    }

    function CopyCustomView()
    {
        extract($_POST);

        $query = $this->db->get_where($Object.'View', array($Object.'ViewID' => $ViewID)); 
      
        foreach ($query->result() as $row){   
           foreach($row as $key=>$val){ 
                if($key != $Object.'ViewID'){    
                    $this->db->set($key, $val); 
                }                
           }
        }
        $query = $this->db->insert($Object.'View');
        API_Model::checkQuery($query);   
        $NewViewID = $this->db->insert_id();
        API_Model::insertCreatedModifiedByAndDt($UserID, $Object.'View', array($Object.'ViewID' => $NewViewID));

        $query = $this->db->get_where($Object.'Filter', array($Object.'ViewID' => $ViewID)); 
       
        foreach ($query->result() as $row){   
           foreach($row as $key => $val){ 
                if($key == $Object.'ViewID'){  
                    $this->db->set($key, $NewViewID);  
                } else if($key != $Object.'FilterID'){    
                    $this->db->set($key, $val); 
                }             
           }
           $query = $this->db->insert($Object.'Filter');
           API_Model::checkQuery($query);   
        }

        return $NewViewID; 
    }

    function RenameCustomView(){

        extract($_POST);

        $data = array(
            $Object.'ViewName' => $ViewName
        );

        $query = $this->db->update($Object.'View', $data, array($Object.'ViewID' => $ViewID)); 

        API_Model::checkQuery($query);  

        API_Model::updateCreatedModifiedByAndDt($UserID, $Object.'View', array($Object.'ViewID' => $ViewID));

        return $ViewID;
    }

    function EditSharingCustomView(){

        extract($_POST);

        $data = array(
            'RestrictVisibility' => $RestrictVisibility
        );

        $query = $this->db->update($Object.'View', $data, array($Object.'ViewID' => $ViewID));  

        API_Model::checkQuery($query);  

        API_Model::updateCreatedModifiedByAndDt($UserID, $Object.'View', array($Object.'ViewID' => $ViewID));

        return $ViewID;
    }

    function EditFiltersCustomView(){ 

        extract($_POST);

        $query = $this->db->delete($Object.'Filter', array($Object.'ViewID' => $ViewID));

        API_Model::checkQuery($query); 

        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        $Object.'ViewID' => $ViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert($Object.'Filter', $data);  

                    API_Model::checkQuery($query); 
                }
            } 
 
        }
        return $ViewID; 
    }

    function EditDisplayedColumnsCustomView(){

        extract($_POST);

        $data = array(
            'SpecifyFieldsDisplay' => implode(",", $SpecifyFieldsDisplay)
        );

        $query = $this->db->update($Object.'View', $data, array($Object.'ViewID' => $ViewID));

        API_Model::checkQuery($query);

        API_Model::updateCreatedModifiedByAndDt($UserID, $Object.'View', array($Object.'ViewID' => $ViewID));

        return $ViewID; 
    }

    function DeleteCustomView(){

        extract($_POST);

        $query = $this->db->delete($Object.'View', array('OrganizationID' => $OrganizationID, $Object.'ViewID' => $ViewID));

        API_Model::checkQuery($query);
          
        return $query; 
    }

    function SortCustomView(){
 
        extract($_POST);

        $data = array(
           'SortByField' => $SortByField,
           'SortByValue' => $SortByValue
        );

        $query = $this->db->update($Object.'View', $data, array($Object.'ViewID' => $ViewID)); 

        API_Model::checkQuery($query);  

        API_Model::updateCreatedModifiedByAndDt($UserID, $Object.'View', array($Object.'ViewID' => $ViewID));

        return $ViewID;
    }
}

?>
