<?php
require_once(APPPATH.'models/API/API_Model.php');

class ActionsModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function CreateNewFile() {

        extract($_POST);

        $FILE_PATH = str_replace('{org_id}', $OrganizationID, FILE_PATH);

        if(!empty($_FILES["FileName"]['name'][0])){
           
            $files  = $_FILES;
            $file_count    = count($_FILES['FileName']['name']);
            for($i = 0; $i < $file_count; $i++)
            {
                $_FILES['FileName']['name']        = $files['FileName']['name'][$i];
                $_FILES['FileName']['type']        = $files['FileName']['type'][$i];
                $_FILES['FileName']['tmp_name']    = $files['FileName']['tmp_name'][$i];
                $_FILES['FileName']['error']       = $files['FileName']['error'][$i];
                $_FILES['FileName']['size']        = $files['FileName']['size'][$i];

                $FileName = explode(".", $_FILES['FileName']['name'])[0];
                $config = array(
                 'upload_path' => $FILE_PATH,
                 'file_name' => $FileName.'_'.date('YmdHis'),
                 'allowed_types' => "jpg|png|jpeg|doc|pptx|xlsx|docx|zip|pdf|txt|rtf",
                 'overwrite' => FALSE,
                 'max_size' => "25600000"   //'max_height' => "768", 'max_width' => "1024" // Can be set to particular file size , here it is 2 MB(2048 Kb)
                );
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
               
                if($this->upload->do_upload('FileName')){
                    $fileData = $this->upload->data();
                    chmod($FILE_PATH.$fileData['file_name'], 0777);

                    $data = array(
                        'OrganizationID' =>  $OrganizationID,
                        'Subject' => $Subject,
                        'RelatedTo' => $RelatedTo,
                        'What' => $What,
                        'AssignedTo' => $AssignedTo,
                        'ContentType' => $fileData['file_type'],
                        'Description' => $Description,
                        'FileName' => $fileData['file_name'],
                        'FileSize' => round($fileData['file_size']/1024, 2)
                    ); 
                    
                    $query = $this->db->insert('File', $data); 
                    $FileID = $this->db->insert_id();
                    API_Model::checkQuery($query);  

                    API_Model::insertCreatedModifiedByAndDt($UserID,'File', array('FileID' => $FileID));
        
                }
            }
        }
        return array('RelatedTo' => $RelatedTo, 'What' => $What);
    }
 
    function SendEmail()
    { 
        extract($_POST); 

        if(isset($EmailTemplate) && !empty($EmailTemplate)){
            $query = $this->db->get_where('EmailTemplates', array('EmailTemplateID' => $EmailTemplate, 'IsDeleted' => 0)); 
            if($query->num_rows() > 0){
                $result = $query->row();
                $EmailBody = str_replace("{{EMAIL_CONTAINS}}", $EmailBody, $result->Body);
            }
        }
       
        $this->load->library('email');
        $this->email->set_newline("\r\n");
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['smtp_user'] = SMTP_USER;
        $config['smtp_pass'] = SMTP_PASS;
        $config['smtp_crypto'] = SMTP_CRYPTO; 
        $config['protocol'] = PROTOCOL; 
        $config['mailtype'] = 'html'; 
        $config['send_multipart'] = FALSE;
        $this->email->initialize($config);
        $this->email->from(ADMIN_EMAIL); 
        $this->email->to($ToEmailID);
        if(isset($CCEmailID) && !empty($CCEmailID))
            $this->email->cc($CCEmailID);
        if(isset($BCCEmailID) && !empty($BCCEmailID))
            $this->email->bcc($BCCEmailID);
        $this->email->subject($EmailSubject);
        $this->email->message($EmailBody);

        $TEMP_PATH = str_replace('{org_id}', $OrganizationID, TEMP_PATH);
       
        if(isset($_FILES) && !empty($_FILES["EmailAttachment"]['name'][0])){

            $files  = $_FILES;
            $file_count    = count($_FILES['EmailAttachment']['name']);
            for($i = 0; $i < $file_count; $i++)
            {
                $_FILES['EmailAttachment']['name']        = $files['EmailAttachment']['name'][$i];
                $_FILES['EmailAttachment']['type']        = $files['EmailAttachment']['type'][$i];
                $_FILES['EmailAttachment']['tmp_name']    = $files['EmailAttachment']['tmp_name'][$i];
                $_FILES['EmailAttachment']['error']        = $files['EmailAttachment']['error'][$i];
                $_FILES['EmailAttachment']['size']        = $files['EmailAttachment']['size'][$i];

                $new_image_name = 'file_'. uniqid().'_'.date('YmdHis');
                $config = array(
                 'upload_path' => $TEMP_PATH,
                 'file_name' => $new_image_name,
                 'allowed_types' => "jpg|jpeg|png|doc|pptx|xlsx|docx|zip|txt|pdf",
                 'overwrite' => TRUE,
                 'max_size' => "25600000"   //'max_height' => "768", 'max_width' => "1024" // Can be set to particular file size , here it is 2 MB(2048 Kb)
                );
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
               
                if($this->upload->do_upload('EmailAttachment')){
                    $fileData = $this->upload->data();
                    chmod($TEMP_PATH.$fileData['file_name'], 0777);
                    $file_name = $fileData['file_name'];
                    $this->email->attach($TEMP_PATH.$file_name);
                }
            }
        }
        
        if($this->email->send()){

            $RelatedObjData = $this->GetRelatedObjData($RelatedTo, $What);
            if($RelatedTo == 'Contact'){

                $this->CreateEmailTask($UserID, $OrganizationID, $EmailSubject, $RelatedTo, $What, $RelatedObjData->AssignedTo, $What, NULL, $ToEmailID, $RelatedObjData->PhoneNo, date("Y-m-d"));

            } else if($RelatedTo == 'WorkOrder'){

                $this->CreateEmailTask($UserID, $OrganizationID, $EmailSubject, $RelatedTo, $What, $RelatedObjData->AssignedTo, $RelatedObjData->PrimaryContact, NULL, $ToEmailID, NULL, date("Y-m-d"));

            } else if($RelatedTo == 'Estimate'){

                $this->CreateEmailTask($UserID, $OrganizationID, $EmailSubject, $RelatedTo, $What, $RelatedObjData->Owner, $RelatedObjData->Contact, NULL, $ToEmailID, $RelatedObjData->Phone, date("Y-m-d"));
            }
            return 1;
        }
        return 0;
    }

    function CreateEmailTask($UserID, $OrganizationID, $Subject, $RelatedTo, $What, $AssignedTo, $Who, $Description, $Email, $Phone, $DueDate){

            $data = array(
                'OrganizationID' =>  $OrganizationID,
                'Subject' => $Subject,
                'RelatedTo' => $RelatedTo,
                'What' => $What,
                'AssignedTo' => $AssignedTo,
                'Who' => $Who,
                'Description' => $Description,
                'Email' => $Email,
                'Phone' => $Phone, 
                'DueDate' => $DueDate
            ); 
            $this->db->set($data);
            $this->db->set('TaskType',"(SELECT TaskTypeID from TaskType where TaskType='Email')", FALSE);
            $result = $this->db->insert('Task');    
           
            $TaskID = $this->db->insert_id();

            API_Model::insertCreatedModifiedByAndDt($UserID,'Task', array('TaskID' => $TaskID));

            return $TaskID;
    }

    function GetRelatedObjData($RelatedTo, $What)
    {
        $query = $this->db->get_where($RelatedTo, array($RelatedTo.'ID' => $What, 'IsDeleted' => 0)); 
        return $query->row();
    }

    function DeleteObject()
    {
        extract($_POST); 

        return $this->db->update($RelatedTo, array('IsDeleted' => 1), array('OrganizationID' => $OrganizationID, $RelatedTo.'ID' => $What));  
    }

    function CreateEvent($UserID, $OrganizationID, $Subject, $AssignedTo, $RelatedTo, $What, $Who, $Description, $EventPriority, $EventType, $EventStartDate, $EventEndDate, $IsAllDayEvent, $IsRecurring, $StartOn, $RepeatEvery, $IntervalEvery, $Ends, $RepeatOn, $EndsOnDate, $EndsAfterOccurrences, $StartTime, $EndTime) {

        extract($_POST);

        $WORecurrenceID = NULL;   
        if(isset($IsRecurring) && $IsRecurring == 1){ 
            $data = array(
                'StartOn' => date("Y-m-d", strtotime($StartOn)),
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'Ends' => $Ends,
                'StartTime' => $StartTime,
                'EndTime' => $EndTime,
                'RepeatOn' => $RepeatOn,
                'EndsOnDate' => date("Y-m-d", strtotime($EndsOnDate)),
                'EndsAfterOccurrences' => $EndsAfterOccurrences,
                'CreatedDate' => date("Y-m-d H:i:s"),
                'LastModifiedDate' => date("Y-m-d H:i:s")
            );   
            $query = $this->db->insert('WORecurrence', $data);

            API_Model::checkQuery($query); 

            $WORecurrenceID = $this->db->insert_id();
        } 

        $data = array(
            'OrganizationID' => $OrganizationID,
            'Subject' => $Subject,
            'AssignedTo' => $AssignedTo,
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            'Who' => $Who,
            'Description' => $Description,
            'IsAllDayEvent' => $IsAllDayEvent,
            'EventPriority' => $EventPriority,
            'EventType' => $EventType,
            'IsRecurrence' => $IsRecurring,
            'RecurrenceID' => $WORecurrenceID,
            'EventStartDate' => date("Y-m-d H:i:s",strtotime($EventStartDate)),
            'EventEndDate' => date("Y-m-d H:i:s",strtotime($EventEndDate)),
            'EventStartTime' => date("H:i:s",strtotime($EventStartDate)),
            'EventEndTime' => date("H:i:s",strtotime($EventEndDate)),
        );   

        if(isset($IsRecurring) && $IsRecurring == 1 && isset($WORecurrenceID) && !empty($WORecurrenceID)){

            $RecurringEventData = $this->CreateRecurringEvent($WORecurrenceID, $data, $UserID);

        } else {
            
            $query = $this->db->insert('Event', $data);  

            API_Model::checkQuery($query);  

            $EventID = $this->db->insert_id(); 

            API_Model::insertCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));

            $RecurringEventData = array('EventID' => $EventID);
        }
    
        $ResData['EventID'] = $RecurringEventData['EventID'];
        $ResData['Subject'] = $Subject;
        return $ResData;
    }

    function EditEvent($EventID, $UserID, $OrganizationID, $Subject, $AssignedTo, $RelatedTo, $What, $Who, $Description, $EventPriority, $EventType, $EventStartDate, $EventEndDate, $IsAllDayEvent) {

        extract($_POST);

        $data = array(
            'Subject' => $Subject,
            'AssignedTo' => $AssignedTo,
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            'Who' => empty($Who) ? NULL : $Who,
            'Description' => isset($Description)?$Description:NULL,
            'IsAllDayEvent' => isset($IsAllDayEvent)?$IsAllDayEvent:NULL,
            'EventPriority' => $EventPriority,
            'EventType' => $EventType,
            'EventStartDate' => date("Y-m-d H:i:s",strtotime($EventStartDate)),
            'EventEndDate' => date("Y-m-d H:i:s",strtotime($EventEndDate)),
            'EventStartTime' => date("H:i:s",strtotime($EventStartDate)),
            'EventEndTime' => date("H:i:s",strtotime($EventEndDate)),
        );   
  
        $query = $this->db->update("Event", $data, array('EventID' => $EventID));

        API_Model::checkQuery($query);  

        API_Model::updateCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));

        $ResData['EventID'] = $EventID;
        $ResData['Subject'] = $Subject;
        return $ResData;
    }

    function CreateRecurringEvent($WORecurrenceID, $EventData, $UserID)
    {
        $query = $this->db->get_where('WORecurrence', array('WORecurrenceID' => $WORecurrenceID));  
        $result = $query->row();

        $StartOnDate = date("Y-m-d", strtotime($result->StartOn));
        $EndsAfterOccurrences = isset($result->EndsAfterOccurrences) ? ($result->EndsAfterOccurrences * $result->IntervalEvery) : (100 * $result->IntervalEvery);

        if(isset($result->RepeatEvery) && ($result->RepeatEvery == "Daily" || $result->RepeatEvery == "Monthly" || $result->RepeatEvery == "Yearly")){

            if($result->RepeatEvery == "Daily"){
                $IntervalUnit = 'D'; 
                $IntervalType = 'day'; 
            } else if($result->RepeatEvery == "Monthly"){
                $IntervalUnit = 'M'; 
                $IntervalType = 'months'; 
            } else if($result->RepeatEvery == "Yearly"){
                $IntervalUnit = 'Y'; 
                $IntervalType = 'years'; 
            }

            if($result->Ends == 'On'){
                $EndsOnDate = date("Y-m-d", strtotime($result->EndsOnDate));
            } else{
                $EndsOnDate = date('Y-m-d', strtotime("+$EndsAfterOccurrences $IntervalType", strtotime($StartOnDate)));
            }

            $start = new DateTime( $StartOnDate );
            $end = new DateTime( $EndsOnDate );
            $interval = new DateInterval('P'.$result->IntervalEvery.''.$IntervalUnit.'');
            $flag = 1;
            $period = new DatePeriod($start, $interval, $end);
            foreach ($period as $date) {
                //print $date->format('Y-m-d H:i:s') . '<br/>';
                $StartDateTime = $date->format('Y-m-d').' '.$result->StartTime;
                $EndDateTime = $date->format('Y-m-d').' '.$result->EndTime;
                $RecurringEventData = $this->InsertRecurringEvent($EventData, $StartDateTime, $EndDateTime, $UserID);
                if($flag == 100)
                        break;
                $flag++;
            }

            return $RecurringEventData;

        } else if(isset($result->RepeatEvery) && $result->RepeatEvery == "Weekly"){

            $RepeatOn = explode(",", $result->RepeatOn);
            if($result->Ends == 'On'){
                $EndsOnDate = date("Y-m-d", strtotime($result->EndsOnDate));
            } else{
                $EndsOnDate = date('Y-m-d', strtotime("+$EndsAfterOccurrences weeks", strtotime($StartOnDate)));
            }

            $start = new DateTime( $StartOnDate );
            $end = new DateTime( $EndsOnDate );
            $interval = new DateInterval('P1D');

            $period = new DatePeriod($start, $interval, $end);

            // only trigger every interval weeks...
            $weekInterval = $result->IntervalEvery;

            $fakeWeek = 0;
            $currentWeek = $start->format('W');
            $flag = 1;
            foreach ($period as $date) {

                if ($date->format('W') !== $currentWeek) {
                    $currentWeek = $date->format('W');
                    $fakeWeek++; 
                    //print ' WEEK ' . $currentWeek . '<br/>';
                }

                if ($fakeWeek % $weekInterval !== 0) {
                    continue;
                }

                $dayOfWeek = $date->format('D');

                if (in_array($dayOfWeek, $RepeatOn)) {
                    //print $date->format('Y-m-d H:i:s') . '   ' . $dayOfWeek . '<br/>';
                    $StartDateTime = $date->format('Y-m-d').' '.$result->StartTime;
                    $EndDateTime = $date->format('Y-m-d').' '.$result->EndTime;
                    $RecurringEventData = $this->InsertRecurringEvent($EventData, $StartDateTime, $EndDateTime, $UserID);

                    if((isset($result->EndsAfterOccurrences) && $flag == $result->EndsAfterOccurrences) || ($flag == 100)){
                        break;
                    }
                    $flag++;
                }
            }

            return $RecurringEventData;

        } /*else if(isset($result->RepeatEvery) && $result->RepeatEvery == "Periodically"){
            
        }*/
    }

    function InsertRecurringEvent($EventData, $StartDateTime, $EndDateTime, $UserID)
    {

         $data = array(
            'OrganizationID' => $EventData['OrganizationID'],
            'Subject' => $EventData['Subject'],
            'AssignedTo' => $EventData['AssignedTo'],
            'RelatedTo' => $EventData['RelatedTo'],
            'What' => $EventData['What'],
            'Who' => $EventData['Who'],
            'Description' => $EventData['Description'],
            'IsAllDayEvent' => $EventData['IsAllDayEvent'],
            'EventPriority' => $EventData['EventPriority'],
            'EventType' => $EventData['EventType'],
            'EventStartDate' => date("Y-m-d H:i:s",strtotime($StartDateTime)),
            'EventEndDate' => date("Y-m-d H:i:s",strtotime($EndDateTime)),
            'EventStartTime' => date("H:i:s",strtotime($StartDateTime)),
            'EventEndTime' => date("H:i:s",strtotime($EndDateTime)),
            'IsRecurrence' => $EventData['IsRecurrence'],
            'RecurrenceID' => $EventData['RecurrenceID'],
        );     
        $query = $this->db->insert('Event', $data);  

        API_Model::checkQuery($query);  

        $EventID = $this->db->insert_id();  

        API_Model::insertCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));

        return array('EventID' => $EventID);
    }

    function EventDetails($UserID, $EventID, $OrganizationID)
    { 
        $query = $this->db->select('e.*, e.EventID, e.RelatedTo, e.What, e.Subject, CONCAT(c.FirstName, " ", c.LastName) as ContactName, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, DATE_FORMAT(e.EventStartDate, "'.RES_DATETIME.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATETIME.'") as EventEndDate, ts.EventStatus, tp.Priority as EventPriority, tt.EventTypeName, ts.EventStatusID, tt.EventTypeID, tp.EventPriorityID, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy')
        ->from('Event e')
        ->join('EventStatus ts','ts.EventStatusID = e.EventStatus', 'left')
        ->join('EventPriority tp','tp.EventPriorityID = e.EventPriority', 'left')
        ->join('EventType tt','tt.EventTypeID = e.EventType', 'left') 
        ->join('User at','at.UserID = e.AssignedTo', 'left') 
        ->join('Contact c','c.ContactID = e.Who', 'left') 
        ->join('User cb','cb.UserID = e.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = e.LastModifiedBy', 'left') 
        ->where(array('e.EventID' => $EventID, 'e.OrganizationID' => $OrganizationID)) //, 'e.IsDeleted' => 0
        ->get();  

        API_Model::checkQuery($query); 

        $result = $query->row_array();
        $RelatedData = API_Model::getRelatedData();
        $RelatedDt = $RelatedData[$result['RelatedTo']][$result['What']];
        $result["RelatedToName"] = $result['RelatedTo'];
        $result["RelatedTo"] = $RelatedDt->No;
        $result["WhatName"] = $RelatedDt->Name;

        return $result; 
    }

    function CreateNote() {

        extract($_POST);

        $data = array(
            'OrganizationID' => $OrganizationID,
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            'Owner' => $UserID,
            'Body' => $Body,
            'Subject' => $Subject
        );   
        $query = $this->db->insert('Note', $data);

        API_Model::checkQuery($query); 

        $NoteID = $this->db->insert_id();

        API_Model::insertCreatedModifiedByAndDt($UserID, 'Note', array('NoteID' => $NoteID));

        return $NoteID;
    }

    function EditNote() {

        extract($_POST);

        $data = array(
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            'Owner' => $UserID,
            'Body' => $Body,
            'Subject' => $Subject
        );   
        $query = $this->db->update('Note', $data, array('NoteID' => $NoteID, 'OrganizationID' => $OrganizationID));

        API_Model::checkQuery($query);

        API_Model::updateCreatedModifiedByAndDt($UserID, 'Note', array('NoteID' => $NoteID));

        return $query;
         
    }

    function NoteDetails($UserID, $NoteID, $OrganizationID)
    {
        $query = $this->db->select('n.*, n.Subject as Title, CONCAT(o.FirstName, " ", o.LastName) as OwnerName, DATE_FORMAT(n.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,DATE_FORMAT(n.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName')
        ->from('Note n')
        ->join('User cb','cb.UserID = n.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = n.LastModifiedBy', 'left')
        ->join('User o','o.UserID = n.Owner', 'left')
        ->where(array('n.OrganizationID' => $OrganizationID, 'n.NoteID' => $NoteID)) 
        ->get();
        
        $data = $query->row_array();

        if(isset($data)){
            $RelatedObjData = API_Model::getRelatedData();
            $RelatedObjData = $RelatedObjData[$data['RelatedTo']][$data['What']];
            $data['RelatedObjNo'] = $RelatedObjData->No;
        }

        return $data;
    }

    function UserDetails($UserID, $OrganizationID, $ViewUserID){
        $this->db->select('u.*, CONCAT(u.FirstName, " ", u.LastName) as FullName, DATE_FORMAT(u.StartDate, "'.RES_DATE.'") as StartDate, DATE_FORMAT(u.EndDate, "'.RES_DATE.'") as EndDate, DATE_FORMAT(u.StartOfDay, "'.RES_TIME.'") as StartOfDay, DATE_FORMAT(u.EndOfDay, "'.RES_TIME.'") as EndOfDay, p.Name as ProfileName, dp.DepartmentName, dv.DivisionName, o.CompanyName, CONCAT(mn.FirstName, " ", mn.LastName) as ManagerName, tz.TimeZone as TimeZoneName, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy, DATE_FORMAT(u.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, DATE_FORMAT(u.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate');
        
        $this->db->from('User u');
        $this->db->join('Profile p','p.ProfileID = u.Profile', 'left');
        $this->db->join('Department dp','dp.DepartmentID = u.Department', 'left');
        $this->db->join('Division dv','dv.DivisionID = u.Division', 'left');
        $this->db->join('Organization o','o.OrganizationID = u.OrganizationID', 'left');
        $this->db->join('User mn','mn.UserID = u.Manager', 'left');
        $this->db->join('OrgTimeZone tz','tz.OrgTimeZoneID = u.TimeZone', 'left');
        $this->db->join('User cb','cb.UserID = u.CreatedBy', 'left');
        $this->db->join('User lmb','cb.UserID = u.LastModifiedBy', 'left');
        $this->db->where(array('u.OrganizationID' => $OrganizationID, 'u.UserID' => $ViewUserID));
        $query = $this->db->get();
        return $query->row_array();
    }

    function ProductDetails($UserID, $OrganizationID, $ProductID){

       extract($_POST);
 
       $this->db->select('p.*, pf.ProductFamily as ProductFamilyName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName, DATE_FORMAT(p.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, DATE_FORMAT(p.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,oum.UnitOfMeasurement as QuantityUnitOfMeasureName');
        $this->db->from('Product p');
        $this->db->join('ProductFamily pf','pf.ProductFamilyID = p.ProductFamily', 'left');
        $this->db->join('User cb','cb.UserID = p.CreatedBy', 'left');
        $this->db->join('User lmb','lmb.UserID = p.LastModifiedBy', 'left');
        $this->db->join('UnitOfMeasurement oum','oum.UnitOfMeasurementID = p.QuantityUnitOfMeasure', 'left');
        //$this->db->join('Account a','a.AccountID = p.Account', 'left');
        $this->db->where(array('p.ProductID' => $ProductID, 'p.OrganizationID' => $OrganizationID));
        $query = $this->db->get();  

        $data = $query->row_array();
        if(isset($RelatedTo) && isset($What)){

            if($RelatedTo == 'WorkOrder'){
                $query = $this->db->select('a.AccountName')->from('WorkOrder wo')->join('Account a','a.AccountID = wo.Account', 'left')->where(array('wo.OrganizationID' => $OrganizationID, 'wo.WorkOrderID' => $What))->get();

                 $result = $query->row();

            } else if($RelatedTo == 'Invoice'){

                $query = $this->db->select('a.AccountName')->from('Invoice in')->join('Account a','a.AccountID = in.Account', 'left')->where(array('in.OrganizationID' => $OrganizationID, 'in.InvoiceID' => $What))->get();

                 $result = $query->row();

            } else if($RelatedTo == 'Estimate'){

                $query = $this->db->select('a.AccountName')->from('Estimate et')->join('Account a','a.AccountID = et.Account', 'left')->where(array('et.OrganizationID' => $OrganizationID, 'EstimateID' => $What))->get();

                 $result = $query->row();

            }
            
            $data['AccountName'] = $result->AccountName;
        } else {
            $data['AccountName'] = '';
        }
        

        return $data;
    }

    function GenerateDocument($UserID, $OrganizationID, $GenDocTemplateID, $RelatedTo, $ObjectID, $SaveToObject, $OutputFormat)
    {

        $FILE_PATH = str_replace('{org_id}', $OrganizationID, FILE_PATH);
        
        include_once APPPATH.'/libraries/DocxTemplate/src/DocxTemplate.php';

        $DOC_TEMPLATE_PATH = str_replace('{org_id}', $OrganizationID, DOC_TEMPLATE_PATH);
        $DOC_TEMPLATE_URL = str_replace('{org_id}', $OrganizationID, DOC_TEMPLATE_URL);

        $query = $this->db->get_where('GenDocTemplates', array('OrganizationID' => $OrganizationID, 'GenDocTemplateID' => $GenDocTemplateID)); 
        $GenDocTemplate = $query->row();
        
        if(!empty($GenDocTemplate)){

            if($RelatedTo == "WorkOrder"){
                $this->load->model('API/WorkOrder/WoActionsModel');
                $TemplateContainsData = $this->WoActionsModel->GetTemplateContainsData($UserID, $ObjectID, $OrganizationID);
                $ObjectNo = $TemplateContainsData["WorkOrderNo"];
                $AssignedTo = $TemplateContainsData["AssignedTo"];

            } else if($RelatedTo == "Invoice"){

                $this->load->model('API/Invoice/INActionsModel');
                $TemplateContainsData = $this->INActionsModel->GetTemplateContainsData($UserID, $ObjectID, $OrganizationID);
                $ObjectNo = $TemplateContainsData["InvoiceNo"];
                $AssignedTo = $TemplateContainsData["AssignedTo"];

            } else if($RelatedTo == "Estimate"){

                $this->load->model('API/Estimate/ETActionsModel');
                $TemplateContainsData = $this->ETActionsModel->GetTemplateContainsData($UserID, $ObjectID, $OrganizationID);
                $ObjectNo = $TemplateContainsData["EstimateNo"];
                $AssignedTo = $TemplateContainsData["AssignedTo"];
            }

            // Start Code for Generate Docx File
            $FileName = pathinfo($ObjectNo.'-'.$GenDocTemplate->TemplateFile, PATHINFO_FILENAME);
            $TemplatePath = $DOC_TEMPLATE_PATH.$GenDocTemplate->TemplateFile;
            $OutputPath = $DOC_TEMPLATE_PATH.$FileName.".docx";
            $PDFOutputURL = $DOC_TEMPLATE_URL.$FileName.'.pdf';

            $docxTemplate = new DocxTemplate($TemplatePath); 
            
            file_exists($OutputPath)?unlink($OutputPath):"";
            
            //$this->assertFalse(file_exists($OutputPath));

            $docxTemplate->merge($TemplateContainsData, $OutputPath);

            //$this->assertTrue(file_exists($OutputPath)); 

            chmod($OutputPath, 0777);

            // End Code for Generate Docx File

            // Start Code for Generate PDF File 

            $PDFOutputPath = $DOC_TEMPLATE_PATH.$FileName.'.pdf';

            API_Controller::convertDocxToPDF($OutputPath, $PDFOutputPath);

            // End Code for Generate PDF File

            // Start Code for Save file into File Object 
            if(isset($SaveToObject) && $SaveToObject == 1){

                if($OutputFormat == "PDF"){

                    $NewFileName = $FileName.'_'.date('YmdHis').".pdf";
                    copy($PDFOutputPath, $FILE_PATH.$NewFileName);
                    $MimeType = mime_content_type($FILE_PATH.$NewFileName);
                    $FileSize = round(filesize($FILE_PATH.$NewFileName)/1048576, 2);

                } else if($OutputFormat == "Word"){

                    $NewFileName = $FileName.'_'.date('YmdHis').".docx";
                    copy($OutputPath, $FILE_PATH.$NewFileName);
                    $MimeType = mime_content_type($FILE_PATH.$NewFileName);
                    $FileSize = round(filesize($FILE_PATH.$NewFileName)/1048576, 2);
                }

                $data = array(
                    'OrganizationID' =>  $OrganizationID,
                    'Subject' => "Generated doc for ".$ObjectNo,
                    'RelatedTo' => $RelatedTo,
                    'What' => $ObjectID,
                    'AssignedTo' => $AssignedTo,
                    'ContentType' => $MimeType,
                    'Description' => 'Generated document.',
                    'FileName' => $NewFileName,
                    'FileSize' => $FileSize
                ); 
                
                $result = $this->db->insert('File', $data); 
                  
                $FileID = $this->db->insert_id();

                API_Model::insertCreatedModifiedByAndDt($UserID, 'File', array('FileID' => $FileID));
            }
            // End Code for Save file into File Object 

            $resdata['GenerateDocumentURL'] = $PDFOutputURL;
            $resdata['RelatedTo'] = $RelatedTo;
            $resdata['ObjectID'] = $ObjectID;
            $resdata['ObjectNo'] = $ObjectNo;

            return $resdata;
        } else {
            return 2;
        }
    }
}

?>
