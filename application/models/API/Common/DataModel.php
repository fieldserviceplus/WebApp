<?php
require_once(APPPATH.'models/API/API_Model.php');

class DataModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function GetEmailTemplates() {

        extract($_POST);

        $query = $this->db->select('EmailTemplateID, Title')
        ->from('EmailTemplates')
        ->where(array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetEventPriorities() {

        extract($_POST);

        $query = $this->db->select('EventPriorityID, Priority, IsDefault')
        ->from('EventPriority')
        ->where(array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetEventTypes() {

        extract($_POST);

        $query = $this->db->select('EventTypeID, EventTypeName, IsDefault')
        ->from('EventType')
        ->where(array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetEventStatus() {

        extract($_POST);

        $query = $this->db->select('EventStatusID, EventStatus, IsDefault, IsClosed')
        ->from('EventStatus')
        ->where(array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetGenDocTemplates() {

        extract($_POST);

        $query = $this->db->select('GenDocTemplateID, TemplateName, RelatedTo, TemplateFile')
        ->from('GenDocTemplates')
        ->where(array('OrganizationID' => $OrganizationID, 'RelatedTo' => $RelatedTo, 'IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

}

?>
