<?php
require_once(APPPATH.'models/API/API_Model.php');

class ViewModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function GetViews($UserID, $OrganizationID)
    { 
        $ContactDefaultView = array(
                array('ContactViewID' => 'MyActiveContacts', 'ContactViewName' => 'My Active Contacts'),
                array('ContactViewID' => 'AllActiveContacts', 'ContactViewName' => 'All Active Contacts'),
                array('ContactViewID' => 'ContactsCreatedThisWeek', 'ContactViewName' => 'Contacts Created This Week')
            );

        $query = $this->db->select('ContactViewID, ContactViewName')
        ->from('ContactView')
        ->where(array('OrganizationID' => $OrganizationID, 'UserID' => $UserID, 'RestrictVisibility'=>'VisibleOnlyToMe'))
        ->or_where(array('RestrictVisibility' => 'VisibleToEveryone'))
        ->order_by('ContactViewName', 'asc')
        ->get();   

        API_Model::checkQuery($query); 
        
        return array_merge($ContactDefaultView, $query->result());
    }

    function GetDefaultContactData($UserID, $OrganizationID, $ContactViewID, $FilterData = array())
    {
            $WhereString = "";
            if($ContactViewID == 'MyActiveContacts'){

                $WhereString .= "ct.AssignedTo = $UserID AND ct.IsDeleted = 0 AND ct.IsActive = 1 AND ct.OrganizationID='$OrganizationID'";
            }
            else if($ContactViewID == 'AllActiveContacts'){

                $WhereString .= "ct.IsDeleted=0 AND ct.IsActive = 1 AND ct.OrganizationID='$OrganizationID'";

            } else if($ContactViewID == 'ContactsCreatedThisWeek'){

                $WhereString .= "YEARWEEK(ct.CreatedDate, 1) = YEARWEEK(CURDATE(), 1) AND ct.IsDeleted=0 AND ct.OrganizationID='$OrganizationID' AND ct.IsActive = 1";

            } else {
                 $WhereString .= "ct.OrganizationID='$OrganizationID' AND ct.IsDeleted=0 AND ct.IsActive = 1";
            }

            if(!empty($FilterData)){
                foreach ($FilterData['FilterField'] as $key => $value) {

                    $WhereField = "";
                    if($FilterData['FilterField'][$key] == 'AssignedTo'){
                        $WhereField = "CONCAT(at.FirstName, ' ', at.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'Account') {
                        $WhereField = "ac.AccountName";
                    } else if($FilterData['FilterField'][$key] == 'OrganizationID') {
                        $WhereField = "o.CompanyName";
                    } else if($FilterData['FilterField'][$key] == 'LeadSource') {
                        $WhereField = "ls.LeadSource";
                    } else if($FilterData['FilterField'][$key] == 'CreatedBy') {
                        $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'LastModifiedBy') {
                        $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'BirthDate'){
                         $WhereField = "DATE_FORMAT(ct.BirthDate, '".RES_DATETIME."')";
                    } else {
                        $WhereField = "ct.".$FilterData['FilterField'][$key];
                    }

                    if($FilterData['FilterCondition'][$key] == 'Equals'){
                         $WhereString .= " AND $WhereField = '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'NotEqualTo'){
                        $WhereString .= " AND $WhereField != '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'LessThan'){
                        $WhereString .= " AND $WhereField < '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'GreaterThan'){
                        $WhereString .= " AND $WhereField > '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'LessOREqualTo'){
                        $WhereString .= " AND $WhereField <= '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'GreaterOREqualTo'){
                        $WhereString .= " AND $WhereField >= '".$FilterData['FilterValue'][$key]."'";
                    } else if($FilterData['FilterCondition'][$key] == 'Contains'){
                        $WhereString .= " AND $WhereField LIKE '%".$FilterData['FilterValue'][$key]."%'";
                    } else if($FilterData['FilterCondition'][$key] == 'StartsWith'){ 
                        $WhereString .= " AND $WhereField LIKE '".$FilterData['FilterValue'][$key]."%'"; 
                    } else if($FilterData['FilterCondition'][$key] == 'DoesNotContain'){
                        $WhereString .= " AND $WhereField NOT LIKE '%".$FilterData['FilterValue'][$key]."%'";  
                    } 
                }
            }
            $OrderByString = "";
            if(isset($FilterData['SortByField']) && isset($FilterData['SortByValue']) && !empty($FilterData['SortByField']) && !empty($FilterData['SortByValue']) && ($FilterData['SortByValue'] == 'asc' || $FilterData['SortByValue'] == 'desc')){ 
                $OrderByString .= "ORDER BY ".$FilterData['SortByField']." ".$FilterData['SortByValue']."";
            }

            $query = "SELECT ct.ContactID, IF(ct.AssignedTo IS NOT NULL, CONCAT(at.FirstName, ' ', at.LastName), '') as AssignedTo, ct.ContactNo, ac.AccountName, CONCAT(ct.FirstName, ' ', ct.LastName) as ContactName, ct.MailingCity, ct.MailingState
            FROM Contact ct
            LEFT JOIN User at ON at.UserID = ct.AssignedTo
            LEFT JOIN User cb ON cb.UserID = ct.CreatedBy
            LEFT JOIN Account ac ON ac.AccountID = ct.Account
            LEFT JOIN LeadSource ls ON ls.LeadSourceID = ct.LeadSource
            LEFT JOIN Organization o ON o.OrganizationID = ct.OrganizationID
            WHERE $WhereString $OrderByString"; 
            $query = $this->db->query($query);
     
            API_Model::checkQuery($query); 

            return $query->result_array();
    }

    function ViewContactList($UserID, $OrganizationID, $ContactViewID)
    {    
        if($ContactViewID == 'MyActiveContacts' 
            || $ContactViewID == 'AllActiveContacts' 
            || $ContactViewID == 'ContactsCreatedThisWeek'
        ){

            return $this->GetDefaultContactData($UserID, $OrganizationID, $ContactViewID);

        } else {

            $data = array();
            $query = $this->db->get_where('ContactView', array('OrganizationID' => $OrganizationID, 'ContactViewID' => $ContactViewID)); 
            $ContactViewData = $query->row();

            if($query->num_rows() > 0){
                $query = $this->db->get_where('ContactFilter', array('ContactViewID' => $ContactViewID)); 
                $ContactFilterData = $query->result_array();

                $ColName = "ct.ContactID, ac.AccountID, ";
                if(!empty($ContactViewData->SpecifyFieldsDisplay)){
                    $SpecifyFieldsDisplays = explode(",", $ContactViewData->SpecifyFieldsDisplay);
                    foreach ($SpecifyFieldsDisplays as $key => $value) {
                        if($value == 'AssignedTo'){
                            $ColName .= "CONCAT(at.FirstName, ' ', at.LastName) as AssignedTo";
                        } else if($value == 'Account'){
                            $ColName .= "ac.AccountName as Account";
                        } else if($value == 'OrganizationID'){
                            $ColName .= "o.CompanyName as OrganizationID";
                        } else if($value == 'LeadSource'){
                            $ColName .= "ls.LeadSource";
                        } else if($value == 'CreatedBy'){
                            $ColName .= "CONCAT(cr.FirstName, ' ', cr.LastName) as CreatedBy";
                        } else if($value == 'LastModifiedBy'){
                            $ColName .= "CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedBy";
                        } else if($value == 'BirthDate'){
                            $ColName .= "DATE_FORMAT(ct.BirthDate, '".RES_DATE."') as BirthDate";
                        } else {
                            $ColName .= "ct.".$value;
                        }

                        if($key != count($SpecifyFieldsDisplays)){
                            $ColName .= ", ";
                        }
                    }
                }
                      
                /* Filter Contact Data Start */
                $this->db->select($ColName);
                $this->db->from('Contact ct');
                $this->db->join('User at','at.UserID = ct.AssignedTo', 'left');
                $this->db->join('Account ac','ac.AccountID = ct.Account', 'left');
                $this->db->join('Organization o','o.OrganizationID = ct.OrganizationID', 'left');
                $this->db->join('LeadSource ls','ls.LeadSourceID = ct.LeadSource', 'left');
                $this->db->join('User cr','cr.UserID = ct.CreatedBy', 'left');
                $this->db->join('User lmb','lmb.UserID = ct.LastModifiedBy', 'left');
                $this->db->where(array('ct.OrganizationID' => $OrganizationID, 'ct.IsDeleted' => 0, 'ct.IsActive' => 1));

                foreach ($ContactFilterData as $key => $value) {

                    $WhereField = "";
                    if($value['FilterField'] == 'AssignedTo'){
                        $WhereField = "CONCAT(at.FirstName, ' ', at.LastName)";
                    } else if($value['FilterField'] == 'Account') {
                        $WhereField = "ac.AccountName";
                    } else if($value['FilterField'] == 'OrganizationID') {
                        $WhereField = "o.CompanyName";
                    } else if($value['FilterField'] == 'LeadSource') {
                        $WhereField = "ls.LeadSource";
                    } else if($value['FilterField'] == 'CreatedBy') {
                        $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                    } else if($value['FilterField'] == 'LastModifiedBy') {
                        $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                    } else if($value['FilterField'] == 'BirthDate'){
                         $WhereField = "DATE_FORMAT(ct.BirthDate, '".RES_DATETIME."')";
                    } else {
                        $WhereField = "ct.".$value['FilterField'];
                    }

                    if($value['FilterCondition'] == 'Equals'){
                        $this->db->where($WhereField.' =',  $value['FilterValue']);  
                    } else if($value['FilterCondition'] == 'NotEqualTo'){
                        $this->db->where($WhereField.' !=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'LessThan'){
                        $this->db->where($WhereField.' <', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'GreaterThan'){
                        $this->db->where($WhereField.' >', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'LessOREqualTo'){
                        $this->db->where($WhereField.' <=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'GreaterOREqualTo'){
                        $this->db->where($WhereField.' >=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'Contains'){
                        $this->db->like($WhereField, $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'StartsWith'){ 
                        $this->db->like($WhereField, $value['FilterValue'], 'after');   
                    } else if($value['FilterCondition'] == 'DoesNotContain'){
                        $this->db->not_like($WhereField, $value['FilterValue']);   
                    } 
                } 
                if(!empty($ContactViewData->SortByField) && !empty($ContactViewData->SortByValue)){
                    $this->db->order_by($ContactViewData->SortByField, $ContactViewData->SortByValue);
                }
                $query = $this->db->get();  
                //echo  $this->db->last_query();exit;
                API_Model::checkQuery($query);

                $data = $query->result_array();
            } 

            return $data;

        }
    }

    function ContactFilter($UserID, $OrganizationID, $ContactViewID, $FilterFields, $FilterConditions, $FilterValues, $SortByField, $SortByValue)
    { 

        $FilterData = array(
            'FilterField' => $FilterFields,
            'FilterCondition' => $FilterConditions,
            'FilterValue' => $FilterValues,
            'SortByField' => $SortByField,
            'SortByValue' => $SortByValue,
        );
     
        return $this->GetDefaultContactData($UserID, $OrganizationID, $ContactViewID, $FilterData);
    }

    function GetContactViewFields()
    {
        $query = $this->db->select('FieldName')
        ->from('ContactViewFields')
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }
}

?>
