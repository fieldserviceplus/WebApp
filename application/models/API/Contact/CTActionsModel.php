<?php

require_once(APPPATH.'models/API/API_Model.php');

class CTActionsModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
  
    function CreateContact() {

        extract($_POST);

        $data = array(
            'OrganizationID' => $OrganizationID,
            'Title' => $Title,
            'Salutation' => $Salutation,
            'Name' => $FirstName.' '.$LastName,
            'FirstName' => $FirstName,
            'LastName' => $LastName,
            'Account' => $Account,
            'AssignedTo' => $AssignedTo,
            'MailingAddress' => $MailingAddress,
            'MailingCity' => $MailingCity,
            'MailingState' => $MailingState,
            'MailingPostalCode' => $MailingPostalCode,
            'MailingCountry' => $MailingCountry,
            'MailingLatitude' => $MailingLatitude,
            'MailingLongitude' => $MailingLongitude,
            'LeadSource' => $LeadSource,
            'Notes' => isset($Notes)?$Notes:NULL,
            'BirthDate' => isset($BirthDate)?date("Y-m-d",strtotime($BirthDate)):NULL,
            'Email' => isset($Email)?$Email:NULL,
            'PhoneNo' => isset($PhoneNo)?$PhoneNo:NULL,
            'MobileNo' => isset($MobileNo)?$MobileNo:NULL,
            'DoNotCall' => isset($DoNotCall)?$DoNotCall:0,
            'IsActive' => isset($IsActive)?$IsActive:1,
        );   
        $query = $this->db->insert('Contact', $data);  

        API_Model::checkQuery($query);  

        $ContactID = $this->db->insert_id();

        $ContactNo = API_Controller::genCode('CT',$ContactID);

        $query = $this->db->update('Contact', array('ContactNo'=>$ContactNo), array('ContactID' => $ContactID));   

        API_Model::checkQuery($query);  

        API_Model::insertCreatedModifiedByAndDt($UserID,'Contact', array('ContactID' => $ContactID));

        API_Model::insertCustomFieldData('Contact', $ContactID, $OrganizationID);

        $ResData['ContactID'] = $ContactID;
        $ResData['ContactNo'] = $ContactNo;
        return $ResData;
    }

    function RecentContacts($UserID, $OrganizationID)
    {   
        $query = $this->db->select('ct.ContactID, CONCAT(ct.FirstName, " ", ct.LastName) as ContactName, ct.PhoneNo, a.AccountName, ct.MailingCity, ct.MailingState, CONCAT(at.FirstName, " ", at.LastName) as AssignedTo')
        ->from('Contact ct')
        ->join('Account a','a.AccountID = ct.Account', 'left') 
        ->join('User at','at.UserID = ct.AssignedTo', 'left') 
        ->where(array('ct.OrganizationID' => $OrganizationID, 'ct.IsDeleted' => 0))
        ->order_by("ct.LastModifiedDate", "desc")
        ->get();  

        API_Model::checkQuery($query);  
        
        return $query->result();
    }

    function ContactDetails($UserID, $ContactID, $OrganizationID)
    { 
        $query = $this->db->select('ct.*, ac.AccountName, ls.LeadSource as LeadSourceName, CONCAT(ct.FirstName, " ", ct.LastName) as ContactName, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, DATE_FORMAT(ct.BirthDate, "'.RES_DATE.'") as BirthDate, DATE_FORMAT(ct.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,DATE_FORMAT(ct.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy')
        ->from('Contact ct')
        ->join('User cb','cb.UserID = ct.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = ct.LastModifiedBy', 'left')
        ->join('User at','at.UserID = ct.AssignedTo', 'left') 
        ->join('LeadSource ls','ls.LeadSourceID = ct.LeadSource', 'left') 
        ->join('Account ac','ac.AccountID = ct.Account', 'left')
        ->where(array('ct.OrganizationID' => $OrganizationID, 'ct.ContactID' => $ContactID)) //, 'ct.IsDeleted' => 0
        ->get();

        API_Model::checkQuery($query);

        $ContactDetails = $query->row_array();

        return $ContactDetails; 
    }

    function ContactRelatedList($UserID, $ContactID, $OrganizationID)
    { 
        /*$query = $this->db->select('Count(*) as AccountCnt')
        ->get_where('Account', array('OrganizationID' => $OrganizationID, 'PrimaryContact' => $ContactID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Account']['title'] = 'Accounts ('.$result->AccountCnt.')';*/

        $query = $this->db->select('Count(*) as WorkOrderCnt')
        ->get_where('WorkOrder', array('OrganizationID' => $OrganizationID, 'PrimaryContact' => $ContactID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['WorkOrder']['title'] = 'Work Orders ('.$result->WorkOrderCnt.')';

        $query = $this->db->select('Count(*) as EstimateCnt')
        ->get_where('Estimate', array('OrganizationID' => $OrganizationID, 'Contact' => $ContactID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Estimate']['title'] = 'Estimates ('.$result->EstimateCnt.')';

        $query = $this->db->select('Count(*) as InvoiceCnt')
        ->get_where('Invoice', array('OrganizationID' => $OrganizationID, 'Contact' => $ContactID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Invoice']['title'] = 'Invoices ('.$result->InvoiceCnt.')';
 
        $query = $this->db->select('Count(*) as EventCnt')
        ->get_where('Event', array('RelatedTo' => 'Contact', 'What' => $ContactID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Event']['title'] = 'Events ('.$result->EventCnt.')';

        $query = $this->db->select('Count(*) as TaskCnt')
        ->get_where('Task', array('RelatedTo' => 'Contact', 'What' => $ContactID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Task']['title'] = 'Tasks ('.$result->TaskCnt.')';

        $query = $this->db->select('Count(*) as FileCnt')
        ->get_where('File', array('OrganizationID' => $OrganizationID, 'RelatedTo' => 'Contact', 'What' => $ContactID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['File']['title'] = 'Files ('.$result->FileCnt.')';

        $this->db->select('Count(*) as NoteCnt');
        $query = $this->db->get_where('Note n', array('n.What' => $ContactID,'n.RelatedTo' => 'Contact', 'n.IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Note']['title'] = 'Notes ('.$result->NoteCnt.')';

        return $data;
    }

    /*function ContactRelatedAccount($UserID, $ContactID, $OrganizationID)
    { 
        $query = $this->db->select('a.AccountID, a.AccountNo, a.AccountName, at.AccountType, a.PhoneNo, a.BillingCity, a.BillingState')
        ->from('Account a')
        ->join('AccountType at','at.AccountTypeID = a.AccountType', 'left')
        ->where(array('a.OrganizationID' => $OrganizationID, 'a.PrimaryContact' => $ContactID, 'a.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }*/

    function ContactRelatedWorkOrder($UserID, $ContactID, $OrganizationID)
    { 
        $query = $this->db->select('wo.WorkOrderID, wo.WorkOrderNo, wo.Subject, wop.Priority, wos.Status, woc.CategoryName, DATE_FORMAT(wo.StartDate, "'.RES_DATETIME.'") as StartDate') 
        ->from('WorkOrder wo') 
        ->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left')
        ->join('WOCategory woc','woc.WOCategoryID = wo.WOCategory', 'left')
        ->join('WOPriority wop','wop.WOPriorityID = wo.WOPriority', 'left')
        ->where(array('wo.OrganizationID' => $OrganizationID, 'wo.PrimaryContact' => $ContactID, 'wo.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function ContactRelatedEstimate($UserID, $ContactID, $OrganizationID)
    { 
        $query = $this->db->select('et.EstimateID, et.EstimateNo, et.EstimateName, DATE_FORMAT(et.ExpirationDate, "'.RES_DATE.'") as ExpirationDate, CONCAT(o.FirstName, " ", o.LastName) as OwnerName, es.Status,  (Select SUM(TotalPrice) FROM EstimateLine WHERE Estimate=et.EstimateID) as GrandTotal, DATE_FORMAT(et.CreatedDate, "'.RES_DATETIME.'") as CreatedDate') 
        ->from('Estimate et') 
        ->join('User o','o.UserID = et.Owner', 'left')
        ->join('EstimateStatus es','es.EstimateStatusID = et.EstimateStatus', 'left')
        ->where(array('et.OrganizationID' => $OrganizationID, 'et.Contact' => $ContactID, 'et.IsDeleted' => 0))
        ->get(); 
 
        API_Model::checkQuery($query); 

        return $query->result_array();
    }
 
    function ContactRelatedInvoice($UserID, $ContactID, $OrganizationID)
    { 
        $query = $this->db->select('i.InvoiceID, i.InvoiceNo as InvoiceNumber, DATE_FORMAT(i.InvoiceDate, "'.RES_DATE.'") as DueDate, DATE_FORMAT(i.DueDate, "'.RES_DATE.'") as DueDate, is.InvoiceStatus, (Select SUM(TotalPrice) FROM InvoiceLineItem WHERE Invoice=i.InvoiceID) as TotalPrice, (Select SUM(SubTotal) FROM InvoiceLineItem WHERE Invoice=i.InvoiceID) as SubTotal, wo.Subject') 
        ->from('Invoice i') 
        ->join('InvoiceStatus is','is.InvoiceStatusID = i.InvoiceStatus', 'left')
        ->join('WorkOrder wo','wo.WorkOrderID = i.WorkOrder', 'left')
        ->where(array('i.OrganizationID' => $OrganizationID, 'i.Contact' => $ContactID, 'i.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function ContactRelatedNote($UserID, $ContactID)
    {  
        $this->db->select('n.NoteID, n.Subject, DATE_FORMAT(n.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, CONCAT(o.FirstName, " ", o.LastName) as OwnerName');
        $this->db->from('Note n'); 
        $this->db->join('User o','o.UserID = n.Owner', 'left');
        $this->db->where(array('n.What' => $ContactID,'n.RelatedTo' => 'Contact', 'n.IsDeleted' => 0));
        $query = $this->db->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function ContactRelatedTask($UserID, $ContactID, $OrganizationID)
    { 
        $this->db->select('t.TaskID, t.Subject, CONCAT(c.FirstName, " ", c.LastName) as Name, tt.TaskType, DATE_FORMAT(t.ActivityDate, "'.RES_DATE.'") as Date, CONCAT(u.FirstName, " ", u.LastName) as AssignedTo, ts.TaskStatus, tp.Priority');
        $this->db->from('Task t'); 
        $this->db->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left');
        $this->db->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left');
        $this->db->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left');
        $this->db->join('Contact c','c.ContactID = t.Who', 'left');
        $this->db->join('User u','u.UserID = t.AssignedTo', 'left');
        $this->db->group_start();
            $this->db->or_group_start();
                $this->db->where(array('t.OrganizationID' => $OrganizationID, 't.RelatedTo' => 'Contact', 't.What' => $ContactID, 't.IsDeleted' => 0));
            $this->db->group_end();
            $this->db->or_group_start();
                $this->db->where(array('t.OrganizationID' => $OrganizationID, 't.Who' => $ContactID, 't.IsDeleted' => 0));
            $this->db->group_end();
        $this->db->group_end() ;
        $query = $this->db->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function ContactRelatedEvent($UserID, $ContactID, $OrganizationID)
    { 
        $query = $this->db->select('e.EventID, e.Subject, CONCAT(c.FirstName, " ", c.LastName) as Name, DATE_FORMAT(e.EventStartDate, "'.RES_DATE.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATETIME.'") as EventEndDate, CONCAT(u.FirstName, " ", u.LastName) as AssignedTo, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, DATE_FORMAT(e.CreatedDate, "'.RES_DATETIME.'") as CreatedDate') 
        ->from('Event e') 
        ->join('Contact c','c.ContactID = e.Who', 'left')
        ->join('User u','u.UserID = e.AssignedTo', 'left')
        ->join('User cb','cb.UserID = e.CreatedBy', 'left')
        ->where(array('e.OrganizationID' => $OrganizationID, 'e.RelatedTo' => 'Contact', 'e.What' => $ContactID, 'e.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function ContactRelatedFile($UserID, $ContactID, $OrganizationID)
    {  
        $query = $this->db->select('f.FileID, f.FileName, f.Subject, f.ContentType')
        ->from('File f') 
        ->where(array('f.OrganizationID' => $OrganizationID, 'f.What' => $ContactID,'f.RelatedTo' => 'Contact', 'f.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function EditContact() {
                
        extract($_POST);

        $data = array(
            'OrganizationID' => $OrganizationID,
            'Title' => $Title,
            'Salutation' => $Salutation,
            'Name' => $FirstName.' '.$LastName,
            'FirstName' => $FirstName,
            'LastName' => $LastName,
            'Account' => $Account,
            'AssignedTo' => $AssignedTo,
            'MailingAddress' => $MailingAddress,
            'MailingCity' => $MailingCity,
            'MailingState' => $MailingState,
            'MailingPostalCode' => $MailingPostalCode,
            'MailingCountry' => $MailingCountry,
            'MailingLatitude' => $MailingLatitude,
            'MailingLongitude' => $MailingLongitude,
            'LeadSource' => $LeadSource,
            'Notes' => isset($Notes)?$Notes:NULL,
            'BirthDate' => isset($BirthDate)?date("Y-m-d",strtotime($BirthDate)):NULL,
            'Email' => isset($Email)?$Email:NULL,
            'PhoneNo' => isset($PhoneNo)?$PhoneNo:NULL,
            'MobileNo' => isset($MobileNo)?$MobileNo:NULL,
            'DoNotCall' => isset($DoNotCall)?$DoNotCall:0,
            'IsActive' => isset($IsActive)?$IsActive:1,
        );   
        $query = $this->db->update('Contact', $data, array('ContactID' => $ContactID));   

        API_Model::checkQuery($query);

        API_Model::updateCreatedModifiedByAndDt($UserID,'Contact', array('ContactID' => $ContactID));
    
        API_Model::updateCustomFieldData('Contact', $ContactID, $OrganizationID);
        
        return $query;
    }
}

?>
