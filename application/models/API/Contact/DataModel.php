<?php
require_once(APPPATH.'models/API/API_Model.php');

class DataModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function GetContacts($UserID, $OrganizationID)
    {  
        $query = $this->db->select('ContactID, CONCAT(FirstName," ",LastName) as FullName')
        ->from('Contact')
        ->where(array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetSalutations($UserID, $OrganizationID)
    {  
        $query = $this->db->select('SalutationID, Salutation')
        ->from('Salutation')
        ->where(array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetTitlesOfContact($UserID, $OrganizationID)
    {  
        $query = $this->db->select('TitleOfPeopleID, Title')
        ->from('TitleOfPeople')
        ->where(array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetLeadSources($UserID, $OrganizationID)
    {  
        $query = $this->db->select('LeadSourceID, LeadSource')
        ->from('LeadSource')
        ->where(array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }
}

?>
