<?php
require_once(APPPATH.'models/API/API_Model.php');

class ViewModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function GetViews($UserID, $OrganizationID)
    { 
        $FileDefaultView = array(
                array('FileViewID' => 'MyFiles', 'FileViewName' => 'My Files'),
                array('FileViewID' => 'AllFiles', 'FileViewName' => 'All Files'),
                array('FileViewID' => 'NewFilesCreatedThisWeek', 'FileViewName' => 'New Files Created This Week'),
            );

        $query = $this->db->select('FileViewID, FileViewName')
        ->from('FileView')
        ->where(array('OrganizationID' => $OrganizationID, 'UserID' => $UserID, 'RestrictVisibility'=>'VisibleOnlyToMe'))
        ->or_where(array('RestrictVisibility' => 'VisibleToEveryone'))
        ->order_by('FileViewName', 'asc')
        ->get();   

        API_Model::checkQuery($query); 
        
        return array_merge($FileDefaultView, $query->result());
    }

    function GetDefaultFileData($UserID, $OrganizationID, $FileViewID, $FilterData = array())
    {
            $WhereString = "";
            if($FileViewID == 'MyFiles'){

                $WhereString .= "f.AssignedTo = $UserID AND f.IsDeleted = 0 AND f.OrganizationID='$OrganizationID'";
            }
            else if($FileViewID == 'AllFiles'){

                $WhereString .= "f.IsDeleted=0 AND f.OrganizationID='$OrganizationID'";

            } else if($FileViewID == 'NewFilesCreatedThisWeek'){

                $WhereString .= "YEARWEEK(f.CreatedDate, 1) = YEARWEEK(CURDATE(), 1) AND f.OrganizationID='$OrganizationID' AND f.IsDeleted=0";

            } else {
                 $WhereString .= "f.OrganizationID='$OrganizationID' AND f.IsDeleted=0";
            }

            if(!empty($FilterData)){
                foreach ($FilterData['FilterField'] as $key => $value) {

                    $WhereField = "";
                    if($FilterData['FilterField'][$key] == 'AssignedTo'){
                        $WhereField = "CONCAT(at.FirstName, ' ', at.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'OrganizationID') {
                        $WhereField = "o.CompanyName";
                    } else if($FilterData['FilterField'][$key] == 'CreatedBy') {
                        $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'LastModifiedBy') {
                        $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                    } else {
                        $WhereField = "f.".$FilterData['FilterField'][$key];
                    }

                    if($FilterData['FilterCondition'][$key] == 'Equals'){
                         $WhereString .= " AND $WhereField = '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'NotEqualTo'){
                        $WhereString .= " AND $WhereField != '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'LessThan'){
                        $WhereString .= " AND $WhereField < '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'GreaterThan'){
                        $WhereString .= " AND $WhereField > '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'LessOREqualTo'){
                        $WhereString .= " AND $WhereField <= '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'GreaterOREqualTo'){
                        $WhereString .= " AND $WhereField >= '".$FilterData['FilterValue'][$key]."'";
                    } else if($FilterData['FilterCondition'][$key] == 'Contains'){
                        $WhereString .= " AND $WhereField LIKE '%".$FilterData['FilterValue'][$key]."%'";
                    } else if($FilterData['FilterCondition'][$key] == 'StartsWith'){ 
                        $WhereString .= " AND $WhereField LIKE '".$FilterData['FilterValue'][$key]."%'"; 
                    } else if($FilterData['FilterCondition'][$key] == 'DoesNotContain'){
                        $WhereString .= " AND $WhereField NOT LIKE '%".$FilterData['FilterValue'][$key]."%'";  
                    } 
                }
            }
            $OrderByString = "";
            if(isset($FilterData['SortByField']) && isset($FilterData['SortByValue']) && !empty($FilterData['SortByField']) && !empty($FilterData['SortByValue']) && ($FilterData['SortByValue'] == 'asc' || $FilterData['SortByValue'] == 'desc')){ 
                $OrderByString .= "ORDER BY ".$FilterData['SortByField']." ".$FilterData['SortByValue']."";
            }

            $query = "SELECT f.*, IF(f.AssignedTo IS NOT NULL, CONCAT(at.FirstName, ' ', at.LastName), '') as AssignedTo, f.Subject, DATE_FORMAT(f.CreatedDate, '".RES_DATETIME."') as CreatedDate, CONCAT(cb.FirstName, ' ', cb.LastName) as CreatedBy
            FROM File f
            LEFT JOIN User at ON at.UserID = f.AssignedTo
            LEFT JOIN User cb ON cb.UserID = f.CreatedBy
            WHERE $WhereString $OrderByString"; 
            $query = $this->db->query($query); 
     
            API_Model::checkQuery($query); 

            $result = $query->result();

            $RelatedData = API_Model::getRelatedData();

            $File = array();
            $FileData = array();
            foreach ($result as $key => $task) {

                    $RelatedDt = $RelatedData[$task->RelatedTo][$task->What];

                    $File['FileID'] = $task->FileID;
                    $File['AssignedTo'] = $task->AssignedTo;
                    $File['Subject'] = $task->Subject;
                    $File['RelatedTo'] = $RelatedDt->Name;
                    $File['CreatedDate'] = $task->CreatedDate;
                    $File['CreatedBy'] = $task->CreatedBy;

                    $FileData[] = $File;
            } 
            return $FileData;
    }

    function ViewFileList($UserID, $OrganizationID, $FileViewID)
    {    
        if($FileViewID == 'MyFiles' 
            || $FileViewID == 'AllFiles'
            || $FileViewID == 'NewFilesCreatedThisWeek'
        ){

            return $this->GetDefaultFileData($UserID, $OrganizationID, $FileViewID);

        } else {

            $data = array();
            $query = $this->db->get_where('FileView', array('OrganizationID' => $OrganizationID, 'FileViewID' => $FileViewID)); 
            $FileViewData = $query->row();

            if($query->num_rows() > 0){
                $query = $this->db->get_where('FileFilter', array('FileViewID' => $FileViewID)); 
                $FileFilterData = $query->result_array();

                $ColName = "f.FileID, f.What, ";
                if(!empty($FileViewData->SpecifyFieldsDisplay)){
                    $SpecifyFieldsDisplays = explode(",", $FileViewData->SpecifyFieldsDisplay);
                    foreach ($SpecifyFieldsDisplays as $key => $value) {
                        if($value == 'AssignedTo'){
                            $ColName .= "CONCAT(at.FirstName, ' ', at.LastName) as AssignedTo";
                        } else if($value == 'OrganizationID'){
                            $ColName .= "o.CompanyName as OrganizationID";
                        } else if($value == 'CreatedBy'){
                            $ColName .= "CONCAT(cr.FirstName, ' ', cr.LastName) as CreatedBy";
                        } else if($value == 'LastModifiedBy'){
                            $ColName .= "CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedBy";
                        } else {
                            $ColName .= "f.".$value;
                        }

                        if($key != count($SpecifyFieldsDisplays)){
                            $ColName .= ", ";
                        }
                    }
                }
                      
                /* Filter File Data Start */
                $this->db->select($ColName);
                $this->db->from('File f');
                $this->db->join('User at','at.UserID = f.AssignedTo', 'left');
                $this->db->join('Organization o','o.OrganizationID = f.OrganizationID', 'left');
                $this->db->join('User cr','cr.UserID = f.CreatedBy', 'left');
                $this->db->join('User lmb','lmb.UserID = f.LastModifiedBy', 'left');
                $this->db->where(array('f.OrganizationID' => $OrganizationID, 'f.IsDeleted' => 0));

                foreach ($FileFilterData as $key => $value) {

                    $WhereField = "";
                    if($value['FilterField'] == 'AssignedTo'){
                        $WhereField = "CONCAT(at.FirstName, ' ', at.LastName)";
                    } else if($value['FilterField'] == 'OrganizationID') {
                        $WhereField = "o.CompanyName";
                    } else if($value['FilterField'] == 'CreatedBy') {
                        $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                    } else if($value['FilterField'] == 'LastModifiedBy') {
                        $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                    } else {
                        $WhereField = "f.".$value['FilterField'];
                    }

                    if($value['FilterCondition'] == 'Equals'){
                        $this->db->where($WhereField.' =',  $value['FilterValue']);  
                    } else if($value['FilterCondition'] == 'NotEqualTo'){
                        $this->db->where($WhereField.' !=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'LessThan'){
                        $this->db->where($WhereField.' <', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'GreaterThan'){
                        $this->db->where($WhereField.' >', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'LessOREqualTo'){
                        $this->db->where($WhereField.' <=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'GreaterOREqualTo'){
                        $this->db->where($WhereField.' >=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'Contains'){
                        $this->db->like($WhereField, $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'StartsWith'){ 
                        $this->db->like($WhereField, $value['FilterValue'], 'after');   
                    } else if($value['FilterCondition'] == 'DoesNotContain'){
                        $this->db->not_like($WhereField, $value['FilterValue']);   
                    } 
                } 
                if(!empty($FileViewData->SortByField) && !empty($FileViewData->SortByValue)){
                    $this->db->order_by($FileViewData->SortByField, $FileViewData->SortByValue);
                }
                $query = $this->db->get();  
                //echo  $this->db->last_query();exit;
                API_Model::checkQuery($query);
                $result = $query->result_array();
                
                $RelatedData = API_Model::getRelatedData();

                $File = array();
                $FileData = array();
                $FinalData = array();
                foreach ($result as $key => $task) {
                    foreach ($task as $key => $taskdetails) { 
                        
                        $data = $task;
                        if(isset($task['RelatedTo']) && $task['What']){
                            $RelatedDt = $RelatedData[$task['RelatedTo']][$task['What']]; 
                            $data['RelatedTo']  = $RelatedDt->No;
                        }
                    } 
                    $FinalData[] = $data;
                } 
            }
            return $FinalData;

        }
    }

    function FileFilter($UserID, $OrganizationID, $FileViewID, $FilterFields, $FilterConditions, $FilterValues, $SortByField, $SortByValue)
    { 

        $FilterData = array(
            'FilterField' => $FilterFields,
            'FilterCondition' => $FilterConditions,
            'FilterValue' => $FilterValues,
            'SortByField' => $SortByField,
            'SortByValue' => $SortByValue,
        );
     
        return $this->GetDefaultFileData($UserID, $OrganizationID, $FileViewID, $FilterData);
    }

    function GetFileViewFields()
    {
        $query = $this->db->select('FieldName')
        ->from('FileViewFields')
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }
}

?>
