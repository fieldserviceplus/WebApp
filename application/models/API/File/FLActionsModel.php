<?php
require_once(APPPATH.'models/API/API_Model.php');

class FLActionsModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function CreateFile() {

        extract($_POST);

        $FILE_PATH = str_replace('{org_id}', $OrganizationID, FILE_PATH);

        $FileName = explode(".", $_FILES['FileName']['name'])[0];
        $config = array(
             'upload_path' => $FILE_PATH,
             'file_name' => $FileName.'_'.date('YmdHis'),
             'overwrite' => FALSE,
             'allowed_types' => "jpg|png|jpeg|doc|pptx|xlsx|docx|zip|pdf|rtf|txt",
        );
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('FileName')){
             return $this->upload->display_errors();
        } else {
            $fileData = $this->upload->data();
            chmod($FILE_PATH.$fileData['file_name'], 0777);
            
            $data = array(
                'OrganizationID' =>  $OrganizationID,
                'Subject' => $Subject,
                'RelatedTo' => $RelatedTo,
                'What' => $What,
                'AssignedTo' => $AssignedTo,
                'ContentType' => $fileData['file_type'],
                'Description' => $Description,
                'FileName' => $fileData['file_name'],
                'FileSize' => round($fileData['file_size']/1024, 2)
            ); 
            
            $query = $this->db->insert('File', $data); 
            $FileID = $this->db->insert_id();
            API_Model::checkQuery($query);  

            API_Model::insertCreatedModifiedByAndDt($UserID,'File', array('FileID' => $FileID));
        
            return array('FileID' => $FileID, 'Subject' => $Subject);
        } 
    }

    function RecentFiles($UserID, $OrganizationID)
    {   
        $query = $this->db->select('f.*, f.RelatedTo, f.What, f.Subject')
        ->from('File f')
        ->join('User at','at.UserID = f.AssignedTo', 'left') 
        ->where(array('f.OrganizationID' => $OrganizationID, 'f.IsDeleted' => 0))
        ->order_by("f.LastModifiedDate", "desc")
        ->get(); 

        API_Model::checkQuery($query);  

        $result = $query->result();
        $RelatedData = API_Model::getRelatedData();

        $File = array();
        foreach ($result as $key => $task) {

                $RelatedDt = $RelatedData[$task->RelatedTo][$task->What];

                $File['FileID'] = $task->FileID;
                $File['Subject'] = $task->Subject;
                $File['FileName'] = $task->FileName;
                $File['RelatedTo'] = $RelatedDt->Name;
                $File['Description'] = $task->Description;
                $File['ContentType'] = $task->ContentType;

                $FileData[] = $File;
        } 

        return $FileData;
    }

    function FileDetails($UserID, $FileID, $OrganizationID)
    { 
        $FILE_URL = str_replace('{org_id}', $OrganizationID, FILE_URL);
        
        $query = $this->db->select('f.*, f.FileID, f.RelatedTo, f.What, f.Subject, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, CONCAT("'.$FILE_URL.'", f.FileName) as FileURL, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy')
        ->from('File f')
        ->join('User at','at.UserID = f.AssignedTo', 'left') 
        ->join('User cb','cb.UserID = f.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = f.LastModifiedBy', 'left') 
        ->where(array('f.FileID' => $FileID, 'f.OrganizationID' => $OrganizationID)) //, 'f.IsDeleted' => 0
        ->get();  

        API_Model::checkQuery($query); 

        $result = $query->row_array();
        $RelatedData = API_Model::getRelatedData();
        $RelatedDt = $RelatedData[$result['RelatedTo']][$result['What']];
        $result["RelatedToName"] = $result['RelatedTo'];
        $result["RelatedTo"] = $RelatedDt->No.' - '.$RelatedDt->Name;
        $result["WhatName"] = $RelatedDt->Name;

        return $result; 
    }

    function EditFile() {
  
        extract($_POST);

        $FILE_PATH = str_replace('{org_id}', $OrganizationID, FILE_PATH);
 
        if(isset($_FILES['FileName']['name']) && !empty($_FILES['FileName']['name'])){ 
            $FileName = explode(".", $_FILES['FileName']['name'])[0];
            $config = array(
                 'upload_path' => $FILE_PATH,
                 'file_name' => $FileName.'_'.date('YmdHis'),
                 'overwrite' => FALSE,
                 'allowed_types' => "jpg|png|jpeg|doc|pptx|xlsx|docx|zip|pdf|rtf|txt",
            );
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($this->upload->do_upload('FileName')){
                $fileData = $this->upload->data();
                chmod($FILE_PATH.$fileData['file_name'], 0777);

                $query = $this->db->get_where('File', array('FileID' => $FileID)); 
                $FileData = $query->row();
                unlink($FILE_PATH.$FileData->FileName);
           
                $data = array(
                    'OrganizationID' =>  $OrganizationID,
                    'Subject' => $Subject,
                    'RelatedTo' => $RelatedTo,
                    'What' => $What,
                    'AssignedTo' => $AssignedTo,
                    'ContentType' => $fileData['file_type'],
                    'Description' => $Description,
                    'FileName' => $fileData['file_name'],
                    'FileSize' => round($fileData['file_size']/1024, 2)
                ); 
                
                $result = $this->db->update('File', $data, array('FileID' => $FileID));    
       
                API_Model::updateCreatedModifiedByAndDt($UserID,'File', array('FileID' => $FileID));

                return $result;
            } else { 
               return $this->upload->display_errors();
            } 
        } else {
            $data = array(
                'OrganizationID' =>  $OrganizationID,
                'Subject' => $Subject,
                'RelatedTo' => $RelatedTo,
                'What' => $What,
                'AssignedTo' => $AssignedTo,
                'Description' => $Description
            ); 
            
            $result = $this->db->update('File', $data, array('FileID' => $FileID));    
   
            API_Model::updateCreatedModifiedByAndDt($UserID,'File', array('FileID' => $FileID));
        
            return $result;
        }
    }
}

?>
