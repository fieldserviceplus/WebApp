<?php
require_once(APPPATH.'models/API/API_Model.php');

class ViewModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function GetViews($UserID, $OrganizationID)
    { 
        $TaskDefaultView = array(
                array('TaskViewID' => 'MyOpenTasksToday', 'TaskViewName' => 'My Open Tasks Today'),
                array('TaskViewID' => 'MyOpenTasksThisWeek', 'TaskViewName' => 'My Open Tasks This Week'),
                array('TaskViewID' => 'AllMyOpenTasks', 'TaskViewName' => 'All My Open Tasks'),
                array('TaskViewID' => 'MyCompletedTasksThisWeek', 'TaskViewName' => 'My Completed Tasks This Week'),
            );

        $query = $this->db->select('TaskViewID, TaskViewName')
        ->from('TaskView')
        ->where(array('OrganizationID' => $OrganizationID, 'UserID' => $UserID, 'RestrictVisibility'=>'VisibleOnlyToMe'))
        ->or_where(array('RestrictVisibility' => 'VisibleToEveryone'))
        ->order_by('TaskViewName', 'asc')
        ->get();   

        API_Model::checkQuery($query); 
        
        return array_merge($TaskDefaultView, $query->result());
    }

    function GetDefaultTaskData($UserID, $OrganizationID, $TaskViewID, $FilterData = array())
    {
            $WhereString = "";
            if($TaskViewID == 'MyOpenTasksToday'){

                $WhereString .= "t.AssignedTo = $UserID AND ts.IsClosed = 0 AND t.IsDeleted = 0 AND t.OrganizationID='$OrganizationID' AND DATE(t.CreatedDate) = CURDATE()";
            }
            else if($TaskViewID == 'MyOpenTasksThisWeek'){

                $WhereString .= "t.AssignedTo = $UserID AND t.IsDeleted=0 AND ts.IsClosed = 0 AND t.OrganizationID='$OrganizationID' AND YEARWEEK(t.CreatedDate, 1) = YEARWEEK(CURDATE(), 1)";

            } else if($TaskViewID == 'AllMyOpenTasks'){

                $WhereString .= "t.AssignedTo = $UserID AND ts.IsClosed=0 AND t.IsDeleted=0 AND t.OrganizationID='$OrganizationID'";

            } else if($TaskViewID == 'MyCompletedTasksThisWeek'){

                $WhereString .= "t.AssignedTo = $UserID AND YEARWEEK(t.CreatedDate, 1) = YEARWEEK(CURDATE(), 1) AND ts.TaskStatus='Completed' AND t.OrganizationID='$OrganizationID' AND t.IsDeleted=0";

            } else {
                 $WhereString .= "t.OrganizationID='$OrganizationID' AND t.IsDeleted=0";
            }

            if(!empty($FilterData)){
                foreach ($FilterData['FilterField'] as $key => $value) {

                    $WhereField = "";
                    if($FilterData['FilterField'][$key] == 'AssignedTo'){
                        $WhereField = "CONCAT(at.FirstName, ' ', at.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'Who') {
                        $WhereField = "c.ContactName";
                    } else if($FilterData['FilterField'][$key] == 'OrganizationID') {
                        $WhereField = "o.CompanyName";
                    } else if($FilterData['FilterField'][$key] == 'TaskStatus') {
                        $WhereField = "ts.TaskStatus";
                    } else if($FilterData['FilterField'][$key] == 'TaskPriority') {
                        $WhereField = "tp.Priority";
                    } else if($FilterData['FilterField'][$key] == 'TaskType') {
                        $WhereField = "tt.TaskType";
                    } else if($FilterData['FilterField'][$key] == 'CreatedBy') {
                        $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'LastModifiedBy') {
                        $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'DueDate'){
                         $WhereField = "DATE_FORMAT(t.DueDate, '".RES_DATETIME."')";
                    } else {
                        $WhereField = "t.".$FilterData['FilterField'][$key];
                    }

                    if($FilterData['FilterCondition'][$key] == 'Equals'){
                         $WhereString .= " AND $WhereField = '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'NotEqualTo'){
                        $WhereString .= " AND $WhereField != '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'LessThan'){
                        $WhereString .= " AND $WhereField < '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'GreaterThan'){
                        $WhereString .= " AND $WhereField > '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'LessOREqualTo'){
                        $WhereString .= " AND $WhereField <= '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'GreaterOREqualTo'){
                        $WhereString .= " AND $WhereField >= '".$FilterData['FilterValue'][$key]."'";
                    } else if($FilterData['FilterCondition'][$key] == 'Contains'){
                        $WhereString .= " AND $WhereField LIKE '%".$FilterData['FilterValue'][$key]."%'";
                    } else if($FilterData['FilterCondition'][$key] == 'StartsWith'){ 
                        $WhereString .= " AND $WhereField LIKE '".$FilterData['FilterValue'][$key]."%'"; 
                    } else if($FilterData['FilterCondition'][$key] == 'DoesNotContain'){
                        $WhereString .= " AND $WhereField NOT LIKE '%".$FilterData['FilterValue'][$key]."%'";  
                    } 
                }
            }
            $OrderByString = "";
            if(isset($FilterData['SortByField']) && isset($FilterData['SortByValue']) && !empty($FilterData['SortByField']) && !empty($FilterData['SortByValue']) && ($FilterData['SortByValue'] == 'asc' || $FilterData['SortByValue'] == 'desc')){ 
                $OrderByString .= "ORDER BY ".$FilterData['SortByField']." ".$FilterData['SortByValue']."";
            }

            $query = "SELECT t.*, IF(t.AssignedTo IS NOT NULL, CONCAT(at.FirstName, ' ', at.LastName), '') as AssignedTo, t.Subject, CONCAT(ct.FirstName, ' ', ct.LastName) as ContactName, ts.TaskStatus, tp.Priority as TaskPriority, tt.TaskType, DATE_FORMAT(t.DueDate, '".RES_DATE."') as DueDate, DATE_FORMAT(t.CreatedDate, '".RES_DATETIME."') as CreatedDate, CONCAT(cb.FirstName, ' ', cb.LastName) as CreatedBy
            FROM Task t
            LEFT JOIN User at ON at.UserID = t.AssignedTo
            LEFT JOIN User cb ON cb.UserID = t.CreatedBy
            LEFT JOIN Contact ct ON ct.ContactID = t.Who
            LEFT JOIN TaskStatus ts ON ts.TaskStatusID = t.TaskStatus
            LEFT JOIN TaskPriority tp ON tp.TaskPriorityID = t.TaskPriority
            LEFT JOIN TaskType tt ON tt.TaskTypeID = t.TaskType
            WHERE $WhereString $OrderByString"; 
            $query = $this->db->query($query); 
     
            API_Model::checkQuery($query); 

            $result = $query->result();

            $RelatedData = API_Model::getRelatedData();

            $Task = array();
            $TaskData = array();
            foreach ($result as $key => $task) {

                    $RelatedDt = $RelatedData[$task->RelatedTo][$task->What];

                    $Task['TaskID'] = $task->TaskID;
                    $Task['AssignedTo'] = $task->AssignedTo;
                    $Task['Subject'] = $task->Subject;
                    $Task['ContactName'] = $task->ContactName;
                    $Task['RelatedTo'] = $RelatedDt->Name;
                    $Task['DueDate'] = $task->DueDate;
                    $Task['TaskStatus'] = $task->TaskStatus;
                    $Task['TaskPriority'] = $task->TaskPriority;
                    $Task['TaskType'] = $task->TaskType;
                    $Task['CreatedDate'] = $task->CreatedDate;
                    $Task['CreatedBy'] = $task->CreatedBy;

                    $TaskData[] = $Task;
            } 
            return $TaskData;
    }

    function ViewTaskList($UserID, $OrganizationID, $TaskViewID)
    {    
        if($TaskViewID == 'MyOpenTasksToday' 
            || $TaskViewID == 'MyOpenTasksThisWeek'
            || $TaskViewID == 'AllMyOpenTasks'
            || $TaskViewID == 'MyCompletedTasksThisWeek'
        ){

            return $this->GetDefaultTaskData($UserID, $OrganizationID, $TaskViewID);

        } else {

            $data = array();
            $query = $this->db->get_where('TaskView', array('OrganizationID' => $OrganizationID, 'TaskViewID' => $TaskViewID)); 
            $TaskViewData = $query->row();

            if($query->num_rows() > 0){
                $query = $this->db->get_where('TaskFilter', array('TaskViewID' => $TaskViewID)); 
                $TaskFilterData = $query->result_array();

                $ColName = "t.TaskID, ct.ContactID, t.What, ";
                if(!empty($TaskViewData->SpecifyFieldsDisplay)){
                    $SpecifyFieldsDisplays = explode(",", $TaskViewData->SpecifyFieldsDisplay);
                    foreach ($SpecifyFieldsDisplays as $key => $value) {
                        if($value == 'AssignedTo'){
                            $ColName .= "CONCAT(at.FirstName, ' ', at.LastName) as AssignedTo";
                        } else if($value == 'Who'){
                            $ColName .= "CONCAT(ct.FirstName, ' ', ct.LastName) ContactName";
                        } else if($value == 'OrganizationID'){
                            $ColName .= "o.CompanyName as OrganizationID";
                        } else if($value == 'TaskStatus'){
                            $ColName .= "ts.TaskStatus";
                        } else if($value == 'TaskPriority'){
                            $ColName .= "tp.Priority as TaskPriority";
                        } else if($value == 'TaskType'){
                            $ColName .= "tt.TaskType";
                        } else if($value == 'CreatedBy'){
                            $ColName .= "CONCAT(cr.FirstName, ' ', cr.LastName) as CreatedBy";
                        } else if($value == 'LastModifiedBy'){
                            $ColName .= "CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedBy";
                        } else if($value == 'DueDate'){
                            $ColName .= "DATE_FORMAT(t.DueDate, '".RES_DATE."') as DueDate";
                        } else {
                            $ColName .= "t.".$value;
                        }

                        if($key != count($SpecifyFieldsDisplays)){
                            $ColName .= ", ";
                        }
                    }
                }
                      
                /* Filter Task Data Start */
                $this->db->select($ColName);
                $this->db->from('Task t');
                $this->db->join('User at','at.UserID = t.AssignedTo', 'left');
                $this->db->join('Contact ct','ct.ContactID = t.Who', 'left');
                $this->db->join('Organization o','o.OrganizationID = t.OrganizationID', 'left');
                $this->db->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left');
                $this->db->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left');
                $this->db->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left');
                $this->db->join('User cr','cr.UserID = t.CreatedBy', 'left');
                $this->db->join('User lmb','lmb.UserID = t.LastModifiedBy', 'left');
                $this->db->where(array('t.OrganizationID' => $OrganizationID, 't.IsDeleted' => 0));

                foreach ($TaskFilterData as $key => $value) {

                    $WhereField = "";
                    if($value['FilterField'] == 'AssignedTo'){
                        $WhereField = "CONCAT(at.FirstName, ' ', at.LastName)";
                    } else if($value['FilterField'] == 'Contact') {
                        $WhereField = "CONCAT(ct.FirstName, ' ', ct.LastName)";
                    } else if($value['FilterField'] == 'OrganizationID') {
                        $WhereField = "o.CompanyName";
                    } else if($value['FilterField'] == 'TaskStatus') {
                        $WhereField = "ts.TaskStatus";
                    } else if($value['FilterField'] == 'TaskPriority') {
                        $WhereField = "tp.Priority as TaskPriority";
                    } else if($value['FilterField'] == 'TaskType') {
                        $WhereField = "tt.TaskType";
                    } else if($value['FilterField'] == 'CreatedBy') {
                        $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                    } else if($value['FilterField'] == 'LastModifiedBy') {
                        $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                    } else if($value['FilterField'] == 'DueDate'){
                         $WhereField = "DATE_FORMAT(t.DueDate, '".RES_DATETIME."')";
                    } else {
                        $WhereField = "t.".$value['FilterField'];
                    }

                    if($value['FilterCondition'] == 'Equals'){
                        $this->db->where($WhereField.' =',  $value['FilterValue']);  
                    } else if($value['FilterCondition'] == 'NotEqualTo'){
                        $this->db->where($WhereField.' !=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'LessThan'){
                        $this->db->where($WhereField.' <', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'GreaterThan'){
                        $this->db->where($WhereField.' >', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'LessOREqualTo'){
                        $this->db->where($WhereField.' <=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'GreaterOREqualTo'){
                        $this->db->where($WhereField.' >=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'Contains'){
                        $this->db->like($WhereField, $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'StartsWith'){ 
                        $this->db->like($WhereField, $value['FilterValue'], 'after');   
                    } else if($value['FilterCondition'] == 'DoesNotContain'){
                        $this->db->not_like($WhereField, $value['FilterValue']);   
                    } 
                } 
                if(!empty($TaskViewData->SortByField) && !empty($TaskViewData->SortByValue)){
                    $this->db->order_by($TaskViewData->SortByField, $TaskViewData->SortByValue);
                }
                $query = $this->db->get();  
                //echo  $this->db->last_query();exit;
                API_Model::checkQuery($query);
                $result = $query->result_array();
                
                $RelatedData = API_Model::getRelatedData();

                $Task = array();
                $TaskData = array();
                $FinalData = array();
                foreach ($result as $key => $task) {
                    foreach ($task as $key => $taskdetails) { 
                        
                        $data = $task;
                        if(isset($task['RelatedTo']) && $task['What']){
                            $RelatedDt = $RelatedData[$task['RelatedTo']][$task['What']]; 
                            $data['RelatedTo']  = $RelatedDt->No;
                        }
                        $FinalData[] = $data;
                    } 
                } 
            }
            return $FinalData;

        }
    }

    function TaskFilter($UserID, $OrganizationID, $TaskViewID, $FilterFields, $FilterConditions, $FilterValues, $SortByField, $SortByValue)
    { 

        $FilterData = array(
            'FilterField' => $FilterFields,
            'FilterCondition' => $FilterConditions,
            'FilterValue' => $FilterValues,
            'SortByField' => $SortByField,
            'SortByValue' => $SortByValue,
        );
     
        return $this->GetDefaultTaskData($UserID, $OrganizationID, $TaskViewID, $FilterData);
    }

    function GetTaskViewFields()
    {
        $query = $this->db->select('FieldName')
        ->from('TaskViewFields')
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }
}

?>
