<?php
require_once(APPPATH.'models/API/API_Model.php');

class DataModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function GetRelatedObjList($UserID)
    {  
        $array[] = array('Name' => 'Account');
        $array[] = array('Name' => 'WorkOrder');
        $array[] = array('Name' => 'Estimate');
        $array[] = array('Name' => 'Invoice');
        $array[] = array('Name' => 'Contact');
        
        return $array;
    }

    function GetRelatedToList($UserID, $OrganizationID, $RelatedTo)
    {  
        $RelatedTo = array();
        $RelatedToList = array();
        if(isset($_POST['RelatedTo']) && $_POST['RelatedTo'] == 'Account'){

            $query = $this->db->get_where('Account', array('OrganizationID' => $OrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
            $Accounts = $query->result();
            foreach ($Accounts as $key => $Account) {
                $RelatedTo['ID'] = $Account->AccountID;
                $RelatedTo['Name'] = $Account->AccountName;
                $RelatedToList[] = $RelatedTo;
            }

        } else if(isset($_POST['RelatedTo']) && $_POST['RelatedTo'] == 'WorkOrder'){

            $query = $this->db->get_where('WorkOrder', array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0)); 
            $WorkOrders = $query->result();
            foreach ($WorkOrders as $key => $WorkOrder) {
                $RelatedTo['ID'] = $WorkOrder->WorkOrderID;
                $RelatedTo['Name'] = $WorkOrder->WorkOrderNo.' - '.$WorkOrder->Subject;
                $RelatedToList[] = $RelatedTo; 
            }
            
        } else if(isset($_POST['RelatedTo']) && $_POST['RelatedTo'] == 'Estimate'){

            $query = $this->db->get_where('Estimate', array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0)); 
            $Estimates = $query->result();
            foreach ($Estimates as $key => $Estimate) {
                $RelatedTo['ID'] = $Estimate->EstimateID;
                $RelatedTo['Name'] = $Estimate->EstimateName;
                $RelatedToList[] = $RelatedTo;
            }
            
        } else if(isset($_POST['RelatedTo']) && $_POST['RelatedTo'] == 'Invoice'){

            $query = $this->db->get_where('Invoice', array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0)); 
            $Invoices = $query->result();
            foreach ($Invoices as $key => $Invoice) {
                $RelatedTo['ID'] = $Invoice->InvoiceID;
                $RelatedTo['Name'] = $Invoice->InvoiceNo;
                $RelatedToList[] = $RelatedTo;
            }
            
        } else if(isset($_POST['RelatedTo']) && $_POST['RelatedTo'] == 'Contact'){

            $query = $this->db->get_where('Contact', array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0)); 
            $Contacts = $query->result();
            foreach ($Contacts as $key => $Contact) {
                $RelatedTo['ID'] = $Contact->ContactID;
                $RelatedTo['Name'] = $Contact->FirstName." ".$Contact->LastName;
                $RelatedToList[] = $RelatedTo;
            }
        }

        return $RelatedToList; 
    }

    function GetTaskStatus($UserID, $OrganizationID)
    {  
        $query = $this->db->select('TaskStatusID, TaskStatus, IsDefault, IsClosed')
        ->from('TaskStatus')
        ->where(array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetTaskPriority($UserID, $OrganizationID)
    {  
        $query = $this->db->select('TaskPriorityID, Priority')
        ->from('TaskPriority')
        ->where(array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetTaskType($UserID, $OrganizationID)
    {  
        $query = $this->db->select('TaskTypeID, TaskType')
        ->from('TaskType')
        ->where(array('OrganizationID' => $OrganizationID, 'IsDeleted' => 0))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }
}

?>
