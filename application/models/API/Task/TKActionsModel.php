<?php
require_once(APPPATH.'models/API/API_Model.php');

class TKActionsModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function CreateTask($UserID, $OrganizationID, $Subject, $RelatedTo, $What, $Who, $Description, $Email, $TaskStatus, $Phone, $TaskPriority, $TaskType, $IsRecurring, $StartOn, $RepeatEvery, $IntervalEvery, $Ends, $RepeatOn, $EndsOnDate, $EndsAfterOccurrences, $StartTime, $EndTime) {

        extract($_POST);

        $WORecurrenceID = NULL;   
        if(isset($IsRecurring) && $IsRecurring == 1){ 
            $data = array(
                'StartOn' => date("Y-m-d", strtotime($StartOn)),
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'Ends' => $Ends,
                'StartTime' => $StartTime,
                'EndTime' => $EndTime,
                'RepeatOn' => $RepeatOn,
                'EndsOnDate' => date("Y-m-d", strtotime($EndsOnDate)),
                'EndsAfterOccurrences' => $EndsAfterOccurrences,
                'CreatedDate' => date("Y-m-d H:i:s"),
                'LastModifiedDate' => date("Y-m-d H:i:s")
            );   
            $query = $this->db->insert('WORecurrence', $data);

            API_Model::checkQuery($query); 

            $WORecurrenceID = $this->db->insert_id();
        } 

        $data = array(
            'OrganizationID' => $OrganizationID,
            'Subject' => $Subject,
            'AssignedTo' => $AssignedTo,
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            'Who' => $Who,
            'Description' => $Description,
            'Email' => $Email,
            'TaskStatus' => $TaskStatus,
            'Phone' => $Phone,
            'TaskPriority' => $TaskPriority,
            'TaskType' => $TaskType,
            'IsRecurrence' => $IsRecurring,
            'RecurrenceID' => $WORecurrenceID,
            'DueDate' => date("Y-m-d H:i:s",strtotime($DueDate)),
        );   
        /*$query = $this->db->insert('Task', $data);  

        API_Model::checkQuery($query);  

        $TaskID = $this->db->insert_id();  

        API_Model::insertCreatedModifiedByAndDt($UserID,'Task', array('TaskID' => $TaskID));*/

        if(isset($IsRecurring) && $IsRecurring == 1 && isset($WORecurrenceID) && !empty($WORecurrenceID)){

            $RecurringTaskData = $this->CreateRecurringTask($WORecurrenceID, $data, $UserID);



        } else {

            $query = $this->db->insert('Task', $data);  

            API_Model::checkQuery($query);  

            $TaskID = $this->db->insert_id();

            API_Model::checkQuery($query);  

            API_Model::insertCreatedModifiedByAndDt($UserID,'Task', array('TaskID' => $TaskID));

            API_Model::insertCustomFieldData('Task', $TaskID, $OrganizationID);

            $RecurringTaskData = array('TaskID' => $TaskID);
        }
    
        $ResData['TaskID'] = $RecurringTaskData['TaskID'];
        $ResData['Subject'] = $Subject;
        return $ResData;
    }

    function RecentTasks($UserID, $OrganizationID)
    {   
        $query = $this->db->select('t.TaskID, t.RelatedTo, t.What, t.Subject, CONCAT(c.FirstName, " ", c.LastName) as ContactName, DATE_FORMAT(t.DueDate, "'.RES_DATE.'") as DueDate, ts.TaskStatus, tp.Priority, tt.TaskType')
        ->from('Task t')
        ->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left')
        ->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left')
        ->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left') 
        ->join('User at','at.UserID = t.AssignedTo', 'left') 
        ->join('Contact c','c.ContactID = t.Who', 'left') 
        ->where(array('t.OrganizationID' => $OrganizationID, 't.IsDeleted' => 0))
        ->order_by("t.LastModifiedDate", "desc")
        ->get(); 

        API_Model::checkQuery($query);  

        $result = $query->result();
        $RelatedData = API_Model::getRelatedData();

        $Task = array();
        foreach ($result as $key => $task) {

                $RelatedDt = $RelatedData[$task->RelatedTo][$task->What];

                $Task['TaskID'] = $task->TaskID;
                $Task['RelatedTo'] = $task->RelatedTo;
                $Task['Subject'] = $task->Subject;
                $Task['ContactName'] = $task->ContactName;
                $Task['DueDate'] = $task->DueDate;
                $Task['TaskStatus'] = $task->TaskStatus;
                $Task['TaskPriority'] = $task->Priority;
                $Task['TaskType'] = $task->TaskType;
                $Task['RelatedTo'] = $RelatedDt->Name;

                $TaskData[] = $Task;
        } 

        return $TaskData;
    }

    function TaskDetails($UserID, $TaskID, $OrganizationID)
    { 
        $query = $this->db->select('t.*, t.TaskID, t.RelatedTo, t.What, t.Subject, CONCAT(c.FirstName, " ", c.LastName) as ContactName, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, DATE_FORMAT(t.DueDate, "'.RES_DATE.'") as DueDate, ts.TaskStatus, tp.Priority as TaskPriority, tt.TaskType, ts.TaskStatusID, tt.TaskTypeID, tp.TaskPriorityID, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy')
        ->from('Task t')
        ->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left')
        ->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left')
        ->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left') 
        ->join('User at','at.UserID = t.AssignedTo', 'left') 
        ->join('Contact c','c.ContactID = t.Who', 'left') 
        ->join('User cb','cb.UserID = t.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = t.LastModifiedBy', 'left') 
        ->where(array('t.TaskID' => $TaskID, 't.OrganizationID' => $OrganizationID)) //, 't.IsDeleted' => 0
        ->get();  

        API_Model::checkQuery($query); 

        $result = $query->row_array();
        $RelatedData = API_Model::getRelatedData();
        $RelatedDt = $RelatedData[$result['RelatedTo']][$result['What']];
        $result["RelatedToName"] = $result['RelatedTo'];
        $result["RelatedTo"] = $RelatedDt->No.' - '.$RelatedDt->Name; 
        $result["WhatName"] = $RelatedDt->Name;

        return $result; 
    }

    function EditTask() {
                
        extract($_POST);

        $data = array(
            'OrganizationID' => $OrganizationID,
            'Subject' => $Subject,
            'AssignedTo' => $AssignedTo,
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            'Who' => $Who,
            'Description' => $Description,
            'Email' => $Email,
            'TaskStatus' => $TaskStatus,
            'Phone' => $Phone,
            'TaskPriority' => $TaskPriority,
            'TaskType' => $TaskType,
            'DueDate' => date("Y-m-d H:i:s",strtotime($DueDate)),
        );   
        $query = $this->db->update('Task', $data, array('TaskID' => $TaskID));   

        API_Model::checkQuery($query);

        API_Model::updateCreatedModifiedByAndDt($UserID,'Task', array('TaskID' => $TaskID));

        API_Model::updateCustomFieldData('Task', $TaskID, $OrganizationID);
    
        return $query;
    }

    function CreateRecurringTask($WORecurrenceID, $TaskData, $UserID)
    {
        $query = $this->db->get_where('WORecurrence', array('WORecurrenceID' => $WORecurrenceID));  
        $result = $query->row();

        $StartOnDate = date("Y-m-d", strtotime($result->StartOn));
        $EndsAfterOccurrences = isset($result->EndsAfterOccurrences) ? ($result->EndsAfterOccurrences * $result->IntervalEvery) : (100 * $result->IntervalEvery);

        if(isset($result->RepeatEvery) && ($result->RepeatEvery == "Daily" || $result->RepeatEvery == "Monthly" || $result->RepeatEvery == "Yearly")){

            if($result->RepeatEvery == "Daily"){
                $IntervalUnit = 'D'; 
                $IntervalType = 'day'; 
            } else if($result->RepeatEvery == "Monthly"){
                $IntervalUnit = 'M'; 
                $IntervalType = 'months'; 
            } else if($result->RepeatEvery == "Yearly"){
                $IntervalUnit = 'Y'; 
                $IntervalType = 'years'; 
            }

            if($result->Ends == 'On'){
                $EndsOnDate = date("Y-m-d", strtotime($result->EndsOnDate));
            } else{
                $EndsOnDate = date('Y-m-d', strtotime("+$EndsAfterOccurrences $IntervalType", strtotime($StartOnDate)));
            }

            $start = new DateTime( $StartOnDate );
            $end = new DateTime( $EndsOnDate );
            $interval = new DateInterval('P'.$result->IntervalEvery.''.$IntervalUnit.'');
            $flag = 1;
            $period = new DatePeriod($start, $interval, $end);
            foreach ($period as $date) {
                //print $date->format('Y-m-d H:i:s') . '<br/>';
                $DueDateTime = $date->format('Y-m-d').' '.$result->StartTime;
                $RecurringTaskData = $this->InsertRecurringTask($TaskData, $DueDateTime, $UserID);
                if($flag == 100)
                        break;
                $flag++;
            }

            return $RecurringTaskData;

        } else if(isset($result->RepeatEvery) && $result->RepeatEvery == "Weekly"){

            $RepeatOn = explode(",", $result->RepeatOn);
            if($result->Ends == 'On'){
                $EndsOnDate = date("Y-m-d", strtotime($result->EndsOnDate));
            } else{
                $EndsOnDate = date('Y-m-d', strtotime("+$EndsAfterOccurrences weeks", strtotime($StartOnDate)));
            }

            $start = new DateTime( $StartOnDate );
            $end = new DateTime( $EndsOnDate );
            $interval = new DateInterval('P1D');

            $period = new DatePeriod($start, $interval, $end);

            // only trigger every interval weeks...
            $weekInterval = $result->IntervalEvery;

            $fakeWeek = 0;
            $currentWeek = $start->format('W');
            $flag = 1;
            foreach ($period as $date) {

                if ($date->format('W') !== $currentWeek) {
                    $currentWeek = $date->format('W');
                    $fakeWeek++; 
                    //print ' WEEK ' . $currentWeek . '<br/>';
                }

                if ($fakeWeek % $weekInterval !== 0) {
                    continue;
                }

                $dayOfWeek = $date->format('D');

                if (in_array($dayOfWeek, $RepeatOn)) {
                    //print $date->format('Y-m-d H:i:s') . '   ' . $dayOfWeek . '<br/>';
                    $DueDateTime = $date->format('Y-m-d').' '.$result->StartTime;
                    $RecurringTaskData = $this->InsertRecurringTask($TaskData, $DueDateTime, $UserID);

                    if((isset($result->EndsAfterOccurrences) && $flag == $result->EndsAfterOccurrences) || ($flag == 100)){
                        break;
                    }
                    $flag++;
                }
            }

            return $RecurringTaskData;

        } /*else if(isset($result->RepeatEvery) && $result->RepeatEvery == "Periodically"){
            
        }*/
    }

    function InsertRecurringTask($TaskData, $DueDateTime, $UserID)
    {
        $data = array(
            'OrganizationID' => $TaskData['OrganizationID'],
            'Subject' => $TaskData['Subject'],
            'AssignedTo' => $TaskData['AssignedTo'],
            'RelatedTo' => $TaskData['RelatedTo'],
            'What' => $TaskData['What'],
            'Who' => $TaskData['Who'],
            'Description' => $TaskData['Description'],
            'Email' => $TaskData['Email'],
            'TaskStatus' => $TaskData['TaskStatus'],
            'Phone' => $TaskData['Phone'],
            'TaskPriority' => $TaskData['TaskPriority'],
            'TaskType' => $TaskData['TaskType'],
            'DueDate' => date("Y-m-d H:i:s",strtotime($TaskData['DueDate'])),
            'IsRecurrence' => $TaskData['IsRecurrence'],
            'RecurrenceID' => $TaskData['RecurrenceID']
        );   
        $query = $this->db->insert('Task', $data);  

        API_Model::checkQuery($query);  

        $TaskID = $this->db->insert_id();  

        API_Model::insertCreatedModifiedByAndDt($UserID,'Task', array('TaskID' => $TaskID));

        API_Model::insertCustomFieldData('Task', $TaskID, $TaskData['OrganizationID']); 

        return array('TaskID' => $TaskID);
    }

    function MarkATask() {
                
        extract($_POST);

        $query = $this->db->select('TaskStatusID')->from('TaskStatus')->where(array('TaskStatus' => 'Completed', 'OrganizationID' => $OrganizationID))->get();   

        $TaskStatus = $query->row();
        $TaskStatusID = $TaskStatus->TaskStatusID;

        $data = array(
            'TaskStatus' => $TaskStatusID,
        );   
        $query = $this->db->update('Task', $data, array('TaskID' => $TaskID, 'OrganizationID' => $OrganizationID));   
        //echo $this->db->last_query();exit;
        API_Model::checkQuery($query);

        API_Model::updateCreatedModifiedByAndDt($UserID,'Task', array('TaskID' => $TaskID));
    
        return $query;
    }
}

?>
