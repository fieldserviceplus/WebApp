<?php
require_once(APPPATH.'models/API/API_Model.php');

class ViewModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function GetViews($UserID, $OrganizationID)
    { 
        $WorkOrderDefaultView = array(
                array('WorkOrderViewID' => 'MyOpenWorkOrdersToday', 'WorkOrderViewName' => 'My Open Work Orders Today'),
                array('WorkOrderViewID' => 'MyOpenWorkOrdersThisWeek', 'WorkOrderViewName' => 'My Open Work Orders This Week'),
                array('WorkOrderViewID' => 'AllOpenWorkOrdersToday', 'WorkOrderViewName' => 'All Open Work Orders Today'),
                array('WorkOrderViewID' => 'AllOpenWorkOrdersThisWeek', 'WorkOrderViewName' => 'All Open Work Orders This Week'),
                array('WorkOrderViewID' => 'WorkOrdersCreatedToday', 'WorkOrderViewName' => 'Work Orders Created Today'),
                array('WorkOrderViewID' => 'WorkOrdersCreatedThisWeek', 'WorkOrderViewName' => 'Work Orders Created This Week'),
                array('WorkOrderViewID' => 'UnscheduledWorkOrders', 'WorkOrderViewName' => 'Unscheduled Work Orders'),
                array('WorkOrderViewID' => 'UnassignedWorkOrders', 'WorkOrderViewName' => 'Unassigned Work Orders')
            );

        $query = $this->db->select('WorkOrderViewID, WorkOrderViewName')
        ->from('WorkOrderView')
        ->where("OrganizationID = '".$OrganizationID."' AND ((UserID = '".$UserID."' AND RestrictVisibility = 'VisibleOnlyToMe') OR (RestrictVisibility = 'VisibleToEveryone'))")
        ->order_by('WorkOrderViewName', 'asc')
        ->get();   

        API_Model::checkQuery($query); 
        
        return array_merge($WorkOrderDefaultView, $query->result());
    }

    function GetDefaultWorkOrderData($OrganizationID, $UserID, $WorkOrderViewID, $FilterData = array())
    {
            $WhereString = "";
            if($WorkOrderViewID == 'MyOpenWorkOrdersToday'){

                $WhereString .= "w.AssignedTo = $UserID AND w.StartDate <= '".CURRENT_DATETIME."' AND w.EndDate >= '".CURRENT_DATETIME."' AND wos.IsClosed=0 AND w.IsDeleted=0 AND w.OrganizationID='".$OrganizationID."'";
            }
            else if($WorkOrderViewID == 'MyOpenWorkOrdersThisWeek'){

                $WhereString .= "w.AssignedTo = $UserID AND YEARWEEK(w.StartDate, 1) <= YEARWEEK(CURDATE(), 1) AND YEARWEEK(w.EndDate, 1) >= YEARWEEK(CURDATE(), 1) AND wos.IsClosed=0 AND w.IsDeleted=0 AND w.OrganizationID='".$OrganizationID."'";

            } else if($WorkOrderViewID == 'AllOpenWorkOrdersToday'){

                $WhereString .= "w.StartDate <= '".CURRENT_DATETIME."' AND w.EndDate >= '".CURRENT_DATETIME."' AND wos.IsClosed=0 AND w.IsDeleted=0 AND w.OrganizationID='".$OrganizationID."'";

            } else if($WorkOrderViewID == 'AllOpenWorkOrdersThisWeek'){

                $WhereString .= "YEARWEEK(w.StartDate, 1) <= YEARWEEK(CURDATE(), 1) AND YEARWEEK(w.EndDate, 1) >= YEARWEEK(CURDATE(), 1) AND wos.IsClosed=0 AND w.IsDeleted=0 AND w.OrganizationID='".$OrganizationID."'";

            } else if($WorkOrderViewID == 'WorkOrdersCreatedToday'){

                $WhereString .= "DATE(w.CreatedDate) = '".CURRENT_DATE."' AND w.IsDeleted=0 AND w.OrganizationID='".$OrganizationID."'";

            } else if($WorkOrderViewID == 'WorkOrdersCreatedThisWeek'){

                $WhereString .= "YEARWEEK(w.CreatedDate, 1) = YEARWEEK(CURDATE(), 1) AND w.IsDeleted=0 AND w.OrganizationID='".$OrganizationID."'";

            } else if($WorkOrderViewID == 'UnscheduledWorkOrders'){

                $WhereString .= "w.StartDate IS NULL AND wos.IsClosed=0 AND w.IsDeleted=0 AND w.OrganizationID='".$OrganizationID."'";

            } else if($WorkOrderViewID == 'UnassignedWorkOrders'){

                $WhereString .= "w.AssignedTo IS NULL AND w.StartDate IS NOT NULL AND wos.IsClosed=0 AND w.IsDeleted=0 AND w.OrganizationID='".$OrganizationID."'";

            } else {
                 $WhereString .= "w.IsDeleted=0 AND w.OrganizationID='".$OrganizationID."'";
            }

            if(!empty($FilterData)){
                foreach ($FilterData['FilterField'] as $key => $value) {

                    $WhereField = "";
                    if($FilterData['FilterField'][$key] == 'AssignedTo'){
                        $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'Account') {
                        $WhereField = "ac.AccountName";
                    } else if($FilterData['FilterField'][$key] == 'ParentWorkOrder') {
                        $WhereField = "pwo.Subject";
                    } else if($FilterData['FilterField'][$key] == 'OrganizationID') {
                        $WhereField = "o.CompanyName";
                    } else if($FilterData['FilterField'][$key] == 'WOPriority') {
                        $WhereField = "wop.Priority";
                    } else if($FilterData['FilterField'][$key] == 'WOCategory') {
                        $WhereField = "woc.CategoryName";
                    } else if($FilterData['FilterField'][$key] == 'WOStatus') {
                        $WhereField = "wos.Status";
                    } else if($FilterData['FilterField'][$key] == 'CreatedBy') {
                        $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'LastModifiedBy') {
                        $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'PrimaryContact') {
                        $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                    } else if($FilterData['FilterField'][$key] == 'WorkOrderType') {
                        $WhereField = "wot.WorkOrderType";
                    } else if($FilterData['FilterField'][$key] == 'StartDate'){
                         $WhereField = "DATE_FORMAT(w.StartDate, '".RES_DATETIME."')";
                    } else if($FilterData['FilterField'][$key] == 'EndDate'){
                         $WhereField = "DATE_FORMAT(w.EndDate, '".RES_DATETIME."')";
                    } else if($FilterData['FilterField'][$key] == 'StartTime'){
                         $WhereField = "DATE_FORMAT(w.StartTime, '".RES_TIME."')";
                    } else if($FilterData['FilterField'][$key] == 'EndTime'){
                         $WhereField = "DATE_FORMAT(w.EndTime, '".RES_TIME."')";
                    } else {
                        $WhereField = "w.".$FilterData['FilterField'][$key];
                    }

                    if($FilterData['FilterCondition'][$key] == 'Equals'){
                         $WhereString .= " AND $WhereField = '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'NotEqualTo'){
                        $WhereString .= " AND $WhereField != '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'LessThan'){
                        $WhereString .= " AND $WhereField < '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'GreaterThan'){
                        $WhereString .= " AND $WhereField > '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'LessOREqualTo'){
                        $WhereString .= " AND $WhereField <= '".$FilterData['FilterValue'][$key]."'";
                    }  else if($FilterData['FilterCondition'][$key] == 'GreaterOREqualTo'){
                        $WhereString .= " AND $WhereField >= '".$FilterData['FilterValue'][$key]."'";
                    } else if($FilterData['FilterCondition'][$key] == 'Contains'){
                        $WhereString .= " AND $WhereField LIKE '%".$FilterData['FilterValue'][$key]."%'";
                    } else if($FilterData['FilterCondition'][$key] == 'StartsWith'){ 
                        $WhereString .= " AND $WhereField LIKE '".$FilterData['FilterValue'][$key]."%'"; 
                    } else if($FilterData['FilterCondition'][$key] == 'DoesNotContain'){
                        $WhereString .= " AND $WhereField NOT LIKE '%".$FilterData['FilterValue'][$key]."%'";  
                    } 
                }
            }
            $OrderByString = "";
            if(isset($FilterData['SortByField']) && isset($FilterData['SortByValue']) && !empty($FilterData['SortByField']) && !empty($FilterData['SortByValue']) && ($FilterData['SortByValue'] == 'asc' || $FilterData['SortByValue'] == 'desc')){ 
                $OrderByString .= "ORDER BY ".$FilterData['SortByField']." ".$FilterData['SortByValue']."";
            }

            $query = "SELECT w.WorkOrderID, IF(w.AssignedTo IS NOT NULL, CONCAT(ast.FirstName, ' ', ast.LastName), '') as AssignedTo, w.WorkOrderNo, ac.AccountName, w.Subject, CONCAT(cnt.FirstName, ' ', cnt.LastName) as ContactName, wos.Status, wop.Priority, wot.WorkOrderType, w.City, w.State
            FROM WorkOrder w
            LEFT JOIN User ast ON ast.UserID = w.AssignedTo
            LEFT JOIN User cb ON cb.UserID = w.CreatedBy
            LEFT JOIN Account ac ON ac.AccountID = w.Account
            LEFT JOIN Contact cnt ON cnt.ContactID = w.PrimaryContact
            LEFT JOIN WOPriority wop ON wop.WOPriorityID = w.WOPriority 
            LEFT JOIN WorkOrderType wot ON wot.WorkOrderTypeID = w.WorkOrderType 
            LEFT JOIN WOStatus wos ON wos.WOStatusID = w.WOStatus
            WHERE $WhereString $OrderByString"; 
            $query = $this->db->query($query);
     
            API_Model::checkQuery($query); 

            return $query->result_array();
    }

    function ViewWorkOrderList($UserID, $WorkOrderViewID, $OrganizationID)
    {    
        if($WorkOrderViewID == 'MyOpenWorkOrdersToday' 
            || $WorkOrderViewID == 'MyOpenWorkOrdersThisWeek' 
            || $WorkOrderViewID == 'AllOpenWorkOrdersToday' 
            || $WorkOrderViewID == 'AllOpenWorkOrdersThisWeek' 
            || $WorkOrderViewID == 'WorkOrdersCreatedToday' 
            || $WorkOrderViewID == 'WorkOrdersCreatedThisWeek' 
            || $WorkOrderViewID == 'UnscheduledWorkOrders' 
            || $WorkOrderViewID == 'UnassignedWorkOrders'
        ){

            return $this->GetDefaultWorkOrderData($OrganizationID, $UserID, $WorkOrderViewID);

        } else {

            $data = array();
            $query = $this->db->get_where('WorkOrderView', array('WorkOrderViewID' => $WorkOrderViewID)); 
            $WorkOrderViewData = $query->row();

            if($query->num_rows() > 0){
                $query = $this->db->get_where('WorkOrderFilter', array('WorkOrderViewID' => $WorkOrderViewID)); 
                $WorkOrderFilterData = $query->result_array();

                $ColName = "w.WorkOrderID, ac.AccountID, ";
                if(!empty($WorkOrderViewData->SpecifyFieldsDisplay)){
                    $SpecifyFieldsDisplays = explode(",", $WorkOrderViewData->SpecifyFieldsDisplay);
                    foreach ($SpecifyFieldsDisplays as $key => $value) {
                        if($value == 'AssignedTo'){
                            $ColName .= "CONCAT(ast.FirstName, ' ', ast.LastName) as AssignedTo";
                        } else if($value == 'Account'){
                            $ColName .= "ac.AccountName as Account";
                        } else if($value == 'WorkOrderType'){
                            $ColName .= "wot.WorkOrderType";
                        } else if($value == 'ParentWorkOrder'){
                            $ColName .= "pwo.Subject as ParentWorkOrder";
                        } else if($value == 'OrganizationID'){
                            $ColName .= "o.CompanyName as OrganizationID";
                        } else if($value == 'WOPriority'){
                            $ColName .= "wop.Priority as WOPriority";
                        } else if($value == 'WOCategory'){
                            $ColName .= "woc.CategoryName as WOCategory";
                        } else if($value == 'WOStatus'){
                            $ColName .= "wos.Status as WOStatus";
                        } else if($value == 'PrimaryContact'){
                            $ColName .= "CONCAT(pc.FirstName, ' ', pc.LastName) as PrimaryContact";
                        } else if($value == 'CreatedBy'){
                            $ColName .= "CONCAT(cr.FirstName, ' ', cr.LastName) as CreatedBy";
                        } else if($value == 'LastModifiedBy'){
                            $ColName .= "CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedBy";
                        } else if($value == 'StartDate'){
                            $ColName .= "DATE_FORMAT(w.StartDate, '".RES_DATETIME."') as StartDate";
                        } else if($value == 'EndDate'){
                            $ColName .= "DATE_FORMAT(w.EndDate, '".RES_DATETIME."') as EndDate";
                        } else if($value == 'StartTime'){
                            $ColName .= "DATE_FORMAT(w.StartTime, '".RES_TIME."') as StartTime";
                        } else if($value == 'EndTime'){
                            $ColName .= "DATE_FORMAT(w.EndTime, '".RES_TIME."') as EndTime";
                        } else {
                            $ColName .= "w.".$value;
                        }

                        if($key != count($SpecifyFieldsDisplays)){
                            $ColName .= ", ";
                        }
                    }
                }
                      
                /* Filter WorkOrder Data Start */
                $this->db->select($ColName);
                $this->db->from('WorkOrder w');
                $this->db->join('User ast','ast.UserID = w.AssignedTo', 'left');
                $this->db->join('Account ac','ac.AccountID = w.Account', 'left');
                $this->db->join('WorkOrder pwo','pwo.WorkOrderID = w.ParentWorkOrder', 'left');
                $this->db->join('Organization o','o.OrganizationID = w.OrganizationID', 'left');
                $this->db->join('WOPriority wop','wop.WOPriorityID = w.WOPriority', 'left');
                $this->db->join('WOCategory woc','woc.WOCategoryID = w.WOCategory', 'left');
                $this->db->join('WOStatus wos','wos.WOStatusID = w.WOStatus', 'left');
                $this->db->join('User cr','cr.UserID = w.CreatedBy', 'left');
                $this->db->join('User lmb','lmb.UserID = w.LastModifiedBy', 'left');
                $this->db->join('Contact pc','pc.ContactID = w.PrimaryContact', 'left');
                $this->db->join('WorkOrderType wot','wot.WorkOrderTypeID = w.WorkOrderType', 'left');
                $this->db->where(array('w.IsDeleted' => 0, 'w.OrganizationID' => $OrganizationID));

                foreach ($WorkOrderFilterData as $key => $value) {

                    $WhereField = "";
                    if($value['FilterField'] == 'AssignedTo'){
                        $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                    } else if($value['FilterField'] == 'Account') {
                        $WhereField = "ac.AccountName";
                    } else if($value['FilterField'] == 'ParentWorkOrder') {
                        $WhereField = "pwo.Subject";
                    } else if($value['FilterField'] == 'OrganizationID') {
                        $WhereField = "o.CompanyName";
                    } else if($value['FilterField'] == 'WOPriority') {
                        $WhereField = "wop.Priority";
                    } else if($value['FilterField'] == 'WOCategory') {
                        $WhereField = "woc.CategoryName";
                    } else if($value['FilterField'] == 'WOStatus') {
                        $WhereField = "wos.Status";
                    } else if($value['FilterField'] == 'CreatedBy') {
                        $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                    } else if($value['FilterField'] == 'LastModifiedBy') {
                        $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                    } else if($value['FilterField'] == 'PrimaryContact') {
                        $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                    } else if($value['FilterField'] == 'WorkOrderType') {
                        $WhereField = "wot.WorkOrderType";
                    } else if($value['FilterField'] == 'StartDate'){
                         $WhereField = "DATE_FORMAT(w.StartDate, '".RES_DATETIME."')";
                    } else if($value['FilterField'] == 'EndDate'){
                         $WhereField = "DATE_FORMAT(w.EndDate, '".RES_DATETIME."')";
                    } else if($value['FilterField'] == 'StartTime'){
                         $WhereField = "DATE_FORMAT(w.StartTime, '".RES_TIME."')";
                    } else if($value['FilterField'] == 'EndTime'){
                         $WhereField = "DATE_FORMAT(w.EndTime, '".RES_TIME."')";
                    } else {
                        $WhereField = "w.".$value['FilterField'];
                    }

                    if($value['FilterCondition'] == 'Equals'){
                        $this->db->where($WhereField.' =',  $value['FilterValue']);  
                    } else if($value['FilterCondition'] == 'NotEqualTo'){
                        $this->db->where($WhereField.' !=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'LessThan'){
                        $this->db->where($WhereField.' <', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'GreaterThan'){
                        $this->db->where($WhereField.' >', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'LessOREqualTo'){
                        $this->db->where($WhereField.' <=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'GreaterOREqualTo'){
                        $this->db->where($WhereField.' >=', $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'Contains'){
                        $this->db->like($WhereField, $value['FilterValue']);   
                    } else if($value['FilterCondition'] == 'StartsWith'){ 
                        $this->db->like($WhereField, $value['FilterValue'], 'after');   
                    } else if($value['FilterCondition'] == 'DoesNotContain'){
                        $this->db->not_like($WhereField, $value['FilterValue']);   
                    } 
                }
                if(!empty($WorkOrderViewData->SortByField) && !empty($WorkOrderViewData->SortByValue)){
                    $this->db->order_by($WorkOrderViewData->SortByField, $WorkOrderViewData->SortByValue);
                }
                $query = $this->db->get();  

                API_Model::checkQuery($query);

                $data = $query->result_array();
            } 

            return $data;

        }
    }

    function WorkOrderFilter($UserID, $WorkOrderViewID, $FilterFields, $FilterConditions, $FilterValues, $SortByField, $SortByValue, $OrganizationID)
    { 

        $FilterData = array(
            'FilterField' => $FilterFields,
            'FilterCondition' => $FilterConditions,
            'FilterValue' => $FilterValues,
            'SortByField' => $SortByField,
            'SortByValue' => $SortByValue,
        );
     
        return $this->GetDefaultWorkOrderData($OrganizationID, $UserID, $WorkOrderViewID, $FilterData);
    }

    function GetWorkOrderViewFields($UserID, $OrganizationID)
    {
        $query = $this->db->select('FieldName')
        ->from('WorkOrderViewFields')
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }
}

?>
