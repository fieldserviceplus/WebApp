<?php

require_once(APPPATH.'models/API/API_Model.php');

class WoActionsModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function CreateWorkOrder($OrganizationID, $UserID, $ParentWorkOrder, $AssignedTo, $Account, $Subject, $Description, $Address, $City, $State, $Country, $PostalCode, $Latitude, $Longitude, $Signature, $PopUpReminder, $WorkOrderType, $WOStatus, $WOPriority, $WOCategory, $StartDate, $EndDate, $WOStartTime, $WOEndTime, $PrimaryContact, $IsRecurring, $StartOn, $RepeatEvery, $IntervalEvery, $Ends, $StartTime, $EndTime, $RepeatOn, $EndsOnDate, $EndsAfterOccurrences) {
        
        $WORecurrenceID = NULL;   
        if(isset($IsRecurring) && $IsRecurring == 1){ 
            $data = array(
                'StartOn' => date("Y-m-d", strtotime($StartOn)),
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'Ends' => $Ends,
                'StartTime' => $StartTime,
                'EndTime' => $EndTime,
                'RepeatOn' => $RepeatOn,
                'EndsOnDate' => date("Y-m-d", strtotime($EndsOnDate)),
                'EndsAfterOccurrences' => $EndsAfterOccurrences,
                'CreatedDate' => date("Y-m-d H:i:s"),
                'LastModifiedDate' => date("Y-m-d H:i:s")
            );   
            $query = $this->db->insert('WORecurrence', $data);

            API_Model::checkQuery($query); 

            $WORecurrenceID = $this->db->insert_id();
        } 
 
        $data = array(
            'OrganizationID' => $OrganizationID,
            'AssignedTo' => $AssignedTo,
            'ParentWorkOrder' => !empty($ParentWorkOrder)?$ParentWorkOrder:NULL,
            'Subject' => $Subject,
            'Account' => $Account,
            'Description' => $Description,
            'Address' => $Address,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => $Latitude,
            'Longitude' => $Longitude,
            'PopUpReminder' => $PopUpReminder,
            'WorkOrderType' => $WorkOrderType,
            'WOStatus' => $WOStatus,
            'WOPriority' => $WOPriority,
            'WOCategory' => $WOCategory,
            'PrimaryContact' => !empty($PrimaryContact)?$PrimaryContact:NULL,
            'IsRecurring' => $IsRecurring,
            'Signature' => $Signature,
            'StartDate' => date("Y-m-d H:i:s",strtotime($StartDate)),
            'EndDate' => date("Y-m-d H:i:s",strtotime($EndDate)),
            'StartTime' => date("H:i:s",strtotime($WOStartTime)),
            'EndTime' => date("H:i:s",strtotime($WOEndTime)),
            'WORecurrenceID' => !empty($WORecurrenceID)?$WORecurrenceID:NULL,
        );   
        
        if(isset($IsRecurring) && $IsRecurring == 1 && isset($WORecurrenceID) && !empty($WORecurrenceID)){

            $RecurringWorkOrderData = $this->CreateRecurringWorkOrder($OrganizationID, $WORecurrenceID, $data, $UserID, $_POST['CustomFieldIDs']);

        } else {

            $query = $this->db->insert('WorkOrder', $data);  

            API_Model::checkQuery($query);  

            $WorkOrderID = $this->db->insert_id();

            $WorkOrderNo = API_Controller::genCode('WO',$WorkOrderID);

            $query = $this->db->update('WorkOrder', array('WorkOrderNo'=>$WorkOrderNo), array('WorkOrderID' => $WorkOrderID));   
            API_Model::checkQuery($query);  

            API_Model::insertCreatedModifiedByAndDt($UserID,'WorkOrder', array('WorkOrderID' => $WorkOrderID));

            API_Model::insertCustomFieldData('WorkOrder', $WorkOrderID, $OrganizationID);

            $this->InsertRecurringEvent($OrganizationID, $WorkOrderID, $WorkOrderNo, $Subject, $AssignedTo, $StartDate, $EndDate, $UserID);

            $RecurringWorkOrderData = array('WorkOrderID' => $WorkOrderID, 'WorkOrderNo' => $WorkOrderNo);
        }
    
        $ResData['WorkOrderID'] = $RecurringWorkOrderData['WorkOrderID'];
        $ResData['WorkOrderNo'] = $RecurringWorkOrderData['WorkOrderNo'];
        return $ResData;
    }

    function CreateRecurringWorkOrder($OrganizationID, $WORecurrenceID, $WorkOrderData, $UserID, $CustomFieldIDs)
    {
        $query = $this->db->get_where('WORecurrence', array('WORecurrenceID' => $WORecurrenceID));  
        $result = $query->row();

        $StartOnDate = date("Y-m-d", strtotime($result->StartOn));
        $EndsAfterOccurrences = isset($result->EndsAfterOccurrences) ? ($result->EndsAfterOccurrences * $result->IntervalEvery) : (100 * $result->IntervalEvery);

        if(isset($result->RepeatEvery) && ($result->RepeatEvery == "Daily" || $result->RepeatEvery == "Monthly" || $result->RepeatEvery == "Yearly")){

            if($result->RepeatEvery == "Daily"){
                $IntervalUnit = 'D'; 
                $IntervalType = 'day'; 
            } else if($result->RepeatEvery == "Monthly"){
                $IntervalUnit = 'M'; 
                $IntervalType = 'months'; 
            } else if($result->RepeatEvery == "Yearly"){
                $IntervalUnit = 'Y'; 
                $IntervalType = 'years'; 
            }

            if($result->Ends == 'On'){
                $EndsOnDate = date("Y-m-d", strtotime($result->EndsOnDate));
            } else{
                $EndsOnDate = date('Y-m-d', strtotime("+$EndsAfterOccurrences $IntervalType", strtotime($StartOnDate)));
            }

            $start = new DateTime( $StartOnDate );
            $end = new DateTime( $EndsOnDate );
            $interval = new DateInterval('P'.$result->IntervalEvery.''.$IntervalUnit.'');
            $flag = 1;
            $period = new DatePeriod($start, $interval, $end);
            foreach ($period as $date) {
 
                //print $date->format('Y-m-d H:i:s') . '<br/>';
                $StartDateTime = $date->format('Y-m-d').' '.$result->StartTime;
                $EndDateTime = $date->format('Y-m-d').' '.$result->EndTime;
                $RecurringWorkOrderData = $this->InsertRecurringWorkOrder($OrganizationID, $WorkOrderData, $StartDateTime, $EndDateTime, $UserID, $CustomFieldIDs);
                $this->InsertRecurringEvent($OrganizationID, $RecurringWorkOrderData['WorkOrderID'], $RecurringWorkOrderData['WorkOrderNo'], $WorkOrderData['Subject'], $WorkOrderData['AssignedTo'], $StartDateTime, $EndDateTime, $UserID);
                
                if($flag == 100)
                        break;
                $flag++;
            }

            return $RecurringWorkOrderData;

        } else if(isset($result->RepeatEvery) && $result->RepeatEvery == "Weekly"){

            $RepeatOn = explode(",", $result->RepeatOn);
            if($result->Ends == 'On'){
                $EndsOnDate = date("Y-m-d", strtotime($result->EndsOnDate));
            } else{
                $EndsOnDate = date('Y-m-d', strtotime("+$EndsAfterOccurrences weeks", strtotime($StartOnDate)));
            }

            $start = new DateTime( $StartOnDate );
            $end = new DateTime( $EndsOnDate );
            $interval = new DateInterval('P1D');

            $period = new DatePeriod($start, $interval, $end);

            // only trigger every interval weeks...
            $weekInterval = $result->IntervalEvery;

            $fakeWeek = 0;
            $currentWeek = $start->format('W');
            $flag = 1;
            foreach ($period as $date) {

                if ($date->format('W') !== $currentWeek) {
                    $currentWeek = $date->format('W');
                    $fakeWeek++; 
                    //print ' WEEK ' . $currentWeek . '<br/>';
                }

                if ($fakeWeek % $weekInterval !== 0) {
                    continue;
                }

                $dayOfWeek = $date->format('D');

                if (in_array($dayOfWeek, $RepeatOn)) {
                    //print $date->format('Y-m-d H:i:s') . '   ' . $dayOfWeek . '<br/>';
                    $StartDateTime = $date->format('Y-m-d').' '.$result->StartTime;
                    $EndDateTime = $date->format('Y-m-d').' '.$result->EndTime;
                    //$this->CreateEvent($WorkOrderID, $WorkOrderNo, $Subject, $AssignedTo, $StartDateTime, $EndDateTime);
                    $RecurringWorkOrderData = $this->InsertRecurringWorkOrder($OrganizationID, $WorkOrderData, $StartDateTime, $EndDateTime, $UserID, $CustomFieldIDs);
                    $this->InsertRecurringEvent($OrganizationID, $RecurringWorkOrderData['WorkOrderID'], $RecurringWorkOrderData['WorkOrderNo'], $WorkOrderData['Subject'], $WorkOrderData['AssignedTo'], $StartDateTime, $EndDateTime, $UserID);

                    if((isset($result->EndsAfterOccurrences) && $flag == $result->EndsAfterOccurrences) || ($flag == 100)){
                        break;
                    }
                    $flag++;
                }
            }

            return $RecurringWorkOrderData;

        } /*else if(isset($result->RepeatEvery) && $result->RepeatEvery == "Periodically"){
            
        }*/
    }

    function InsertRecurringWorkOrder($OrganizationID, $WorkOrderData, $StartDateTime, $EndDateTime, $UserID, $CustomFieldIDs)
    {
        $data = array(
            'OrganizationID' => $OrganizationID,
            'AssignedTo' => $WorkOrderData['AssignedTo'],
            'Account' => $WorkOrderData['Account'],
            'Subject' => $WorkOrderData['Subject'],
            'Description' => $WorkOrderData['Description'],
            'Address' => $WorkOrderData['Address'],
            'City' => $WorkOrderData['City'],
            'State' => $WorkOrderData['State'],
            'Country' => $WorkOrderData['Country'],
            'PostalCode' => $WorkOrderData['PostalCode'],
            'Latitude' => $WorkOrderData['Latitude'],
            'Longitude' => $WorkOrderData['Longitude'],
            'WorkOrderType' => $WorkOrderData['WorkOrderType'],
            'WOStatus' => $WorkOrderData['WOStatus'],
            'WOPriority' => $WorkOrderData['WOPriority'],
            'WOCategory' => $WorkOrderData['WOCategory'],
            'StartDate' => date("Y-m-d H:i:s",strtotime($StartDateTime)),
            'EndDate' => date("Y-m-d H:i:s",strtotime($EndDateTime)),
            'StartTime' => date("H:i:s",strtotime($StartDateTime)),
            'EndTime' => date("H:i:s",strtotime($EndDateTime)),
            'PopUpReminder' => $WorkOrderData['PopUpReminder'],
            'PrimaryContact' => !empty($WorkOrderData['PrimaryContact'])?$WorkOrderData['PrimaryContact']:NULL,
            'WORecurrenceID' => $WorkOrderData['WORecurrenceID'],
            'IsRecurring' => $WorkOrderData['IsRecurring']
        );  
        $result = $this->db->insert('WorkOrder', $data);  
        //echo $this->db->last_query(); exit;
        $WorkOrderID = $this->db->insert_id(); 
        $WorkOrderNo = API_Controller::genCode('WO',$WorkOrderID);

        $this->db->update('WorkOrder', array('WorkOrderNo' => $WorkOrderNo), array('WorkOrderID' => $WorkOrderID)); 
     
        API_Model::insertCreatedModifiedByAndDt($UserID,'WorkOrder', array('WorkOrderID' => $WorkOrderID));

        $POST['CustomFieldIDs'] = $CustomFieldIDs;
        API_Model::insertCustomFieldData('WorkOrder', $WorkOrderID, $OrganizationID);
 
        return array('WorkOrderID' => $WorkOrderID, 'WorkOrderNo' => $WorkOrderNo);
    }

    function InsertRecurringEvent($OrganizationID, $WorkOrderID, $WorkOrderNo, $Subject, $AssignedTo, $StartDateTime, $EndDateTime, $UserID)
    {
        $data = array(
            'OrganizationID' => $OrganizationID,
            'Subject' => $WorkOrderNo.' - '.$Subject,
            'RelatedTo' => 'WorkOrder',
            'What' => $WorkOrderID,
            'AssignedTo' => $AssignedTo,
            'EventStartDate' => date("Y-m-d H:i:s", strtotime($StartDateTime)),
            'EventEndDate' => date("Y-m-d H:i:s", strtotime($EndDateTime)),
            'EventStartTime' => date("H:i:s", strtotime($StartDateTime)),
            'EventEndTime' => date("H:i:s", strtotime($EndDateTime))
        );
        $result = $this->db->insert('Event', $data);  
        //echo $this->db->last_query(); exit;
        $EventID = $this->db->insert_id();

        API_Model::insertCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));
    } 
 
    function RecentMyWorkOrders($UserID, $OrganizationID)
    {  
        $query = $this->db->select('wo.WorkOrderID, wo.WorkOrderNo, wo.Subject, wop.Priority, wos.Status')
        ->from('WorkOrder wo')
        ->join('WOPriority wop','wop.WOPriorityID = wo.WOPriority', 'left') 
        ->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left') 
        ->where(array('wo.OrganizationID' => $OrganizationID, 'wo.AssignedTo' => $UserID, 'wo.IsDeleted' => 0))
        ->get();  

        API_Model::checkQuery($query);  
        
        return $query->result();
    }
 
    function RecentAllWorkOrders($UserID, $OrganizationID)
    {
        $query = $this->db->select('wo.WorkOrderID, wo.WorkOrderNo, wo.Subject, wop.Priority, wos.Status')
        ->from('WorkOrder wo')
        ->join('WOPriority wop','wop.WOPriorityID = wo.WOPriority', 'left') 
        ->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left') 
        ->where(array('wo.OrganizationID' => $OrganizationID, 'wo.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query);   
        
        return $query->result();
    }

    function RecentWorkOrders($UserID, $OrganizationID)
    {  
        $query = $this->db->select('wo.WorkOrderID, wo.WorkOrderNo as WO#, wo.Subject, a.AccountName, wot.WorkOrderType as Type, wos.Status, wop.Priority, CONCAT(at.FirstName, " ", at.LastName) as AssignedTo')
        ->from('WorkOrder wo')
        ->join('WOPriority wop','wop.WOPriorityID = wo.WOPriority', 'left') 
        ->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left') 
        ->join('WorkOrderType wot','wot.WorkOrderTypeID = wo.WorkOrderType', 'left') 
        ->join('Account a','a.AccountID = wo.Account', 'left') 
        ->join('User at','at.UserID = wo.AssignedTo', 'left') 
        ->where(array('wo.OrganizationID' => $OrganizationID, 'wo.IsDeleted' => 0))
        ->order_by("wo.LastModifiedDate", "desc")
        ->get();  

        API_Model::checkQuery($query);  
        
        return $query->result();
    }

    function WorkOrderDetails($UserID, $WorkOrderID, $OrganizationID)
    { 
        $query = $this->db->select('wo.*, pwor.Subject as ParentWorkOrderName,ac.AccountName, CONCAT(pc.FirstName, " ", pc.LastName) as PrimaryContactName, IF(pc.MobileNo IS NOT NULL, pc.MobileNo, ac.PhoneNo) as MobileNo, wos.Status, woc.CategoryName, wot.WorkOrderType as WorkOrderTypeName, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, wor.*, wop.Priority, DATE_FORMAT(wo.StartDate, "'.RES_DATETIME.'") as StartDate, DATE_FORMAT(wo.EndDate, "'.RES_DATETIME.'") as EndDate, DATE_FORMAT(wo.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,DATE_FORMAT(wo.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy, org.CompanyName, org.Website, org.Phone as OrgPhone, org.BillingAddress as OrgBillingAddress, org.BillingCity as OrgBillingCity, org.BillingState as OrgBillingState,org.BillingCountry as OrgBillingCountry, org.BillingPostalCode as OrgBillingPostalCode, ac.PhoneNo as AcPhoneNo, org.Tax as TaxRate, org.Tax as Tax')
        ->from('WorkOrder wo')
        ->join('User cb','cb.UserID = wo.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = wo.LastModifiedBy', 'left')
        ->join('WOPriority wop','wop.WOPriorityID = wo.WOPriority', 'left')
        ->join('WORecurrence wor','wor.WORecurrenceID = wo.WORecurrenceID', 'left')
        ->join('WorkOrderType wot','wot.WorkOrderTypeID = wo.WorkOrderType', 'left')
        ->join('User at','at.UserID = wo.AssignedTo', 'left')
        ->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left')
        ->join('WOCategory woc','woc.WOCategoryID = wo.WOCategory', 'left')
        ->join('Contact pc','pc.ContactID = wo.PrimaryContact', 'left')
        ->join('Account ac','ac.AccountID = wo.Account', 'left')
        ->join('WorkOrder pwor','pwor.WorkOrderID = wo.ParentWorkOrder', 'left')
        ->join('Organization org','org.OrganizationID = wo.OrganizationID', 'left')
        ->where(array('wo.OrganizationID' => $OrganizationID, 'wo.WorkOrderID' => $WorkOrderID)) //, 'wo.IsDeleted' => 0
        ->get();

        API_Model::checkQuery($query); 
        $WorkOrderDetails = $query->row_array();

        $this->db->select('SUM(SubTotal) as SubTot, SUM(NetTotal) as NetTot, SUM(ListPrice) as ListTot, SUM(UnitPrice) as UnitTot, COUNT(*) as LineItemCnt');
        $this->db->from('WOLineItem');
        $this->db->where(array('OrganizationID' => $OrganizationID, 'WorkOrder' => $WorkOrderID, 'IsDeleted' => 0));
        $query = $this->db->get(); 
        $result = $query->row();
        if(!empty($result->SubTot) && !empty($result->NetTot) && !empty($result->ListTot) && !empty($result->UnitTot)){
            $Financials['SubTotal'] = $result->SubTot;
            $Financials['Discount'] = (100 - ($result->UnitTot / $result->ListTot) * 100);
            $Financials['Tax'] = ($result->NetTot - $result->SubTot);
            $Financials['TotalPrice'] = $result->ListTot;
            $Financials['GrandTotal'] = $result->NetTot;
            $Financials['LineItemCount'] = $result->LineItemCnt;
        } else {
            $Financials['SubTotal'] = 0.00;
            $Financials['Discount'] = 0.00;
            $Financials['Tax'] = 0.00;
            $Financials['TotalPrice'] = 0.00;
            $Financials['GrandTotal'] = 0.00;
            $Financials['LineItemCount'] = 0;
        }
     
        return array_merge($WorkOrderDetails, $Financials); 
    }

    function WorkOrderRelatedList($UserID, $WorkOrderID, $OrganizationID)
    { 
        $this->db->select('Count(*) as LineItemCnt');
        $query = $this->db->get_where('WOLineItem woli', array('woli.OrganizationID' => $OrganizationID, 'woli.WorkOrder' => $WorkOrderID, 'woli.IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['WOLineItem']['title'] = 'Work Order Line Items ('.$result->LineItemCnt.')';    

        $this->db->select('Count(*) as ChemicalCnt');
        $query = $this->db->get_where('Chemical c', array('c.OrganizationID' => $OrganizationID, 'c.WorkOrder' => $WorkOrderID, 'c.IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Chemical']['title'] = 'Chemicals ('.$result->ChemicalCnt.')'; 

        $this->db->select('Count(*) as InvoiceCnt');
        $query = $this->db->get_where('Invoice i', array('i.OrganizationID' => $OrganizationID, 'i.WorkOrder' => $WorkOrderID, 'i.IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Invoice']['title'] = 'Invoices ('.$result->InvoiceCnt.')';

        $query = $this->db->select('Count(*) as EventCnt')
        ->get_where('Event', array('OrganizationID' => $OrganizationID, 'RelatedTo' => 'WorkOrder', 'What' => $WorkOrderID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Event']['title'] = 'Events ('.$result->EventCnt.')';

        $query = $this->db->select('Count(*) as TaskCnt')
        ->get_where('Task', array('OrganizationID' => $OrganizationID, 'RelatedTo' => 'WorkOrder', 'What' => $WorkOrderID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Task']['title'] = 'Tasks ('.$result->TaskCnt.')';
 
        $this->db->select('Count(*) as NoteCnt');
        $query = $this->db->get_where('Note n', array('n.OrganizationID' => $OrganizationID, 'n.What' => $WorkOrderID,'n.RelatedTo' => 'WorkOrder', 'n.IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['Note']['title'] = 'Notes ('.$result->NoteCnt.')';

        $query = $this->db->select('Count(*) as FileCnt')
        ->get_where('File', array('OrganizationID' => $OrganizationID, 'RelatedTo' => 'WorkOrder', 'What' => $WorkOrderID, 'IsDeleted' => 0));
        API_Model::checkQuery($query); 
        $result = $query->row();
        $data['File']['title'] = 'Files ('.$result->FileCnt.')';

        return $data;
    }

    function WorkOrderRelatedLineItem($UserID, $WorkOrderID, $OrganizationID)
    {
        $query = $this->db->select('woli.*, p.ProductID, p.ProductName, p.IsListPriceEditable, p.IsQuantityEditable, org.Tax');
        $this->db->from('WOLineItem woli');
        $this->db->join('Product p','p.ProductID = woli.Product', 'left');
        $this->db->join('WorkOrder wo','wo.WorkOrderID = woli.WorkOrder', 'left');
        $this->db->join('Organization org','org.OrganizationID = woli.OrganizationID', 'left');
        $this->db->where(array('woli.WorkOrder' => $WorkOrderID, 'woli.OrganizationID' => $OrganizationID, 'woli.IsDeleted' => 0));
        $query = $this->db->get(); 

        API_Model::checkQuery($query);

        return $query->result_array();
    }

    function WorkOrderRelatedChemical($UserID, $WorkOrderID, $OrganizationID)
    {
        $query = $this->db->select('c.*, p.ProductID, p.ProductName, tum.UnitOfMeasurement as TestedUnitOfMeasure, aum.UnitOfMeasurement as ApplicationUnitOfMeasure');
        $this->db->from('Chemical c');
        $this->db->join('Product p','p.ProductID = c.Product', 'left');
        $this->db->join('UnitOfMeasurement tum','tum.UnitOfMeasurementID = c.TestedUnitOfMeasure', 'left');
        $this->db->join('UnitOfMeasurement aum','aum.UnitOfMeasurementID = c.ApplicationUnitOfMeasure', 'left');
        $this->db->where(array('c.WorkOrder' => $WorkOrderID, 'c.OrganizationID' => $OrganizationID, 'c.IsDeleted' => 0));
        $query = $this->db->get(); 

        API_Model::checkQuery($query);
        
        return $query->result_array();
    }

    function WorkOrderRelatedInvoice($UserID, $WorkOrderID, $OrganizationID)
    {
        $this->db->select('i.InvoiceID, i.InvoiceNo as InvoiceNumber, (Select SUM(TotalPrice) FROM InvoiceLineItem WHERE Invoice=i.InvoiceID) as TotalPrice, (Select SUM(SubTotal) FROM InvoiceLineItem WHERE Invoice=i.InvoiceID) as SubTotal, is.InvoiceStatus, DATE_FORMAT(i.InvoiceDate, "'.RES_DATE.'") as InvoiceDate, DATE_FORMAT(i.DueDate, "'.RES_DATE.'") as DueDate, wo.Subject as WorkOrderName');
        $this->db->from('Invoice i');
        $this->db->join('InvoiceStatus is','is.InvoiceStatusID = i.InvoiceStatus', 'left');
        $this->db->join('WorkOrder wo','wo.WorkOrderID = i.WorkOrder', 'left');
        $this->db->where(array('i.WorkOrder' => $WorkOrderID, 'i.OrganizationID' => $OrganizationID, 'i.IsDeleted' => 0));
        $query = $this->db->get();  

        API_Model::checkQuery($query);

        return $query->result_array();
    }

    function WorkOrderRelatedEvent($UserID, $WorkOrderID, $OrganizationID)
    { 
        $query = $this->db->select('e.EventID, e.Subject, es.EventStatus, et.EventTypeName, DATE_FORMAT(e.EventStartDate, "'.RES_DATE.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATE.'") as EventEndDate, DATE_FORMAT(e.EventStartTime, "'.RES_TIME.'") as EventStartTime, DATE_FORMAT(e.EventEndTime, "'.RES_TIME.'") as EventEndTime')
        ->from('Event e') 
        ->join('User cb','cb.UserID = e.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = e.LastModifiedBy', 'left')
        ->join('EventStatus es','es.EventStatusID = e.EventStatus', 'left')
        ->join('EventType et','et.EventTypeID = e.EventType', 'left')
        ->where(array('e.OrganizationID' => $OrganizationID, 'e.What' => $WorkOrderID,'e.RelatedTo' => 'WorkOrder', 'e.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function WorkOrderRelatedTask($UserID, $WorkOrderID, $OrganizationID)
    {  
        $query = $this->db->select('t.TaskID, t.Subject, tt.TaskType, tp.Priority, ts.TaskStatus')
        ->from('Task t') 
        ->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left')
        ->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left')
        ->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left')
        ->where(array('t.OrganizationID' => $OrganizationID, 't.What' => $WorkOrderID,'t.RelatedTo' => 'WorkOrder', 't.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function WorkOrderRelatedNote($UserID, $WorkOrderID, $OrganizationID)
    {  
        $this->db->select('n.NoteID, n.Subject, DATE_FORMAT(n.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, CONCAT(o.FirstName, " ", o.LastName) as OwnerName');
        $this->db->from('Note n'); 
        $this->db->join('User o','o.UserID = n.Owner', 'left');
        $this->db->where(array('n.OrganizationID' => $OrganizationID, 'n.What' => $WorkOrderID,'n.RelatedTo' => 'WorkOrder', 'n.IsDeleted' => 0));
        $query = $this->db->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function WorkOrderRelatedFile($UserID, $WorkOrderID, $OrganizationID)
    {  
        $query = $this->db->select('f.FileID, f.FileName, f.Subject, f.ContentType')
        ->from('File f') 
        ->where(array('f.OrganizationID' => $OrganizationID, 'f.What' => $WorkOrderID,'f.RelatedTo' => 'WorkOrder', 'f.IsDeleted' => 0))
        ->get(); 

        API_Model::checkQuery($query); 

        return $query->result_array();
    }

    function GetProducts($UserID, $OrganizationID)
    {
        extract($_POST);

        $Products = array();

        $query = $this->db->select('p.ProductID, p.ProductCode, p.ProductName, p.Description, p.ListPrice, p.DefaultQuantity, p.Taxable, p.IsListPriceEditable, p.IsQuantityEditable');
        $this->db->from('Product p');
        $this->db->where(array('p.OrganizationID' => $OrganizationID, 'p.IsDeleted' => 0, 'p.IsActive' => 1));
        if(isset($ProductFamily) && !empty($ProductFamily)){
            $this->db->where('ProductFamily',  $ProductFamily);  
        }
        if(isset($ProductCode) && !empty($ProductCode)){
            $this->db->like('ProductCode',  $ProductCode);  
        }
        $query = $this->db->get(); 
        $result = $query->result();

        API_Model::checkQuery($query);

        /*$query = $this->db->get_where('WorkOrder', array('WorkOrderID' => $WorkOrderID, 'OrganizationID' => $OrganizationID)); 
         $WorkOrderTax = $query->row();*/ 

        $query = $this->db->get_where('Organization', array('OrganizationID'=>$OrganizationID));  
        $OrganizationTax = $query->row();

        foreach ($result as $key => $value) {
            $Product = $value;
            $Product->Tax = $OrganizationTax->Tax;
            
            $Products[] = $Product;
            
        }

        return $Products;
    }

    function GetChemicals($UserID, $OrganizationID)
    {
        $this->db->select('p.ProductID, p.ProductCode, p.ProductName, p.Description, p.ListPrice, p.DefaultQuantity, p.Taxable');
        $this->db->from('Product p');
        $this->db->where(array('p.OrganizationID' => $OrganizationID, 'p.IsChemical' => 1, 'p.IsDeleted' => 0, 'p.IsActive' => 1));
        $query = $this->db->get(); 

        API_Model::checkQuery($query);

        return $query->result();
    }


    function EditWorkOrder($OrganizationID, $WorkOrderID, $ParentWorkOrder, $UserID, $Account, $AssignedTo, $Subject, $Description, $Address, $City, $State, $Country, $PostalCode, $Latitude, $Longitude, $PopUpReminder, $WorkOrderType, $WOStatus, $WOPriority, $PrimaryContact, $WOCategory, $Signature, $IsRecurring, $StartOn, $RepeatEvery, $IntervalEvery, $Ends, $StartTime, $EndTime, $RepeatOn, $EndsOnDate, $EndsAfterOccurrences, $WORecurrenceID) {
                
     
        if(isset($IsRecurring) && $IsRecurring == 1){ 

            if($WORecurrenceID != NULL){
                $data = array(
                    'StartOn' => date("Y-m-d", strtotime($StartOn)),
                    'RepeatEvery' => $RepeatEvery,
                    'IntervalEvery' => $IntervalEvery,
                    'RepeatOn' => ($RepeatEvery=='Weekly')?$RepeatOn:NULL, // implode(',', $RepeatOn)
                    'Ends' => $Ends,
                    'EndsOnDate' => ($Ends=='On')?date("Y-m-d",strtotime($EndsOnDate)):NULL,
                    'EndsAfterOccurrences' => ($Ends=='After')?$EndsAfterOccurrences:NULL,
                    'StartTime' => date("H:i:s",strtotime($StartTime)),
                    'EndTime' => date("H:i:s",strtotime($EndTime)),
                    'LastModifiedDate' => date("Y-m-d H:i:s")
                );   
                $query = $this->db->update('WORecurrence', $data, array('WORecurrenceID' => $WORecurrenceID));

                API_Model::checkQuery($query); 

            } else {
 
                 $data = array(
                    'StartOn' => date("Y-m-d", strtotime($StartOn)),
                    'RepeatEvery' => $RepeatEvery,
                    'IntervalEvery' => $IntervalEvery,
                    'RepeatOn' => ($RepeatEvery=='Weekly')?:NULL, //implode(',', $RepeatOn)
                    'Ends' => $Ends,
                    'EndsOnDate' => ($Ends=='On')?date("Y-m-d",strtotime($EndsOnDate)):NULL,
                    'EndsAfterOccurrences' => ($Ends=='After')?$EndsAfterOccurrences:NULL,
                    'StartTime' => date("H:i:s",strtotime($StartTime)),
                    'EndTime' => date("H:i:s",strtotime($EndTime)),
                    'CreatedDate' => date("Y-m-d H:i:s"),
                    'LastModifiedDate' => date("Y-m-d H:i:s")
                ); 
               
                $query = $this->db->insert('WORecurrence', $data);

                API_Model::checkQuery($query);

                $WORecurrenceID = $this->db->insert_id();
            }
        } else {
            if($WORecurrenceID != NULL){

                $this->db->delete('WORecurrence', array('WORecurrenceID' => $WORecurrenceID));
                $WORecurrenceID = NULL;   
            }
        } 
 
        $data = array(
            'AssignedTo' => $AssignedTo,
            'ParentWorkOrder' => !empty($ParentWorkOrder)?$ParentWorkOrder:NULL,
            'Account' => $Account,
            'Subject' => $Subject,
            'Description' => $Description,
            'Address' => $Address,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => $Latitude,
            'Longitude' => $Longitude,
            'PopUpReminder' => $PopUpReminder,
            'WorkOrderType' => $WorkOrderType,
            'WOStatus' => $WOStatus,
            'WOPriority' => $WOPriority,
            'PrimaryContact' => !empty($PrimaryContact)?$PrimaryContact:NULL,
            'IsRecurring' => $IsRecurring,
            'WORecurrenceID' => !empty($WORecurrenceID)?$WORecurrenceID:NULL,
            'WOCategory' => $WOCategory,
            'Signature' => $Signature,
        );   
        $query = $this->db->update('WorkOrder', $data, array('WorkOrderID' => $WorkOrderID, 'OrganizationID' => $OrganizationID));   

        API_Model::checkQuery($query);

        API_Model::updateCreatedModifiedByAndDt($UserID,'WorkOrder', array('WorkOrderID' => $WorkOrderID));

        API_Model::updateCustomFieldData('WorkOrder', $WorkOrderID, $OrganizationID);
    
        return 1;
    }

    function SaveWorkOrderSignature($UserID, $WorkOrderID, $Signature, $OrganizationID)
    {
        $data = array(
            'Signature' => $Signature
        );
        $query = $this->db->update('WorkOrder', $data, array('WorkOrderID' => $WorkOrderID, 'OrganizationID' => $OrganizationID));   

        API_Model::checkQuery($query);

        API_Model::updateCreatedModifiedByAndDt($UserID,'WorkOrder', array('WorkOrderID' => $WorkOrderID));
    
        return $query;
    }
 
    function SaveWorkorderLineItems()
    { 
        extract($_POST);

        $query = $this->db->delete('WOLineItem', array('WorkOrder' => $WorkOrderID, 'OrganizationID' => $OrganizationID));
        if(isset($Product) && count($Product) > 0 && is_array($Product)){ 

            foreach ($Product as $key => $value) {
               
               if(!empty($Product[$key])){
                    $data = array(
                        'OrganizationID' => $OrganizationID,
                        'WorkOrder' => $WorkOrderID,
                        'Product' => $Product[$key],
                        'ListPrice' => $ListPrice[$key],
                        'Discount' => $Discount[$key],
                        'UnitPrice' => $UnitPrice[$key],
                        'Quantity' => $Quantity[$key],
                        'SubTotal' => $SubTotal[$key],
                        'Taxable' => empty($Taxable[$key])?0:1,
                        'NetTotal' => $NetTotal[$key]
                    );     
                    $query = $this->db->insert('WOLineItem', $data);  
                    $WOLineItemID = $this->db->insert_id();

                    $LineItemNo = MY_Controller::genCode('WOLI' ,$WOLineItemID);
                    $this->db->update('WOLineItem', array('LineItemNo'=>$LineItemNo), array('WOLineItemID' => $WOLineItemID));  
           
                    API_Model::insertCreatedModifiedByAndDt($UserID,'WOLineItem', array('WOLineItemID' => $WOLineItemID));
                }
            }
        }

        return $query;
    }

    function GetProductFamily($OrganizationID)
    {
        $this->db->select('pf.ProductFamilyID, pf.ProductFamily');
        $this->db->from('ProductFamily pf');
        $this->db->where(array('pf.OrganizationID' => $OrganizationID, 'pf.IsDeleted' => 0));
        $query = $this->db->get(); 

        API_Model::checkQuery($query);

        return $query->result();
    }

    function GetUnitOfMeasurement($OrganizationID)
    {
        $this->db->select('uom.UnitOfMeasurementID, uom.UnitOfMeasurement');
        $this->db->from('UnitOfMeasurement uom');
        $this->db->where(array('uom.OrganizationID' => $OrganizationID, 'uom.IsDeleted' => 0));
        $query = $this->db->get(); 

        API_Model::checkQuery($query);

        return $query->result();
    }

    function SaveWorkorderChemicals()
    { 
        extract($_POST);

        $query = $this->db->delete('Chemical', array('WorkOrder' => $WorkOrderID, 'OrganizationID' => $OrganizationID));

        if(isset($Product) && count($Product) > 0 && is_array($Product)){ 

            foreach ($Product as $key => $value) {
               
               if(!empty($Product[$key]) && !empty($TestConcentration[$key]) && !empty($TestedUnitOfMeasure[$key]) && !empty($ApplicationAmount[$key]) && !empty($ApplicationUnitOfMeasure[$key])){
                    $data = array(
                        'OrganizationID' => $OrganizationID,
                        'WorkOrder' => $WorkOrderID,
                        'Account' => $Account,
                        'Owner' => $UserID,
                        'Product' => $Product[$key],
                        'TestConcentration' => $TestConcentration[$key],
                        'TestedUnitOfMeasure' => $TestedUnitOfMeasure[$key],
                        'ApplicationAmount' => $ApplicationAmount[$key],
                        'ApplicationUnitOfMeasure' => $ApplicationUnitOfMeasure[$key],
                        'ApplicationArea' => $ApplicationArea[$key],
                        'AdditionalNotes' => $AdditionalNotes[$key]
                    );     
                    $query = $this->db->insert('Chemical', $data);  
                    $ChemicalID = $this->db->insert_id();

                    $LineItemNo = MY_Controller::genCode('CH' ,$ChemicalID);
                    $this->db->update('Chemical', array('ChemicalNo'=>$LineItemNo), array('ChemicalID' => $ChemicalID));  
           
                    API_Model::insertCreatedModifiedByAndDt($UserID,'Chemical', array('ChemicalID' => $ChemicalID));
                }
            }
        }

        return $query;
    }

    function ConvertToInvoice()
    {
        extract($_POST);

        $query = $this->db->get_where('WorkOrder', array('OrganizationID' => $OrganizationID, 'WorkOrderID' => $WorkOrderID)); 
        
        if($query->num_rows() > 0){ 

            $result = $query->row(); 

            $data = array(
                'OrganizationID' => $OrganizationID,
                'AssignedTo' => $result->AssignedTo,
                'Account' => $result->Account,
                'WorkOrder' => $result->WorkOrderID,
                'Contact' => $result->PrimaryContact,
                'Address' => $result->Address,
                'City' => $result->City,
                'State' => $result->State,
                'Country' => $result->Country,
                'PostalCode' => $result->PostalCode,
                'Latitude' => $result->Latitude,
                'Longitude' => $result->Longitude,
                'InvoiceDate' => date("Y-m-d"),
                'DueDate' => date("Y-m-d"),
            );     
            $result = $this->db->insert('Invoice', $data); 
            $InvoiceID = $this->db->insert_id();

            $InvoiceNo = MY_Controller::genCode('IN', $InvoiceID);
            $this->db->update('Invoice', array('InvoiceNo' => $InvoiceNo), array('InvoiceID' => $InvoiceID));  

            MY_Model::insertCreatedModifiedByAndDt($UserID,'Invoice', array('InvoiceID' => $InvoiceID));

            $query = $this->db->get_where('WOLineItem', array('OrganizationID' => $OrganizationID, 'WorkOrder' => $WorkOrderID));
            if($query->num_rows() > 0){ 

                $LineItemsResult = $query->result(); 

                foreach ($LineItemsResult as $key => $LineItem) {
                    
                    $data = array(
                        'OrganizationID' => $OrganizationID,
                        'Invoice' => $InvoiceID,
                        'Product' => $LineItem->Product,
                        'ListPrice' => $LineItem->ListPrice,
                        'Discount' => $LineItem->Discount,
                        'UnitPrice' => $LineItem->UnitPrice,
                        'Quantity' => $LineItem->Quantity,
                        'SubTotal' => $LineItem->SubTotal,
                        'Taxable' => $LineItem->Taxable,
                        'TotalPrice' => $LineItem->NetTotal,
                    );     
                    $query = $this->db->insert('InvoiceLineItem', $data);  
                    $LineItemID = $this->db->insert_id();

                    $LineItemNo = MY_Controller::genCode('INL' ,$LineItemID);
                    $this->db->update('InvoiceLineItem', array('InvoiceLineNo'=>$LineItemNo), array('InvoiceLineItemID' => $LineItemID));  
           
                    MY_Model::insertCreatedModifiedByAndDt($UserID,'InvoiceLineItem', array('InvoiceLineItemID' => $LineItemID));
                }
            }

            return $InvoiceID;
        }

        return 0;
    }

    function GetTemplateContainsData($UserID, $WorkOrderID, $OrganizationID){

        $WorkOrderData = $this->WorkOrderDetails($UserID, $WorkOrderID, $OrganizationID);
        $WOLineItemsData = $this->WoActionsModel->WorkOrderRelatedLineItem($UserID, $WorkOrderID, $OrganizationID);
        
        $data = array();

        $data['AssignedTo'] = $WorkOrderData['AssignedTo'];
        $data['WorkOrderNo'] = $WorkOrderData['WorkOrderNo'];
        
        $company = array();
        $company["name"] = $WorkOrderData['CompanyName'];
        $company["street_address"] = $WorkOrderData['OrgBillingAddress'];
        $company["city_state_zip"] = $WorkOrderData['OrgBillingCity'].', '.$WorkOrderData['OrgBillingState'].', '.$WorkOrderData['OrgBillingCountry'].', '.$WorkOrderData['OrgBillingPostalCode'];
        $company["phone"] = $WorkOrderData['OrgPhone'];
        $company["fax"] = "";
        $company["website"] = $WorkOrderData['Website'];
        $data["company"] = $company;

       /* $host = array();
        $host["logo"] = dirname(__FILE__).'/logo.png';
        $data["host"] = $host;*/

        $wo = array();
        $wo["no"] = $WorkOrderData['WorkOrderNo'];
        $wo["date"] = date("m/d/Y", strtotime($WorkOrderData['StartDate']));
        $wo["subject"] = $WorkOrderData['Subject'];
        $wo["description"] = $WorkOrderData['Description'];
        $data["wo"] = $wo;

        $customer = array();
        $customer["name"] = "-";
        $customer["id"] = "-";
        $data["customer"] = $customer;

        $bill = array();
        $bill["name"] = "john jojo";
        $bill["company_name"] = $WorkOrderData['CompanyName'];
        $bill["street_address"] = $WorkOrderData['Address'];
        $bill["city_state_zip"] = $WorkOrderData['City'].','.$WorkOrderData['State'].','.$WorkOrderData['Country'].','.$WorkOrderData['PostalCode'];
        $bill["phone"] = $WorkOrderData['AcPhoneNo'];
        $data["bill"] = $bill;

        $items = array();
        $taxable = 0;

        foreach ($WOLineItemsData as $key => $WOLineItem) {
            
            $item = array();
            $item["qty"] = $WOLineItem['Quantity'];
            $item["description"] = $WOLineItem['ProductName'];
            $item["tax"] = ($WOLineItem['Taxable'] == 1) ? "Y" : "N";
            $item["price"] = number_format((double)$WOLineItem['UnitPrice'], 2, '.', ',');
            $item["line_total"] = number_format((double)$WOLineItem['NetTotal'], 2, '.', ',');
            $items[] = $item;

            $taxable += ($WOLineItem['Taxable'] == 1) ? $WOLineItem['NetTotal'] : 0;
        } 
        $data["items"] = $items;

        $invoice = array();
        $invoice["subtotal"] = number_format($WorkOrderData['SubTotal'], 2, '.', ',');
        $invoice["taxable"] = number_format($taxable, 2, '.', ',');
        $invoice["tax_rate"] = $WorkOrderData['TaxRate'].'%';
        $invoice["tax"] = number_format((double)$WorkOrderData['Tax'], 2, '.', ',');
        $invoice["s_h"] = "-";
        $invoice["other"] = "-";
        $invoice["total"] = number_format((double)$WorkOrderData['GrandTotal'], 2, '.', ',');
        $data["invoice"] = $invoice;

        return $data; 
    }
}

?>
