<?php
require_once(APPPATH.'models/API/API_Model.php');

class DataModel extends CI_Model {
 
    function __construct() {
        parent::__construct();
    }
 
    function GetWorkOrderTypes($UserID, $OrganizationID)
    { 
        $query = $this->db->select('WorkOrderTypeID, WorkOrderType, IsDefault')
        ->from('WorkOrderType')
        ->where(array('IsDeleted' => 0, 'OrganizationID' => $OrganizationID))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetWorkOrderPriorities($UserID, $OrganizationID)
    { 
        $query = $this->db->select('WOPriorityID, Priority')
        ->from('WOPriority')
        ->where(array('IsDeleted' => 0, 'OrganizationID' => $OrganizationID))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetWorkOrderStatus($UserID, $OrganizationID)
    { 
        $query = $this->db->select('WOStatusID, Status, IsDefault, IsClosed, IsCompleted')
        ->from('WOStatus')
        ->where(array('IsDeleted' => 0, 'OrganizationID' => $OrganizationID))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetWorkOrderCategories($UserID, $OrganizationID)
    { 
        $query = $this->db->select('WOCategoryID, CategoryName')
        ->from('WOCategory')
        ->where(array('IsDeleted' => 0, 'OrganizationID' => $OrganizationID))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }

    function GetParentWorkOrders($UserID, $OrganizationID)
    { 
        $query = $this->db->select('WorkOrderID, CONCAT(WorkOrderNo, " - ", Subject) AS Subject')
        ->from('WorkOrder')
        ->where(array('IsDeleted' => 0, 'OrganizationID' => $OrganizationID))
        ->get();   

        API_Model::checkQuery($query); 
        
        return $query->result();
    }
}

?>
