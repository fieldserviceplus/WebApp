<?php

class ViewModel extends MY_Model{
 
    public $LoginUserID, $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }
 
    function ViewEstimates() 
    {
        $aColumns = array('e.EstimateID','e.EstimateNo', 'e.EstimateName', 'ac.AccountName', 'CONCAT(o.FirstName, " ", o.LastName)', 'es.Status','(Select SUM(TotalPrice) FROM EstimateLine WHERE Estimate=e.EstimateID)', 'e.BillingCity', 'e.BillingState', 'DATE_FORMAT(e.CreatedDate, "'.RES_DATETIME.'")', 'CONCAT(cb.FirstName, " ", cb.LastName)', 'e.Account', 'e.Owner', 'e.CreatedBy'); 
        $bColumns = array('EstimateID','EstimateNo', 'EstimateName', 'AccountName', 'CONCAT(o.FirstName, " ", o.LastName)','Status', '(Select SUM(TotalPrice) FROM EstimateLine WHERE Estimate=e.EstimateID)', 'BillingCity', 'BillingState', 'DATE_FORMAT(e.CreatedDate, "'.RES_DATETIME.'")', 'CONCAT(cb.FirstName, " ", cb.LastName)', 'Account', 'Owner', 'CreatedBy'); 
      
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "e.EstimateID";

        /* DB table to use */
        $sTable = "Estimate e
        LEFT JOIN User o ON o.UserID = e.Owner
        LEFT JOIN User cb ON cb.UserID = e.CreatedBy
        LEFT JOIN Account ac ON ac.AccountID = e.Account
        LEFT JOIN EstimateStatus es ON es.EstimateStatusID = e.EstimateStatus";

        /* Limit */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".( $_GET['iDisplayStart'] ).", ".
                        ( $_GET['iDisplayLength'] );
        }

        /* Ordering */
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                    {
                            $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                                    ".( $_GET['sSortDir_'.$i] ) .", ";
                    }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                    $sOrder = "";
            }
        }

        /* Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited */
        $sWhere = "";
        if ( $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                        $sWhere .= $aColumns[$i]." LIKE '%".( $_GET['sSearch'] )."%' OR ";
                }
                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ')';
        }
        
        /* Where Condition */
        $UserID = $this->session->userdata('UserID');
        if(isset($_GET['flag']) && $_GET['flag'] == 'MyOpenEstimates'){

            $where_str = "e.Owner = $UserID AND es.IsClosed=0 AND e.OrganizationID='".$this->LoginOrganizationID."' AND e.IsDeleted=0";
        }
        else if(isset($_GET['flag']) && $_GET['flag'] == 'AllOpenEstimates'){

            $where_str = "es.IsClosed=0 AND e.OrganizationID='".$this->LoginOrganizationID."' AND e.IsDeleted=0";

        } else if(isset($_GET['flag']) && $_GET['flag'] == 'EstimatesCreatedThisWeek'){

            $where_str = "YEARWEEK(e.CreatedDate, 1) = YEARWEEK(CURDATE(), 1) AND e.OrganizationID='".$this->LoginOrganizationID."' AND e.IsDeleted=0";

        } else {
            $where_str = "e.IsDeleted=0 AND e.OrganizationID='".$this->LoginOrganizationID."'";
        }
        
        if ( $sWhere == "" )
        {
                $sWhere = "WHERE $where_str";
        }
        else
        {
                $sWhere .= "AND $where_str";
        }
        
        /* SQL queries Get data to display */
        $sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit"; 
        //echo $sQuery;exit;
        $rResult = $this->db->query($sQuery); 
        $rResult = $rResult->result_array(); 
      
        /* Data set length after filtering */
        $sQuery = "SELECT FOUND_ROWS() as cnt";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iFilteredTotal = $result->cnt;

        /* Total data set length */
        $sQuery = "SELECT COUNT(".$sIndexColumn.") as cnt
                FROM   $sTable  $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iTotal = $result->cnt;

        /* Output */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );

        foreach ( $rResult as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($bColumns) ; $i++ )
            {
                if($i == 0)
                {
                    $row[] = '<a href="'.SITE_URL.'Estimate/Actions/EditEstimate/'.$aRow[$bColumns[0]].'"><i class="fa fa-pencil-square-o"></i></a>';
                } 
                else if($i == 4)
                {
                     $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['Owner'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($i == 10)
                {
                     $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['CreatedBy'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($i == 3)
                {
                     $row[] = '<a href="'.SITE_URL.'Account/Actions/AccountDetails/'.$aRow['Account'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($bColumns[$i] == 'EstimateName' || $bColumns[$i] == 'EstimateNo')
                {
                    $row[] = '<a href="'.SITE_URL.'Estimate/Actions/EstimateDetails/'.$aRow[$bColumns[0]].'">'.$aRow[$bColumns[$i]].'</a>';
                }
                else if ( $bColumns[$i] != ' ' )
                {
                    $row[] = $aRow[$bColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode( $output );
    } 

    function CreateNewView()
    {
        extract($_POST);

        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'EstimateViewName' => $EstimateViewName,
            'UserID' => $this->LoginUserID,
            'RestrictVisibility' => $RestrictVisibility,
            'SpecifyFieldsDisplay' => implode(",", $SpecifyFieldsDisplay),
            'CreatedDate' => date("Y-m-d H:i:s"),
            'LastModifiedDate' => date("Y-m-d H:i:s")
        );     
        $query = $this->db->insert('EstimateView', $data);  
        $EstimateViewID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($this->LoginUserID, 'EstimateView', array('EstimateViewID' => $EstimateViewID));

        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'EstimateViewID' => $EstimateViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('EstimateFilter', $data);  

                   
                }
            } 
 
        }
        return $EstimateViewID;
    }
 
    function EditCustomView($EstimateViewID)
    {  
        extract($_POST);
  
        $data = array(
            'EstimateViewName' => $EstimateViewName,
            'UserID' => $this->LoginUserID,
            'RestrictVisibility' => $RestrictVisibility,
            'SpecifyFieldsDisplay' => implode(",", $SpecifyFieldsDisplay),
            'CreatedDate' => date("Y-m-d H:i:s"),
            'LastModifiedDate' => date("Y-m-d H:i:s")
        );     
        $query = $this->db->update('EstimateView', $data, array('EstimateViewID' => $EstimateViewID));  

        $this->db->delete('EstimateFilter', array('EstimateViewID' => $EstimateViewID));  
        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'EstimateViewID' => $EstimateViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('EstimateFilter', $data);  
                }
            } 
 
        }
        return $query;
    }

    function GetEstimateViewData($EstimateViewID){

        $query = $this->db->get_where('EstimateView', array('OrganizationID' => $this->LoginOrganizationID, 'EstimateViewID' => $EstimateViewID)); 
        $EstimateViewData = $query->row();

        $query = $this->db->get_where('EstimateFilter', array('EstimateViewID' => $EstimateViewID)); 
        $EstimateFilterData = $query->result_array();

        $ColName = "e.EstimateID, ac.AccountID, pc.ContactID, e.Owner as OwnerID, e.CreatedBy as CreatedByID, e.LastModifiedBy as LastModifiedByID, ";
        if(!empty($EstimateViewData->SpecifyFieldsDisplay)){
            $SpecifyFieldsDisplays = explode(",", $EstimateViewData->SpecifyFieldsDisplay);
            foreach ($SpecifyFieldsDisplays as $key => $value) {
                if($value == 'Owner'){
                    $ColName .= "CONCAT(ast.FirstName, ' ', ast.LastName) as Owner";
                } else if($value == 'Account'){
                    $ColName .= "ac.AccountName as Account";
                } else if($value == 'OrganizationID'){
                    $ColName .= "o.CompanyName as OrganizationID";
                } else if($value == 'EstimateStatus'){
                    $ColName .= "es.Status as EstimateStatus";
                } else if($value == 'Contact'){
                    $ColName .= "CONCAT(pc.FirstName, ' ', pc.LastName) as Contact";
                } else if($value == 'CreatedBy'){
                    $ColName .= "CONCAT(cr.FirstName, ' ', cr.LastName) as CreatedBy";
                } else if($value == 'LastModifiedBy'){
                    $ColName .= "CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedBy";
                } else if($value == 'ExpirationDate'){
                    $ColName .= "DATE_FORMAT(e.ExpirationDate, '".RES_DATE."') as ExpirationDate";
                } else {
                    $ColName .= "e.".$value;
                }

                if($key != count($SpecifyFieldsDisplays)){
                    $ColName .= ", ";
                }
            }
        }
            
         /* Filter Estimate Data Start */
            $this->db->select($ColName);
            $this->db->from('Estimate e');
            $this->db->join('User ast','ast.UserID = e.Owner', 'left');
            $this->db->join('Account ac','ac.AccountID = e.Account', 'left');
            $this->db->join('Organization o','o.OrganizationID = e.OrganizationID', 'left');
            $this->db->join('EstimateStatus es','es.EstimateStatusID = e.EstimateStatus', 'left');
            $this->db->join('User cr','cr.UserID = e.CreatedBy', 'left');
            $this->db->join('User lmb','lmb.UserID = e.LastModifiedBy', 'left');
            $this->db->join('Contact pc','pc.ContactID = e.Contact', 'left');
            $this->db->where(array('e.OrganizationID' => $this->LoginOrganizationID, 'e.IsDeleted' => 0));

            foreach ($EstimateFilterData as $key => $value) {

                $WhereField = "";
                if($value['FilterField'] == 'Owner'){
                    $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                } else if($value['FilterField'] == 'Account') {
                    $WhereField = "ac.AccountName";
                } else if($value['FilterField'] == 'OrganizationID') {
                    $WhereField = "o.CompanyName";
                } else if($value['FilterField'] == 'EstimateStatus') {
                    $WhereField = "es.Status";
                } else if($value['FilterField'] == 'CreatedBy') {
                    $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                } else if($value['FilterField'] == 'LastModifiedBy') {
                    $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                } else if($value['FilterField'] == 'Contact') {
                    $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                } else if($value['FilterField'] == 'StartDate'){
                     $WhereField = "DATE_FORMAT(e.StartDate, '".RES_DATETIME."')";
                } else if($value['FilterField'] == 'ExpirationDate'){
                     $WhereField = "DATE_FORMAT(e.ExpirationDate, '".RES_DATE."')";
                } else {
                    $WhereField = "e.".$value['FilterField'];
                }

                if($value['FilterCondition'] == 'Equals'){
                    $this->db->where($WhereField.' =',  $value['FilterValue']);  
                } else if($value['FilterCondition'] == 'NotEqualTo'){
                    $this->db->where($WhereField.' !=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'LessThan'){
                    $this->db->where($WhereField.' <', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'GreaterThan'){
                    $this->db->where($WhereField.' >', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'LessOREqualTo'){
                    $this->db->where($WhereField.' <=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'GreaterOREqualTo'){
                    $this->db->where($WhereField.' >=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'Contains'){
                    $this->db->like($WhereField, $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'StartsWith'){ 
                    $this->db->like($WhereField, $value['FilterValue'], 'after');   
                } else if($value['FilterCondition'] == 'DoesNotContain'){
                    $this->db->not_like($WhereField, $value['FilterValue']);   
                } 
            } 
            $query = $this->db->get();  

            //echo $this->db->last_query();exit;
            $data['ColumnsNames'] = $SpecifyFieldsDisplays;
            $data['ColumnsValues'] = $query->result();

            return $data; 
    }

    function GetCustomView($EstimateViewID)
    {
        $query = $this->db->get_where('EstimateView', array('OrganizationID' => $this->LoginOrganizationID, 'EstimateViewID' => $EstimateViewID)); 
        
        $data['EstimateViewData'] = $query->row();

        $query = $this->db->get_where('EstimateFilter', array('EstimateViewID' => $EstimateViewID)); 
        
        $data['EstimateFilterData'] = $query->result();

        return $data;
    }

    function GetViews(){

        $query = $this->db->query("SELECT * FROM EstimateView WHERE (RestrictVisibility = 'VisibleOnlyToMe' AND UserID = '".$this->LoginUserID."') OR (RestrictVisibility = 'VisibleToEveryone') AND OrganizationID = '".$this->LoginOrganizationID."' ORDER BY EstimateViewName ASC");
        
        return $query->result();
    }

    function EstimateViewFields()
    {
        $query = $this->db->get_where('EstimateViewFields'); 
        //echo ($query->db->last_query());exit;
        return $query->result();
    }

    function DeleteCustomView($EstimateViewID)
    {
        return $this->db->delete('EstimateView', array('EstimateViewID' => $EstimateViewID)); 
    }

    function CopyCustomView($EstimateViewID)
    {
       $query = $this->db->get_where('EstimateView', array('EstimateViewID' => $EstimateViewID)); 
      
        foreach ($query->result() as $row){   
           foreach($row as $key=>$val){ 
                if($key != 'EstimateViewID'){    
                    $this->db->set($key, $val); 
                }                
           }
        }
        $this->db->insert('EstimateView');
        $NewEstimateViewID = $this->db->insert_id();

        $query = $this->db->get_where('EstimateFilter', array('EstimateViewID' => $EstimateViewID)); 
       
        foreach ($query->result() as $row){   
           foreach($row as $key => $val){ 
                if($key == 'EstimateViewID'){  
                    $this->db->set($key, $NewEstimateViewID);  
                } else if($key != 'EstimateFilterID'){    
                    $this->db->set($key, $val); 
                }             
           }
           $this->db->insert('EstimateFilter');
        }

        return 1; 
    }

    function RenameCustomView($EstimateViewID){

        $data = array(
            'EstimateViewName' => $_POST['EstimateViewName']
        );

        return $this->db->update('EstimateView', $data, array('EstimateViewID' => $EstimateViewID));  
    }

    function EditSharingCustomView($EstimateViewID){

        $data = array(
            'RestrictVisibility' => $_REQUEST['RestrictVisibility']
        );

        return $this->db->update('EstimateView', $data, array('EstimateViewID' => $EstimateViewID));  
    }
 
    function EditFiltersCustomView($EstimateViewID){ 

        extract($_POST);

        $this->db->delete('EstimateFilter', array('EstimateViewID' => $EstimateViewID));  
        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'EstimateViewID' => $EstimateViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('EstimateFilter', $data);  
                }
            } 
 
        }
        return $query; 
    }

    function EditDisplayedColumnsCustomView($EstimateViewID){

        $data = array(
            'SpecifyFieldsDisplay' => implode(",", $_REQUEST['SpecifyFieldsDisplay'])
        );

        return $this->db->update('EstimateView', $data, array('EstimateViewID' => $EstimateViewID));  
    }
    
}

?>
