<?php

class ActionsModel extends MY_Model{
 
    public $LoginUserID, $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }
 
    function RecentEstimates() 
    {
        $aColumns = array('e.EstimateID','e.EstimateNo', 'e.EstimateName', 'ac.AccountName', 'wos.Status', 'CONCAT(o.FirstName, " ", o.LastName)', '(Select SUM(TotalPrice) FROM EstimateLine WHERE Estimate=e.EstimateID)', 'e.BillingCity', 'e.BillingState','e.Account', 'e.Owner'); 
        $bColumns = array('EstimateID','EstimateNo', 'EstimateName', 'AccountName', 'Status', 'CONCAT(o.FirstName, " ", o.LastName)', '(Select SUM(TotalPrice) FROM EstimateLine WHERE Estimate=e.EstimateID)', 'BillingCity', 'BillingState','Account','Owner'); 
        
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "e.EstimateID";

        /* DB table to use */
        $sTable = "Estimate e
        LEFT JOIN Account ac ON ac.AccountID = e.Account  
        LEFT JOIN User o ON o.UserID = e.Owner
        LEFT JOIN EstimateStatus wos ON wos.EstimateStatusID = e.EstimateStatus";

        /* Limit */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".( $_GET['iDisplayStart'] ).", ".
                        ( $_GET['iDisplayLength'] );
        }

        /* Ordering */
        $sOrder = "ORDER BY e.LastModifiedDate DESC";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                    {
                            $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                                    ".( $_GET['sSortDir_'.$i] ) .", ";
                    }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                    $sOrder = "";
            }
        }

        /* Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited */
        $sWhere = "";
        if ( $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                        $sWhere .= $aColumns[$i]." LIKE '%".( $_GET['sSearch'] )."%' OR ";
                }
                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ')';
        }
        
        /* Where Condition */
        $UserID = $this->session->userdata('UserID');
       
        $where_str = "e.IsDeleted=0 AND e.OrganizationID = '".$this->LoginOrganizationID."'";
        if ( $sWhere == "" )
        {
                $sWhere = "WHERE $where_str";
        }
        else
        {
                $sWhere .= "AND $where_str";
        }
        
        /* SQL queries Get data to display */
        $sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit"; 
 
        $rResult = $this->db->query($sQuery); 
        $rResult = $rResult->result_array(); 
      
        /* Data set length after filtering */
        $sQuery = "SELECT FOUND_ROWS() as cnt";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iFilteredTotal = $result->cnt;

        /* Total data set length */
        $sQuery = "SELECT COUNT(".$sIndexColumn.") as cnt
                FROM   $sTable  $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iTotal = $result->cnt;

        /* Output */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );

        foreach ( $rResult as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($bColumns) ; $i++ )
            {
                if($i == 0)
                {
                    //$row[] = '';
                }
                else if($i == 5)
                {
                     $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['Owner'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($i == 3)
                {
                     $row[] = '<a href="'.SITE_URL.'Account/Actions/AccountDetails/'.$aRow['Account'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($bColumns[$i] == 'EstimateName' || $bColumns[$i] == 'EstimateNo')
                {
                    $row[] = '<a href="'.SITE_URL.'Estimate/Actions/EstimateDetails/'.$aRow[$bColumns[0]].'">'.$aRow[$bColumns[$i]].'</a>';
                }
                else if ( $bColumns[$i] != ' ' )
                {
                    $row[] = $aRow[$bColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode( $output );
    } 

    function CreateEstimate() {
               
        extract($_POST); 

        $UserID = $this->LoginUserID;

        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'EstimateName' => $EstimateName,
            'Owner' => $Owner,
            'Account' => $Account,
            'Contact' => !empty($Contact)?$Contact:NULL,
            'Phone' => $Phone,
            'Email' => $Email,
            'Description' => $Description,
            'BillingName' => $BillingName,
            'BillingAddress' => $BillingAddress,
            'BillingCity' => $BillingCity,
            'BillingState' => $BillingState,
            'BillingCountry' => $BillingCountry,
            'BillingPostalCode' => $BillingPostalCode,
            'BillingLatitude' => !empty($BillingLatitude)?$BillingLatitude:NULL,
            'BillingLongitude' => !empty($BillingLongitude)?$BillingLongitude:NULL,
            'ShippingName' => $ShippingName,
            'ShippingAddress' => $ShippingAddress,
            'ShippingCity' => $ShippingCity,
            'ShippingState' => $ShippingState,
            'ShippingCountry' => $ShippingCountry,
            'ShippingPostalCode' => $ShippingPostalCode,
            'ShippingLatitude' => !empty($ShippingLatitude)?$ShippingLatitude:NULL,
            'ShippingLongitude' => !empty($ShippingLongitude)?$ShippingLongitude:NULL,
            'EstimateStatus' => $EstimateStatus,
            'ExpirationDate' => date("Y-m-d",strtotime($ExpirationDate))
        );  

        $result = $this->db->insert('Estimate', $data); 
        $this->db->last_query();
        $EstimateID = $this->db->insert_id();

        $EstimateNo = MY_Controller::genCode('ET', $EstimateID);
        $this->db->update('Estimate', array('EstimateNo' => $EstimateNo), array('EstimateID' => $EstimateID));  

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Estimate', array('EstimateID' => $EstimateID));

        /* For insert custom fields */ 
        MY_Model::insertCustomFieldData('Estimate', $EstimateID);
    
        return $EstimateID;
    } 
 
    function EditEstimate($EstimateID) {
               
        extract($_POST); 

        $UserID = $this->LoginUserID;
  
        $data = array(
            'EstimateName' => $EstimateName,
            'Owner' => $Owner,
            'Account' => $Account,
            'Contact' => !empty($Contact)?$Contact:NULL,
            'Phone' => $Phone,
            'Email' => $Email,
            'Description' => $Description,
            'BillingName' => $BillingName,
            'BillingAddress' => $BillingAddress,
            'BillingCity' => $BillingCity,
            'BillingState' => $BillingState,
            'BillingCountry' => $BillingCountry,
            'BillingPostalCode' => $BillingPostalCode,
            'BillingLatitude' => !empty($BillingLatitude)?$BillingLatitude:NULL,
            'BillingLongitude' => !empty($BillingLongitude)?$BillingLongitude:NULL,
            'ShippingName' => $ShippingName,
            'ShippingAddress' => $ShippingAddress,
            'ShippingCity' => $ShippingCity,
            'ShippingState' => $ShippingState,
            'ShippingCountry' => $ShippingCountry,
            'ShippingPostalCode' => $ShippingPostalCode,
            'ShippingLatitude' => !empty($ShippingLatitude)?$ShippingLatitude:NULL,
            'ShippingLongitude' => !empty($ShippingLongitude)?$ShippingLongitude:NULL,
            'EstimateStatus' => $EstimateStatus,
            'ExpirationDate' => date("Y-m-d",strtotime($ExpirationDate))
        );
  
        $result = $this->db->update('Estimate', $data, array('EstimateID' => $EstimateID));    
    
        MY_Model::updateCreatedModifiedByAndDt($UserID,'Estimate', array('EstimateID' => $EstimateID));
    
        /* For insert custom fields */ 
        MY_Model::updateCustomFieldData('Estimate', $EstimateID);
        
        return $result;
    } 

    function getEstimateData($EstimateID, $RelatedObjName = 'AllObject')
    { 
        $result = $this->db->update('Estimate', array('LastModifiedDate' => date("Y-m-d H:i:s")), array('EstimateID' => $EstimateID));  

        $query = $this->db->select('e.*, e.EstimateName as Title, ac.AccountName, CONCAT(o.FirstName, " ", o.LastName) as OwnerName, CONCAT(pm.FirstName, " ", pm.LastName) as ContactName, es.Status, DATE_FORMAT(e.ExpirationDate, "'.RES_DATE.'") as ExpirationDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName, DATE_FORMAT(e.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, DATE_FORMAT(e.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, e.CreatedBy, e.LastModifiedBy, org.CompanyName, org.Website, org.Phone as OrgPhone, org.BillingAddress as OrgBillingAddress, org.BillingCity as OrgBillingCity, org.BillingState as OrgBillingState,org.BillingCountry as OrgBillingCountry, org.BillingPostalCode as OrgBillingPostalCode, ac.PhoneNo as AcPhoneNo, org.Tax')
        ->from('Estimate e')
        ->join('Account ac','ac.AccountID = e.Account', 'left')
        ->join('User o','o.UserID = e.Owner', 'left')
        ->join('Contact pm','pm.ContactID = e.Contact', 'left')
        ->join('EstimateStatus es','es.EstimateStatusID = e.EstimateStatus', 'left')
        ->join('User cb','cb.UserID = e.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = e.LastModifiedBy', 'left')
        ->join('Organization org','org.OrganizationID = e.OrganizationID', 'left')
        ->where(array('e.OrganizationID' => $this->LoginOrganizationID, 'e.EstimateID' => $EstimateID)) 
        ->get();
        $data['EstimateData'] = $query->row();

        $this->db->select('SUM(SubTotal) as SubTot, SUM(TotalPrice) as TotPrc, SUM(ListPrice) as ListTot, SUM(UnitPrice) as UnitTot');
        $this->db->from('EstimateLine');
        $this->db->where(array('OrganizationID' => $this->LoginOrganizationID, 'Estimate' => $EstimateID, 'IsDeleted' => 0));
        $query = $this->db->get(); 
        $result = $query->row();
        if(!empty($result->SubTot) && !empty($result->TotPrc) && !empty($result->ListTot) && !empty($result->UnitTot)){
            $Financials['SubTotal'] = $result->SubTot;
            $Financials['Discount'] = (100 - ($result->UnitTot / $result->ListTot) * 100);
            $Financials['TotalPrice'] = $result->ListTot;
            $Financials['Tax'] = ($result->TotPrc - $result->SubTot);
            $Financials['ShippingHandling'] = 0.00;
            $Financials['GrandTotal'] = $result->TotPrc;
        } else {
            $Financials['SubTotal'] = 0.00;
            $Financials['Discount'] = 0.00;
            $Financials['TotalPrice'] = 0.00;
            $Financials['Tax'] = 0.00;
            $Financials['ShippingHandling'] = 0.00;
            $Financials['GrandTotal'] = 0.00;
        }
        $data['EstimateFinancials'] = $Financials;

        if($RelatedObjName == 'EstimateLines'  || $RelatedObjName == 'AllObject'){
            $query = $this->db->select('el.*, p.ProductName');
            $this->db->from('EstimateLine el');
            $this->db->join('Product p','p.ProductID = el.Product', 'left');
            $this->db->where(array('el.OrganizationID' => $this->LoginOrganizationID, 'el.Estimate' => $EstimateID, 'el.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['EstimateLines'] = $query->result();
        }

       /* if($RelatedObjName == 'Invoices'  || $RelatedObjName == 'AllObject'){
            $this->db->select('i.InvoiceNumber, i.TotalPrice, i.SubTotal, is.InvoiceStatus, DATE_FORMAT(i.InvoiceDate, "'.RES_DATE.'") as InvoiceDate, DATE_FORMAT(i.DueDate, "'.RES_DATE.'") as DueDate, wo.Subject as EstimateName');
            $this->db->from('Invoice i');
            $this->db->join('InvoiceStatus is','is.InvoiceStatusID = i.InvoiceStatus', 'left');
            $this->db->join('Estimate wo','wo.EstimateID = i.Estimate', 'left');
            $this->db->where(array('i.Estimate' => $EstimateID, 'i.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                    $this->db->limit(10);
            $query = $this->db->get();  

            $data['Invoices'] = $query->result();
        }
    */
        if($RelatedObjName == 'Events'  || $RelatedObjName == 'AllObject'){
            $query = $this->db->select('e.EventID, e.AssignedTo, e.Subject, CONCAT(pc.FirstName, " ", pc.LastName) as ContactName,  CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, DATE_FORMAT(e.EventStartDate, "'.RES_DATETIME.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATETIME.'") as EventEndDate, DATE_FORMAT(e.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, e.AssignedTo, e.Who, e.CreatedBy');
            $this->db->from('Event e');
            $this->db->join('User cb','cb.UserID = e.CreatedBy', 'left');
            $this->db->join('User at','at.UserID = e.AssignedTo', 'left');
            $this->db->join('Contact pc','pc.ContactID = e.Who', 'left');
            $this->db->join('User lmb','lmb.UserID = e.LastModifiedBy', 'left');
            $this->db->where(array('e.OrganizationID' => $this->LoginOrganizationID, 'e.What' => $EstimateID,'e.RelatedTo' => 'Estimate', 'e.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Events'] = $query->result();
        }

        if($RelatedObjName == 'Files'  || $RelatedObjName == 'AllObject'){
            $this->db->select('f.FileID, f.FileName, f.Subject, f.ContentType, DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, f.CreatedBy');
            $this->db->from('File f');
            $this->db->join('User cb','cb.UserID = f.CreatedBy', 'left');
            $this->db->where(array('f.OrganizationID' => $this->LoginOrganizationID, 'f.What' => $EstimateID,'f.RelatedTo' => 'Estimate', 'f.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Files'] = $query->result();
        }

        if($RelatedObjName == 'Tasks'  || $RelatedObjName == 'AllObject'){
           $this->db->select('t.TaskID, t.Subject, tt.TaskType, DATE_FORMAT(t.DueDate, "'.RES_DATETIME.'") as DueDate, tp.Priority, ts.TaskStatus, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, CONCAT(pc.FirstName, " ", pc.LastName) as ContactName, t.AssignedTo, t.Who');
            $this->db->from('Task t'); 
            $this->db->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left');
            $this->db->join('User at','at.UserID = t.AssignedTo', 'left');
            $this->db->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left');
            $this->db->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left');
            $this->db->join('Contact pc','pc.ContactID = t.Who', 'left');
            $this->db->where(array('t.OrganizationID' => $this->LoginOrganizationID, 't.What' => $EstimateID,'t.RelatedTo' => 'Estimate', 't.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Tasks'] = $query->result();
        }

        if($RelatedObjName == 'Notes'  || $RelatedObjName == 'AllObject'){
           $this->db->select('n.NoteID, n.Subject, DATE_FORMAT(n.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, CONCAT(o.FirstName, " ", o.LastName) as OwnerName, n.Owner');
            $this->db->from('Note n'); 
            $this->db->join('User o','o.UserID = n.Owner', 'left');
            $this->db->where(array('n.OrganizationID' => $this->LoginOrganizationID, 'n.What' => $EstimateID,'n.RelatedTo' => 'Estimate', 'n.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Notes'] = $query->result();
        }
        return $data;
    }

    /*function CreateNote($EstimateID)
    {
        extract($_POST); 

        $UserID = $this->LoginUserID;
  
        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'Owner' => $UserID,
            'What' => $EstimateID,
            'Subject' => $Subject,
            'Body' => $Body,
            'RelatedTo' => 'Estimate'
        );
        $result = $this->db->insert('Note', $data);    
        $NoteID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Note', array('NoteID' => $NoteID));
    
        return $result;

    }*/

    function EstimateLine($EstimateID)
    {
        $UserID = $this->LoginUserID;

        extract($_POST);

        $query = $this->db->delete('EstimateLine', array('Estimate' => $EstimateID));

        if(count($Product) > 0){ 

            foreach ($Product as $key => $value) {
            
               if(!empty($Product[$key]) && !empty($ListPrice[$key]) && !empty($UnitPrice[$key]) && !empty($Quantity[$key]) && !empty($SubTotal[$key]) && !empty($TotalPrice[$key])){
                    $data = array(
                        'Estimate' => $EstimateID,
                        'OrganizationID' => $this->LoginOrganizationID,
                        'Product' => $Product[$key],
                        'ListPrice' => filter_var($ListPrice[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
                        'Discount' => filter_var($Discount[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
                        'UnitPrice' => filter_var($UnitPrice[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
                        'Quantity' => $Quantity[$key],
                        'SubTotal' => filter_var($SubTotal[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
                        'Taxable' => empty($Taxable[$key])?0:1,
                        'TotalPrice' => filter_var($TotalPrice[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION)
                    );     
                    $query = $this->db->insert('EstimateLine', $data);  
                    $EstimateLineID = $this->db->insert_id();

                    $LineItemNo = MY_Controller::genCode('ETL' ,$EstimateLineID);
                    $this->db->update('EstimateLine', array('EstimateLineNo'=>$LineItemNo), array('EstimateLineID' => $EstimateLineID));  
           
                    MY_Model::insertCreatedModifiedByAndDt($UserID,'EstimateLine', array('EstimateLineID' => $EstimateLineID));
                }
            }
        }

        return $query;
    }

    function CreateWOChemicalLineItems($EstimateID)
    {
        $UserID = $this->LoginUserID;

        extract($_POST);

        $query = $this->db->delete('Chemical', array('Estimate' => $EstimateID));

        if(count($Product) > 0){ 

            foreach ($Product as $key => $value) {
               
               if(!empty($Product[$key]) && !empty($TestConcentration[$key]) && !empty($TestedUnitOfMeasure[$key]) && !empty($ApplicationAmount[$key]) && !empty($ApplicationUnitOfMeasure[$key])){
                    $data = array(
                        'Estimate' => $EstimateID,
                        'OrganizationID' => $this->LoginOrganizationID,
                        'Product' => $Product[$key],
                        'Owner' => $UserID,
                        'Account' => $Account,
                        'TestConcentration' => $TestConcentration[$key],
                        'TestedUnitOfMeasure' => $TestedUnitOfMeasure[$key],
                        'ApplicationAmount' => $ApplicationAmount[$key],
                        'ApplicationUnitOfMeasure' => $ApplicationUnitOfMeasure[$key],
                        'ApplicationArea' => $ApplicationArea[$key],
                        'AdditionalNotes' => $AdditionalNotes[$key]
                    );     
                    $query = $this->db->insert('Chemical', $data);  

                    $ChemicalID = $this->db->insert_id();

                    $ChemicalNo = MY_Controller::genCode('CH' ,$ChemicalID);
                    $this->db->update('Chemical', array('ChemicalNo'=>$ChemicalNo), array('ChemicalID' => $ChemicalID));  
                    
                    MY_Model::insertCreatedModifiedByAndDt($UserID,'Chemical', array('ChemicalID' => $ChemicalID));
                }
            }
        }

        return $query;
    }

    function GetPrimaryContactOptions()
    { 

        $query = $this->db->get_where('Contact', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0, 'Account' => $_POST['AccountID'])); 
        $results = $query->result();
        $PCOptions = '<option value="">Select Contact</option>';
        foreach ($results as $key => $value) {
            if(isset($_POST['PrimaryContact']) && !empty($_POST['PrimaryContact']) && $_POST['PrimaryContact'] == $value->ContactID)
                $PCOptions .= '<option value="'.$value->ContactID.'" selected="">'.$value->FirstName.' '.$value->LastName.'</option>';
            else 
                 $PCOptions .= '<option value="'.$value->ContactID.'">'.$value->FirstName.' '.$value->LastName.'</option>';
        }
        return $PCOptions;
    }

    function ConvertToWorkOrder($EstimateID)
    {
        $UserID = $this->LoginUserID;

        extract($_POST);

        $query = $this->db->get_where('Estimate', array('OrganizationID' => $this->LoginOrganizationID, 'EstimateID' => $EstimateID)); 
        
        if($query->num_rows() > 0){ 

            $result = $query->row(); 

            $data = array(
                'OrganizationID' => $this->LoginOrganizationID,
                'AssignedTo' => $result->Owner,
                'Account' => $result->Account,
                'Subject' => $result->EstimateName,
                'PrimaryContact' => $result->Contact,
                'Description' => $result->Description,
                'Address' => $result->BillingAddress,
                'City' => $result->BillingCity,
                'State' => $result->BillingState,
                'Country' => $result->BillingCountry,
                'PostalCode' => $result->BillingPostalCode,
                'Latitude' => $result->BillingLatitude,
                'Longitude' => $result->BillingLongitude,
            );     
            $result = $this->db->insert('WorkOrder', $data); 
            $WorkOrderID = $this->db->insert_id();

            $WorkOrderNo = MY_Controller::genCode('WO', $WorkOrderID);
            $this->db->update('WorkOrder', array('WorkOrderNo' => $WorkOrderNo), array('WorkOrderID' => $WorkOrderID));  

            MY_Model::insertCreatedModifiedByAndDt($UserID,'WorkOrder', array('WorkOrderID' => $WorkOrderID));

            $query = $this->db->get_where('EstimateLine', array('OrganizationID' => $this->LoginOrganizationID, 'Estimate' => $EstimateID));
            if($query->num_rows() > 0){ 

                $LineItemsResult = $query->result(); 

                foreach ($LineItemsResult as $key => $LineItem) {
                    
                    $data = array(
                        'OrganizationID' => $this->LoginOrganizationID,
                        'WorkOrder' => $WorkOrderID,
                        'Product' => $LineItem->Product,
                        'ListPrice' => $LineItem->ListPrice,
                        'Discount' => $LineItem->Discount,
                        'UnitPrice' => $LineItem->UnitPrice,
                        'Quantity' => $LineItem->Quantity,
                        'SubTotal' => $LineItem->SubTotal,
                        'Taxable' => $LineItem->Taxable,
                        'NetTotal' => $LineItem->TotalPrice,
                    );     
                    $query = $this->db->insert('WOLineItem', $data);  
                    $WOLineItemID = $this->db->insert_id();

                    $LineItemNo = MY_Controller::genCode('WOLI' ,$WOLineItemID);
                    $this->db->update('WOLineItem', array('LineItemNo'=>$LineItemNo), array('WOLineItemID' => $WOLineItemID));  
           
                    MY_Model::insertCreatedModifiedByAndDt($UserID,'WOLineItem', array('WOLineItemID' => $WOLineItemID));
                }
            }

            return $WorkOrderID;
        }

        return 0;
    }

    function GetProductDetails($ProductID)
    {
        $query = $this->db->get_where('Product', array('OrganizationID' => $this->LoginOrganizationID, 'ProductID' => $ProductID)); 
        return json_encode($query->row()); 
    }

    function GetAccountDetails($AccountID){
        $query = $this->db->get_where('Account', array('OrganizationID' => $this->LoginOrganizationID, 'AccountID' => $AccountID)); 
        return json_encode($query->row()); 
    }

    function GetContactDetails($ContactID)
    {
        $query = $this->db->get_where('Contact', array('OrganizationID' => $this->LoginOrganizationID, 'ContactID' => $ContactID)); 
        return json_encode($query->row()); 
    }
  
    function GetUsers()
    { 
        $query = $this->db->get_where('User', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetEstimateStatus()
    { 
        $query = $this->db->get_where('EstimateStatus', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        return $query->result();
    }

    function GetAccounts()
    { 
        $UserID = $this->session->userdata('UserID');

        $query = $this->db->get_where('Account',array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetContacts()
    { 
        $query = $this->db->get_where('Contact',array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0, 'IsActive' => 1)); 
        
        return $query->result();
    }

    function GetProducts()
    {
        $query = $this->db->get_where('Product', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }
 
    function GetViews(){

        $query = $this->db->query("SELECT * FROM EstimateView WHERE (RestrictVisibility = 'VisibleOnlyToMe' AND UserID = '".$this->LoginUserID."') OR (RestrictVisibility = 'VisibleToEveryone') AND OrganizationID = '".$this->LoginOrganizationID."' ORDER BY EstimateViewName ASC");
        
        return $query->result();
    }

    function DeleteEstimate($EstimateID)
    {
        return $this->db->update('Estimate', array('IsDeleted' => 1), array('EstimateID' => $EstimateID));  
    }

    /* Scheduling Start */
    function SaveScheduling(){

        extract($_POST); 

        $UserID = $this->LoginUserID;
  
        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'Subject' => $EstimateNo.' - '.$EstimateName,
            'RelatedTo' => 'Estimate',
            'What' => $EstimateID,
            'AssignedTo' => $AssignedTo,
            'Who' => !empty($Who)?$Who:NULL,
            'Description' => $Description,
            'EventStartDate' => date("Y-m-d H:i:s", strtotime($EventStartDate)),
            'EventEndDate' => date("Y-m-d H:i:s", strtotime($EventEndDate)),
            'EventStartTime' => date("H:i:s", strtotime($EventStartDate)),
            'EventEndTime' => date("H:i:s", strtotime($EventEndDate)),
        );
        $result = $this->db->insert('Event', $data);  

        $EventID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));

        if($result){

            return $this->GetAllSchedulingData($EstimateID);
        }
    }

    function UpdateScheduling($EventID)
    {
        extract($_POST); 

        $UserID = $this->LoginUserID;
  
         $data = array(
            'AssignedTo' => $AssignedTo,
            'EventStartDate' => date("Y-m-d H:i:s", strtotime($EventStartDate)),
            'EventEndDate' => date("Y-m-d H:i:s", strtotime($EventEndDate)),
            'EventStartTime' => date("H:i:s", strtotime($EventStartDate)),
            'EventEndTime' => date("H:i:s", strtotime($EventEndDate)),
        );
        $result = $this->db->update('Event', $data, array('EventID' => $EventID));  

        MY_Model::updateCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));

        if($result){

            return $this->GetAllSchedulingData($EstimateID);
        }
    }

    function DeleteScheduling()
    {
        extract($_POST); 

        $this->db->delete('Event', array('EventID' => $EventID)); 

        return $this->GetAllSchedulingData($EstimateID);
    }

    function GetScheduling($EventID)
    {
        $query = $this->db->get_where('Event', array('OrganizationID' => $this->LoginOrganizationID, 'EventID' => $EventID)); 
        $Event = $query->row(); 

        $SchedulingWrap = "";
        $SchedulingWrap .= '<div id="Event'.$EventID.'"><div class="form-wrap">
                   <form action="#" class="form-horizontal" id="SchedulingForm'.$EventID.'">
                    <input type="hidden" name="EstimateID" value="'.$Event->What.'">
                   <div class="form-body">
                       <div class="form-group">
                           <label class="col-md-3 control-label text-left">Assigned To</label>
                           <div class="col-md-6">
                               <select name="AssignedTo" id="AssignedTo" class="form-control select2">
                               <option value="">Select Owner</option>';
                                 $Users = $this->ActionsModel->GetUsers();
                                    foreach ($Users as $User) { 
                                        if($Event->AssignedTo == $User->UserID){
                                           $SchedulingWrap .= '<option value="'.$User->UserID.'" selected="">'.$User->FirstName.' '.$User->LastName.'</option>';
                                        } else {
                                           $SchedulingWrap .= '<option value="'.$User->UserID.'">'.$User->FirstName.' '.$User->LastName.'</option>';
                                        }
                                    } 
                            $SchedulingWrap .= '</select>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-md-3 control-label text-left">Start</label>
                           <div class="col-md-6">
                               <input type="text" class="form-control showdatetimepicker" name="EventStartDate" value="'.date("m/d/Y h:i A", strtotime($Event->EventStartDate)).'" required>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-md-3 control-label text-left">End</label>
                           <div class="col-md-6">
                               <input type="text" class="form-control showdatetimepicker" name="EventEndDate" value="'.date("m/d/Y h:i A", strtotime($Event->EventEndDate)).'" required>
                           </div>
                       </div>
                   </div>
                   <div class="pull-right">
                       <button type="button" class="btn btn-default" OnClick="UpdateScheduling('.$EventID.')" data-dismiss="modal">Close</button>
                       <button type="button" class="btn btn-primary" OnClick="UpdateScheduling('.$EventID.')">Save</button>
                   </div>
                   </form>
                 <div class="clearfix"></div>
                 <hr class="light-grey-hr">
                 </div>
                 </div>';

        return $SchedulingWrap;
    }

    function EditSchAssignedTo()
    { 
        extract($_POST); 
        $SchedulingWrap = "";
        $SchedulingWrap .= '<select name="AssignedTo" id="AssignedToVal'.$EventID.'" class="form-control select2">
                               <option value="">Select Owner</option>';
                                 $Users = $this->ActionsModel->GetUsers();
                                    foreach ($Users as $User) { 
                                        if($AssignedTo == $User->UserID){
                                           $SchedulingWrap .= '<option value="'.$User->UserID.'" selected="">'.$User->FirstName.' '.$User->LastName.'</option>';
                                        } else {
                                           $SchedulingWrap .= '<option value="'.$User->UserID.'">'.$User->FirstName.' '.$User->LastName.'</option>';
                                        }
                                    } 
                            $SchedulingWrap .= '</select>';
        return $SchedulingWrap;

    }

    function UpdateAssignedTo()
    {
        $UserID = $this->LoginUserID;

        extract($_POST); 

        $result = $this->db->update('Event', array('AssignedTo' => $AssignedTo), array('EventID' => $EventID));  

        MY_Model::updateCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));

        $UserRes = $query = $this->db->get_where('User', array('OrganizationID' => $this->LoginOrganizationID, 'UserID' => $AssignedTo));  
        $User = $UserRes->row(); 

        return $User->FirstName.' '.$User->LastName;
    }

    function GetAllSchedulingData($EstimateID)
    {
        $query = $this->db->select('e.*, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, DATE_FORMAT(e.EventStartDate, "'.RES_DATETIME.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATETIME.'") as EventEndDate')
        ->from('Event e')
        ->join('User at','at.UserID = e.AssignedTo', 'left')
        ->where(array('e.OrganizationID' => $this->LoginOrganizationID, 'e.RelatedTo' => 'Estimate', 'e.What' => $EstimateID)) 
        ->order_by("e.CreatedDate", "desc")
        ->limit(5)
        ->get();
        $Events = array_reverse($query->result()); 

        $SchedulingWrap = "";
        foreach ($Events as $key => $Event) {
        
            $SchedulingWrap .= '<div id="Event'.$Event->EventID.'"><div class="form-wrap">
                        <form action="#" class="form-horizontal schedulingfrmclss">
                        <div class="form-body">
                            <div class="form-group">
                                <div class="col-md-3 text-left txt-dark">
                                    Assigned To
                                </div>
                                <div class="col-md-4" id="AssignedToDropDown'.$Event->EventID.'">
                                    '.$Event->AssignedToName.' 
                                </div>
                                <div class="col-md-4" id="AssignedToBtn'.$Event->EventID.'">
                                   <a OnClick="EditSchAssignedTo('.$Event->EventID.', '.$Event->AssignedTo.')">Change</a>
                                </div>
                                <div class="col-md-1 pl-10">
                                    <div class="dropdown" >
                                        <a data-toggle="dropdown"><i class="fa fa-toggle-down"></i></a>
                                        <ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu" style="right:0;left: unset;">
                                            <li>
                                                <a>View in Calendar</a>
                                                <a OnClick="EditScheduling('.$Event->EventID.')">Edit</a>
                                                <a OnClick="DeleteScheduling('.$Event->EventID.','.$EstimateID.')">Delete</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group pt-5">
                                <div class="col-md-3 text-left txt-dark">
                                    Start
                                </div>
                                <div class="col-md-6">
                                    '.$Event->EventStartDate.'
                                </div>
                            </div>
                            <div class="form-group pt-5">
                                <div class="col-md-3 text-left txt-dark">
                                    End
                                </div>
                                <div class="col-md-6">
                                    '.$Event->EventEndDate.'
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="light-grey-hr">
                    </div>';
        }
        return $SchedulingWrap;
    }
    /* Scheduling End */

    function GenDocTemplates($RelatedTo)
    {
        $query = $this->db->get_where('GenDocTemplates', array('OrganizationID' => $this->LoginOrganizationID, 'RelatedTo' => $RelatedTo, 'IsDeleted' => 0, 'IsActive' => 1)); 
        
        return $query->result();
    }

    function GenerateDocument($EstimateData)
    {

        $_POST['SaveToInvoice'] = isset($_POST['SaveToInvoice'])?$_POST['SaveToInvoice']:0;
        
        extract($_POST);
        extract($EstimateData);

        $data = array();

        $data['AssignedTo'] = $EstimateData->Owner;
        $data['EstimateNo'] = $EstimateData->EstimateNo;
        
        $company = array();
        $company["name"] = $EstimateData->CompanyName;
        $company["street_address"] = $EstimateData->OrgBillingAddress;
        $company["city_state_zip"] = $EstimateData->OrgBillingCity.', '.$EstimateData->OrgBillingState.', '.$EstimateData->OrgBillingCountry.', '.$EstimateData->OrgBillingPostalCode;
        $company["phone"] = $EstimateData->OrgPhone;
        $company["fax"] = "";
        $company["website"] = $EstimateData->Website;
        $data["company"] = $company;

       /* $host = array();
        $host["logo"] = dirname(__FILE__).'/logo.png';
        $data["host"] = $host;*/

        $et = array();
        $et["no"] = $EstimateData->EstimateNo;
        $et["date"] = date("m/d/Y", strtotime($EstimateData->ExpirationDate));
        $et["description"] = $EstimateData->Description;
        $et["subject"] = $EstimateData->EstimateName;
        $data["et"] = $et;

        $customer = array();
        $customer["name"] = "-";
        $customer["id"] = "-";
        $data["customer"] = $customer;

        $bill = array();
        $bill["name"] = "john jojo";
        $bill["company_name"] = $EstimateData->CompanyName;
        $bill["street_address"] = $EstimateData->BillingAddress;
        $bill["city_state_zip"] = $EstimateData->BillingCity.','.$EstimateData->BillingState.','.$EstimateData->BillingCountry.','.$EstimateData->BillingPostalCode;
        $bill["phone"] = $EstimateData->AcPhoneNo;
        $data["bill"] = $bill;

        $items = array();
        $taxable = 0;

        foreach ($EstimateLines as $key => $EstimateLineItem) {
            
            $item = array();
            $item["qty"] = $EstimateLineItem->Quantity;
            $item["description"] = $EstimateLineItem->ProductName;
            $item["tax"] = ($EstimateLineItem->Taxable == 1) ? "Y" : "N";
            $item["price"] = number_format((double)$EstimateLineItem->UnitPrice, 2, '.', ',');
            $item["line_total"] = number_format((double)$EstimateLineItem->TotalPrice, 2, '.', ',');
            $items[] = $item;

            $taxable += ($EstimateLineItem->Taxable == 1) ? $EstimateLineItem->TotalPrice : 0;
        } 
        $data["items"] = $items;

        $invoice = array();
        $invoice["subtotal"] = number_format($EstimateFinancials['SubTotal'], 2, '.', ',');
        $invoice["taxable"] = number_format($taxable, 2, '.', ',');
        $invoice["tax_rate"] = $EstimateData->Tax.'%';
        $invoice["tax"] = number_format((double)$EstimateFinancials['Tax'], 2, '.', ',');
        $invoice["s_h"] = "-";
        $invoice["other"] = "-";
        $invoice["total"] = number_format((double)$EstimateFinancials['GrandTotal'], 2, '.', ',');
        $data["invoice"] = $invoice;

        return MY_Model::generateDocs($GenDocTemplateID, $data, $EstimateData->EstimateID, $EstimateData->EstimateNo, $EstimateData->Owner, $SaveToEstimate, $OutputFormat, 'Estimate');
    }
}

?>
