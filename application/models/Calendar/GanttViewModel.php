<?php
 
class GanttViewModel extends MY_Model{
 
    public $LoginUserID, $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }

    function Gantt()
    {
        extract($_GET);

        if(isset($start) && isset($end))
        {
            $_SESSION['GLVStartDate'] = $start;
            $_SESSION['GLVEndDate'] = $end;
        }
        $FromDate = $_SESSION['GLVStartDate'];
        $ToDate = $_SESSION['GLVEndDate'];
      
        $finaldata = array();
        $WorkOrderIDs = array();
        $TaskData = array();

        if(isset($ShowTasks) && $ShowTasks == 1)
        {
            $this->db->select('t.TaskID, t.Subject, t.DueDate as Time, t.DueDate as StartDate, t.DueDate as EndDate, ts.TaskStatus as Status, tp.Priority, t.Subject, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, t.City, t.State, t.AssignedTo');
            $this->db->from('Task t');
            $this->db->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left');
            $this->db->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left');
            $this->db->join('User at','at.UserID = t.AssignedTo', 'left');
            $this->db->where(array('ts.IsClosed' => 0, 't.IsDeleted' => 0, 't.OrganizationID' => $this->LoginOrganizationID));
           /* $this->db->where('DATE(t.DueDate) >=', date("Y-m-d", strtotime($FromDate)));
            $this->db->where('DATE(t.DueDate) <=', date("Y-m-d", strtotime($ToDate)));*/
            $this->db->where("(DATE(t.DueDate) >= '".date("Y-m-d", strtotime($FromDate))."' AND DATE(t.DueDate) <= '".date("Y-m-d", strtotime($ToDate))."')");
            
            if(isset($AssignedTo) && !empty($AssignedTo))
                $this->db->where_in('t.AssignedTo', $AssignedTo);
            $TaskQuery = $this->db->get(); 
            //echo $this->db->last_query(); exit;

            $TaskData = $TaskQuery->result_array();
        }

        $this->db->select('WorkOrderID');
        $this->db->from('WorkOrder');
        if(isset($WorkOrderType) && !empty($WorkOrderType))
            $this->db->where_in('WorkOrderType', $WorkOrderType);
        else
            $this->db->where_in('WorkOrderType', 0);
        if(isset($WOStatus) && !empty($WOStatus))
            $this->db->where_in('WOStatus', $WOStatus);
        else
            $this->db->where_in('WOStatus', 0);
        if(isset($WOPriority))
            $this->db->where_in('WOPriority', $WOPriority);
        else
            $this->db->where_in('WOPriority', 0);
        $this->db->where(array('IsDeleted' => 0, 'OrganizationID' => $this->LoginOrganizationID));
        $WorkOrderQuery = $this->db->get(); 
        //echo $this->db->last_query(); exit;
        $WorkOrderRes = $WorkOrderQuery->result();
        foreach ($WorkOrderRes as $key => $value) {
           $WorkOrderIDs[] = $value->WorkOrderID;
        } 

        $this->db->select('e.EventID, e.Subject, e.EventStartDate as StartDate, e.EventEndDate as EndDate, e.EventEndDate as Time, es.EventStatus as Status, ep.Priority, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, e.City, e.State, e.AssignedTo');
        $this->db->from('Event e');
        $this->db->join('EventType et','et.EventTypeID = e.EventType', 'left');
        $this->db->join('EventStatus es','es.EventStatusID = e.EventStatus', 'left');
        $this->db->join('EventPriority ep','ep.EventPriorityID = e.EventPriority', 'left');
        $this->db->join('User at','at.UserID = e.AssignedTo', 'left');
        /*$this->db->where("((DATE(e.EventStartDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."') OR (DATE(e.EventEndDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."') OR ('".date("Y-m-d", strtotime($FromDate))."' BETWEEN DATE(e.EventStartDate) AND DATE(e.EventEndDate)) OR ('".date("Y-m-d", strtotime($ToDate))."' BETWEEN DATE(e.EventStartDate) AND DATE(e.EventEndDate)))");*/
        $this->db->where("((DATE(e.EventStartDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."') OR (DATE(e.EventEndDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."'))");

        $this->db->where("IF(e.RelatedTo = 'WorkOrder', IF(e.What IN ('".implode("','", $WorkOrderIDs)."'),1,0), 1)");

        if(isset($AssignedTo) && !empty($AssignedTo))
            $this->db->where_in('e.AssignedTo', $AssignedTo);
        else 
            $this->db->where_in('e.AssignedTo', 0);

        $this->db->where(array('e.IsDeleted' => 0, 'e.OrganizationID' => $this->LoginOrganizationID));
        $EventQuery =  $this->db->get();   
        //echo $this->db->last_query(); exit;
        $OpenActivities = array_merge($TaskData, $EventQuery->result_array());

        foreach ($OpenActivities as $key => $OpenActivity) {

            $data['title'] = $OpenActivity['Subject'].' '.$OpenActivity['City'].', '.$OpenActivity['State'];
            if(date("H:i:s",strtotime($OpenActivity['StartDate'])) == '00:00:00' && date("H:i:s",strtotime($OpenActivity['EndDate'])) == '00:00:00'){ 
                $data['start'] = date("Y-m-d",strtotime($OpenActivity['StartDate']));
                $data['end'] = date("Y-m-d",strtotime($OpenActivity['EndDate']));
            } else if(date("H:i:s",strtotime($OpenActivity['StartDate'])) == date("H:i:s",strtotime($OpenActivity['EndDate']))){
                $data['start'] = NULL;
                $data['end'] = NULL;
            } else {
                $data['start'] = date("Y-m-d H:i:s",strtotime($OpenActivity['StartDate']));
                $data['end'] = date("Y-m-d H:i:s",strtotime($OpenActivity['EndDate']));
            }
           
            $data['startdate'] = date("Y-m-d H:i:s",strtotime($OpenActivity['StartDate']));
            $data['enddate'] = date("Y-m-d H:i:s",strtotime($OpenActivity['EndDate']));
            if(isset($OpenActivity['TaskID'])){
                $data['type'] = 'Task';
                $data['id'] = $OpenActivity['TaskID'];
            } else if(isset($OpenActivity['EventID'])){
                $data['type'] = 'Event';
                $data['id'] = $OpenActivity['EventID'];
            }
            $data['resourceId'] = ($OpenActivity['AssignedTo']==NULL)?'NULL':$OpenActivity['AssignedTo'];
            $finaldata[] = $data;
        }
        return json_encode($finaldata);
    } 

    function DateCompareDesc($a, $b)
    {
        $t1 = strtotime($a['Time']);
        $t2 = strtotime($b['Time']);
        return $t2 - $t1;
    } 

    function GetEventTaskDetails()
    { 
        extract($_POST);

        if(isset($Type) && $Type == 'Event'){
            $this->db->select('e.EventID, e.Subject, DATE_FORMAT(e.EventStartDate, "'.RES_DATETIME.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATETIME.'") as EventEndDate, es.EventStatus, ep.Priority, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, e.RelatedTo, e.What, e.AssignedTo'); 
            $this->db->from('Event e');
            $this->db->join('EventStatus es','es.EventStatusID = e.EventStatus', 'left');
            $this->db->join('EventPriority ep','ep.EventPriorityID = e.EventPriority', 'left');
            $this->db->join('User at','at.UserID = e.AssignedTo', 'left');
            $this->db->where(array('e.EventID' => $ID, 'e.OrganizationID' => $this->LoginOrganizationID));
            $EventQuery =  $this->db->get();   
            //echo $this->db->last_query(); exit;
            $EventData = $EventQuery->row_array();

            /* Fetch data from Related Object */
            $RelatedDataArr = MY_Model::getRelatedData();
            $ObjRelatedData = $RelatedDataArr[$EventData['RelatedTo']][$EventData['What']];

            if($EventData['RelatedTo'] == 'WorkOrder')
            {
                $SapeArr = array('Low' => 'circle', 'High' => 'star', 'Normal' => 'square',);
                $StatusImage = isset($ObjRelatedData->ColorCode)?'<i class="fa fa-'.$SapeArr[$ObjRelatedData->Priority].' fa-2x" style="color:'.$ObjRelatedData->ColorCode.';"></i>':"";
                $Priority = $ObjRelatedData->Priority;
            }
            else 
            {
                $StatusImage = isset($ObjRelatedData->ColorCode)?'<i class="fa fa-circle fa-2x" style="color:'.$ObjRelatedData->ColorCode.';"></i>':"";
                $Priority = "-";
            }
            $Status = isset($ObjRelatedData->Status)?$ObjRelatedData->Status:"-";


           /* if (strtotime(Date("Y-m-d H:i:s")) >= strtotime(date("Y-m-d H:i:s", strtotime($EventData['EventStartDate']))) && strtotime(Date("Y-m-d H:i:s")) <= strtotime(date("Y-m-d H:i:s", strtotime($EventData['EventEndDate']))))
                $Status = "In Progress";
            else if(strtotime(Date("Y-m-d H:i:s")) > strtotime(date("Y-m-d H:i:s", strtotime($EventData['EventEndDate']))))
                $Status = "Past";
            else 
                $Status = "Upcoming";*/

            $EventTaskDettailHtml = "<div class='col-md-12 text-left'>";
            $EventTaskDettailHtml .= "<div class='col-md-4 col-lg-4 col-xs-12 col-sm-4 pt-5'><strong>Assigned To :</strong></div> <div class='col-md-8 col-lg-8 col-xs-12 col-sm-8 pt-5'>".(!empty($EventData['AssignedToName'])?"<a href='".SITE_URL."Setup/Administration/Actions/UserDetails/".$EventData['AssignedTo']."'>".$EventData['AssignedToName']."</a>":'-')."</div>";
            $EventTaskDettailHtml .= "<div class='col-md-4 col-lg-4 col-xs-12 col-sm-4 pt-5'><strong>Subject :</strong></div> <div class='col-md-8 col-lg-8 col-xs-12 col-sm-8 pt-5'><a href='".SITE_URL."Event/Actions/EventDetails/".$EventData['EventID']."'>".$EventData['Subject']."</a></div>";
            $EventTaskDettailHtml .= "<div class='col-md-4 col-lg-4 col-xs-12 col-sm-4 pt-5'><strong>Related To :</strong> </div> <div class='col-md-8 col-lg-8 col-xs-12 col-sm-8 pt-5'><a href='".SITE_URL.$EventData['RelatedTo']."/Actions/".$EventData['RelatedTo']."Details/".$ObjRelatedData->ID."'>". $ObjRelatedData->No."</a></div>";
            $EventTaskDettailHtml .= "<div class='col-md-4 col-lg-4 col-xs-12 col-sm-4 pt-5'><strong>Start Date Time :</strong> </div> <div class='col-md-8 col-lg-8 col-xs-12 col-sm-8 pt-5'>".$EventData['EventStartDate']."</div>";
            $EventTaskDettailHtml .= "<div class='col-md-4 col-lg-4 col-xs-12 col-sm-4 pt-5'><strong>End Date Time :</strong> </div> <div class='col-md-8 col-lg-8 col-xs-12 col-sm-8 pt-5'>".$EventData['EventEndDate']."</div>";
            $EventTaskDettailHtml .= "<div class='col-md-4 col-lg-4 col-xs-12 col-sm-4 pt-5'><strong>Status :</strong> </div> <div class='col-md-8 col-lg-8 col-xs-12 col-sm-8 pt-5'>".$StatusImage." <span style='padding-top: 3px;position: fixed;padding-left: 5px;'>". $Status."</span>&nbsp;</div>";
            $EventTaskDettailHtml .= "<div class='col-md-4 col-lg-4 col-xs-12 col-sm-4 pt-5'><strong>Priority :</strong> </div> <div class='col-md-8 col-lg-8 col-xs-12 col-sm-8 pt-5'>".$Priority.'</div>';
            $EventTaskDettailHtml .= "</div>";

            return $EventTaskDettailHtml;

        } else if(isset($Type) && $Type == 'Task'){

            $this->db->select('t.TaskID, t.Subject, DATE_FORMAT(t.DueDate, "'.RES_DATETIME.'") as StartDate, DATE_FORMAT(t.DueDate, "'.RES_DATETIME.'") as EndDate, ts.TaskStatus, tp.Priority, t.Subject, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, t.RelatedTo, t.What, t.AssignedTo');
            $this->db->from('Task t');
            $this->db->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left');
            $this->db->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left');
            $this->db->join('User at','at.UserID = t.AssignedTo', 'left');
            $this->db->where(array('t.TaskID' => $ID, 't.OrganizationID' => $this->LoginOrganizationID));
            $TaskQuery =  $this->db->get();   
            //echo $this->db->last_query(); exit;
            $TaskData = $TaskQuery->row_array();

            /* Fetch data from Related Object */
            $RelatedDataArr = MY_Model::getRelatedData();
            $ObjRelatedData = $RelatedDataArr[$TaskData['RelatedTo']][$TaskData['What']];

            if($TaskData['RelatedTo'] == 'WorkOrder')
            {
                $SapeArr = array('Low' => 'circle', 'High' => 'star', 'Normal' => 'square',);
                $StatusImage = isset($ObjRelatedData->ColorCode)?'<i class="fa fa-'.$SapeArr[$ObjRelatedData->Priority].' fa-2x" style="color:'.$ObjRelatedData->ColorCode.';"></i>':"";
                $Priority = $ObjRelatedData->Priority;
            }
            else 
            {
                $StatusImage = isset($ObjRelatedData->ColorCode)?'<i class="fa fa-circle fa-2x" style="color:'.$ObjRelatedData->ColorCode.';"></i>':"";
                $Priority = "-";
            }
            $Status = isset($ObjRelatedData->Status)?$ObjRelatedData->Status:"-";


            /*if (strtotime(Date("Y-m-d H:i:s")) >= strtotime(date("Y-m-d H:i:s", strtotime($TaskData['StartDate']))) && strtotime(Date("Y-m-d H:i:s")) <= strtotime(date("Y-m-d H:i:s", strtotime($TaskData['EndDate']))))
                $Status = "In Progress";
            else if(strtotime(Date("Y-m-d H:i:s")) > strtotime(date("Y-m-d H:i:s", strtotime($TaskData['EndDate']))))
                $Status = "Past";
            else 
                $Status = "Upcoming";*/



            $EventTaskDettailHtml = "<div class='col-md-12 text-left'>";
            $EventTaskDettailHtml .= "<div class='col-md-4 col-lg-4 col-xs-12 col-sm-4 pt-5'><strong>Assigned To :</strong></div> <div class='col-md-8 col-lg-8 col-xs-12 col-sm-8 pt-5'><a href='".SITE_URL."Setup/Administration/Actions/UserDetails/".$TaskData['AssignedTo']."'>".$TaskData['AssignedToName']."</a></div>";
            $EventTaskDettailHtml .= "<div class='col-md-4 col-lg-4 col-xs-12 col-sm-4 pt-5'><strong>Subject :</strong></div> <div class='col-md-8 col-lg-8 col-xs-12 col-sm-8 pt-5'><a href='".SITE_URL."Task/Actions/TaskDetails/".$TaskData['TaskID']."'>".$TaskData['Subject']."</a></div>";
            $EventTaskDettailHtml .= "<div class='col-md-4 col-lg-4 col-xs-12 col-sm-4 pt-5'><strong>Related To :</strong> </div> <div class='col-md-8 col-lg-8 col-xs-12 col-sm-8 pt-5'><a href='".SITE_URL.$TaskData['RelatedTo']."/Actions/".$TaskData['RelatedTo']."Details/".$ObjRelatedData->ID."'>". $ObjRelatedData->No."</a></div>";
            $EventTaskDettailHtml .= "<div class='col-md-4 col-lg-4 col-xs-12 col-sm-4 pt-5'><strong>Start Date Time :</strong> </div> <div class='col-md-8 col-lg-8 col-xs-12 col-sm-8 pt-5'>".$TaskData['StartDate']."</div>";
            $EventTaskDettailHtml .= "<div class='col-md-4 col-lg-4 col-xs-12 col-sm-4 pt-5'><strong>End Date Time :</strong> </div> <div class='col-md-8 col-lg-8 col-xs-12 col-sm-8 pt-5'>".$TaskData['EndDate']."</div>";
            $EventTaskDettailHtml .= "<div class='col-md-4 col-lg-4 col-xs-12 col-sm-4 pt-5'><strong>Status :</strong> </div> <div class='col-md-8 col-lg-8 col-xs-12 col-sm-8 pt-5'>".$StatusImage." <span style='padding-top: 3px;position: fixed;padding-left: 5px;'>". $Status."</span>&nbsp;</div>";
            $EventTaskDettailHtml .= "<div class='col-md-4 col-lg-4 col-xs-12 col-sm-4 pt-5'><strong>Priority :</strong> </div> <div class='col-md-8 col-lg-8 col-xs-12 col-sm-8 pt-5'>".$Priority.'</div>';
            $EventTaskDettailHtml .= "</div>";

            return $EventTaskDettailHtml;
        }
    } 

    function FilterData()
    {
        $this->db->select('wt.WorkOrderTypeID, wt.WorkOrderType');
        $this->db->from('WorkOrderType wt');
        $this->db->where(array('wt.IsDeleted' => 0, 'wt.OrganizationID' => $this->LoginOrganizationID));
        $Type = $this->db->get();
        $data['WOType'] = $Type->result();

        $this->db->select('wos.WOStatusID, wos.Status');
        $this->db->from('WOStatus wos');
        $this->db->where(array('wos.IsDeleted' => 0, 'wos.OrganizationID' => $this->LoginOrganizationID));
        $Status = $this->db->get();
        $data['WOStatus'] = $Status->result();

        $this->db->select('wop.WOPriorityID, wop.Priority');
        $this->db->from('WOPriority wop');
        $this->db->where(array('wop.IsDeleted' => 0, 'wop.OrganizationID' => $this->LoginOrganizationID));
        $Priority = $this->db->get();
        $data['WOPriority'] = $Priority->result();

        return $data;
    }

    function GetUnassignedUnscheduled()
    {
        $this->db->select('wo.*');
        $this->db->from('WorkOrder wo');
        $this->db->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left');
        $this->db->where(array('wo.AssignedTo' => NULL,'wo.StartDate is NOT NULL' => NULL, 'wo.IsDeleted' => 0, 'wo.OrganizationID' => $this->LoginOrganizationID));
        $Unassigned = $this->db->get();
        $data['Unassigned'] = $Unassigned->result();

        $this->db->select('wo.*');
        $this->db->from('WorkOrder wo');
        $this->db->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left');
        $this->db->where(array('wo.StartDate' => NULL, 'wos.IsClosed' => 0, 'wo.IsDeleted' => 0, 'wo.OrganizationID' => $this->LoginOrganizationID));
        $Unscheduled = $this->db->get();
        $data['Unscheduled'] = $Unscheduled->result();

        return $data;
    }

    function GetUsers()
    {
        $Users = $this->db->get_where('User', array('IsActive' => 1, 'IsDeleted' => 0, 'OrganizationID' => $this->LoginOrganizationID));
        return $Users->result();
    }

    function GetAssignToResource()
    {
        extract($_GET);

        $AssignedTo = (!empty($AssignedTo)?$AssignedTo:0);

        if(isset($AssignedTo)){
            $this->db->select('*');
            $this->db->from('User');
            $this->db->where(array('IsActive' => 1, 'IsDeleted' => 0, 'OrganizationID' => $this->LoginOrganizationID));
            $this->db->where_in('UserID', $AssignedTo);
            $query = $this->db->get();
        } else {
            $query = $this->db->get_where('User', array('IsActive' => 1, 'IsDeleted' => 0, 'OrganizationID' => $this->LoginOrganizationID));
        }
       
        $Users = $query->result();

        foreach ($Users as $key => $value) {
            $data['id'] = $value->UserID;
            $data['title'] = $value->FirstName.' '.$value->LastName;
            $data['eventColor'] = "#5fc55f";
            $FinalData[] = $data;
        }
        $data['id'] = 'NULL';
        $data['title'] = 'Unassigned Work Orders';
        $data['eventColor'] = "#8fc88f";
        $FinalData[] = $data;

        return json_encode($FinalData);
    }

    function UpdateWorkOrderDetails()
    {
        extract($_POST);
       
        $EndDate = date("Y-m-d H:i:s", strtotime($Date) + 7200);

        if($WorkOrderFlag == 'Unassigned'){

            $WorkOrder = $this->db->select('WorkOrderNo, Subject, PrimaryContact')
                        ->from('WorkOrder')
                        ->where(array('WorkOrderID' => $WorkOrderID, 'OrganizationID' => $this->LoginOrganizationID))
                        ->get();
            $WorkOrder = $WorkOrder->row();

            $data = array(
                'OrganizationID' => $this->LoginOrganizationID,
                'Subject' => $WorkOrder->WorkOrderNo.' - '.$WorkOrder->Subject,
                'RelatedTo' => 'WorkOrder',
                'What' => $WorkOrderID,
                'Who' => $WorkOrder->PrimaryContact,
                'AssignedTo' => ($AssignedTo=='NULL')?NULL:$AssignedTo,
                'EventStartDate' => date("Y-m-d H:i:s", strtotime($Date)),
                'EventEndDate' => date("Y-m-d H:i:s", strtotime($EndDate)),
                'EventStartTime' => date("H:i:s", strtotime($Date)),
                'EventEndTime' => date("H:i:s", strtotime($EndDate))
            );
            $result = $this->db->insert('Event', $data);  

            $EventID = $this->db->insert_id();

            MY_Model::insertCreatedModifiedByAndDt($this->LoginUserID,'Event', array('EventID' => $EventID));

            $this->UpdateWOStartEndDateTime($WorkOrderID);

            return $this->db->update('WorkOrder', array('AssignedTo' => $this->LoginUserID), array('WorkOrderID' => $WorkOrderID));  

        } else if($WorkOrderFlag == 'Unscheduled'){

            $WorkOrder = $this->db->select('WorkOrderNo, Subject, PrimaryContact')
                        ->from('WorkOrder')
                        ->where(array('WorkOrderID' => $WorkOrderID, 'OrganizationID' => $this->LoginOrganizationID))
                        ->get();
            $WorkOrder = $WorkOrder->row();
             
            $data = array(
                'OrganizationID' => $this->LoginOrganizationID,
                'Subject' => $WorkOrder->WorkOrderNo.' - '.$WorkOrder->Subject,
                'RelatedTo' => 'WorkOrder',
                'What' => $WorkOrderID,
                'Who' => $WorkOrder->PrimaryContact,
                'AssignedTo' => ($AssignedTo=='NULL')?NULL:$AssignedTo,
                'EventStartDate' => date("Y-m-d H:i:s", strtotime($Date)),
                'EventEndDate' => date("Y-m-d H:i:s", strtotime($EndDate)),
                'EventStartTime' => date("H:i:s", strtotime($Date)),
                'EventEndTime' => date("H:i:s", strtotime($EndDate))
            );
            $result = $this->db->insert('Event', $data);  
            $EventID = $this->db->insert_id();

            MY_Model::insertCreatedModifiedByAndDt($this->LoginUserID,'Event', array('EventID' => $EventID));

            $this->UpdateWOStartEndDateTime($WorkOrderID);

            return $result;
        }
    }

    function UpdateEventTaskDetails()
    {
        extract($_POST);
       
        //$EndDate = ($View == "month") ? date("Y-m-d H:i:s", strtotime($EndDate) + 3600) : $EndDate;

        if(isset($Type) && $Type == 'Event'){

            $query = $this->db->select('What')
            ->from('Event')
            ->where(array('RelatedTo' => 'WorkOrder', 'EventID' => $ID, 'OrganizationID' => $this->LoginOrganizationID))
            ->get(); 
            $Event = $query->row();
           
            $this->UpdateWOStartEndDateTime($Event->What);

            $data = array(
                'AssignedTo' => ($AssignedTo=='NULL')?NULL:$AssignedTo,
                'EventStartDate' => date("Y-m-d H:i:s", strtotime($StartDate)),
                'EventEndDate' => date("Y-m-d H:i:s", strtotime($EndDate)),
                'EventStartTime' => date("H:i:s", strtotime($StartDate)),
                'EventEndTime' => date("H:i:s", strtotime($EndDate))
            );

            $result = $this->db->update('Event', $data, array('EventID' => $ID, 'OrganizationID' => $this->LoginOrganizationID));  

            MY_Model::updateCreatedModifiedByAndDt($this->LoginUserID, 'Event', array('EventID' => $ID));

        } else if(isset($Type) && $Type == 'Task'){

            $query = $this->db->select('What')
            ->from('Task')
            ->where(array('RelatedTo' => 'WorkOrder', 'TaskID' => $ID, 'OrganizationID' => $this->LoginOrganizationID))
            ->get(); 
            $Task = $query->row();

            $this->UpdateWOStartEndDateTime($Task->What); 

            $data = array(
                'AssignedTo' => $this->LoginUserID,
                'DueDate' => date("Y-m-d H:i:s", strtotime($EndDate)),
            );
            $result = $this->db->update('Task', $data, array('TaskID' => $ID, 'OrganizationID' => $this->LoginOrganizationID));  

            MY_Model::updateCreatedModifiedByAndDt($this->LoginUserID, 'Task', array('TaskID' => $ID));

        }
        
    }
 
    function UpdateWOStartEndDateTime($WorkOrderID)
    {
        $UserID = $this->LoginUserID;
 
        $query = $this->db->select('MIN(EventStartDate) as MinEventStartDate, MAX(EventEndDate) as MaxEventEndDate, MIN(EventStartDate) as MinEventStartTime, MAX(EventEndDate) as MaxEventEndTime')
        ->from('Event')
        ->where(array('RelatedTo' => 'WorkOrder', 'What' => $WorkOrderID, 'OrganizationID' => $this->LoginOrganizationID))
        ->get(); 
        $Event = $query->row(); 
  
        $data = array(
            'StartDate' => date("Y-m-d H:i:s", strtotime($Event->MinEventStartDate)),
            'EndDate' => date("Y-m-d H:i:s", strtotime($Event->MaxEventEndDate)),
            'StartTime' => date("H:i:s", strtotime($Event->MinEventStartTime)),
            'EndTime' => date("H:i:s", strtotime($Event->MaxEventEndTime)),
        ); 
        $result = $this->db->update('WorkOrder', $data, array('WorkOrderID' => $WorkOrderID, 'OrganizationID' => $this->LoginOrganizationID));  

        MY_Model::updateCreatedModifiedByAndDt($UserID,'WorkOrder', array('WorkOrderID' => $WorkOrderID));
    }
}

?>
