<?php
 
class MapViewModel extends MY_Model{
 
    public $LoginUserID, $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }

    function Map()
    {
        extract($_POST);

        if ($ViewType == 'Day') {
            $FromDate = $Date;
            $ToDate = $Date;
        } else if ($ViewType == 'Week') {
            $DateArr = explode(" - ", ($Date));
            $FromDate = trim($DateArr[0]);
            $ToDate = trim($DateArr[1]);
        }
        
        $finaldata = array(); 

        $this->db->select('wo.*, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, wos.Status, wos.ColorCode, wop.Priority, wo.StartDate, wos.IsClosed, wo.AssignedTo');
        $this->db->from('WorkOrder wo');
        $this->db->join('User at','at.UserID = wo.AssignedTo', 'left');
        $this->db->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left');
        $this->db->join('WOPriority wop','wop.WOPriorityID = wo.WOPriority', 'left');
       
        if((isset($Unassigned) && $Unassigned == 1) && (isset($Unscheduled) && $Unscheduled == 1)){
             $this->db->where('((wo.AssignedTo IS NULL) OR (wo.StartDate IS NULL AND wos.IsClosed = 0))');
        } else if(isset($Unassigned) && $Unassigned == 1){
             /*$this->db->where("((DATE(wo.StartDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."') OR (DATE(wo.EndDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."') OR ('".date("Y-m-d", strtotime($FromDate))."' BETWEEN DATE(wo.StartDate) AND DATE(wo.EndDate)) OR ('".date("Y-m-d", strtotime($ToDate))."' BETWEEN DATE(wo.StartDate) AND DATE(wo.EndDate)))");*/
            $this->db->where("((DATE(wo.StartDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."') OR (DATE(wo.EndDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."'))");

            $this->db->where('((wo.AssignedTo IS NULL AND wo.StartDate IS NOT NULL))');

        } else if(isset($Unscheduled) && $Unscheduled == 1){
            $this->db->where('((wo.StartDate IS NULL AND wos.IsClosed = 0))');

            if(isset($AssignedTo) && !empty($AssignedTo))
                $this->db->where_in('wo.AssignedTo', $AssignedTo);
            else 
                $this->db->where_in('wo.AssignedTo', 0);
        } else {
            /*$this->db->where("((DATE(wo.StartDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."') OR (DATE(wo.EndDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."') OR ('".date("Y-m-d", strtotime($FromDate))."' BETWEEN DATE(wo.StartDate) AND DATE(wo.EndDate)) OR ('".date("Y-m-d", strtotime($ToDate))."' BETWEEN DATE(wo.StartDate) AND DATE(wo.EndDate)))");*/
            $this->db->where("((DATE(wo.StartDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."') OR (DATE(wo.EndDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."'))");

            if(isset($AssignedTo) && !empty($AssignedTo))
                $this->db->where_in('wo.AssignedTo', $AssignedTo);
            else 
                $this->db->where_in('wo.AssignedTo', 0);
        }

        if(isset($WorkOrderType))
            $this->db->where_in('wo.WorkOrderType', $WorkOrderType);
        else
            $this->db->where_in('wo.WorkOrderType', 0);
        if(isset($WOStatus))
            $this->db->where_in('wo.WOStatus', $WOStatus);
        else
            $this->db->where_in('wo.WOStatus', 0);
        if(isset($WOPriority))
            $this->db->where_in('wo.WOPriority', $WOPriority);
        else
            $this->db->where_in('wo.WOPriority', 0);
       
        $this->db->where(array('wo.IsDeleted' => 0, 'wo.OrganizationID' => $this->LoginOrganizationID));
        $WorkOrderQuery =  $this->db->get();   

        //echo $this->db->last_query(); exit;
        
        $WorkOrders = $WorkOrderQuery->result_array();

        foreach ($WorkOrders as $key => $WorkOrder) {

            $StartDate = is_null($WorkOrder['StartDate'])?'':date("m/d/Y h:i A",strtotime($WorkOrder['StartDate']));
            $EndDate = is_null($WorkOrder['StartDate'])?"":date("m/d/Y h:i A",strtotime($WorkOrder['EndDate']));
            $WorkOrderInfo ="";
            $WorkOrderInfo .= "<div class='pt-5'><strong>Subject :</strong> <a href='".SITE_URL."WorkOrder/Actions/WorkOrderDetails/".$WorkOrder['WorkOrderID']."'>". $WorkOrder['Subject']."</a></div>";
            $WorkOrderInfo .= "<div class='pt-5'><strong>Assigned To :</strong> <a href='".SITE_URL."Setup/Administration/Actions/UserDetails/".$WorkOrder['AssignedTo']."'>". $WorkOrder['AssignedToName']."</a></div>";
            $WorkOrderInfo .= "<div class='pt-5'><strong>Related To :</strong> <a href='".SITE_URL."WorkOrder/Actions/WorkOrderDetails/".$WorkOrder['WorkOrderID']."'>". $WorkOrder['WorkOrderNo']."</a></div>";
            $WorkOrderInfo .= "<div class='pt-5'><strong>Status :</strong> ". $WorkOrder['Status']."</div>";
            $WorkOrderInfo .= "<div class='pt-5'><strong>Priority :</strong> ". $WorkOrder['Priority'].'</div>';
            $WorkOrderInfo .= "<div class='pt-5'><strong>Start DateTime :</strong> ".$StartDate."</div>";
            $WorkOrderInfo .= "<div class='pt-5'><strong>End DateTime :</strong> ".$EndDate."</div>";

            $data['WorkOrderID'] = $WorkOrder['WorkOrderID'];
            $data['AssignedTo'] = $WorkOrder['AssignedTo'];
            $data['Title'] = $WorkOrderInfo;
            $data['Lat'] = $WorkOrder['Latitude'];
            $data['Long'] = $WorkOrder['Longitude'];
            $data['Priority'] = $WorkOrder['Priority'];
            $data['Color'] = $WorkOrder['ColorCode'];
            $data['StartDate'] = $WorkOrder['StartDate'];
            $data['IsClosed'] = $WorkOrder['IsClosed'];
            $finaldata[] = $data;
        }
        return json_encode($finaldata);
    } 

    function GetTechnicians()
    {
        extract($_POST);

        $finaldata = array();

        if(isset($AssignedTo)){ 

            $this->db->select('u.*');
            $this->db->from('User u');
            $this->db->where_in('u.UserID', $AssignedTo);
            $this->db->where(array('u.IsDeleted' => 0, 'u.IsActive' => 1, 'u.OrganizationID' => $this->LoginOrganizationID));
            $Technicians =  $this->db->get();   
            //echo $this->db->last_query(); exit;
            
            $Technicians = $Technicians->result_array();

            foreach ($Technicians as $key => $Technician) {

                $TechnicianInfo ="";
                $TechnicianInfo .= "<div class='pt-5'><strong>Technician Name :</strong> <a href='".SITE_URL."Setup/Administration/Actions/UserDetails/".$Technician['UserID']."'>".$Technician['FirstName'].' '.$Technician['LastName']."</a></div>";

                $data['UserID'] = $Technician['UserID'];
                $data['Title'] = $TechnicianInfo;
                $data['Lat'] = $Technician['Latitude'];
                $data['Long'] = $Technician['Longitude'];
                $data['Color'] = '#4a235a';
                $finaldata[] = $data;
            }
        }
        return json_encode($finaldata);
    }

    function DateCompareDesc($a, $b)
    {
        $t1 = strtotime($a['Time']);
        $t2 = strtotime($b['Time']);
        return $t2 - $t1;
    } 

    function FilterData()
    {
        $this->db->select('wt.WorkOrderTypeID, wt.WorkOrderType');
        $this->db->from('WorkOrderType wt');
        $this->db->where(array('wt.IsDeleted' => 0, 'wt.OrganizationID' => $this->LoginOrganizationID));
        $Type = $this->db->get();
        $data['WOType'] = $Type->result();

        $this->db->select('wos.WOStatusID, wos.Status, wos.ColorCode');
        $this->db->from('WOStatus wos');
        $this->db->where(array('wos.IsDeleted' => 0, 'wos.OrganizationID' => $this->LoginOrganizationID));
        $Status = $this->db->get();
        $data['WOStatus'] = $Status->result();

        $this->db->select('wop.WOPriorityID, wop.Priority');
        $this->db->from('WOPriority wop');
        $this->db->where(array('wop.IsDeleted' => 0, 'wop.OrganizationID' => $this->LoginOrganizationID));
        $Priority = $this->db->get();
        $data['WOPriority'] = $Priority->result();

        return $data;
    }

    function GetUsers()
    {
        $Users = $this->db->get_where('User', array('IsActive' => 1, 'IsDeleted' => 0, 'OrganizationID' => $this->LoginOrganizationID));
        return $Users->result();
    }
}

?>
