<?php
 
class ListViewModel extends MY_Model{
 
    public $LoginUserID, $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }

    function GetList()
    {
        extract($_POST);

        $TaskData = array(); 

        if(isset($ShowTasks) && $ShowTasks == 1)
        {
            $this->db->select('t.TaskID as ID, "Task" as Object, t.Subject, t.DueDate as StartDate, t.DueDate as EndDate, ts.TaskStatus as Status, tp.Priority, t.Subject, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, t.City, t.State, t.RelatedTo, t.What, t.AssignedTo');
            $this->db->from('Task t');
            $this->db->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left');
            $this->db->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left');
            $this->db->join('User at','at.UserID = t.AssignedTo', 'left');
            $this->db->where(array('t.OrganizationID' => $this->LoginOrganizationID, 'ts.IsClosed' => 0, 't.IsDeleted' => 0));
            if($ViewType == 'Day')
                $this->db->where('DATE(t.DueDate)', date("Y-m-d", strtotime($Date)));
            else if($ViewType == 'Week'){
                $DateArr = explode("-", trim($Date));
                $FromDate = trim($DateArr[0]);
                $ToDate = trim($DateArr[1]);
                $this->db->where("(DATE(t.DueDate) >= '".date("Y-m-d", strtotime($FromDate))."' AND DATE(t.DueDate) <= '".date("Y-m-d", strtotime($ToDate))."')");
            } else if($ViewType == 'Month'){
                $DateArr = explode("/", trim($Date));

                $FromDate = date("".$DateArr[1]."-".$DateArr[0]."-01");
                $ToDate = date('Y-m-t',strtotime($FromDate));
                $this->db->where("(DATE(t.DueDate) >= '".date("Y-m-d", strtotime($FromDate))."' AND DATE(t.DueDate) <= '".date("Y-m-d", strtotime($ToDate))."')");
            } 
            if(isset($AssignedTo) && !empty($AssignedTo))
                $this->db->where_in('t.AssignedTo', $AssignedTo);
            else 
                $this->db->where_in('t.AssignedTo', 0);

            $TaskQuery = $this->db->get(); 
            //echo $this->db->last_query(); exit;

            $TaskData = $TaskQuery->result_array();
        }

        $this->db->select('WorkOrderID');
        $this->db->from('WorkOrder');
        if(isset($WorkOrderType) && !empty($WorkOrderType))
            $this->db->where_in('WorkOrderType', $WorkOrderType);
        else
            $this->db->where_in('WorkOrderType', 0);
        if(isset($WOStatus) && !empty($WOStatus))
            $this->db->where_in('WOStatus', $WOStatus);
        else
            $this->db->where_in('WOStatus', 0);
        if(isset($WOPriority))
            $this->db->where_in('WOPriority', $WOPriority);
        else
            $this->db->where_in('WOPriority', 0);
        $this->db->where(array('IsDeleted' => 0, 'OrganizationID' => $this->LoginOrganizationID));
        $WorkOrderQuery = $this->db->get(); 
        //echo $this->db->last_query(); exit;
        $WorkOrderRes = $WorkOrderQuery->result();
        foreach ($WorkOrderRes as $key => $value) {
           $WorkOrderIDs[] = $value->WorkOrderID;
        }

        $this->db->select('e.EventID as ID, "Event" as Object, e.Subject, e.EventStartDate as StartDate, e.EventEndDate as EndDate, es.EventStatus as Status, ep.Priority, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, e.City, e.State, e.RelatedTo, e.What, e.AssignedTo'); 
        $this->db->from('Event e');
        $this->db->join('EventType et','et.EventTypeID = e.EventType', 'left');
        $this->db->join('EventStatus es','es.EventStatusID = e.EventStatus', 'left');
        $this->db->join('EventPriority ep','ep.EventPriorityID = e.EventPriority', 'left');
        $this->db->join('User at','at.UserID = e.AssignedTo', 'left');
        if($ViewType == 'Day')
            $this->db->where("(DATE(e.EventStartDate) <= '".date("Y-m-d", strtotime($Date))."' AND DATE(e.EventEndDate) >= '".date("Y-m-d", strtotime($Date))."')");
        else if($ViewType == 'Week'){
            $DateArr = explode("-", ($Date));
            $FromDate = trim($DateArr[0]);
            $ToDate = trim($DateArr[1]);
           /* $this->db->where("((DATE(e.EventStartDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."') OR (DATE(e.EventEndDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."') OR ('".date("Y-m-d", strtotime($FromDate))."' BETWEEN DATE(e.EventStartDate) AND DATE(e.EventEndDate)) OR ('".date("Y-m-d", strtotime($ToDate))."' BETWEEN DATE(e.EventStartDate) AND DATE(e.EventEndDate)))");*/
           $this->db->where("((DATE(e.EventStartDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."') OR (DATE(e.EventEndDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."'))");
        } else if($ViewType == 'Month'){
            $DateArr = explode("/", trim($Date));

            $FromDate = date("".$DateArr[1]."-".$DateArr[0]."-01");
            $ToDate = date('Y-m-t',strtotime($FromDate));
            /*$this->db->where("((DATE(e.EventStartDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."') OR (DATE(e.EventEndDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."') OR ('".date("Y-m-d", strtotime($FromDate))."' BETWEEN DATE(e.EventStartDate) AND DATE(e.EventEndDate)) OR ('".date("Y-m-d", strtotime($ToDate))."' BETWEEN DATE(e.EventStartDate) AND DATE(e.EventEndDate)))");*/
            $this->db->where("((DATE(e.EventStartDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."') OR (DATE(e.EventEndDate) BETWEEN '".date("Y-m-d", strtotime($FromDate))."' AND '".date("Y-m-d", strtotime($ToDate))."'))");
        }

        $this->db->where("IF(e.RelatedTo = 'WorkOrder', IF(e.What IN ('".implode("','", $WorkOrderIDs)."'),1,0), 1)");

        if(isset($AssignedTo) && !empty($AssignedTo))
            $this->db->where_in('e.AssignedTo', $AssignedTo);
        else 
            $this->db->where_in('e.AssignedTo', 0);

        $this->db->where(array('e.IsDeleted' => 0, 'e.OrganizationID' => $this->LoginOrganizationID));
        $EventQuery =  $this->db->get();   
        //echo $this->db->last_query(); exit;
        $OpenActivities = array_merge($TaskData, $EventQuery->result_array());

        usort($OpenActivities, array($this,'DateCompareDesc'));

        /* Fetch data from Related Object */
        $RelatedDataArr = MY_Model::getRelatedData();

        $DayDataRow = "";

        $SapeArr = array('Low' => 'circle', 'High' => 'star', 'Normal' => 'square',);

        foreach ($OpenActivities as $key => $OpenActivity) {

            $ObjRelatedData = $RelatedDataArr[$OpenActivity['RelatedTo']][$OpenActivity['What']];
            $Status = isset($ObjRelatedData->Status)?$ObjRelatedData->Status:$OpenActivity['Status'];
            $Priority = isset($ObjRelatedData->Priority)?$ObjRelatedData->Priority:$OpenActivity['Priority'];
            $StatusImage = ($OpenActivity['RelatedTo'] == 'WorkOrder')?'<i class="fa fa-'.$SapeArr[$ObjRelatedData->Priority].' fa-2x" style="color:'.$ObjRelatedData->ColorCode.';"></i>':"";;

            $DayDataRow .= "<tr>";
            $DayDataRow .= "<td class='text-center'><a href='".SITE_URL."Setup/Administration/Actions/UserDetails/".$OpenActivity['AssignedTo']."''>".$OpenActivity['AssignedToName']."</a></td>";
            $DayDataRow .= "<td><a href='".SITE_URL.$OpenActivity['Object']."/Actions/".$OpenActivity['Object']."Details/".$OpenActivity['ID']."'>".$OpenActivity['Subject']."</a></td>";
            $DayDataRow .= "<td class='text-center'><a href='".SITE_URL.$OpenActivity['RelatedTo']."/Actions/".$OpenActivity['RelatedTo']."Details/".$ObjRelatedData->ID."'>". $ObjRelatedData->No."</a></td>";
            $DayDataRow .= "<td class='text-center'>".date("m/d/Y h:i A",strtotime($OpenActivity['StartDate']))."</td>";
            $DayDataRow .= "<td class='text-center'>".date("m/d/Y h:i A",strtotime($OpenActivity['EndDate']))."</td>";
            $DayDataRow .= "<td class='text-center' class='text-center'>".$Status."</td>";
            $DayDataRow .= "<td class='text-center'>".$StatusImage."</td>";
            $DayDataRow .= "<td class='text-center'>".$Priority."</td>";
            $DayDataRow .= "</tr>";
        }

        return $DayDataRow;
    } 

    function DateCompareDesc($a, $b)
    {
        $t1 = strtotime($a['StartDate']);
        $t2 = strtotime($b['StartDate']);
        return $t2 - $t1;
    } 

    function FilterData()
    {
        $this->db->select('wt.WorkOrderTypeID, wt.WorkOrderType');
        $this->db->from('WorkOrderType wt');
        $this->db->where(array('wt.IsDeleted' => 0, 'wt.OrganizationID' => $this->LoginOrganizationID));
        $Type = $this->db->get();
        $data['WOType'] = $Type->result();

        $this->db->select('wos.WOStatusID, wos.Status');
        $this->db->from('WOStatus wos');
        $this->db->where(array('wos.IsDeleted' => 0, 'wos.OrganizationID' => $this->LoginOrganizationID));
        $Status = $this->db->get();
        $data['WOStatus'] = $Status->result();

        $this->db->select('wop.WOPriorityID, wop.Priority');
        $this->db->from('WOPriority wop');
        $this->db->where(array('wop.IsDeleted' => 0, 'wop.OrganizationID' => $this->LoginOrganizationID));
        $Priority = $this->db->get();
        $data['WOPriority'] = $Priority->result();

        return $data;
    }

    function GetUsers()
    {
        $Users = $this->db->get_where('User', array('IsActive' => 1, 'IsDeleted' => 0, 'OrganizationID' => $this->LoginOrganizationID));
        return $Users->result();
    }
}

?>
