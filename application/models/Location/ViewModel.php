<?php

class ViewModel extends MY_Model{
 
    public $LoginUserID, $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }
 
    function ViewLocations() 
    {
        $aColumns = array('l.LocationID','l.LocationNo','l.Name', 'lt.LocationType', 'ac.AccountName', 'CONCAT(pc.FirstName, " ", pc.LastName)','l.IsActive', 'CONCAT(pt.FirstName, " ", pt.LastName)', 'l.City', 'l.State', 'DATE_FORMAT(l.CreatedDate, "'.RES_DATETIME.'")', 'CONCAT(cb.FirstName, " ", cb.LastName)','l.Account','l.PrimaryContact'); 
        $bColumns = array('LocationID','LocationNo', 'Name', 'LocationType', 'AccountName', 'CONCAT(pc.FirstName, " ", pc.LastName)', 'IsActive', 'CONCAT(pt.FirstName, " ", pt.LastName)', 'City', 'State', 'DATE_FORMAT(l.CreatedDate, "'.RES_DATETIME.'")', 'CONCAT(cb.FirstName, " ", cb.LastName)', 'Account','PrimaryContact');  
      
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "l.LocationID";

        /* DB table to use */
        $sTable = "Location l
        LEFT JOIN User cb ON cb.UserID = l.CreatedBy
        LEFT JOIN Account ac ON ac.AccountID = l.Account  
        LEFT JOIN Contact pc ON pc.ContactID = l.PrimaryContact
        LEFT JOIN LocationType lt ON lt.LocationTypeID = l.LocationType
        LEFT JOIN User pt ON pt.UserID = l.PreferredTechnician";

        /* Limit */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".( $_GET['iDisplayStart'] ).", ".
                        ( $_GET['iDisplayLength'] );
        }

        /* Ordering */
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                    {
                            $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                                    ".( $_GET['sSortDir_'.$i] ) .", ";
                    }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                    $sOrder = "";
            }
        }

        /* Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited */
        $sWhere = "";
        if ( $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                        $sWhere .= $aColumns[$i]." LIKE '%".( $_GET['sSearch'] )."%' OR ";
                }
                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ')';
        }
        
        /* Where Condition */
        $UserID = $this->session->userdata('UserID');
        if(isset($_GET['flag']) && $_GET['flag'] == 'MyLocations'){

            $where_str = "l.Owner = $UserID AND l.OrganizationID='".$this->LoginOrganizationID."' AND l.IsDeleted=0 AND l.IsActive=1";
        }
        else if(isset($_GET['flag']) && $_GET['flag'] == 'AllLocations'){

            $where_str = "l.OrganizationID='".$this->LoginOrganizationID."' AND l.IsDeleted=0 AND l.IsActive=1";

        } else if(isset($_GET['flag']) && $_GET['flag'] == 'LocationsCreatedThisWeek'){

            $where_str = "YEARWEEK(l.CreatedDate, 1) = YEARWEEK(CURDATE(), 1) AND l.OrganizationID='".$this->LoginOrganizationID."' AND l.IsDeleted=0 AND l.IsActive=1";

        } else {
            $where_str = "l.IsDeleted=0 AND l.OrganizationID='".$this->LoginOrganizationID."'";
        }
        
        if ( $sWhere == "" )
        {
                $sWhere = "WHERE $where_str";
        }
        else
        {
                $sWhere .= "AND $where_str";
        }
        
        /* SQL queries Get data to display */
        $sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit"; 
        //echo $sQuery;exit;
        $rResult = $this->db->query($sQuery); 
        $rResult = $rResult->result_array(); 
      
        /* Data set length after filtering */
        $sQuery = "SELECT FOUND_ROWS() as cnt";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iFilteredTotal = $result->cnt;

        /* Total data set length */
        $sQuery = "SELECT COUNT(".$sIndexColumn.") as cnt
                FROM   $sTable  $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iTotal = $result->cnt;

        /* Output */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );

        foreach ( $rResult as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($bColumns) ; $i++ )
            {
                if($i == 0)
                {
                    $row[] = '<a href="'.SITE_URL.'Location/Actions/EditLocation/'.$aRow[$bColumns[0]].'"><i class="fa fa-pencil-square-o"></i></a>';
                } 
                else if($i == 7)
                {
                     $row[] = '<a href="#">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($bColumns[$i] == 'AccountName')
                {
                     $row[] = '<a href="'.SITE_URL.'Account/Actions/AccountDetails/'.$aRow['Account'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($i == 5)
                {
                     $row[] = '<a href="'.SITE_URL.'Contact/Actions/ContactDetails/'.$aRow['PrimaryContact'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($bColumns[$i] == 'IsActive')
                {
                    $str = ($aRow[$bColumns[$i]]==1)?"checked":"";
                    $row[] = '<div class="checkbox checkbox-primary">
                                <input type="checkbox" '.$str.' disabled="disabled">
                                <label for="checkbox2"></label>
                            </div>';
                } 
                else if($bColumns[$i] == 'LocationNo' || $bColumns[$i] == 'Name')
                {
                    $row[] = '<a href="'.SITE_URL.'Location/Actions/LocationDetails/'.$aRow[$bColumns[0]].'">'.$aRow[$bColumns[$i]].'</a>';
                }
                else if ( $bColumns[$i] != ' ' )
                {
                    $row[] = $aRow[$bColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode( $output );
    } 

    function CreateNewView()
    {
        extract($_POST);

        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'LocationViewName' => $LocationViewName,
            'UserID' => $this->LoginUserID,
            'RestrictVisibility' => $RestrictVisibility,
            'SpecifyFieldsDisplay' => implode(",", $SpecifyFieldsDisplay),
            'CreatedDate' => date("Y-m-d H:i:s"),
            'LastModifiedDate' => date("Y-m-d H:i:s")
        );     
        $query = $this->db->insert('LocationView', $data);  
        $LocationViewID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($this->LoginUserID, 'LocationView', array('LocationViewID' => $LocationViewID));

        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'LocationViewID' => $LocationViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('LocationFilter', $data);  

                   
                }
            } 
 
        }
        return $LocationViewID;
    }

    function EditCustomView($LocationViewID)
    {  
        extract($_POST);
  
        $data = array(
            'LocationViewName' => $LocationViewName,
            'UserID' => $this->LoginUserID,
            'RestrictVisibility' => $RestrictVisibility,
            'SpecifyFieldsDisplay' => implode(",", $SpecifyFieldsDisplay),
            'CreatedDate' => date("Y-m-d H:i:s"),
            'LastModifiedDate' => date("Y-m-d H:i:s")
        );     
        $query = $this->db->update('LocationView', $data, array('LocationViewID' => $LocationViewID));  

        $this->db->delete('LocationFilter', array('LocationViewID' => $LocationViewID));  
        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'LocationViewID' => $LocationViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('LocationFilter', $data);  
                }
            } 
 
        }
        return $query;
    }

    function GetLocationViewData($LocationViewID){

        $query = $this->db->get_where('LocationView', array('OrganizationID' => $this->LoginOrganizationID, 'LocationViewID' => $LocationViewID)); 
        $LocationViewData = $query->row();

        $query = $this->db->get_where('LocationFilter', array('LocationViewID' => $LocationViewID)); 
        $LocationFilterData = $query->result_array();

        $ColName = "l.LocationID, ac.AccountID, pc.ContactID, pt.UserID as PreferredTechnicianID, pl.LocationID as ParentLocationID, ";
        if(!empty($LocationViewData->SpecifyFieldsDisplay)){
            $SpecifyFieldsDisplays = explode(",", $LocationViewData->SpecifyFieldsDisplay);
            foreach ($SpecifyFieldsDisplays as $key => $value) {
                if($value == 'Owner'){
                    $ColName .= "CONCAT(ow.FirstName, ' ', ow.LastName) as Owner";
                } else if($value == 'Account'){
                    $ColName .= "ac.AccountName as Account";
                } else if($value == 'OrganizationID'){
                    $ColName .= "o.CompanyName as OrganizationID";
                } else if($value == 'LocationType'){
                    $ColName .= "lt.LocationType as LocationType";
                } else if($value == 'ParentLocation'){
                    $ColName .= "pl.Name as ParentLocation";
                } else if($value == 'PrimaryContact'){
                    $ColName .= "CONCAT(pc.FirstName, ' ', pc.LastName) as PrimaryContact";
                } else if($value == 'PreferredTechnician'){
                    $ColName .= "CONCAT(pt.FirstName, ' ', pt.LastName) as PreferredTechnician";
                } else if($value == 'CreatedBy'){
                    $ColName .= "CONCAT(cr.FirstName, ' ', cr.LastName) as CreatedBy";
                } else if($value == 'LastModifiedBy'){
                    $ColName .= "CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedBy";
                } else {
                    $ColName .= "l.".$value;
                }

                if($key != count($SpecifyFieldsDisplays)){
                    $ColName .= ", ";
                }
            }
        }
            
         /* Filter Location Data Start */
            $this->db->select($ColName);
            $this->db->from('Location l');
            $this->db->join('User ow','ow.UserID = l.Owner', 'left');
            $this->db->join('Account ac','ac.AccountID = l.Account', 'left');
            $this->db->join('User pt','pt.UserID = l.PreferredTechnician', 'left');
            $this->db->join('Organization o','o.OrganizationID = l.OrganizationID', 'left');
            $this->db->join('Location pl','pl.LocationID = l.ParentLocation', 'left');
            $this->db->join('LocationType lt','lt.LocationTypeID = l.LocationType', 'left');
            $this->db->join('User cr','cr.UserID = l.CreatedBy', 'left');
            $this->db->join('User lmb','lmb.UserID = l.LastModifiedBy', 'left');
            $this->db->join('Contact pc','pc.ContactID = l.PrimaryContact', 'left');
            $this->db->where(array('l.OrganizationID' => $this->LoginOrganizationID, 'l.IsDeleted' => 0));

            foreach ($LocationFilterData as $key => $value) {

                $WhereField = "";
                if($value['FilterField'] == 'Owner'){
                    $WhereField = "CONCAT(ow.FirstName, ' ', ow.LastName)";
                } else if($value['FilterField'] == 'Account') {
                    $WhereField = "ac.AccountName";
                } else if($value['FilterField'] == 'ParentLocation') {
                    $WhereField = "pl.Name";
                } else if($value['FilterField'] == 'OrganizationID') {
                    $WhereField = "o.CompanyName";
                } else if($value['FilterField'] == 'LocationType') {
                    $WhereField = "lt.LocationType";
                } else if($value['FilterField'] == 'PreferredTechnician') {
                    $WhereField = "CONCAT(pt.FirstName, ' ', pt.LastName)";
                } else if($value['FilterField'] == 'CreatedBy') {
                    $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                } else if($value['FilterField'] == 'LastModifiedBy') {
                    $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                } else if($value['FilterField'] == 'PrimaryContact') {
                    $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                } else {
                    $WhereField = "l.".$value['FilterField'];
                }

                if($value['FilterCondition'] == 'Equals'){
                    $this->db->where($WhereField.' =',  $value['FilterValue']);  
                } else if($value['FilterCondition'] == 'NotEqualTo'){
                    $this->db->where($WhereField.' !=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'LessThan'){
                    $this->db->where($WhereField.' <', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'GreaterThan'){
                    $this->db->where($WhereField.' >', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'LessOREqualTo'){
                    $this->db->where($WhereField.' <=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'GreaterOREqualTo'){
                    $this->db->where($WhereField.' >=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'Contains'){
                    $this->db->like($WhereField, $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'StartsWith'){ 
                    $this->db->like($WhereField, $value['FilterValue'], 'after');   
                } else if($value['FilterCondition'] == 'DoesNotContain'){
                    $this->db->not_like($WhereField, $value['FilterValue']);   
                } 
            } 
            $query = $this->db->get();  

            //    echo $this->db->last_query();exit;
            $data['ColumnsNames'] = $SpecifyFieldsDisplays;
            $data['ColumnsValues'] = $query->result();

            return $data; 
    }

    function GetCustomView($LocationViewID)
    {
        $query = $this->db->get_where('LocationView', array('OrganizationID' => $this->LoginOrganizationID, 'LocationViewID' => $LocationViewID)); 
        
        $data['LocationViewData'] = $query->row();

        $query = $this->db->get_where('LocationFilter', array('LocationViewID' => $LocationViewID)); 
        
        $data['LocationFilterData'] = $query->result();

        return $data;
    }

    function GetViews(){

        $query = $this->db->query("SELECT * FROM LocationView WHERE (RestrictVisibility = 'VisibleOnlyToMe' AND UserID = '".$this->LoginUserID."') OR (RestrictVisibility = 'VisibleToEveryone') AND OrganizationID = '".$this->LoginOrganizationID."' ORDER BY LocationViewName ASC");
        
        return $query->result();
    }

    function LocationViewFields()
    {
        $query = $this->db->get_where('LocationViewFields'); 
        return $query->result();
    }

    function DeleteCustomView($LocationViewID)
    {
        return $this->db->delete('LocationView', array('LocationViewID' => $LocationViewID)); 
    }

    function CopyCustomView($LocationViewID)
    {
       $query = $this->db->get_where('LocationView', array('LocationViewID' => $LocationViewID)); 
      
        foreach ($query->result() as $row){   
           foreach($row as $key=>$val){ 
                if($key != 'LocationViewID'){    
                    $this->db->set($key, $val); 
                }                
           }
        }
        $this->db->insert('LocationView');
        $NewLocationViewID = $this->db->insert_id();

        $query = $this->db->get_where('LocationFilter', array('LocationViewID' => $LocationViewID)); 
       
        foreach ($query->result() as $row){   
           foreach($row as $key => $val){ 
                if($key == 'LocationViewID'){  
                    $this->db->set($key, $NewLocationViewID);  
                } else if($key != 'LocationFilterID'){    
                    $this->db->set($key, $val); 
                }             
           }
           $this->db->insert('LocationFilter');
        }

        return 1; 
    }

    function RenameCustomView($LocationViewID){

        $data = array(
            'LocationViewName' => $_POST['LocationViewName']
        );

        return $this->db->update('LocationView', $data, array('LocationViewID' => $LocationViewID));  
    }

    function EditSharingCustomView($LocationViewID){

        $data = array(
            'RestrictVisibility' => $_REQUEST['RestrictVisibility']
        );

        return $this->db->update('LocationView', $data, array('LocationViewID' => $LocationViewID));  
    }
 
    function EditFiltersCustomView($LocationViewID){ 

        extract($_POST);

        $this->db->delete('LocationFilter', array('LocationViewID' => $LocationViewID));  
        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'LocationViewID' => $LocationViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('LocationFilter', $data);  
                }
            } 
 
        }
        return $query; 
    }

    function EditDisplayedColumnsCustomView($LocationViewID){

        $data = array(
            'SpecifyFieldsDisplay' => implode(",", $_REQUEST['SpecifyFieldsDisplay'])
        );

        return $this->db->update('LocationView', $data, array('LocationViewID' => $LocationViewID));  
    }
    
}

?>
