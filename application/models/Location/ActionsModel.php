<?php

class ActionsModel extends MY_Model{
 
    public $LoginUserID, $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }
 
    function RecentLocations() 
    {
        $aColumns = array('l.LocationID','l.LocationNo','l.Name', 'lt.LocationType', 'ac.AccountName', 'CONCAT(o.FirstName, " ", o.LastName)','l.IsActive', 'CONCAT(pt.FirstName, " ", pt.LastName)', 'l.City', 'l.State','l.Account'); 
        $bColumns = array('LocationID','LocationNo', 'Name', 'LocationType', 'AccountName', 'CONCAT(o.FirstName, " ", o.LastName)', 'IsActive', 'CONCAT(pt.FirstName, " ", pt.LastName)', 'City', 'State','Account'); 
        
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "l.LocationID";

        /* DB table to use */
        $sTable = "Location l
        LEFT JOIN Account ac ON ac.AccountID = l.Account  
        LEFT JOIN User o ON o.UserID = l.Owner
        LEFT JOIN LocationType lt ON lt.LocationTypeID = l.LocationType
        LEFT JOIN User pt ON pt.UserID = l.PreferredTechnician";

        /* Limit */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".( $_GET['iDisplayStart'] ).", ".
                        ( $_GET['iDisplayLength'] );
        }

        /* Ordering */
        $sOrder = "ORDER BY l.LastModifiedDate DESC";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                    {
                            $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                                    ".( $_GET['sSortDir_'.$i] ) .", ";
                    }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                    $sOrder = "";
            }
        }

        /* Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited */
        $sWhere = "";
        if ( $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                        $sWhere .= $aColumns[$i]." LIKE '%".( $_GET['sSearch'] )."%' OR ";
                }
                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ')';
        }
        
        /* Where Condition */
        $UserID = $this->session->userdata('UserID');
       
        $where_str = "l.IsDeleted=0 AND l.OrganizationID = '".$this->LoginOrganizationID."'";
        if ( $sWhere == "" )
        {
                $sWhere = "WHERE $where_str";
        }
        else
        {
                $sWhere .= "AND $where_str";
        }
        
        /* SQL queries Get data to display */
        $sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit"; 

        $rResult = $this->db->query($sQuery); 
        $rResult = $rResult->result_array(); 
      
        /* Data set length after filtering */
        $sQuery = "SELECT FOUND_ROWS() as cnt";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iFilteredTotal = $result->cnt;

        /* Total data set length */
        $sQuery = "SELECT COUNT(".$sIndexColumn.") as cnt
                FROM   $sTable  $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iTotal = $result->cnt;

        /* Output */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );

        foreach ( $rResult as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($bColumns) ; $i++ )
            {
                if($i == 0)
                {
                    $row[] = '';
                }
                else if($i == 5 || $i == 7)
                {
                     $row[] = '<a href="#">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($bColumns[$i] == 'AccountName')
                {
                     $row[] = '<a href="'.SITE_URL.'Account/Actions/AccountDetails/'.$aRow['Account'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($bColumns[$i] == 'IsActive')
                {
                    $str = ($aRow[$bColumns[$i]]==1)?"checked":"";
                    $row[] = '<div class="checkbox checkbox-primary">
                                <input type="checkbox" '.$str.' disabled="disabled">
                                <label for="checkbox2"></label>
                            </div>';
                } 
                else if($bColumns[$i] == 'LocationNo' || $bColumns[$i] == 'Name')
                {
                    $row[] = '<a href="'.SITE_URL.'Location/Actions/LocationDetails/'.$aRow[$bColumns[0]].'">'.$aRow[$bColumns[$i]].'</a>';
                }
                else if ( $bColumns[$i] != ' ' )
                {
                    $row[] = $aRow[$bColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode( $output );
    } 

    function CreateLocation() {
               
        extract($_POST); 

        $UserID = $this->LoginUserID;

        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'Name' => $Name,
            'Account' => $Account,
            'PrimaryContact' => !empty($PrimaryContact)?$PrimaryContact:NULL,
            'Owner' => $Owner,
            'PreferredTechnician' => !empty($PreferredTechnician)?$PreferredTechnician:NULL,
            'ParentLocation' => !empty($ParentLocation)?$ParentLocation:NULL,
            'Address' => $Address,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => !empty($Latitude)?$Latitude:NULL,
            'Longitude' => !empty($Longitude)?$Longitude:NULL,
            'LocationType' => $LocationType,
            'AccessNotes' => $AccessNotes,
            'Notes' => $Notes,
        );  

        $result = $this->db->insert('Location', $data); 
        
        $LocationID = $this->db->insert_id();

        $LocationNo = MY_Controller::genCode('LC', $LocationID);
        $this->db->update('Location', array('LocationNo' => $LocationNo), array('LocationID' => $LocationID));  

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Location', array('LocationID' => $LocationID));

        return $result;
    } 
 
    function EditLocation($LocationID) {
               
        extract($_POST); 

        $UserID = $this->LoginUserID;
  
        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'Name' => $Name,
            'Account' => $Account,
            'PrimaryContact' => !empty($PrimaryContact)?$PrimaryContact:NULL,
            'Owner' => $Owner,
            'PreferredTechnician' => !empty($PreferredTechnician)?$PreferredTechnician:NULL,
            'ParentLocation' => !empty($ParentLocation)?$ParentLocation:NULL,
            'Address' => $Address,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => !empty($Latitude)?$Latitude:NULL,
            'Longitude' => !empty($Longitude)?$Longitude:NULL,
            'LocationType' => $LocationType,
            'AccessNotes' => $AccessNotes,
            'Notes' => $Notes,
        );
  
        $result = $this->db->update('Location', $data, array('LocationID' => $LocationID));    
    
        MY_Model::updateCreatedModifiedByAndDt($UserID,'Location', array('LocationID' => $LocationID));
    
        return $result;
    } 

    function getLocationData($LocationID, $RelatedObjName = 'AllObject')
    { 
        $result = $this->db->update('Location', array('LastModifiedDate' => date("Y-m-d H:i:s")), array('LocationID' => $LocationID));  

        $query = $this->db->select('l.*, l.Name as Title, ac.AccountName, CONCAT(o.FirstName, " ", o.LastName) as OwnerName, CONCAT(pm.FirstName, " ", pm.LastName) as PrimaryContactName, lt.LocationType as LocationTypeName, CONCAT(pt.FirstName, " ", pt.LastName) as PreferredTechnicianName, pl.Name as ParentLocationName, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy, DATE_FORMAT(l.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, DATE_FORMAT(l.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate')
        ->from('Location l')
        ->join('Account ac','ac.AccountID = l.Account', 'left')
        ->join('Location pl','pl.LocationID = l.ParentLocation', 'left')
        ->join('User o','o.UserID = l.Owner', 'left')
        ->join('Contact pm','pm.ContactID = l.PrimaryContact', 'left')
        ->join('LocationType lt','lt.LocationTypeID = l.LocationType', 'left')
        ->join('User cb','cb.UserID = l.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = l.LastModifiedBy', 'left')
        ->join('User pt','pt.UserID = l.PreferredTechnician', 'left')
        ->where(array('l.OrganizationID' => $this->LoginOrganizationID, 'l.LocationID' => $LocationID)) 
        ->get();
        $data['LocationData'] = $query->row();

        /*if($RelatedObjName == 'Accounts'  || $RelatedObjName == 'AllObject'){
           $this->db->select('a.AccountID, n.AccountName');
            $this->db->from('Note n'); 
            $this->db->join('User o','o.UserID = n.Owner', 'left');
            $this->db->where(array('n.OrganizationID' => $this->LoginOrganizationID, 'n.What' => $LocationID,'n.RelatedTo' => 'Location', 'n.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Notes'] = $query->result();
        }*/
        return $data;
    }

    function CreateNote($LocationID)
    {
        extract($_POST); 

        $UserID = $this->LoginUserID;
  
        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'Owner' => $UserID,
            'What' => $LocationID,
            'Subject' => $Subject,
            'Body' => $Body,
            'RelatedTo' => 'Location'
        );
        $result = $this->db->insert('Note', $data);    
        $NoteID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Note', array('NoteID' => $NoteID));
    
        return $result;

    }

    function LocationLineItem($LocationID)
    {
        $UserID = $this->LoginUserID;

        extract($_POST);

        $query = $this->db->delete('LocationLineItem', array('Location' => $LocationID));

        if(count($Product) > 0){ 

            foreach ($Product as $key => $value) {
            
               if(!empty($Product[$key]) && !empty($ListPrice[$key]) && !empty($UnitPrice[$key]) && !empty($Quantity[$key]) && !empty($SubTotal[$key]) && !empty($TotalPrice[$key])){
                    $data = array(
                        'Location' => $LocationID,
                        'OrganizationID' => $this->LoginOrganizationID,
                        'Product' => $Product[$key],
                        'ListPrice' => filter_var($ListPrice[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
                        'Discount' => filter_var($Discount[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
                        'UnitPrice' => filter_var($UnitPrice[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
                        'Quantity' => $Quantity[$key],
                        'SubTotal' => filter_var($SubTotal[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
                        'Taxable' => empty($Taxable[$key])?0:1,
                        'TotalPrice' => filter_var($TotalPrice[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION)
                    );     
                    $query = $this->db->insert('LocationLineItem', $data);  
                    $LocationLineItemID = $this->db->insert_id();

                    $LocationLineNo = MY_Controller::genCode('INL' ,$LocationLineItemID); 
                    $this->db->update('LocationLineItem', array('LocationLineNo'=>$LocationLineNo), array('LocationLineItemID' => $LocationLineItemID));  
           
                    MY_Model::insertCreatedModifiedByAndDt($UserID,'LocationLineItem', array('LocationLineItemID' => $LocationLineItemID));
                }
            }
        }

        return $query;
    }

    /*function CreateWOChemicalLineItems($LocationID)
    {
        $UserID = $this->LoginUserID;

        extract($_POST);

        $query = $this->db->delete('Chemical', array('Location' => $LocationID));

        if(count($Product) > 0){ 

            foreach ($Product as $key => $value) {
               
               if(!empty($Product[$key]) && !empty($TestConcentration[$key]) && !empty($TestedUnitOfMeasure[$key]) && !empty($ApplicationAmount[$key]) && !empty($ApplicationUnitOfMeasure[$key])){
                    $data = array(
                        'Location' => $LocationID,
                        'OrganizationID' => $this->LoginOrganizationID,
                        'Product' => $Product[$key],
                        'Owner' => $UserID,
                        'Account' => $Account,
                        'TestConcentration' => $TestConcentration[$key],
                        'TestedUnitOfMeasure' => $TestedUnitOfMeasure[$key],
                        'ApplicationAmount' => $ApplicationAmount[$key],
                        'ApplicationUnitOfMeasure' => $ApplicationUnitOfMeasure[$key],
                        'ApplicationArea' => $ApplicationArea[$key],
                        'AdditionalNotes' => $AdditionalNotes[$key]
                    );     
                    $query = $this->db->insert('Chemical', $data);  

                    $ChemicalID = $this->db->insert_id();

                    $ChemicalNo = MY_Controller::genCode('CH' ,$ChemicalID);
                    $this->db->update('Chemical', array('ChemicalNo'=>$ChemicalNo), array('ChemicalID' => $ChemicalID));  
                    
                    MY_Model::insertCreatedModifiedByAndDt($UserID,'Chemical', array('ChemicalID' => $ChemicalID));
                }
            }
        }

        return $query;
    }*/

    function GetPrimaryContactOptions()
    { 
        $query = $this->db->get_where('Contact', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0, 'Account' => $_POST['AccountID'])); 
        $results = $query->result();
        $PCOptions = '<option value="">Select Contact</option>';
        foreach ($results as $key => $value) {
            if(isset($_POST['PrimaryContact']) && !empty($_POST['PrimaryContact']) && $_POST['PrimaryContact'] == $value->ContactID)
                $PCOptions .= '<option value="'.$value->ContactID.'" selected="">'.$value->FirstName.' '.$value->LastName.'</option>';
            else 
                 $PCOptions .= '<option value="'.$value->ContactID.'">'.$value->FirstName.' '.$value->LastName.'</option>';
        }
        return $PCOptions;
    }

    function GetProductDetails($ProductID)
    {
        $query = $this->db->get_where('Product', array('OrganizationID' => $this->LoginOrganizationID, 'ProductID' => $ProductID)); 
        return json_encode($query->row()); 
    }

    function GetAccountDetails($AccountID){
        $query = $this->db->get_where('Account', array('OrganizationID' => $this->LoginOrganizationID, 'AccountID' => $AccountID)); 
        return json_encode($query->row()); 
    }

    function GetContactDetails($ContactID)
    {
        $query = $this->db->get_where('Contact', array('OrganizationID' => $this->LoginOrganizationID, 'ContactID' => $ContactID)); 
        return json_encode($query->row()); 
    }
  
    function GetUsers()
    { 
        $query = $this->db->get_where('User', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetLocationStatus()
    { 
        $query = $this->db->get_where('LocationStatus', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        return $query->result();
    }

    function GetLocationPaymentTerms()
    { 
        $query = $this->db->get_where('LocationPaymentTerms', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        return $query->result();
    }

    function GetAccounts()
    { 
        $UserID = $this->session->userdata('UserID');

        $query = $this->db->get_where('Account',array('OrganizationID' => $this->LoginOrganizationID, 'AssignedTo' => $UserID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetContacts()
    { 
        $query = $this->db->get_where('Contact',array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0, 'IsActive' => 1)); 
        
        return $query->result();
    }

    function GetLocations()
    {
        $query = $this->db->get_where('Location', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0, 'IsActive' => 1)); 
        
        return $query->result();
    }

    function GetLocationTypes()
    {
        $query = $this->db->get_where('LocationType', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }
 
    function GetViews(){

        $query = $this->db->query("SELECT * FROM LocationView WHERE (RestrictVisibility = 'VisibleOnlyToMe' AND UserID = '".$this->LoginUserID."') OR (RestrictVisibility = 'VisibleToEveryone') AND OrganizationID = '".$this->LoginOrganizationID."' ORDER BY LocationViewName ASC");
        
        return $query->result();
    }

    function DeleteLocation($LocationID)
    {
        return $this->db->update('Location', array('IsDeleted' => 1), array('LocationID' => $LocationID));  
    }
}

?>
