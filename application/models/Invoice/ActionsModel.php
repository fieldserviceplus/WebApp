<?php

class ActionsModel extends MY_Model{
 
    public $LoginUserID, $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }
 
    function RecentInvoices() 
    {
        $aColumns = array('i.InvoiceID','i.InvoiceNo', 'ac.AccountName', 'ins.InvoiceStatus', 'CONCAT(at.FirstName, " ", at.LastName)', '(Select SUM(TotalPrice) FROM InvoiceLineItem WHERE Invoice=i.InvoiceID)', 'i.City', 'i.State','i.Account', 'i.AssignedTo'); 
        $bColumns = array('InvoiceID','InvoiceNo', 'AccountName', 'InvoiceStatus', 'CONCAT(at.FirstName, " ", at.LastName)', '(Select SUM(TotalPrice) FROM InvoiceLineItem WHERE Invoice=i.InvoiceID)', 'City', 'State','Account', 'AssignedTo'); 
        
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "i.InvoiceID";

        /* DB table to use */
        $sTable = "Invoice i
        LEFT JOIN Account ac ON ac.AccountID = i.Account  
        LEFT JOIN User at ON at.UserID = i.AssignedTo
        LEFT JOIN InvoiceStatus ins ON ins.InvoiceStatusID = i.InvoiceStatus";

        /* Limit */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".( $_GET['iDisplayStart'] ).", ".
                        ( $_GET['iDisplayLength'] );
        }

        /* Ordering */
        $sOrder = "ORDER BY i.LastModifiedDate DESC";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                    {
                            $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                                    ".( $_GET['sSortDir_'.$i] ) .", ";
                    }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                    $sOrder = "";
            }
        }

        /* Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited */
        $sWhere = "";
        if ( $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                        $sWhere .= $aColumns[$i]." LIKE '%".( $_GET['sSearch'] )."%' OR ";
                }
                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ')';
        }
        
        /* Where Condition */
        $UserID = $this->session->userdata('UserID');
       
        $where_str = "i.IsDeleted=0 AND i.OrganizationID = '".$this->LoginOrganizationID."'";
        if ( $sWhere == "" )
        {
                $sWhere = "WHERE $where_str";
        }
        else
        {
                $sWhere .= "AND $where_str";
        }
        
        /* SQL queries Get data to display */
        $sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit"; 

        $rResult = $this->db->query($sQuery); 
        $rResult = $rResult->result_array(); 
      
        /* Data set length after filtering */
        $sQuery = "SELECT FOUND_ROWS() as cnt";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iFilteredTotal = $result->cnt;

        /* Total data set length */
        $sQuery = "SELECT COUNT(".$sIndexColumn.") as cnt
                FROM   $sTable  $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iTotal = $result->cnt;

        /* Output */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );

        foreach ( $rResult as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($bColumns) ; $i++ )
            {
                if($i == 0)
                {
                    //$row[] = '';
                }
                else if($i == 4)
                {
                     $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['AssignedTo'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($bColumns[$i] == 'AccountName')
                {
                     $row[] = '<a href="'.SITE_URL.'Account/Actions/AccountDetails/'.$aRow['Account'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($bColumns[$i] == 'InvoiceNo')
                {
                    $row[] = '<a href="'.SITE_URL.'Invoice/Actions/InvoiceDetails/'.$aRow[$bColumns[0]].'">'.$aRow[$bColumns[$i]].'</a>';
                }
                else if ( $bColumns[$i] != ' ' )
                {
                    $row[] = $aRow[$bColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode( $output );
    } 

    function CreateInvoice() {
               
        extract($_POST); 

        $UserID = $this->LoginUserID;

        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'WorkOrder' => $WorkOrder,
            'AssignedTo' => $AssignedTo,
            'Account' => $Account,
            'Contact' => !empty($Contact)?$Contact:NULL,
            'Description' => $Description,
            'Address' => $Address,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => !empty($Latitude)?$Latitude:NULL,
            'Longitude' => !empty($Longitude)?$Longitude:NULL,
            'InvoiceStatus' => $InvoiceStatus,
            'InvoiceDate' => date("Y-m-d",strtotime($InvoiceDate)),
            'DueDate' => date("Y-m-d",strtotime($DueDate)),
            'PaymentTerms' => $PaymentTerms,
            'AdditionalInformation' => $AdditionalInformation,
        );  

        $result = $this->db->insert('Invoice', $data); 
        //echo $this->db->last_query();exit;
        $InvoiceID = $this->db->insert_id();

        $InvoiceNo = MY_Controller::genCode('IN', $InvoiceID);
        $this->db->update('Invoice', array('InvoiceNo' => $InvoiceNo), array('InvoiceID' => $InvoiceID));  

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Invoice', array('InvoiceID' => $InvoiceID));

        /* For insert custom fields */ 
        MY_Model::insertCustomFieldData('Invoice', $InvoiceID);

        return $InvoiceID;
    } 
 
    function EditInvoice($InvoiceID) {
               
        extract($_POST); 

        $UserID = $this->LoginUserID;
  
        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'WorkOrder' => $WorkOrder,
            'AssignedTo' => $AssignedTo,
            'Account' => $Account,
            'Contact' => !empty($Contact)?$Contact:NULL,
            'Description' => $Description,
            'Address' => $Address,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => !empty($Latitude)?$Latitude:NULL,
            'Longitude' => !empty($Longitude)?$Longitude:NULL,
            'InvoiceStatus' => $InvoiceStatus,
            'InvoiceDate' => date("Y-m-d",strtotime($InvoiceDate)),
            'DueDate' => date("Y-m-d",strtotime($DueDate)),
            'PaymentTerms' => $PaymentTerms,
            'AdditionalInformation' => $AdditionalInformation,
        );
  
        $result = $this->db->update('Invoice', $data, array('InvoiceID' => $InvoiceID));    
    
        MY_Model::updateCreatedModifiedByAndDt($UserID,'Invoice', array('InvoiceID' => $InvoiceID));
    
        /* For insert custom fields */ 
        MY_Model::updateCustomFieldData('Invoice', $InvoiceID);

        return $result;
    } 

    function getInvoiceData($InvoiceID, $RelatedObjName = 'AllObject')
    { 
        $result = $this->db->update('Invoice', array('LastModifiedDate' => date("Y-m-d H:i:s")), array('InvoiceID' => $InvoiceID));  

        $query = $this->db->select('i.*, ac.AccountName, i.InvoiceNo as Title, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, CONCAT(pm.FirstName, " ", pm.LastName) as ContactName, pt.PaymentTerms, pt.InvoicePaymentTermID, ins.InvoiceStatus as Status, DATE_FORMAT(i.InvoiceDate, "'.RES_DATE.'") as InvoiceDate, DATE_FORMAT(i.DueDate, "'.RES_DATE.'") as DueDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName, DATE_FORMAT(i.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, DATE_FORMAT(i.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, wo.Subject as WOSubject, i.CreatedBy, i.LastModifiedBy, org.CompanyName, org.Website, org.Phone as OrgPhone, org.BillingAddress as OrgBillingAddress, org.BillingCity as OrgBillingCity, org.BillingState as OrgBillingState,org.BillingCountry as OrgBillingCountry, org.BillingPostalCode as OrgBillingPostalCode, ac.PhoneNo as AcPhoneNo, org.Tax')
        ->from('Invoice i')
        ->join('Account ac','ac.AccountID = i.Account', 'left')
        ->join('WorkOrder wo','wo.WorkOrderID = i.WorkOrder', 'left')
        ->join('User at','at.UserID = i.AssignedTo', 'left')
        ->join('Contact pm','pm.ContactID = i.Contact', 'left')
        ->join('InvoiceStatus ins','ins.InvoiceStatusID = i.InvoiceStatus', 'left')
        ->join('InvoicePaymentTerm pt','pt.InvoicePaymentTermID = i.PaymentTerms', 'left') 
        ->join('User cb','cb.UserID = i.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = i.LastModifiedBy', 'left')
        ->join('Organization org','org.OrganizationID = i.OrganizationID', 'left')
        ->where(array('i.OrganizationID' => $this->LoginOrganizationID, 'i.InvoiceID' => $InvoiceID)) 
        ->get();
        $data['InvoiceData'] = $query->row();

        $this->db->select('SUM(SubTotal) as SubTot, SUM(TotalPrice) as TotPrc, SUM(ListPrice) as ListTot, SUM(UnitPrice) as UnitTot');
        $this->db->from('InvoiceLineItem');
        $this->db->where(array('OrganizationID' => $this->LoginOrganizationID, 'Invoice' => $InvoiceID, 'IsDeleted' => 0));
        $query = $this->db->get(); 
        $result = $query->row();
        if(!empty($result->SubTot) && !empty($result->TotPrc) && !empty($result->ListTot) && !empty($result->UnitTot)){
            $Financials['SubTotal'] = $result->SubTot;
            $Financials['Discount'] = (100 - ($result->UnitTot / $result->ListTot) * 100);
            $Financials['TotalPrice'] = $result->ListTot;
            $Financials['Tax'] = ($result->TotPrc - $result->SubTot);
            $Financials['GrandTotal'] = $result->TotPrc;
        } else {
            $Financials['SubTotal'] = 0.00;
            $Financials['Discount'] = 0.00;
            $Financials['TotalPrice'] = 0.00;
            $Financials['Tax'] = 0.00;
            $Financials['GrandTotal'] = 0.00;
        }
        $data['InvoiceFinancials'] = $Financials;

        if($RelatedObjName == 'InvoiceLineItems'  || $RelatedObjName == 'AllObject'){
            $query = $this->db->select('ili.*, p.ProductName');
            $this->db->from('InvoiceLineItem ili');
            $this->db->join('Product p','p.ProductID = ili.Product', 'left');
            $this->db->where(array('ili.OrganizationID' => $this->LoginOrganizationID, 'ili.Invoice' => $InvoiceID, 'ili.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['InvoiceLineItems'] = $query->result();
        }

        if($RelatedObjName == 'Events'  || $RelatedObjName == 'AllObject'){
            $query = $this->db->select('e.EventID, e.AssignedTo, e.Subject, CONCAT(pc.FirstName, " ", pc.LastName) as ContactName,  CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, DATE_FORMAT(e.EventStartDate, "'.RES_DATETIME.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATETIME.'") as EventEndDate, DATE_FORMAT(e.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, e.CreatedBy, e.Who');
            $this->db->from('Event e');
            $this->db->join('User cb','cb.UserID = e.CreatedBy', 'left');
            $this->db->join('User at','at.UserID = e.AssignedTo', 'left');
            $this->db->join('Contact pc','pc.ContactID = e.Who', 'left');
            $this->db->join('User lmb','lmb.UserID = e.LastModifiedBy', 'left');
            $this->db->where(array('e.OrganizationID' => $this->LoginOrganizationID, 'e.What' => $InvoiceID,'e.RelatedTo' => 'Invoice', 'e.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Events'] = $query->result();
        }

        if($RelatedObjName == 'Files'  || $RelatedObjName == 'AllObject'){
            $this->db->select('f.FileID, f.FileName, f.Subject, f.ContentType, DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, f.CreatedBy');
            $this->db->from('File f');
            $this->db->join('User cb','cb.UserID = f.CreatedBy', 'left');
            $this->db->where(array('f.OrganizationID' => $this->LoginOrganizationID, 'f.What' => $InvoiceID,'f.RelatedTo' => 'Invoice', 'f.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Files'] = $query->result();
        }

        if($RelatedObjName == 'Tasks'  || $RelatedObjName == 'AllObject'){
           $this->db->select('t.TaskID, t.Subject, tt.TaskType, DATE_FORMAT(t.DueDate, "'.RES_DATETIME.'") as DueDate, tp.Priority, ts.TaskStatus, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, CONCAT(pc.FirstName, " ", pc.LastName) as ContactName, t.CreatedBy, t.Who, t.AssignedTo');
            $this->db->from('Task t'); 
            $this->db->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left');
            $this->db->join('User at','at.UserID = t.AssignedTo', 'left');
            $this->db->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left');
            $this->db->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left');
            $this->db->join('Contact pc','pc.ContactID = t.Who', 'left');
            $this->db->where(array('t.OrganizationID' => $this->LoginOrganizationID, 't.What' => $InvoiceID,'t.RelatedTo' => 'Invoice', 't.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Tasks'] = $query->result();
        }

        if($RelatedObjName == 'Notes'  || $RelatedObjName == 'AllObject'){
           $this->db->select('n.NoteID, n.Subject, DATE_FORMAT(n.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, CONCAT(o.FirstName, " ", o.LastName) as OwnerName, n.Owner');
            $this->db->from('Note n'); 
            $this->db->join('User o','o.UserID = n.Owner', 'left');
            $this->db->where(array('n.OrganizationID' => $this->LoginOrganizationID, 'n.What' => $InvoiceID,'n.RelatedTo' => 'Invoice', 'n.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Notes'] = $query->result();
        }
        return $data;
    }

    function CreateNote($InvoiceID)
    {
        extract($_POST); 

        $UserID = $this->LoginUserID;
  
        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'Owner' => $UserID,
            'What' => $InvoiceID,
            'Subject' => $Subject,
            'Body' => $Body,
            'RelatedTo' => 'Invoice'
        );
        $result = $this->db->insert('Note', $data);    
        $NoteID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Note', array('NoteID' => $NoteID));
    
        return $result;

    }

    function InvoiceLineItem($InvoiceID)
    {
        $UserID = $this->LoginUserID;

        extract($_POST);

        $query = $this->db->delete('InvoiceLineItem', array('Invoice' => $InvoiceID));

        if(count($Product) > 0){ 

            foreach ($Product as $key => $value) {
            
               if(!empty($Product[$key]) && !empty($ListPrice[$key]) && !empty($UnitPrice[$key]) && !empty($Quantity[$key]) && !empty($SubTotal[$key]) && !empty($TotalPrice[$key])){
                    $data = array(
                        'Invoice' => $InvoiceID,
                        'OrganizationID' => $this->LoginOrganizationID,
                        'Product' => $Product[$key],
                        'ListPrice' => filter_var($ListPrice[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
                        'Discount' => filter_var($Discount[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
                        'UnitPrice' => filter_var($UnitPrice[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
                        'Quantity' => $Quantity[$key],
                        'SubTotal' => filter_var($SubTotal[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
                        'Taxable' => empty($Taxable[$key])?0:1,
                        'TotalPrice' => filter_var($TotalPrice[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION)
                    );     
                    $query = $this->db->insert('InvoiceLineItem', $data);  
                    $InvoiceLineItemID = $this->db->insert_id();

                    $InvoiceLineNo = MY_Controller::genCode('INL' ,$InvoiceLineItemID); 
                    $this->db->update('InvoiceLineItem', array('InvoiceLineNo'=>$InvoiceLineNo), array('InvoiceLineItemID' => $InvoiceLineItemID));  
           
                    MY_Model::insertCreatedModifiedByAndDt($UserID,'InvoiceLineItem', array('InvoiceLineItemID' => $InvoiceLineItemID));
                }
            }
        }

        return $query;
    }

    /*function CreateWOChemicalLineItems($InvoiceID)
    {
        $UserID = $this->LoginUserID;

        extract($_POST);

        $query = $this->db->delete('Chemical', array('Invoice' => $InvoiceID));

        if(count($Product) > 0){ 

            foreach ($Product as $key => $value) {
               
               if(!empty($Product[$key]) && !empty($TestConcentration[$key]) && !empty($TestedUnitOfMeasure[$key]) && !empty($ApplicationAmount[$key]) && !empty($ApplicationUnitOfMeasure[$key])){
                    $data = array(
                        'Invoice' => $InvoiceID,
                        'OrganizationID' => $this->LoginOrganizationID,
                        'Product' => $Product[$key],
                        'Owner' => $UserID,
                        'Account' => $Account,
                        'TestConcentration' => $TestConcentration[$key],
                        'TestedUnitOfMeasure' => $TestedUnitOfMeasure[$key],
                        'ApplicationAmount' => $ApplicationAmount[$key],
                        'ApplicationUnitOfMeasure' => $ApplicationUnitOfMeasure[$key],
                        'ApplicationArea' => $ApplicationArea[$key],
                        'AdditionalNotes' => $AdditionalNotes[$key]
                    );     
                    $query = $this->db->insert('Chemical', $data);  

                    $ChemicalID = $this->db->insert_id();

                    $ChemicalNo = MY_Controller::genCode('CH' ,$ChemicalID);
                    $this->db->update('Chemical', array('ChemicalNo'=>$ChemicalNo), array('ChemicalID' => $ChemicalID));  
                    
                    MY_Model::insertCreatedModifiedByAndDt($UserID,'Chemical', array('ChemicalID' => $ChemicalID));
                }
            }
        }

        return $query;
    }*/

    function GetPrimaryContactOptions()
    { 
        $query = $this->db->get_where('Contact', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0, 'Account' => $_POST['AccountID'])); 
        $results = $query->result();
        $PCOptions = '<option value="">Select Contact</option>';
        foreach ($results as $key => $value) {
            if(isset($_POST['PrimaryContact']) && !empty($_POST['PrimaryContact']) && $_POST['PrimaryContact'] == $value->ContactID)
                $PCOptions .= '<option value="'.$value->ContactID.'" selected="">'.$value->FirstName.' '.$value->LastName.'</option>';
            else 
                 $PCOptions .= '<option value="'.$value->ContactID.'">'.$value->FirstName.' '.$value->LastName.'</option>';
        }
        return $PCOptions;
    }

    function GetProductDetails($ProductID)
    {
        $query = $this->db->get_where('Product', array('OrganizationID' => $this->LoginOrganizationID, 'ProductID' => $ProductID)); 
        return json_encode($query->row()); 
    }

    function GetAccountDetails($AccountID){
        $query = $this->db->get_where('Account', array('OrganizationID' => $this->LoginOrganizationID, 'AccountID' => $AccountID)); 
        return json_encode($query->row()); 
    }

    function GetContactDetails($ContactID)
    {
        $query = $this->db->get_where('Contact', array('OrganizationID' => $this->LoginOrganizationID, 'ContactID' => $ContactID)); 
        return json_encode($query->row()); 
    }
  
    function GetUsers()
    { 
        $query = $this->db->get_where('User', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetInvoiceStatus()
    { 
        $query = $this->db->get_where('InvoiceStatus', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        return $query->result();
    }

    function GetInvoicePaymentTerms()
    { 
        $query = $this->db->get_where('InvoicePaymentTerm', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        return $query->result();
    }

    function GetAccounts()
    { 
        $UserID = $this->session->userdata('UserID');

        $query = $this->db->get_where('Account',array('OrganizationID' => $this->LoginOrganizationID, 'AssignedTo' => $UserID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetContacts()
    { 
        $query = $this->db->get_where('Contact',array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0, 'IsActive' => 1)); 
        
        return $query->result();
    }

    function GetWorkOrders()
    {
        $query = $this->db->get_where('WorkOrder', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetProducts()
    {
        $query = $this->db->get_where('Product', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }
 
    function GetViews(){

        $query = $this->db->query("SELECT * FROM InvoiceView WHERE (RestrictVisibility = 'VisibleOnlyToMe' AND UserID = '".$this->LoginUserID."') OR (RestrictVisibility = 'VisibleToEveryone') AND OrganizationID = '".$this->LoginOrganizationID."' ORDER BY InvoiceViewName ASC");
        
        return $query->result();
    }

    function DeleteInvoice($InvoiceID)
    {
        return $this->db->update('Invoice', array('IsDeleted' => 1), array('InvoiceID' => $InvoiceID));  
    }

    function GenDocTemplates($RelatedTo)
    {
        $query = $this->db->get_where('GenDocTemplates', array('OrganizationID' => $this->LoginOrganizationID, 'RelatedTo' => $RelatedTo, 'IsDeleted' => 0, 'IsActive' => 1)); 
        
        return $query->result();
    }

    function GenerateDocument($InvoiceData){

        $_POST['SaveToInvoice'] = isset($_POST['SaveToInvoice'])?$_POST['SaveToInvoice']:0;
        
        extract($_POST);
        extract($InvoiceData);

        $data = array();

        $data['AssignedTo'] = $InvoiceData->AssignedTo;
        $data['InvoiceNo'] = $InvoiceData->InvoiceNo;
        
        $company = array();
        $company["name"] = $InvoiceData->CompanyName;
        $company["street_address"] = $InvoiceData->OrgBillingAddress;
        $company["city_state_zip"] = $InvoiceData->OrgBillingCity.', '.$InvoiceData->OrgBillingState.', '.$InvoiceData->OrgBillingCountry.', '.$InvoiceData->OrgBillingPostalCode;
        $company["phone"] = $InvoiceData->OrgPhone;
        $company["fax"] = "";
        $company["website"] = $InvoiceData->Website;
        $data["company"] = $company;

       /* $host = array();
        $host["logo"] = dirname(__FILE__).'/logo.png';
        $data["host"] = $host;*/

        $in = array();
        $in["no"] = $InvoiceData->InvoiceNo;
        $in["date"] = date("m/d/Y", strtotime($InvoiceData->InvoiceDate));
        $in["description"] = $InvoiceData->Description;
        $data["in"] = $in;

        $customer = array();
        $customer["name"] = "-";
        $customer["id"] = "-";
        $data["customer"] = $customer;

        $bill = array();
        $bill["name"] = "john jojo";
        $bill["company_name"] = $InvoiceData->CompanyName;
        $bill["street_address"] = $InvoiceData->Address;
        $bill["city_state_zip"] = $InvoiceData->City.','.$InvoiceData->State.','.$InvoiceData->Country.','.$InvoiceData->PostalCode;
        $bill["phone"] = $InvoiceData->AcPhoneNo;
        $data["bill"] = $bill;

        $items = array();
        $taxable = 0;

        foreach ($InvoiceLineItems as $key => $InvoiceLineItem) {
            
            $item = array();
            $item["qty"] = $InvoiceLineItem->Quantity;
            $item["description"] = $InvoiceLineItem->ProductName;
            $item["tax"] = ($InvoiceLineItem->Taxable == 1) ? "Y" : "N";
            $item["price"] = number_format((double)$InvoiceLineItem->UnitPrice, 2, '.', ',');
            $item["line_total"] = number_format((double)$InvoiceLineItem->TotalPrice, 2, '.', ',');
            $items[] = $item;

            $taxable += ($InvoiceLineItem->Taxable == 1) ? $InvoiceLineItem->TotalPrice : 0;
        } 
        $data["items"] = $items;

        $invoice = array();
        $invoice["subtotal"] = number_format($InvoiceFinancials['SubTotal'], 2, '.', ',');
        $invoice["taxable"] = number_format($taxable, 2, '.', ',');
        $invoice["tax_rate"] = $InvoiceData->Tax.'%';
        $invoice["tax"] = number_format((double)$InvoiceFinancials['Tax'], 2, '.', ',');
        $invoice["s_h"] = "-";
        $invoice["other"] = "-";
        $invoice["total"] = number_format((double)$InvoiceFinancials['GrandTotal'], 2, '.', ',');
        $data["invoice"] = $invoice;

        return MY_Model::generateDocs($GenDocTemplateID, $data, $InvoiceData->InvoiceID, $InvoiceData->InvoiceNo, $InvoiceData->AssignedTo, $SaveToInvoice, $OutputFormat, 'Invoice'); 
    }
}

?>
