<?php
class ViewModel extends MY_Model{
 
    public $LoginUserID, $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }
 
    function ViewInvoices() 
    {
        $aColumns = array('i.InvoiceID', 'CONCAT(at.FirstName, " ", at.LastName)', 'i.InvoiceNo', 'ac.AccountName', 'CONCAT(c.FirstName, " ", c.LastName)', '(Select SUM(TotalPrice) FROM InvoiceLineItem WHERE Invoice=i.InvoiceID)', 'es.InvoiceStatus', 'DATE_FORMAT(i.InvoiceDate, "'.RES_DATETIME.'")', 'pt.PaymentTerms' , 'DATE_FORMAT(i.DueDate, "'.RES_DATETIME.'")', 'i.City', 'i.State', 'DATE_FORMAT(i.CreatedDate, "'.RES_DATETIME.'")', 'CONCAT(cb.FirstName, " ", cb.LastName)', 'i.Account', 'i.Contact', 'i.AssignedTo'); 
        $bColumns = array('InvoiceID', 'CONCAT(at.FirstName, " ", at.LastName)', 'InvoiceNo', 'AccountName', 'CONCAT(c.FirstName, " ", c.LastName)', '(Select SUM(TotalPrice) FROM InvoiceLineItem WHERE Invoice=i.InvoiceID)', 'InvoiceStatus', 'DATE_FORMAT(i.InvoiceDate, "'.RES_DATETIME.'")', 'PaymentTerms', 'DATE_FORMAT(i.DueDate, "'.RES_DATETIME.'")', 'City', 'State', 'DATE_FORMAT(i.CreatedDate, "'.RES_DATETIME.'")', 'CONCAT(cb.FirstName, " ", cb.LastName)', 'Account', 'Contact', 'AssignedTo');  
      
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "i.InvoiceID";

        /* DB table to use */
        $sTable = "Invoice i
        LEFT JOIN User at ON at.UserID = i.AssignedTo
        LEFT JOIN User cb ON cb.UserID = i.CreatedBy
        LEFT JOIN Account ac ON ac.AccountID = i.Account
        LEFT JOIN Contact c ON c.ContactID = i.Contact
        LEFT JOIN InvoiceStatus es ON es.InvoiceStatusID = i.InvoiceStatus
        LEFT JOIN InvoicePaymentTerm pt ON pt.InvoicePaymentTermID = i.PaymentTerms";

        /* Limit */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".( $_GET['iDisplayStart'] ).", ".
                        ( $_GET['iDisplayLength'] );
        }

        /* Ordering */
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                    {
                            $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                                    ".( $_GET['sSortDir_'.$i] ) .", ";
                    }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                    $sOrder = "";
            }
        }

        /* Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited */
        $sWhere = "";
        if ( $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                        $sWhere .= $aColumns[$i]." LIKE '%".( $_GET['sSearch'] )."%' OR ";
                }
                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ')';
        }
        
        /* Where Condition */
        $UserID = $this->session->userdata('UserID');
        if(isset($_GET['flag']) && $_GET['flag'] == 'MyOpenInvoices'){

            $where_str = "i.AssignedTo = $UserID AND es.IsClosed=0 AND i.OrganizationID='".$this->LoginOrganizationID."' AND i.IsDeleted=0";
        }
        else if(isset($_GET['flag']) && $_GET['flag'] == 'AllOpenInvoices'){

            $where_str = "es.IsClosed=0 AND i.OrganizationID='".$this->LoginOrganizationID."' AND i.IsDeleted=0";

        } else if(isset($_GET['flag']) && $_GET['flag'] == 'AllOpenInvoicesPastDue'){

            $where_str = "DATE(i.DueDate) < CURDATE() AND es.IsClosed=0 AND i.OrganizationID='".$this->LoginOrganizationID."' AND i.IsDeleted=0";

        } else if(isset($_GET['flag']) && $_GET['flag'] == 'InvoicesCreatedThisWeek'){

            $where_str = "YEARWEEK(i.CreatedDate, 1) = YEARWEEK(CURDATE(), 1) AND i.OrganizationID='".$this->LoginOrganizationID."' AND i.IsDeleted=0";

        } else {
            $where_str = "i.IsDeleted=0 AND i.OrganizationID='".$this->LoginOrganizationID."'";
        }
        
        if ( $sWhere == "" )
        {
                $sWhere = "WHERE $where_str";
        }
        else
        {
                $sWhere .= "AND $where_str";
        }
        
        /* SQL queries Get data to display */
        $sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit";  
        //echo $sQuery;exit;
        $rResult = $this->db->query($sQuery); 
        $rResult = $rResult->result_array(); 
      
        /* Data set length after filtering */
        $sQuery = "SELECT FOUND_ROWS() as cnt";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iFilteredTotal = $result->cnt;

        /* Total data set length */
        $sQuery = "SELECT COUNT(".$sIndexColumn.") as cnt
                FROM   $sTable  $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iTotal = $result->cnt;

        /* Output */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );

        foreach ( $rResult as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($bColumns) ; $i++ )
            {
                if($i == 0)
                {
                    $row[] = '<a href="'.SITE_URL.'Invoice/Actions/EditInvoice/'.$aRow[$bColumns[0]].'"><i class="fa fa-pencil-square-o"></i></a>';
                } 
                else if($i == 1)
                {
                     $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['AssignedTo'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($i == 4)
                {
                     $row[] = '<a href="'.SITE_URL.'Contact/Actions/ContactDetails/'.$aRow['Contact'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($bColumns[$i] == 'AccountName')
                {
                     $row[] = '<a href="'.SITE_URL.'Account/Actions/AccountDetails/'.$aRow['Account'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($bColumns[$i] == 'InvoiceNo')
                {
                    $row[] = '<a href="'.SITE_URL.'Invoice/Actions/InvoiceDetails/'.$aRow[$bColumns[0]].'">'.$aRow[$bColumns[$i]].'</a>';
                }
                else if ( $bColumns[$i] != ' ' )
                {
                    $row[] = $aRow[$bColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode( $output );
    } 

    function CreateNewView()
    {
        extract($_POST);

        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'InvoiceViewName' => $InvoiceViewName,
            'UserID' => $this->LoginUserID,
            'RestrictVisibility' => $RestrictVisibility,
            'SpecifyFieldsDisplay' => implode(",", $SpecifyFieldsDisplay),
            'CreatedDate' => date("Y-m-d H:i:s"),
            'LastModifiedDate' => date("Y-m-d H:i:s")
        );     
        $query = $this->db->insert('InvoiceView', $data);  
        $InvoiceViewID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($this->LoginUserID, 'InvoiceView', array('InvoiceViewID' => $InvoiceViewID));

        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'InvoiceViewID' => $InvoiceViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('InvoiceFilter', $data);  

                   
                }
            } 
 
        }
        return $InvoiceViewID;
    }

    function EditCustomView($InvoiceViewID)
    {  
        extract($_POST);
  
        $data = array(
            'InvoiceViewName' => $InvoiceViewName,
            'UserID' => $this->LoginUserID,
            'RestrictVisibility' => $RestrictVisibility,
            'SpecifyFieldsDisplay' => implode(",", $SpecifyFieldsDisplay),
            'CreatedDate' => date("Y-m-d H:i:s"),
            'LastModifiedDate' => date("Y-m-d H:i:s")
        );     
        $query = $this->db->update('InvoiceView', $data, array('InvoiceViewID' => $InvoiceViewID));  

        $this->db->delete('InvoiceFilter', array('InvoiceViewID' => $InvoiceViewID));  
        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'InvoiceViewID' => $InvoiceViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('InvoiceFilter', $data);  
                }
            } 
 
        }
        return $query;
    }

    function GetInvoiceViewData($InvoiceViewID){

        $query = $this->db->get_where('InvoiceView', array('OrganizationID' => $this->LoginOrganizationID, 'InvoiceViewID' => $InvoiceViewID)); 
        $InvoiceViewData = $query->row();

        $query = $this->db->get_where('InvoiceFilter', array('InvoiceViewID' => $InvoiceViewID)); 
        $InvoiceFilterData = $query->result_array();

        $ColName = "i.InvoiceID, ac.AccountID, wo.WorkOrderID, c.ContactID, i.AssignedTo as AssignedToID, ";
        if(!empty($InvoiceViewData->SpecifyFieldsDisplay)){
            $SpecifyFieldsDisplays = explode(",", $InvoiceViewData->SpecifyFieldsDisplay);
            foreach ($SpecifyFieldsDisplays as $key => $value) {
                if($value == 'AssignedTo'){
                    $ColName .= "CONCAT(ast.FirstName, ' ', ast.LastName) as AssignedTo";
                } else if($value == 'Account'){
                    $ColName .= "ac.AccountName as Account";
                } else if($value == 'OrganizationID'){
                    $ColName .= "o.CompanyName as OrganizationID";
                } else if($value == 'InvoiceStatus'){
                    $ColName .= "es.InvoiceStatus as InvoiceStatus";
                } else if($value == 'PaymentTerms'){
                    $ColName .= "pt.PaymentTerms";
                } else if($value == 'Contact'){
                    $ColName .= "CONCAT(c.FirstName, ' ', c.LastName) as Contact";
                } else if($value == 'WorkOrder'){
                    $ColName .= "wo.Subject as WorkOrder";
                } else if($value == 'CreatedBy'){
                    $ColName .= "CONCAT(cr.FirstName, ' ', cr.LastName) as CreatedBy";
                } else if($value == 'LastModifiedBy'){
                    $ColName .= "CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedBy";
                } else if($value == 'InvoiceDate'){
                    $ColName .= "DATE_FORMAT(i.InvoiceDate, '".RES_DATETIME."') as InvoiceDate";
                } else if($value == 'DueDate'){
                    $ColName .= "DATE_FORMAT(i.DueDate, '".RES_DATETIME."') as DueDate";
                } else {
                    $ColName .= "i.".$value;
                }

                if($key != count($SpecifyFieldsDisplays)){
                    $ColName .= ", ";
                }
            }
        }
            
         /* Filter Invoice Data Start */
            $this->db->select($ColName);
            $this->db->from('Invoice i');
            $this->db->join('User ast','ast.UserID = i.AssignedTo', 'left');
            $this->db->join('Account ac','ac.AccountID = i.Account', 'left');
            $this->db->join('WorkOrder wo','wo.WorkOrderID = i.WorkOrder', 'left');
            $this->db->join('Organization o','o.OrganizationID = i.OrganizationID', 'left');
            $this->db->join('InvoiceStatus es','es.InvoiceStatusID = i.InvoiceStatus', 'left');
            $this->db->join('InvoicePaymentTerm pt','pt.InvoicePaymentTermID = i.PaymentTerms', 'left');
            $this->db->join('User cr','cr.UserID = i.CreatedBy', 'left');
            $this->db->join('User lmb','lmb.UserID = i.LastModifiedBy', 'left');
            $this->db->join('Contact c','c.ContactID = i.Contact', 'left');
            $this->db->where(array('i.OrganizationID' => $this->LoginOrganizationID, 'i.IsDeleted' => 0));

            foreach ($InvoiceFilterData as $key => $value) {

                $WhereField = "";
                if($value['FilterField'] == 'AssignedTo'){
                    $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                } else if($value['FilterField'] == 'Account') {
                    $WhereField = "ac.AccountName";
                } else if($value['FilterField'] == 'WorkOrder') {
                    $WhereField = "wo.Subject as WorkOrder";
                } else if($value['FilterField'] == 'OrganizationID') {
                    $WhereField = "o.CompanyName";
                } else if($value['FilterField'] == 'InvoiceStatus') {
                    $WhereField = "es.InvoiceStatus";
                } else if($value['FilterField'] == 'PaymentTerms') {
                    $WhereField = "pt.PaymentTerms";
                } else if($value['FilterField'] == 'CreatedBy') {
                    $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                } else if($value['FilterField'] == 'LastModifiedBy') {
                    $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                } else if($value['FilterField'] == 'Contact') {
                    $WhereField = "CONCAT(c.FirstName, ' ', c.LastName)";
                } else if($value['FilterField'] == 'InvoiceDate'){
                     $WhereField = "DATE_FORMAT(i.InvoiceDate, '".RES_DATETIME."')";
                } else if($value['FilterField'] == 'DueDate'){
                     $WhereField = "DATE_FORMAT(i.DueDate, '".RES_DATETIME."')";
                } else {
                    $WhereField = "i.".$value['FilterField'];
                }

                if($value['FilterCondition'] == 'Equals'){
                    $this->db->where($WhereField.' =',  $value['FilterValue']);  
                } else if($value['FilterCondition'] == 'NotEqualTo'){
                    $this->db->where($WhereField.' !=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'LessThan'){
                    $this->db->where($WhereField.' <', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'GreaterThan'){
                    $this->db->where($WhereField.' >', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'LessOREqualTo'){
                    $this->db->where($WhereField.' <=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'GreaterOREqualTo'){
                    $this->db->where($WhereField.' >=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'Contains'){
                    $this->db->like($WhereField, $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'StartsWith'){ 
                    $this->db->like($WhereField, $value['FilterValue'], 'after');   
                } else if($value['FilterCondition'] == 'DoesNotContain'){
                    $this->db->not_like($WhereField, $value['FilterValue']);   
                } 
            } 
            $query = $this->db->get();  

            //echo $this->db->last_query();exit;
            $data['ColumnsNames'] = $SpecifyFieldsDisplays;
            $data['ColumnsValues'] = $query->result();

            return $data; 
    }

    function GetCustomView($InvoiceViewID)
    {
        $query = $this->db->get_where('InvoiceView', array('OrganizationID' => $this->LoginOrganizationID, 'InvoiceViewID' => $InvoiceViewID)); 
        
        $data['InvoiceViewData'] = $query->row();

        $query = $this->db->get_where('InvoiceFilter', array('InvoiceViewID' => $InvoiceViewID)); 
        
        $data['InvoiceFilterData'] = $query->result();

        return $data;
    }

    function GetViews(){

        $query = $this->db->query("SELECT * FROM InvoiceView WHERE (RestrictVisibility = 'VisibleOnlyToMe' AND UserID = '".$this->LoginUserID."') OR (RestrictVisibility = 'VisibleToEveryone') AND OrganizationID = '".$this->LoginOrganizationID."' ORDER BY InvoiceViewName ASC");
        
        return $query->result();
    }

    function InvoiceViewFields()
    {
        $query = $this->db->get_where('InvoiceViewFields'); 
        return $query->result();
    }

    function DeleteCustomView($InvoiceViewID)
    {
        return $this->db->delete('InvoiceView', array('InvoiceViewID' => $InvoiceViewID)); 
    }

    function CopyCustomView($InvoiceViewID)
    {
       $query = $this->db->get_where('InvoiceView', array('InvoiceViewID' => $InvoiceViewID)); 
      
        foreach ($query->result() as $row){   
           foreach($row as $key=>$val){ 
                if($key != 'InvoiceViewID'){    
                    $this->db->set($key, $val); 
                }                
           }
        }
        $this->db->insert('InvoiceView');
        $NewInvoiceViewID = $this->db->insert_id();

        $query = $this->db->get_where('InvoiceFilter', array('InvoiceViewID' => $InvoiceViewID)); 
       
        foreach ($query->result() as $row){   
           foreach($row as $key => $val){ 
                if($key == 'InvoiceViewID'){  
                    $this->db->set($key, $NewInvoiceViewID);  
                } else if($key != 'InvoiceFilterID'){    
                    $this->db->set($key, $val); 
                }             
           }
           $this->db->insert('InvoiceFilter');
        }

        return 1; 
    }

    function RenameCustomView($InvoiceViewID){

        $data = array(
            'InvoiceViewName' => $_POST['InvoiceViewName']
        );

        return $this->db->update('InvoiceView', $data, array('InvoiceViewID' => $InvoiceViewID));  
    }

    function EditSharingCustomView($InvoiceViewID){

        $data = array(
            'RestrictVisibility' => $_REQUEST['RestrictVisibility']
        );

        return $this->db->update('InvoiceView', $data, array('InvoiceViewID' => $InvoiceViewID));  
    }
 
    function EditFiltersCustomView($InvoiceViewID){ 

        extract($_POST);

        $this->db->delete('InvoiceFilter', array('InvoiceViewID' => $InvoiceViewID));  
        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'InvoiceViewID' => $InvoiceViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('InvoiceFilter', $data);  
                }
            } 
 
        }
        return $query; 
    }

    function EditDisplayedColumnsCustomView($InvoiceViewID){

        $data = array(
            'SpecifyFieldsDisplay' => implode(",", $_REQUEST['SpecifyFieldsDisplay'])
        );

        return $this->db->update('InvoiceView', $data, array('InvoiceViewID' => $InvoiceViewID));  
    }
    
}

?>
