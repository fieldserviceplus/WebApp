<?php
 
class ActionsModel extends MY_Model{
 
    public $LoginUserID, $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }
 
    function RecentAccounts() 
    {
        $aColumns = array('a.AccountID','a.AccountName', 'a.PhoneNo', 'at.AccountType', 'a.BillingCity', 'a.BillingState', 'CONCAT(ast.FirstName, " ", ast.LastName)', 'a.AssignedTo');
        $bColumns = array('AccountID','AccountName', 'PhoneNo' , 'AccountType', 'BillingCity', 'BillingState', 'CONCAT(ast.FirstName, " ", ast.LastName)', 'AssignedTo');
        
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "a.AccountID";

        /* DB table to use */
        $sTable = "Account a
        LEFT JOIN User ast ON ast.UserID = a.AssignedTo
        LEFT JOIN AccountType at ON at.AccountTypeID = a.AccountType";

        /* Limit */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".( $_GET['iDisplayStart'] ).", ".
                        ( $_GET['iDisplayLength'] );
        }

        /* Ordering */
        $sOrder = "ORDER BY a.LastModifiedDate DESC";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                    {
                            $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                                    ".( $_GET['sSortDir_'.$i] ) .", ";
                    }
            }
           
            $sOrder = substr_replace( $sOrder, "", -2 );

            if ( $sOrder == "ORDER BY" )
            {
                    $sOrder = "";
            } 
        }

        /* Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited */
        $sWhere = "";
        if ( $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                        $sWhere .= $aColumns[$i]." LIKE '%".( $_GET['sSearch'] )."%' OR ";
                }
                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ')';
        }
        
        /* Where Condition */
        /*$UserID = $this->session->userdata('UserID');
        if(isset($_GET['flag']) && $_GET['flag'] == 'MyAccounts')
            $where_str = "a.AssignedTo = $UserID and a.IsActive=1 and a.IsDeleted=0";
        else {*/
            $where_str = "a.IsActive=1 and a.IsDeleted=0 and a.OrganizationID = '".$this->LoginOrganizationID."'";
        //}
        if ( $sWhere == "" )
        {
                $sWhere = "WHERE $where_str";
        }
        else
        {
                $sWhere .= "AND $where_str";
        }
        
        /* SQL queries Get data to display */
        $sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit"; 
        
        $rResult = $this->db->query($sQuery); 
        $rResult = $rResult->result_array(); 
      
        /* Data set length after filtering */
        $sQuery = "SELECT FOUND_ROWS() as cnt";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iFilteredTotal = $result->cnt;

        /* Total data set length */
        $sQuery = "SELECT COUNT(".$sIndexColumn.") as cnt
                FROM   $sTable  $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iTotal = $result->cnt;

        /* Output */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );

        foreach ( $rResult as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($bColumns) ; $i++ )
            {
                if($i == 0)
                {
                    $row[] = '';
                } 
                else if($bColumns[$i] == 'AccountName')
                {
                    $row[] = '<a href="'.SITE_URL.'Account/Actions/AccountDetails/'.$aRow[$bColumns[0]].'">'.$aRow[$bColumns[$i]].'</a>';
                }
                else if($i == 6)
                {
                    $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['AssignedTo'].'">'.$aRow[$bColumns[$i]].'</a>';
                }
                else if ( $bColumns[$i] != ' ' )
                {
                    $row[] = $aRow[$bColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode( $output );
    } 

    function CreateAccount() {
               
        extract($_POST); 

        $UserID = $this->LoginUserID;

        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'AssignedTo' => $AssignedTo,
            'AccountName' => $AccountName,
            'PhoneNo' => $PhoneNo,
            'PreferredTechnician' => (empty($PreferredTechnician)?NULL:$PreferredTechnician),
            'BillingAddress' => $BillingAddress,
            'BillingState' => $BillingState,
            'BillingCity' => $BillingCity,
            'BillingCountry' => $BillingCountry,
            'BillingPostalCode' => $BillingPostalCode,
            'BillingLatitude' => (double)$BillingLatitude,
            'BillingLongitude' => (double)$BillingLongitude,
            'ShippingAddress' => $ShippingAddress,
            'ShippingState' => $ShippingState,
            'ShippingCity' => $ShippingCity,
            'ShippingCountry' => $ShippingCountry,
            'ShippingPostalCode' => $ShippingPostalCode,
            'ShippingLatitude' => (double)$ShippingLatitude,
            'ShippingLongitude' => (double)$ShippingLongitude,
            'AccessNotes' => $AccessNotes,
            'PopUpReminder' => $PopUpReminder,
            'Notes' => $Notes,
            'AccountType' => $AccountType,
            'IsActive' => !empty($IsActive)?$IsActive:0,
            /*'LastActivityDate' => date("Y-m-d",strtotime($LastActivityDate)),
            'LastServiceDate' => date("Y-m-d",strtotime($LastServiceDate)),*/
            'PrimaryContact' => (empty($PrimaryContact)?NULL:$PrimaryContact)
        ); 
        $result = $this->db->insert('Account', $data);    

        $AccountID = $this->db->insert_id();

        $this->db->update('Account', array('AccountNo'=>'AC'.'-'.sprintf("%05d", $AccountID)), array('AccountID' => $AccountID));  

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Account', array('AccountID' => $AccountID));

        /* For insert custom fields */ 
        MY_Model::insertCustomFieldData('Account', $AccountID);
    
        return $AccountID;
    } 

    /*function CreateTask() {
                
        extract($_POST); 

        $UserID = $this->LoginUserID;

        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'Subject' => $Subject,
            'Description' => isset($Description)?$Description:NULL,
            'AssignedTo' => $AssignedTo,
            'TaskStatus' => $TaskStatus,
            'TaskPriority' => $TaskPriority,
            'DueDate' => date("Y-m-d",strtotime($DueDate)),
            'Who' => !empty($Who)?$Who:NULL,
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            'IsRecurrence' => isset($IsRecurring)?$IsRecurring:0,
            'RecurrenceID' => !empty($WORecurrenceID)?$WORecurrenceID:NULL
        ); 
        $result = $this->db->insert('Task', $data);    
       
        $TaskID = $this->db->insert_id(); 

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Task', array('TaskID' => $TaskID));
    
        return $result;
    } 

     function CreateEvent() {
                
        extract($_POST); 

        $UserID = $this->LoginUserID;

        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'Subject' => $Subject,
            'Description' => isset($Description)?$Description:NULL,
            'AssignedTo' => $AssignedTo,
            'EventType' => $EventType,
            'EventPriority' => $EventPriority,
            'EventStartDate' => date("Y-m-d H:i:s",strtotime($EventStartDate)),
            'EventEndDate' => date("Y-m-d H:i:s",strtotime($EventEndDate)),
            'EventStartTime' => date("H:i:s",strtotime($EventStartDate)),
            'EventEndTime' => date("H:i:s",strtotime($EventEndDate)),
            'Who' => !empty($Who)?$Who:NULL,
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            'IsAllDayEvent' => isset($IsAllDayEvent)?$IsAllDayEvent:0,
            'IsRecurrence' => isset($IsRecurring)?$IsRecurring:0,
            'RecurrenceID' => !empty($WORecurrenceID)?$WORecurrenceID:NULL
        ); 
        $result = $this->db->insert('Event', $data);   

        $EventID = $this->db->insert_id(); 

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));
    
        return $result;
    } */

    function removeRecurrenceSetting()
    {
        $WORecurrenceID = $_REQUEST['WORecurrenceID'];
        return $this->db->delete('WORecurrence', array('WORecurrenceID' => $WORecurrenceID));
    }

    function EditAccount($AccountID) {

        extract($_POST);          

        $UserID = $this->LoginUserID;

        $data = array(
            'AssignedTo' => $AssignedTo,
            'AccountName' => $AccountName,
            'PhoneNo' => $PhoneNo,
            'PreferredTechnician' => (empty($PreferredTechnician)?NULL:$PreferredTechnician),
            'BillingAddress' => $BillingAddress,
            'BillingState' => $BillingState,
            'BillingCity' => $BillingCity,
            'BillingCountry' => $BillingCountry,
            'BillingPostalCode' => $BillingPostalCode,
            'BillingLatitude' => (double)$BillingLatitude,
            'BillingLongitude' => (double)$BillingLongitude,
            'ShippingAddress' => $ShippingAddress,
            'ShippingState' => $ShippingState,
            'ShippingCity' => $ShippingCity,
            'ShippingCountry' => $ShippingCountry,
            'ShippingPostalCode' => $ShippingPostalCode,
            'ShippingLatitude' => (double)$ShippingLatitude,
            'ShippingLongitude' => (double)$ShippingLongitude,
            'AccessNotes' => $AccessNotes,
            'PopUpReminder' => $PopUpReminder,
            'Notes' => $Notes,
            'AccountType' => $AccountType,
            'IsActive' => !empty($IsActive)?$IsActive:0,
            /*'LastActivityDate' => date("Y-m-d",strtotime($LastActivityDate)),
            'LastServiceDate' => date("Y-m-d",strtotime($LastServiceDate)),*/
            'PrimaryContact' => (empty($PrimaryContact)?NULL:$PrimaryContact)
        ); 
       
        $result = $this->db->update('Account', $data, array('AccountID' => $AccountID));    
   
        MY_Model::updateCreatedModifiedByAndDt($UserID,'Account', array('AccountID' => $AccountID));

        /* For insert custom fields */ 
        MY_Model::updateCustomFieldData('Account', $AccountID);
    
        return $result;
    } 

    function getAccountData($AccountID, $RelatedObjName = 'AllObject')
    {
        $result = $this->db->update('Account', array('LastModifiedDate' => date("Y-m-d H:i:s")), array('AccountID' => $AccountID));  

        $query = $this->db->select('a.*, a.AccountName as Title, CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName, CONCAT(pc.FirstName, " ", pc.LastName) as PrimaryContactName, CONCAT(pmt.FirstName, " ", pmt.LastName) as PreferredTechnicianName, at.AccountType as AccountTypeName, DATE_FORMAT(a.LastActivityDate, "'.RES_DATE.'") as LastActivityDate, DATE_FORMAT(a.LastServiceDate, "'.RES_DATE.'") as LastServiceDate, DATE_FORMAT(a.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,DATE_FORMAT(a.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName, a.AssignedTo, a.CreatedBy, a.LastModifiedBy')
        ->from('Account a')
        ->join('User cb','cb.UserID = a.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = a.LastModifiedBy', 'left')
        ->join('User act','act.UserID = a.AssignedTo', 'left')
        ->join('AccountType at','at.AccountTypeID = a.AccountType', 'left')
        ->join('User ast','ast.UserID = a.AssignedTo', 'left')
        ->join('User pmt','pmt.UserID = a.PreferredTechnician', 'left')
        ->join('Contact pc','pc.ContactID = a.PrimaryContact', 'left')
        ->where(array('a.OrganizationID' => $this->LoginOrganizationID, 'a.AccountID' => $AccountID)) 
        ->get();
        $data['AccountData'] = $query->row();

        if($RelatedObjName == 'Contacts'  || $RelatedObjName == 'AllObject'){
            $this->db->select('c.ContactID, c.FirstName, c.LastName, c.Title, c.PhoneNo, c.Email');
            $this->db->from('Contact c') ;
            $this->db->join('User cb','cb.UserID = c.CreatedBy', 'left');
            $this->db->join('User lmb','lmb.UserID = c.LastModifiedBy', 'left');
            $this->db->where(array('c.Account' => $AccountID, 'c.OrganizationID' => $this->LoginOrganizationID, 'c.IsActive' => 1, 'c.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get();  
            $data['Contacts'] = $query->result();
        } 

        /*if($RelatedObjName == 'Locations'  || $RelatedObjName == 'AllObject'){
            $this->db->select('l.LocationID, l.Name, l.Address, l.City');
            $this->db->from('Location l');
            $this->db->where(array('l.Account' => $AccountID, 'l.OrganizationID' => $this->LoginOrganizationID, 'l.IsActive' => 1, 'l.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get();   
            $data['Locations'] = $query->result();
        }*/
        
        if($RelatedObjName == 'WorkOrders'  || $RelatedObjName == 'AllObject'){
            $this->db->select('wo.WorkOrderID, wot.WorkOrderType, wo.WorkOrderNo, wo.Subject, wop.Priority, wos.Status, DATE_FORMAT(wo.EndDate, "'.RES_DATETIME.'") as CreatedDate, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, wo.AssignedTo');
            $this->db->from('WorkOrder wo');
            $this->db->join('WorkOrderType wot','wot.WorkOrderTypeID = wo.WorkOrderType', 'left');
            $this->db->join('User at','at.UserID = wo.AssignedTo', 'left');
            $this->db->join('WOStatus wos','wos.WOStatusID = wo.WOStatus', 'left');
            $this->db->join('WOPriority wop','wop.WOPriorityID = wo.WOPriority', 'left');
            $this->db->where(array('wo.Account' => $AccountID, 'wo.OrganizationID' => $this->LoginOrganizationID, 'wo.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get();   
            $data['WorkOrders'] = $query->result();
        }

        if($RelatedObjName == 'Estimates'  || $RelatedObjName == 'AllObject'){
            $this->db->select('e.EstimateID ,e.EstimateNo, e.EstimateName, DATE_FORMAT(e.ExpirationDate, "'.RES_DATE.'") as ExpirationDate, (Select SUM(TotalPrice) FROM EstimateLine WHERE Estimate=e.EstimateID) as GrandTotal, es.Status, CONCAT(o.FirstName, " ", o.LastName) as OwnerName, DATE_FORMAT(e.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, e.Owner');
            $this->db->from('Estimate e');
            $this->db->join('User o','o.UserID = e.Owner', 'left');
            $this->db->join('EstimateStatus es','es.EstimateStatusID = e.EstimateStatus', 'left');
            $this->db->where(array('e.Account' => $AccountID, 'e.OrganizationID' => $this->LoginOrganizationID, 'e.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                    $this->db->limit(10);
            $query = $this->db->get();   
            $data['Estimates'] = $query->result();
        }

        if($RelatedObjName == 'Invoices'  || $RelatedObjName == 'AllObject'){
            $this->db->select('i.InvoiceID, i.InvoiceNo, (Select SUM(TotalPrice) FROM InvoiceLineItem WHERE Invoice=i.InvoiceID) as TotalPrice, (Select SUM(SubTotal) FROM InvoiceLineItem WHERE Invoice=i.InvoiceID) as SubTotal, is.InvoiceStatus, DATE_FORMAT(i.InvoiceDate, "'.RES_DATE.'") as InvoiceDate, DATE_FORMAT(i.DueDate, "'.RES_DATE.'") as DueDate, wo.Subject as WorkOrderName, i.WorkOrder');
            $this->db->from('Invoice i');
            $this->db->join('InvoiceStatus is','is.InvoiceStatusID = i.InvoiceStatus', 'left');
            $this->db->join('WorkOrder wo','wo.WorkOrderID = i.WorkOrder', 'left');
            $this->db->where(array('i.Account' => $AccountID, 'i.OrganizationID' => $this->LoginOrganizationID, 'i.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                    $this->db->limit(10);
            $query = $this->db->get();   
            $data['Invoices'] = $query->result();
        }

        if($RelatedObjName == 'Files'  || $RelatedObjName == 'AllObject'){
            $this->db->select('f.FileID, f.FileName, f.Subject, DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, f.ContentType, CONCAT(cr.FirstName, " ", cr.LastName) as CreatedByName, f.CreatedBy');
            $this->db->from('File f'); 
            $this->db->join('User cr','cr.UserID = f.CreatedBy', 'left');
            $this->db->where(array('f.What' => $AccountID, 'f.OrganizationID' => $this->LoginOrganizationID, 'f.RelatedTo' => 'Account', 'f.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                    $this->db->limit(10);
            $query = $this->db->get();   
            $data['Files'] = $query->result();
        }

        if($RelatedObjName == 'Events'  || $RelatedObjName == 'AllObject'){
            $this->db->select('e.EventID, e.Subject, e.Who, DATE_FORMAT(e.EventStartDate, "'.RES_DATETIME.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATETIME.'") as EventEndDate, DATE_FORMAT(e.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, CONCAT(cr.FirstName, " ", cr.LastName) as CreatedByName, CONCAT(cn.FirstName, " ", cn.LastName) as ContactName, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, e.AssignedTo, e.CreatedBy');
            $this->db->from('Event e'); 
            $this->db->join('User cr','cr.UserID = e.CreatedBy', 'left');
            $this->db->join('Contact cn','cn.ContactID = e.Who', 'left');
            $this->db->join('User at','at.UserID = e.AssignedTo', 'left');
            $this->db->where(array('e.What' => $AccountID, 'e.OrganizationID' => $this->LoginOrganizationID, 'e.RelatedTo' => 'Account', 'e.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                    $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Events'] = $query->result(); 
        }

        if($RelatedObjName == 'Tasks'  || $RelatedObjName == 'AllObject'){
            $this->db->select('t.TaskID, t.Subject, t.Who, DATE_FORMAT(t.DueDate, "'.RES_DATETIME.'") as DueDate, ts.TaskStatus, tp.Priority, CONCAT(cn.FirstName, " ", cn.LastName) as ContactName, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, tt.TaskType, t.AssignedTo');
            $this->db->from('Task t');
            $this->db->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left');
            $this->db->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left');
            $this->db->join('Contact cn','cn.ContactID = t.Who', 'left');
            $this->db->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left');
            $this->db->join('User at','at.UserID = t.AssignedTo', 'left');
            $this->db->where(array('t.What' => $AccountID, 't.OrganizationID' => $this->LoginOrganizationID, 't.RelatedTo' => 'Account', 't.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                    $this->db->limit(10);
            $query = $this->db->get();
            $data['Tasks'] = $query->result();
        }  
    
       /* if($RelatedObjName == 'Chemicals'  || $RelatedObjName == 'AllObject'){

            $this->db->select('p.ProductName, c.ChemicalNo, c.ApplicationAmount, CONCAT(cr.FirstName, " ", cr.LastName) as CreatedBy, DATE_FORMAT(p.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, wo.Subject, c.WorkOrder');
            $this->db->from('Chemical c'); 
            $this->db->join('User cr','cr.UserID = c.CreatedBy', 'left');
            $this->db->join('WorkOrder wo','wo.WorkOrderID = c.WorkOrder', 'left');
            $this->db->join('Product p','p.ProductID = c.Product', 'left');
            $this->db->where(array('c.Account' => $AccountID, 'c.OrganizationID' => $this->LoginOrganizationID, 'c.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                    $this->db->limit(10);
            $query = $this->db->get();   
            $data['Chemicals'] = $query->result();
        }  
       
        if($RelatedObjName == 'Products'  || $RelatedObjName == 'AllObject'){
            $this->db->select('p.ProductName, p.ProductCode, p.DefaultQuantity, p.ListPrice, DATE_FORMAT(p.DatePurchased, "'.RES_DATETIME.'") as DatePurchased, CONCAT(cr.FirstName, " ", cr.LastName) as CreatedBy, DATE_FORMAT(p.CreatedDate, "'.RES_DATETIME.'") as CreatedDate');
            $this->db->from('Product p'); 
            $this->db->join('User cr','cr.UserID = p.CreatedBy', 'left');
            $this->db->where(array('p.Account' => $AccountID, 'p.OrganizationID' => $this->LoginOrganizationID, 'p.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                    $this->db->limit(10);
            $query = $this->db->get();   
            $data['Products'] = $query->result();
        }*/

        /* For Open Activity */
        $data['OpenActivities'] = $this->GetActivities(10, $AccountID, 'Open');
        $data['CompletedActivities'] = $this->GetActivities(10, $AccountID, 'Completed');

        return $data;
    }

    function GetActivities($limit = 0, $AccountID, $ActivityState)
    { 
        $this->db->select('t.TaskID, t.Subject, DATE_FORMAT(t.DueDate, "'.RES_DATETIME.'") as Date');
        $this->db->from('Task t');
        $this->db->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left');
        $this->db->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left');
        if($ActivityState == 'Open')
            $this->db->where(array('ts.IsClosed' => 0, 't.OrganizationID' => $this->LoginOrganizationID, 't.What' => $AccountID, 't.RelatedTo' => 'Account', 't.IsDeleted' => 0));
        else if($ActivityState == 'Completed')
            $this->db->where(array('ts.IsClosed' => 1, 't.OrganizationID' => $this->LoginOrganizationID, 't.What' => $AccountID, 't.RelatedTo' => 'Account', 't.IsDeleted' => 0));
       // $this->db->order_by("t.CreatedDate", "asc");
        if($limit != 0)
            $this->db->limit($limit);
    
        $open_act_tsk_query = $this->db->get();   

        $this->db->select('e.EventID, e.Subject, DATE_FORMAT(e.EventStartDate, "'.RES_DATETIME.'") as Date');
        $this->db->from('Event e');
        $this->db->join('EventType et','et.EventTypeID = e.EventType', 'left');
        if($ActivityState == 'Open')
            $this->db->where(array('e.EventEndDate >=' => date("Y-m-d H:i:s"), 'e.What' => $AccountID, 'e.RelatedTo' => 'Account', 'e.OrganizationID' => $this->LoginOrganizationID, 'e.IsDeleted' => 0));
        else if($ActivityState == 'Completed')
            $this->db->where(array('e.EventEndDate <' => date("Y-m-d H:i:s"), 'e.What' => $AccountID, 'e.RelatedTo' => 'Account', 'e.OrganizationID' => $this->LoginOrganizationID, 'e.IsDeleted' => 0));
        //$this->db->order_by("e.CreatedDate", "asc");
        if($limit != 0)
            $this->db->limit($limit);
            
        $open_act_evt_query =  $this->db->get();   

        $OpenActivities = array_merge($open_act_tsk_query->result_array(), $open_act_evt_query->result_array());
        $open_act_arr = array();
        foreach ($OpenActivities as $key => $value) {

            if(isset($value['TaskID']))
            {
                $value['Type'] = 'Task';
            } else {
                $value['Type'] = 'Event';
            }
            $open_act_arr[] = $value;
        }
        if($ActivityState == 'Open')
            usort($open_act_arr, array($this,'date_compare_asc'));
        else if($ActivityState == 'Completed')
             usort($open_act_arr, array($this,'date_compare_desc'));

        return $open_act_arr;
    }

    function date_compare_asc($a, $b)
    {
        $t1 = strtotime($a['Date']);
        $t2 = strtotime($b['Date']);
        return $t1 - $t2;
    } 

    function date_compare_desc($a, $b)
    {
        $t1 = strtotime($a['Date']);
        $t2 = strtotime($b['Date']);
        return $t2 - $t1;
    } 
  
    function GetUsers()
    { 
        $query = $this->db->get_where('User', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetAccountTypes()
    { 
        $query = $this->db->get_where('AccountType', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        return $query->result();
    }

    function GetContacts($AccountID)
    { 
        $query = $this->db->get_where('Contact', array('Account' => $AccountID, 'OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetTaskTypes()
    { 
        $query = $this->db->get_where('TaskType', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetTaskStatus()
    {
        $query = $this->db->get_where('TaskStatus', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetTaskPriorities()
    {
        $query = $this->db->get_where('TaskPriority', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetEventTypes()
    {
        $query = $this->db->get_where('EventType', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetEventPriorities()
    {
        $query = $this->db->get_where('EventPriority', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetViews(){

        $query = $this->db->query("SELECT * FROM AccountView WHERE ((RestrictVisibility = 'VisibleOnlyToMe' AND UserID = '".$this->LoginUserID."') OR (RestrictVisibility = 'VisibleToEveryone')) AND OrganizationID='".$this->LoginOrganizationID."' ORDER BY AccountViewName ASC");
        
        return $query->result();
    }

    function DeleteAccount($AccountID)
    {
        return $this->db->update('Account', array('IsDeleted' => 1), array('AccountID' => $AccountID));  
    }
}

?>
