<?php
class ViewModel extends MY_Model{
 
    public $LoginUserID, $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }
  
    function ViewAccounts() 
    {
        if(isset($_GET['flag']) && $_GET['flag'] == 'NewAccountsThisWeek'){
            $aColumns = array('a.AccountID','CONCAT(ast.FirstName, " ", ast.LastName)','a.AccountName', 'at.AccountType', 'a.PhoneNo', 'a.BillingCity', 'a.BillingState', 'DATE_FORMAT(a.LastActivityDate, "'.RES_DATE.'")', 'DATE_FORMAT(a.LastServiceDate, "'.RES_DATE.'")','a.AccessNotes', 'CONCAT(pt.FirstName, " ", pt.LastName)','a.PopUpReminder','DATE_FORMAT(a.CreatedDate, "'.RES_DATETIME.'")','CONCAT(crby.FirstName, " ", crby.LastName)', 'a.AssignedTo', 'a.PreferredTechnician');

            $bColumns = array('AccountID','CONCAT(ast.FirstName, " ", ast.LastName)', 'AccountName', 'AccountType', 'PhoneNo', 'BillingCity', 'BillingState','DATE_FORMAT(a.LastActivityDate, "'.RES_DATE.'")','DATE_FORMAT(a.LastServiceDate, "'.RES_DATE.'")','AccessNotes', 'CONCAT(pt.FirstName, " ", pt.LastName)','PopUpReminder','DATE_FORMAT(a.CreatedDate, "'.RES_DATETIME.'")','CONCAT(crby.FirstName, " ", crby.LastName)', 'AssignedTo', 'PreferredTechnician');
        } else {

            $aColumns = array('a.AccountID','CONCAT(ast.FirstName, " ", ast.LastName)','a.AccountName', 'at.AccountType', 'a.PhoneNo', 'a.BillingCity', 'a.BillingState', 'DATE_FORMAT(a.LastActivityDate, "'.RES_DATE.'")', 'DATE_FORMAT(a.LastServiceDate, "'.RES_DATE.'")','a.AccessNotes', 'CONCAT(pt.FirstName, " ", pt.LastName)','a.PopUpReminder', 'a.AssignedTo', 'a.PreferredTechnician');

            $bColumns = array('AccountID','CONCAT(ast.FirstName, " ", ast.LastName)', 'AccountName', 'AccountType', 'PhoneNo', 'BillingCity', 'BillingState','DATE_FORMAT(a.LastActivityDate, "'.RES_DATE.'")','DATE_FORMAT(a.LastServiceDate, "'.RES_DATE.'")','AccessNotes', 'CONCAT(pt.FirstName, " ", pt.LastName)','PopUpReminder', 'AssignedTo', 'PreferredTechnician');
        }
      
        
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "a.AccountID";

        /* DB table to use */
        $sTable = "Account a
        LEFT JOIN User ast ON ast.UserID = a.AssignedTo
        LEFT JOIN User pt ON pt.UserID = a.PreferredTechnician
        LEFT JOIN User crby ON crby.UserID = a.CreatedBy
        LEFT JOIN AccountType at ON at.AccountTypeID = a.AccountType";

        /* Limit */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".( $_GET['iDisplayStart'] ).", ".
                        ( $_GET['iDisplayLength'] );
        }

        /* Ordering */
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                    {
                            $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                                    ".( $_GET['sSortDir_'.$i] ) .", ";
                    }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                    $sOrder = "";
            }
        }

        /* Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited */
        $sWhere = "";
        if ( $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                        $sWhere .= $aColumns[$i]." LIKE '%".( $_GET['sSearch'] )."%' OR ";
                }
                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ')';
        }
        
        /* Where Condition */
        $UserID = $this->session->userdata('UserID');
        if(isset($_GET['flag']) && $_GET['flag'] == 'MyAccounts')
            $where_str = "a.AssignedTo = $UserID and a.IsDeleted=0 and a.OrganizationID = '".$this->LoginOrganizationID."'";
        else if(isset($_GET['flag']) && $_GET['flag'] == 'NewAccountsThisWeek'){
            $where_str = "a.IsDeleted=0 and (YEARWEEK(a.CreatedDate, 1) = YEARWEEK(CURDATE(), 1)) and a.OrganizationID = '".$this->LoginOrganizationID."'";
        } else {
            $where_str = "a.IsDeleted=0 and a.OrganizationID = '".$this->LoginOrganizationID."'";
        }
        
        if ( $sWhere == "" )
        {
                $sWhere = "WHERE $where_str";
        }
        else
        {
                $sWhere .= "AND $where_str";
        }
        
        /* SQL queries Get data to display */
        $sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit";  
        
        $rResult = $this->db->query($sQuery); 
        $rResult = $rResult->result_array(); 
      
        /* Data set length after filtering */
        $sQuery = "SELECT FOUND_ROWS() as cnt";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iFilteredTotal = $result->cnt;

        /* Total data set length */
        $sQuery = "SELECT COUNT(".$sIndexColumn.") as cnt
                FROM   $sTable  $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iTotal = $result->cnt;

        /* Output */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );

        foreach ( $rResult as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($bColumns) ; $i++ )
            {
                if($i == 0)
                {
                    $row[] = '<a href="'.SITE_URL.'Account/Actions/EditAccount/'.$aRow[$bColumns[0]].'"><i class="fa fa-pencil-square-o"></i></a>';
                } 
                else if($i == 1)
                {
                     $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['AssignedTo'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($i == 10)
                {
                     $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['PreferredTechnician'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($bColumns[$i] == 'AccountName')
                {
                    $row[] = '<a href="'.SITE_URL.'Account/Actions/AccountDetails/'.$aRow[$bColumns[0]].'">'.$aRow[$bColumns[$i]].'</a>';
                }
                else if($bColumns[$i] == 'Action')
                {
                    $row[] = '<select><option></option></select>';
                } 
                else if ( $bColumns[$i] != ' ' )
                {
                    $row[] = $aRow[$bColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode( $output );
    } 

    /*function SaveAccountFilter($FilterFields, $FilterConditions, $FilterValues)
    {

         if(count($FilterValues) > 0){ 

            $res = $this->db->delete('AccountFilter', array('UserID' => $this->LoginUserID));

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'UserID' => $this->LoginUserID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('AccountFilter', $data);  
                }
            }

            $aColumns = array('a.AccountID','CONCAT(ast.FirstName, " ", ast.LastName)','a.AccountName', 'at.AccountType', 'a.PhoneNo', 'a.BillingCity', 'a.BillingState', 'DATE_FORMAT(a.LastActivityDate, "'.RES_DATE.'")', 'DATE_FORMAT(a.LastServiceDate, "'.RES_DATE.'")','a.AccessNotes', 'CONCAT(pt.FirstName, " ", pt.LastName)','a.PopUpReminder','a.AccountID');            
            $this->db->select('a.AccountID, a.PhoneNo, a.AccountName,  a.BillingState, a.BillingCity, at.AccountType, DATE_FORMAT(a.LastServiceDate, "'.RES_DATE.'") as LastServiceDate, DATE_FORMAT(a.LastActivityDate, "'.RES_DATE.'") as LastActivityDate, CONCAT(ast.FirstName, " ", ast.LastName) as OwnerName, a.PopUpReminder, CONCAT(pt.FirstName, " ", pt.LastName) as PreferredTechnician, a.AccessNotes');
            $this->db->from('Account a');
            $this->db->join('User ast','ast.UserID = a.AssignedTo', 'left');
            $this->db->join('User pt','pt.UserID = a.PreferredTechnician', 'left');
            $this->db->join('AccountType at','at.AccountTypeID = a.AccountType', 'left');
            $this->db->where(array('a.IsActive' => 1, 'a.IsDeleted' => 0));

            foreach ($FilterValues as $key => $value) {
                if($FilterConditions[$key] == 'Equals'){
                    $this->db->where($FilterFields[$key], $FilterValues[$key]);  
                } else if($FilterConditions[$key] == 'Contains'){
                    $this->db->like($FilterFields[$key], $FilterValues[$key]);   
                } else if($FilterConditions[$key] == 'StartsWith'){
                    $this->db->like($FilterFields[$key], $FilterValues[$key], 'after');   
                } else if($FilterConditions[$key] == 'DoesNotContain'){
                    $this->db->not_like($FilterFields[$key], $FilterValues[$key]);   
                } 
            } 
            $query = $this->db->get(); 

            return $query->result(); 
 
        }
    }*/

    function CreateNewView()
    {
        extract($_POST);

        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'AccountViewName' => $AccountViewName,
            'UserID' => $this->LoginUserID,
            'RestrictVisibility' => $RestrictVisibility,
            'SpecifyFieldsDisplay' => implode(",", $SpecifyFieldsDisplay),
            'CreatedDate' => date("Y-m-d H:i:s"),
            'LastModifiedDate' => date("Y-m-d H:i:s")
        );     
        $query = $this->db->insert('AccountView', $data);  
        $AccountViewID = $this->db->insert_id();

        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'AccountViewID' => $AccountViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('AccountFilter', $data);  
                }
            } 
 
        }
        return $AccountViewID;
    }

    function EditCustomView($AccountViewID)
    {  
        extract($_POST);
  
        $data = array(
            'AccountViewName' => $AccountViewName,
            'UserID' => $this->LoginUserID,
            'RestrictVisibility' => $RestrictVisibility,
            'SpecifyFieldsDisplay' => implode(",", $SpecifyFieldsDisplay),
            'CreatedDate' => date("Y-m-d H:i:s"),
            'LastModifiedDate' => date("Y-m-d H:i:s")
        );     
        $query = $this->db->update('AccountView', $data, array('AccountViewID' => $AccountViewID));  

        $this->db->delete('AccountFilter', array('AccountViewID' => $AccountViewID));  
        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'AccountViewID' => $AccountViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('AccountFilter', $data);  
                }
            } 
 
        }
        return $query;
    }

    /*function GetAccountViewData($AccountViewID, $rowno, $rowperpage, $sortBy, $order){

        $query = $this->db->get_where('AccountView', array('AccountViewID' => $AccountViewID)); 
        $AccountViewData = $query->row();

        $query = $this->db->get_where('AccountFilter', array('AccountViewID' => $AccountViewID)); 
        $AccountFilterData = $query->result_array();

        $ColName = "a.AccountID,pc.ContactID, a.AssignedTo as AssignedToID, a.PreferredTechnician as PreferredTechnicianID, a.CreatedBy as CreatedByID, a.LastModifiedBy as LastModifiedByID, ";

        if(!empty($AccountViewData->SpecifyFieldsDisplay)){
            $SpecifyFieldsDisplays = explode(",", $AccountViewData->SpecifyFieldsDisplay);
            foreach ($SpecifyFieldsDisplays as $key => $value) {
                if($value == 'AssignedTo'){
                    $ColName .= "CONCAT(ast.FirstName, ' ', ast.LastName) as AssignedTo";
                } else if($value == 'AccountType'){
                    $ColName .= "at.AccountType";
                } else if($value == 'PreferredTechnician'){
                    $ColName .= "CONCAT(pt.FirstName, ' ', pt.LastName) as PreferredTechnician";
                } else if($value == 'PrimaryContact'){
                    $ColName .= "CONCAT(pc.FirstName, ' ', pc.LastName) as PrimaryContact";
                } else if($value == 'CreatedBy'){
                    $ColName .= "CONCAT(cr.FirstName, ' ', cr.LastName) as CreatedBy";
                } else if($value == 'LastModifiedBy'){
                    $ColName .= "CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedBy";
                } else if($value == 'LastActivityDate'){
                    $ColName .= "DATE_FORMAT(a.LastActivityDate, '".RES_DATE."') as LastActivityDate";
                } else if($value == 'LastServiceDate'){
                    $ColName .= "DATE_FORMAT(a.LastServiceDate, '".RES_DATE."') as LastServiceDate";
                }  else {
                    $ColName .= "a.".$value;
                }

                if($key != count($SpecifyFieldsDisplays)){
                    $ColName .= ", ";
                }
            }
        }
              
         // Filter Account Data Start 
            $this->db->select($ColName);
            $this->db->from('Account a');
            $this->db->join('User ast','ast.UserID = a.AssignedTo', 'left');
            $this->db->join('User pt','pt.UserID = a.PreferredTechnician', 'left');
            $this->db->join('User cr','cr.UserID = a.CreatedBy', 'left');
            $this->db->join('User lmb','lmb.UserID = a.LastModifiedBy', 'left');
            $this->db->join('Contact pc','pc.ContactID = a.PrimaryContact', 'left');
            $this->db->join('AccountType at','at.AccountTypeID = a.AccountType', 'left');
            $this->db->where(array('a.OrganizationID' => $this->LoginOrganizationID, 'a.IsActive' => 1, 'a.IsDeleted' => 0));

            foreach ($AccountFilterData as $key => $value) {

                $WhereField = "";
                if($value['FilterField'] == 'AssignedTo'){
                    $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                } else if($value['FilterField'] == 'PreferredTechnician') {
                    $WhereField = "CONCAT(pt.FirstName, ' ', pt.LastName)";
                } else if($value['FilterField'] == 'CreatedBy') {
                    $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                } else if($value['FilterField'] == 'LastModifiedBy') {
                    $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                } else if($value['FilterField'] == 'PrimaryContact') {
                    $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                } else if($value['FilterField'] == 'AccountType') {
                    $WhereField = "at.AccountType";
                } else if($value['FilterField'] == 'LastActivityDate'){
                     $WhereField = "DATE_FORMAT(a.LastActivityDate, '".RES_DATE."')";
                } else if($value['FilterField'] == 'LastServiceDate'){
                     $WhereField = "DATE_FORMAT(a.LastServiceDate, '".RES_DATE."')";
                } else {
                    $WhereField = "a.".$value['FilterField'];
                }

                if($value['FilterCondition'] == 'Equals'){
                    $this->db->where($WhereField.' =',  $value['FilterValue']);  
                } else if($value['FilterCondition'] == 'NotEqualTo'){
                    $this->db->where($WhereField.' !=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'LessThan'){
                    $this->db->where($WhereField.' <', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'GreaterThan'){
                    $this->db->where($WhereField.' >', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'LessOREqualTo'){
                    $this->db->where($WhereField.' <=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'GreaterOREqualTo'){
                    $this->db->where($WhereField.' >=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'Contains'){
                    $this->db->like($WhereField, $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'StartsWith'){ 
                    $this->db->like($WhereField, $value['FilterValue'], 'after');   
                } else if($value['FilterCondition'] == 'DoesNotContain'){
                    $this->db->not_like($WhereField, $value['FilterValue']);   
                } 
            } 
              
            if(!empty($sortBy) && !empty($order))
                $this->db->order_by($sortBy, $order);

            $tempdb = clone $this->db;
            $num_rows = $tempdb->count_all_results();

            $this->db->limit($rowperpage, $rowno); 
            $query = $this->db->get(); 
            
            //echo $this->db->last_query();exit;
            $data['ColumnsNames'] = $SpecifyFieldsDisplays;
            $data['ColumnsValues'] = $query->result();
            $data['TotalRows'] = $num_rows;
            return $data; 
    }*/

    function GetAccountViewData($AccountViewID, $rowno, $rowperpage, $sortBy, $order){

        $query = $this->db->get_where('AccountView', array('AccountViewID' => $AccountViewID)); 
        $AccountViewData = $query->row();

        $query = $this->db->get_where('AccountFilter', array('AccountViewID' => $AccountViewID)); 
        $AccountFilterData = $query->result_array();

        $query = $this->db->get_where('CustomFields', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0, 'Object' => 'Account')); 
        
        $CustomFields = $query->result();
        $CustomFieldArr = array();
        $Where = "";
        $FieldName = "";
        $TableAlias = "";
        $CFWhere = "";
        $CFWhere2 = "";
        $SortBy = "";
        $leftJoin = "";

        $ColName = "a.AccountID,pc.ContactID, a.AssignedTo as AssignedToID, a.PreferredTechnician as PreferredTechnicianID, a.CreatedBy as CreatedByID, a.LastModifiedBy as LastModifiedByID, ";
       
       if(!empty($AccountViewData->SpecifyFieldsDisplay)){
            $SpecifyFieldsDisplays = explode(",", $AccountViewData->SpecifyFieldsDisplay);

            $tblAlias = "";
            $x = 1;
            foreach($CustomFields as $key => $CustomField){

                if(in_array($CustomField->FieldName, $SpecifyFieldsDisplays)){

                    $CustomFieldArr[] = $CustomField->FieldName;

                    $FieldName .= " , ac".$x.".FieldValue '".$CustomField->FieldName."'";

                    $leftJoin .= " LEFT JOIN AccountCustomFieldValues AS ac".$x."
                    ON ac".$x.".AccountID = ac.AccountID AND ac".$x.".CustomFieldID = ".$CustomField->CustomFieldID;

                    $x++;
                }
            }

            foreach ($SpecifyFieldsDisplays as $key => $value) {
                if($value == 'AssignedTo'){
                    $ColName .= "CONCAT(ast.FirstName, ' ', ast.LastName) as AssignedTo";
                } else if($value == 'AccountType'){
                    $ColName .= "at.AccountType";
                } else if($value == 'PreferredTechnician'){
                    $ColName .= "CONCAT(pt.FirstName, ' ', pt.LastName) as PreferredTechnician";
                } else if($value == 'PrimaryContact'){
                    $ColName .= "CONCAT(pc.FirstName, ' ', pc.LastName) as PrimaryContact";
                } else if($value == 'CreatedBy'){
                    $ColName .= "CONCAT(cr.FirstName, ' ', cr.LastName) as CreatedBy";
                } else if($value == 'LastModifiedBy'){
                    $ColName .= "CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedBy";
                } else if($value == 'LastActivityDate'){
                    $ColName .= "DATE_FORMAT(a.LastActivityDate, '".RES_DATE."') as LastActivityDate";
                } else if($value == 'LastServiceDate'){
                    $ColName .= "DATE_FORMAT(a.LastServiceDate, '".RES_DATE."') as LastServiceDate";
                } else if(!in_array($value, $CustomFieldArr)){
                    $ColName .= "a.".$value;
                } else if(in_array($value, $CustomFieldArr)){
                    $ColName .= "accf.".$value;
                }

                if(($key+1) != count($SpecifyFieldsDisplays)){
                    $ColName .= ", ";
                }
            }
        }

        foreach ($AccountFilterData as $key => $value) {

                $WhereField = "";
                if($value['FilterField'] == 'AssignedTo'){
                    $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                } else if($value['FilterField'] == 'PreferredTechnician') {
                    $WhereField = "CONCAT(pt.FirstName, ' ', pt.LastName)";
                } else if($value['FilterField'] == 'CreatedBy') {
                    $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                } else if($value['FilterField'] == 'LastModifiedBy') {
                    $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                } else if($value['FilterField'] == 'PrimaryContact') {
                    $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                } else if($value['FilterField'] == 'AccountType') {
                    $WhereField = "at.AccountType";
                } else if($value['FilterField'] == 'LastActivityDate'){
                     $WhereField = "DATE_FORMAT(a.LastActivityDate, '".RES_DATE."')";
                } else if($value['FilterField'] == 'LastServiceDate'){
                     $WhereField = "DATE_FORMAT(a.LastServiceDate, '".RES_DATE."')";
                } else if(!in_array($value['FilterField'], $CustomFieldArr)){
                    $WhereField = "a.".$value['FilterField'];
                } else if(in_array($value['FilterField'], $CustomFieldArr)){
                    $WhereField = "accf.".$value['FilterField'];
                }

                if($value['FilterCondition'] == 'Equals'){
                    $Where .= " AND ".$WhereField." = '".$value['FilterValue']."'";  
                } else if($value['FilterCondition'] == 'NotEqualTo'){
                    $Where .= " AND ".$WhereField." != '".$value['FilterValue']."'";   
                } else if($value['FilterCondition'] == 'LessThan'){
                    $Where .= " AND ".$WhereField." < '".$value['FilterValue']."'";   
                } else if($value['FilterCondition'] == 'GreaterThan'){
                    $Where .= " AND ".$WhereField." > '".$value['FilterValue']."'";   
                } else if($value['FilterCondition'] == 'LessOREqualTo'){
                    $Where .= " AND ".$WhereField." <= '".$value['FilterValue']."'";   
                } else if($value['FilterCondition'] == 'GreaterOREqualTo'){
                    $Where .= " AND ".$WhereField." >= '".$value['FilterValue']."'";   
                } else if($value['FilterCondition'] == 'Contains'){
                    $Where .= " AND ".$WhereField." LIKE '%".$value['FilterValue']."%'";   
                } else if($value['FilterCondition'] == 'StartsWith'){ 
                    $Where .= " AND ".$WhereField." LIKE '%".$value['FilterValue']."'";   
                } else if($value['FilterCondition'] == 'DoesNotContain'){
                    $Where .= " AND ".$WhereField." NOT LIKE '%".$value['FilterValue']."%'";   
                } 
        }

        if(!empty($sortBy) && !empty($order)){
            if(in_array($sortBy, $CustomFieldArr)){
                $SortBy = "ORDER BY accf.$sortBy $order";
            } else {
                $SortBy = "ORDER BY $sortBy $order";
            }
        }

        if(!empty($CustomFieldArr)){

/*             $query = "SELECT $ColName FROM
                ( 
                    SELECT $tblAlias.AccountID $FieldName 
                    FROM $TableAlias
                    WHERE $tblAlias.OrganizationID = ".$this->LoginOrganizationID."
                    $CFWhere
                    $CFWhere2
                )  accf
                RIGHT JOIN Account a ON accf.AccountID = a.AccountID
                LEFT JOIN User ast ON ast.UserID = a.AssignedTo
                LEFT JOIN User pt ON pt.UserID = a.PreferredTechnician
                LEFT JOIN User cr ON cr.UserID = a.CreatedBy
                LEFT JOIN User lmb ON lmb.UserID = a.LastModifiedBy
                LEFT JOIN Contact pc ON pc.ContactID = a.PrimaryContact
                LEFT JOIN AccountType at ON at.AccountTypeID = a.AccountType
                WHERE a.OrganizationID = ".$this->LoginOrganizationID." AND a.IsActive = 1 AND a.IsDeleted = 0
                $Where $SortBy";*/

            $query = "SELECT $ColName FROM
                (
                    SELECT DISTINCT ac.AccountID $FieldName FROM Account ac
                    $leftJoin
                )  accf
                RIGHT JOIN Account a ON accf.AccountID = a.AccountID
                LEFT JOIN User ast ON ast.UserID = a.AssignedTo
                LEFT JOIN User pt ON pt.UserID = a.PreferredTechnician
                LEFT JOIN User cr ON cr.UserID = a.CreatedBy
                LEFT JOIN User lmb ON lmb.UserID = a.LastModifiedBy
                LEFT JOIN Contact pc ON pc.ContactID = a.PrimaryContact
                LEFT JOIN AccountType at ON at.AccountTypeID = a.AccountType
                WHERE a.OrganizationID = ".$this->LoginOrganizationID." AND a.IsActive = 1 AND a.IsDeleted = 0
                $Where $SortBy";

        } else{

            $query = "SELECT $ColName FROM Account a
            LEFT JOIN User ast ON ast.UserID = a.AssignedTo
            LEFT JOIN User pt ON pt.UserID = a.PreferredTechnician
            LEFT JOIN User cr ON cr.UserID = a.CreatedBy
            LEFT JOIN User lmb ON lmb.UserID = a.LastModifiedBy
            LEFT JOIN Contact pc ON pc.ContactID = a.PrimaryContact
            LEFT JOIN AccountType at ON at.AccountTypeID = a.AccountType
            WHERE a.OrganizationID = ".$this->LoginOrganizationID." AND a.IsActive = 1 AND a.IsDeleted = 0
            $Where $SortBy";
        }
        //echo $query;exit;
        $query1 = $this->db->query($query." LIMIT $rowno,$rowperpage");

        $query2 = $this->db->query($query); 

        $num_rows = $query2->num_rows();

        $data['ColumnsNames'] = $SpecifyFieldsDisplays;
        $data['ColumnsValues'] = $query1->result();
        $data['TotalRows'] = $num_rows;
        return $data; 
    }

    function GetCustomView($AccountViewID)
    {
        $query = $this->db->get_where('AccountView', array('OrganizationID' => $this->LoginOrganizationID, 'AccountViewID' => $AccountViewID)); 
        
        $data['AccountViewData'] = $query->row();

        $query = $this->db->get_where('AccountFilter', array('AccountViewID' => $AccountViewID)); 
        
        $data['AccountFilterData'] = $query->result();

        return $data;
    }

    /*function GetAccountFilter()
    {
        $query = $this->db->get_where('AccountFilter', array('UserID' => $this->LoginUserID)); 
        
        return $query->result();
    }*/

    function GetViews(){

        $query = $this->db->query("SELECT * FROM AccountView WHERE ((RestrictVisibility = 'VisibleOnlyToMe' AND UserID = '".$this->LoginUserID."') OR (RestrictVisibility = 'VisibleToEveryone')) AND OrganizationID='".$this->LoginOrganizationID."' ORDER BY AccountViewName ASC");
        
        return $query->result();
    }

    function AccountViewFields()
    {
        $query = $this->db->get_where('CustomFields', array('Object' => 'Account', 'IsDeleted' => 0, 'OrganizationID' => $this->LoginOrganizationID, 'AddToPageLayout' => 1)); 
        
        $CustomFields = $query->result();

        $query = $this->db->get_where('AccountViewFields'); 
        
        $DefaultFields = $query->result();

        return array_merge($DefaultFields, $CustomFields);
    }

    function DeleteCustomView($AccountViewID)
    {
        return $this->db->delete('AccountView', array('OrganizationID' => $this->LoginOrganizationID, 'AccountViewID' => $AccountViewID)); 
    }

    function CopyCustomView($AccountViewID)
    {
       $query = $this->db->get_where('AccountView', array('AccountViewID' => $AccountViewID)); 
      
        foreach ($query->result() as $row){   
           foreach($row as $key=>$val){ 
                if($key != 'AccountViewID'){    
                    $this->db->set($key, $val); 
                }                
           }
        }
        $this->db->insert('AccountView');
        $NewAccountViewID = $this->db->insert_id();

        $query = $this->db->get_where('AccountFilter', array('AccountViewID' => $AccountViewID)); 
       
        foreach ($query->result() as $row){   
           foreach($row as $key => $val){ 
                if($key == 'AccountViewID'){  
                    $this->db->set($key, $NewAccountViewID);  
                } else if($key != 'AccountFilterID'){    
                    $this->db->set($key, $val); 
                }             
           }
           $this->db->insert('AccountFilter');
        }

        return 1; 
    }

    function RenameCustomView($AccountViewID){

        $data = array(
            'AccountViewName' => $_POST['AccountViewName']
        );

        return $this->db->update('AccountView', $data, array('AccountViewID' => $AccountViewID));  
    }

    function EditSharingCustomView($AccountViewID){

        $data = array(
            'RestrictVisibility' => $_REQUEST['RestrictVisibility']
        );

        return $this->db->update('AccountView', $data, array('AccountViewID' => $AccountViewID));  
    }

    function EditFiltersCustomView($AccountViewID){ 

        extract($_POST);

        $this->db->delete('AccountFilter', array('AccountViewID' => $AccountViewID));  
        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'AccountViewID' => $AccountViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('AccountFilter', $data);  
                }
            } 
 
        }
        return $query; 
    }

    function EditDisplayedColumnsCustomView($AccountViewID){

        $data = array(
            'SpecifyFieldsDisplay' => implode(",", $_REQUEST['SpecifyFieldsDisplay'])
        );

        return $this->db->update('AccountView', $data, array('AccountViewID' => $AccountViewID));  
    }
    
}

?>
