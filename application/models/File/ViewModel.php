<?php

class ViewModel extends MY_Model{
 
    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }
 
    function ViewFiles() 
    {
        $mainColumns = array('FileID', 'AssignedToName', 'Subject', 'FileName', 'ObjName', 'Description', 'ContentType', 'CreatedDate', 'CreatedByName', 'RelatedTo', 'What', 'AssignedTo', 'CreatedBy');

        $aColumns = array('f.FileID','CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName', 'f.Subject', 'f.FileName', 'a.AccountName as ObjName', 'f.Description', 'f.ContentType', 'DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate', 'CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName', 'f.LastModifiedDate', 'f.RelatedTo', 'f.What', 'f.AssignedTo', 'f.CreatedBy');

        $wColumns = array('f.FileID','CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName', 'f.Subject', 'f.FileName', 'wo.Subject as ObjName','f.Description', 'f.ContentType', 'DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate', 'CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName', 'f.LastModifiedDate', 'f.RelatedTo', 'f.What', 'f.AssignedTo', 'f.CreatedBy');

        $eColumns = array('f.FileID','CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName', 'f.Subject', 'f.FileName', 'e.EstimateName as ObjName','f.Description', 'f.ContentType', 'DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate', 'CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName', 'f.LastModifiedDate', 'f.RelatedTo', 'f.What', 'f.AssignedTo', 'f.CreatedBy');

        $cColumns = array('f.FileID','CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName', 'f.Subject', 'f.FileName', 'CONCAT(ct.FirstName, " ", ct.LastName) as ObjName','f.Description', 'f.ContentType', 'DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate', 'CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName', 'f.LastModifiedDate', 'f.RelatedTo', 'f.What', 'f.AssignedTo', 'f.CreatedBy');

        $iColumns = array('f.FileID','CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName', 'f.Subject', 'f.FileName', 'i.InvoiceNo as ObjName','f.Description', 'f.ContentType', 'DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate', 'CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName', 'f.LastModifiedDate', 'f.RelatedTo', 'f.What', 'f.AssignedTo', 'f.CreatedBy');

        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "f.FileID";

        /* DB table to use */
        $sTable = "File f
        LEFT JOIN User ast ON ast.UserID = f.AssignedTo
        LEFT JOIN User cb ON cb.UserID = f.CreatedBy";

        /* Limit */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".( $_GET['iDisplayStart'] ).", ".
                        ( $_GET['iDisplayLength'] );
        }

        /* Ordering */
        $sOrder = "ORDER BY LastModifiedDate DESC";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                    {
                            $sOrder .= $mainColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                                    ".( $_GET['sSortDir_'.$i] ) .", ";
                    }
            }
           
            $sOrder = substr_replace( $sOrder, "", -2 );

            if ( $sOrder == "ORDER BY" )
            {
                    $sOrder = "";
            } 
        }

        /* Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited */
        $sWhere = "";
        if ( $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
                for ( $i=0 ; $i<count($mainColumns) ; $i++ )
                {
                        $sWhere .= $mainColumns[$i]." LIKE '%".( $_GET['sSearch'] )."%' OR ";
                }
                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ')';
        }
        
        /* Where Condition */
        $UserID = $this->session->userdata('UserID');
        if(isset($_GET['flag']) && $_GET['flag'] == 'MyFiles'){

            $Where = "Where f.AssignedTo = $UserID AND f.OrganizationID='".$this->LoginOrganizationID."' AND f.IsDeleted=0";
        }
        else if(isset($_GET['flag']) && $_GET['flag'] == 'AllFiles'){

            $Where = "Where f.OrganizationID='".$this->LoginOrganizationID."' AND f.IsDeleted=0";

        } else if(isset($_GET['flag']) && $_GET['flag'] == 'NewFilesCreatedThisWeek'){

            $Where = "Where YEARWEEK(f.CreatedDate, 1) = YEARWEEK(CURDATE(), 1) AND f.OrganizationID='".$this->LoginOrganizationID."' AND f.IsDeleted=0";

        } else {
            $Where = "Where f.IsDeleted=0 AND f.OrganizationID='".$this->LoginOrganizationID."'";
        }

        $sqlUnionQuery = "(SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
        FROM $sTable
        JOIN Account a ON f.What = a.AccountID 
        $Where AND f.RelatedTo = 'Account')
        UNION ALL
        (SELECT ".str_replace(" , ", " ", implode(", ", $wColumns))."
        FROM $sTable
        JOIN WorkOrder wo ON f.What = wo.WorkOrderID
        $Where AND f.RelatedTo = 'WorkOrder')
        UNION ALL
        (SELECT ".str_replace(" , ", " ", implode(", ", $eColumns))."
        FROM $sTable
        JOIN Estimate e ON f.What = e.EstimateID
        $Where AND f.RelatedTo = 'Estimate')
        UNION ALL
        (SELECT ".str_replace(" , ", " ", implode(", ", $cColumns))."
        FROM $sTable
        JOIN Contact ct ON f.What = ct.ContactID
        $Where AND f.RelatedTo = 'Contact')
        UNION ALL
        (SELECT ".str_replace(" , ", " ", implode(", ", $iColumns))."
        FROM $sTable
        JOIN Invoice i ON f.What = i.InvoiceID
        $Where AND f.RelatedTo = 'Invoice')";

        /* SQL queries Get data to display */
        $sQuery = "SELECT ".str_replace(" , ", " ", implode(", ", $mainColumns))." FROM
        (".$sqlUnionQuery.") results
        $sWhere
        $sOrder
        $sLimit";  
        
        $rResult = $this->db->query($sQuery); 
        $rResult = $rResult->result_array(); 
      
        /* Data set length after filtering */
        $sQuery = "SELECT COUNT(*) as cnt FROM (".$sqlUnionQuery.") results $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iFilteredTotal = $result->cnt;

        /* Total data set length */
        $sQuery = "SELECT COUNT(*) as cnt FROM (".$sqlUnionQuery.") results $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iTotal = $result->cnt;

        /* Output */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );

        foreach ( $rResult as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($mainColumns) ; $i++ )
            {
                if($i == 0)
                {
                    $row[] = '<a href="'.SITE_URL.'File/Actions/EditFile/'.$aRow[$mainColumns[0]].'"><i class="fa fa-pencil-square-o"></i></a>';
                } 
                else if($mainColumns[$i] == 'AssignedToName')
                {
                    $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['AssignedTo'].'">'.$aRow[$mainColumns[$i]].'</a>';
                }
                else if($mainColumns[$i] == 'CreatedByName')
                {
                    $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['CreatedBy'].'">'.$aRow[$mainColumns[$i]].'</a>';
                }
                else if($mainColumns[$i] == 'Subject')
                {
                    $row[] = '<a href="'.SITE_URL.'File/Actions/FileDetails/'.$aRow[$mainColumns[0]].'">'.$aRow[$mainColumns[$i]].'</a>';
                }
                else if($mainColumns[$i] == 'ObjName')
                {
                    $row[] = '<a href="'.SITE_URL.$aRow['RelatedTo'].'/Actions/'.$aRow['RelatedTo'].'Details/'.$aRow['What'].'">'.$aRow[$mainColumns[$i]].'</a>';
                }
                else if ( $mainColumns[$i] != ' ' )
                {
                    $row[] = $aRow[$mainColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode( $output );
    } 

    function CreateNewView()
    {
        extract($_POST);

        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'FileViewName' => $FileViewName,
            'UserID' => $this->LoginUserID,
            'RestrictVisibility' => $RestrictVisibility,
            'SpecifyFieldsDisplay' => implode(",", $SpecifyFieldsDisplay),
            'CreatedDate' => date("Y-m-d H:i:s"),
            'LastModifiedDate' => date("Y-m-d H:i:s")
        );     
        $query = $this->db->insert('FileView', $data);  
        $FileViewID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($this->LoginUserID, 'FileView', array('FileViewID' => $FileViewID));

        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'FileViewID' => $FileViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('FileFilter', $data);  
                }
            } 
 
        }
        return $FileViewID;
    }

    function EditCustomView($FileViewID)
    {  
        extract($_POST);
  
        $data = array(
            'FileViewName' => $FileViewName,
            'UserID' => $this->LoginUserID,
            'RestrictVisibility' => $RestrictVisibility,
            'SpecifyFieldsDisplay' => implode(",", $SpecifyFieldsDisplay),
            'CreatedDate' => date("Y-m-d H:i:s"),
            'LastModifiedDate' => date("Y-m-d H:i:s")
        );     
        $query = $this->db->update('FileView', $data, array('FileViewID' => $FileViewID));  

        $this->db->delete('FileFilter', array('FileViewID' => $FileViewID));  
        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'FileViewID' => $FileViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('FileFilter', $data);  
                }
            } 
 
        }
        return $query;
    }

    function GetFileViewData($FileViewID){

        $query = $this->db->get_where('FileView', array('FileViewID' => $FileViewID)); 
        $FileViewData = $query->row();

        $query = $this->db->get_where('FileFilter', array('FileViewID' => $FileViewID)); 
        $FileFilterData = $query->result_array();

        $ColName = "f.FileID, f.What, f.AssignedTo as AssignedToID, f.CreatedBy as CreatedByID, f.LastModifiedBy as LastModifiedByID, ";
        if(!empty($FileViewData->SpecifyFieldsDisplay)){
            $SpecifyFieldsDisplays = explode(",", $FileViewData->SpecifyFieldsDisplay);
            foreach ($SpecifyFieldsDisplays as $key => $value) {
                if($value == 'AssignedTo'){
                    $ColName .= "CONCAT(ast.FirstName, ' ', ast.LastName) as AssignedTo";
                } else if($value == 'OrganizationID'){
                    $ColName .= "o.CompanyName";
                } else if($value == 'CreatedBy'){
                    $ColName .= "CONCAT(cr.FirstName, ' ', cr.LastName) as CreatedBy";
                } else if($value == 'LastModifiedBy'){
                    $ColName .= "CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedBy";
                } else {
                    $ColName .= "f.".$value;
                }

                if($key != count($SpecifyFieldsDisplays)){
                    $ColName .= ", ";
                }
            }
        }
        
         /* Filter File Data Start */
            $this->db->select($ColName);
            $this->db->from('File f');
            $this->db->join('User ast','ast.UserID = f.AssignedTo', 'left');
            $this->db->join('User cr','cr.UserID = f.CreatedBy', 'left');
            $this->db->join('User lmb','lmb.UserID = f.LastModifiedBy', 'left');
            $this->db->join('Organization o','o.OrganizationID = f.OrganizationID', 'left');
            $this->db->where(array('f.OrganizationID' => $this->LoginOrganizationID, 'f.IsDeleted' => 0));

            foreach ($FileFilterData as $key => $value) {

                $WhereField = "";
                if($value['FilterField'] == 'AssignedTo'){
                    $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                } else if($value['FilterField'] == 'CreatedBy') {
                    $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                } else if($value['FilterField'] == 'LastModifiedBy') {
                    $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                } else if($value['FilterField'] == 'OrganizationID') {
                    $WhereField = "o.CompanyName";
                } else {
                    $WhereField = "f.".$value['FilterField'];
                }

                if($value['FilterCondition'] == 'Equals'){
                    $this->db->where($WhereField.' =',  $value['FilterValue']);  
                } else if($value['FilterCondition'] == 'NotEqualTo'){
                    $this->db->where($WhereField.' !=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'LessThan'){
                    $this->db->where($WhereField.' <', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'GreaterThan'){
                    $this->db->where($WhereField.' >', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'LessOREqualTo'){
                    $this->db->where($WhereField.' <=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'GreaterOREqualTo'){
                    $this->db->where($WhereField.' >=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'Contains'){
                    $this->db->like($WhereField, $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'StartsWith'){ 
                    $this->db->like($WhereField, $value['FilterValue'], 'after');   
                } else if($value['FilterCondition'] == 'DoesNotContain'){
                    $this->db->not_like($WhereField, $value['FilterValue']);   
                } 
            } 
            $query = $this->db->get();  
            //echo $this->db->last_query();exit;
            $data['ColumnsNames'] = $SpecifyFieldsDisplays;

            $RelatedData = MY_Model::getRelatedData();

            $result = $query->result_array();
            $File = array();
            $FileData = array();
            $FinalData = array();
            foreach ($result as $key => $task) {
                foreach ($task as $key => $taskdetails) {  

                    $tmpdata = $task;
                    if(isset($task['RelatedTo']) && isset($task['What'])){
                        //echo $task['RelatedTo'];echo $task['What'];
                        $RelatedDt = $RelatedData[$task['RelatedTo']][$task['What']]; 
                        $tmpdata['RelatedToNo']  = $RelatedDt->No;
                    }
                    
                } 
                $FinalData[] = $tmpdata;
            } 

            $data['ColumnsValues'] = $FinalData;
            return $data; 
    }

    function GetCustomView($FileViewID)
    {
        $query = $this->db->get_where('FileView', array('FileViewID' => $FileViewID)); 
        
        $data['FileViewData'] = $query->row();

        $query = $this->db->get_where('FileFilter', array('FileViewID' => $FileViewID)); 
        
        $data['FileFilterData'] = $query->result();

        return $data;
    }

    /*function GetFileFilter()
    {
        $query = $this->db->get_where('FileFilter', array('UserID' => $this->LoginUserID)); 
        
        return $query->result();
    }*/

    function GetViews(){

        $query = $this->db->query("SELECT * FROM FileView WHERE ((RestrictVisibility = 'VisibleOnlyToMe' AND UserID = '".$this->LoginUserID."') OR (RestrictVisibility = 'VisibleToEveryone')) AND OrganizationID='".$this->LoginOrganizationID."' ORDER BY FileViewName ASC");
        
        return $query->result();
    }

    function FileViewFields()
    {
        $query = $this->db->get_where('FileViewFields'); 
        
        return $query->result();
    }

    function DeleteCustomView($FileViewID)
    {
        return $this->db->delete('FileView', array('FileViewID' => $FileViewID)); 
    }

    function CopyCustomView($FileViewID)
    {
       $query = $this->db->get_where('FileView', array('FileViewID' => $FileViewID)); 
      
        foreach ($query->result() as $row){   
           foreach($row as $key=>$val){ 
                if($key != 'FileViewID'){    
                    $this->db->set($key, $val); 
                }                
           }
        }
        $this->db->insert('FileView');
        $NewFileViewID = $this->db->insert_id();

        $query = $this->db->get_where('FileFilter', array('FileViewID' => $FileViewID)); 
       
        foreach ($query->result() as $row){   
           foreach($row as $key => $val){ 
                if($key == 'FileViewID'){  
                    $this->db->set($key, $NewFileViewID);  
                } else if($key != 'FileFilterID'){    
                    $this->db->set($key, $val); 
                }             
           }
           $this->db->insert('FileFilter');
        }

        return 1; 
    }

    function RenameCustomView($FileViewID){

        $data = array(
            'FileViewName' => $_POST['FileViewName']
        );

        return $this->db->update('FileView', $data, array('FileViewID' => $FileViewID));  
    }

    function EditSharingCustomView($FileViewID){

        $data = array(
            'RestrictVisibility' => $_REQUEST['RestrictVisibility']
        );

        return $this->db->update('FileView', $data, array('FileViewID' => $FileViewID));  
    }

    function EditFiltersCustomView($FileViewID){ 

        extract($_POST);

        $this->db->delete('FileFilter', array('FileViewID' => $FileViewID));  
        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'FileViewID' => $FileViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('FileFilter', $data);  
                }
            } 
 
        }
        return $query; 
    }

    function EditDisplayedColumnsCustomView($FileViewID){

        $data = array(
            'SpecifyFieldsDisplay' => implode(",", $_REQUEST['SpecifyFieldsDisplay'])
        );

        return $this->db->update('FileView', $data, array('FileViewID' => $FileViewID));  
    }
    
}

?>
