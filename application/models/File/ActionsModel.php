<?php
 
class ActionsModel extends MY_Model{

    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }

    function RecentFiles() 
    {

        $mainColumns = array('FileID', 'AssignedToName', 'Subject', 'FileName', 'ObjName', 'Description', 'ContentType', 'CreatedDate', 'CreatedByName', 'RelatedTo', 'What', 'AssignedTo', 'CreatedBy');

        $aColumns = array('f.FileID','CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName', 'f.Subject', 'f.FileName', 'a.AccountName as ObjName', 'f.Description', 'f.ContentType', 'DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate', 'CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName', 'f.LastModifiedDate', 'f.RelatedTo', 'f.What', 'f.AssignedTo', 'f.CreatedBy');

        $wColumns = array('f.FileID','CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName', 'f.Subject', 'f.FileName', 'wo.Subject as ObjName','f.Description', 'f.ContentType', 'DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate', 'CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName', 'f.LastModifiedDate', 'f.RelatedTo', 'f.What', 'f.AssignedTo', 'f.CreatedBy');

        $eColumns = array('f.FileID','CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName', 'f.Subject', 'f.FileName', 'e.EstimateName as ObjName','f.Description', 'f.ContentType', 'DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate', 'CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName', 'f.LastModifiedDate', 'f.RelatedTo', 'f.What', 'f.AssignedTo', 'f.CreatedBy');

        $cColumns = array('f.FileID','CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName', 'f.Subject', 'f.FileName', 'CONCAT(ct.FirstName, " ", ct.LastName) as ObjName','f.Description', 'f.ContentType', 'DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate', 'CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName', 'f.LastModifiedDate', 'f.RelatedTo', 'f.What', 'f.AssignedTo', 'f.CreatedBy');

        $iColumns = array('f.FileID','CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName', 'f.Subject', 'f.FileName', 'i.InvoiceNo as ObjName','f.Description', 'f.ContentType', 'DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate', 'CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName', 'f.LastModifiedDate', 'f.RelatedTo', 'f.What', 'f.AssignedTo', 'f.CreatedBy');

        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "f.FileID";

        /* DB table to use */
        $sTable = "File f
        LEFT JOIN User ast ON ast.UserID = f.AssignedTo
        LEFT JOIN User cb ON cb.UserID = f.CreatedBy";

        /* Limit */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".( $_GET['iDisplayStart'] ).", ".
                        ( $_GET['iDisplayLength'] );
        }

        /* Ordering */
        $sOrder = "ORDER BY LastModifiedDate DESC";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                    {
                            $sOrder .= $mainColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                                    ".( $_GET['sSortDir_'.$i] ) .", ";
                    }
            }
           
            $sOrder = substr_replace( $sOrder, "", -2 );

            if ( $sOrder == "ORDER BY" )
            {
                    $sOrder = "";
            } 
        }

        /* Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited */
        $sWhere = "";
        if ( $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
                for ( $i=0 ; $i<count($mainColumns) ; $i++ )
                {
                        $sWhere .= $mainColumns[$i]." LIKE '%".( $_GET['sSearch'] )."%' OR ";
                }
                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ')';
        }
        
        /* Where Condition */

        $Where = "Where f.OrganizationID='".$this->LoginOrganizationID."' and f.IsDeleted=0 AND";

        $sqlUnionQuery = "(SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
        FROM $sTable
        JOIN Account a ON f.What = a.AccountID 
        $Where f.RelatedTo = 'Account')
        UNION ALL
        (SELECT ".str_replace(" , ", " ", implode(", ", $wColumns))."
        FROM $sTable
        JOIN WorkOrder wo ON f.What = wo.WorkOrderID
        $Where f.RelatedTo = 'WorkOrder')
        UNION ALL
        (SELECT ".str_replace(" , ", " ", implode(", ", $eColumns))."
        FROM $sTable
        JOIN Estimate e ON f.What = e.EstimateID
        $Where f.RelatedTo = 'Estimate')
        UNION ALL
        (SELECT ".str_replace(" , ", " ", implode(", ", $cColumns))."
        FROM $sTable
        JOIN Contact ct ON f.What = ct.ContactID
        $Where f.RelatedTo = 'Contact')
        UNION ALL
        (SELECT ".str_replace(" , ", " ", implode(", ", $iColumns))."
        FROM $sTable
        JOIN Invoice i ON f.What = i.InvoiceID
        $Where f.RelatedTo = 'Invoice')";

        /* SQL queries Get data to display */
        $sQuery = "SELECT ".str_replace(" , ", " ", implode(", ", $mainColumns))." FROM
        (".$sqlUnionQuery.") results
        $sWhere
        $sOrder
        $sLimit";  
        
        $rResult = $this->db->query($sQuery); 
        $rResult = $rResult->result_array(); 
      
        /* Data set length after filtering */
        $sQuery = "SELECT COUNT(*) as cnt FROM (".$sqlUnionQuery.") results $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iFilteredTotal = $result->cnt;

        /* Total data set length */
        $sQuery = "SELECT COUNT(*) as cnt FROM (".$sqlUnionQuery.") results $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iTotal = $result->cnt;

        /* Output */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );

        foreach ( $rResult as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($mainColumns) ; $i++ )
            {
                if($i == 0)
                {
                    $row[] = '';
                } 
                else if($i == 1)
                {
                    $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['AssignedTo'].'">'.$aRow[$mainColumns[$i]].'</a>';
                }
                else if($i == 8)
                {
                    $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['CreatedBy'].'">'.$aRow[$mainColumns[$i]].'</a>';
                }
                else if($i == 2)
                {
                    $row[] = '<a href="'.SITE_URL.'File/Actions/FileDetails/'.$aRow[$mainColumns[0]].'">'.$aRow[$mainColumns[$i]].'</a>';
                }
                else if($mainColumns[$i] == 'ObjName')
                {
                    $row[] = '<a href="'.SITE_URL.$aRow['RelatedTo'].'/Actions/'.$aRow['RelatedTo'].'Details/'.$aRow['What'].'">'.$aRow[$mainColumns[$i]].'</a>';
                }
                else if ( $mainColumns[$i] != ' ' )
                {
                    $row[] = $aRow[$mainColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode( $output );
    } 

    function CreateFile() {

        extract($_POST); 

        $FILE_PATH = str_replace('{org_id}', $this->LoginOrganizationID, FILE_PATH);

        $UserID = $this->LoginUserID;
        $FileName = explode(".", $FileName)[0];
        $config = array(
             'upload_path' => $FILE_PATH,
             'file_name' => $FileName.'_'.date('YmdHis'),
             'overwrite' => FALSE,
             'allowed_types' => "jpg|png|jpeg|doc|pptx|xlsx|docx|zip",
        );
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('Upload')){
            $fileData = $this->upload->data();
            chmod($FILE_PATH.$fileData['file_name'], 0777);
       
            $data = array(
                'OrganizationID' =>  $this->LoginOrganizationID,
                'Subject' => $Subject,
                'RelatedTo' => $RelatedTo,
                'What' => $What,
                'AssignedTo' => $AssignedTo,
                'ContentType' => $fileData['file_type'],
                'Description' => $Description,
                'FileName' => $fileData['file_name'],
                'FileSize' => round($fileData['file_size']/1024, 2)
            ); 
            
            $result = $this->db->insert('File', $data); 
              
           //echo $this->db->last_query();exit;
            $FileID = $this->db->insert_id();

            MY_Model::insertCreatedModifiedByAndDt($UserID,'File', array('FileID' => $FileID));
        
            return $FileID;
        }
        return 0;
    } 

    function EditFile($FileID) {

        extract($_POST);          

        $UserID = $this->LoginUserID;

        $FILE_PATH = str_replace('{org_id}', $this->LoginOrganizationID, FILE_PATH);
    
        if(isset($_FILES['Upload']['error']) && $_FILES['Upload']['error'] == 0){ 
            $FileName = explode(".", $FileName)[0];
            $config = array(
                 'upload_path' => $FILE_PATH,
                 'file_name' => $FileName.'_'.date('YmdHis'),
                 'overwrite' => FALSE,
                 'allowed_types' => "jpg|png|jpeg|doc|pptx|xlsx|docx|zip|txt",
            );
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if($this->upload->do_upload('Upload')){
                $fileData = $this->upload->data();
                chmod($FILE_PATH.$fileData['file_name'], 0777);

                $query = $this->db->get_where('File', array('FileID' => $FileID)); 
                $FileData = $query->row();
                unlink($FILE_PATH.$FileData->FileName);
           
                $data = array(
                    'OrganizationID' =>  $this->LoginOrganizationID,
                    'Subject' => $Subject,
                    'RelatedTo' => $RelatedTo,
                    'What' => $What,
                    'AssignedTo' => $AssignedTo,
                    'ContentType' => $fileData['file_type'],
                    'Description' => $Description,
                    'FileName' => $fileData['file_name'],
                    'FileSize' => round($fileData['file_size']/1024, 2)
                ); 
                
                $result = $this->db->update('File', $data, array('FileID' => $FileID));    
       
                MY_Model::updateCreatedModifiedByAndDt($UserID,'File', array('FileID' => $FileID));

                return $result;
            }
        } else {
            $data = array(
                'OrganizationID' =>  $this->LoginOrganizationID,
                'Subject' => $Subject,
                'RelatedTo' => $RelatedTo,
                'What' => $What,
                'AssignedTo' => $AssignedTo,
                'Description' => $Description
            ); 
            
            $result = $this->db->update('File', $data, array('FileID' => $FileID));    
   
            MY_Model::updateCreatedModifiedByAndDt($UserID,'File', array('FileID' => $FileID));
        
            return $result;
        }
    } 

    function getFileData($FileID, $RelatedObjName = 'AllObject')
    {

        $result = $this->db->update('File', array('LastModifiedDate' => date("Y-m-d H:i:s")), array('OrganizationID' => $this->LoginOrganizationID,'FileID' => $FileID));  

        $query = $this->db->select('f.*, f.Subject as Title, CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName, DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, DATE_FORMAT(f.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName, f.CreatedBy, f.LastModifiedBy, f.AssignedTo')
        ->from('File f')
        ->join('User cb','cb.UserID = f.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = f.LastModifiedBy', 'left')
        ->join('User ast','ast.UserID = f.AssignedTo', 'left')
        ->where(array('f.OrganizationID' => $this->LoginOrganizationID, 'f.FileID' => $FileID)) 
        ->get();

        $data['FileData'] = $query->row();
        
        if(isset($data['FileData'])){
            $RelatedObjData = MY_Model::getRelatedData();
            $RelatedObjData = $RelatedObjData[$data['FileData']->RelatedTo][$data['FileData']->What];
            $data['FileData']->RelatedObjNo = $RelatedObjData->No;
        }

        return $data;
    }

    function CreateNote($FileID)
    {
        extract($_POST); 

        $UserID = $this->LoginUserID;
  
        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'Owner' => $UserID,
            'What' => $FileID,
            'Subject' => $Subject,
            'Body' => $Body,
            'RelatedTo' => 'File'
        );
        $result = $this->db->insert('Note', $data);    
        $NoteID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Note', array('NoteID' => $NoteID));
    
        return $result;
    }

    /*function GetRelatedToObjectData()
    {
        $OptionStr = "";
        if(isset($_POST['RelatedTo']) && $_POST['RelatedTo'] == 'Account'){

            $query = $this->db->get_where('Account', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
            $Accounts = $query->result();
            foreach ($Accounts as $key => $Account) {
                $OptionStr .= "<option value=".$Account->AccountID.">".$Account->AccountName."</option>";
            }

        } else if(isset($_POST['RelatedTo']) && $_POST['RelatedTo'] == 'WorkOrder'){

            $query = $this->db->get_where('WorkOrder', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
            $WorkOrders = $query->result();
            foreach ($WorkOrders as $key => $WorkOrder) {
                $OptionStr .= "<option value=".$WorkOrder->WorkOrderID.">".$WorkOrder->Subject."</option>";
            }
            
        } else if(isset($_POST['RelatedTo']) && $_POST['RelatedTo'] == 'Estimate'){

            $query = $this->db->get_where('Estimate', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
            $Estimates = $query->result();
            foreach ($Estimates as $key => $Estimate) {
                $OptionStr .= "<option value=".$Estimate->EstimateID.">".$Estimate->EstimateName."</option>";
            }
            
        } else if(isset($_POST['RelatedTo']) && $_POST['RelatedTo'] == 'Invoice'){

            $query = $this->db->get_where('Invoice', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
            $Invoices = $query->result();
            foreach ($Invoices as $key => $Invoice) {
                $OptionStr .= "<option value=".$Invoice->InvoiceID.">".$Invoice->InvoiceNo."</option>";
            }
            
        } else if(isset($_POST['RelatedTo']) && $_POST['RelatedTo'] == 'Contact'){

            $query = $this->db->get_where('Contact', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
            $Contacts = $query->result();
            foreach ($Contacts as $key => $Contact) {
                $OptionStr .= "<option value=".$Contact->ContactID.">".$Contact->FirstName." ".$Contact->LastName."</option>";
            }
        }

        return $OptionStr;
    }*/

    function SaveRecurrenceSetting()
    { 
        extract($_POST);

        if(isset($WORecurrenceID) && !empty($WORecurrenceID))
        {
            $data = array(
                'StartOn' => date("Y-m-d",strtotime($StartOn)),
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'RepeatOn' => ($RepeatEvery=='Weekly')?implode(',', $RepeatOn):NULL,
                'Ends' => $Ends,
                'EndsOnDate' => ($Ends=='On')?date("Y-m-d",strtotime($EndsOnDate)):NULL,
                'EndsAfterOccurrences' => ($Ends=='After')?$EndsAfterOccurrences:NULL,
                'StartTime' => date("H:i:s",strtotime($StartTime)),
                'EndTime' => date("H:i:s",strtotime($EndTime)),
                'LastModifiedDate' => date("Y-m-d H:i:s")
            ); 
            $result = $this->db->update('WORecurrence', $data, array('WORecurrenceID'=>$WORecurrenceID));    

        } else {
            $data = array(
                'StartOn' => date("Y-m-d",strtotime($StartOn)),
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'RepeatOn' => ($RepeatEvery=='Weekly')?implode(',', $RepeatOn):NULL,
                'Ends' => $Ends,
                'EndsOnDate' => ($Ends=='On')?date("Y-m-d",strtotime($EndsOnDate)):NULL,
                'EndsAfterOccurrences' => ($Ends=='After')?$EndsAfterOccurrences:NULL,
                'StartTime' => date("H:i:s",strtotime($StartTime)),
                'EndTime' => date("H:i:s",strtotime($EndTime)),
                'CreatedDate' => date("Y-m-d H:i:s"),
                'LastModifiedDate' => date("Y-m-d H:i:s")
            ); 
            $result = $this->db->insert('WORecurrence', $data);    
            $WORecurrenceID = $this->db->insert_id();
        }
        return $WORecurrenceID;
    }

    function removeRecurrenceSetting()
    {
        $WORecurrenceID = $_REQUEST['WORecurrenceID'];
        $WorkOrderID = $_REQUEST['WorkOrderID'];
        $this->db->update('WorkOrder',  array('IsRecurring' => 0), array('WorkOrderID' => $WorkOrderID));  
        return $this->db->delete('WORecurrence', array('WORecurrenceID' => $WORecurrenceID));
    }

    function GetContacts()
    { 
        $query = $this->db->get_where('Contact', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetFileTypes()
    { 
        $query = $this->db->get_where('FileType', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetUsers()
    { 
        $query = $this->db->get_where('User', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetFileStatus()
    { 
        $query = $this->db->get_where('FileStatus', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetFilePriority()
    { 
        $query = $this->db->get_where('FilePriority', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetViews(){

        $query = $this->db->query("SELECT * FROM FileView WHERE ((RestrictVisibility = 'VisibleOnlyToMe' AND UserID = '".$this->LoginUserID."') OR (RestrictVisibility = 'VisibleToEveryone')) AND OrganizationID='".$this->LoginOrganizationID."' ORDER BY FileViewName ASC");
        
        return $query->result();
    }

    function DeleteFile($FileID)
    {
        return $this->db->update('File', array('IsDeleted' => 1), array('FileID' => $FileID));  
    }
}

?>
