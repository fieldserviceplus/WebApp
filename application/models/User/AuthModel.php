<?php

class AuthModel extends MY_Model {
 
    function __construct() {
        parent::__construct();
    }

    function checkUserAuthentication($UserID, $token) {
        $data = array(
            'UserID' => $UserID,
            'Token' => $token
        );

        $query = $this->db->get_where('User', $data);
        return $query->num_rows();
    }
 
    function SignUp() {

        extract($_POST);

        $query = $this->db->get_where('OrgPlan', array('OrgPlanID' => $Plan));
        $PlanData = $query->row();

        $data = array(
            'Email' => $Email
        );
        $query = $this->db->get_where('User', $data);
 
        if ($query->num_rows() > 0) {
            return 2;
        } else {

            /* Start User Insert */
            $data = array(
                'Profile' => 1,
                'FirstName' => $FirstName,
                'LastName' => $LastName,
                'PhoneNo' => $Phone,
                'Email' => $Email,
                'Password' => md5($Password)
            );
            $result = $this->db->insert('User', $data);

            $UserID = $this->db->insert_id();

            MY_Model::insertCreatedModifiedByAndDt($UserID,'User', array('UserID' => $UserID));
            /* End User Insert */

            /* Start Company Insert */
            if($ContractType == "Annual"){
                $ContractRenewalDate = date('Y-m-d', strtotime('+1 year'));
            } else if($ContractType == "Monthly"){
                $ContractRenewalDate = date('Y-m-d', strtotime('+1 month'));
            }
            $data = array(
                'CompanyName' => $CompanyName,
                'Owner' => $UserID,
                'BillingAddress' => $BillingAddress,
                'BillingCity' => $BillingCity,
                'BillingState' => $BillingState,
                'BillingCountry' => $BillingCountry,
                'BillingLatitude' => !empty($BillingLatitude)?$BillingLatitude:NULL,
                'BillingLongitude' => !empty($BillingLongitude)?$BillingLongitude:NULL,
                'BillingPostalCode' => $BillingPostalCode,
                'ContractType' => $ContractType,
                'ContractRenewalDate' => $ContractRenewalDate,
                'CompanyEdition' => !empty($CompanyEdition)?$CompanyEdition:NULL,
                'Website' => $Website,
                'Plan' => $Plan,
                'Phone' => $Phone,
            );
            $result = $this->db->insert('Organization', $data);

            $OrganizationID = $this->db->insert_id();

            MY_Model::insertCreatedModifiedByAndDt($UserID,'Organization', array('OrganizationID' => $OrganizationID));
            /* End Company Insert */

            /* Start User update company */
            $result = $this->db->update('User', array('OrganizationID' => $OrganizationID), array('UserID' => $UserID));
            /* End User update company */

            /* Start Licenses Insert */
            $data = array(
                'Name' => $CompanyName,
                'OrganizationID' => $OrganizationID,
                'UserLicenseStatus' => 1,
                'TotalLicenses' => $PlanData->MaxLicenses,
                'UsedLicenses' => 1,
                'RenewalDate' => $ContractRenewalDate
            );
            $result = $this->db->insert('UserLicense', $data);

            $UserLicenseID = $this->db->insert_id();

            MY_Model::insertCreatedModifiedByAndDt($UserID,'UserLicense', array('UserLicenseID' => $UserLicenseID));

            /* End Licenses Insert */

            /* Start Pick List Insert */
            $this->SetDefaultPickListData($OrganizationID, $UserID);
            /* End Pick List Insert */

            mkdir('org_data/'.$OrganizationID);

            chmod('org_data/'.$OrganizationID, 0777);

            mkdir('org_data/'.$OrganizationID.'/doc_templates');
            mkdir('org_data/'.$OrganizationID.'/files');
            mkdir('org_data/'.$OrganizationID.'/temp');

            chmod('org_data/'.$OrganizationID.'/doc_templates', 0777);
            chmod('org_data/'.$OrganizationID.'/files', 0777);
            chmod('org_data/'.$OrganizationID.'/temp', 0777);

            return $result;
        }
    }
 
    function Login($Email, $Password){

        $where = array(
            'Email' => $Email,
            'Password' => md5($Password)
        );
       // $query = $this->db->get_where('User', $where);
        $query = $this->db->select('u.*, p.Name as ProfileType')
        ->from('User u')
        ->join('Profile p','p.ProfileID = u.Profile', 'left')
        ->where($where) 
        ->get();
    
        if ($query->num_rows() > 0) {

            $userdata = $query->row_array();

            $this->session->set_userdata($userdata);

            /* Start - For Login History */ 

            $result = $this->db->update('User', array('LastLoginDT' => date("Y-m-d H:i:s")), array('UserID' => $userdata['UserID']));

            $this->load->library('user_agent');
 
            $location = json_decode(@file_get_contents('http://ip-api.com/json/'.$this->input->ip_address()));
           
            if (isset($location->status) && $location->status != "fail") { 
                    
                    //$location = json_decode(file_get_contents('http://ip-api.com/json/'.$this->input->ip_address()));

                    $data = array(
                        'OrganizationID' => $userdata['OrganizationID'],
                        'UserID' => $userdata['UserID'],
                        'Browser' => $this->agent->browser(),
                        'Platform' => $this->agent->platform(),
                        'SourceIP' => $this->input->ip_address(),
                        'LoginURL' => SITE_URL.'User/Auth/Login',
                        'CountryCode' => ($location->status=='success')?$location->countryCode:"",
                        'City' => ($location->status=='success')?$location->city:"",
                        'PostalCode' => ($location->status=='success')?$location->zip:"",
                        'Latitude' => ($location->status=='success')?$location->lat:NULL,
                        'Longitude' => ($location->status=='success')?$location->lon:NULL,
                        'SourceIP' => $this->input->ip_address(),
                        'LoginTime' => date("H:i:s"),
                        'LoginDateTime' => date("Y-m-d H:i:s"),
                    );
                    $result = $this->db->insert('Login', $data);
                   
                    $LoginID = $this->db->insert_id();
          
                    MY_Model::insertCreatedModifiedByAndDt($userdata['UserID'],'Login', array('LoginID' => $LoginID));
                    /* End - For Login History */ 

            }
 
            return 1; 

        } else {
            return 0; 
        }
    }  

    function ForgotPassword($Email){
 
        $where = array(
            'Email' => $Email
        );
        $query = $this->db->get_where('User', $where);
    
        if ($query->num_rows() > 0) {

            $userdata = $query->row_array();

            $Password = MY_Controller::genNumRandCode(6);

            $message = "Hello, <br /><br /> Your new password is $Password <br /><br /> Thank You";  
 
            MY_Controller::sendMail($Email, 'Forgot Password', $message);

            $data = array('Password' => md5($Password));

            $result = $this->db->update('User', $data, array('UserID' => $userdata['UserID']));
 
            return 1; 

        } else {
            return 0; 
        }
    }  

    function GetPlans() {

        $query = $this->db->get_where('OrgPlan', array('IsDeleted' => 0));
        return $query->result();
    }

    function GetPlanDetails($Plan) {

        $query = $this->db->get_where('OrgPlan', array('OrgPlanID' => $Plan));
        return json_encode($query->row());
    }

    function SetDefaultPickListData($OrganizationID, $UserID){

        /* Insert - Account Type*/
        $AccountType = array('Prospect', 'Customer', 'Partner', 'Competitor');
        foreach ($AccountType as $key => $value) {

            $AccountTypeData[] = array('AccountType' => $value,
                        'OrganizationID' => $OrganizationID,
                        'CreatedBy' => $UserID,
                        'LastModifiedBy' => $UserID,
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );
        }
        $result = $this->db->insert_batch('AccountType', $AccountTypeData);

        /* Insert - Lead Source*/
        $LeadSource = array('Email', 'Flier', 'Inbound Call', 'Mail / Postcard', 'Online Advertising', 'Outbound Call', 'Referral', 'Technician', 'Website');
        foreach ($LeadSource as $key => $value) {

            $LeadSourceData[] = array('LeadSource' => $value,
                        'OrganizationID' => $OrganizationID,
                        'CreatedBy' => $UserID,
                        'LastModifiedBy' => $UserID,
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );
        }
        $result = $this->db->insert_batch('LeadSource', $LeadSourceData);

        /* Insert - Salutation*/
        $Salutation = array('Mr.', 'Mrs.', 'Ms.');
        foreach ($Salutation as $key => $value) {

            $SalutationData[] = array('Salutation' => $value,
                        'OrganizationID' => $OrganizationID,
                        'CreatedBy' => $UserID,
                        'LastModifiedBy' => $UserID,
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );
        }
        $result = $this->db->insert_batch('Salutation', $SalutationData);

        /* Insert - Work Order Type*/
        $WorkOrderType = array('Field Service', 'Installation', 'Preventative Maintenance', 'Depot Repair', 'Recurring Service');
        foreach ($WorkOrderType as $key => $value) {

            $WorkOrderTypeData[] = array('WorkOrderType' => $value,
                        'OrganizationID' => $OrganizationID,
                        'CreatedBy' => $UserID,
                        'LastModifiedBy' => $UserID,
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );
        }
        $result = $this->db->insert_batch('WorkOrderType', $WorkOrderTypeData);

        /* Insert - Work Order Status*/
        $WOStatus = array('New', 'Assigned', 'In Progress', 'On Hold', 'Awaiting Customer', 'Completed', 'Cancelled', 'Cannot Complete');
        $WOStatusColor = array('#0000FF', '#0080FF', '#008080', '#808000', '#FFFF00', '#800000', '#FF0000', '#000000');
        $WOStatusClosed = array(0, 0, 0, 0, 0, 1, 1, 1);
        foreach ($WOStatus as $key => $value) {

            $WOStatusData[] = array('Status' => $value,
                        'ColorCode' => $WOStatusColor[$key],
                        'IsClosed' => $WOStatusClosed[$key],
                        'IsCompleted' => (($value == "Completed") ? 1 : 0),
                        'OrganizationID' => $OrganizationID,
                        'CreatedBy' => $UserID,
                        'LastModifiedBy' => $UserID,
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );
        }
        $result = $this->db->insert_batch('WOStatus', $WOStatusData);

        /* Insert - Work Order Priority*/
        $WorkOrderType = array('Low', 'Normal', 'High');
        foreach ($WorkOrderType as $key => $value) {

            $WorkOrderPriorityData[] = array('Priority' => $value,
                        'OrganizationID' => $OrganizationID,
                        'CreatedBy' => $UserID,
                        'LastModifiedBy' => $UserID,
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );
        }
        $result = $this->db->insert_batch('WOPriority', $WorkOrderPriorityData);

        /* Insert - Work Order Category*/
        $WorkOrderCategory = array('Billable', 'Contract', 'Courtesy', 'Loan', 'Warranty');
        foreach ($WorkOrderCategory as $key => $value) {

            $WorkOrderCategoryData[] = array('CategoryName' => $value,
                        'OrganizationID' => $OrganizationID,
                        'CreatedBy' => $UserID,
                        'LastModifiedBy' => $UserID,
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );
        }
        $result = $this->db->insert_batch('WOCategory', $WorkOrderCategoryData);


        /* Insert - Estimate Status*/
        $EstimateStatus = array('New', 'Assigned', 'Sent to Customer', 'Won', 'Lost', 'No Decision');
        $EstimateStatusColor = array('#0000FF', '#008080', '#808000', '#FFFF00', '#800000', '#000000');
        foreach ($EstimateStatus as $key => $value) {

            $EstimateStatusData[] = array('Status' => $value,
                        'ColorCode' => $EstimateStatusColor[$key],
                        'OrganizationID' => $OrganizationID,
                        'CreatedBy' => $UserID,
                        'LastModifiedBy' => $UserID,
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );
        }
        $result = $this->db->insert_batch('EstimateStatus', $EstimateStatusData);

        /* Insert - Invoice Status */
        $InvoiceStatus = array('Ready to Invoice', 'Invoiced', 'Paid', 'Cancelled');
        foreach ($InvoiceStatus as $key => $value) {

            $InvoiceStatusData[] = array('InvoiceStatus' => $value,
                        'IsPaid' => (($value == "Paid") ? 1 : 0),
                        'OrganizationID' => $OrganizationID,
                        'CreatedBy' => $UserID,
                        'LastModifiedBy' => $UserID,
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );
        }
        $result = $this->db->insert_batch('InvoiceStatus', $InvoiceStatusData);

        /* Insert - Invoice Payment Terms */
        $InvoicePaymentTerm = array('Upon Receipt', 'Net 30', 'Net 45', 'Net 60');
        foreach ($InvoicePaymentTerm as $key => $value) {

            $InvoicePaymentTermData[] = array('PaymentTerms' => $value,
                        'IsDefault' => ($value == "Upon Receipt") ? 1 : 0,
                        'OrganizationID' => $OrganizationID,
                        'CreatedBy' => $UserID,
                        'LastModifiedBy' => $UserID,
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );
        }
        $result = $this->db->insert_batch('InvoicePaymentTerm', $InvoicePaymentTermData);

        /* Insert - Task Type */
        $TaskType = array('Call', 'Meeting', 'Email', 'Task', 'Other');
        foreach ($TaskType as $key => $value) {

            $TaskTypeData[] = array('TaskType' => $value,
                        'IsDefault' => ($value == "Call") ? 1 : 0,
                        'OrganizationID' => $OrganizationID,
                        'CreatedBy' => $UserID,
                        'LastModifiedBy' => $UserID,
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );
        }
        $result = $this->db->insert_batch('TaskType', $TaskTypeData);

        /* Insert - Task Status */
        $TaskStatus = array('Not Started', 'In Progress', 'Completed', 'Waiting on Someone Else', 'Deferred');
        foreach ($TaskStatus as $key => $value) {

            $TaskStatusData[] = array('TaskStatus' => $value,
                        'IsDefault' => ($value == "Not Started") ? 1 : 0,
                        'IsClosed' => ($value == "Completed") ? 1 : 0,
                        'OrganizationID' => $OrganizationID,
                        'CreatedBy' => $UserID,
                        'LastModifiedBy' => $UserID,
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );
        }
        $result = $this->db->insert_batch('TaskStatus', $TaskStatusData);

        /* Insert - Task Priority */
        $TaskPriority = array('Low', 'Normal', 'High');
        foreach ($TaskPriority as $key => $value) {

            $TaskPriorityData[] = array('Priority' => $value,
                        'IsDefault' => ($value == "Normal") ? 1 : 0,
                        'OrganizationID' => $OrganizationID,
                        'CreatedBy' => $UserID,
                        'LastModifiedBy' => $UserID,
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );
        }
        $result = $this->db->insert_batch('TaskPriority', $TaskPriorityData);

        /* Insert - Event Type */
        $EventType = array('Call', 'Meeting', 'Other', 'Estimate', 'Work Order');
        foreach ($EventType as $key => $value) {

            $EventTypeData[] = array('EventTypeName' => $value,
                        'OrganizationID' => $OrganizationID,
                        'CreatedBy' => $UserID,
                        'LastModifiedBy' => $UserID,
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );
        }
        $result = $this->db->insert_batch('EventType', $EventTypeData);
        
        /* Insert - Event Priority */
        $EventPriority = array('Low', 'Normal', 'High');
        foreach ($EventPriority as $key => $value) {

            $EventPriorityData[] = array('Priority' => $value,
                        'IsDefault' => ($value == "Normal") ? 1 : 0,
                        'OrganizationID' => $OrganizationID,
                        'CreatedBy' => $UserID,
                        'LastModifiedBy' => $UserID,
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );
        }
        $result = $this->db->insert_batch('EventPriority', $EventPriorityData);


        /* Insert - Unit Of Measurement */
        $UnitOfMeasurement = array('F°', 'Gallon', 'Liters', 'pH', 'Pounds (lbs.)', 'PPM', 'Quarts', 'Square Feet (sq. ft.)', 'Unit');
        foreach ($UnitOfMeasurement as $key => $value) {

            $UnitOfMeasurementData[] = array('UnitOfMeasurement' => $value,
                        'OrganizationID' => $OrganizationID,
                        'CreatedBy' => $UserID,
                        'LastModifiedBy' => $UserID,
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );
        }
        $result = $this->db->insert_batch('UnitOfMeasurement', $UnitOfMeasurementData);

        /* Insert - Product Family */
        $ProductFamily = array('Accessories', 'Hardware', 'Appliance', 'Labor', 'Services');
        foreach ($ProductFamily as $key => $value) {

            $ProductFamilyData[] = array('ProductFamily' => $value,
                        'OrganizationID' => $OrganizationID,
                        'CreatedBy' => $UserID,
                        'LastModifiedBy' => $UserID,
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );
        }
        $result = $this->db->insert_batch('ProductFamily', $ProductFamilyData);

        /* Insert - Product Qty Unit Of Measure */
        $ProductQtyUnitOfMeasure = array('Per Unit', 'Per Quantity');
        foreach ($ProductQtyUnitOfMeasure as $key => $value) {

            $ProductQtyUnitOfMeasureData[] = array('UnitOfMeasure' => $value,
                        'OrganizationID' => $OrganizationID,
                        'CreatedBy' => $UserID,
                        'LastModifiedBy' => $UserID,
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );
        }
        $result = $this->db->insert_batch('ProductQtyUnitOfMeasure', $ProductQtyUnitOfMeasureData);
    }
    
}?>