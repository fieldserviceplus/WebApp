<?php
 
class HomeModel extends MY_Model{

    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }

    function GetTaskData()
    { 
        extract($_POST);

        $this->db->select("e.EventID as ID, e.Subject, DATE_FORMAT(e.EventEndDate, '".RES_DATE."') as Date, e.EventEndDate as OrgDate, 'Event' as Type");
        $this->db->from("Event e");
        $this->db->join('EventStatus es','es.EventStatusID = e.EventStatus', 'left');
        $this->db->where(array('es.IsClosed' => 0, 'e.IsDeleted' => 0, 'e.OrganizationID' => $this->LoginOrganizationID, 'e.AssignedTo' => $this->LoginUserID));
        if($Filter == 'Today')
            $this->db->where("(DATE(e.EventEndDate) = '".date("Y-m-d")."')");
        else if($Filter == 'TodayOverdue')
            $this->db->where("(DATE(e.EventEndDate) <= '".date("Y-m-d")."')");
        else if($Filter == 'Overdue')
            $this->db->where("(DATE(e.EventEndDate) < '".date("Y-m-d")."')");
        $this->db->get(); 
        $query1 = $this->db->last_query();
         
        $this->db->select("t.TaskID as ID, t.Subject, DATE_FORMAT(t.DueDate, '".RES_DATE."') as Date, t.DueDate as OrgDate, 'Task' as Type");
        $this->db->from("Task t");
        $this->db->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left');
        $this->db->where(array('ts.IsClosed' => 0, 't.IsDeleted' => 0, 't.OrganizationID' => $this->LoginOrganizationID, 't.AssignedTo' => $this->LoginUserID));
        if($Filter == 'Today')
            $this->db->where("(DATE(t.DueDate) = '".date("Y-m-d")."')");
        else if($Filter == 'TodayOverdue')
            $this->db->where("(DATE(t.DueDate) <= '".date("Y-m-d")."')");
        else if($Filter == 'Overdue')
            $this->db->where("(DATE(t.DueDate) < '".date("Y-m-d")."')");
        $this->db->get(); 
        $query2 =  $this->db->last_query();
         
        $query = $this->db->query("SELECT ID, Subject, Date, Type FROM (". $query1. " UNION ". $query2. ") as TaskEvent ORDER BY DATE(OrgDate) desc LIMIT 20");

        $Tasks = $query->result();

        /* Fetch data from Related Object */
        //$RelatedDataArr = MY_Model::getRelatedData();

        $FinalData = "";
        foreach ($Tasks as $key => $Task) {

            //$ObjRelatedData = $RelatedDataArr[$OpenActivity['RelatedTo']][$OpenActivity['What']];
            $FinalData .= '<li class="mb-10"><i class="fa fa-angle-double-right text-info mr-5"></i><a href="'.SITE_URL.$Task->Type.'/Actions/'.$Task->Type.'Details/'.$Task->ID.'" style="text-decoration:underline"> '.$Task->Date.' '.$Task->Subject.'</a></li>';
            
        }
        return $FinalData;
    }
}

?>
