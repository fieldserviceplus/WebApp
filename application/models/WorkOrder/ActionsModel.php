<?php

class ActionsModel extends MY_Model{
 
    public $LoginUserID, $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }
 
    function RecentWorkOrders() 
    {
        $aColumns = array('w.WorkOrderID','w.WorkOrderNo', 'w.Subject', 'ac.AccountName', 'wot.WorkOrderType', 'wos.Status','wop.Priority', 'CONCAT(at.FirstName, " ", at.LastName)', 'DATE_FORMAT(w.StartDate, "'.RES_DATETIME.'")', 'DATE_FORMAT(w.EndDate, "'.RES_DATETIME.'")', 'AccountID', 'w.AssignedTo'); 
        $bColumns = array('WorkOrderID','WorkOrderNo','Subject', 'AccountName', 'WorkOrderType', 'Status', 'Priority', 'CONCAT(at.FirstName, " ", at.LastName)', 'DATE_FORMAT(w.StartDate, "'.RES_DATETIME.'")', 'DATE_FORMAT(w.EndDate, "'.RES_DATETIME.'")', 'AccountID', 'AssignedTo');
        
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "w.WorkOrderID";

        /* DB table to use */
        $sTable = "WorkOrder w
        LEFT JOIN WOPriority wop ON wop.WOPriorityID = w.WOPriority 
        LEFT JOIN Account ac ON ac.AccountID = w.Account 
        LEFT JOIN WorkOrderType wot ON wot.WorkOrderTypeID = w.WorkOrderType 
        LEFT JOIN User at ON at.UserID = w.AssignedTo
        LEFT JOIN WOStatus wos ON wos.WOStatusID = w.WOStatus";

        /* Limit */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".( $_GET['iDisplayStart'] ).", ".
                        ( $_GET['iDisplayLength'] );
        }

        /* Ordering */
        $sOrder = "ORDER BY w.LastModifiedDate DESC";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                    {
                            $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                                    ".( $_GET['sSortDir_'.$i] ) .", ";
                    }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                    $sOrder = "";
            }
        }

        /* Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited */
        $sWhere = "";
        if ( $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                        $sWhere .= $aColumns[$i]." LIKE '%".( $_GET['sSearch'] )."%' OR ";
                }
                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ')';
        }
        
        /* Where Condition */
        $UserID = $this->session->userdata('UserID');
       
        $where_str = "w.OrganizationID = '".$this->LoginOrganizationID."' AND w.IsDeleted=0";
        if ( $sWhere == "" )
        {
                $sWhere = "WHERE $where_str";
        }
        else
        {
                $sWhere .= "AND $where_str";
        }
        
        /* SQL queries Get data to display */
        $sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit"; 
  
        $rResult = $this->db->query($sQuery); 
        $rResult = $rResult->result_array(); 
      
        /* Data set length after filtering */
        $sQuery = "SELECT FOUND_ROWS() as cnt";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iFilteredTotal = $result->cnt;

        /* Total data set length */
        $sQuery = "SELECT COUNT(".$sIndexColumn.") as cnt
                FROM   $sTable  $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iTotal = $result->cnt;

        /* Output */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );

        foreach ( $rResult as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($bColumns) ; $i++ )
            {
                if($i == 0)
                {
                    //$row[] = '';
                } 
                else if($bColumns[$i] == 'AccountName')
                {
                     $row[] = '<a href="'.SITE_URL.'Account/Actions/AccountDetails/'.$aRow['AccountID'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($bColumns[$i] == 'Subject' || $bColumns[$i] == 'WorkOrderNo')
                {
                    $row[] = '<a href="'.SITE_URL.'WorkOrder/Actions/WorkOrderDetails/'.$aRow[$bColumns[0]].'">'.$aRow[$bColumns[$i]].'</a>';
                }
                else if($i == 7)
                {
                     $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['AssignedTo'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if ( $bColumns[$i] != ' ' )
                {
                    $row[] = $aRow[$bColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode( $output );
    } 

    function CreateWorkOrder() {
               
        extract($_POST); 

        $UserID = $this->LoginUserID;

        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'AssignedTo' => $AssignedTo,
            'ParentWorkOrder' => !empty($ParentWorkOrder)?$ParentWorkOrder:NULL,
            'Account' => $Account,
            'Subject' => $Subject,
            'Description' => $Description,
            'Address' => $Address,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => $Latitude,
            'Longitude' => $Longitude,
            'WorkOrderType' => $WorkOrderType,
            'WOStatus' => $WOStatus,
            'WOPriority' => $WOPriority,
            'WOCategory' => $WOCategory,
            'StartDate' => isset($StartDateTime)?date("Y-m-d H:i:s",strtotime($StartDateTime)):NULL,
            'EndDate' => isset($EndDateTime)?date("Y-m-d H:i:s",strtotime($EndDateTime)):NULL,
            'StartTime' => isset($StartDateTime)?date("H:i:s",strtotime($StartDateTime)):NULL,
            'EndTime' => isset($EndDateTime)?date("H:i:s",strtotime($EndDateTime)):NULL,
            'PopUpReminder' => $PopUpReminder,
            'PrimaryContact' => !empty($PrimaryContact)?$PrimaryContact:NULL,
            'WORecurrenceID' => !empty($WORecurrenceID)?$WORecurrenceID:NULL,
            'IsRecurring' => isset($IsRecurring)?$IsRecurring:0
        );  

        if(isset($IsRecurring) && $IsRecurring == 1 && isset($WORecurrenceID) && !empty($WORecurrenceID)){

            $WorkOrderID = $this->CreateRecurringWorkOrder($WORecurrenceID, $data);

        } else {

            $result = $this->db->insert('WorkOrder', $data); 
            $WorkOrderID = $this->db->insert_id();

            $WorkOrderNo = MY_Controller::genCode('WO', $WorkOrderID);
            $this->db->update('WorkOrder', array('WorkOrderNo' => $WorkOrderNo), array('WorkOrderID' => $WorkOrderID));  

            MY_Model::insertCreatedModifiedByAndDt($UserID,'WorkOrder', array('WorkOrderID' => $WorkOrderID));

            if(isset($StartDateTime) && isset($EndDateTime) && !empty($StartDateTime) && !empty($EndDateTime))
                $this->InsertRecurringEvent($WorkOrderID, $WorkOrderNo, $Subject, $AssignedTo, $StartDateTime, $EndDateTime);
        }

        /*$data = array(
            'Subject' => $WorkOrderNo.' - '.$Subject,
            'RelatedTo' => 'WorkOrder',
            'What' => $WorkOrderID,
            'AssignedTo' => $AssignedTo,
            'Who' => !empty($PrimaryContact)?$PrimaryContact:NULL,
            'Description' => $Description,
            'EventStartDate' => date("Y-m-d H:i:s", strtotime($StartDateTime)),
            'EventEndDate' => date("Y-m-d H:i:s", strtotime($EndDateTime)),
            'EventStartTime' => date("H:i:s", strtotime($StartDateTime)),
            'EventEndTime' => date("H:i:s", strtotime($EndDateTime)),
            'RecurrenceID' => !empty($WORecurrenceID)?$WORecurrenceID:NULL,
            'IsRecurrence' => isset($IsRecurring)?$IsRecurring:0
        );
        $result = $this->db->insert('Event', $data);  

        $EventID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));*/

        /* For insert custom fields */ 
        MY_Model::insertCustomFieldData('WorkOrder', $WorkOrderID);
    
        return $WorkOrderID;
    }

    function CreateRecurringWorkOrder($WORecurrenceID, $WorkOrderData)
    {
        $query = $this->db->get_where('WORecurrence', array('WORecurrenceID' => $WORecurrenceID));  
        $result = $query->row();

        $StartOnDate = date("Y-m-d", strtotime($result->StartOn));
        $EndsAfterOccurrences = isset($result->EndsAfterOccurrences) ? ($result->EndsAfterOccurrences * $result->IntervalEvery) : (100 * $result->IntervalEvery);

        if(isset($result->RepeatEvery) && ($result->RepeatEvery == "Daily" || $result->RepeatEvery == "Monthly" || $result->RepeatEvery == "Yearly")){

            if($result->RepeatEvery == "Daily"){
                $IntervalUnit = 'D'; 
                $IntervalType = 'day'; 
            } else if($result->RepeatEvery == "Monthly"){
                $IntervalUnit = 'M'; 
                $IntervalType = 'months'; 
            } else if($result->RepeatEvery == "Yearly"){
                $IntervalUnit = 'Y'; 
                $IntervalType = 'years'; 
            }

            if($result->Ends == 'On'){
                $EndsOnDate = date("Y-m-d", strtotime($result->EndsOnDate));
            } else{
                $EndsOnDate = date('Y-m-d', strtotime("+$EndsAfterOccurrences $IntervalType", strtotime($StartOnDate)));
            }

            $start = new DateTime( $StartOnDate );
            $end = new DateTime( $EndsOnDate );
            $interval = new DateInterval('P'.$result->IntervalEvery.''.$IntervalUnit.'');
            $flag = 1;
            $period = new DatePeriod($start, $interval, $end);
            foreach ($period as $key => $date) {
 
                //print $date->format('Y-m-d H:i:s') . '<br/>';
                $StartDateTime = $date->format('Y-m-d').' '.$result->StartTime;
                $EndDateTime = $date->format('Y-m-d').' '.$result->EndTime;
             
                $RecurringWorkOrderData =  $this->InsertRecurringWorkOrder($WorkOrderData, $StartDateTime, $EndDateTime);

                if($key == 0)
                    $FirstWorkOrderID = $RecurringWorkOrderData['WorkOrderID'];

                $this->InsertRecurringEvent($RecurringWorkOrderData['WorkOrderID'], $RecurringWorkOrderData['WorkOrderNo'], $WorkOrderData['Subject'], $WorkOrderData['AssignedTo'], $StartDateTime, $EndDateTime);
                
                if((isset($result->EndsAfterOccurrences) && $flag == $result->EndsAfterOccurrences) || $flag == 100)
                        break;
                $flag++;
            }

        } else if(isset($result->RepeatEvery) && $result->RepeatEvery == "Weekly"){

            $RepeatOn = explode(",", $result->RepeatOn);
            if($result->Ends == 'On'){
                $EndsOnDate = date("Y-m-d", strtotime($result->EndsOnDate));
            } else{
                $EndsOnDate = date('Y-m-d', strtotime("+$EndsAfterOccurrences weeks", strtotime($StartOnDate)));
            }

            $start = new DateTime( $StartOnDate );
            $end = new DateTime( $EndsOnDate );
            $interval = new DateInterval('P1D');

            $period = new DatePeriod($start, $interval, $end);

            // only trigger every interval weeks...
            $weekInterval = $result->IntervalEvery;

            $fakeWeek = 0;
            $currentWeek = $start->format('W');
            $flag = 1;
            foreach ($period as $key => $date) {

                if ($date->format('W') !== $currentWeek) {
                    $currentWeek = $date->format('W');
                    $fakeWeek++; 
                    //print ' WEEK ' . $currentWeek . '<br/>';
                }

                if ($fakeWeek % $weekInterval !== 0) {
                    continue;
                }

                $dayOfWeek = $date->format('D');

                if (in_array($dayOfWeek, $RepeatOn)) {
                    //print $date->format('Y-m-d H:i:s') . '   ' . $dayOfWeek . '<br/>';
                    $StartDateTime = $date->format('Y-m-d').' '.$result->StartTime;
                    $EndDateTime = $date->format('Y-m-d').' '.$result->EndTime;
                    
                    $RecurringWorkOrderData = $this->InsertRecurringWorkOrder($WorkOrderData, $StartDateTime, $EndDateTime);

                    if($key == 0)
                        $FirstWorkOrderID = $RecurringWorkOrderData['WorkOrderID'];
                    
                    $this->InsertRecurringEvent($RecurringWorkOrderData['WorkOrderID'], $RecurringWorkOrderData['WorkOrderNo'], $WorkOrderData['Subject'], $WorkOrderData['AssignedTo'], $StartDateTime, $EndDateTime);

                    if((isset($result->EndsAfterOccurrences) && $flag == $result->EndsAfterOccurrences) || ($flag == 100)){
                        break;
                    }
                    $flag++;
                }
            }
        } /*else if(isset($result->RepeatEvery) && $result->RepeatEvery == "Periodically"){
            
        }*/

        return $FirstWorkOrderID;
    }

    function InsertRecurringWorkOrder($WorkOrderData, $StartDateTime, $EndDateTime)
    {

        $UserID = $this->LoginUserID;
        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'AssignedTo' => $WorkOrderData['AssignedTo'],
            'Account' => $WorkOrderData['Account'],
            'Subject' => $WorkOrderData['Subject'],
            'Description' => $WorkOrderData['Description'],
            'Address' => $WorkOrderData['Address'],
            'City' => $WorkOrderData['City'],
            'State' => $WorkOrderData['State'],
            'Country' => $WorkOrderData['Country'],
            'PostalCode' => $WorkOrderData['PostalCode'],
            'Latitude' => $WorkOrderData['Latitude'],
            'Longitude' => $WorkOrderData['Longitude'],
            'WorkOrderType' => $WorkOrderData['WorkOrderType'],
            'WOStatus' => $WorkOrderData['WOStatus'],
            'WOPriority' => $WorkOrderData['WOPriority'],
            'WOCategory' => $WorkOrderData['WOCategory'],
            'StartDate' => date("Y-m-d H:i:s",strtotime($StartDateTime)),
            'EndDate' => date("Y-m-d H:i:s",strtotime($EndDateTime)),
            'StartTime' => date("H:i:s",strtotime($StartDateTime)),
            'EndTime' => date("H:i:s",strtotime($EndDateTime)),
            'PopUpReminder' => $WorkOrderData['PopUpReminder'],
            'PrimaryContact' => !empty($WorkOrderData['PrimaryContact'])?$WorkOrderData['PrimaryContact']:NULL,
            'WORecurrenceID' => $WorkOrderData['WORecurrenceID'],
            'IsRecurring' => $WorkOrderData['IsRecurring']
        );  
        $result = $this->db->insert('WorkOrder', $data);  
        //echo $this->db->last_query(); exit;
        $WorkOrderID = $this->db->insert_id();

        $WorkOrderNo = MY_Controller::genCode('WO', $WorkOrderID);
        $this->db->update('WorkOrder', array('WorkOrderNo' => $WorkOrderNo), array('WorkOrderID' => $WorkOrderID)); 

        MY_Model::insertCreatedModifiedByAndDt($UserID,'WorkOrder', array('WorkOrderID' => $WorkOrderID));

        return array('WorkOrderID' => $WorkOrderID, 'WorkOrderNo' => $WorkOrderNo);
    }

    function InsertRecurringEvent($WorkOrderID, $WorkOrderNo, $Subject, $AssignedTo, $StartDateTime, $EndDateTime)
    {

        $UserID = $this->LoginUserID;

        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'Subject' => $WorkOrderNo.' - '.$Subject,
            'RelatedTo' => 'WorkOrder',
            'What' => $WorkOrderID,
            'AssignedTo' => $AssignedTo,
            'EventStartDate' => date("Y-m-d H:i:s", strtotime($StartDateTime)),
            'EventEndDate' => date("Y-m-d H:i:s", strtotime($EndDateTime)),
            'EventStartTime' => date("H:i:s", strtotime($StartDateTime)),
            'EventEndTime' => date("H:i:s", strtotime($EndDateTime))
        );
        $result = $this->db->insert('Event', $data);  
        //echo $this->db->last_query(); exit;
        $EventID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));
    }

    function saveRecurrenceSetting()
    { 
        extract($_POST);

        if(isset($WORecurrenceID) && !empty($WORecurrenceID))
        {
            $data = array(
                'StartOn' => date("Y-m-d",strtotime($StartOn)),
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'RepeatOn' => ($RepeatEvery=='Weekly')?implode(',', $RepeatOn):NULL,
                'Ends' => $Ends,
                'EndsOnDate' => ($Ends=='On')?date("Y-m-d",strtotime($EndsOnDate)):NULL,
                'EndsAfterOccurrences' => ($Ends=='After')?$EndsAfterOccurrences:NULL,
                'StartTime' => date("H:i:s",strtotime($StartTime)),
                'EndTime' => date("H:i:s",strtotime($EndTime)),
                'LastModifiedDate' => date("Y-m-d H:i:s")
            ); 
            $result = $this->db->update('WORecurrence', $data, array('WORecurrenceID'=>$WORecurrenceID));    

        } else {
            $data = array(
                'StartOn' => date("Y-m-d",strtotime($StartOn)),
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'RepeatOn' => ($RepeatEvery=='Weekly')?implode(',', $RepeatOn):NULL,
                'Ends' => $Ends,
                'EndsOnDate' => ($Ends=='On')?date("Y-m-d",strtotime($EndsOnDate)):NULL,
                'EndsAfterOccurrences' => ($Ends=='After')?$EndsAfterOccurrences:NULL,
                'StartTime' => date("H:i:s",strtotime($StartTime)),
                'EndTime' => date("H:i:s",strtotime($EndTime)),
                'CreatedDate' => date("Y-m-d H:i:s"),
                'LastModifiedDate' => date("Y-m-d H:i:s")
            ); 
            $result = $this->db->insert('WORecurrence', $data);    
            $WORecurrenceID = $this->db->insert_id();
        }
        return $WORecurrenceID;
    }
 
    function EditWorkOrder($WorkOrderID) {
               
        extract($_POST); 

        $UserID = $this->LoginUserID;
  
        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'AssignedTo' => $AssignedTo,
            'ParentWorkOrder' => !empty($ParentWorkOrder)?$ParentWorkOrder:NULL,
            'Account' => $Account,
            'Subject' => $Subject,
            'Description' => $Description,
            'Address' => $Address,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => $Latitude,
            'Longitude' => $Longitude,
            'WorkOrderType' => $WorkOrderType,
            'WOStatus' => $WOStatus,
            'WOPriority' => $WOPriority,
            'WOCategory' => $WOCategory,
            'PrimaryContact' => !empty($PrimaryContact)?$PrimaryContact:NULL,
            'PopUpReminder' => $PopUpReminder,
            /*'WORecurrenceID' => !empty($WORecurrenceID)?$WORecurrenceID:NULL,*/
            /*'IsRecurring' => isset($IsRecurring)?$IsRecurring:0*/
        ); 
  
        $result = $this->db->update('WorkOrder', $data, array('WorkOrderID' => $WorkOrderID));    
    
        MY_Model::updateCreatedModifiedByAndDt($UserID,'WorkOrder', array('WorkOrderID' => $WorkOrderID));
    
        /* For insert custom fields */ 
        MY_Model::updateCustomFieldData('WorkOrder', $WorkOrderID);
        
        return $result;
    } 

    function removeRecurrenceSetting()
    {
        $WORecurrenceID = $_REQUEST['WORecurrenceID'];
        $WorkOrderID = $_REQUEST['WorkOrderID'];
        $this->db->update('WorkOrder',  array('IsRecurring' => 0), array('WorkOrderID' => $WorkOrderID));  
        return $this->db->delete('WORecurrence', array('WORecurrenceID' => $WORecurrenceID));
    }

    function getWorkOrderData($WorkOrderID, $RelatedObjName = 'AllObject')
    {
        $result = $this->db->update('WorkOrder', array('LastModifiedDate' => date("Y-m-d H:i:s")), array('WorkOrderID' => $WorkOrderID));  

        $query = $this->db->select('w.*, w.Subject as Title, ac.AccountName, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, CONCAT(pm.FirstName, " ", pm.LastName) as PrimaryContactName, wos.Status, wop.Priority, woc.CategoryName, pwo.Subject as ParentWorkOrderName, wot.WorkOrderType as WorkOrderTypeName, DATE_FORMAT(w.StartDate, "'.RES_DATETIME.'") as StartDate, DATE_FORMAT(w.EndDate, "'.RES_DATETIME.'") as EndDate, DATE_FORMAT(w.EndTime, "'.RES_TIME.'") as EndTime,DATE_FORMAT(w.StartTime, "'.RES_TIME.'") as StartTime, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName, DATE_FORMAT(w.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, DATE_FORMAT(w.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, org.CompanyName, org.Website, org.Phone as OrgPhone, org.BillingAddress as OrgBillingAddress, org.BillingCity as OrgBillingCity, org.BillingState as OrgBillingState,org.BillingCountry as OrgBillingCountry, org.BillingPostalCode as OrgBillingPostalCode, ac.PhoneNo as AcPhoneNo, org.Tax')
        ->from('WorkOrder w')
        ->join('Account ac','ac.AccountID = w.Account', 'left')
        ->join('User at','at.UserID = w.AssignedTo', 'left')
        ->join('Contact pm','pm.ContactID = w.PrimaryContact', 'left')
        ->join('WorkOrder pwo','pwo.WorkOrderID = w.ParentWorkOrder', 'left')
        ->join('WorkOrderType wot','wot.WorkOrderTypeID = w.WorkOrderType', 'left')
        ->join('WOStatus wos','wos.WOStatusID = w.WOStatus', 'left')
        ->join('WOPriority wop','wop.WOPriorityID = w.WOPriority', 'left')
        ->join('WOCategory woc','woc.WOCategoryID = w.WOCategory', 'left')
        ->join('User cb','cb.UserID = w.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = w.LastModifiedBy', 'left')
        ->join('Organization org','org.OrganizationID = w.OrganizationID', 'left')
        ->where(array('w.OrganizationID' => $this->LoginOrganizationID, 'w.WorkOrderID' => $WorkOrderID)) 
        ->get();
        $data['WorkOrderData'] = $query->row();

        $this->db->select('SUM(SubTotal) as SubTot, SUM(NetTotal) as NetTot, SUM(ListPrice) as ListTot, SUM(UnitPrice) as UnitTot');
        $this->db->from('WOLineItem');
        $this->db->where(array('WorkOrder' => $WorkOrderID, 'OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0));
        $query = $this->db->get(); 
        $result = $query->row();
        if(!empty($result->SubTot) && !empty($result->NetTot) && !empty($result->ListTot) && !empty($result->UnitTot)){
            $Financials['SubTotal'] = $result->SubTot;
            $Financials['Discount'] = (100 - ($result->UnitTot / $result->ListTot) * 100);
            $Financials['Tax'] = ($result->NetTot - $result->SubTot);
            $Financials['TotalPrice'] = $result->ListTot;
            $Financials['GrandTotal'] = $result->NetTot;
        } else {
            $Financials['SubTotal'] = 0.00;
            $Financials['Discount'] = 0.00;
            $Financials['Tax'] = 0.00;
            $Financials['TotalPrice'] = 0.00;
            $Financials['GrandTotal'] = 0.00;
        }
        $data['WOFinancials'] = $Financials;

        if(isset($data['WorkOrderData']->WORecurrenceID)){
            $query = $this->db->select('wor.RepeatEvery, wor.IntervalEvery, wor.RepeatOn, wor.Ends, wor.EndsOnDate, wor.EndsAfterOccurrences, wor.StartTime, wor.EndTime')
            ->from('WORecurrence wor')
            ->where(array('wor.WORecurrenceID' => $data['WorkOrderData']->WORecurrenceID)) 
            ->get();
            $data['WORecurrence'] = $query->row();
        } 

        if($RelatedObjName == 'Chemicals'  || $RelatedObjName == 'AllObject'){
            $query = $this->db->select('c.*, p.ProductName, tcuom.UnitOfMeasurement as TCUOM, auom.UnitOfMeasurement as AUOM');
            $this->db->from('Chemical c');
            $this->db->join('Product p','p.ProductID = c.Product', 'left');
            $this->db->join('UnitOfMeasurement tcuom','tcuom.UnitOfMeasurementID = c.TestedUnitOfMeasure', 'left');
            $this->db->join('UnitOfMeasurement auom','auom.UnitOfMeasurementID = c.ApplicationUnitOfMeasure', 'left');
            $this->db->where(array('c.WorkOrder' => $WorkOrderID, 'c.OrganizationID' => $this->LoginOrganizationID, 'c.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Chemicals'] = $query->result();
        }

        if($RelatedObjName == 'WOLineItems'  || $RelatedObjName == 'AllObject'){

            $query = $this->db->select('woli.*, p.ProductName');
            $this->db->from('WOLineItem woli');
            $this->db->join('Product p','p.ProductID = woli.Product', 'left');
            $this->db->where(array('woli.WorkOrder' => $WorkOrderID, 'woli.OrganizationID' => $this->LoginOrganizationID, 'woli.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['WOLineItems'] = $query->result();
        }

        if($RelatedObjName == 'Invoices'  || $RelatedObjName == 'AllObject'){
            $this->db->select('i.InvoiceID,i.InvoiceNo, (Select SUM(TotalPrice) FROM InvoiceLineItem WHERE Invoice=i.InvoiceID) as TotalPrice, (Select SUM(SubTotal) FROM InvoiceLineItem WHERE Invoice=i.InvoiceID) as SubTotal, is.InvoiceStatus, DATE_FORMAT(i.InvoiceDate, "'.RES_DATE.'") as InvoiceDate, DATE_FORMAT(i.DueDate, "'.RES_DATE.'") as DueDate, wo.Subject as WorkOrderName');
            $this->db->from('Invoice i');
            $this->db->join('InvoiceStatus is','is.InvoiceStatusID = i.InvoiceStatus', 'left');
            $this->db->join('WorkOrder wo','wo.WorkOrderID = i.WorkOrder', 'left');
            $this->db->where(array('i.WorkOrder' => $WorkOrderID, 'i.OrganizationID' => $this->LoginOrganizationID, 'i.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                    $this->db->limit(10);
            $query = $this->db->get();  

            $data['Invoices'] = $query->result();
        }
    
        if($RelatedObjName == 'Events'  || $RelatedObjName == 'AllObject'){
            $query = $this->db->select('e.EventID, e.AssignedTo, e.Subject, CONCAT(pc.FirstName, " ", pc.LastName) as ContactName,  CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, DATE_FORMAT(e.EventStartDate, "'.RES_DATETIME.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATETIME.'") as EventEndDate, DATE_FORMAT(e.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, e.CreatedBy, e.Who');
            $this->db->from('Event e');
            $this->db->join('User cb','cb.UserID = e.CreatedBy', 'left');
            $this->db->join('User at','at.UserID = e.AssignedTo', 'left');
            $this->db->join('Contact pc','pc.ContactID = e.Who', 'left');
            $this->db->join('User lmb','lmb.UserID = e.LastModifiedBy', 'left');
            $this->db->where(array('e.What' => $WorkOrderID,'e.RelatedTo' => 'WorkOrder', 'e.OrganizationID' => $this->LoginOrganizationID, 'e.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Events'] = $query->result();
        }

        if($RelatedObjName == 'Files'  || $RelatedObjName == 'AllObject'){
            $this->db->select('f.FileID, f.FileName, f.Subject, f.ContentType, DATE_FORMAT(f.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, f.CreatedBy');
            $this->db->from('File f');
            $this->db->join('User cb','cb.UserID = f.CreatedBy', 'left');
            $this->db->where(array('f.What' => $WorkOrderID,'f.RelatedTo' => 'WorkOrder', 'f.OrganizationID' => $this->LoginOrganizationID, 'f.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Files'] = $query->result();
        }

        if($RelatedObjName == 'Tasks'  || $RelatedObjName == 'AllObject'){
           $this->db->select('t.TaskID, t.Subject, tt.TaskType, DATE_FORMAT(t.DueDate, "'.RES_DATETIME.'") as DueDate, tp.Priority, ts.TaskStatus, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, CONCAT(pc.FirstName, " ", pc.LastName) as ContactName, t.CreatedBy, t.Who, t.AssignedTo');
            $this->db->from('Task t'); 
            $this->db->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left');
            $this->db->join('User at','at.UserID = t.AssignedTo', 'left');
            $this->db->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left');
            $this->db->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left');
            $this->db->join('Contact pc','pc.ContactID = t.Who', 'left');
            $this->db->where(array('t.What' => $WorkOrderID,'t.RelatedTo' => 'WorkOrder', 't.OrganizationID' => $this->LoginOrganizationID, 't.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Tasks'] = $query->result();
        }

        if($RelatedObjName == 'Notes'  || $RelatedObjName == 'AllObject'){
           $this->db->select('n.NoteID, n.Subject, DATE_FORMAT(n.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, CONCAT(o.FirstName, " ", o.LastName) as OwnerName, n.Owner');
            $this->db->from('Note n'); 
            $this->db->join('User o','o.UserID = n.Owner', 'left');
            $this->db->where(array('n.What' => $WorkOrderID,'n.RelatedTo' => 'WorkOrder', 'n.OrganizationID' => $this->LoginOrganizationID, 'n.IsDeleted' => 0));
            if($RelatedObjName == 'AllObject')
                $this->db->limit(10);
            $query = $this->db->get(); 
            $data['Notes'] = $query->result();
        }
        return $data;
    }

    function ConvertToInvoice($WorkOrderID)
    {
        $UserID = $this->LoginUserID;

        extract($_POST);

        $query = $this->db->get_where('WorkOrder', array('OrganizationID' => $this->LoginOrganizationID, 'WorkOrderID' => $WorkOrderID)); 
        
        if($query->num_rows() > 0){ 

            $result = $query->row(); 

            $data = array(
                'OrganizationID' => $this->LoginOrganizationID,
                'AssignedTo' => $result->AssignedTo,
                'Account' => $result->Account,
                'WorkOrder' => $result->WorkOrderID,
                'Contact' => $result->PrimaryContact,
                'Address' => $result->Address,
                'City' => $result->City,
                'State' => $result->State,
                'Country' => $result->Country,
                'PostalCode' => $result->PostalCode,
                'Latitude' => $result->Latitude,
                'Longitude' => $result->Longitude,
                'InvoiceDate' => date("Y-m-d"),
                'DueDate' => date("Y-m-d"),
            );     
            $result = $this->db->insert('Invoice', $data); 
            $InvoiceID = $this->db->insert_id();

            $InvoiceNo = MY_Controller::genCode('IN', $InvoiceID);
            $this->db->update('Invoice', array('InvoiceNo' => $InvoiceNo), array('InvoiceID' => $InvoiceID));  

            MY_Model::insertCreatedModifiedByAndDt($UserID,'Invoice', array('InvoiceID' => $InvoiceID));

            $query = $this->db->get_where('WOLineItem', array('OrganizationID' => $this->LoginOrganizationID, 'WorkOrder' => $WorkOrderID));
            if($query->num_rows() > 0){ 

                $LineItemsResult = $query->result(); 

                foreach ($LineItemsResult as $key => $LineItem) {
                    
                    $data = array(
                        'OrganizationID' => $this->LoginOrganizationID,
                        'Invoice' => $InvoiceID,
                        'Product' => $LineItem->Product,
                        'ListPrice' => $LineItem->ListPrice,
                        'Discount' => $LineItem->Discount,
                        'UnitPrice' => $LineItem->UnitPrice,
                        'Quantity' => $LineItem->Quantity,
                        'SubTotal' => $LineItem->SubTotal,
                        'Taxable' => $LineItem->Taxable,
                        'TotalPrice' => $LineItem->NetTotal,
                    );     
                    $query = $this->db->insert('InvoiceLineItem', $data);  
                    $LineItemID = $this->db->insert_id();

                    $LineItemNo = MY_Controller::genCode('INL' ,$LineItemID);
                    $this->db->update('InvoiceLineItem', array('InvoiceLineNo'=>$LineItemNo), array('InvoiceLineItemID' => $LineItemID));  
           
                    MY_Model::insertCreatedModifiedByAndDt($UserID,'InvoiceLineItem', array('InvoiceLineItemID' => $LineItemID));
                }
            }

            return $InvoiceID;
        }

        return 0;
    }

    function SaveScheduling(){

        extract($_POST); 

        $UserID = $this->LoginUserID;
  
         $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'Subject' => $WorkOrderNo.' - '.$Subject,
            'RelatedTo' => 'WorkOrder',
            'What' => $WorkOrderID,
            'AssignedTo' => $AssignedTo,
            'Who' => !empty($Who)?$Who:NULL,
            'Description' => $Description,
            'EventStartDate' => date("Y-m-d H:i:s", strtotime($EventStartDate)),
            'EventEndDate' => date("Y-m-d H:i:s", strtotime($EventEndDate)),
            'EventStartTime' => date("H:i:s", strtotime($EventStartDate)),
            'EventEndTime' => date("H:i:s", strtotime($EventEndDate)),
        );
        $result = $this->db->insert('Event', $data);  

        $EventID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));

        if($result){

            $this->UpdateWOStartEndDateTime($WorkOrderID);

            $UserRes = $query = $this->db->get_where('User', array('UserID' => $AssignedTo));  
            $User = $UserRes->row();

            return $this->GetAllSchedulingData($WorkOrderID);
        }
    }

    function UpdateScheduling($EventID)
    {
        extract($_POST); 

        $UserID = $this->LoginUserID;
  
         $data = array(
            'AssignedTo' => $AssignedTo,
            'EventStartDate' => date("Y-m-d H:i:s", strtotime($EventStartDate)),
            'EventEndDate' => date("Y-m-d H:i:s", strtotime($EventEndDate)),
            'EventStartTime' => date("H:i:s", strtotime($EventStartDate)),
            'EventEndTime' => date("H:i:s", strtotime($EventEndDate)),
        );
        $result = $this->db->update('Event', $data, array('EventID' => $EventID));  

        MY_Model::updateCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));

        if($result){
 
            $this->UpdateWOStartEndDateTime($WorkOrderID);
            $UserRes = $query = $this->db->get_where('User', array('UserID' => $AssignedTo));  
            $User = $UserRes->row();

            return $this->GetAllSchedulingData($WorkOrderID);
        }
    }

    function UpdateWOStartEndDateTime($WorkOrderID)
    {
        $UserID = $this->LoginUserID;
 
        $query = $this->db->select('MIN(EventStartDate) as MinEventStartDate, MAX(EventEndDate) as MaxEventEndDate, MIN(EventStartDate) as MinEventStartTime, MAX(EventEndDate) as MaxEventEndTime')
        ->from('Event')
        ->where(array('OrganizationID' => $this->LoginOrganizationID, 'RelatedTo' => 'WorkOrder', 'What' => $WorkOrderID, 'IsDeleted' => 0))
        ->get(); 
        $Event = $query->row(); 
        
        if(!empty($Event->MinEventStartDate) && !empty($Event->MaxEventEndDate) && !empty($Event->MinEventStartTime) && !empty($Event->MaxEventEndTime)){
            $data = array(
                'StartDate' => date("Y-m-d H:i:s", strtotime($Event->MinEventStartDate)),
                'EndDate' => date("Y-m-d H:i:s", strtotime($Event->MaxEventEndDate)),
                'StartTime' => date("H:i:s", strtotime($Event->MinEventStartTime)),
                'EndTime' => date("H:i:s", strtotime($Event->MaxEventEndTime)),
            ); 
        } else {
            $data = array(
                'StartDate' => NULL,
                'EndDate' => NULL,
                'StartTime' => NULL,
                'EndTime' => NULL,
            ); 
        }

        $result = $this->db->update('WorkOrder', $data, array('WorkOrderID' => $WorkOrderID, 'OrganizationID' => $this->LoginOrganizationID));  

        MY_Model::updateCreatedModifiedByAndDt($UserID,'WorkOrder', array('WorkOrderID' => $WorkOrderID));
    }

    function DeleteScheduling()
    {
        extract($_POST); 

        $this->db->delete('Event', array('EventID' => $EventID, 'OrganizationID' => $this->LoginOrganizationID)); 

        $this->UpdateWOStartEndDateTime($WorkOrderID);

        return $this->GetAllSchedulingData($WorkOrderID);
    }

    function GetScheduling($EventID)
    {
        $query = $this->db->get_where('Event', array('EventID' => $EventID, 'OrganizationID' => $this->LoginOrganizationID)); 
        $Event = $query->row(); 

        $SchedulingWrap = "";
        $SchedulingWrap .= '<div id="Event'.$EventID.'"><div class="form-wrap">
                   <form action="#" class="form-horizontal" id="SchedulingForm'.$EventID.'">
                    <input type="hidden" name="WorkOrderID" value="'.$Event->What.'">
                   <div class="form-body">
                       <div class="form-group">
                           <label class="col-md-3 control-label text-left">Assigned To</label>
                           <div class="col-md-6">
                               <select name="AssignedTo" id="AssignedTo" class="form-control select2">
                               <option value="">Select Owner</option>';
                                 $Users = $this->ActionsModel->GetUsers();
                                    foreach ($Users as $User) { 
                                        if($Event->AssignedTo == $User->UserID){
                                           $SchedulingWrap .= '<option value="'.$User->UserID.'" selected="">'.$User->FirstName.' '.$User->LastName.'</option>';
                                        } else {
                                           $SchedulingWrap .= '<option value="'.$User->UserID.'">'.$User->FirstName.' '.$User->LastName.'</option>';
                                        }
                                    } 
                            $SchedulingWrap .= '</select>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-md-3 control-label text-left">Start</label>
                           <div class="col-md-6">
                               <input type="text" class="form-control showdatetimepicker" name="EventStartDate" value="'.date("m/d/Y h:i A", strtotime($Event->EventStartDate)).'" required>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-md-3 control-label text-left">End</label>
                           <div class="col-md-6">
                               <input type="text" class="form-control showdatetimepicker" name="EventEndDate" value="'.date("m/d/Y h:i A", strtotime($Event->EventEndDate)).'" required>
                           </div>
                       </div>
                   </div>
                   <div class="pull-right">
                       <button type="button" class="btn btn-default" OnClick="UpdateScheduling('.$EventID.')" data-dismiss="modal">Close</button>
                       <button type="button" class="btn btn-primary" OnClick="UpdateScheduling('.$EventID.')">Save</button>
                   </div>
                   </form>
                 <div class="clearfix"></div>
                 <hr class="light-grey-hr">
                 </div>
                 </div>';

        return $SchedulingWrap;
    }

    function EditSchAssignedTo()
    { 
        extract($_POST); 
        $SchedulingWrap = "";
        $SchedulingWrap .= '<select name="AssignedTo" id="AssignedToVal'.$EventID.'" class="form-control select2">
                               <option value="">Select Owner</option>';
                                 $Users = $this->ActionsModel->GetUsers();
                                    foreach ($Users as $User) { 
                                        if($AssignedTo == $User->UserID){
                                           $SchedulingWrap .= '<option value="'.$User->UserID.'" selected="">'.$User->FirstName.' '.$User->LastName.'</option>';
                                        } else {
                                           $SchedulingWrap .= '<option value="'.$User->UserID.'">'.$User->FirstName.' '.$User->LastName.'</option>';
                                        }
                                    } 
                            $SchedulingWrap .= '</select>';
        return $SchedulingWrap;

    }

    function UpdateAssignedTo()
    {
        $UserID = $this->LoginUserID;

        extract($_POST); 

        $result = $this->db->update('Event', array('AssignedTo' => $AssignedTo), array('EventID' => $EventID, 'OrganizationID' => $this->LoginOrganizationID));  

        MY_Model::updateCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));

        $UserRes = $query = $this->db->get_where('User', array('UserID' => $AssignedTo));  
        $User = $UserRes->row(); 

        return $User->FirstName.' '.$User->LastName;
    }

    function GetAllSchedulingData($WorkOrderID)
    {
        $query = $this->db->select('e.*, CONCAT(at.FirstName, " ", at.LastName) as AssignedToName, DATE_FORMAT(e.EventStartDate, "'.RES_DATETIME.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATETIME.'") as EventEndDate')
        ->from('Event e')
        ->join('User at','at.UserID = e.AssignedTo', 'left')
        ->where(array('e.RelatedTo' => 'WorkOrder', 'e.What' => $WorkOrderID, 'e.OrganizationID' => $this->LoginOrganizationID)) 
        ->order_by("CreatedDate", "desc")
        ->limit(5)
        ->get();
        $Events = array_reverse($query->result()); 

        $SchedulingWrap = "";
        foreach ($Events as $key => $Event) {
        
            $SchedulingWrap .= '<div id="Event'.$Event->EventID.'"><div class="form-wrap">
                        <form action="#" class="form-horizontal schedulingfrmclss">
                        <div class="form-body">
                            <div class="form-group">
                                <div class="col-md-3 text-left txt-dark">
                                    Assigned To
                                </div>
                                <div class="col-md-4" id="AssignedToDropDown'.$Event->EventID.'">
                                    '.$Event->AssignedToName.' 
                                </div>
                                <div class="col-md-4" id="AssignedToBtn'.$Event->EventID.'">
                                   <a OnClick="EditSchAssignedTo('.$Event->EventID.', '.$Event->AssignedTo.')">Change</a>
                                </div>
                                <div class="col-md-1 pl-10">
                                    <div class="dropdown" >
                                        <a data-toggle="dropdown"><i class="fa fa-toggle-down"></i></a>
                                        <ul role="menu" data-dropdown-in="flipInY" data-dropdown-out="flipOutY" class="dropdown-menu" style="right:0;left: unset;">
                                            <li>
                                                <a>View in Calendar</a>
                                                <a OnClick="EditScheduling('.$Event->EventID.')">Edit</a>
                                                <a OnClick="DeleteScheduling('.$Event->EventID.','.$WorkOrderID.')">Delete</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group pt-5">
                                <div class="col-md-3 text-left txt-dark">
                                    Start
                                </div>
                                <div class="col-md-6">
                                    '.$Event->EventStartDate.'
                                </div>
                            </div>
                            <div class="form-group pt-5">
                                <div class="col-md-3 text-left txt-dark">
                                    End
                                </div>
                                <div class="col-md-6">
                                    '.$Event->EventEndDate.'
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="light-grey-hr">
                    </div>';
        }
        return $SchedulingWrap;
    }

    function WOLineItem($WorkOrderID)
    {
        $UserID = $this->LoginUserID;

        extract($_POST);

        $query = $this->db->delete('WOLineItem', array('WorkOrder' => $WorkOrderID, 'OrganizationID' => $this->LoginOrganizationID));

        if(count($Product) > 0){ 

            foreach ($Product as $key => $value) {
            
               if(!empty($Product[$key]) && !empty($ListPrice[$key]) && !empty($UnitPrice[$key]) && !empty($Quantity[$key]) && !empty($SubTotal[$key]) && !empty($NetTotal[$key])){
                    $data = array(
                        'OrganizationID' => $this->LoginOrganizationID,
                        'WorkOrder' => $WorkOrderID,
                        'Product' => $Product[$key],
                        'ListPrice' => filter_var($ListPrice[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
                        'Discount' => filter_var($Discount[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
                        'UnitPrice' => filter_var($UnitPrice[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
                        'Quantity' => $Quantity[$key],
                        'SubTotal' => filter_var($SubTotal[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
                        'Taxable' => empty($Taxable[$key])?0:1,
                        'NetTotal' => filter_var($NetTotal[$key], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION)
                    );     
                    $query = $this->db->insert('WOLineItem', $data);  
                    $WOLineItemID = $this->db->insert_id();

                    $LineItemNo = MY_Controller::genCode('WOLI' ,$WOLineItemID);
                    $this->db->update('WOLineItem', array('LineItemNo'=>$LineItemNo), array('WOLineItemID' => $WOLineItemID));  
           
                    MY_Model::insertCreatedModifiedByAndDt($UserID,'WOLineItem', array('WOLineItemID' => $WOLineItemID));
                }
            }
        }

        return $query;
    }

    function CreateWOChemicalLineItems($WorkOrderID)
    {
        $UserID = $this->LoginUserID;

        extract($_POST);

        $query = $this->db->delete('Chemical', array('WorkOrder' => $WorkOrderID));

        if(count($Product) > 0){ 

            foreach ($Product as $key => $value) {
               
               if(!empty($Product[$key]) && !empty($TestConcentration[$key]) && !empty($TestedUnitOfMeasure[$key]) && !empty($ApplicationAmount[$key]) && !empty($ApplicationUnitOfMeasure[$key])){
                    $data = array(
                        'OrganizationID' => $this->LoginOrganizationID,
                        'WorkOrder' => $WorkOrderID,
                        'Product' => $Product[$key],
                        'Owner' => $UserID,
                        'Account' => $Account,
                        'TestConcentration' => $TestConcentration[$key],
                        'TestedUnitOfMeasure' => $TestedUnitOfMeasure[$key],
                        'ApplicationAmount' => $ApplicationAmount[$key],
                        'ApplicationUnitOfMeasure' => $ApplicationUnitOfMeasure[$key],
                        'ApplicationArea' => $ApplicationArea[$key],
                        'AdditionalNotes' => $AdditionalNotes[$key]
                    );     
                    $query = $this->db->insert('Chemical', $data);  

                    $ChemicalID = $this->db->insert_id();

                    $ChemicalNo = MY_Controller::genCode('CH' ,$ChemicalID);
                    $this->db->update('Chemical', array('ChemicalNo'=>$ChemicalNo), array('ChemicalID' => $ChemicalID));  
                    
                    MY_Model::insertCreatedModifiedByAndDt($UserID,'Chemical', array('ChemicalID' => $ChemicalID));
                }
            }
        }

        return $query;
    }

    function GetPrimaryContactOptions()
    { 

        $query = $this->db->get_where('Contact', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0, 'Account' => $_POST['AccountID'])); 
        $results = $query->result();
        $PCOptions = '<option value="">Select Contact</option>';
        foreach ($results as $key => $value) {
            if(isset($_POST['PrimaryContact']) && !empty($_POST['PrimaryContact']) && $_POST['PrimaryContact'] == $value->ContactID)
                $PCOptions .= '<option value="'.$value->ContactID.'" selected="">'.$value->FirstName.' '.$value->LastName.'</option>';
            else 
                 $PCOptions .= '<option value="'.$value->ContactID.'">'.$value->FirstName.' '.$value->LastName.'</option>';
        }
        return $PCOptions;
    }

    function GetProductDetails($ProductID)
    {
        $query = $this->db->get_where('Product', array('OrganizationID' => $this->LoginOrganizationID, 'ProductID' => $ProductID)); 
        return json_encode($query->row()); 
    }
  
    function GetUsers()
    { 
        $query = $this->db->get_where('User', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetWorkOrderTypes()
    { 
        $query = $this->db->get_where('WorkOrderType', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetWOStatus()
    { 
        $query = $this->db->get_where('WOStatus', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function WOPriorities()
    { 
        $query = $this->db->get_where('WOPriority', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function WOCategories()
    { 
        $query = $this->db->get_where('WOCategory', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetAccounts()
    { 
        $UserID = $this->session->userdata('UserID');
        // 'AssignedTo' => $UserID, 
        $query = $this->db->get_where('Account',array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0, 'IsActive' => 1)); 
        
        return $query->result();
    }

    function GetContacts()
    { 
        $query = $this->db->get_where('Contact', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0, 'IsActive' => 1)); 
        
        return $query->result();
    }

    function GetWorkOrders()
    { 
        $query = $this->db->get_where('WorkOrder', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetProducts()
    {
        $query = $this->db->get_where('Product', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetChemicals()
    {
        $query = $this->db->get_where('Product', array('OrganizationID' => $this->LoginOrganizationID, 'IsChemical' => 1, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetUnitOfMeasurement()
    {
        $query = $this->db->get_where('UnitOfMeasurement', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GenDocTemplates($RelatedTo)
    {
        $query = $this->db->get_where('GenDocTemplates', array('OrganizationID' => $this->LoginOrganizationID, 'RelatedTo' => $RelatedTo, 'IsDeleted' => 0, 'IsActive' => 1)); 
        
        return $query->result();
    }
 
    function GetViews(){

        $query = $this->db->query("SELECT * FROM WorkOrderView WHERE ((RestrictVisibility = 'VisibleOnlyToMe' AND UserID = '".$this->LoginUserID."') OR (RestrictVisibility = 'VisibleToEveryone')) AND OrganizationID = '".$this->LoginOrganizationID."' ORDER BY WorkOrderViewName ASC");
        
        return $query->result();
    }

    function GetAccountDetails($AccountID){
        $query = $this->db->get_where('Account', array('OrganizationID' => $this->LoginOrganizationID, 'AccountID' => $AccountID)); 
        return json_encode($query->row()); 
    }

    function DeleteWorkOrder($WorkOrderID, $Action)
    {
        if($Action == 'OnlyThisOne'){
            return $this->db->update('WorkOrder', array('IsDeleted' => 1), array('WorkOrderID' => $WorkOrderID, 'OrganizationID' => $this->LoginOrganizationID)); 

        } else if($Action == 'ThisAndFutureThisSeries'){

            $query = $this->db->select('WorkOrderID, WORecurrenceID')
            ->from('WorkOrder')
            ->where(array('OrganizationID' => $this->LoginOrganizationID,'WorkOrderID' => $WorkOrderID))
            ->get(); 
            $WorkOrder = $query->row();
            return $this->db->update('WorkOrder', array('IsDeleted' => 1), array('WORecurrenceID' => $WorkOrder->WORecurrenceID, 'OrganizationID' => $this->LoginOrganizationID, 'DATE(EndDate) >' => date("Y-m-d")));
            
        } else if($Action == 'ThisAndOpenThisSeries'){

            $query = $this->db->select('w.WorkOrderID, w.WORecurrenceID')
            ->from('WorkOrder w')
            ->where(array('w.OrganizationID' => $this->LoginOrganizationID, 'w.WorkOrderID' => $WorkOrderID))
            ->get(); 
            $WorkOrder = $query->row();

            $this->db->set('w.IsDeleted', 1);
            $this->db->where(array('w.WORecurrenceID' => $WorkOrder->WORecurrenceID, 'w.OrganizationID' => $this->LoginOrganizationID));
            $this->db->where('(w.WOStatus IN (SELECT WOStatusID FROM WOStatus WHERE IsClosed = 0) OR w.WorkOrderID='.$WorkOrderID.')');
            return $this->db->update('WorkOrder w');
            
        } else if($Action == 'AllThisSeries'){

            $query = $this->db->select('w.WorkOrderID, w.WORecurrenceID')
            ->from('WorkOrder w')
            ->where(array('w.OrganizationID' => $this->LoginOrganizationID, 'w.WorkOrderID' => $WorkOrderID))
            ->get(); 
            $WorkOrder = $query->row();

            return $this->db->update('WorkOrder', array('IsDeleted' => 1), array('WORecurrenceID' => $WorkOrder->WORecurrenceID, 'OrganizationID' => $this->LoginOrganizationID));
            
        }
    }

    function GenerateDocument($WorkOrderData) {

        $_POST['SaveToWorkOrder'] = isset($_POST['SaveToWorkOrder'])?$_POST['SaveToWorkOrder']:0;
        
        extract($_POST);
        
        $data = array();
        
        $company = array();
        $company["name"] = $WorkOrderData['WorkOrderData']->CompanyName;
        $company["street_address"] = $WorkOrderData['WorkOrderData']->OrgBillingAddress;
        $company["city_state_zip"] = $WorkOrderData['WorkOrderData']->OrgBillingCity.', '.$WorkOrderData['WorkOrderData']->OrgBillingState.', '.$WorkOrderData['WorkOrderData']->OrgBillingCountry.', '.$WorkOrderData['WorkOrderData']->OrgBillingPostalCode;
        $company["phone"] = $WorkOrderData['WorkOrderData']->OrgPhone;
        $company["fax"] = "";
        $company["website"] = $WorkOrderData['WorkOrderData']->Website;
        $data["company"] = $company;

       /* $host = array();
        $host["logo"] = dirname(__FILE__).'/logo.png';
        $data["host"] = $host;*/

        $wo = array();
        $wo["no"] = $WorkOrderData['WorkOrderData']->WorkOrderNo;
        $wo["date"] = date("m/d/Y", strtotime($WorkOrderData['WorkOrderData']->StartDate));
        $wo["subject"] = $WorkOrderData['WorkOrderData']->Subject;
        $wo["description"] = $WorkOrderData['WorkOrderData']->Description;
        $data["wo"] = $wo;

        $customer = array();
        $customer["name"] = "-";
        $customer["id"] = "-";
        $data["customer"] = $customer;

        $bill = array();
        $bill["name"] = "john jojo";
        $bill["company_name"] = $WorkOrderData['WorkOrderData']->CompanyName;
        $bill["street_address"] = $WorkOrderData['WorkOrderData']->Address;
        $bill["city_state_zip"] = $WorkOrderData['WorkOrderData']->City.','.$WorkOrderData['WorkOrderData']->State.','.$WorkOrderData['WorkOrderData']->Country.','.$WorkOrderData['WorkOrderData']->PostalCode;
        $bill["phone"] = $WorkOrderData['WorkOrderData']->AcPhoneNo;
        $data["bill"] = $bill;

        $items = array();
        $taxable = 0;
        foreach ($WorkOrderData['WOLineItems'] as $key => $WOLineItem) {
            
            $item = array();
            $item["qty"] = $WOLineItem->Quantity;
            $item["description"] = $WOLineItem->ProductName;
            $item["tax"] = ($WOLineItem->Taxable == 1) ? "Y" : "N";
            $item["price"] = number_format((double)$WOLineItem->UnitPrice, 2, '.', ',');
            $item["line_total"] = number_format((double)$WOLineItem->NetTotal, 2, '.', ',');
            $items[] = $item;

            $taxable += ($WOLineItem->Taxable == 1) ? $WOLineItem->NetTotal : 0;
        } 
        $data["items"] = $items;

        $invoice = array();
        $invoice["subtotal"] = number_format($WorkOrderData['WOFinancials']['SubTotal'], 2, '.', ',');
        $invoice["taxable"] = number_format($taxable, 2, '.', ',');
        $invoice["tax_rate"] = $WorkOrderData['WorkOrderData']->Tax.'%';
        $invoice["tax"] = number_format((double)$WorkOrderData['WOFinancials']['Tax'], 2, '.', ',');
        $invoice["s_h"] = "-";
        $invoice["other"] = "-";
        $invoice["total"] = number_format((double)$WorkOrderData['WOFinancials']['GrandTotal'], 2, '.', ',');
        $data["invoice"] = $invoice;

        return MY_Model::generateDocs($GenDocTemplateID, $data, $WorkOrderData['WorkOrderData']->WorkOrderID, $WorkOrderData['WorkOrderData']->WorkOrderNo, $WorkOrderData['WorkOrderData']->AssignedTo, $SaveToWorkOrder, $OutputFormat, 'WorkOrder'); 

    }
}

?>
