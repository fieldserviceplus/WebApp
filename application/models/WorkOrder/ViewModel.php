<?php

class ViewModel extends MY_Model{
 
    public $LoginUserID, $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }
 
    function ViewWorkOrders() 
    {
        if(isset($_GET['flag']) && ($_GET['flag'] == 'WorkOrdersCreatedToday' || $_GET['flag'] == 'WorkOrdersCreatedThisWeek')){

            $aColumns = array('w.WorkOrderID','CONCAT(ast.FirstName, " ", ast.LastName)','w.WorkOrderNo','ac.AccountName','w.Subject','CONCAT(cnt.FirstName, " ", cnt.LastName)','wos.Status', 'wop.Priority', 'wot.WorkOrderType','w.City','w.State','(Select SUM(NetTotal) FROM WOLineItem WHERE WorkOrder=w.WorkOrderID)','DATE_FORMAT(w.CreatedDate, "'.RES_DATETIME.'")','CONCAT(cb.FirstName, " ", cb.LastName)', 'ac.AccountID', 'w.AssignedTo', 'w.CreatedBy', 'w.PrimaryContact');
            $bColumns = array('WorkOrderID','CONCAT(ast.FirstName, " ", ast.LastName)','WorkOrderNo','AccountName','Subject','CONCAT(cnt.FirstName, " ", cnt.LastName)','Status', 'Priority', 'WorkOrderType','City','State','(Select SUM(NetTotal) FROM WOLineItem WHERE WorkOrder=w.WorkOrderID)','DATE_FORMAT(w.CreatedDate, "'.RES_DATETIME.'")','CONCAT(cb.FirstName, " ", cb.LastName)', 'AccountID', 'AssignedTo', 'CreatedBy', 'PrimaryContact');
        } else {

            $aColumns = array('w.WorkOrderID','CONCAT(ast.FirstName, " ", ast.LastName)','w.WorkOrderNo','ac.AccountName','w.Subject','CONCAT(cnt.FirstName, " ", cnt.LastName)','wos.Status', 'wop.Priority', 'wot.WorkOrderType','w.City','w.State','(Select SUM(NetTotal) FROM WOLineItem WHERE WorkOrder=w.WorkOrderID)', 'ac.AccountID', 'w.AssignedTo', 'w.CreatedBy', 'w.PrimaryContact');
            $bColumns = array('WorkOrderID','CONCAT(ast.FirstName, " ", ast.LastName)','WorkOrderNo','AccountName','Subject','CONCAT(cnt.FirstName, " ", cnt.LastName)','Status', 'Priority', 'WorkOrderType','City','State','(Select SUM(NetTotal) FROM WOLineItem WHERE WorkOrder=w.WorkOrderID)', 'AccountID', 'AssignedTo', 'CreatedBy', 'PrimaryContact');
        }
      
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "w.WorkOrderID";

        /* DB table to use */
        $sTable = "WorkOrder w
        LEFT JOIN User ast ON ast.UserID = w.AssignedTo
        LEFT JOIN User cb ON cb.UserID = w.CreatedBy
        LEFT JOIN Account ac ON ac.AccountID = w.Account
        LEFT JOIN Contact cnt ON cnt.ContactID = w.PrimaryContact
        LEFT JOIN WOPriority wop ON wop.WOPriorityID = w.WOPriority 
        LEFT JOIN WorkOrderType wot ON wot.WorkOrderTypeID = w.WorkOrderType 
        LEFT JOIN WOStatus wos ON wos.WOStatusID = w.WOStatus";

        /* Limit */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".( $_GET['iDisplayStart'] ).", ".
                        ( $_GET['iDisplayLength'] );
        }

        /* Ordering */
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                    {
                            $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                                    ".( $_GET['sSortDir_'.$i] ) .", ";
                    }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                    $sOrder = "";
            }
        }

        /* Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited */
        $sWhere = "";
        if ( $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
                for ( $i=0 ; $i<count($aColumns) ; $i++ )
                {
                        $sWhere .= $aColumns[$i]." LIKE '%".( $_GET['sSearch'] )."%' OR ";
                }
                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ')';
        }
        
        /* Where Condition */
        $UserID = $this->session->userdata('UserID');
        if(isset($_GET['flag']) && $_GET['flag'] == 'MyOpenWorkOrdersToday'){

            $where_str = "w.AssignedTo = $UserID AND w.StartDate <= '".CURRENT_DATETIME."' AND w.EndDate >= '".CURRENT_DATETIME."' AND wos.IsClosed=0 AND w.IsDeleted=0 AND w.OrganizationID = '".$this->LoginOrganizationID."'";
        }
        else if(isset($_GET['flag']) && $_GET['flag'] == 'MyOpenWorkOrdersThisWeek'){

            $where_str = "w.AssignedTo = $UserID AND YEARWEEK(w.StartDate, 1) <= YEARWEEK(CURDATE(), 1) AND YEARWEEK(w.EndDate, 1) >= YEARWEEK(CURDATE(), 1) AND wos.IsClosed=0 AND w.IsDeleted=0 AND w.OrganizationID = '".$this->LoginOrganizationID."'";

        } else if(isset($_GET['flag']) && $_GET['flag'] == 'AllOpenWorkOrdersToday'){

            $where_str = "w.StartDate <= '".CURRENT_DATETIME."' AND w.EndDate >= '".CURRENT_DATETIME."' AND wos.IsClosed=0 AND w.IsDeleted=0 AND w.OrganizationID = '".$this->LoginOrganizationID."'";

        } else if(isset($_GET['flag']) && $_GET['flag'] == 'AllWorkOrdersToday'){

            $where_str = "w.StartDate <= '".CURRENT_DATETIME."' AND w.EndDate >= '".CURRENT_DATETIME."' AND w.IsDeleted=0 AND w.OrganizationID = '".$this->LoginOrganizationID."'";

        } else if(isset($_GET['flag']) && $_GET['flag'] == 'AllOpenWorkOrdersThisWeek'){

            $where_str = "YEARWEEK(w.StartDate, 1) <= YEARWEEK(CURDATE(), 1) AND YEARWEEK(w.EndDate, 1) >= YEARWEEK(CURDATE(), 1) AND wos.IsClosed=0 AND w.IsDeleted=0 AND w.OrganizationID = '".$this->LoginOrganizationID."'";

        } else if(isset($_GET['flag']) && $_GET['flag'] == 'WorkOrdersCreatedToday'){

            $where_str = "DATE(w.CreatedDate) = '".CURRENT_DATE."' AND w.IsDeleted=0 AND w.OrganizationID = '".$this->LoginOrganizationID."'";

        } else if(isset($_GET['flag']) && $_GET['flag'] == 'WorkOrdersCreatedThisWeek'){

            $where_str = "YEARWEEK(w.CreatedDate, 1) = YEARWEEK(CURDATE(), 1) AND w.IsDeleted=0 AND w.OrganizationID = '".$this->LoginOrganizationID."'";

        } else if(isset($_GET['flag']) && $_GET['flag'] == 'UnscheduledWorkOrders'){

            $where_str = "w.StartDate IS NULL AND wos.IsClosed=0 AND w.IsDeleted=0 AND w.OrganizationID = '".$this->LoginOrganizationID."'";

        } else if(isset($_GET['flag']) && $_GET['flag'] == 'UnassignedWorkOrders'){

            $where_str = "w.AssignedTo IS NULL AND w.StartDate IS NOT NULL AND wos.IsClosed=0 AND w.IsDeleted=0 AND w.OrganizationID = '".$this->LoginOrganizationID."'";

        } else if(isset($_GET['flag']) && $_GET['flag'] == 'MyOpenWorkOrdersPastDue'){

            $where_str = "w.AssignedTo = $UserID  AND w.EndDate < '".CURRENT_DATE."' AND wos.IsClosed=0 AND w.IsDeleted=0 AND w.OrganizationID = '".$this->LoginOrganizationID."'";

        } else if(isset($_GET['flag']) && $_GET['flag'] == 'AllOpenWorkOrdersPastDue'){

            $where_str = "w.EndDate < '".CURRENT_DATE."' AND wos.IsClosed=0 AND w.IsDeleted=0 AND w.OrganizationID = '".$this->LoginOrganizationID."'"; 

        } else {
            $where_str = "w.IsDeleted = 0 AND w.OrganizationID = '".$this->LoginOrganizationID."'";
        }
        
        if ( $sWhere == "" )
        {
                $sWhere = "WHERE $where_str";
        }
        else
        {
                $sWhere .= "AND $where_str";
        }
        
        /* SQL queries Get data to display */
        $sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
                FROM   $sTable
                $sWhere
                $sOrder
                $sLimit";   
        //echo $sQuery;exit;
        $rResult = $this->db->query($sQuery); 
        $rResult = $rResult->result_array(); 
      
        /* Data set length after filtering */
        $sQuery = "SELECT FOUND_ROWS() as cnt";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iFilteredTotal = $result->cnt;

        /* Total data set length */
        $sQuery = "SELECT COUNT(".$sIndexColumn.") as cnt
                FROM   $sTable  $sWhere";
        $result = $this->db->query($sQuery);
        $result = $result->row();
        $iTotal = $result->cnt;

        /* Output */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );

        foreach ( $rResult as $aRow )
        {
            $row = array();
            for ( $i=0 ; $i<count($bColumns) ; $i++ )
            {
                if($i == 0)
                {
                    $row[] = '<a href="'.SITE_URL.'WorkOrder/Actions/EditWorkOrder/'.$aRow[$bColumns[0]].'"><i class="fa fa-pencil-square-o"></i></a>';
                } 
                else if($i == 1)
                {
                     $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['AssignedTo'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($i == 5)
                {
                     $row[] = '<a href="'.SITE_URL.'Contact/Actions/ContactDetails/'.$aRow['PrimaryContact'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($i == 3)
                {
                     $row[] = '<a href="'.SITE_URL.'Account/Actions/AccountDetails/'.$aRow['AccountID'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($i == 13)
                {
                     $row[] = '<a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$aRow['CreatedBy'].'">'.$aRow[$bColumns[$i]].'</a>';
                } 
                else if($bColumns[$i] == 'Subject' || $bColumns[$i] == 'WorkOrderNo')
                {
                    $row[] = '<a href="'.SITE_URL.'WorkOrder/Actions/WorkOrderDetails/'.$aRow[$bColumns[0]].'">'.$aRow[$bColumns[$i]].'</a>';
                }
                else if ( $bColumns[$i] != ' ' )
                {
                    $row[] = $aRow[$bColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode( $output );
    } 

    /*function SaveWorkOrderFilter($FilterFields, $FilterConditions, $FilterValues)
    {

         if(count($FilterValues) > 0){ 

            $res = $this->db->delete('WorkOrderFilter', array('UserID' => $this->LoginUserID));

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'UserID' => $this->LoginUserID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('WorkOrderFilter', $data);  
                }
            }

            $aColumns = array('a.WorkOrderID','CONCAT(ast.FirstName, " ", ast.LastName)','a.WorkOrderName', 'at.WorkOrderType', 'a.PhoneNo', 'a.BillingCity', 'a.BillingState', 'DATE_FORMAT(a.LastActivityDate, "'.RES_DATE.'")', 'DATE_FORMAT(a.LastServiceDate, "'.RES_DATE.'")','a.AccessNotes', 'CONCAT(pt.FirstName, " ", pt.LastName)','a.PopUpReminder','a.WorkOrderID');            
            $this->db->select('a.WorkOrderID, a.PhoneNo, a.WorkOrderName,  a.BillingState, a.BillingCity, at.WorkOrderType, DATE_FORMAT(a.LastServiceDate, "'.RES_DATE.'") as LastServiceDate, DATE_FORMAT(a.LastActivityDate, "'.RES_DATE.'") as LastActivityDate, CONCAT(ast.FirstName, " ", ast.LastName) as OwnerName, a.PopUpReminder, CONCAT(pt.FirstName, " ", pt.LastName) as PreferredTechnician, a.AccessNotes');
            $this->db->from('WorkOrder a');
            $this->db->join('User ast','ast.UserID = a.AssignedTo', 'left');
            $this->db->join('User pt','pt.UserID = a.PreferredTechnician', 'left');
            $this->db->join('WorkOrderType at','at.WorkOrderTypeID = a.WorkOrderType', 'left');
            $this->db->where(array('a.IsActive' => 1, 'a.IsDeleted' => 0));

            foreach ($FilterValues as $key => $value) {
                if($FilterConditions[$key] == 'Equals'){
                    $this->db->where($FilterFields[$key], $FilterValues[$key]);  
                } else if($FilterConditions[$key] == 'Contains'){
                    $this->db->like($FilterFields[$key], $FilterValues[$key]);   
                } else if($FilterConditions[$key] == 'StartsWith'){
                    $this->db->like($FilterFields[$key], $FilterValues[$key], 'after');   
                } else if($FilterConditions[$key] == 'DoesNotContain'){
                    $this->db->not_like($FilterFields[$key], $FilterValues[$key]);   
                } 
            } 
            $query = $this->db->get(); 

            return $query->result(); 
 
        }
    }*/ 

    function CreateNewView()
    {
        extract($_POST);

        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'WorkOrderViewName' => $WorkOrderViewName,
            'UserID' => $this->LoginUserID,
            'RestrictVisibility' => $RestrictVisibility,
            'SpecifyFieldsDisplay' => implode(",", $SpecifyFieldsDisplay),
            'CreatedDate' => date("Y-m-d H:i:s"),
            'LastModifiedDate' => date("Y-m-d H:i:s")
        );     
        $query = $this->db->insert('WorkOrderView', $data);  
        $WorkOrderViewID = $this->db->insert_id();

        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'WorkOrderViewID' => $WorkOrderViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('WorkOrderFilter', $data);  

                   
                }
            } 
 
        }
        return $WorkOrderViewID;
    }

    function EditCustomView($WorkOrderViewID)
    {  
        extract($_POST);
  
        $data = array(
            'WorkOrderViewName' => $WorkOrderViewName,
            'UserID' => $this->LoginUserID,
            'RestrictVisibility' => $RestrictVisibility,
            'SpecifyFieldsDisplay' => implode(",", $SpecifyFieldsDisplay),
            'CreatedDate' => date("Y-m-d H:i:s"),
            'LastModifiedDate' => date("Y-m-d H:i:s")
        );     
        $query = $this->db->update('WorkOrderView', $data, array('WorkOrderViewID' => $WorkOrderViewID));  

        $this->db->delete('WorkOrderFilter', array('WorkOrderViewID' => $WorkOrderViewID));  
        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'WorkOrderViewID' => $WorkOrderViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('WorkOrderFilter', $data);  
                }
            } 
 
        }
        return $query;
    }

    /*function GetWorkOrderViewData($WorkOrderViewID){

        $query = $this->db->get_where('WorkOrderView', array('WorkOrderViewID' => $WorkOrderViewID, 'OrganizationID' => $this->LoginOrganizationID)); 
        $WorkOrderViewData = $query->row();

        $query = $this->db->get_where('WorkOrderFilter', array('WorkOrderViewID' => $WorkOrderViewID)); 
        $WorkOrderFilterData = $query->result_array();

        $ColName = "w.WorkOrderID, ac.AccountID, pc.ContactID, w.AssignedTo as AssignedToID, w.CreatedBy as CreatedByID, w.LastModifiedBy as LastModifiedByID, w.ParentWorkOrder as ParentWorkOrderID, ";
        if(!empty($WorkOrderViewData->SpecifyFieldsDisplay)){
            $SpecifyFieldsDisplays = explode(",", $WorkOrderViewData->SpecifyFieldsDisplay);
            foreach ($SpecifyFieldsDisplays as $key => $value) {
                if($value == 'AssignedTo'){
                    $ColName .= "CONCAT(ast.FirstName, ' ', ast.LastName) as AssignedTo";
                } else if($value == 'Account'){
                    $ColName .= "ac.AccountName as Account";
                } else if($value == 'WorkOrderType'){
                    $ColName .= "wot.WorkOrderType";
                } else if($value == 'ParentWorkOrder'){
                    $ColName .= "pwo.Subject as ParentWorkOrder";
                } else if($value == 'OrganizationID'){
                    $ColName .= "o.CompanyName as OrganizationID";
                } else if($value == 'WOPriority'){
                    $ColName .= "wop.Priority as WOPriority";
                } else if($value == 'WOCategory'){
                    $ColName .= "woc.CategoryName as WOCategory";
                } else if($value == 'WOStatus'){
                    $ColName .= "wos.Status as WOStatus";
                } else if($value == 'PrimaryContact'){
                    $ColName .= "CONCAT(pc.FirstName, ' ', pc.LastName) as PrimaryContact";
                } else if($value == 'CreatedBy'){
                    $ColName .= "CONCAT(cr.FirstName, ' ', cr.LastName) as CreatedBy";
                } else if($value == 'LastModifiedBy'){
                    $ColName .= "CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedBy";
                } else if($value == 'StartDate'){
                    $ColName .= "DATE_FORMAT(w.StartDate, '".RES_DATETIME."') as StartDate";
                } else if($value == 'EndDate'){
                    $ColName .= "DATE_FORMAT(w.EndDate, '".RES_DATETIME."') as EndDate";
                } else if($value == 'StartTime'){
                    $ColName .= "DATE_FORMAT(w.StartTime, '".RES_TIME."') as StartTime";
                } else if($value == 'EndTime'){
                    $ColName .= "DATE_FORMAT(w.EndTime, '".RES_TIME."') as EndTime";
                } else {
                    $ColName .= "w.".$value;
                }

                if($key != count($SpecifyFieldsDisplays)){
                    $ColName .= ", ";
                }
            }
        }
            
         // Filter WorkOrder Data Start 
            $this->db->select($ColName);
            $this->db->from('WorkOrder w');
            $this->db->join('User ast','ast.UserID = w.AssignedTo', 'left');
            $this->db->join('Account ac','ac.AccountID = w.Account', 'left');
            $this->db->join('WorkOrder pwo','pwo.WorkOrderID = w.ParentWorkOrder', 'left');
            $this->db->join('Organization o','o.OrganizationID = w.OrganizationID', 'left');
            $this->db->join('WOPriority wop','wop.WOPriorityID = w.WOPriority', 'left');
            $this->db->join('WOCategory woc','woc.WOCategoryID = w.WOCategory', 'left');
            $this->db->join('WOStatus wos','wos.WOStatusID = w.WOStatus', 'left');
            $this->db->join('User cr','cr.UserID = w.CreatedBy', 'left');
            $this->db->join('User lmb','lmb.UserID = w.LastModifiedBy', 'left');
            $this->db->join('Contact pc','pc.ContactID = w.PrimaryContact', 'left');
            $this->db->join('WorkOrderType wot','wot.WorkOrderTypeID = w.WorkOrderType', 'left');
            $this->db->where(array('w.IsDeleted' => 0, 'w.OrganizationID' => $this->LoginOrganizationID));

            foreach ($WorkOrderFilterData as $key => $value) {

                $WhereField = "";
                if($value['FilterField'] == 'AssignedTo'){
                    $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                } else if($value['FilterField'] == 'Account') {
                    $WhereField = "ac.AccountName";
                } else if($value['FilterField'] == 'ParentWorkOrder') {
                    $WhereField = "pwo.Subject";
                } else if($value['FilterField'] == 'OrganizationID') {
                    $WhereField = "o.CompanyName";
                } else if($value['FilterField'] == 'WOPriority') {
                    $WhereField = "wop.Priority";
                } else if($value['FilterField'] == 'WOCategory') {
                    $WhereField = "woc.CategoryName";
                } else if($value['FilterField'] == 'WOStatus') {
                    $WhereField = "wos.Status";
                } else if($value['FilterField'] == 'CreatedBy') {
                    $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                } else if($value['FilterField'] == 'LastModifiedBy') {
                    $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                } else if($value['FilterField'] == 'PrimaryContact') {
                    $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                } else if($value['FilterField'] == 'WorkOrderType') {
                    $WhereField = "wot.WorkOrderType";
                } else if($value['FilterField'] == 'StartDate'){
                     $WhereField = "DATE_FORMAT(w.StartDate, '".RES_DATETIME."')";
                } else if($value['FilterField'] == 'EndDate'){
                     $WhereField = "DATE_FORMAT(w.EndDate, '".RES_DATETIME."')";
                } else if($value['FilterField'] == 'StartTime'){
                     $WhereField = "DATE_FORMAT(w.StartTime, '".RES_TIME."')";
                } else if($value['FilterField'] == 'EndTime'){
                     $WhereField = "DATE_FORMAT(w.EndTime, '".RES_TIME."')";
                } else {
                    $WhereField = "w.".$value['FilterField'];
                }

                if($value['FilterCondition'] == 'Equals'){
                    $this->db->where($WhereField.' =',  $value['FilterValue']);  
                } else if($value['FilterCondition'] == 'NotEqualTo'){
                    $this->db->where($WhereField.' !=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'LessThan'){
                    $this->db->where($WhereField.' <', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'GreaterThan'){
                    $this->db->where($WhereField.' >', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'LessOREqualTo'){
                    $this->db->where($WhereField.' <=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'GreaterOREqualTo'){
                    $this->db->where($WhereField.' >=', $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'Contains'){
                    $this->db->like($WhereField, $value['FilterValue']);   
                } else if($value['FilterCondition'] == 'StartsWith'){ 
                    $this->db->like($WhereField, $value['FilterValue'], 'after');   
                } else if($value['FilterCondition'] == 'DoesNotContain'){
                    $this->db->not_like($WhereField, $value['FilterValue']);   
                } 
            } 
            $query = $this->db->get();  

            //echo $this->db->last_query();exit;
            $data['ColumnsNames'] = $SpecifyFieldsDisplays;
            $data['ColumnsValues'] = $query->result();

            return $data; 
    }*/

    function GetWorkOrderViewData($WorkOrderViewID, $rowno, $rowperpage, $sortBy, $order){

        $query = $this->db->get_where('WorkOrderView', array('WorkOrderViewID' => $WorkOrderViewID, 'OrganizationID' => $this->LoginOrganizationID)); 
        $WorkOrderViewData = $query->row();

        $query = $this->db->get_where('WorkOrderFilter', array('WorkOrderViewID' => $WorkOrderViewID)); 
        $WorkOrderFilterData = $query->result_array();

        $query = $this->db->get_where('CustomFields', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0, 'Object' => 'WorkOrder')); 
        
        $CustomFields = $query->result();
        $CustomFieldArr = array();
        $Where = "";
        $FieldName = "";
        $TableAlias = "";
        $CFWhere = "";
        $CFWhere2 = "";
        $SortBy = "";
        $leftJoin = "";
        $selectStr = "";

        $ColName = "w.WorkOrderID, ac.AccountID, pc.ContactID, w.AssignedTo as AssignedToID, w.CreatedBy as CreatedByID, w.LastModifiedBy as LastModifiedByID, w.ParentWorkOrder as ParentWorkOrderID, ";
        if(!empty($WorkOrderViewData->SpecifyFieldsDisplay)){
            $SpecifyFieldsDisplays = explode(",", $WorkOrderViewData->SpecifyFieldsDisplay);

            $tblAlias = "";
            $x = 1;
            foreach($CustomFields as $key => $CustomField){

                if(in_array($CustomField->FieldName, $SpecifyFieldsDisplays)){

                    $CustomFieldArr[] = $CustomField->FieldName;

                    $FieldName .= ", wo".$x.".FieldValue '".$CustomField->FieldName."'";

                    $leftJoin .= " LEFT JOIN WorkOrderCustomFieldValues AS wo".$x."
                    ON wo".$x.".WorkOrderID = wo.WorkOrderID AND wo".$x.".CustomFieldID = ".$CustomField->CustomFieldID;

                    $x++;
                }
            }

            foreach ($SpecifyFieldsDisplays as $key => $value) {
                if($value == 'AssignedTo'){
                    $ColName .= "CONCAT(ast.FirstName, ' ', ast.LastName) as AssignedTo";
                } else if($value == 'Account'){
                    $ColName .= "ac.AccountName as Account";
                } else if($value == 'WorkOrderType'){
                    $ColName .= "wot.WorkOrderType";
                } else if($value == 'ParentWorkOrder'){
                    $ColName .= "pwo.Subject as ParentWorkOrder";
                } else if($value == 'OrganizationID'){
                    $ColName .= "o.CompanyName as OrganizationID";
                } else if($value == 'WOPriority'){
                    $ColName .= "wop.Priority as WOPriority";
                } else if($value == 'WOCategory'){
                    $ColName .= "woc.CategoryName as WOCategory";
                } else if($value == 'WOStatus'){
                    $ColName .= "wos.Status as WOStatus";
                } else if($value == 'PrimaryContact'){
                    $ColName .= "CONCAT(pc.FirstName, ' ', pc.LastName) as PrimaryContact";
                } else if($value == 'CreatedBy'){
                    $ColName .= "CONCAT(cr.FirstName, ' ', cr.LastName) as CreatedBy";
                } else if($value == 'LastModifiedBy'){
                    $ColName .= "CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedBy";
                } else if($value == 'StartDate'){
                    $ColName .= "DATE_FORMAT(w.StartDate, '".RES_DATETIME."') as StartDate";
                } else if($value == 'EndDate'){
                    $ColName .= "DATE_FORMAT(w.EndDate, '".RES_DATETIME."') as EndDate";
                } else if($value == 'StartTime'){
                    $ColName .= "DATE_FORMAT(w.StartTime, '".RES_TIME."') as StartTime";
                } else if($value == 'EndTime'){
                    $ColName .= "DATE_FORMAT(w.EndTime, '".RES_TIME."') as EndTime";
                } else if(!in_array($value, $CustomFieldArr)){
                    $ColName .= "w.".$value;
                } else if(in_array($value, $CustomFieldArr)){
                    $ColName .= "wocf.$value";
                }

                if(($key+1) != count($SpecifyFieldsDisplays)){
                    $ColName .= ", ";
                }
            }
        }

        foreach ($WorkOrderFilterData as $key => $value) {

            $WhereField = "";
            if($value['FilterField'] == 'AssignedTo'){
                $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
            } else if($value['FilterField'] == 'Account') {
                $WhereField = "ac.AccountName";
            } else if($value['FilterField'] == 'ParentWorkOrder') {
                $WhereField = "pwo.Subject";
            } else if($value['FilterField'] == 'OrganizationID') {
                $WhereField = "o.CompanyName";
            } else if($value['FilterField'] == 'WOPriority') {
                $WhereField = "wop.Priority";
            } else if($value['FilterField'] == 'WOCategory') {
                $WhereField = "woc.CategoryName";
            } else if($value['FilterField'] == 'WOStatus') {
                $WhereField = "wos.Status";
            } else if($value['FilterField'] == 'CreatedBy') {
                $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
            } else if($value['FilterField'] == 'LastModifiedBy') {
                $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
            } else if($value['FilterField'] == 'PrimaryContact') {
                $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
            } else if($value['FilterField'] == 'WorkOrderType') {
                $WhereField = "wot.WorkOrderType";
            } else if($value['FilterField'] == 'StartDate'){
                 $WhereField = "DATE_FORMAT(w.StartDate, '".RES_DATETIME."')";
            } else if($value['FilterField'] == 'EndDate'){
                 $WhereField = "DATE_FORMAT(w.EndDate, '".RES_DATETIME."')";
            } else if($value['FilterField'] == 'StartTime'){
                 $WhereField = "DATE_FORMAT(w.StartTime, '".RES_TIME."')";
            } else if($value['FilterField'] == 'EndTime'){
                 $WhereField = "DATE_FORMAT(w.EndTime, '".RES_TIME."')";
            } else if(!in_array($value['FilterField'], $CustomFieldArr)){
                $WhereField = "w.".$value['FilterField'];
            } else if(in_array($value['FilterField'], $CustomFieldArr)){
                $WhereField = "wocf.".$value['FilterField'];
            }

           if($value['FilterCondition'] == 'Equals'){
                $Where .= " AND ".$WhereField." = '".$value['FilterValue']."'";  
            } else if($value['FilterCondition'] == 'NotEqualTo'){
                $Where .= " AND ".$WhereField." != '".$value['FilterValue']."'";   
            } else if($value['FilterCondition'] == 'LessThan'){
                $Where .= " AND ".$WhereField." < '".$value['FilterValue']."'";   
            } else if($value['FilterCondition'] == 'GreaterThan'){
                $Where .= " AND ".$WhereField." > '".$value['FilterValue']."'";   
            } else if($value['FilterCondition'] == 'LessOREqualTo'){
                $Where .= " AND ".$WhereField." <= '".$value['FilterValue']."'";   
            } else if($value['FilterCondition'] == 'GreaterOREqualTo'){
                $Where .= " AND ".$WhereField." >= '".$value['FilterValue']."'";   
            } else if($value['FilterCondition'] == 'Contains'){
                $Where .= " AND ".$WhereField." LIKE '%".$value['FilterValue']."%'";   
            } else if($value['FilterCondition'] == 'StartsWith'){ 
                $Where .= " AND ".$WhereField." LIKE '%".$value['FilterValue']."'";   
            } else if($value['FilterCondition'] == 'DoesNotContain'){
                $Where .= " AND ".$WhereField." NOT LIKE '%".$value['FilterValue']."%'";   
            } 
        }

        if(!empty($sortBy) && !empty($order)){
            if(in_array($sortBy, $CustomFieldArr)){
                $SortBy = "ORDER BY wocf.$sortBy $order";
            } else {
                $SortBy = "ORDER BY $sortBy $order";
            }
        }

        if(!empty($CustomFieldArr)){

            $query = "SELECT $ColName FROM
                (
                    SELECT DISTINCT wo.WorkOrderID $FieldName FROM WorkOrder wo
                    $leftJoin
                )  wocf
                RIGHT JOIN WorkOrder w ON wocf.WorkOrderID = w.WorkOrderID
                LEFT JOIN Account ac ON ac.AccountID = w.Account
                LEFT JOIN User ast ON ast.UserID = w.AssignedTo
                LEFT JOIN WorkOrder pwo ON pwo.WorkOrderID = w.ParentWorkOrder
                LEFT JOIN WOPriority wop ON wop.WOPriorityID = w.WOPriority
                LEFT JOIN WOCategory woc ON woc.WOCategoryID = w.WOCategory
                LEFT JOIN WOStatus wos ON wos.WOStatusID = w.WOStatus
                LEFT JOIN User cr ON cr.UserID = w.CreatedBy
                LEFT JOIN User lmb ON lmb.UserID = w.LastModifiedBy
                LEFT JOIN Contact pc ON pc.ContactID = w.PrimaryContact
                LEFT JOIN WorkOrderType wot ON wot.WorkOrderTypeID = w.WorkOrderType
                LEFT JOIN Organization o ON o.OrganizationID = w.OrganizationID
                WHERE w.OrganizationID = ".$this->LoginOrganizationID." AND w.IsDeleted = 0
                $Where $SortBy";

        } else {

            $query = "SELECT $ColName FROM WorkOrder w
                LEFT JOIN Account ac ON ac.AccountID = w.Account
                LEFT JOIN User ast ON ast.UserID = w.AssignedTo
                LEFT JOIN WorkOrder pwo ON pwo.WorkOrderID = w.ParentWorkOrder
                LEFT JOIN WOPriority wop ON wop.WOPriorityID = w.WOPriority
                LEFT JOIN WOCategory woc ON woc.WOCategoryID = w.WOCategory
                LEFT JOIN WOStatus wos ON wos.WOStatusID = w.WOStatus
                LEFT JOIN User cr ON cr.UserID = w.CreatedBy
                LEFT JOIN User lmb ON lmb.UserID = w.LastModifiedBy
                LEFT JOIN Contact pc ON pc.ContactID = w.PrimaryContact
                LEFT JOIN WorkOrderType wot ON wot.WorkOrderTypeID = w.WorkOrderType
                LEFT JOIN Organization o ON o.OrganizationID = w.OrganizationID
                WHERE w.OrganizationID = ".$this->LoginOrganizationID." AND w.IsDeleted = 0
                $Where $SortBy";
        }
        //echo $query;exit;
        $query1 = $this->db->query($query." LIMIT $rowno,$rowperpage");

        $query2 = $this->db->query($query);
          
        $num_rows = $query2->num_rows();

        $data['ColumnsNames'] = $SpecifyFieldsDisplays;
        $data['ColumnsValues'] = $query1->result();
        $data['TotalRows'] = $num_rows;
        
        return $data; 
    }

    function GetCustomView($WorkOrderViewID)
    {
        $query = $this->db->get_where('WorkOrderView', array('WorkOrderViewID' => $WorkOrderViewID, 'OrganizationID' => $this->LoginOrganizationID)); 
        
        $data['WorkOrderViewData'] = $query->row();

        $query = $this->db->get_where('WorkOrderFilter', array('WorkOrderViewID' => $WorkOrderViewID)); 
        
        $data['WorkOrderFilterData'] = $query->result();

        return $data;
    }

    /*function GetWorkOrderFilter()
    {
        $query = $this->db->get_where('WorkOrderFilter', array('UserID' => $this->LoginUserID)); 
        
        return $query->result();
    }*/

    function GetViews(){

        $query = $this->db->query("SELECT * FROM WorkOrderView WHERE ((RestrictVisibility = 'VisibleOnlyToMe' AND UserID = '".$this->LoginUserID."') OR (RestrictVisibility = 'VisibleToEveryone')) AND OrganizationID = '".$this->LoginOrganizationID."' ORDER BY WorkOrderViewName ASC");
        
        return $query->result();
    }

    function WorkOrderViewFields()
    {
        $query = $this->db->get_where('CustomFields', array('Object' => 'WorkOrder', 'IsDeleted' => 0, 'OrganizationID' => $this->LoginOrganizationID, 'AddToPageLayout' => 1)); 
        
        $CustomFields = $query->result();

        $query = $this->db->get_where('WorkOrderViewFields'); 
        
        $DefaultFields = $query->result();

        return array_merge($DefaultFields, $CustomFields);
    }

    function DeleteCustomView($WorkOrderViewID)
    {
        return $this->db->delete('WorkOrderView', array('WorkOrderViewID' => $WorkOrderViewID)); 
    }

    function CopyCustomView($WorkOrderViewID)
    {
       $query = $this->db->get_where('WorkOrderView', array('WorkOrderViewID' => $WorkOrderViewID)); 
      
        foreach ($query->result() as $row){   
           foreach($row as $key=>$val){ 
                if($key != 'WorkOrderViewID'){    
                    $this->db->set($key, $val); 
                }                
           }
        }
        $this->db->insert('WorkOrderView');
        $NewWorkOrderViewID = $this->db->insert_id();

        $query = $this->db->get_where('WorkOrderFilter', array('WorkOrderViewID' => $WorkOrderViewID)); 
       
        foreach ($query->result() as $row){   
           foreach($row as $key => $val){ 
                if($key == 'WorkOrderViewID'){  
                    $this->db->set($key, $NewWorkOrderViewID);  
                } else if($key != 'WorkOrderFilterID'){    
                    $this->db->set($key, $val); 
                }             
           }
           $this->db->insert('WorkOrderFilter');
        }

        return 1; 
    }

    function RenameCustomView($WorkOrderViewID){

        $data = array(
            'WorkOrderViewName' => $_POST['WorkOrderViewName']
        );

        return $this->db->update('WorkOrderView', $data, array('WorkOrderViewID' => $WorkOrderViewID));  
    }

    function EditSharingCustomView($WorkOrderViewID){

        $data = array(
            'RestrictVisibility' => $_REQUEST['RestrictVisibility']
        );

        return $this->db->update('WorkOrderView', $data, array('WorkOrderViewID' => $WorkOrderViewID));  
    }

    function EditFiltersCustomView($WorkOrderViewID){ 

        extract($_POST);

        $this->db->delete('WorkOrderFilter', array('WorkOrderViewID' => $WorkOrderViewID));  
        if(!empty($FilterValues)){ 

            foreach ($FilterValues as $key => $value) {
                if(!empty($FilterFields[$key]) && !empty($FilterConditions[$key]) && !empty($FilterValues[$key])){
                    $data = array(
                        'WorkOrderViewID' => $WorkOrderViewID,
                        'FilterField' => $FilterFields[$key],
                        'FilterCondition' => $FilterConditions[$key],
                        'FilterValue' => $FilterValues[$key],
                        'CreatedDate' => date("Y-m-d H:i:s"),
                        'LastModifiedDate' => date("Y-m-d H:i:s")
                    );     
                    $query = $this->db->insert('WorkOrderFilter', $data);  
                }
            } 
 
        }
        return $query; 
    }

    function EditDisplayedColumnsCustomView($WorkOrderViewID){

        $data = array(
            'SpecifyFieldsDisplay' => implode(",", $_REQUEST['SpecifyFieldsDisplay'])
        );

        return $this->db->update('WorkOrderView', $data, array('WorkOrderViewID' => $WorkOrderViewID));  
    }
    
}

?>
