<?php
 
class ActionsModel extends MY_Model{

    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }

    function CreateNewFile() {

        extract($_POST); 

        $FILE_PATH = str_replace('{org_id}', $this->LoginOrganizationID, FILE_PATH);

        if(!empty($_FILES["Upload"]['name'][0])){

            $files  = $_FILES;
            $file_count    = count($_FILES['Upload']['name']);
            for($i = 0; $i < $file_count; $i++)
            {
                $_FILES['Upload']['name']        = $files['Upload']['name'][$i];
                $_FILES['Upload']['type']        = $files['Upload']['type'][$i];
                $_FILES['Upload']['tmp_name']    = $files['Upload']['tmp_name'][$i];
                $_FILES['Upload']['error']       = $files['Upload']['error'][$i];
                $_FILES['Upload']['size']        = $files['Upload']['size'][$i];

                $FileName = explode(".", $_FILES['Upload']['name'])[0];
                $config = array(
                 'upload_path' => $FILE_PATH,
                 'file_name' => $FileName.'_'.date('YmdHis'),
                 'allowed_types' => "jpg|png|jpeg|doc|pptx|xlsx|docx|zip|pdf|txt",
                 'overwrite' => FALSE,
                 'max_size' => "25600000"   //'max_height' => "768", 'max_width' => "1024" // Can be set to particular file size , here it is 2 MB(2048 Kb)
                );
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
               
                if($this->upload->do_upload('Upload')){
                    $fileData = $this->upload->data();
                    chmod($FILE_PATH.$fileData['file_name'], 0777);

                    $data = array(
                        'OrganizationID' =>  $this->LoginOrganizationID,
                        'Subject' => $Subject,
                        'RelatedTo' => $RelatedTo,
                        'What' => $What,
                        'AssignedTo' => $AssignedTo,
                        'ContentType' => $fileData['file_type'],
                        'Description' => $Description,
                        'FileName' => $fileData['file_name'],
                        'FileSize' => round($fileData['file_size']/1024, 2)
                    ); 
                    
                    $result = $this->db->insert('File', $data);    
                   //echo $this->db->last_query();exit;
                    $FileID = $this->db->insert_id();

                    MY_Model::insertCreatedModifiedByAndDt($this->LoginUserID,'File', array('FileID' => $FileID));
        
                }
            }
        }

        return $result;
    } 

    function GetRelatedObjData($RelatedTo, $What)
    {
        $query = $this->db->get_where($RelatedTo, array($RelatedTo.'ID' => $What, 'IsDeleted' => 0)); 
        return $query->row();
    }
 
    function SendEmail($RelatedTo, $What)
    { 
        extract($_POST); 

        $UserID = $this->LoginUserID;

        if(!empty($EmailTemplate)){
            $query = $this->db->get_where('EmailTemplates', array('EmailTemplateID' => $EmailTemplate, 'IsDeleted' => 0)); 
            if($query->num_rows() > 0){
                $result = $query->row();
                $NewEmailBody = str_replace("{{EMAIL_CONTAINS}}", $EmailBody, $result->Body);
            }
        } else {
            $NewEmailBody = $EmailBody;
        }
       
        $this->load->library('email');
        $this->email->set_newline("\r\n");
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['smtp_user'] = SMTP_USER;
        $config['smtp_pass'] = SMTP_PASS;
        $config['smtp_crypto'] = SMTP_CRYPTO; 
        $config['protocol'] = PROTOCOL; 
        $config['mailtype'] = 'html'; 
        $config['send_multipart'] = FALSE;
        $this->email->initialize($config);
        $this->email->from(ADMIN_EMAIL); 
        $this->email->to($ToEmailID);
        if(isset($CCEmailID) && !empty($CCEmailID))
            $this->email->cc($CCEmailID);
        if(isset($BCCEmailID) && !empty($BCCEmailID))
            $this->email->bcc($BCCEmailID);
        $this->email->subject($EmailSubject);
        $this->email->message($NewEmailBody);

        $TEMP_PATH = str_replace('{org_id}', $this->LoginOrganizationID, TEMP_PATH);
       
        if(!empty($_FILES["EmailAttachment"]['name'][0])){

            $files  = $_FILES;
            $file_count    = count($_FILES['EmailAttachment']['name']);
            for($i = 0; $i < $file_count; $i++)
            {
                $_FILES['EmailAttachment']['name']        = $files['EmailAttachment']['name'][$i];
                $_FILES['EmailAttachment']['type']        = $files['EmailAttachment']['type'][$i];
                $_FILES['EmailAttachment']['tmp_name']    = $files['EmailAttachment']['tmp_name'][$i];
                $_FILES['EmailAttachment']['error']        = $files['EmailAttachment']['error'][$i];
                $_FILES['EmailAttachment']['size']        = $files['EmailAttachment']['size'][$i];

                $new_image_name = 'file_'. uniqid().'_'.date('YmdHis');
                $config = array(
                 'upload_path' => $TEMP_PATH,
                 'file_name' => $new_image_name,
                 'allowed_types' => "jpg|jpeg|png|doc|pptx|xlsx|docx|zip",
                 'overwrite' => TRUE,
                 'max_size' => "25600000"   //'max_height' => "768", 'max_width' => "1024" // Can be set to particular file size , here it is 2 MB(2048 Kb)
                );
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
               
                if($this->upload->do_upload('EmailAttachment')){
                    $fileData = $this->upload->data();
                    chmod($TEMP_PATH.$fileData['file_name'], 0777);
                    $file_name = $fileData['file_name'];
                    $this->email->attach($TEMP_PATH.$file_name);
                }
            }
        }
         
        if($this->email->send()){

            $RelatedObjData = $this->GetRelatedObjData($RelatedTo, $What);
            if($RelatedTo == 'Contact'){

                $this->CreateEmailTask($EmailSubject, $RelatedTo, $What, $RelatedObjData->AssignedTo, $What, $EmailBody, $ToEmailID, $RelatedObjData->PhoneNo, date("Y-m-d"));

            } else if($RelatedTo == 'WorkOrder'){

                $this->CreateEmailTask($EmailSubject, $RelatedTo, $What, $RelatedObjData->AssignedTo, $RelatedObjData->PrimaryContact, $EmailBody, $ToEmailID, NULL, date("Y-m-d"));

            } else if($RelatedTo == 'Estimate'){

                $this->CreateEmailTask($EmailSubject, $RelatedTo, $What, $RelatedObjData->Owner, $RelatedObjData->Contact, $EmailBody, $ToEmailID, $RelatedObjData->Phone, date("Y-m-d"));
            }
            return 1;
        }

        return 0;
    }

    function CreateEmailTask($Subject, $RelatedTo, $What, $AssignedTo, $Who, $Description, $Email, $Phone, $DueDate){
            $UserID = $this->LoginUserID;

            $data = array(
                'OrganizationID' =>  $this->LoginOrganizationID,
                'Subject' => $Subject,
                'RelatedTo' => $RelatedTo,
                'What' => $What,
                'AssignedTo' => $AssignedTo,
                'Who' => $Who,
                'Description' => $Description,
                'Email' => $Email,
                'Phone' => $Phone, 
                'DueDate' => $DueDate
            ); 
            $this->db->set($data);
            $this->db->set('TaskType',"(SELECT TaskTypeID from TaskType where TaskType like 'Email' AND OrganizationID = '".$this->LoginOrganizationID."' LIMIT 1)", FALSE);
            $this->db->set('TaskStatus',"(SELECT TaskStatusID from TaskStatus where TaskStatus like 'Completed' AND OrganizationID = '".$this->LoginOrganizationID."' LIMIT 1)", FALSE);
            $result = $this->db->insert('Task');    
           
            $TaskID = $this->db->insert_id();

            MY_Model::insertCreatedModifiedByAndDt($UserID,'Task', array('TaskID' => $TaskID));

            return $TaskID;
    }

    function CreateNewNote()
    {
        extract($_POST); 

        $UserID = $this->LoginUserID;
  
        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'Owner' => $UserID,
            'What' => $What,
            'Subject' => $Subject,
            'Body' => $Body,
            'RelatedTo' => $RelatedTo
        );
        $result = $this->db->insert('Note', $data);    
        $NoteID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Note', array('NoteID' => $NoteID));
    
        return $result;
    }

    function GetTemplateDetails($EmailTemplateID)
    {
        $query = $this->db->get_where('EmailTemplates', array('EmailTemplateID' => $EmailTemplateID)); 
        return json_encode($query->row());
    }
}

?>
