<?php
 
class ActionsModel extends MY_Model{

    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }

    function CreateEvent() {
               
        extract($_POST); 

        $UserID = $this->LoginUserID;
    
        $data = array(
            'OrganizationID' =>  $this->LoginOrganizationID,
            'Subject' => $Subject,
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            'AssignedTo' => $AssignedTo,
            'Who' => !empty($Who)?$Who:NULL,
            'Description' => $Description,
            'Email' => $Email,
            'PhoneNo' => $PhoneNo,
            'EventPriority' => $EventPriority,
            'RecurrenceID' => !empty($RecurrenceID)?$RecurrenceID:NULL,
            'IsRecurrence' => isset($IsRecurring)?$IsRecurring:0,
            'IsAllDayEvent' => isset($IsAllDayEvent)?$IsAllDayEvent:0,
            'EventType' => $EventType, 
            'EventStartDate' => !empty($EventStartDate)?date("Y-m-d H:i:s",strtotime($EventStartDate)):NULL,
            'EventEndDate' => !empty($EventEndDate)?date("Y-m-d H:i:s",strtotime($EventEndDate)):NULL,
            'EventStartTime' => !empty($EventStartDate)?date("H:i:s",strtotime($EventStartDate)):NULL,
            'EventEndTime' => !empty($EventEndDate)?date("H:i:s",strtotime($EventEndDate)):NULL
        ); 

        if(isset($IsRecurring) && $IsRecurring == 1 && isset($RecurrenceID) && !empty($RecurrenceID)){

            $EventID = $this->CreateRecurringEvent($RecurrenceID, $data);

        } else {

            $result = $this->db->insert('Event', $data);    
           //echo $this->db->last_query();exit;
            $EventID = $this->db->insert_id();

            MY_Model::insertCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));
        }
    
        return $EventID;
    } 

    function CreateRecurringEvent($WORecurrenceID, $EventData)
    {
        $query = $this->db->get_where('WORecurrence', array('WORecurrenceID' => $WORecurrenceID));  
        $result = $query->row();

        $StartOnDate = date("Y-m-d", strtotime($result->StartOn));
        $EndsAfterOccurrences = isset($result->EndsAfterOccurrences) ? ($result->EndsAfterOccurrences * $result->IntervalEvery) : (100 * $result->IntervalEvery);

        if(isset($result->RepeatEvery) && ($result->RepeatEvery == "Daily" || $result->RepeatEvery == "Monthly" || $result->RepeatEvery == "Yearly")){

            if($result->RepeatEvery == "Daily"){
                $IntervalUnit = 'D'; 
                $IntervalType = 'day'; 
            } else if($result->RepeatEvery == "Monthly"){
                $IntervalUnit = 'M'; 
                $IntervalType = 'months'; 
            } else if($result->RepeatEvery == "Yearly"){
                $IntervalUnit = 'Y'; 
                $IntervalType = 'years'; 
            }

            if($result->Ends == 'On'){
                $EndsOnDate = date("Y-m-d", strtotime("+1 day", strtotime($result->EndsOnDate)));
            } else{
                $EndsOnDate = date('Y-m-d', strtotime("+$EndsAfterOccurrences $IntervalType", strtotime($StartOnDate)));
            }
          
            $start = new DateTime( $StartOnDate );
            $end = new DateTime( $EndsOnDate );
            $interval = new DateInterval('P'.$result->IntervalEvery.''.$IntervalUnit.'');
            $flag = 1;
            $period = new DatePeriod($start, $interval, $end);
            foreach ($period as $date) {
 
                //print $date->format('Y-m-d H:i:s') . '<br/>';
                $StartDateTime = $date->format('Y-m-d').' '.$result->StartTime;
                $EndDateTime = $date->format('Y-m-d').' '.$result->EndTime;
                $RecurringEventData = $this->InsertRecurringEvent($EventData, $StartDateTime, $EndDateTime);
                
                if((isset($result->EndsAfterOccurrences) && $flag == $result->EndsAfterOccurrences) || $flag == 100)
                        break;
                $flag++;
            }

        } else if(isset($result->RepeatEvery) && $result->RepeatEvery == "Weekly"){

            $RepeatOn = explode(",", $result->RepeatOn);
            if($result->Ends == 'On'){
                $EndsOnDate = date("Y-m-d", strtotime($result->EndsOnDate));
            } else{
                $EndsOnDate = date('Y-m-d', strtotime("+$EndsAfterOccurrences weeks", strtotime($StartOnDate)));
            }

            $start = new DateTime( $StartOnDate );
            $end = new DateTime( $EndsOnDate );
            $interval = new DateInterval('P1D');

            $period = new DatePeriod($start, $interval, $end);

            // only trigger every interval weeks...
            $weekInterval = $result->IntervalEvery;

            $fakeWeek = 0;
            $currentWeek = $start->format('W');
            $flag = 1;
            foreach ($period as $date) {

                if ($date->format('W') !== $currentWeek) {
                    $currentWeek = $date->format('W');
                    $fakeWeek++; 
                    //print ' WEEK ' . $currentWeek . '<br/>';
                }

                if ($fakeWeek % $weekInterval !== 0) {
                    continue;
                }

                $dayOfWeek = $date->format('D');

                if (in_array($dayOfWeek, $RepeatOn)) {
                    //print $date->format('Y-m-d H:i:s') . '   ' . $dayOfWeek . '<br/>';
                    $StartDateTime = $date->format('Y-m-d').' '.$result->StartTime;
                    $EndDateTime = $date->format('Y-m-d').' '.$result->EndTime;
                    $RecurringEventData = $this->InsertRecurringEvent($EventData, $StartDateTime, $EndDateTime);

                    if((isset($result->EndsAfterOccurrences) && $flag == $result->EndsAfterOccurrences) || ($flag == 100)){
                        break;
                    }
                    $flag++;
                }
            }
        } /*else if(isset($result->RepeatEvery) && $result->RepeatEvery == "Periodically"){
            
        }*/

        return $RecurringEventData;
    }

    function InsertRecurringEvent($EventData, $StartDateTime, $EndDateTime)
    {

        $UserID = $this->LoginUserID;
    
        $data = array(
            'OrganizationID' =>  $this->LoginOrganizationID,
            'Subject' => $EventData['Subject'],
            'RelatedTo' => $EventData['RelatedTo'],
            'What' => $EventData['What'],
            'AssignedTo' => $EventData['AssignedTo'],
            'Who' => $EventData['Who'],
            'Description' => $EventData['Description'],
            'Email' => $EventData['Email'],
            'PhoneNo' => $EventData['PhoneNo'],
            'EventPriority' => $EventData['EventPriority'],
            'EventType' => $EventData['EventType'], 
            'EventStartDate' => date("Y-m-d H:i:s",strtotime($StartDateTime)),
            'EventEndDate' => date("Y-m-d H:i:s",strtotime($EndDateTime)),
            'EventStartTime' => date("H:i:s",strtotime($StartDateTime)),
            'EventEndTime' => date("H:i:s",strtotime($EndDateTime)),
            'RecurrenceID' => $EventData['RecurrenceID'],
            'IsRecurrence' => $EventData['IsRecurrence'],
            'IsAllDayEvent' => $EventData['IsAllDayEvent'],
        );   
        $result = $this->db->insert('Event', $data);  
        //echo $this->db->last_query(); exit;
        $EventID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));

        return $EventID;
    }

    function EditEvent($EventID) {

        extract($_POST);          

        $UserID = $this->LoginUserID;

        $data = array(
            'OrganizationID' =>  $this->LoginOrganizationID,
            'Subject' => $Subject,
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            'AssignedTo' => $AssignedTo,
            'Who' => !empty($Who)?$Who:NULL,
            'Description' => $Description,
            'Email' => $Email,
            'PhoneNo' => $PhoneNo,
            'EventPriority' => $EventPriority,
            'RecurrenceID' => !empty($RecurrenceID)?$RecurrenceID:NULL,
            'IsRecurrence' => isset($IsRecurring)?$IsRecurring:0,
            'IsAllDayEvent' => isset($IsAllDayEvent)?$IsAllDayEvent:0,
            'EventType' => $EventType, 
            'EventStartDate' => !empty($EventStartDate)?date("Y-m-d H:i:s",strtotime($EventStartDate)):NULL,
            'EventEndDate' => !empty($EventEndDate)?date("Y-m-d H:i:s",strtotime($EventEndDate)):NULL,
            'EventStartTime' => !empty($EventStartDate)?date("H:i:s",strtotime($EventStartDate)):NULL,
            'EventEndTime' => !empty($EventEndDate)?date("H:i:s",strtotime($EventEndDate)):NULL
        ); 
       
        $result = $this->db->update('Event', $data, array('EventID' => $EventID));    
        
        MY_Model::updateCreatedModifiedByAndDt($UserID,'Event', array('EventID' => $EventID));
    
        return $result;
    } 

    function GetEventData($EventID)
    {
        $query = $this->db->select('e.*, e.Subject as Title, CONCAT(ast.FirstName, " ", ast.LastName) as AssignedToName, CONCAT(c.FirstName, " ", c.LastName) as ContactName, ts.EventStatus, ts.EventStatusID, tp.Priority, tt.EventTypeName, tt.EventTypeID, DATE_FORMAT(e.EventStartDate, "'.RES_DATETIME.'") as EventStartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATETIME.'") as EventEndDate, DATE_FORMAT(e.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,DATE_FORMAT(e.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName')
        ->from('Event e')
        ->join('User cb','cb.UserID = e.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = e.LastModifiedBy', 'left')
        ->join('User ast','ast.UserID = e.AssignedTo', 'left')
        ->join('Contact c','c.ContactID = e.Who', 'left')
        ->join('EventStatus ts','ts.EventStatusID = e.EventStatus', 'left')
        ->join('EventPriority tp','tp.EventPriorityID = e.EventPriority', 'left')
        ->join('EventType tt','tt.EventTypeID = e.EventType', 'left')
        ->where(array('e.OrganizationID' => $this->LoginOrganizationID, 'e.EventID' => $EventID)) 
        ->get();
        
        $data['EventData'] = $query->row();
        
        if(isset($data['EventData'])){
            $RelatedObjData = MY_Model::getRelatedData();
            $RelatedObjData = $RelatedObjData[$data['EventData']->RelatedTo][$data['EventData']->What];
            $data['EventData']->RelatedObjNo = $RelatedObjData->Name;
        }

        return $data;
    }

    function SaveRecurrenceSetting()
    { 
        extract($_POST);

        if(isset($WORecurrenceID) && !empty($WORecurrenceID))
        {
            $data = array(
                'StartOn' => date("Y-m-d",strtotime($StartOn)),
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'RepeatOn' => ($RepeatEvery=='Weekly')?implode(',', $RepeatOn):NULL,
                'Ends' => $Ends,
                'EndsOnDate' => ($Ends=='On')?date("Y-m-d",strtotime($EndsOnDate)):NULL,
                'EndsAfterOccurrences' => ($Ends=='After')?$EndsAfterOccurrences:NULL,
                'StartTime' => date("H:i:s",strtotime($StartTime)),
                'EndTime' => date("H:i:s",strtotime($EndTime)),
                'LastModifiedDate' => date("Y-m-d H:i:s")
            ); 
            $result = $this->db->update('WORecurrence', $data, array('WORecurrenceID'=>$WORecurrenceID));    

        } else {
            $data = array(
                'StartOn' => date("Y-m-d",strtotime($StartOn)),
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'RepeatOn' => ($RepeatEvery=='Weekly')?implode(',', $RepeatOn):NULL,
                'Ends' => $Ends,
                'EndsOnDate' => ($Ends=='On')?date("Y-m-d",strtotime($EndsOnDate)):NULL,
                'EndsAfterOccurrences' => ($Ends=='After')?$EndsAfterOccurrences:NULL,
                'StartTime' => date("H:i:s",strtotime($StartTime)),
                'EndTime' => date("H:i:s",strtotime($EndTime)),
                'CreatedDate' => date("Y-m-d H:i:s"),
                'LastModifiedDate' => date("Y-m-d H:i:s")
            ); 
            $result = $this->db->insert('WORecurrence', $data);    
            $WORecurrenceID = $this->db->insert_id();
        }
        return $WORecurrenceID;
    }

    function removeRecurrenceSetting()
    {
        $WORecurrenceID = $_REQUEST['WORecurrenceID'];
        $WorkOrderID = $_REQUEST['WorkOrderID'];
        $this->db->update('WorkOrder',  array('IsRecurring' => 0), array('WorkOrderID' => $WorkOrderID));  
        return $this->db->delete('WORecurrence', array('WORecurrenceID' => $WORecurrenceID));
    }

    function GetContacts()
    { 
        $query = $this->db->get_where('Contact', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetEventTypes()
    { 
        $query = $this->db->get_where('EventType', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetEventStatus()
    { 
        $query = $this->db->get_where('EventStatus', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetEventPriority()
    { 
        $query = $this->db->get_where('EventPriority', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function DeleteEvent($EventID)
    {
        return $this->db->update('Event', array('IsDeleted' => 1), array('EventID' => $EventID));  
    }
}

?>
