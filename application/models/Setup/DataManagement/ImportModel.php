<?php
class ImportModel extends MY_Model{

    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }

    function UploadFile() {

        $TEMP_PATH = str_replace('{org_id}', $this->LoginOrganizationID, TEMP_PATH);

        extract($_POST); 
       
        if(isset($_FILES['UploadedFile']['name'])){
            $new_image_name = 'file_'. uniqid().'_'.date('YmdHis');
            $config = array(
             'upload_path' => $TEMP_PATH,
             'file_name' => $new_image_name, 
             'allowed_types' => "csv|xlsx|xls",
             'overwrite' => TRUE
             //'max_size' => "1024000"   'max_height' => "768", 'max_width' => "1024" // Can be set to particular file size , here it is 1 MB(1024 Kb)
            );
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if($this->upload->do_upload('UploadedFile')){
                $fileData = $this->upload->data();
                chmod($TEMP_PATH.$fileData['file_name'], 0777);
                $FileName = $fileData['file_name'];

                $data['FileName'] = $FileName;
                $data['Object'] = $Object;
                return $data;

            } else {
                return $this->upload->display_errors();
            }
        }
    } 

    function GetWorkSheetFromSpreadsheet($FileName){

        $TEMP_PATH = str_replace('{org_id}', $this->LoginOrganizationID, TEMP_PATH);

        require 'vendor/autoload.php';
        $FileExt = explode(".", $FileName)[1];
        if($FileExt == 'xlsx')
            $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
        else if($FileExt == 'xls')
            $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xls');
        else if($FileExt == 'csv')
            $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Csv');

        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($TEMP_PATH.$FileName); // File Name

        $WorkSheet = $objPHPExcel->setActiveSheetIndex(0);  // ->toArray()

        return $WorkSheet;
    }

    function GetObjFields($Object){
        $query = $this->db->get($Object.'ViewFields'); 
        return $query->result();
    }

    function FetchDataFromExcel($Object, $FileName){

        $WorkSheet = $this->GetWorkSheetFromSpreadsheet($FileName);

        $Columns = array();

        if($Object == 'Account'){
            $ObjectFields = array('AccountType','AccountName','AssignedTo','PreferredTechnician','PrimaryContact','BillingAddress','BillingCity','BillingState','BillingCountry','BillingPostalCode','Description','Notes','PhoneNo','ShippingAddress','ShippingCity','ShippingState','ShippingCountry','ShippingPostalCode','AccessNotes','PopUpReminder');

        } else if($Object == 'WorkOrder'){
             $ObjectFields = array('Account','AssignedTo','PrimaryContact','Address','City','State','Country','PostalCode','Description','StartDate','EndDate','StartTime','EndTime','WorkOrderType','WOPriority','WOStatus','WOCategory','Subject','PopUpReminder');

        } else if($Object == 'Contact'){
             $ObjectFields = array('Title','Salutation','Name','FirstName','LastName','Email','EmailOptOut','PhoneNo','MobileNo','AssignedTo','Account','BirthDate','DoNotCall','MailingAddress','MailingCity','MailingState','MailingPostalCode','MailingCountry','LeadSource','Notes');

        } else if($Object == 'Invoice'){
             $ObjectFields = array('WorkOrder','Account','Contact','AssignedTo','Address','City','State','Country','PostalCode','Description','InvoiceStatus','InvoiceDate','DueDate','PaymentTerms','AdditionalInformation');

        } else if($Object == 'Estimate'){
             $ObjectFields = array('EstimateName','Contact','Account','Owner','BillingAddress','BillingCity','BillingState','BillingCountry','BillingPostalCode','BillingName','Description','ExpirationDate','Phone','Email','EstimateToAddress','EstimateToName','ShippingAddress','ShippingCity','ShippingState','ShippingCountry','ShippingPostalCode','ShippingName','EstimateStatus');

        } else if($Object == 'Task'){
             $ObjectFields = array('Subject','RelatedTo','What','AssignedTo','Who','Description','Email','TaskStatus','Phone','TaskPriority','Description','DueDate','TaskType');
        }

        foreach ($ObjectFields as $key => $ObjectField) {
            $ExcelColumns[$key+1] = $WorkSheet->getCellByColumnAndRow(($key+1), 1)->getValue(); 
            $DBColumns[$key+1] = $ObjectField; 

            $ExcelColumnsForCheck[] = preg_replace('/\s*/', '', $WorkSheet->getCellByColumnAndRow(($key+1), 1)->getValue()); 
        }

        $count = count(array_intersect($ExcelColumnsForCheck, $DBColumns));
       
        if($count < 10){
            return 0;
        }
        
        /*$query = $this->db->get($Object.'ViewFields'); 
        $DBColumnNames = $query->result();
        foreach ($DBColumnNames as $key => $DBColumnName) {
            $ExcelColumns[$key+1] = $WorkSheet->getCellByColumnAndRow(($key+1), 1)->getValue(); 
            $DBColumns[$key+1] = $DBColumnName->FieldName; 
        }*/

        $data['ExcelColumns'] = $ExcelColumns;
        $data['DBColumns'] = $DBColumns;
        $data['Object'] = $Object;
        $data['FileName'] = $FileName;

        return $data;
    }

    function ImportData(){

        extract($_POST); 

        /* Get All User IDs by Name  */
        $AssignedToArr = array();
        $Users = MY_Model::getAllUsers(); 
        foreach ($Users as $key => $AssignedTo) {
            $AssignedToArr[preg_replace('/\s*/', '', strtoupper($AssignedTo->FirstName.$AssignedTo->LastName))] = $AssignedTo->UserID;
        }

        $ContactArr = array();
        $query = $this->db->get_where('Contact', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0, 'IsActive' => 1)); 
        $Contacts = $query->result();   
        foreach ($Contacts as $key => $Contact) {
            $ContactArr[preg_replace('/\s*/', '', strtoupper($Contact->FirstName.$Contact->LastName))] = $Contact->ContactID;
        }

        if($Object == 'Account')
            return $this->ImportDataInAccount($FileName, $AssignedToArr, $ContactArr);
        else if($Object == 'WorkOrder')
            return $this->ImportDataInWorkOrder($FileName, $AssignedToArr, $ContactArr);
        else if($Object == 'Contact')
            return $this->ImportDataInContact($FileName, $AssignedToArr);
        else if($Object == 'Invoice')
            return $this->ImportDataInInvoice($FileName, $AssignedToArr, $ContactArr);
        else if($Object == 'Estimate')
            return $this->ImportDataInEstimate($FileName, $AssignedToArr, $ContactArr);
        else if($Object == 'Task')
            return $this->ImportDataInTask($FileName, $AssignedToArr, $ContactArr);
    }

    function ImportDataInAccount($FileName, $AssignedToArr, $ContactArr)
    {
        $UserID = $this->LoginUserID;

        extract($_POST); 

        $RequiredColumns = $this->GetRequiredColumnsArray('Account');
        
        $result = array_diff($RequiredColumns, $DBColumnNames);
        if(!empty($result)){
            return 2;
        }
        $WorkSheet = $this->GetWorkSheetFromSpreadsheet($FileName);
        $highestRow = $WorkSheet->getHighestRow();

        /* Get All Account Type IDs by Name  */
        $AccountTypeArr = array();
        $query = $this->db->get_where('AccountType', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        $AccountTypes = $query->result();
        foreach ($AccountTypes as $key => $AccountType) {
            $AccountTypeArr[preg_replace('/\s*/', '', strtoupper($AccountType->AccountType))] = $AccountType->AccountTypeID;
        }

        for ($row = 2; $row <= $highestRow; ++$row) {

            foreach ($ExcelColumnKeys as $key => $ExcelColumnKey) {

                $ExcelCellVal = $WorkSheet->getCellByColumnAndRow($ExcelColumnKey, $row)->getValue();

                $ExcelCellValCheck = preg_replace('/\s*/', '', strtoupper($ExcelCellVal));

                if($DBColumnNames[$ExcelColumnKey] == 'AccountType')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($AccountTypeArr[$ExcelCellValCheck])?$AccountTypeArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'AssignedTo')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($AssignedToArr[$ExcelCellValCheck])?$AssignedToArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'PrimaryContact')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($ContactArr[$ExcelCellValCheck])?$ContactArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'PreferredTechnician')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($AssignedToArr[$ExcelCellValCheck])?$AssignedToArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] != 'UNMAPPED')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($ExcelCellVal)?$ExcelCellVal:NULL; 
            } 
            $data['OrganizationID'] = $this->LoginOrganizationID; 

            $final_data[] = $data;
        }

        if(!empty($final_data)){

            $result = $this->db->insert_batch('Account', $final_data); 

            $first_id = $this->db->insert_id();

            for ($a = 0;$a < $result; $a++){

                $AccountID = $first_id;

                $AccountNo = MY_Controller::genCode('AC', $AccountID);
                $this->db->update('Account', array('AccountNo' => $AccountNo), array('AccountID' => $AccountID));  

                MY_Model::insertCreatedModifiedByAndDt($UserID,'Account', array('AccountID' => $AccountID));

                $first_id += 1;
            }

            return 1;

        } else {

            return 0;
        }

        return $result;
    }

    function ImportDataInWorkOrder($FileName, $AssignedToArr, $ContactArr)
    {
        $UserID = $this->LoginUserID;

        extract($_POST); 

        $RequiredColumns = $this->GetRequiredColumnsArray('WorkOrder');

        $result = array_diff($RequiredColumns, $DBColumnNames);
        if(!empty($result)){
            return 2;
        }
        $WorkSheet = $this->GetWorkSheetFromSpreadsheet($FileName);
        $highestRow = $WorkSheet->getHighestRow();

        /* Get All Work Order Type IDs by Name  */
        $WorkOrderTypeArr = array();
        $query = $this->db->get_where('WorkOrderType', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        $WorkOrderTypes = $query->result();
        foreach ($WorkOrderTypes as $key => $WorkOrderType) {
            $WorkOrderTypeArr[preg_replace('/\s*/', '', strtoupper($WorkOrderType->WorkOrderType))] = $WorkOrderType->WorkOrderTypeID;
        }

        /* Get All Work Order Type IDs by Name  */
        $WOPriorityArr = array(); 
        $query = $this->db->get_where('WOPriority', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        $WOPriorities = $query->result();
        foreach ($WOPriorities as $key => $WOPriority) {
            $WOPriorityArr[preg_replace('/\s*/', '', strtoupper($WOPriority->Priority))] = $WOPriority->WOPriorityID;
        }

        /* Get All Work Order Type IDs by Name  */
        $WOStatusArr = array(); 
        $query = $this->db->get_where('WOStatus', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        $WOStatus = $query->result();
        foreach ($WOStatus as $key => $Status) {
            $WOStatusArr[preg_replace('/\s*/', '', strtoupper($Status->Status))] = $Status->WOStatusID;
        }

        /* Get All Work Order Type IDs by Name  */
        $WOCategoryArr = array(); 
        $query = $this->db->get_where('WOCategory', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        $WOCategory = $query->result();
        foreach ($WOCategory as $key => $Category) {
            $WOCategoryArr[preg_replace('/\s*/', '', strtoupper($Category->CategoryName))] = $Category->WOCategoryID;
        }

        /* Get All Work Order Type IDs by Name  */
        $AccountArr = array(); 
        $query = $this->db->get_where('Account', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        $Accounts = $query->result();
        foreach ($Accounts as $key => $Account) {
            $AccountArr[preg_replace('/\s*/', '', strtoupper($Account->AccountName))] = $Account->AccountID;
        }
        $final_data = array();
        for ($row = 2; $row <= $highestRow; ++$row) {

            foreach ($ExcelColumnKeys as $key => $ExcelColumnKey) {

                $ExcelCellVal = $WorkSheet->getCellByColumnAndRow($ExcelColumnKey, $row)->getValue();

                $ExcelCellValCheck = preg_replace('/\s*/', '', strtoupper($ExcelCellVal));

                if($DBColumnNames[$ExcelColumnKey] == 'WorkOrderType')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($WorkOrderTypeArr[$ExcelCellValCheck])?$WorkOrderTypeArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'AssignedTo')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($AssignedToArr[$ExcelCellValCheck])?$AssignedToArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'Account')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($AccountArr[$ExcelCellValCheck])?$AccountArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'PrimaryContact'){
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($ContactArr[$ExcelCellValCheck])?$ContactArr[$ExcelCellValCheck]:NULL; 
                }
                else if($DBColumnNames[$ExcelColumnKey] == 'WOPriority')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($WOPriorityArr[$ExcelCellValCheck])?$WOPriorityArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'WOCategory')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($WOCategoryArr[$ExcelCellValCheck])?$WOCategoryArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'WOStatus')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($WOStatusArr[$ExcelCellValCheck])?$WOStatusArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'StartTime' || $DBColumnNames[$ExcelColumnKey] == 'EndTime')
                    $data[$DBColumnNames[$ExcelColumnKey]] = !empty($ExcelCellVal)?date("H:i:s",strtotime($ExcelCellVal)):NULL;
                else if($DBColumnNames[$ExcelColumnKey] == 'StartDate' || $DBColumnNames[$ExcelColumnKey] == 'EndDate'){
                    $data[$DBColumnNames[$ExcelColumnKey]] = !empty($ExcelCellVal)?date("Y-m-d H:i:s",strtotime($ExcelCellVal)):NULL;
                }
                else if($DBColumnNames[$ExcelColumnKey] == 'WOStatus')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($WOStatusArr[$ExcelCellValCheck])?$WOStatusArr[$ExcelCellValCheck]:NULL;
                else if($DBColumnNames[$ExcelColumnKey] != 'UNMAPPED')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($ExcelCellVal)?$ExcelCellVal:NULL; 
            } 
            $data['OrganizationID'] = $this->LoginOrganizationID; 

            $final_data[] = $data;
        }

        if(!empty($final_data)){

            $result = $this->db->insert_batch('WorkOrder', $final_data); 

            $first_id = $this->db->insert_id();

            for ($a = 0;$a < $result; $a++){

                $WorkOrderID = $first_id;

                $WorkOrderNo = MY_Controller::genCode('WO', $WorkOrderID);
                $this->db->update('WorkOrder', array('WorkOrderNo' => $WorkOrderNo), array('WorkOrderID' => $WorkOrderID));  

                MY_Model::insertCreatedModifiedByAndDt($UserID,'WorkOrder', array('WorkOrderID' => $WorkOrderID));

                $first_id += 1;
            }

            return 1;

        } else {

            return 0;
        }
    }

    function ImportDataInContact($FileName, $AssignedToArr)
    {
        $UserID = $this->LoginUserID;

        extract($_POST); 

        $RequiredColumns = $this->GetRequiredColumnsArray('Contact');

        $result = array_diff($RequiredColumns, $DBColumnNames);
        if(!empty($result)){
            return 2;
        }
        $WorkSheet = $this->GetWorkSheetFromSpreadsheet($FileName);
        $highestRow = $WorkSheet->getHighestRow();
 
        /* Get All Account IDs by Name  */
        $AccountArr = array();
        $query = $this->db->get_where('Account', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        $Accounts = $query->result();
        foreach ($Accounts as $key => $Account) {
            $AccountArr[preg_replace('/\s*/', '', strtoupper($Account->AccountName))] = $Account->AccountID;
        }

         /* Get All Lead Source IDs by Name  */
        $LeadSourceArr = array();
        $query = $this->db->get_where('LeadSource', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        $LeadSources = $query->result();
        foreach ($LeadSources as $key => $LeadSource) {
            $LeadSourceArr[preg_replace('/\s*/', '', strtoupper($LeadSource->LeadSource))] = $LeadSource->LeadSourceID;
        }

        $final_data = array();
        for ($row = 2; $row <= $highestRow; ++$row) {

            foreach ($ExcelColumnKeys as $key => $ExcelColumnKey) {

                $ExcelCellVal = $WorkSheet->getCellByColumnAndRow($ExcelColumnKey, $row)->getValue();

                $ExcelCellValCheck = preg_replace('/\s*/', '', strtoupper($ExcelCellVal));

                if($DBColumnNames[$ExcelColumnKey] == 'Account')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($AccountArr[$ExcelCellValCheck])?$AccountArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'AssignedTo')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($AssignedToArr[$ExcelCellValCheck])?$AssignedToArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'LeadSource')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($LeadSourceArr[$ExcelCellValCheck])?$LeadSourceArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'BirthDate')
                    $data[$DBColumnNames[$ExcelColumnKey]] = !empty($ExcelCellVal)?date("Y-m-d",strtotime($ExcelCellVal)):NULL;
                else if($DBColumnNames[$ExcelColumnKey] != 'UNMAPPED')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($ExcelCellVal)?$ExcelCellVal:NULL; 
            } 
            $data['OrganizationID'] = $this->LoginOrganizationID; 

            $final_data[] = $data;
        }

        if(!empty($final_data)){

            $result = $this->db->insert_batch('Contact', $final_data); 

            $first_id = $this->db->insert_id();

            for ($a = 0;$a < $result; $a++){

                $ContactID = $first_id;

                $ContactNo = MY_Controller::genCode('CT', $ContactID);
                $this->db->update('Contact', array('ContactNo' => $ContactNo), array('ContactID' => $ContactID));  

                MY_Model::insertCreatedModifiedByAndDt($UserID,'Contact', array('ContactID' => $ContactID));

                $first_id += 1;
            }

            return 1;

        } else {

            return 0;
        }
    }

    function ImportDataInInvoice($FileName, $AssignedToArr, $ContactArr)
    {
        $UserID = $this->LoginUserID;

        extract($_POST); 

        $RequiredColumns = $this->GetRequiredColumnsArray('Invoice');

        $result = array_diff($RequiredColumns, $DBColumnNames);
        if(!empty($result)){
            return 2;
        }
        $WorkSheet = $this->GetWorkSheetFromSpreadsheet($FileName);
        $highestRow = $WorkSheet->getHighestRow();
 
        /* Get All Account IDs by Name  */
        $AccountArr = array();
        $query = $this->db->get_where('Account', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        $Accounts = $query->result();
        foreach ($Accounts as $key => $Account) {
            $AccountArr[preg_replace('/\s*/', '', strtoupper($Account->AccountName))] = $Account->AccountID;
        }

        /* Get All Work Order IDs by Name  */
        $WorkOrderArr = array();
        $query = $this->db->get_where('WorkOrder', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        $WorkOrders = $query->result();
        foreach ($WorkOrders as $key => $WorkOrder) {
            $WorkOrderArr[preg_replace('/\s*/', '', strtoupper($WorkOrder->Subject))] = $WorkOrder->WorkOrderID;
        }

        /* Get All Invoice Status IDs by Name  */
        $InvoiceStatusArr = array();
        $query = $this->db->get_where('InvoiceStatus', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        $InvoiceStatus = $query->result();
        foreach ($InvoiceStatus as $key => $Status) {
            $InvoiceStatusArr[preg_replace('/\s*/', '', strtoupper($Status->InvoiceStatus))] = $Status->InvoiceStatusID;
        }

        /* Get All Invoice Payment Terms IDs by Name  */
        $InvoicePaymentTermsArr = array();
        $query = $this->db->get_where('InvoicePaymentTerm', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        $InvoicePaymentTerms = $query->result();
        foreach ($InvoicePaymentTerms as $key => $InvoicePaymentTerm) {
            $InvoicePaymentTermsArr[preg_replace('/\s*/', '', strtoupper($InvoicePaymentTerm->PaymentTerms))] = $InvoicePaymentTerm->InvoicePaymentTermID;
        }

        $final_data = array();
        for ($row = 2; $row <= $highestRow; ++$row) {

            foreach ($ExcelColumnKeys as $key => $ExcelColumnKey) {

                $ExcelCellVal = $WorkSheet->getCellByColumnAndRow($ExcelColumnKey, $row)->getValue();

                $ExcelCellValCheck = preg_replace('/\s*/', '', strtoupper($ExcelCellVal));

                if($DBColumnNames[$ExcelColumnKey] == 'WorkOrder')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($WorkOrderArr[$ExcelCellValCheck])?$WorkOrderArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'AssignedTo')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($AssignedToArr[$ExcelCellValCheck])?$AssignedToArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'Contact')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($ContactArr[$ExcelCellValCheck])?$ContactArr[$ExcelCellValCheck]:NULL;
                else if($DBColumnNames[$ExcelColumnKey] == 'Account')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($AccountArr[$ExcelCellValCheck])?$AccountArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'PaymentTerms')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($InvoicePaymentTermsArr[$ExcelCellValCheck])?$InvoicePaymentTermsArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'InvoiceStatus')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($InvoiceStatusArr[$ExcelCellValCheck])?$InvoiceStatusArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'InvoiceDate')
                    $data[$DBColumnNames[$ExcelColumnKey]] = !empty($ExcelCellVal)?date("Y-m-d H:i:s",strtotime($ExcelCellVal)):NULL;
                else if($DBColumnNames[$ExcelColumnKey] == 'DueDate')
                    $data[$DBColumnNames[$ExcelColumnKey]] = !empty($ExcelCellVal)?date("Y-m-d H:i:s",strtotime($ExcelCellVal)):NULL;
                else if($DBColumnNames[$ExcelColumnKey] != 'UNMAPPED')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($ExcelCellVal)?$ExcelCellVal:NULL; 
            } 
            $data['OrganizationID'] = $this->LoginOrganizationID; 

            $final_data[] = $data;
        }

        if(!empty($final_data)){

            $result = $this->db->insert_batch('Invoice', $final_data); 

            $first_id = $this->db->insert_id();

            for ($a = 0;$a < $result; $a++){

                $InvoiceID = $first_id;

                $InvoiceNo = MY_Controller::genCode('IN', $InvoiceID);
                $this->db->update('Invoice', array('InvoiceNo' => $InvoiceNo), array('InvoiceID' => $InvoiceID));  

                MY_Model::insertCreatedModifiedByAndDt($UserID,'Invoice', array('InvoiceID' => $InvoiceID));

                $first_id += 1;
            }

            return 1;

        } else {

            return 0;
        }
    }

    function ImportDataInEstimate($FileName, $OwnerArr, $ContactArr)
    {
        $UserID = $this->LoginUserID;

        extract($_POST); 
 
        $RequiredColumns = $this->GetRequiredColumnsArray('Estimate');

        $result = array_diff($RequiredColumns, $DBColumnNames);
        if(!empty($result)){
            return 2;
        }
        $WorkSheet = $this->GetWorkSheetFromSpreadsheet($FileName);
        $highestRow = $WorkSheet->getHighestRow();
 
        /* Get All Account IDs by Name  */
        $AccountArr = array();
        $query = $this->db->get_where('Account', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        $Accounts = $query->result();
        foreach ($Accounts as $key => $Account) {
            $AccountArr[preg_replace('/\s*/', '', strtoupper($Account->AccountName))] = $Account->AccountID;
        }

        /* Get All Invoice Status IDs by Name  */
        $EstimateStatusArr = array();
        $query = $this->db->get_where('EstimateStatus', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        $EstimateStatus = $query->result();
        foreach ($EstimateStatus as $key => $Status) {
            $EstimateStatusArr[preg_replace('/\s*/', '', strtoupper($Status->Status))] = $Status->EstimateStatusID;
        }

        $final_data = array();
        for ($row = 2; $row <= $highestRow; ++$row) {

            foreach ($ExcelColumnKeys as $key => $ExcelColumnKey) {

                $ExcelCellVal = $WorkSheet->getCellByColumnAndRow($ExcelColumnKey, $row)->getValue();

                $ExcelCellValCheck = preg_replace('/\s*/', '', strtoupper($ExcelCellVal));

                if($DBColumnNames[$ExcelColumnKey] == 'Contact')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($ContactArr[$ExcelCellValCheck])?$ContactArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'Account')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($AccountArr[$ExcelCellValCheck])?$AccountArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'Owner')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($OwnerArr[$ExcelCellValCheck])?$OwnerArr[$ExcelCellValCheck]:NULL;
                else if($DBColumnNames[$ExcelColumnKey] == 'EstimateStatus')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($EstimateStatusArr[$ExcelCellValCheck])?$EstimateStatusArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'ExpirationDate')
                    $data[$DBColumnNames[$ExcelColumnKey]] = !empty($ExcelCellVal)?date("Y-m-d",strtotime($ExcelCellVal)):NULL;
                else if($DBColumnNames[$ExcelColumnKey] != 'UNMAPPED')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($ExcelCellVal)?$ExcelCellVal:NULL; 
            } 
            $data['OrganizationID'] = $this->LoginOrganizationID; 

            $final_data[] = $data;
        }

        if(!empty($final_data)){

            $result = $this->db->insert_batch('Estimate', $final_data); 

            $first_id = $this->db->insert_id();

            for ($a = 0;$a < $result; $a++){

                $EstimateID = $first_id;

                $EstimateNo = MY_Controller::genCode('ET', $EstimateID);
                $this->db->update('Estimate', array('EstimateNo' => $EstimateNo), array('EstimateID' => $EstimateID));  

                MY_Model::insertCreatedModifiedByAndDt($UserID,'Estimate', array('EstimateID' => $EstimateID));

                $first_id += 1;
            }

            return 1;

        } else {

            return 0;
        }
    }
 
    function ImportDataInTask($FileName, $AssignedToArr, $ContactArr)
    {
        $UserID = $this->LoginUserID;

        extract($_POST); 

        $RequiredColumns = $this->GetRequiredColumnsArray('Task');

        $result = array_diff($RequiredColumns, $DBColumnNames);

        if(!empty($result)){
            return 2;
        }
        $WorkSheet = $this->GetWorkSheetFromSpreadsheet($FileName);
        $highestRow = $WorkSheet->getHighestRow();

        $WhatArr = MY_Model::getRelatedDataByName();

        /* Get All Task Type IDs by Name  */
        $TaskTypesArr = array();
        $query = $this->db->get_where('TaskType', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        $TaskTypes = $query->result();
        foreach ($TaskTypes as $key => $TaskType) {
            $TaskTypesArr[preg_replace('/\s*/', '', strtoupper($TaskType->TaskType))] = $TaskType->TaskTypeID;
        }

        /* Get All Task Status IDs by Name  */
        $TaskStatusArr = array();
        $query = $this->db->get_where('TaskStatus', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        $TaskStatus = $query->result();
        foreach ($TaskStatus as $key => $Status) {
            $TaskStatusArr[preg_replace('/\s*/', '', strtoupper($Status->TaskStatus))] = $Status->TaskStatusID;
        }

        /* Get All Task Priority IDs by Name  */
        $PrioritiesArr = array();
        $query = $this->db->get_where('TaskPriority', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        $TaskPriority = $query->result();
        foreach ($TaskPriority as $key => $Priority) {
            $PrioritiesArr[preg_replace('/\s*/', '', strtoupper($Priority->Priority))] = $Priority->TaskPriorityID;
        }

        $final_data = array();
        for ($row = 2; $row <= $highestRow; ++$row) {

            foreach ($ExcelColumnKeys as $key => $ExcelColumnKey) {

                $ExcelCellVal = $WorkSheet->getCellByColumnAndRow($ExcelColumnKey, $row)->getValue();

                $ExcelCellValCheck = preg_replace('/\s*/', '', strtoupper($ExcelCellVal));

                if($DBColumnNames[$ExcelColumnKey] == 'TaskType')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($TaskTypesArr[$ExcelCellValCheck])?$TaskTypesArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'TaskPriority')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($PrioritiesArr[$ExcelCellValCheck])?$PrioritiesArr[$ExcelCellValCheck]:NULL;
                else if($DBColumnNames[$ExcelColumnKey] == 'TaskStatus')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($TaskStatusArr[$ExcelCellValCheck])?$TaskStatusArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'What'){ 
                    if(!empty($data['RelatedTo'])){
                        $data[$DBColumnNames[$ExcelColumnKey]] = isset($WhatArr[$data['RelatedTo']][$ExcelCellValCheck]) ? $WhatArr[$data['RelatedTo']][$ExcelCellValCheck]->ID:NULL;
                    } 
                }
                else if($DBColumnNames[$ExcelColumnKey] == 'Who')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($ContactArr[$ExcelCellValCheck])?$ContactArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'AssignedTo')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($AssignedToArr[$ExcelCellValCheck])?$AssignedToArr[$ExcelCellValCheck]:NULL; 
                else if($DBColumnNames[$ExcelColumnKey] == 'DueDate')
                    $data[$DBColumnNames[$ExcelColumnKey]] = !empty($ExcelCellVal)?date("Y-m-d",strtotime($ExcelCellVal)):NULL;
                else if($DBColumnNames[$ExcelColumnKey] != 'UNMAPPED')
                    $data[$DBColumnNames[$ExcelColumnKey]] = isset($ExcelCellVal)?$ExcelCellVal:NULL; 
            } 
            $data['OrganizationID'] = $this->LoginOrganizationID; 

            $final_data[] = $data;
        }

        if(!empty($final_data)){

            $result = $this->db->insert_batch('Task', $final_data); 
           
            $first_id = $this->db->insert_id();

            for ($a = 0;$a < $result; $a++){

                $TaskID = $first_id;

                MY_Model::insertCreatedModifiedByAndDt($UserID, 'Task', array('TaskID' => $TaskID));

                $first_id += 1;
            }

            return 1;

        } else {

            return 0;
        } 
    }

    function GetRequiredColumnsArray($Object){
        
        if($Object == "Task")
            $RequiredColumns = array('Subject','RelatedTo','What','AssignedTo','TaskStatus','TaskPriority','DueDate','TaskType');
        else if($Object == "Estimate")
            $RequiredColumns = array('EstimateName','Account','Owner','BillingAddress','BillingCity','BillingState','BillingCountry','BillingPostalCode','BillingName','Description','ExpirationDate','ShippingAddress','ShippingCity','ShippingState','ShippingCountry','ShippingPostalCode','ShippingName','EstimateStatus');
        else if($Object == "Invoice")
            $RequiredColumns = array('WorkOrder','Account','Address','City','State','Country','PostalCode','Description','InvoiceStatus','InvoiceDate','DueDate','PaymentTerms');
        else if($Object == "Contact")
            $RequiredColumns = array('Title','Salutation','FirstName','LastName','Account','MailingAddress','MailingCity','MailingState','MailingPostalCode','MailingCountry','LeadSource');
        else if($Object == "WorkOrder")
            $RequiredColumns = array('AssignedTo','Account','Subject','Description','WorkOrderType','WOStatus','WOPriority','WOCategory','StartDate','EndDate','StartTime','EndTime','Address','City','State','Country','PostalCode');
        else if($Object == "Account")
            $RequiredColumns = array('AssignedTo','AccountName','PhoneNo','BillingCountry','BillingState','BillingCity','BillingAddress','BillingPostalCode','ShippingCountry','ShippingState','ShippingCity','ShippingAddress','ShippingPostalCode','AccountType');

        return $RequiredColumns;
    }
}
?>