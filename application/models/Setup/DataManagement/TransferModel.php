<?php
class TransferModel extends MY_Model{

    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }
    
    function FindRecords(){

        extract($_POST);
     
        if($Object == "Account"){

            $this->db->select('a.AccountID, a.AccountName, a.BillingCity, a.BillingState, at.AccountType, DATE_FORMAT(a.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,DATE_FORMAT(a.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cr.FirstName, " ", cr.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName');
            $this->db->from("Account a");
            $this->db->join('User ast','ast.UserID = a.AssignedTo', 'left');
            $this->db->join('User pt','pt.UserID = a.PreferredTechnician', 'left');
            $this->db->join('User cr','cr.UserID = a.CreatedBy', 'left');
            $this->db->join('User lmb','lmb.UserID = a.LastModifiedBy', 'left');
            $this->db->join('Contact pc','pc.ContactID = a.PrimaryContact', 'left');
            $this->db->join('AccountType at','at.AccountTypeID = a.AccountType', 'left');
            $this->db->where(array('a.AssignedTo' => $TransferFrom, 'a.OrganizationID' => $this->LoginOrganizationID, 'a.IsDeleted' => 0));

            if(isset($FilterFields) && !empty($FilterFields) && isset($FilterConditions) && !empty($FilterConditions) && isset($FilterValues) && !empty($FilterValues)){

                foreach ($FilterFields as $key => $value) {

                    if(!empty($FilterFields[$key]) && !empty($FilterValues[$key]) && !empty($FilterConditions[$key]))
                    {

                        $WhereField = "";
                        if($FilterFields[$key] == 'AssignedTo'){
                            $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                        } else if($FilterFields[$key] == 'PreferredTechnician') {
                            $WhereField = "CONCAT(pt.FirstName, ' ', pt.LastName)";
                        } else if($FilterFields[$key] == 'CreatedBy') {
                            $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                        } else if($FilterFields[$key] == 'LastModifiedBy') {
                            $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                        } else if($FilterFields[$key] == 'PrimaryContact') {
                            $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                        } else if($FilterFields[$key] == 'AccountType') {
                            $WhereField = "at.AccountType";
                        } else if($FilterFields[$key] == 'LastActivityDate'){
                             $WhereField = "DATE_FORMAT(a.LastActivityDate, '".RES_DATE."')";
                        } else if($FilterFields[$key] == 'LastServiceDate'){
                             $WhereField = "DATE_FORMAT(a.LastServiceDate, '".RES_DATE."')";
                        } else {
                            $WhereField = "a.".$FilterFields[$key];
                        }

                        MY_Model::checkFilterConditions($FilterConditions[$key], $FilterValues[$key], $WhereField);
                    }
                }
            }
        } 
        else if ($Object == "Contact") 
        {
            $this->db->select('c.ContactID, CONCAT(c.FirstName, " ", c.LastName) as ContactName, c.MailingCity, c.MailingState, c.PhoneNo, DATE_FORMAT(c.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,DATE_FORMAT(c.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cr.FirstName, " ", cr.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName');
            $this->db->from('Contact c');
            $this->db->join('User ast','ast.UserID = c.AssignedTo', 'left');
            $this->db->join('Account a','a.AccountID = c.Account', 'left');
            $this->db->join('User cr','cr.UserID = c.CreatedBy', 'left');
            $this->db->join('User lmb','lmb.UserID = c.LastModifiedBy', 'left');
            $this->db->join('LeadSource ls','ls.LeadSourceID = c.LeadSource', 'left');
            $this->db->where(array('c.AssignedTo' => $TransferFrom, 'c.OrganizationID' => $this->LoginOrganizationID, 'c.IsActive' => 1, 'c.IsDeleted' => 0));

            if(isset($FilterFields) && !empty($FilterFields) && isset($FilterConditions) && !empty($FilterConditions) && isset($FilterValues) && !empty($FilterValues)){
                
                foreach ($FilterFields as $key => $value) {

                    if(!empty($FilterFields[$key]) && !empty($FilterValues[$key]) && !empty($FilterConditions[$key]))
                    {
                        $WhereField = "";
                        if($FilterFields[$key] == 'AssignedTo'){
                            $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                        } else if($FilterFields[$key] == 'Account') {
                            $WhereField = "a.AccountName";
                        } else if($FilterFields[$key] == 'CreatedBy') {
                            $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                        } else if($FilterFields[$key] == 'LastModifiedBy') {
                            $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                        } else if($FilterFields[$key] == 'LeadSource') {
                            $WhereField = "ls.LeadSource";
                        } else if($FilterFields[$key] == 'BirthDate'){
                             $WhereField = "DATE_FORMAT(c.BirthDate, '".RES_DATE."')";
                        } else { 
                            $WhereField = "c.".$FilterFields[$key];
                        }

                        MY_Model::checkFilterConditions($FilterConditions[$key], $FilterValues[$key], $WhereField);
                    }
                }
            }
        } 
        else if ($Object == "WorkOrder") 
        {
            $this->db->select('w.WorkOrderID, w.Subject, w.City, w.State, wot.WorkOrderType, DATE_FORMAT(w.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,DATE_FORMAT(w.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cr.FirstName, " ", cr.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName');
            $this->db->from('WorkOrder w');
            $this->db->join('User ast','ast.UserID = w.AssignedTo', 'left');
            $this->db->join('Account ac','ac.AccountID = w.Account', 'left');
            $this->db->join('WorkOrder pwo','pwo.WorkOrderID = w.ParentWorkOrder', 'left');
            $this->db->join('WOPriority wop','wop.WOPriorityID = w.WOPriority', 'left');
            $this->db->join('WOCategory woc','woc.WOCategoryID = w.WOCategory', 'left');
            $this->db->join('WOStatus wos','wos.WOStatusID = w.WOStatus', 'left');
            $this->db->join('User cr','cr.UserID = w.CreatedBy', 'left');
            $this->db->join('User lmb','lmb.UserID = w.LastModifiedBy', 'left');
            $this->db->join('Contact pc','pc.ContactID = w.PrimaryContact', 'left');
            $this->db->join('WorkOrderType wot','wot.WorkOrderTypeID = w.WorkOrderType', 'left');
            $this->db->where(array('w.AssignedTo' => $TransferFrom, 'w.IsDeleted' => 0, 'w.OrganizationID' => $this->LoginOrganizationID));

            if(isset($FilterFields) && !empty($FilterFields) && isset($FilterConditions) && !empty($FilterConditions) && isset($FilterValues) && !empty($FilterValues)){

                foreach ($FilterFields as $key => $value) {

                    if(!empty($FilterFields[$key]) && !empty($FilterValues[$key]) && !empty($FilterConditions[$key]))
                    {
                        $WhereField = "";
                        if($FilterFields[$key] == 'AssignedTo'){
                            $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                        } else if($FilterFields[$key] == 'Account') {
                            $WhereField = "ac.AccountName";
                        } else if($FilterFields[$key] == 'ParentWorkOrder') {
                            $WhereField = "pwo.Subject";
                        } else if($FilterFields[$key] == 'WOPriority') {
                            $WhereField = "wop.Priority";
                        } else if($FilterFields[$key] == 'WOCategory') {
                            $WhereField = "woc.CategoryName";
                        } else if($FilterFields[$key] == 'WOStatus') {
                            $WhereField = "wos.Status";
                        } else if($FilterFields[$key] == 'CreatedBy') {
                            $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                        } else if($FilterFields[$key] == 'LastModifiedBy') {
                            $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                        } else if($FilterFields[$key] == 'PrimaryContact') {
                            $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                        } else if($FilterFields[$key] == 'WorkOrderType') {
                            $WhereField = "wot.WorkOrderType";
                        } else if($FilterFields[$key] == 'StartDate'){
                             $WhereField = "DATE_FORMAT(w.StartDate, '".RES_DATETIME."')";
                        } else if($FilterFields[$key] == 'EndDate'){
                             $WhereField = "DATE_FORMAT(w.EndDate, '".RES_DATETIME."')";
                        } else if($FilterFields[$key] == 'StartTime'){
                             $WhereField = "DATE_FORMAT(w.StartTime, '".RES_TIME."')";
                        } else if($FilterFields[$key] == 'EndTime'){
                             $WhereField = "DATE_FORMAT(w.EndTime, '".RES_TIME."')";
                        } else {
                            $WhereField = "w.".$FilterFields[$key];
                        }

                        MY_Model::checkFilterConditions($FilterConditions[$key], $FilterValues[$key], $WhereField);
                    }
                }
            }
        } 
        else if ($Object == "Estimate") 
        {
            $this->db->select('e.EstimateID, e.EstimateName, e.BillingCity, e.BillingState, ac.AccountName, DATE_FORMAT(e.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, DATE_FORMAT(e.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cr.FirstName, " ", cr.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName');
            $this->db->from('Estimate e');
            $this->db->join('User ast','ast.UserID = e.Owner', 'left');
            $this->db->join('Account ac','ac.AccountID = e.Account', 'left');
            $this->db->join('EstimateStatus es','es.EstimateStatusID = e.EstimateStatus', 'left');
            $this->db->join('User cr','cr.UserID = e.CreatedBy', 'left');
            $this->db->join('User lmb','lmb.UserID = e.LastModifiedBy', 'left');
            $this->db->join('Contact pc','pc.ContactID = e.Contact', 'left');
            $this->db->where(array('e.Owner' => $TransferFrom, 'e.OrganizationID' => $this->LoginOrganizationID, 'e.IsDeleted' => 0));

            if(isset($FilterFields) && !empty($FilterFields) && isset($FilterConditions) && !empty($FilterConditions) && isset($FilterValues) && !empty($FilterValues)){

                foreach ($FilterFields as $key => $value) {

                    if(!empty($FilterFields[$key]) && !empty($FilterValues[$key]) && !empty($FilterConditions[$key]))
                    { 
                        $WhereField = "";
                        if($FilterFields[$key] == 'Owner'){
                            $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                        } else if($FilterFields[$key] == 'Account') {
                            $WhereField = "ac.AccountName";
                        } else if($FilterFields[$key] == 'EstimateStatus') {
                            $WhereField = "es.Status";
                        } else if($FilterFields[$key] == 'CreatedBy') {
                            $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                        } else if($FilterFields[$key] == 'LastModifiedBy') {
                            $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                        } else if($FilterFields[$key] == 'Contact') {
                            $WhereField = "CONCAT(pc.FirstName, ' ', pc.LastName)";
                        } else if($FilterFields[$key] == 'ExpirationDate'){
                             $WhereField = "DATE_FORMAT(e.ExpirationDate, '".RES_DATE."')";
                        } else {
                            $WhereField = "e.".$FilterFields[$key];
                        }

                        MY_Model::checkFilterConditions($FilterConditions[$key], $FilterValues[$key], $WhereField);
                    }
                } 
            }
        } 
        else if ($Object == "Task") 
        {
            $this->db->select('t.TaskID, t.Subject, ts.TaskStatus, DATE_FORMAT(t.DueDate, "'.RES_DATE.'") as Date, CONCAT(c.FirstName, " ", c.LastName) as ContactName, DATE_FORMAT(t.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, DATE_FORMAT(t.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cr.FirstName, " ", cr.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName');
            $this->db->from('Task t');
            $this->db->join('User ast','ast.UserID = t.AssignedTo', 'left');
            $this->db->join('Contact c','c.ContactID = t.Who', 'left');
            $this->db->join('User cr','cr.UserID = t.CreatedBy', 'left');
            $this->db->join('User lmb','lmb.UserID = t.LastModifiedBy', 'left');
            $this->db->join('TaskStatus ts','ts.TaskStatusID = t.TaskStatus', 'left');
            $this->db->join('TaskPriority tp','tp.TaskPriorityID = t.TaskPriority', 'left');
            $this->db->join('TaskType tt','tt.TaskTypeID = t.TaskType', 'left');
            $this->db->where(array('t.AssignedTo' => $TransferFrom, 't.OrganizationID' => $this->LoginOrganizationID, 't.IsDeleted' => 0));

            if(isset($FilterFields) && !empty($FilterFields) && isset($FilterConditions) && !empty($FilterConditions) && isset($FilterValues) && !empty($FilterValues)){

                foreach ($FilterFields as $key => $value) {

                    if(!empty($FilterFields[$key]) && !empty($FilterValues[$key]) && !empty($FilterConditions[$key]))
                    { 

                        $WhereField = "";
                        if($FilterFields[$key] == 'AssignedTo'){
                            $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                        } else if($FilterFields[$key] == 'Who') {
                            $WhereField = "CONCAT(c.FirstName, ' ', c.LastName)";
                        } else if($FilterFields[$key] == 'CreatedBy') {
                            $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                        } else if($FilterFields[$key] == 'LastModifiedBy') {
                            $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                        } else if($FilterFields[$key] == 'TaskStatus') {
                            $WhereField = "ts.TaskStatus";
                        } else if($FilterFields[$key] == 'TaskPriority') {
                            $WhereField = "tp.Priority";
                        } else if($FilterFields[$key] == 'TaskType') {
                            $WhereField = "tt.TaskType";
                        } else if($FilterFields[$key] == 'DueDate'){
                            $WhereField = "DATE_FORMAT(t.DueDate, '".RES_DATE."')";
                        } else {
                            $WhereField = "t.".$FilterFields[$key];
                        }

                        MY_Model::checkFilterConditions($FilterConditions[$key], $FilterValues[$key], $WhereField);
                    }
                }
            }
        } 
        else if ($Object == "Invoice") 
        {
            $this->db->select('i.InvoiceID, i.InvoiceNo, ac.AccountName, i.City, i.State, DATE_FORMAT(i.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, DATE_FORMAT(i.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cr.FirstName, " ", cr.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName');
            $this->db->from('Invoice i');
            $this->db->join('User ast','ast.UserID = i.AssignedTo', 'left');
            $this->db->join('Account ac','ac.AccountID = i.Account', 'left');
            $this->db->join('WorkOrder wo','wo.WorkOrderID = i.WorkOrder', 'left');
            $this->db->join('InvoiceStatus es','es.InvoiceStatusID = i.InvoiceStatus', 'left');
            $this->db->join('InvoicePaymentTerm pt','pt.InvoicePaymentTermID = i.PaymentTerms', 'left');
            $this->db->join('User cr','cr.UserID = i.CreatedBy', 'left');
            $this->db->join('User lmb','lmb.UserID = i.LastModifiedBy', 'left');
            $this->db->join('Contact c','c.ContactID = i.Contact', 'left');
            $this->db->where(array('i.AssignedTo' => $TransferFrom, 'i.OrganizationID' => $this->LoginOrganizationID, 'i.IsDeleted' => 0));

            if(isset($FilterFields) && !empty($FilterFields) && isset($FilterConditions) && !empty($FilterConditions) && isset($FilterValues) && !empty($FilterValues)){

                foreach ($FilterFields as $key => $value) {

                    if(!empty($FilterFields[$key]) && !empty($FilterValues[$key]) && !empty($FilterConditions[$key]))
                    { 
                        $WhereField = "";

                        if($FilterFields[$key] == 'AssignedTo'){
                            $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                        } else if($FilterFields[$key] == 'Account') {
                            $WhereField = "ac.AccountName";
                        } else if($FilterFields[$key] == 'WorkOrder') {
                            $WhereField = "wo.Subject";
                        } else if($FilterFields[$key] == 'InvoiceStatus') {
                            $WhereField = "es.InvoiceStatus";
                        } else if($FilterFields[$key] == 'PaymentTerms') {
                            $WhereField = "pt.PaymentTerms";
                        } else if($FilterFields[$key] == 'CreatedBy') {
                            $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                        } else if($FilterFields[$key] == 'LastModifiedBy') {
                            $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                        } else if($FilterFields[$key] == 'Contact') {
                            $WhereField = "CONCAT(c.FirstName, ' ', c.LastName)";
                        } else if($FilterFields[$key] == 'InvoiceDate'){
                             $WhereField = "DATE_FORMAT(i.InvoiceDate, '".RES_DATE."')";
                        } else if($FilterFields[$key] == 'DueDate'){
                             $WhereField = "DATE_FORMAT(i.DueDate, '".RES_DATE."')";
                        } else {
                            $WhereField = "i.".$FilterFields[$key];
                        }

                        MY_Model::checkFilterConditions($FilterConditions[$key], $FilterValues[$key], $WhereField);
                    }
                }
            }
        }
        else if ($Object == "Event")
        {
            $this->db->select('e.EventID, e.Subject, et.EventTypeName, es.EventStatus, DATE_FORMAT(e.EventStartDate, "'.RES_DATE.'") as StartDate, DATE_FORMAT(e.EventEndDate, "'.RES_DATE.'") as EndDate, CONCAT(c.FirstName, " ", c.LastName) as ContactName, DATE_FORMAT(e.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, DATE_FORMAT(e.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cr.FirstName, " ", cr.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName');
            $this->db->from('Event e');
            $this->db->join('User ast','ast.UserID = e.AssignedTo', 'left');
            $this->db->join('Contact c','c.ContactID = e.Who', 'left');
            $this->db->join('User cr','cr.UserID = e.CreatedBy', 'left');
            $this->db->join('User lmb','lmb.UserID = e.LastModifiedBy', 'left');
            $this->db->join('EventStatus es','es.EventStatusID = e.EventStatus', 'left');
            $this->db->join('EventPriority ep','ep.EventPriorityID = e.EventPriority', 'left');
            $this->db->join('EventType et','et.EventTypeID = e.EventType', 'left');
            $this->db->where(array('e.AssignedTo' => $TransferFrom, 'e.OrganizationID' => $this->LoginOrganizationID, 'e.IsDeleted' => 0));

            if(isset($FilterFields) && !empty($FilterFields) && isset($FilterConditions) && !empty($FilterConditions) && isset($FilterValues) && !empty($FilterValues)){

                foreach ($FilterFields as $key => $value) {

                    if(!empty($FilterFields[$key]) && !empty($FilterValues[$key]) && !empty($FilterConditions[$key]))
                    { 

                        $WhereField = "";
                        if($FilterFields[$key] == 'AssignedTo'){
                            $WhereField = "CONCAT(ast.FirstName, ' ', ast.LastName)";
                        } else if($FilterFields[$key] == 'Who') {
                            $WhereField = "CONCAT(c.FirstName, ' ', c.LastName)";
                        } else if($FilterFields[$key] == 'CreatedBy') {
                            $WhereField = "CONCAT(cr.FirstName, ' ', cr.LastName)";
                        } else if($FilterFields[$key] == 'LastModifiedBy') {
                            $WhereField = "CONCAT(lmb.FirstName, ' ', lmb.LastName)";
                        } else if($FilterFields[$key] == 'EventStatus') {
                            $WhereField = "es.EventStatus";
                        } else if($FilterFields[$key] == 'EventPriority') {
                            $WhereField = "ep.Priority";
                        } else if($FilterFields[$key] == 'EventType') {
                            $WhereField = "et.EventTypeName";
                        } else if($FilterFields[$key] == 'EventStartDate'){
                            $WhereField = "DATE_FORMAT(e.EventStartDate, '".RES_DATE."')";
                        } else if($FilterFields[$key] == 'EventEndDate'){
                            $WhereField = "DATE_FORMAT(e.EventEndDate, '".RES_DATE."')";
                        } else {
                            $WhereField = "e.".$FilterFields[$key];
                        }

                        MY_Model::checkFilterConditions($FilterConditions[$key], $FilterValues[$key], $WhereField);
                    }
                }
            }
        }

        $this->db->limit(100);   
        $query = $this->db->get();
        $results['Records'] = $query->result(); 
        $results['Object'] = $Object;
        $results['TransferFrom'] = $TransferFrom;
        $results['TransferTo'] = $TransferTo;
        return $results; 
    } 

    function MassTransfer(){
        extract($_POST);

        if($Object == 'Estimate'){

            $this->db->where_in("EstimateID", $ID);
            $this->db->where('OrganizationID', $this->LoginOrganizationID);  
            $result = $this->db->update($Object, array('Owner' => $TransferTo)); 

        } else {

            $this->db->where_in($Object."ID", $ID);
            $this->db->where('OrganizationID', $this->LoginOrganizationID);  
            $result = $this->db->update($Object, array('AssignedTo' => $TransferTo));  

        }

        return $result;
    }    
}?>