<?php
 
class ActionsModel extends MY_Model{

    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }

    /*function CreateOrganization() {

        extract($_POST); 

        $UserID = $this->LoginUserID;
        $OrganizationName = explode(".", $OrganizationName)[0];
        $config = array(
             'upload_path' => FILE_PATH,
             'file_name' => $OrganizationName.'_'.date('YmdHis'),
             'overwrite' => FALSE,
             'allowed_types' => "jpg|png|jpeg|doc|pptx|xlsx|docx|zip",
        );
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('Upload')){
            $fileData = $this->upload->data();
            chmod(FILE_PATH.$fileData['file_name'], 0777);
       
            $data = array(
                'OrganizationID' =>  $this->LoginOrganizationID,
                'Subject' => $Subject,
                'RelatedTo' => $RelatedTo,
                'What' => $What,
                'AssignedTo' => $AssignedTo,
                'ContentType' => $fileData['file_type'],
                'Description' => $Description,
                'OrganizationName' => $fileData['file_name'],
                'OrganizationSize' => round($fileData['file_size']/1024, 2)
            ); 
            
            $result = $this->db->insert('Organization', $data);    
           //echo $this->db->last_query();exit;
            $OrganizationID = $this->db->insert_id();

            MY_Model::insertCreatedModifiedByAndDt($UserID,'Organization', array('OrganizationID' => $OrganizationID));
        
            return $OrganizationID;
        }
        return 0;
    } */
    function ChangeLicenses() {
        extract($_POST);          

        $UserID = $this->LoginUserID;
        $ActionStr = ($Action == "Add")?"+":"-";

        $this->db->set('TotalLicenses', 'TotalLicenses '.$ActionStr.' '.$Licenses, FALSE);
        $this->db->where(array('OrganizationID' => $this->LoginOrganizationID));
        $result = $this->db->update('UserLicense');
   
        MY_Model::updateCreatedModifiedByAndDt($UserID,'Organization', array('OrganizationID' => $this->LoginOrganizationID));
    
        return $result;
    }

    function ChangePlan() {

        extract($_POST);          

        $UserID = $this->LoginUserID;
    
        $data = array(
            'Plan' => $OrgPlanID
        ); 
        
        $result = $this->db->update('Organization', $data, array('OrganizationID' => $this->LoginOrganizationID));    
        MY_Model::updateCreatedModifiedByAndDt($UserID,'Organization', array('OrganizationID' => $this->LoginOrganizationID));
    
        return $result;
    }

    function DefaultTaxRate() {

        extract($_POST);          

        $UserID = $this->LoginUserID;
    
        $data = array(
            'Tax' => $TaxRate,
        ); 
        
        $result = $this->db->update('Organization', $data, array('OrganizationID' => $this->LoginOrganizationID));    
        MY_Model::updateCreatedModifiedByAndDt($UserID,'Organization', array('OrganizationID' => $this->LoginOrganizationID));
    
        return $result;
    }

    function FiscalYear() {

        extract($_POST);          

        $UserID = $this->LoginUserID;
    
        $data = array(
            'FiscalYearStart' => $FiscalYearStart,
            'FiscalYearIsBasedOn' => $FiscalYearIsBasedOn
        ); 
        
        $result = $this->db->update('Organization', $data, array('OrganizationID' => $this->LoginOrganizationID));    
        MY_Model::updateCreatedModifiedByAndDt($UserID,'Organization', array('OrganizationID' => $this->LoginOrganizationID));
    
        return $result;
    }

    function EditOrganization() {

        extract($_POST);          

        $UserID = $this->LoginUserID;
    
        $data = array(
            'CompanyName' => $CompanyName,
            'Contact' => (!empty($Contact) ? $Contact : NULL),
            'Phone' => $Phone,
            'Website' => $Website,
            'CompanyEdition' => $CompanyEdition,
            'BillingAddress' => $BillingAddress,
            'BillingCity' => $BillingCity,
            'BillingState' => $BillingState,
            'BillingCountry' => $BillingCountry,
            'BillingLatitude' => $BillingLatitude,
            'BillingLongitude' => $BillingLongitude,
            'BillingPostalCode' => $BillingPostalCode,
        ); 
        
        $result = $this->db->update('Organization', $data, array('OrganizationID' => $this->LoginOrganizationID));    

        MY_Model::updateCreatedModifiedByAndDt($UserID,'Organization', array('OrganizationID' => $OrganizationID));
    
        return $result;
    } 

    function getOrganizationData()
    {

        $query = $this->db->select('o.*, CONCAT(c.FirstName, " ", c.LastName) as ContactName, CONCAT(ow.FirstName, " ", ow.LastName) as OwnerName, ul.TotalLicenses, ul.UsedLicenses, DATE_FORMAT(o.ContractRenewalDate, "'.RES_DATE.'") as ContractRenewalDate, DATE_FORMAT(o.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, op.PlanName, otz.TimeZone, op.UserPriceMonthly, op.UserPriceAnnual, DATE_FORMAT(o.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy')
        ->from('Organization o')
        ->join('User cb','cb.UserID = o.CreatedBy', 'left')
        ->join('User lmb','lmb.UserID = o.LastModifiedBy', 'left')
        ->join('OrgPlan op','op.OrgPlanID = o.Plan', 'left')
        ->join('UserLicense ul','ul.OrganizationID = o.OrganizationID', 'left')
        ->join('Contact c','c.ContactID = o.Contact', 'left')
        ->join('User ow','ow.UserID = o.Owner', 'left')
        ->join('OrgTimeZone otz','otz.OrgTimeZoneID = o.DefaultTimeZone', 'left')
        ->where(array('o.OrganizationID' => $this->LoginOrganizationID)) 
        ->get();

        $data['OrganizationData'] = $query->row();
        return $data;
    }

    function GetContacts()
    { 
        $query = $this->db->get_where('Contact', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetPlans()
    { 
        $query = $this->db->get_where('OrgPlan', array('IsDeleted' => 0)); 
        
        return $query->result();
    }

    function OrgFiscalYearStart()
    { 
        $query = $this->db->get_where('OrgFiscalYearStart'); 
        
        return $query->result();
    }

    function GetUsers()
    { 
        $query = $this->db->get_where('User', array('OrganizationID' => $this->LoginOrganizationID, 'IsActive' => 1, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function DeactivateAccount() {

        extract($_POST);          

        $UserID = $this->LoginUserID;
    
        $data = array(
            'IsActive' => 0
        ); 
        
        $result = $this->db->update('Organization', $data, array('OrganizationID' => $this->LoginOrganizationID));    

        MY_Model::updateCreatedModifiedByAndDt($UserID,'Organization', array('OrganizationID' => $OrganizationID));
    
        return $result;
    } 

}

?>
