<?php
 
class ActionsModel extends MY_Model{

    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }

    function GetLoginHistory()
    { 
        $query = $this->db->select('l.*, CONCAT(nm.FirstName, " ", nm.LastName) as Name, DATE_FORMAT(l.LoginDateTime, "'.RES_DATE.'") as LoginDateTime')
        ->from('Login l')
        ->join('User nm','nm.UserID = l.UserID', 'left')
        ->where(array('l.OrganizationID' => $this->LoginOrganizationID)) 
        ->where('DATE(LoginDateTime) > DATE_SUB(now(), INTERVAL 6 MONTH)')
        ->get();
        return $query->result();
    }


    function GetUserList($View)
    {  
        $this->db->select('u.*, CONCAT(u.FirstName, " ", u.LastName) as FullName, DATE_FORMAT(u.LastLoginDT, "'.RES_DATETIME.'") as LastLogin, p.Name as Profile');
        $this->db->from('User u');
        $this->db->join('Profile p','p.ProfileID = u.Profile', 'left');
        if($View == 'ActiveUsers')
            $this->db->where(array('u.IsActive' => 1));
        else if($View == 'AllAdministrators')
            $this->db->where(array('u.Profile' => 1));
        $this->db->where(array('u.OrganizationID' => $this->LoginOrganizationID));
        $query = $this->db->get();
        $result = $query->result();
        $RowData = "";
        foreach ($result as $key => $user) {
            $Status = ($user->IsActive==1)?"Yes":"No";
            $RowData .= '<tr> 
                            <td class="text-center"><a href="'.SITE_URL.'Setup/Administration/Actions/EditUser/'.$user->UserID.'"><i class="fa fa-pencil-square-o"></i></a></td>
                            <td class="text-center"><a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$user->UserID.'">'.$user->FullName.'</a></td>
                            <td class="text-center">'.$user->Email.'</td>
                            <td class="text-center">'.$user->LastLogin.'</td>
                            <td class="text-center">'.$Status.'</td>
                            <td class="text-center">'.$user->Profile.'</td>
                        </tr>';
        }
        return $RowData;
    }

    function getUserData($UserID){
        $this->db->select('u.*, CONCAT(u.FirstName, " ", u.LastName) as FullName, DATE_FORMAT(u.StartDate, "'.RES_DATE.'") as StartDate, DATE_FORMAT(u.EndDate, "'.RES_DATE.'") as EndDate, DATE_FORMAT(u.StartOfDay, "'.RES_TIME.'") as StartOfDay, DATE_FORMAT(u.EndOfDay, "'.RES_TIME.'") as EndOfDay, p.Name as ProfileName, dp.DepartmentName, dv.DivisionName, o.CompanyName, CONCAT(mn.FirstName, " ", mn.LastName) as ManagerName, tz.TimeZone as TimeZoneName, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy, DATE_FORMAT(u.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, DATE_FORMAT(u.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate');
        
        $this->db->from('User u');
        $this->db->join('Profile p','p.ProfileID = u.Profile', 'left');
        $this->db->join('Department dp','dp.DepartmentID = u.Department', 'left');
        $this->db->join('Division dv','dv.DivisionID = u.Division', 'left');
        $this->db->join('Organization o','o.OrganizationID = u.OrganizationID', 'left');
        $this->db->join('User mn','mn.UserID = u.Manager', 'left');
        $this->db->join('OrgTimeZone tz','tz.OrgTimeZoneID = u.TimeZone', 'left');
        $this->db->join('User cb','cb.UserID = u.CreatedBy', 'left');
        $this->db->join('User lmb','lmb.UserID = u.LastModifiedBy', 'left');
        $this->db->where(array('u.UserID' => $UserID));
        $query = $this->db->get();
        return $query->row();
    }

    function RegTotalUsers() {
        $query = $this->db->get_where('User', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0));
        $data['UsedLicenses'] = $query->num_rows();

        $query = $this->db->get_where('UserLicense', array('OrganizationID' => $this->LoginOrganizationID));
        $result = $query->row();
        $data['TotalLicenses'] = $result->TotalLicenses;

        return $data; 
    }

    function CreateUser() {

        extract($_POST); 

        
            $data = array(
                'OrganizationID' =>  $this->LoginOrganizationID,
                'FirstName' => $FirstName,
                'LastName' => $LastName,
                'Email' => $Email,
                'Department' => !empty($Department)?$Department:NULL,
                'Division' => !empty($Division)?$Division:NULL,
                'Manager' => $Manager,
                'MobileNo' => $MobileNo,
                'Profile' => $Profile,
                'StartDate' => !empty($StartDate)?date("Y-m-d H:i:s",strtotime($StartDate)):NULL,
                'EndDate' => !empty($EndDate)?date("Y-m-d H:i:s",strtotime($EndDate)):NULL,
                'Address' => $Address,
                'City' => $City,
                'State' => $State,
                'Country' => $Country,
                'PostalCode' => $PostalCode,
                'Latitude' => (double)$Latitude,
                'Longitude' => (double)$Longitude,
                'TimeZone' => $TimeZone,
                'StartOfDay' => date("H:i:s",strtotime($StartOfDay)),
                'EndOfDay' => date("H:i:s",strtotime($EndOfDay)),
                'SenderEmail' => $SenderEmail,
                'SenderName' => $SenderName,
                /*'EmailSignature' => $EmailSignature,*/
                'IsActive' => isset($IsActive)?$IsActive:0,
                'PhoneNo' => $PhoneNo,
                /*'DefaultGrpNotificationFreq' => $DefaultGrpNotificationFreq,*/
                'ReceiveAdminEmails' => isset($ReceiveAdminEmails)?$ReceiveAdminEmails:0
            ); 
            
            $result = $this->db->insert('User', $data); 
              
            //echo $this->db->last_query();exit;
            $UserID = $this->db->insert_id();

            MY_Model::insertCreatedModifiedByAndDt($this->LoginUserID, 'User', array('UserID' => $UserID));

            $EmployeeNo = MY_Controller::genCode('EM', $UserID);
            $this->db->update('User', array('EmployeeNo' => $EmployeeNo), array('UserID' => $UserID)); 
        
            return $UserID;
    } 

    function EditUser($UserID) {

        extract($_POST);          

        $data = array(
                'FirstName' => $FirstName,
                'LastName' => $LastName,
                'Email' => $Email,
                'Department' => !empty($Department)?$Department:NULL,
                'Division' => !empty($Division)?$Division:NULL,
                'Manager' => $Manager,
                'MobileNo' => $MobileNo,
                'Profile' => $Profile,
                'StartDate' => !empty($StartDate)?date("Y-m-d H:i:s",strtotime($StartDate)):NULL,
                'EndDate' => !empty($EndDate)?date("Y-m-d H:i:s",strtotime($EndDate)):NULL,
                'Address' => $Address,
                'City' => $City,
                'State' => $State,
                'Country' => $Country,
                'PostalCode' => $PostalCode,
                'Latitude' => (double)$Latitude,
                'Longitude' => (double)$Longitude,
                'TimeZone' => $TimeZone,
                'StartOfDay' => date("H:i:s",strtotime($StartOfDay)),
                'EndOfDay' => date("H:i:s",strtotime($EndOfDay)),
                'SenderEmail' => $SenderEmail,
                'SenderName' => $SenderName,
               /* 'EmailSignature' => $EmailSignature,*/
                'IsActive' => isset($IsActive)?$IsActive:0,
                'PhoneNo' => $PhoneNo,
                /*'DefaultGrpNotificationFreq' => $DefaultGrpNotificationFreq,*/
                'ReceiveAdminEmails' => isset($ReceiveAdminEmails)?$ReceiveAdminEmails:0
            );  
       
        $result = $this->db->update('User', $data, array('UserID' => $UserID, 'OrganizationID' => $this->LoginOrganizationID));    
   
        MY_Model::updateCreatedModifiedByAndDt($this->LoginUserID,'User', array('UserID' => $UserID));
    
        return $result;
    } 

    function GetOrganizations()
    {  
        $this->db->select('OrganizationID, CompanyName');
        $this->db->from('Organization');
        $this->db->where(array('IsDeleted' => 0));
        $query = $this->db->get();
        return $query->result();
    }

    function GetDepartments()
    {  
        $this->db->select('DepartmentID, DepartmentName');
        $this->db->from('Department');
        $this->db->where(array('IsDeleted' => 0));
        $query = $this->db->get();
        return $query->result();
    }

    function GetDivisions()
    {  
        $this->db->select('DivisionID, DivisionName');
        $this->db->from('Division');
        $this->db->where(array('IsDeleted' => 0));
        $query = $this->db->get();
        return $query->result();
    }

    function GetProfiles()
    {  
        $this->db->select('ProfileID, Name');
        $this->db->from('Profile');
        $this->db->where(array('IsDeleted' => 0));
        $query = $this->db->get();
        return $query->result();
    }

    function GetTimeZones()
    {  
        $this->db->select('OrgTimeZoneID, TimeZone');
        $this->db->from('OrgTimeZone');
        /*$this->db->where(array('IsDeleted' => 0));*/
        $query = $this->db->get();
        return $query->result();
    }

    function ResetPassword() {

        $UserID = $this->LoginUserID;

        extract($_POST);    

        $query = $this->db->get_where('User', array('UserID' => $this->LoginUserID, 'Password' => md5($OldPassword), 'OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0));

        if($query->num_rows() > 0){
             $data = array(
                'Password' => md5($NewPassword),
            );  
       
            $result = $this->db->update('User', $data, array('UserID' => $UserID, 'OrganizationID' => $this->LoginOrganizationID));    
           
            MY_Model::updateCreatedModifiedByAndDt($this->LoginUserID,'User', array('UserID' => $UserID));

            return $result;
        } else {
            return 2;
        }
    } 
}

?>
