<?php

class PickListsModel extends MY_Model{

    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }

    function GetPickList($PickList, $PickListID = 0)
    {  
        $data['ShowDefaultCol'] = 1;
        $data['ShowColorCodeCol'] = 0; 
        $Select = "";

        if($PickList == "Department" || $PickList == "Division"){

            $Select = ", pl.".$PickList."Name as Name";
            $data['ShowDefaultCol'] = 0;

        } else if($PickList == "EventType"){

            $Select = ", pl.".$PickList."Name as Name, IF(pl.IsDefault = 1, 'True', 'False') as IsDefault";

        } else if($PickList == "EventPriority" || $PickList == "TaskPriority" || $PickList == "WOPriority"){

            $Select = ", pl.Priority as Name, IF(pl.IsDefault = 1, 'True', 'False') as IsDefault";

        } else if($PickList == "EstimateStatus" || $PickList == "WOStatus"){

            $Select = ", pl.Status as Name, IF(pl.IsDefault = 1, 'True', 'False') as IsDefault";
            $data['ShowColorCodeCol'] = 1; 

        } else if($PickList == "LeadSource" || $PickList == "ProductFamily" || $PickList == "ProductFamily" || $PickList == "UnitOfMeasurement" || $PickList == "Salutation"){

            $Select = ", pl.".$PickList." as Name";
            $data['ShowDefaultCol'] = 0; 
            
        } else if($PickList == "ProductQtyUnitOfMeasure"){

            $Select = ", pl.UnitOfMeasure as Name";
            $data['ShowDefaultCol'] = 0; 
            
        } else if($PickList == "TaskCallType"){

            $Select = ", pl.CallType as Name";
            $data['ShowDefaultCol'] = 0; 
            
        } else if($PickList == "WOCategory"){

            $Select = ", pl.CategoryName as Name, IF(pl.IsDefault = 1, 'True', 'False') as IsDefault";
            
        } else if($PickList == "InvoicePaymentTerm"){

            $Select = ", pl.PaymentTerms as Name, IF(pl.IsDefault = 1, 'True', 'False') as IsDefault";
            
        } else {

            $Select = ", pl.".$PickList." as Name, IF(pl.IsDefault = 1, 'True', 'False') as IsDefault";

        }

        $this->db->select("pl.*, pl.".$PickList."ID as ID, CONCAT(cb.FirstName, ' ', cb.LastName) as CreatedByName, CONCAT(lmb.FirstName, ' ', lmb.LastName) as LastModifiedByName, DATE_FORMAT(pl.CreatedDate, '".RES_DATETIME."') as CreatedDate, DATE_FORMAT(pl.LastModifiedDate, '".RES_DATETIME."') as LastModifiedDate $Select");
        $this->db->from("$PickList pl");
        $this->db->join('User cb','cb.UserID = pl.CreatedBy', 'left');
        $this->db->join('User lmb','lmb.UserID = pl.LastModifiedBy', 'left');
        $this->db->where(array('pl.OrganizationID' => $this->LoginOrganizationID, 'pl.IsDeleted' => 0));
        if($PickListID != 0){
             $this->db->where(array("pl.".$PickList."ID" => $PickListID));
        }
        $query = $this->db->get();
        if($PickListID != 0){
            $data['PickList'] = $query->row();
        }
        else { 
            $PickList = $query->result();
            $data['PickLists'] = $PickList;
        }

        return $data;
    }

    function StorePickList($PickList, $PickListID = 0) {

        extract($_POST);

        $ShowDefaultCol = 1;
        $ShowColorCodeCol = 0; 

        if($PickList == "Department" || $PickList == "Division"){

            $FieldName = $PickList."Name";
            $ShowDefaultCol = 0;

        } else if($PickList == "EventType"){

            $FieldName = $PickList."Name";

        } else if($PickList == "EventPriority" || $PickList == "TaskPriority" || $PickList == "WOPriority"){

            $FieldName = 'Priority';

        } else if($PickList == "EstimateStatus" || $PickList == "WOStatus"){

            $FieldName = 'Status';
            $ShowColorCodeCol = 1; 

        } else if($PickList == "LeadSource" || $PickList == "ProductFamily" || $PickList == "ProductFamily" || $PickList == "UnitOfMeasurement" || $PickList == "Salutation"){

            $FieldName = $PickList;
            $ShowDefaultCol = 0;
            
        } else if($PickList == "ProductQtyUnitOfMeasure"){

            $FieldName = 'UnitOfMeasure';
            $ShowDefaultCol = 0;
            
        } else if($PickList == "TaskCallType"){

            $FieldName = 'CallType';
            $ShowDefaultCol = 0; 
            
        } else if($PickList == "WOCategory"){

            $FieldName = 'CategoryName';
            
        } else if($PickList == "InvoicePaymentTerm"){

            $FieldName = "PaymentTerms";
            
        } else {

            $FieldName = $PickList;
        }
       
        if($ShowDefaultCol && $ShowColorCodeCol){
            $data = array(
                'OrganizationID' =>  $this->LoginOrganizationID,
                $FieldName => $Name,
                'IsDefault' => isset($IsDefault) ? $IsDefault : 0,
                'ColorCode' => $ColorCode
            ); 
        } else if($ShowColorCodeCol){
            $data = array(
                'OrganizationID' =>  $this->LoginOrganizationID,
                $FieldName => $Name,
                'ColorCode' => $ColorCode
            ); 
        } else if($ShowDefaultCol){
            $data = array(
                'OrganizationID' =>  $this->LoginOrganizationID,
                $FieldName => $Name,
                'IsDefault' => isset($IsDefault) ? $IsDefault : 0,
            ); 
        } else {
            $data = array(
                'OrganizationID' =>  $this->LoginOrganizationID,
                $FieldName => $Name
            ); 
        }   

        if($ShowDefaultCol && (isset($IsDefault) && $IsDefault == 1)){
            $result = $this->db->update($PickList, array('IsDefault' => 0), array('OrganizationID' => $this->LoginOrganizationID)); 
        }
        
        if(!empty($PickListID) && $PickListID != 0){

            $result = $this->db->update($PickList, $data, array($PickList."ID" => $PickListID, 'OrganizationID' => $this->LoginOrganizationID));    
            MY_Model::updateCreatedModifiedByAndDt($this->LoginUserID, $PickList, array($PickList."ID" => $PickListID));

        } else {
            $result = $this->db->insert($PickList, $data); 
            $PickListID = $this->db->insert_id();
            MY_Model::insertCreatedModifiedByAndDt($this->LoginUserID, $PickList, array($PickList.'ID' => $PickListID));
        }

        if(isset($PickList)){
            foreach ($PickList as $key => $val) {
               if(isset($val)){
                    MY_Model::Update($val);
                    $data = $PickList;
               }
               return $data; 
            }
        } else {
            
        }   

        return $result;
    }

    function DeletePickList($PickList, $PickListID){

        return $this->db->update($PickList, array('IsDeleted' => 1), array($PickList.'ID' => $PickListID, 'OrganizationID' => $this->LoginOrganizationID)); 
    }
}
?>