<?php
 
class ProductsModel extends MY_Model{

    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }

    function ProductList($View)
    {  
        $this->db->select('p.*, pf.ProductFamily, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName, DATE_FORMAT(p.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate');
        $this->db->from('Product p');
        $this->db->join('ProductFamily pf','pf.ProductFamilyID = p.ProductFamily', 'left');
        $this->db->join('User lmb','lmb.UserID = p.LastModifiedBy', 'left');
        if($View == 'ActiveProducts')
            $this->db->where(array('p.IsActive' => 1));
        $this->db->where(array('p.IsDeleted' => 0, 'p.OrganizationID' => $this->LoginOrganizationID));
        $query = $this->db->get(); 
        $result = $query->result();
        $RowData = "";  
        foreach ($result as $key => $product) {
            $Status = ($product->IsActive==1)?"<div class='checkbox checkbox-primary pt-0 pb-0'><input type='checkbox' name='IsActive' id='".$product->ProductID."' class='pckb' value='1' checked='' disabled=''><label for='".$product->ProductID."'></label><div>":"<div class='checkbox checkbox-primary pt-0 pb-0'><input type='checkbox' name='IsActive' value='1' id='".$product->ProductID."' class='pckb' disabled=''><label for='".$product->ProductID."'></label><div>";
            $RowData .= '<tr> 
                            <td class="text-center"><a href="'.SITE_URL.'Setup/Customize/Products/EditProduct/'.$product->ProductID.'"><i class="fa fa-pencil-square-o"></i></a></td>
                            <td class="text-center"><a href="'.SITE_URL.'Setup/Customize/Products/ProductDetails/'.$product->ProductID.'">'.$product->ProductName.'</a></td>
                            <td class="text-center">'.$Status.'</td>
                            <td class="text-center">'.$product->ProductFamily.'</td>
                            <td class="text-right">$'.$product->ListPrice.'</td>
                            <td class="text-center"><a href="'.SITE_URL.'Setup/Administration/Actions/UserDetails/'.$product->LastModifiedBy.'">'.$product->LastModifiedByName.'</a></td>
                            <td class="text-center">'.$product->LastModifiedDate.'</td>
                        </tr>';
        }
        return $RowData;
    }

    function ChangeStatus($ProductID){

        extract($_POST);

        return $this->db->update('Product', array('IsActive' => $Status), array('ProductID' => $ProductID, 'OrganizationID' => $this->LoginOrganizationID));
    }

    function CreateProduct() {
               
        extract($_POST); 

        $UserID = $this->LoginUserID;

        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'ProductName' => $ProductName,
            'ProductCode' => $ProductCode,
            /*'Account' => $Account,*/
            'DisplayUrl' => $DisplayUrl,
            'Description' => isset($Description)?$Description:NULL,
            'ProductFamily' => $ProductFamily,
            'QuantityUnitOfMeasure' => isset($QuantityUnitOfMeasure)?$QuantityUnitOfMeasure:NULL,
            'StockKeepingUnit' => isset($StockKeepingUnit)?$StockKeepingUnit:"",
            'ProductCost' => $ProductCost,
            'IsListPriceEditable' => isset($IsListPriceEditable)?$IsListPriceEditable:0,
            'DefaultQuantity' => isset($DefaultQuantity)?$DefaultQuantity:NULL,
            'ListPrice' => $ListPrice,
            'IsQuantityEditable' => isset($IsQuantityEditable)?$IsQuantityEditable:0,
            'ReplacementAvailable' => isset($ReplacementAvailable)?$ReplacementAvailable:0,
            'IsDiscountable' => isset($IsDiscountable)?$IsDiscountable:0,
            'IsRenewable' => isset($IsRenewable)?$IsRenewable:0,
            'ReplacementAvailable' => isset($ReplacementAvailable)?$ReplacementAvailable:0,
            'IsChemical' => isset($IsChemical)?$IsChemical:0,
            'IsActive' => isset($IsActive)?$IsActive:0,
        ); 
        $result = $this->db->insert('Product', $data);    
       
        $ProductID = $this->db->insert_id();

        if(empty($ProductCode))
            $this->db->update('Product', array('ProductCode'=>'P'.'-'.sprintf("%05d", $ProductID)), array('ProductID' => $ProductID));  

        MY_Model::insertCreatedModifiedByAndDt($UserID,'Product', array('ProductID' => $ProductID));
    
        return $ProductID;
    } 

    function GetProductFamily(){
        $query = $this->db->get_where('ProductFamily', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetAccounts(){
        $query = $this->db->get_where('Account', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }

    function GetUnitOfMeasurements(){
        $query = $this->db->get_where('ProductQtyUnitOfMeasure', array('OrganizationID' => $this->LoginOrganizationID, 'IsDeleted' => 0)); 
        
        return $query->result();
    }


    function GetProductData($ProductID)
    {
        $this->db->select('p.*, pf.ProductFamily as ProductFamilyName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName, DATE_FORMAT(p.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, DATE_FORMAT(p.CreatedDate, "'.RES_DATETIME.'") as CreatedDate,oum.UnitOfMeasure, a.AccountName');
        $this->db->from('Product p');
        $this->db->join('ProductFamily pf','pf.ProductFamilyID = p.ProductFamily', 'left');
        $this->db->join('User cb','cb.UserID = p.CreatedBy', 'left');
        $this->db->join('User lmb','lmb.UserID = p.LastModifiedBy', 'left');
        $this->db->join('ProductQtyUnitOfMeasure oum','oum.ProductQtyUnitOfMeasureID = p.QuantityUnitOfMeasure', 'left');
        $this->db->join('Account a','a.AccountID = p.Account', 'left');
        $this->db->where(array('p.ProductID' => $ProductID,'p.OrganizationID' => $this->LoginOrganizationID));
        $query = $this->db->get();  
        $data['ProductData'] = $query->row();
        return $data; 
    }

    function EditProduct($ProductID) {
               
        extract($_POST); 

        $UserID = $this->LoginUserID;

        $data = array(
            'OrganizationID' => $this->LoginOrganizationID,
            'ProductName' => $ProductName,
            'ProductCode' => $ProductCode,
            /*'Account' => $Account,*/
            'DisplayUrl' => $DisplayUrl,
            'Description' => isset($Description)?$Description:NULL,
            'ProductFamily' => $ProductFamily,
            'QuantityUnitOfMeasure' => isset($QuantityUnitOfMeasure)?$QuantityUnitOfMeasure:NULL,
            'StockKeepingUnit' => isset($StockKeepingUnit)?$StockKeepingUnit:"",
            'ProductCost' => $ProductCost,
            'IsListPriceEditable' => isset($IsListPriceEditable)?$IsListPriceEditable:0,
            'DefaultQuantity' => isset($DefaultQuantity)?$DefaultQuantity:NULL,
            'ListPrice' => $ListPrice,
            'IsQuantityEditable' => isset($IsQuantityEditable)?$IsQuantityEditable:0,
            'ReplacementAvailable' => isset($ReplacementAvailable)?$ReplacementAvailable:0,
            'IsDiscountable' => isset($IsDiscountable)?$IsDiscountable:0,
            'IsRenewable' => isset($IsRenewable)?$IsRenewable:0,
            'ReplacementAvailable' => isset($ReplacementAvailable)?$ReplacementAvailable:0,
            'IsChemical' => isset($IsChemical)?$IsChemical:0,
            'IsActive' => isset($IsActive)?$IsActive:0,
        ); 
        $result = $this->db->update('Product', $data, array('ProductID' => $ProductID));    
        
        MY_Model::updateCreatedModifiedByAndDt($UserID,'Product', array('ProductID' => $ProductID));
    
        return $ProductID;
    } 

    function DeleteProduct($ProductID){

        extract($_POST);

        return $this->db->update('Product', array('IsDeleted' => 1), array('ProductID' => $ProductID, 'OrganizationID' => $this->LoginOrganizationID));
    }
}

?>
