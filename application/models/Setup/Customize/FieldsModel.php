<?php

class FieldsModel extends MY_Model{

    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }

    function GetStandardFields($Object)
    {  
        $query = $this->db->query("SHOW FIELDS FROM ".$Object." WHERE Field NOT LIKE '%ID'");

        $data['StandardFields'] = $query->result();

        $query = $this->db->query("SELECT column_name, referenced_table_name, referenced_column_name FROM information_schema.KEY_COLUMN_USAGE WHERE table_name = '".$Object."' AND referenced_column_name IS NOT NULL");
        $result = $query->result();
        $fields = array();
        foreach ($result as $key => $value) {
            $fields[$value->column_name] = $value->referenced_table_name;
        }
        $data['ReferenceFields'] = $fields;

        $this->db->select('acf.*, CustomFieldID, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy, DATE_FORMAT(acf.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, DATE_FORMAT(acf.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate');
        $this->db->from('CustomFields acf');
        $this->db->join('User cb','cb.UserID = acf.CreatedBy', 'left');
        $this->db->join('User lmb','lmb.UserID = acf.LastModifiedBy', 'left');
        $this->db->where(array('acf.Object' => $Object, 'acf.OrganizationID' => $this->LoginOrganizationID));
        $query = $this->db->get();
        $CustomFields = $query->result();
        $data['CustomFields'] = $CustomFields;
 
        return $data;
    }

    function GetCustomFieldData($Object, $CustomFieldID)
    {  
        $this->db->select('acf.*, CustomFieldID, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy, DATE_FORMAT(acf.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, DATE_FORMAT(acf.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate');
        $this->db->from('CustomFields acf');
        $this->db->join('User cb','cb.UserID = acf.CreatedBy', 'left');
        $this->db->join('User lmb','lmb.UserID = acf.LastModifiedBy', 'left');
        $this->db->where(array('acf.CustomFieldID' => $CustomFieldID,'acf.OrganizationID' => $this->LoginOrganizationID));
        $query = $this->db->get();
        return $query->row();
    }

    function CreateCustomField($Object) {

        extract($_POST); 
       
        $data = array(
            'OrganizationID' =>  $this->LoginOrganizationID,
            'Object' => $Object,
            'FieldName' => $FieldName,
            'FieldLabel' => $FieldLabel,
            'FieldType' => $FieldType,
            'FieldLength' => isset($FieldLength)?$FieldLength:NULL,
            'DefaultValue' => $DefaultValue,
            'IsRequired' => isset($IsRequired)?$IsRequired:0,
            'AddToPageLayout' => isset($AddToPageLayout)?$AddToPageLayout:0,
            'OptionValues' => isset($OptionValues)?$OptionValues:NULL,
            'OptionLabels' => isset($OptionLabels)?$OptionLabels:NULL,
        ); 
        
        $result = $this->db->insert('CustomFields', $data); 
          
        $CustomFieldID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($this->LoginUserID, 'CustomFields', array('CustomFieldID' => $CustomFieldID));
    
        return $CustomFieldID;
    } 

    function EditCustomField($Object, $CustomFieldID) {

        extract($_POST);          

        $data = array(
            'OrganizationID' =>  $this->LoginOrganizationID,
            'FieldName' => $FieldName,
            'FieldLabel' => $FieldLabel,
            'FieldType' => $FieldType,
            'FieldLength' => isset($FieldLength)?$FieldLength:NULL,
            'DefaultValue' => $DefaultValue,
            'IsRequired' => isset($IsRequired)?$IsRequired:0,
            'AddToPageLayout' => isset($AddToPageLayout)?$AddToPageLayout:0,
            'OptionValues' => isset($OptionValues)?$OptionValues:NULL,
            'OptionLabels' => isset($OptionLabels)?$OptionLabels:NULL,
        ); 
       
        $result = $this->db->update('CustomFields', $data, array('CustomFieldID' => $CustomFieldID, 'OrganizationID' => $this->LoginOrganizationID));    
       
        MY_Model::updateCreatedModifiedByAndDt($this->LoginUserID, 'CustomFields', array('CustomFieldID' => $CustomFieldID));
    
        return $result;
    } 
}

?>
