<?php
 
class SettingsModel extends MY_Model{

    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }

    function ChemicalSettings() {

        extract($_POST); 

        $query = $this->db->get_where('ChemicalSettings', array('OrganizationID' => $this->LoginOrganizationID)); 
        if($query->num_rows() > 0){
            $result = $query->row();
            $ChemicalSettingID = $result->ChemicalSettingID;
            $result = $this->db->update('ChemicalSettings', array('EnableChemicals' => isset($EnableChemicals)?$EnableChemicals:0), array('ChemicalSettingID' => $ChemicalSettingID, 'OrganizationID' => $this->LoginOrganizationID)); 
        } else {
            $result = $this->db->insert('ChemicalSettings', array('EnableChemicals' => isset($EnableChemicals)?$EnableChemicals:0, 'OrganizationID' => $this->LoginOrganizationID));
        }

        return $result;
    }

    function GetChemicalSetting() {
        $query = $this->db->get_where('ChemicalSettings', array('OrganizationID' => $this->LoginOrganizationID)); 
        return $query->row();
    }

    function NoteSettings() {

        extract($_POST); 

        $query = $this->db->get_where('NoteSettings', array('OrganizationID' => $this->LoginOrganizationID)); 
        if($query->num_rows() > 0){
            $result = $query->row();
            $NoteSettingID = $result->NoteSettingID;
            $result = $this->db->update('NoteSettings', array('EnableNotes' => isset($EnableNotes)?$EnableNotes:0), array('NoteSettingID' => $NoteSettingID, 'OrganizationID' => $this->LoginOrganizationID)); 
        } else {
            $result = $this->db->insert('NoteSettings', array('EnableNotes' => isset($EnableNotes)?$EnableNotes:0, 'OrganizationID' => $this->LoginOrganizationID));
        }

        return $result;
    }

    function GetNoteSetting() {
        $query = $this->db->get_where('NoteSettings', array('OrganizationID' => $this->LoginOrganizationID)); 
        return $query->row();
    }

    function EstimateSettings() {

        extract($_POST); 

        $query = $this->db->get_where('EstimateSettings', array('OrganizationID' => $this->LoginOrganizationID)); 
        if($query->num_rows() > 0){
            $result = $query->row();
            $EstimateSettingID = $result->EstimateSettingID;
            $result = $this->db->update('EstimateSettings', array('EnableEstimates' => isset($EnableEstimates)?$EnableEstimates:0), array('EstimateSettingID' => $EstimateSettingID, 'OrganizationID' => $this->LoginOrganizationID)); 
        } else {
            $result = $this->db->insert('EstimateSettings', array('EnableEstimates' => isset($EnableEstimates)?$EnableEstimates:0, 'OrganizationID' => $this->LoginOrganizationID));
        }

        return $result;
    }

    function GetEstimateSetting() {
        $query = $this->db->get_where('EstimateSettings', array('OrganizationID' => $this->LoginOrganizationID)); 
        return $query->row();
    }
}

?>
