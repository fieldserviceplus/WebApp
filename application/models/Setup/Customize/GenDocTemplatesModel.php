<?php
class GenDocTemplatesModel extends MY_Model{

    public $LoginUserID;
    public $LoginOrganizationID;

    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }

    function GetTemplates($RelatedTo)
    {  
        $this->db->select('gdt.*, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName, DATE_FORMAT(gdt.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, DATE_FORMAT(gdt.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate');
        $this->db->from('GenDocTemplates gdt');
        $this->db->join('User cb','cb.UserID = gdt.CreatedBy', 'left');
        $this->db->join('User lmb','lmb.UserID = gdt.LastModifiedBy', 'left');
        $this->db->where(array('gdt.OrganizationID' => $this->LoginOrganizationID, 'gdt.RelatedTo' => $RelatedTo, 'gdt.IsDeleted' => 0));
        $query = $this->db->get();
        return $query->result();
    } 

    function GetTemplateData($GenDocTemplateID){
        $this->db->select('gdt.*, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedByName, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName, DATE_FORMAT(gdt.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, DATE_FORMAT(gdt.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate');
        $this->db->from('GenDocTemplates gdt');
        $this->db->join('User cb','cb.UserID = gdt.CreatedBy', 'left');
        $this->db->join('User lmb','lmb.UserID = gdt.LastModifiedBy', 'left');
        $this->db->where(array('gdt.GenDocTemplateID' => $GenDocTemplateID));
        $query = $this->db->get();
        return $query->row();
    }

    function CreateTemplate($RelatedTo) {
 
        extract($_POST); 

        $DOC_TEMPLATE_PATH = str_replace('{org_id}', $this->LoginOrganizationID, DOC_TEMPLATE_PATH);
 
        if(!empty($_FILES["TemplateFile"]['name'])){

            $FileName = explode(".", $_FILES['TemplateFile']['name'])[0];
            $config = array(
             'upload_path' => $DOC_TEMPLATE_PATH,
             'file_name' => $RelatedTo.'-Template-'.date('YmdHis'),
             'allowed_types' => "docx",
             'overwrite' => FALSE,
             'max_size' => "25600000" //here it is 25 MB(25600000 Kb)
            );
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
           
            if($this->upload->do_upload('TemplateFile')){
                $fileData = $this->upload->data();
                chmod($DOC_TEMPLATE_PATH.$fileData['file_name'], 0777);

                $data = array(
                    'OrganizationID' =>  $this->LoginOrganizationID,
                    'TemplateName' => $TemplateName,
                    'RelatedTo' => $RelatedTo,
                    'TemplateFile' => $fileData['file_name'],
                    'Description' => $Description,
                    'IsActive' => isset($IsActive)?$IsActive:0
                ); 
                
                $result = $this->db->insert('GenDocTemplates', $data); 
                  
                $GenDocTemplateID = $this->db->insert_id();

                MY_Model::insertCreatedModifiedByAndDt($this->LoginUserID, 'GenDocTemplates', array('GenDocTemplateID' => $GenDocTemplateID));
            }
        }

        return $GenDocTemplateID;
    }  
 
    function EditTemplate($GenDocTemplateID){

        extract($_POST);     

        $DOC_TEMPLATE_PATH = str_replace('{org_id}', $this->LoginOrganizationID, DOC_TEMPLATE_PATH);     

        if(!empty($_FILES["TemplateFile"]['name'])){

            $FileName = explode(".", $_FILES['TemplateFile']['name'])[0];
            $config = array(
             'upload_path' => $DOC_TEMPLATE_PATH,
             'file_name' => $RelatedTo.'-Template-'.date('YmdHis'),
             'allowed_types' => "docx",
             'overwrite' => FALSE,
             'max_size' => "25600000" //here it is 25 MB(25600000 Kb)
            );
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
           
            if($this->upload->do_upload('TemplateFile')){
                $fileData = $this->upload->data();
                chmod($DOC_TEMPLATE_PATH.$fileData['file_name'], 0777);

                unlink($DOC_TEMPLATE_PATH.$OldTemplateFile); 

                $data = array(
                    'OrganizationID' =>  $this->LoginOrganizationID,
                    'TemplateName' => $TemplateName,
                    'RelatedTo' => $RelatedTo,
                    'TemplateFile' => $fileData['file_name'],
                    'Description' => $Description,
                    'IsActive' => isset($IsActive)?$IsActive:0
                ); 
                
                $result = $this->db->update('GenDocTemplates', $data, array('GenDocTemplateID' => $GenDocTemplateID)); 

                MY_Model::updateCreatedModifiedByAndDt($this->LoginUserID, 'GenDocTemplates', array('GenDocTemplateID' => $GenDocTemplateID));
            }
        } else {

            $data = array(
                'OrganizationID' =>  $this->LoginOrganizationID,
                'TemplateName' => $TemplateName,
                'RelatedTo' => $RelatedTo,
                'Description' => $Description,
                'IsActive' => isset($IsActive)?$IsActive:0
            ); 
            
            $result = $this->db->update('GenDocTemplates', $data, array('GenDocTemplateID' => $GenDocTemplateID)); 

            MY_Model::updateCreatedModifiedByAndDt($this->LoginUserID, 'GenDocTemplates', array('GenDocTemplateID' => $GenDocTemplateID));
        }   
    
        return $result;
    } 

    function DeleteTemplate($GenDocTemplateID){

        return $this->db->update('GenDocTemplates', array('IsDeleted' => 1), array('GenDocTemplateID' => $GenDocTemplateID));
    }
}

