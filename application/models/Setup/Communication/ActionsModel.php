<?php
 
class ActionsModel extends MY_Model{

    public $LoginUserID;
    public $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }

    function GetEmailTemplateList()
    {  
        $this->db->select('et.*, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedByName');
        $this->db->from('EmailTemplates et');
        $this->db->join('User lmb','lmb.UserID = et.LastModifiedBy', 'left');
        $this->db->where(array('et.OrganizationID' => $this->LoginOrganizationID, 'et.IsDeleted' => 0));
        $query = $this->db->get();
        return $query->result();
    }

    function getEmailTemplateData($EmailTemplateID){
        $this->db->select('et.*, CONCAT(cb.FirstName, " ", cb.LastName) as CreatedBy, CONCAT(lmb.FirstName, " ", lmb.LastName) as LastModifiedBy, DATE_FORMAT(et.CreatedDate, "'.RES_DATETIME.'") as CreatedDate, DATE_FORMAT(et.LastModifiedDate, "'.RES_DATETIME.'") as LastModifiedDate');
        
        $this->db->from('EmailTemplates et');
        $this->db->join('User cb','cb.UserID = et.CreatedBy', 'left');
        $this->db->join('User lmb','lmb.UserID = et.LastModifiedBy', 'left');
        $this->db->where(array('et.EmailTemplateID' => $EmailTemplateID));
        $query = $this->db->get();
        return $query->row();
    }

    function CreateEmailTemplate() {

        extract($_POST); 
       
        $data = array(
            'OrganizationID' =>  $this->LoginOrganizationID,
            'Title' => $Title,
            'Type' => $Type,
            'Body' => $Body,
            'Description' => $Description,
            'AvailableForUse' => isset($AvailableForUse)?$AvailableForUse:0
        ); 
        
        $result = $this->db->insert('EmailTemplates', $data); 
          
        $EmailTemplateID = $this->db->insert_id();

        MY_Model::insertCreatedModifiedByAndDt($this->LoginUserID, 'EmailTemplates', array('EmailTemplateID' => $EmailTemplateID));
    
        return $EmailTemplateID;
    } 

    function EditEmailTemplate($EmailTemplateID) {

        extract($_POST);          

        $data = array(
            'OrganizationID' =>  $this->LoginOrganizationID,
            'Title' => $Title,
            'Type' => $Type,
            'Body' => $Body,
            'Description' => $Description,
            'AvailableForUse' => isset($AvailableForUse)?$AvailableForUse:0
        ); 
       
        $result = $this->db->update('EmailTemplates', $data, array('EmailTemplateID' => $EmailTemplateID, 'OrganizationID' => $this->LoginOrganizationID));    
   
        MY_Model::updateCreatedModifiedByAndDt($this->LoginUserID,'EmailTemplates', array('EmailTemplateID' => $EmailTemplateID));
    
        return $result;
    } 
}

?>
