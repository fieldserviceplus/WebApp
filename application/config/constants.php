<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code



/*
|--------------------------------------------------------------------------
| Custom Global Var For Api
|--------------------------------------------------------------------------
|
*/

define('MAINTITLE','Service Hub');

define('AUTH_KEY','03d612662bf82f471741773cf9bd5a79');

//define('IMAGES_EVENT_PATH',$_SERVER['DOCUMENT_ROOT'].'/fieldserviceplus/images/');
//define('IMAGES_EVENT_URL',$_SERVER['HTTP_HOST'].'/fieldserviceplus/images/');

define('SITE_URL', 'http://'.$_SERVER['SERVER_NAME'].'/fieldserviceplus/');

//define('IMAGE_URL', SITE_URL.'images/');
//define('IMAGE_THUMB_URL', SITE_URL.'images/thumb/');
define('SITE_LOGO_URL', SITE_URL.'img/logo.png');

define('FILE_URL', SITE_URL.'org_data/{org_id}/files/');
define('DOC_TEMPLATE_URL', SITE_URL.'org_data/{org_id}/doc_templates/');

//define('IMAGE_PATH', 'images/');
//define('IMAGE_THUMB_PATH', 'images/thumb/');
define('FILE_PATH', 'org_data/{org_id}/files/');
define('DOC_TEMPLATE_PATH', 'org_data/{org_id}/doc_templates/');
define('TEMP_PATH', 'org_data/{org_id}/temp/');

/* MySql Date Formate */
define('RES_DATETIME', "%m/%d/%Y %h:%i %p");
define('RES_DATE', "%m/%d/%Y");
define('RES_TIME', "%h:%i %p");
define('DISPLAY_DATE', "m/d/Y");
define('CURRENT_DATETIME', date("Y-m-d H:i:s"));
define('CURRENT_DATE', date("Y-m-d"));

/* Mail Configuration */
define('ADMIN_EMAIL', "fieldserviceplus@beforesubmit.com");
define('SMTP_HOST', "mail.beforesubmit.com");
define('SMTP_PORT', "25");
define('SMTP_USER', "fieldserviceplus@beforesubmit.com");
define('SMTP_PASS', "oV)*mZ-uA6Z=");
define('SMTP_CRYPTO', 'tls');
define('PROTOCOL', 'smtp'); 
 
//define('MAP_JS_KEY','AIzaSyCVzNNdPexRApDmEYSNnc3Yif6rbjDSnsA');
define('MAP_JS_KEY','AIzaSyASeU7clEQO8tsMa9UyGl4Y3HwurjaC_1o');

/* Object Array */
define('OBJECT_DETAILS', serialize(array(
	'Account' => array(
		'icon' => 'fa fa-user',
		'title' => 'Accounts'
	),
	'Contact' => array(
		'icon' => 'fa fa-user',
		'title' => 'Contacts'
	),
	'Calendar' => array(
		'icon' => 'fa fa-calendar',
		'title' => 'Calendar'
	),
	'WorkOrder' => array(
		'icon' => 'fa fa-first-order',
		'title' => 'Work Orders'
	),
	'Estimate' => array(
		'icon' => 'fa fa-flag',
		'title' => 'Estimates'
	),
	'Invoice' => array(
		'icon' => 'fa fa-file-pdf-o',
		'title' => 'Invoices'
	),
	'File' => array(
		'icon' => 'fa fa-file',
		'title' => 'Files'
	),
	'Task' => array(
		'icon' => 'fa fa-tasks',
		'title' => 'Tasks'
	),
	'Location' => array(
		'icon' => 'fa fa-map-marker',
		'title' => 'Locations'
	),
	'User' => array(
		'icon' => 'fa fa-user',
		'title' => 'Users'
	),
	'Note' => array(
		'icon' => 'fa fa-sticky-note-o',
		'title' => 'Notes'
	),
	'Event' => array(
		'icon' => 'fa fa-calendar-check-o',
		'title' => 'Events'
	),
	'Product' => array(
		'icon' => 'fa fa-product-hunt',
		'title' => 'Products'
	)
)));

