<?php
 
class Actions extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Event/ActionsModel');
    }
   
    function index() { 
        $this->load->view('Event/EventHome');
    }

    function RecentEvents() { 
        echo $this->ActionsModel->RecentEvents();
    }
  
    function CreateEvent($EventID = 0) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
 
        if(isset($_POST['CreateEvent'])){
 
            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('RelatedTo', 'related to', 'required');
            $this->form_validation->set_rules('What', 'what', 'required');
            $this->form_validation->set_rules('AssignedTo', 'assigned to', 'required');
            $this->form_validation->set_rules('EventPriority', 'task priority', 'required');
            $this->form_validation->set_rules('EventType', 'task type', 'required'); 
            $this->form_validation->set_rules('EventStartDate', 'start date', 'required');
            $this->form_validation->set_rules('EventEndDate', 'end date', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {  
                $EventID = $this->ActionsModel->CreateEvent();    
                if(!empty($EventID)){

                    $this->session->set_flashdata('SuccessMsg', 'Event created successfully.');
                    redirect('Event/Actions/EventDetails/'.$EventID); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Event/Actions/CreateEvent');
                }
            } else {
                $this->load->view('Event/CreateEvent');
            }

        } else { 

            $data =  $this->ActionsModel->GetEventData($EventID); 
            $this->load->view('Event/CreateEvent', $data);
        }
    }

    function EventDetails($EventID) {

        $data =  $this->ActionsModel->GetEventData($EventID); 
        $this->load->view('Event/EventDetails', $data);
    }

    function RelatedObject($EventID, $RelatedObjName) {

        $RelatedObjData =  $this->ActionsModel->GetEventData($EventID, $RelatedObjName); 
        $EventData =  $this->ActionsModel->GetEventData($EventID);
        $data['RelatedObjData'] = $RelatedObjData;
        $data['EventData'] = $EventData['EventData'];
        $data['RelatedObjName'] = $RelatedObjName;
        $this->load->view('Event/EventRelated'.$RelatedObjName, $data);
    }

    function EditEvent($EventID) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data =  $this->ActionsModel->GetEventData($EventID); 

        if(isset($_POST['EditEvent'])){

            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('RelatedTo', 'related to', 'required');
            $this->form_validation->set_rules('What', 'what', 'required');
            $this->form_validation->set_rules('AssignedTo', 'assigned to', 'required');
            $this->form_validation->set_rules('EventPriority', 'task priority', 'required');
            $this->form_validation->set_rules('EventType', 'task type', 'required'); 
            $this->form_validation->set_rules('EventStartDate', 'start date', 'required');
            $this->form_validation->set_rules('EventEndDate', 'end date', 'required');
             
            if($this->form_validation->run() == TRUE)
            {  
                $result = $this->ActionsModel->EditEvent($EventID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Event updated successfully.');
                    redirect('Event/Actions/EventDetails/'.$EventID); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Event/Actions/EditEvent/'.$EventID); 
                }
            } else { 
                $this->load->view('Event/EditEvent', $data); 
            }

        } else { 
           
            $this->load->view('Event/EditEvent', $data);
        }
    }

    function DeleteEvent($EventID, $Object, $ObjectID){
        $result =  $this->ActionsModel->DeleteEvent($EventID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Event deleted successfully.');
            redirect($Object.'/Actions/'.$Object.'Details/'.$ObjectID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Event/Actions/EventDetails/'.$EventID); 
        }
    }

    function GetRelatedToObjectOptions($RelatedTo, $ObjectID = 0)
    {
        echo MY_Model::getRelatedToObjectOptions($RelatedTo, $ObjectID);
    }

    function SaveRecurrenceSetting(){
        echo $this->ActionsModel->SaveRecurrenceSetting();
    }
}
