<?php
 
class Actions extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Note/ActionsModel');
    }
   
    /*function index() { 
        $this->load->view('Note/NoteHome');
    }*/

    function NoteDetails($NoteID) {

        $data =  $this->ActionsModel->GetNoteData($NoteID);
        $this->load->view('Note/NoteDetails', $data);
    }

    function EditNote($NoteID) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data =  $this->ActionsModel->GetNoteData($NoteID); 

        if(isset($_POST['EditNote'])){

            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('RelatedTo', 'related to', 'required');
            $this->form_validation->set_rules('What', 'what', 'required');
            $this->form_validation->set_rules('Owner', 'owner', 'required');
            $this->form_validation->set_rules('Body', 'body', 'required');
             
            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->ActionsModel->EditNote($NoteID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Note updated successfully.');
                    redirect('Note/Actions/NoteDetails/'.$NoteID); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Note/Actions/EditNote/'.$NoteID); 
                }
            } else { 
                $this->load->view('Note/EditNote', $data); 
            }

        } else { 
           
            $this->load->view('Note/EditNote', $data);
        }
    }

    function DeleteNote($NoteID, $Object, $ObjectID){
        $result =  $this->ActionsModel->DeleteNote($NoteID, $Object, $ObjectID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Note deleted successfully.');
            redirect($Object.'/Actions/'.$Object.'Details/'.$ObjectID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Note/Actions/NoteDetails/'.$NoteID); 
        }
    }

    function GetRelatedToObjectOptions($RelatedTo, $ObjectID = 0)
    {
        echo MY_Model::getRelatedToObjectOptions($RelatedTo, $ObjectID);
    }
}
