<?php

class View extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Account/ViewModel');

        $this->load->helper('url');
        // Load session
        $this->load->library('session');
        
        // Load Pagination library
        $this->load->library('pagination');
    }
   
    /*function index($AccountViewID = 0) {

        if(isset($AccountViewID) && $AccountViewID != 0)
            $data['SelectedAccount'] = $AccountViewID;
        else 
            $data['SelectedAccount'] = isset($_POST['SelectedAccount'])?$_POST['SelectedAccount']:"";

        if($data['SelectedAccount'] == 'MyAccounts' || $data['SelectedAccount'] == 'AllAccounts' || $data['SelectedAccount'] == 'NewAccountsThisWeek' || $data['SelectedAccount'] == ""){
            $this->load->view('Account/AccountView', $data);
        }
        else 
        {
            $data['AccountCustomViewData'] = $this->ViewModel->GetCustomView($data['SelectedAccount']);
            $data['AccountData'] = $this->ViewModel->GetAccountViewData($data['SelectedAccount']);
           
            $this->load->view('Account/AccountCustomView', $data);
        }
    }*/

    function index($AccountViewID = 0){

        if(isset($AccountViewID) && $AccountViewID != 0)
            $data['SelectedAccount'] = $AccountViewID;
        else 
            $data['SelectedAccount'] = isset($_POST['SelectedAccount'])?$_POST['SelectedAccount']:"";

        if($data['SelectedAccount'] == 'MyAccounts' || $data['SelectedAccount'] == 'AllAccounts' || $data['SelectedAccount'] == 'NewAccountsThisWeek' || $data['SelectedAccount'] == ""){
            $this->load->view('Account/AccountView', $data);
        }
        else 
        {
            //return $this->List($data['SelectedAccount']);

            $data['AccountCustomViewData'] = $this->ViewModel->GetCustomView($data['SelectedAccount']);
            //$data['AccountData'] = $this->ViewModel->GetAccountViewData($data['SelectedAccount'], 0, 10, "", "");
            $this->load->view('Account/AccountCustomView', $data);
        }
        
    }

    public function List($AccountViewID, $rowno = 0, $rowperpage = 10, $sortBy = "", $order=""){

        $data['SelectedAccount'] = $AccountViewID;

        $config = array();
        //$rowperpage = 10;
        $config["cur_page"] = empty($rowno) ? 1 : $rowno;
        
        if($rowno != 0){ 
           $rowno = ($rowno - 1) * $rowperpage; 
        }

        $order = ($order != "") ? (($order == "asc") ? "desc" : "asc") : "";

        // Get records
        $data['AccountCustomViewData'] = $this->ViewModel->GetCustomView($AccountViewID);
        $data['AccountData'] = $this->ViewModel->GetAccountViewData($AccountViewID, $rowno, $rowperpage, $sortBy, $order);

        // Total records count
        $allcount =  $data['AccountData']['TotalRows'];
       
        // Pagination Configuration
        $config['base_url'] = base_url().'Account/View/List/'.$AccountViewID;
        $config['first_url'] = '1';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $allcount;
        $config['num_links'] = 2;
        $config['per_page'] = $rowperpage;
        $config['attributes'] = array('class' => 'paginate_button');
        $config['cur_tag_open'] = '<a class="paginate_button current">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';

        $start = (int) ($allcount != 0) ? ($config["cur_page"]-1) * $config['per_page'] + 1 : 0;
        $end = (($config["cur_page"]-1) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int)($config["cur_page"]-1) * $config['per_page'] + $config['per_page'];

        $data['result_count']= "Showing ".$start." to ".$end." of ".$config['total_rows']." entries";

        // Initialize
        $this->pagination->initialize($config);
       
        $data['pagination'] = $this->pagination->create_links();
        $data['order'][$sortBy] = $order;

        echo json_encode($data);

    }
    
    function ViewAccounts() { 
         echo $this->ViewModel->ViewAccounts();
    }

    /*function ViewResult()
    {
         if(isset($_POST['FilterForm'])){

            extract($_POST);
            $result = $this->ViewModel->SaveAccountFilter($FilterFields, $FilterConditions, $FilterValues); 
            $data['FilteredAccounts'] = $result; 
            $this->load->view('Account/AccountViewResult', $data);

         } else { 
            $this->load->view('Account/AccountViewResult');
        }
    }*/

    function CreateNewView() { 

        $data['AccountViewFields'] = $this->ViewModel->AccountViewFields(); 
        if(isset($_POST['CreateNewView'])){

            $AccountViewID = $this->ViewModel->CreateNewView(); 

            if($AccountViewID){

                $this->session->set_flashdata('SuccessMsg', 'Account new view created successfully.');
                redirect('Account/View/Index/'.$AccountViewID);

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Account/View/CreateNewView', $data);
            }
        } else { 
            $this->load->view('Account/CreateNewView', $data);
        }
    }

    function EditCustomView($AccountViewID) { 

        $data = $this->ViewModel->GetCustomView($AccountViewID);

        if(isset($_POST['EditCustomView'])){

            $result = $this->ViewModel->EditCustomView($AccountViewID); 

            if($result){

                $this->session->set_flashdata('SuccessMsg', 'Account view updated successfully.');
                redirect('Account/Actions');

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Account/View/EditCustomView', $data);
            }
        } else { 
            $this->load->view('Account/EditCustomView', $data);
        }
    }

    function DeleteCustomView($AccountViewID) { 
        $result =  $this->ViewModel->DeleteCustomView($AccountViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Account view deleted successfully.');
            redirect('Account/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Account/View/Index/'.$AccountViewID);
        }
    }

    function CopyCustomView($AccountViewID)
    { 
        $result =  $this->ViewModel->CopyCustomView($AccountViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Account view copy successfully.');
            redirect('Account/View/Index/'.$AccountViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Account/View/Index/'.$AccountViewID);
        }
    }

    function RenameCustomView($AccountViewID){

        $result =  $this->ViewModel->RenameCustomView($AccountViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Account view rename successfully.');
            redirect('Account/View/Index/'.$AccountViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Account/View/Index/'.$AccountViewID);

        }
    }

    function EditSharingCustomView($AccountViewID){

        $result =  $this->ViewModel->EditSharingCustomView($AccountViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Account view sharing updated successfully.');
            redirect('Account/View/Index/'.$AccountViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Account/View/Index/'.$AccountViewID);

        }
    }
  
    function EditFiltersCustomView($AccountViewID)
    {
        $result =  $this->ViewModel->EditFiltersCustomView($AccountViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Account view filters updated successfully.');
            redirect('Account/View/Index/'.$AccountViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Account/View/Index/'.$AccountViewID);

        }
    }

    function EditDisplayedColumnsCustomView($AccountViewID)
    {
        $result =  $this->ViewModel->EditDisplayedColumnsCustomView($AccountViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Account view displayed columns updated successfully.');
            redirect('Account/View/Index/'.$AccountViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Account/View/Index/'.$AccountViewID);

        }
    }
}
