<?php
 
class Actions extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Account/ActionsModel');
    }
   
    function index() { 
        $this->load->view('Account/AccountHome');
    }

    function RecentAccounts() { 
        echo $this->ActionsModel->RecentAccounts();
    }
  
    function CreateAccount($AccountID = 0) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
 
        if(isset($_POST['CreateAccount'])){
 
            $this->form_validation->set_rules('AssignedTo', 'owner', 'required');
            $this->form_validation->set_rules('AccountName', 'account name', 'required');
            $this->form_validation->set_rules('PhoneNo', 'phone no', 'required');
            //$this->form_validation->set_rules('PreferredTechnician', 'preferred technician', 'required');
            $this->form_validation->set_rules('BillingCountry', 'billing country', 'required');
            $this->form_validation->set_rules('BillingState', 'billing state', 'required');
            $this->form_validation->set_rules('BillingCity', 'billing city', 'required');
            $this->form_validation->set_rules('BillingAddress', 'billing address', 'required');
            $this->form_validation->set_rules('BillingPostalCode', 'billing postal code', 'required'); 
            $this->form_validation->set_rules('ShippingCountry', 'billing country', 'required');
            $this->form_validation->set_rules('ShippingState', 'billing state', 'required');
            $this->form_validation->set_rules('ShippingCity', 'billing city', 'required');
            $this->form_validation->set_rules('ShippingAddress', 'billing address', 'required');
            $this->form_validation->set_rules('ShippingPostalCode', 'billing postal code', 'required'); 
            //$this->form_validation->set_rules('AccessNotes', 'access notes', 'required');
            //$this->form_validation->set_rules('PopUpReminder', 'popup reminder', 'required');
            //$this->form_validation->set_rules('Notes', 'notes', 'required');
            $this->form_validation->set_rules('AccountType', 'account type', 'required');
            /*$this->form_validation->set_rules('LastActivityDate', 'last activity date', 'required');
            $this->form_validation->set_rules('LastServiceDate', 'last service date', 'required');*/
            //$this->form_validation->set_rules('PrimaryContact', 'primary contact', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {  
                $AccountID = $this->ActionsModel->CreateAccount();    
                if(!empty($AccountID)){

                    $this->session->set_flashdata('SuccessMsg', 'Account created successfully.');
                    redirect('Account/Actions/AccountDetails/'.$AccountID);

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Account/Actions/CreateAccount');
                }
            } else {
                $this->load->view('Account/CreateAccount');
            }

        } else { 

            $data =  $this->ActionsModel->getAccountData($AccountID); 
            $this->load->view('Account/CreateAccount', $data);
        }
    }

    function AccountDetails($AccountID) {

        $data =  $this->ActionsModel->getAccountData($AccountID); 
        $this->load->view('Account/AccountDetails', $data);
    }

    function OpenActivities($AccountID) {

        $Activities =  $this->ActionsModel->GetActivities(0, $AccountID, 'Open'); 
        $AccountData =  $this->ActionsModel->getAccountData($AccountID);
        $data['Activities'] = $Activities;
        $data['AccountData'] = $AccountData['AccountData'];
        $data['Flag'] = 'Open';
        $this->load->view('Account/OpenActivities', $data);
    }

    function CompletedActivities($AccountID) {

        $Activities =  $this->ActionsModel->GetActivities(0, $AccountID, 'Completed'); 
        $AccountData =  $this->ActionsModel->getAccountData($AccountID);
        $data['Activities'] = $Activities;
        $data['AccountData'] = $AccountData['AccountData'];
        $data['Flag'] = 'Completed';
        $this->load->view('Account/OpenActivities', $data);
    }

    function RelatedObject($AccountID, $RelatedObj) {

        $RelatedObjData =  $this->ActionsModel->getAccountData($AccountID, $RelatedObj); 
        $data['RelatedObjData'] = $RelatedObjData;
        $data['Data'] = $RelatedObjData['AccountData'];
        $data['RelatedObj'] = $RelatedObj;
        $this->load->view('Common/ObjectRelatedList/'.$RelatedObj, $data);
    }

    function EditAccount($AccountID) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data =  $this->ActionsModel->getAccountData($AccountID); 

        if(isset($_POST['EditAccount'])){

            $this->form_validation->set_rules('AssignedTo', 'owner', 'required');
            $this->form_validation->set_rules('AccountName', 'account name', 'required');
            $this->form_validation->set_rules('PhoneNo', 'phone no', 'required');
            $this->form_validation->set_rules('BillingCountry', 'billing country', 'required');
            $this->form_validation->set_rules('BillingState', 'billing state', 'required');
            $this->form_validation->set_rules('BillingCity', 'billing city', 'required');
            $this->form_validation->set_rules('BillingAddress', 'billing address', 'required');
            $this->form_validation->set_rules('BillingPostalCode', 'billing postal code', 'required'); 
            $this->form_validation->set_rules('ShippingCountry', 'billing country', 'required');
            $this->form_validation->set_rules('ShippingState', 'billing state', 'required');
            $this->form_validation->set_rules('ShippingCity', 'billing city', 'required');
            $this->form_validation->set_rules('ShippingAddress', 'billing address', 'required');
            $this->form_validation->set_rules('ShippingPostalCode', 'billing postal code', 'required'); 
            $this->form_validation->set_rules('AccountType', 'account type', 'required');
             
            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->ActionsModel->EditAccount($AccountID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Account updated successfully.');
                    redirect('Account/Actions/AccountDetails/'.$AccountID); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Account/Actions/EditAccount/'.$AccountID); 
                }
            } else { 
                $this->load->view('Account/EditAccount', $data); 
            }

        } else { 
           
            $this->load->view('Account/EditAccount', $data);
        }
    }

    function CreateTask() { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data =  $this->ActionsModel->getAccountData($_REQUEST['AccountID']); 
        $AccountID = $_REQUEST['AccountID'];

        if(isset($_POST['CreateTask'])){

            $this->form_validation->set_rules('AccountID', 'account', 'required');
            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('AssignedTo', 'assigned to', 'required');
            $this->form_validation->set_rules('TaskType', 'task type', 'required');
            $this->form_validation->set_rules('TaskStatus', 'task status', 'required');
            $this->form_validation->set_rules('TaskPriority', 'task priority', 'required');
            $this->form_validation->set_rules('DueDate', 'due date', 'required');
            $this->form_validation->set_rules('RelatedTo', 'related to', 'required');
            $this->form_validation->set_rules('What', 'what', 'required');
            
            if(isset($_POST['IsRecurring']) && $_POST['IsRecurring']==1){
                $this->form_validation->set_rules('WORecurrenceID', 'recurrence', 'required');
            } 

            if ($this->form_validation->run() == TRUE)
            { 
                $result = $this->ActionsModel->CreateTask();    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Task save successfully.');
                    redirect('Account/Actions/AccountDetails/'.$AccountID); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Account/Actions/AccountDetails/'.$AccountID); 
                }
            } else { 
                $this->load->view('Account/AccountDetails', $data); 
            }

        } else { 
           
            $this->load->view('Account/AccountDetails', $data);
        }
    }
 
    function CreateEvent() { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data =  $this->ActionsModel->getAccountData($_REQUEST['AccountID']); 
        $AccountID = $_REQUEST['AccountID'];

        if(isset($_POST['CreateEvent'])){

            $this->form_validation->set_rules('AccountID', 'account', 'required');
            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('AssignedTo', 'assigned to', 'required');
            $this->form_validation->set_rules('EventType', 'event type', 'required');
            $this->form_validation->set_rules('EventPriority', 'event priority', 'required');
            $this->form_validation->set_rules('EventStartDate', 'start date', 'required');
            $this->form_validation->set_rules('EventEndDate', 'end date', 'required');
            $this->form_validation->set_rules('RelatedTo', 'related to', 'required');
            $this->form_validation->set_rules('What', 'what', 'required');
             
            if(isset($_POST['IsRecurring']) && $_POST['IsRecurring']==1){
                $this->form_validation->set_rules('WORecurrenceID', 'recurrence', 'required');
            } 

            if ($this->form_validation->run() == TRUE)
            { 
                $result = $this->ActionsModel->CreateEvent();    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Event save successfully.');
                    redirect('Account/Actions/AccountDetails/'.$AccountID); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Account/Actions/AccountDetails/'.$AccountID); 
                }
            } else { 
                $this->load->view('Account/AccountDetails', $data); 
            }

        } else { 
           
            $this->load->view('Account/AccountDetails', $data);
        }
    }

    function removeRecurrenceSetting()
    {
        echo $this->ActionsModel->removeRecurrenceSetting();
    }

    function getState(){
        echo MY_Model::getState($_POST['CountryID']);
    }

    function getCity(){
        echo MY_Model::getCity($_POST['StateID']);
    } 

    function DeleteAccount($AccountID){
        $result =  $this->ActionsModel->DeleteAccount($AccountID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Account deleted successfully.');
            redirect('Account/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Account/Actions/AccountDetails/'.$AccountID); 
        }
    }
}
