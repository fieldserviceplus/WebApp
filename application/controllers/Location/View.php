<?php

class View extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Location/ViewModel');
    }
   
    function index($LocationViewID = 0) {

        if(isset($LocationViewID) && $LocationViewID != 0)
            $data['SelectedLocation'] = $LocationViewID;
        else 
            $data['SelectedLocation'] = isset($_POST['SelectedLocation'])?$_POST['SelectedLocation']:"";

        if($data['SelectedLocation'] == 'MyLocations' 
            || $data['SelectedLocation'] == 'AllLocations' 
            || $data['SelectedLocation'] == 'LocationsCreatedThisWeek'
            || $data['SelectedLocation'] == ""){
           
            $this->load->view('Location/LocationView', $data);
        }
        else 
        {
            
            $data['LocationCustomViewData'] = $this->ViewModel->GetCustomView($data['SelectedLocation']);
            $data['LocationData'] = $this->ViewModel->GetLocationViewData($data['SelectedLocation']);
           
            $this->load->view('Location/LocationCustomView', $data);
        }
    }

    function ViewLocations() { 
        echo $this->ViewModel->ViewLocations();
    }

    /*function ViewResult()
    {
         if(isset($_POST['FilterForm'])){

            extract($_POST);
            $result = $this->ViewModel->SaveLocationFilter($FilterFields, $FilterConditions, $FilterValues); 
            $data['FilteredLocations'] = $result; 
            $this->load->view('Location/LocationViewResult', $data);

         } else { 
            $this->load->view('Location/LocationViewResult');
        }
    }*/

    function CreateNewView() { 

        $data['LocationViewFields'] = $this->ViewModel->LocationViewFields(); 
        if(isset($_POST['CreateNewView'])){

            $LocationViewID = $this->ViewModel->CreateNewView(); 

            if($LocationViewID){

                $this->session->set_flashdata('SuccessMsg', 'Location new view created successfully.');
                redirect('Location/View/Index/'.$LocationViewID);

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Location/View/CreateNewView', $data);
            }
        } else { 
            $this->load->view('Location/CreateNewView', $data);
        }
    }

    function EditCustomView($LocationViewID) { 

        $data = $this->ViewModel->GetCustomView($LocationViewID);

        if(isset($_POST['EditCustomView'])){

            $result = $this->ViewModel->EditCustomView($LocationViewID); 

            if($result){

                $this->session->set_flashdata('SuccessMsg', 'Location view updated successfully.');
                redirect('Location/Actions');

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Location/View/EditCustomView', $data);
            }
        } else { 
            $this->load->view('Location/EditCustomView', $data);
        }
    }

    function DeleteCustomView($LocationViewID) { 
        $result =  $this->ViewModel->DeleteCustomView($LocationViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Location view deleted successfully.');
            redirect('Location/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Location/View/Index/'.$LocationViewID);
        }
    }

    function CopyCustomView($LocationViewID)
    { 
        $result =  $this->ViewModel->CopyCustomView($LocationViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Location view copy successfully.');
            redirect('Location/View/Index/'.$LocationViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Location/View/Index/'.$LocationViewID);
        }
    }

    function RenameCustomView($LocationViewID){

        $result =  $this->ViewModel->RenameCustomView($LocationViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Location view rename successfully.');
            redirect('Location/View/Index/'.$LocationViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Location/View/Index/'.$LocationViewID);

        }
    }

    function EditSharingCustomView($LocationViewID){

        $result =  $this->ViewModel->EditSharingCustomView($LocationViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Location view sharing updated successfully.');
            redirect('Location/View/Index/'.$LocationViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Location/View/Index/'.$LocationViewID);

        }
    }
  
    function EditFiltersCustomView($LocationViewID)
    {
        $result =  $this->ViewModel->EditFiltersCustomView($LocationViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Location view filters updated successfully.');
            redirect('Location/View/Index/'.$LocationViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Location/View/Index/'.$LocationViewID);

        }
    }

    function EditDisplayedColumnsCustomView($LocationViewID)
    {
        $result =  $this->ViewModel->EditDisplayedColumnsCustomView($LocationViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Location view displayed columns updated successfully.');
            redirect('Location/View/Index/'.$LocationViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Location/View/Index/'.$LocationViewID);

        }
    }
}
