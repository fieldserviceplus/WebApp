<?php

class Actions extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Location/ActionsModel');
    }
   
    function index() { 
        $this->load->view('Location/LocationHome');
    }

    function RecentLocations() { 
        echo $this->ActionsModel->RecentLocations();
    }
  
    function CreateLocation($LocationID = 0) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
 
        if(isset($_POST['CreateLocation'])){

            $this->form_validation->set_rules('Name', 'name', 'required');
            $this->form_validation->set_rules('Account', 'account name', 'required');
            $this->form_validation->set_rules('Owner', 'owner', 'required');  
            $this->form_validation->set_rules('Address', 'address', 'required');
            $this->form_validation->set_rules('City', 'city', 'required');
            $this->form_validation->set_rules('State', 'state', 'required');
            $this->form_validation->set_rules('Country', 'country', 'required');
            $this->form_validation->set_rules('PostalCode', 'postal code', 'required');
            $this->form_validation->set_rules('LocationType', 'type', 'required');  
            
            if ($this->form_validation->run() == TRUE)
            {   
                $result = $this->ActionsModel->CreateLocation();    
                if($result){ 

                    if(isset($_POST['Redirect']) && $_POST['Redirect'] = 'Account'){
                        $this->session->set_flashdata('SuccessMsg', 'Location created successfully.');
                        redirect('Account/Actions/AccountDetails/'.$_POST['Account']);
                    } else {
                        $this->session->set_flashdata('SuccessMsg', 'Location created successfully.');
                        redirect('Location/Actions');
                    }
                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Location/Actions/CreateLocation');
                }
            } else {
                $this->load->view('Location/CreateLocation');
            }

        } else { 

            $data =  $this->ActionsModel->getLocationData($LocationID);
            $this->load->view('Location/CreateLocation', $data);
        }
    }

    function EditLocation($LocationID) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data =  $this->ActionsModel->getLocationData($LocationID); 

        if(isset($_POST['EditLocation'])){

            $this->form_validation->set_rules('Name', 'name', 'required');
            $this->form_validation->set_rules('Account', 'account name', 'required');
            $this->form_validation->set_rules('Owner', 'owner', 'required');  
            $this->form_validation->set_rules('Address', 'address', 'required');
            $this->form_validation->set_rules('City', 'city', 'required');
            $this->form_validation->set_rules('State', 'state', 'required');
            $this->form_validation->set_rules('Country', 'country', 'required');
            $this->form_validation->set_rules('PostalCode', 'postal code', 'required');
            $this->form_validation->set_rules('LocationType', 'type', 'required');  

            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->ActionsModel->EditLocation($LocationID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Location updated successfully.');
                    redirect('Location/Actions/LocationDetails/'.$LocationID); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Location/Actions/EditLocation/'.$LocationID); 
                }
            } else { 
                $this->load->view('Location/EditLocation', $data); 
            }

        } else { 
          
            $this->load->view('Location/EditLocation', $data);
        }
    }

    function RelatedObject($LocationID, $RelatedObj) {

        $RelatedObjData =  $this->ActionsModel->getLocationData($LocationID, $RelatedObj); 
        $data['RelatedObjData'] = $RelatedObjData;
        $data['Data'] = $RelatedObjData['LocationData'];
        $data['RelatedObj'] = $RelatedObj;
        $this->load->view('Common/ObjectRelatedList/'.$RelatedObj, $data);
    }

    function CreateNote($LocationID)
    {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        if(isset($_POST['CreateNote'])){

            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('Body', 'body', 'required');
            if ($this->form_validation->run() == TRUE)
            { 
                $result = $this->ActionsModel->CreateNote($LocationID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Note saved successfully.');
                    redirect('Location/Actions/LocationDetails/'.$LocationID);

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Location/Actions/LocationDetails/'.$LocationID);
                }
            }
        }
    }

    function LocationDetails($LocationID) {

        $data =  $this->ActionsModel->getLocationData($LocationID); 
        $this->load->view('Location/LocationDetails', $data);
    }

    function LocationLineItem($LocationID)
    {
        $result =  $this->ActionsModel->LocationLineItem($LocationID); 
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Line item saved successfully.');
            redirect('Location/Actions/LocationDetails/'.$LocationID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Location/Actions/LocationDetails/'.$LocationID);
        }
    }

    function GetAccountDetails($AccountID)
    {
        echo $this->ActionsModel->GetAccountDetails($AccountID); 
    }

    function GetContactDetails($ContactID)
    {
        echo $this->ActionsModel->GetContactDetails($ContactID); 
    }

    function GetProductDetails($ProductID)
    {
        echo $this->ActionsModel->GetProductDetails($ProductID); 
    }

    function GetPrimaryContactOptions()
    {
        echo $this->ActionsModel->GetPrimaryContactOptions(); 
    }

    function SaveScheduling()
    {
        echo $this->ActionsModel->SaveScheduling(); 
    }

    function GetScheduling($EventID)
    {
        echo $this->ActionsModel->GetScheduling($EventID); 
    }

    function UpdateScheduling($EventID)
    {
        echo $this->ActionsModel->UpdateScheduling($EventID); 
    }

    function DeleteScheduling()
    {
        echo $this->ActionsModel->DeleteScheduling();
    }

    function EditSchAssignedTo()
    { 
        echo $this->ActionsModel->EditSchAssignedTo();
    }

    function UpdateAssignedTo()
    {
         echo $this->ActionsModel->UpdateAssignedTo();
    }

    function getState(){
        echo MY_Model::getState($_POST['CountryID']);
    }

    function getCity(){
        echo MY_Model::getCity($_POST['StateID']);
    } 

    function DeleteLocation($LocationID){
        $result =  $this->ActionsModel->DeleteLocation($LocationID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Work Order deleted successfully.');
            redirect('Location/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Location/Actions/LocationDetails/'.$LocationID); 
        }
    }
}
