<?php
 
class Actions extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Common/ActionsModel');
    }
  
    function CreateNewFile($FileID = 0) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
 
        if(isset($_POST['CreateFile'])){
 
            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('RelatedTo', 'related to', 'required');
            $this->form_validation->set_rules('What', 'what', 'required');
            $this->form_validation->set_rules('AssignedTo', 'assigned to', 'required');
            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->ActionsModel->CreateNewFile();    
                if($result == 1){

                    $this->session->set_flashdata('SuccessMsg', 'File created successfully.');
                    redirect($_POST['RelatedTo'].'/Actions/'.$_POST['RelatedTo'].'Details/'.$_POST['What']);

                } else {

                    $this->session->set_flashdata('ErrorMsg', $result);
                    redirect($_POST['RelatedTo'].'/Actions/'.$_POST['RelatedTo'].'Details/'.$_POST['What']);
                }
            } else {
                $this->session->set_flashdata('ErrorMsg', 'Please fill required fields.');
                redirect($_POST['RelatedTo'].'/Actions/'.$_POST['RelatedTo'].'Details/'.$_POST['What']);
            }
        } 
    }

    function SendEmail($RelatedTo, $What) { 
 
        $data['data'] =  array('RelatedTo' => $RelatedTo, 'What' => $What, 'ObjData' => $this->ActionsModel->GetRelatedObjData($RelatedTo, $What));
       
        if(isset($_POST['SendEmail'])){

            $result = $this->ActionsModel->SendEmail($RelatedTo, $What);    
            if($result){

                $this->session->set_flashdata('SuccessMsg', 'Email sent successfully.');
                redirect($RelatedTo.'/Actions/'.$RelatedTo.'Details/'.$What);

            } else {
                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Common/Actions/SendEmail/'.$RelatedTo.'/'.$What, $data);
            }
        } else { 

            $this->load->view('Common/SendEmail', $data);
        }
    }

    function CreateNewNote()
    {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        if(isset($_POST['CreateNote'])){

            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('Body', 'body', 'required');
            $this->form_validation->set_rules('RelatedTo', 'related to', 'required');
            $this->form_validation->set_rules('What', 'what', 'required');
            if ($this->form_validation->run() == TRUE)
            { 
                $result = $this->ActionsModel->CreateNewNote();    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Note saved successfully.');
                    redirect($_POST['RelatedTo'].'/Actions/'.$_POST['RelatedTo'].'Details/'.$_POST['What']);

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect($_POST['RelatedTo'].'/Actions/'.$_POST['RelatedTo'].'Details/'.$_POST['What']);
                }
            } else {

                $this->session->set_flashdata('ErrorMsg', 'Please fill required fields.');
                redirect($_POST['RelatedTo'].'/Actions/'.$_POST['RelatedTo'].'Details/'.$_POST['What']);
            }
        }
    }

    function GetTemplateDetails($EmailTemplateID)
    {
        echo $this->ActionsModel->GetTemplateDetails($EmailTemplateID); 
    }
}
