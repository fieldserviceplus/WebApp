<?php

class View extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('WorkOrder/ViewModel');

        $this->load->helper('url');
        // Load session
        $this->load->library('session');
        
        // Load Pagination library
        $this->load->library('pagination');
    }
   
    function index($WorkOrderViewID = 0) {

        if(isset($WorkOrderViewID) && $WorkOrderViewID != 0)
            $data['SelectedWorkOrder'] = $WorkOrderViewID;
        else 
            $data['SelectedWorkOrder'] = isset($_POST['SelectedWorkOrder'])?$_POST['SelectedWorkOrder']:"";

        if($data['SelectedWorkOrder'] == 'MyOpenWorkOrdersToday' 
            || $data['SelectedWorkOrder'] == 'MyOpenWorkOrdersThisWeek' 
            || $data['SelectedWorkOrder'] == 'AllOpenWorkOrdersToday' 
            || $data['SelectedWorkOrder'] == 'AllWorkOrdersToday' 
            || $data['SelectedWorkOrder'] == 'AllOpenWorkOrdersThisWeek' 
            || $data['SelectedWorkOrder'] == 'WorkOrdersCreatedToday' 
            || $data['SelectedWorkOrder'] == 'WorkOrdersCreatedThisWeek' 
            || $data['SelectedWorkOrder'] == 'UnscheduledWorkOrders' 
            || $data['SelectedWorkOrder'] == 'UnassignedWorkOrders' 
            || $data['SelectedWorkOrder'] == 'MyOpenWorkOrdersPastDue' 
            || $data['SelectedWorkOrder'] == 'AllOpenWorkOrdersPastDue' 
            || $data['SelectedWorkOrder'] == ""){
           
            $this->load->view('WorkOrder/WorkOrderView', $data);
        }
        else 
        {
            //return $this->List($data['SelectedContact']);

            $data['WorkOrderCustomViewData'] = $this->ViewModel->GetCustomView($data['SelectedWorkOrder']);
            //$data['WorkOrderData'] = $this->ViewModel->GetContactViewData($data['SelectedWorkOrder'], 0, 10, "", "");
            $this->load->view('WorkOrder/WorkOrderCustomView', $data);
        }
    }

    public function List($WorkOrderViewID, $rowno = 0, $rowperpage = 10, $sortBy = "", $order=""){

        $data['SelectedWorkOrder'] = $WorkOrderViewID;

        $config = array();
        //$rowperpage = 10;
        $config["cur_page"] = empty($rowno) ? 1 : $rowno;
        
        if($rowno != 0){ 
           $rowno = ($rowno - 1) * $rowperpage; 
        }

        $order = ($order != "") ? (($order == "asc") ? "desc" : "asc") : "";

        // Get records
        $data['WorkOrderCustomViewData'] = $this->ViewModel->GetCustomView($WorkOrderViewID);
        $data['WorkOrderData'] = $this->ViewModel->GetWorkOrderViewData($WorkOrderViewID, $rowno, $rowperpage, $sortBy, $order);

        // Total records count
        $allcount =  $data['WorkOrderData']['TotalRows'];
       
        // Pagination Configuration
        $config['base_url'] = base_url().'WorkOrder/View/List/'.$WorkOrderViewID;
        $config['first_url'] = '1';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $allcount;
        $config['num_links'] = 2;
        $config['per_page'] = $rowperpage;
        $config['attributes'] = array('class' => 'paginate_button');
        $config['cur_tag_open'] = '<a class="paginate_button current">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';

        $start = (int) ($allcount != 0) ? ($config["cur_page"]-1) * $config['per_page'] + 1 : 0;
        $end = (($config["cur_page"]-1) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int)($config["cur_page"]-1) * $config['per_page'] + $config['per_page'];

        $data['result_count']= "Showing ".$start." to ".$end." of ".$config['total_rows']." entries";

        // Initialize
        $this->pagination->initialize($config);
       
        $data['pagination'] = $this->pagination->create_links();
        $data['order'][$sortBy] = $order;

        echo json_encode($data);

    }

    function ViewWorkOrders() { 
        echo $this->ViewModel->ViewWorkOrders();
    }

    /*function ViewResult()
    {
         if(isset($_POST['FilterForm'])){

            extract($_POST);
            $result = $this->ViewModel->SaveWorkOrderFilter($FilterFields, $FilterConditions, $FilterValues); 
            $data['FilteredWorkOrders'] = $result; 
            $this->load->view('WorkOrder/WorkOrderViewResult', $data);

         } else { 
            $this->load->view('WorkOrder/WorkOrderViewResult');
        }
    }*/

    function CreateNewView() { 

        $data['WorkOrderViewFields'] = $this->ViewModel->WorkOrderViewFields(); 
        if(isset($_POST['CreateNewView'])){

            $WorkOrderViewID = $this->ViewModel->CreateNewView(); 

            if($WorkOrderViewID){

                $this->session->set_flashdata('SuccessMsg', 'WorkOrder new view created successfully.');
                redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('WorkOrder/View/CreateNewView', $data);
            }
        } else { 
            $this->load->view('WorkOrder/CreateNewView', $data);
        }
    }

    function EditCustomView($WorkOrderViewID) { 

        $data = $this->ViewModel->GetCustomView($WorkOrderViewID);

        if(isset($_POST['EditCustomView'])){

            $result = $this->ViewModel->EditCustomView($WorkOrderViewID); 

            if($result){

                $this->session->set_flashdata('SuccessMsg', 'WorkOrder view updated successfully.');
                redirect('WorkOrder/Actions');

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('WorkOrder/View/EditCustomView', $data);
            }
        } else { 
            $this->load->view('WorkOrder/EditCustomView', $data);
        }
    }

    function DeleteCustomView($WorkOrderViewID) { 
        $result =  $this->ViewModel->DeleteCustomView($WorkOrderViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'WorkOrder view deleted successfully.');
            redirect('WorkOrder/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);
        }
    }

    function CopyCustomView($WorkOrderViewID, $WorkOrderViewName)
    { 
        $result =  $this->ViewModel->CopyCustomView($WorkOrderViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'WorkOrder view copy successfully.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);
        }
    }

    function RenameCustomView($WorkOrderViewID){

        $result =  $this->ViewModel->RenameCustomView($WorkOrderViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'WorkOrder view rename successfully.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

        }
    }

    function EditSharingCustomView($WorkOrderViewID){

        $result =  $this->ViewModel->EditSharingCustomView($WorkOrderViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'WorkOrder view sharing updated successfully.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

        }
    }
  
    function EditFiltersCustomView($WorkOrderViewID)
    {
        $result =  $this->ViewModel->EditFiltersCustomView($WorkOrderViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'WorkOrder view filters updated successfully.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

        }
    }

    function EditDisplayedColumnsCustomView($WorkOrderViewID)
    {
        $result =  $this->ViewModel->EditDisplayedColumnsCustomView($WorkOrderViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'WorkOrder view displayed columns updated successfully.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('WorkOrder/View/Index/'.$WorkOrderViewID);

        }
    }
}
