<?php

class View extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Estimate/ViewModel');
    }
   
    function index($EstimateViewID = 0) {

        if(isset($EstimateViewID) && $EstimateViewID != 0)
            $data['SelectedEstimate'] = $EstimateViewID;
        else 
            $data['SelectedEstimate'] = isset($_POST['SelectedEstimate'])?$_POST['SelectedEstimate']:"";

        if($data['SelectedEstimate'] == 'MyOpenEstimates' 
            || $data['SelectedEstimate'] == 'AllOpenEstimates' 
            || $data['SelectedEstimate'] == 'EstimatesCreatedThisWeek'
            || $data['SelectedEstimate'] == ""){
           
            $this->load->view('Estimate/EstimateView', $data);
        }
        else 
        {
            $data['EstimateCustomViewData'] = $this->ViewModel->GetCustomView($data['SelectedEstimate']);
            $data['EstimateData'] = $this->ViewModel->GetEstimateViewData($data['SelectedEstimate']);
           
            $this->load->view('Estimate/EstimateCustomView', $data);
        }
    }

    function ViewEstimates() { 
        echo $this->ViewModel->ViewEstimates();
    }

    /*function ViewResult()
    {
         if(isset($_POST['FilterForm'])){

            extract($_POST);
            $result = $this->ViewModel->SaveEstimateFilter($FilterFields, $FilterConditions, $FilterValues); 
            $data['FilteredEstimates'] = $result; 
            $this->load->view('Estimate/EstimateViewResult', $data);

         } else { 
            $this->load->view('Estimate/EstimateViewResult');
        }
    }*/

    function CreateNewView() { 

        $data['EstimateViewFields'] = $this->ViewModel->EstimateViewFields(); 
        if(isset($_POST['CreateNewView'])){

            $EstimateViewID = $this->ViewModel->CreateNewView(); 

            if($EstimateViewID){

                $this->session->set_flashdata('SuccessMsg', 'Estimate new view created successfully.');
                redirect('Estimate/View/Index/'.$EstimateViewID);

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Estimate/View/CreateNewView', $data);
            }
        } else { 
            $this->load->view('Estimate/CreateNewView', $data);
        }
    }

    function EditCustomView($EstimateViewID) { 

        $data = $this->ViewModel->GetCustomView($EstimateViewID);

        if(isset($_POST['EditCustomView'])){

            $result = $this->ViewModel->EditCustomView($EstimateViewID); 

            if($result){

                $this->session->set_flashdata('SuccessMsg', 'Estimate view updated successfully.');
                redirect('Estimate/Actions');

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Estimate/View/EditCustomView', $data);
            }
        } else { 
            $this->load->view('Estimate/EditCustomView', $data);
        }
    }

    function DeleteCustomView($EstimateViewID) { 
        $result =  $this->ViewModel->DeleteCustomView($EstimateViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Estimate view deleted successfully.');
            redirect('Estimate/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Estimate/View/Index/'.$EstimateViewID);
        }
    }

    function CopyCustomView($EstimateViewID, $EstimateViewName)
    { 
        $result =  $this->ViewModel->CopyCustomView($EstimateViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Estimate view copy successfully.');
            redirect('Estimate/View/Index/'.$EstimateViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Estimate/View/Index/'.$EstimateViewID);
        }
    }

    function RenameCustomView($EstimateViewID){

        $result =  $this->ViewModel->RenameCustomView($EstimateViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Estimate view rename successfully.');
            redirect('Estimate/View/Index/'.$EstimateViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Estimate/View/Index/'.$EstimateViewID);

        }
    }

    function EditSharingCustomView($EstimateViewID){

        $result =  $this->ViewModel->EditSharingCustomView($EstimateViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Estimate view sharing updated successfully.');
            redirect('Estimate/View/Index/'.$EstimateViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Estimate/View/Index/'.$EstimateViewID);

        }
    }
  
    function EditFiltersCustomView($EstimateViewID)
    {
        $result =  $this->ViewModel->EditFiltersCustomView($EstimateViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Estimate view filters updated successfully.');
            redirect('Estimate/View/Index/'.$EstimateViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Estimate/View/Index/'.$EstimateViewID);

        }
    }

    function EditDisplayedColumnsCustomView($EstimateViewID)
    {
        $result =  $this->ViewModel->EditDisplayedColumnsCustomView($EstimateViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Estimate view displayed columns updated successfully.');
            redirect('Estimate/View/Index/'.$EstimateViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Estimate/View/Index/'.$EstimateViewID);

        }
    }
}
