<?php
class Actions extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Estimate/ActionsModel');
    }
   
    function index() { 
        $this->load->view('Estimate/EstimateHome');
    }

    function RecentEstimates() { 
        echo $this->ActionsModel->RecentEstimates();
    }
  
    function CreateEstimate($EstimateID = 0) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
 
        if(isset($_POST['CreateEstimate'])){

            $this->form_validation->set_rules('EstimateName', 'estimate name', 'required');
            $this->form_validation->set_rules('Owner', 'owner', 'required');
            $this->form_validation->set_rules('Account', 'account name', 'required');
            $this->form_validation->set_rules('Description', 'description', 'required');
            $this->form_validation->set_rules('BillingName', 'billing name', 'required');  
            $this->form_validation->set_rules('BillingAddress', 'address', 'required');  
            $this->form_validation->set_rules('BillingCity', 'city', 'required');
            $this->form_validation->set_rules('BillingState', 'state', 'required');
            $this->form_validation->set_rules('BillingCountry', 'country', 'required');
            $this->form_validation->set_rules('BillingPostalCode', 'postal code', 'required');
            $this->form_validation->set_rules('ShippingName', 'billing name', 'required');  
            $this->form_validation->set_rules('ShippingAddress', 'address', 'required');  
            $this->form_validation->set_rules('ShippingCity', 'city', 'required');
            $this->form_validation->set_rules('ShippingState', 'state', 'required');
            $this->form_validation->set_rules('ShippingCountry', 'country', 'required');
            $this->form_validation->set_rules('ShippingPostalCode', 'postal code', 'required');
            $this->form_validation->set_rules('EstimateStatus', 'status', 'required');
            $this->form_validation->set_rules('ExpirationDate', 'end date', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {   
                $EstimateID = $this->ActionsModel->CreateEstimate();    
                if(!empty($EstimateID)){ 

                   /* if(isset($_POST['Redirect']) && $_POST['Redirect'] = 'Account'){
                        $this->session->set_flashdata('SuccessMsg', 'Estimate created successfully.');
                        redirect('Account/Actions/AccountDetails/'.$_POST['Account']);
                    } else */{
                        $this->session->set_flashdata('SuccessMsg', 'Estimate created successfully.');
                        redirect('Estimate/Actions/EstimateDetails/'.$EstimateID); 
                    }
                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Estimate/Actions/CreateEstimate');
                }
            } else {
                $this->load->view('Estimate/CreateEstimate');
            }

        } else { 

            $data =  $this->ActionsModel->getEstimateData($EstimateID);
            $this->load->view('Estimate/CreateEstimate', $data);
        }
    }

    function EditEstimate($EstimateID) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data =  $this->ActionsModel->getEstimateData($EstimateID); 

        if(isset($_POST['EditEstimate'])){

            $this->form_validation->set_rules('EstimateName', 'estimate name', 'required');
            $this->form_validation->set_rules('Owner', 'owner', 'required');
            $this->form_validation->set_rules('Account', 'account name', 'required');
            $this->form_validation->set_rules('Description', 'description', 'required');
            $this->form_validation->set_rules('BillingName', 'billing name', 'required');  
            $this->form_validation->set_rules('BillingAddress', 'address', 'required');  
            $this->form_validation->set_rules('BillingCity', 'city', 'required');
            $this->form_validation->set_rules('BillingState', 'state', 'required');
            $this->form_validation->set_rules('BillingCountry', 'country', 'required');
            $this->form_validation->set_rules('BillingPostalCode', 'postal code', 'required');
            $this->form_validation->set_rules('ShippingName', 'billing name', 'required');  
            $this->form_validation->set_rules('ShippingAddress', 'address', 'required');  
            $this->form_validation->set_rules('ShippingCity', 'city', 'required');
            $this->form_validation->set_rules('ShippingState', 'state', 'required');
            $this->form_validation->set_rules('ShippingCountry', 'country', 'required');
            $this->form_validation->set_rules('ShippingPostalCode', 'postal code', 'required');
            $this->form_validation->set_rules('EstimateStatus', 'status', 'required');
            $this->form_validation->set_rules('ExpirationDate', 'end date', 'required');

            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->ActionsModel->EditEstimate($EstimateID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Estimate updated successfully.');
                    redirect('Estimate/Actions/EstimateDetails/'.$EstimateID); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Estimate/Actions/EditEstimate/'.$EstimateID); 
                }
            } else { 
                $this->load->view('Estimate/EditEstimate', $data); 
            }

        } else { 
          
            $this->load->view('Estimate/EditEstimate', $data);
        }
    }

    function RelatedObject($EstimateID, $RelatedObj) {

        $RelatedObjData =  $this->ActionsModel->getEstimateData($EstimateID, $RelatedObj); 
        $data['RelatedObjData'] = $RelatedObjData;
        $data['Data'] = $RelatedObjData['EstimateData'];
        $data['RelatedObj'] = $RelatedObj;
        $this->load->view('Common/ObjectRelatedList/'.$RelatedObj, $data);
    }

    function CreateNote($EstimateID)
    {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        if(isset($_POST['CreateNote'])){

            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('Body', 'body', 'required');
            if ($this->form_validation->run() == TRUE)
            { 
                $result = $this->ActionsModel->CreateNote($EstimateID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Note saved successfully.');
                    redirect('Estimate/Actions/EstimateDetails/'.$EstimateID);

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Estimate/Actions/EstimateDetails/'.$EstimateID);
                }
            }
        }
    }

    function EstimateDetails($EstimateID) {

        $data =  $this->ActionsModel->getEstimateData($EstimateID); 
        $this->load->view('Estimate/EstimateDetails', $data);
    }

    function EstimateLine($EstimateID)
    {
        $result =  $this->ActionsModel->EstimateLine($EstimateID); 
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Line item saved successfully.');
            redirect('Estimate/Actions/EstimateDetails/'.$EstimateID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Estimate/Actions/EstimateDetails/'.$EstimateID);
        }
    }

    function ConvertToWorkOrder($EstimateID)
    {
        $WorkOrderID =  $this->ActionsModel->ConvertToWorkOrder($EstimateID); 
        if(!empty($WorkOrderID)){

            $this->session->set_flashdata('SuccessMsg', 'Work order converted successfully.');
            redirect('WorkOrder/Actions/WorkOrderDetails/'.$WorkOrderID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Estimate/Actions/EstimateDetails/'.$EstimateID);
        }
    }

    function GetAccountDetails($AccountID)
    {
        echo $this->ActionsModel->GetAccountDetails($AccountID); 
    }

    function GetContactDetails($ContactID)
    {
        echo $this->ActionsModel->GetContactDetails($ContactID); 
    }

    function GetProductDetails($ProductID)
    {
        echo $this->ActionsModel->GetProductDetails($ProductID); 
    }

    function GetPrimaryContactOptions()
    {
        echo $this->ActionsModel->GetPrimaryContactOptions(); 
    }

    function SaveScheduling()
    {
        echo $this->ActionsModel->SaveScheduling(); 
    }

    function GetScheduling($EventID)
    {
        echo $this->ActionsModel->GetScheduling($EventID); 
    }

    function UpdateScheduling($EventID)
    {
        echo $this->ActionsModel->UpdateScheduling($EventID); 
    }

    function DeleteScheduling()
    {
        echo $this->ActionsModel->DeleteScheduling();
    }

    function EditSchAssignedTo()
    { 
        echo $this->ActionsModel->EditSchAssignedTo();
    }

    function UpdateAssignedTo()
    {
         echo $this->ActionsModel->UpdateAssignedTo();
    }

    function getState(){
        echo MY_Model::getState($_POST['CountryID']);
    }

    function getCity(){
        echo MY_Model::getCity($_POST['StateID']);
    } 

    function DeleteEstimate($EstimateID){
        $result =  $this->ActionsModel->DeleteEstimate($EstimateID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Work Order deleted successfully.');
            redirect('Estimate/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Estimate/Actions/EstimateDetails/'.$EstimateID); 
        }
    }

    function GenerateDocument($EstimateID) { 

        $data =  $this->ActionsModel->getEstimateData($EstimateID, 'EstimateLines'); 

        if(isset($_REQUEST['GenerateDocument'])){

            $result = $this->ActionsModel->GenerateDocument($data);
            if($result == 2){
                
                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Estimate/Actions/GenerateDocument/'.$EstimateID); 
                
            } else {

                $data['PDFOutputFile'] = $result;
                $this->load->view('Estimate/GenerateDocument', $data);
            }

        } else {
            
            $this->load->view('Estimate/GenerateDocument', $data);

        }
    }
}
