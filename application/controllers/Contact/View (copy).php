<?php

class View extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Contact/ViewModel');
    }
   
    function index($ContactViewID = 0) {

        if(isset($ContactViewID) && $ContactViewID != 0)
            $data['SelectedContact'] = $ContactViewID;
        else 
            $data['SelectedContact'] = isset($_POST['SelectedContact'])?$_POST['SelectedContact']:"";

        if($data['SelectedContact'] == 'MyActiveContacts' || $data['SelectedContact'] == 'AllActiveContacts' || $data['SelectedContact'] == 'ContactsCreatedThisWeek' || $data['SelectedContact'] == ""){
            $this->load->view('Contact/ContactView', $data);
        }
        else 
        {
            $data['ContactCustomViewData'] = $this->ViewModel->GetCustomView($data['SelectedContact']);
            $data['ContactData'] = $this->ViewModel->GetContactViewData($data['SelectedContact']);
           
            $this->load->view('Contact/ContactCustomView', $data);
        }
    }

    function ViewContacts() { 
        echo $this->ViewModel->ViewContacts();
    }

    /*function ViewResult()
    {
         if(isset($_POST['FilterForm'])){

            extract($_POST);
            $result = $this->ViewModel->SaveContactFilter($FilterFields, $FilterConditions, $FilterValues); 
            $data['FilteredContacts'] = $result; 
            $this->load->view('Contact/ContactViewResult', $data);

         } else { 
            $this->load->view('Contact/ContactViewResult');
        }
    }*/

    function CreateNewView() { 

        $data['ContactViewFields'] = $this->ViewModel->ContactViewFields(); 
        if(isset($_POST['CreateNewView'])){

            $ContactViewID = $this->ViewModel->CreateNewView(); 

            if($ContactViewID){

                $this->session->set_flashdata('SuccessMsg', 'Contact new view created successfully.');
                redirect('Contact/View/Index/'.$ContactViewID);

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Contact/View/CreateNewView', $data);
            }
        } else { 
            $this->load->view('Contact/CreateNewView', $data);
        }
    }

    function EditCustomView($ContactViewID) { 

        $data = $this->ViewModel->GetCustomView($ContactViewID);

        if(isset($_POST['EditCustomView'])){

            $result = $this->ViewModel->EditCustomView($ContactViewID); 

            if($result){

                $this->session->set_flashdata('SuccessMsg', 'Contact view updated successfully.');
                redirect('Contact/Actions');

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Contact/View/EditCustomView', $data);
            }
        } else { 
            $this->load->view('Contact/EditCustomView', $data);
        }
    }

    function DeleteCustomView($ContactViewID) { 
        $result =  $this->ViewModel->DeleteCustomView($ContactViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Contact view deleted successfully.');
            redirect('Contact/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Contact/View/Index/'.$ContactViewID);
        }
    }

    function CopyCustomView($ContactViewID, $ContactViewName)
    { 
        $result =  $this->ViewModel->CopyCustomView($ContactViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Contact view copy successfully.');
            redirect('Contact/View/Index/'.$ContactViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Contact/View/Index/'.$ContactViewID);
        }
    }

    function RenameCustomView($ContactViewID){

        $result =  $this->ViewModel->RenameCustomView($ContactViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Contact view rename successfully.');
            redirect('Contact/View/Index/'.$ContactViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Contact/View/Index/'.$ContactViewID);

        }
    }

    function EditSharingCustomView($ContactViewID){

        $result =  $this->ViewModel->EditSharingCustomView($ContactViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Contact view sharing updated successfully.');
            redirect('Contact/View/Index/'.$ContactViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Contact/View/Index/'.$ContactViewID);

        }
    }
  
    function EditFiltersCustomView($ContactViewID)
    {
        $result =  $this->ViewModel->EditFiltersCustomView($ContactViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Contact view filters updated successfully.');
            redirect('Contact/View/Index/'.$ContactViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Contact/View/Index/'.$ContactViewID);

        }
    }

    function EditDisplayedColumnsCustomView($ContactViewID)
    {
        $result =  $this->ViewModel->EditDisplayedColumnsCustomView($ContactViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Contact view displayed columns updated successfully.');
            redirect('Contact/View/Index/'.$ContactViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Contact/View/Index/'.$ContactViewID);

        }
    }
}
