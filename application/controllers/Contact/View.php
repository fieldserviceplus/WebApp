<?php

class View extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Contact/ViewModel');

        $this->load->helper('url');
        // Load session
        $this->load->library('session');
        
        // Load Pagination library
        $this->load->library('pagination');
    }
   
    /*function index($ContactViewID = 0) {

        if(isset($ContactViewID) && $ContactViewID != 0)
            $data['SelectedContact'] = $ContactViewID;
        else 
            $data['SelectedContact'] = isset($_POST['SelectedContact'])?$_POST['SelectedContact']:"";

        if($data['SelectedContact'] == 'MyActiveContacts' || $data['SelectedContact'] == 'AllActiveContacts' || $data['SelectedContact'] == 'ContactsCreatedThisWeek' || $data['SelectedContact'] == ""){
            $this->load->view('Contact/ContactView', $data);
        }
        else 
        {
            $data['ContactCustomViewData'] = $this->ViewModel->GetCustomView($data['SelectedContact']);
            $data['ContactData'] = $this->ViewModel->GetContactViewData($data['SelectedContact']);
           
            $this->load->view('Contact/ContactCustomView', $data);
        }
    }*/

    function index($ContactViewID = 0){

        if(isset($ContactViewID) && $ContactViewID != 0)
            $data['SelectedContact'] = $ContactViewID;
        else 
            $data['SelectedContact'] = isset($_POST['SelectedContact'])?$_POST['SelectedContact']:"";

        if($data['SelectedContact'] == 'MyActiveContacts' || $data['SelectedContact'] == 'AllActiveContacts' || $data['SelectedContact'] == 'ContactsCreatedThisWeek' || $data['SelectedContact'] == ""){
            $this->load->view('Contact/ContactView', $data);
        }
        else 
        {
            //return $this->List($data['SelectedContact']);

            $data['ContactCustomViewData'] = $this->ViewModel->GetCustomView($data['SelectedContact']);
            //$data['ContactData'] = $this->ViewModel->GetContactViewData($data['SelectedContact'], 0, 10, "", "");
            $this->load->view('Contact/ContactCustomView', $data);
        }
    }

    public function List($ContactViewID, $rowno = 0, $rowperpage = 10, $sortBy = "", $order=""){

        $data['SelectedContact'] = $ContactViewID;

        $config = array();
        //$rowperpage = 10;
        $config["cur_page"] = empty($rowno) ? 1 : $rowno;
        
        if($rowno != 0){ 
           $rowno = ($rowno - 1) * $rowperpage; 
        }

        $order = ($order != "") ? (($order == "asc") ? "desc" : "asc") : "";

        // Get records
        $data['ContactCustomViewData'] = $this->ViewModel->GetCustomView($ContactViewID);
        $data['ContactData'] = $this->ViewModel->GetContactViewData($ContactViewID, $rowno, $rowperpage, $sortBy, $order);

        // Total records count
        $allcount =  $data['ContactData']['TotalRows'];
       
        // Pagination Configuration
        $config['base_url'] = base_url().'Contact/View/List/'.$ContactViewID;
        $config['first_url'] = '1';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $allcount;
        $config['num_links'] = 2;
        $config['per_page'] = $rowperpage;
        $config['attributes'] = array('class' => 'paginate_button');
        $config['cur_tag_open'] = '<a class="paginate_button current">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';

        $start = (int) ($allcount != 0) ? ($config["cur_page"]-1) * $config['per_page'] + 1 : 0;
        $end = (($config["cur_page"]-1) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int)($config["cur_page"]-1) * $config['per_page'] + $config['per_page'];

        $data['result_count']= "Showing ".$start." to ".$end." of ".$config['total_rows']." entries";

        // Initialize
        $this->pagination->initialize($config);
       
        $data['pagination'] = $this->pagination->create_links();
        $data['order'][$sortBy] = $order;

        echo json_encode($data);

    }

    function ViewContacts() { 
        echo $this->ViewModel->ViewContacts();
    }

    /*function ViewResult()
    {
         if(isset($_POST['FilterForm'])){

            extract($_POST);
            $result = $this->ViewModel->SaveContactFilter($FilterFields, $FilterConditions, $FilterValues); 
            $data['FilteredContacts'] = $result; 
            $this->load->view('Contact/ContactViewResult', $data);

         } else { 
            $this->load->view('Contact/ContactViewResult');
        }
    }*/

    function CreateNewView() { 

        $data['ContactViewFields'] = $this->ViewModel->ContactViewFields(); 
        if(isset($_POST['CreateNewView'])){

            $ContactViewID = $this->ViewModel->CreateNewView(); 

            if($ContactViewID){

                $this->session->set_flashdata('SuccessMsg', 'Contact new view created successfully.');
                redirect('Contact/View/Index/'.$ContactViewID);

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Contact/View/CreateNewView', $data);
            }
        } else { 
            $this->load->view('Contact/CreateNewView', $data);
        }
    }

    function EditCustomView($ContactViewID) { 

        $data = $this->ViewModel->GetCustomView($ContactViewID);

        if(isset($_POST['EditCustomView'])){

            $result = $this->ViewModel->EditCustomView($ContactViewID); 

            if($result){

                $this->session->set_flashdata('SuccessMsg', 'Contact view updated successfully.');
                redirect('Contact/Actions');

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Contact/View/EditCustomView', $data);
            }
        } else { 
            $this->load->view('Contact/EditCustomView', $data);
        }
    }

    function DeleteCustomView($ContactViewID) { 
        $result =  $this->ViewModel->DeleteCustomView($ContactViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Contact view deleted successfully.');
            redirect('Contact/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Contact/View/Index/'.$ContactViewID);
        }
    }

    function CopyCustomView($ContactViewID, $ContactViewName)
    { 
        $result =  $this->ViewModel->CopyCustomView($ContactViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Contact view copy successfully.');
            redirect('Contact/View/Index/'.$ContactViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Contact/View/Index/'.$ContactViewID);
        }
    }

    function RenameCustomView($ContactViewID){

        $result =  $this->ViewModel->RenameCustomView($ContactViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Contact view rename successfully.');
            redirect('Contact/View/Index/'.$ContactViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Contact/View/Index/'.$ContactViewID);

        }
    }

    function EditSharingCustomView($ContactViewID){

        $result =  $this->ViewModel->EditSharingCustomView($ContactViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Contact view sharing updated successfully.');
            redirect('Contact/View/Index/'.$ContactViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Contact/View/Index/'.$ContactViewID);

        }
    }
  
    function EditFiltersCustomView($ContactViewID)
    {
        $result =  $this->ViewModel->EditFiltersCustomView($ContactViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Contact view filters updated successfully.');
            redirect('Contact/View/Index/'.$ContactViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Contact/View/Index/'.$ContactViewID);

        }
    }

    function EditDisplayedColumnsCustomView($ContactViewID)
    {
        $result =  $this->ViewModel->EditDisplayedColumnsCustomView($ContactViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Contact view displayed columns updated successfully.');
            redirect('Contact/View/Index/'.$ContactViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Contact/View/Index/'.$ContactViewID);

        }
    }
}
