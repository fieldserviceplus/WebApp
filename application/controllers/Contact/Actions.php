<?php
 
class Actions extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Contact/ActionsModel');
    }
   
    function index() { 
        $this->load->view('Contact/ContactHome');
    }

    function RecentContacts() { 
        echo $this->ActionsModel->RecentContacts();
    }
  
    function CreateContact($ContactID = 0) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
 
        if(isset($_POST['CreateContact'])){
 
            $this->form_validation->set_rules('Salutation', 'salutation', 'required');
            $this->form_validation->set_rules('FirstName', 'first name', 'required');
            $this->form_validation->set_rules('LastName', 'last name', 'required');
            $this->form_validation->set_rules('Title', 'title', 'required');
            $this->form_validation->set_rules('AssignedTo', 'assigned to', 'required');
            $this->form_validation->set_rules('Account', 'account', 'required');
            $this->form_validation->set_rules('LeadSource', 'lead source', 'required');
            $this->form_validation->set_rules('MailingAddress', 'mailing address', 'required'); 
            $this->form_validation->set_rules('MailingCity', 'mailing city', 'required');
            $this->form_validation->set_rules('MailingState', 'mailing state', 'required');
            $this->form_validation->set_rules('MailingCountry', 'mailing country', 'required');
            $this->form_validation->set_rules('MailingPostalCode', 'mailing postal code', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {  
                $ContactID = $this->ActionsModel->CreateContact();    
                if(!empty($ContactID)){

                    $this->session->set_flashdata('SuccessMsg', 'Contact created successfully.');
                    redirect('Contact/Actions/ContactDetails/'.$ContactID);

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Contact/Actions/CreateContact');
                }
            } else {
                $this->load->view('Contact/CreateContact');
            }

        } else { 

            $data =  $this->ActionsModel->getContactData($ContactID); 
            $this->load->view('Contact/CreateContact', $data);
        }
    }

    function ContactDetails($ContactID) {

        $data =  $this->ActionsModel->getContactData($ContactID); 
        $this->load->view('Contact/ContactDetails', $data);
    }

    function RelatedObject($ContactID, $RelatedObj) {

        $RelatedObjData =  $this->ActionsModel->getContactData($ContactID, $RelatedObj); 
        $data['RelatedObjData'] = $RelatedObjData;
        $data['Data'] = $RelatedObjData['ContactData'];
        $data['RelatedObj'] = $RelatedObj;
        $this->load->view('Common/ObjectRelatedList/'.$RelatedObj, $data);
    }

    function EditContact($ContactID) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data =  $this->ActionsModel->getContactData($ContactID); 

        if(isset($_POST['EditContact'])){

            $this->form_validation->set_rules('Salutation', 'salutation', 'required');
            $this->form_validation->set_rules('FirstName', 'first name', 'required');
            $this->form_validation->set_rules('LastName', 'last name', 'required');
            $this->form_validation->set_rules('Title', 'title', 'required');
            $this->form_validation->set_rules('AssignedTo', 'assigned to', 'required');
            $this->form_validation->set_rules('Account', 'account', 'required');
            $this->form_validation->set_rules('LeadSource', 'lead source', 'required');
            $this->form_validation->set_rules('MailingAddress', 'mailing address', 'required'); 
            $this->form_validation->set_rules('MailingCity', 'mailing city', 'required');
            $this->form_validation->set_rules('MailingState', 'mailing state', 'required');
            $this->form_validation->set_rules('MailingCountry', 'mailing country', 'required');
            $this->form_validation->set_rules('MailingPostalCode', 'mailing postal code', 'required');
             
            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->ActionsModel->EditContact($ContactID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Contact updated successfully.');
                    redirect('Contact/Actions/ContactDetails/'.$ContactID); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Contact/Actions/EditContact/'.$ContactID); 
                }
            } else { 
                $this->load->view('Contact/EditContact', $data); 
            }

        } else { 
           
            $this->load->view('Contact/EditContact', $data);
        }
    }

    function DeleteContact($ContactID){
        $result =  $this->ActionsModel->DeleteContact($ContactID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Contact deleted successfully.');
            redirect('Contact/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Contact/Actions/ContactDetails/'.$ContactID); 
        }
    }

    /*function CreateNote($ContactID)
    {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        if(isset($_POST['CreateNote'])){

            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('Body', 'body', 'required');
            if ($this->form_validation->run() == TRUE)
            { 
                $result = $this->ActionsModel->CreateNote($ContactID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Note saved successfully.');
                    redirect('Contact/Actions/ContactDetails/'.$ContactID);

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Contact/Actions/ContactDetails/'.$ContactID);
                }
            }
        }
    }*/

    /*function SendEmail($ContactID) { 

        if(isset($_POST['SendEmail'])){

            $result = $this->ActionsModel->SendEmail($ContactID);    
            if($result){

                $this->session->set_flashdata('SuccessMsg', 'Email sent successfully.');
                redirect('Contact/Actions/ContactDetails/'.$ContactID);

            } else {

                $data =  $this->ActionsModel->getContactData($ContactID); 
                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Contact/Actions/SendEmail', $data);
            }
        } else { 

            $data =  $this->ActionsModel->getContactData($ContactID); 
            $this->load->view('Contact/SendEmail', $data);
        }
    }*/
}
