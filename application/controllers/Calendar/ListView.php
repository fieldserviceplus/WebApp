<?php
 
class ListView extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Calendar/ListViewModel');
    }
   
    function index() { 

        $data = $this->ListViewModel->FilterData();
        $this->load->view('Calendar/ListDay', $data);
    }

    function GetList() { 
        echo $this->ListViewModel->GetList();
    }

    function Week() { 
        $data = $this->ListViewModel->FilterData();
        $this->load->view('Calendar/ListWeek', $data);
    }

    function Month() { 
        $data = $this->ListViewModel->FilterData();
        $this->load->view('Calendar/ListMonth', $data);
    }
}
