<?php
 
class CalendarView extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Calendar/CalendarViewModel');
    }
   
    function index() { 
       
        $data = $this->CalendarViewModel->FilterData();
        $data['UnassignedUnscheduled'] = $this->CalendarViewModel->GetUnassignedUnscheduled();
       
        $this->load->view('Calendar/Calendar', $data);
    }

    function Calendar() { 
        echo $this->CalendarViewModel->Calendar();
    }

    function Week() { 
        
        $data = $this->CalendarViewModel->FilterData();
        $this->load->view('Calendar/CalendarWeek', $data);
    }

    function Month() { 
        $data = $this->CalendarViewModel->FilterData();
        $this->load->view('Calendar/CalendarMonth', $data);
    }

    function UpdateWorkOrderDetails()
    {
        echo $this->CalendarViewModel->UpdateWorkOrderDetails();
    }

    function UpdateEventTaskDetails()
    {
        echo $this->CalendarViewModel->UpdateEventTaskDetails();
    }

    function GetEventTaskDetails()
    {
        echo $this->CalendarViewModel->GetEventTaskDetails();
    }
    
}
