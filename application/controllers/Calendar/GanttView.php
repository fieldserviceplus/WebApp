<?php
 
class GanttView extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Calendar/GanttViewModel');
    }
   
    function index() { 

        $data = $this->GanttViewModel->FilterData();
        $data['UnassignedUnscheduled'] = $this->GanttViewModel->GetUnassignedUnscheduled();
        
        $this->load->view('Calendar/Gantt', $data);
    }

    function Gantt() { 
        echo $this->GanttViewModel->Gantt();
    }

    function Week() { 
        $data = $this->GanttViewModel->FilterData();
        $this->load->view('Calendar/GanttWeek', $data);
    }

    function GetAssignToResource()
    {
       echo $this->GanttViewModel->GetAssignToResource();
    }

    function UpdateWorkOrderDetails()
    {
        echo $this->GanttViewModel->UpdateWorkOrderDetails();
    }

    function UpdateEventTaskDetails()
    {
        echo $this->GanttViewModel->UpdateEventTaskDetails();
    }

    function GetEventTaskDetails()
    {
        echo $this->GanttViewModel->GetEventTaskDetails();
    }

}
