<?php
 
class MapView extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Calendar/MapViewModel');
    }
   
    function index() { 

        $data = $this->MapViewModel->FilterData();
        $this->load->view('Calendar/MapDay', $data);
    }

    function Map() { 
        echo $this->MapViewModel->Map();
    }

    function Week() { 
        $data = $this->MapViewModel->FilterData();
        $this->load->view('Calendar/MapWeek', $data);
    }

    function GetTechnicians()
    {
        echo $this->MapViewModel->GetTechnicians();
    }
}
