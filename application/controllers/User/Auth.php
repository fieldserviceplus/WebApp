<?php

class Auth extends MY_Controller {

    function __construct() { 
        parent::__construct();
        $this->load->model('User/AuthModel');

        if(($this->session->userdata('UserID') != NULL) && ($this->uri->segment(3) != 'Logout')){
            redirect('User/Home');
        } 
    }

    function SignUp() { 
      
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
 
        if(isset($_POST['SignUp'])){

            $this->form_validation->set_rules('FirstName', 'firstname', 'required');
            $this->form_validation->set_rules('LastName', 'lastname', 'required');
            $this->form_validation->set_rules('CompanyName', 'company name', 'required');
            $this->form_validation->set_rules('Phone', 'phone no', 'required');
            $this->form_validation->set_rules('Website', 'website no', 'required');
            $this->form_validation->set_rules('CompanyEdition', 'edition', 'required');
            $this->form_validation->set_rules('BillingAddress', 'address', 'required');
            $this->form_validation->set_rules('BillingCity', 'city', 'required');
            $this->form_validation->set_rules('BillingState', 'state', 'required');
            $this->form_validation->set_rules('BillingCountry', 'country', 'required');
            $this->form_validation->set_rules('BillingPostalCode', 'postal code', 'required');
            $this->form_validation->set_rules('Plan', 'plan', 'required');
            $this->form_validation->set_rules('ContractType', 'contract', 'required');
            $this->form_validation->set_rules('Email', 'email', 'required|valid_email|is_unique[User.Email]');
            $this->form_validation->set_rules('Password', 'password', 'required');
            $this->form_validation->set_rules('ConfirmPassword', 'confirm password', 'required|matches[Password]');  
            $this->form_validation->set_rules('TermsConditions', 'terms & conditions', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {
                extract($_POST); 

                $result = $this->AuthModel->SignUp();  
                if($result == 1){

                    $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">Yay! Sign-up successfully.</p><div class="clearfix"></div></div>');
                    redirect('User/Auth/SignUp');

                } else if($result == 2) {

                    $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">The email address you have entered is already registered.</p><div class="clearfix"></div></div>');
                    redirect('User/Auth/SignUp');
                } else {

                    $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.</p><div class="clearfix"></div></div>');
                    redirect('User/Auth/SignUp');
                }
            } else {
                $this->load->view('User/SignUpView');
            }

        } else {
            $this->load->view('User/SignUpView');
        }
    }
    
    function Login(){
 
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
 
        if(isset($_POST['Login'])){

            $this->form_validation->set_rules('Email', 'email', 'required');
            $this->form_validation->set_rules('Password', 'password', 'required');

            if ($this->form_validation->run() == TRUE)
            {
                extract($_POST);
                $result = $this->AuthModel->Login($Email, $Password);
          
                if($result){
                        redirect('User/Home');
                } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Email or password wrong.</p><div class="clearfix"></div></div>');
                        redirect('User/Auth/Login');
                }
            } else {
                $this->load->view('User/LoginView');
            }
        
        } else {
            $this->load->view('User/LoginView');
        }
    }

    function ForgotPassword(){
 
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
 
        if(isset($_POST['ForgotPassword'])){

            $this->form_validation->set_rules('Email', 'email', 'required');

            if ($this->form_validation->run() == TRUE)
            { 
                extract($_POST);
                $result = $this->AuthModel->ForgotPassword($Email);
          
                if($result){
                        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="zmdi zmdi-check pr-15 pull-left"></i><p class="">Yay! A new password has been sent to your e-mail address.</p><div class="clearfix"></div></div>'); 
                        redirect('User/Auth/ForgotPassword'); 
                } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Email not found.</p><div class="clearfix"></div></div>');
                        redirect('User/Auth/ForgotPassword');
                }
            } else {
                $this->load->view('User/ForgotPasswordView');
            }
        
        } else {
            $this->load->view('User/ForgotPasswordView');
        }
    }

    function Logout(){ 
        session_destroy();

        redirect('User/Auth/Login');
    }

    function GetPlanDetails($Plan){ 
       echo $this->AuthModel->GetPlanDetails($Plan);
    }

}
