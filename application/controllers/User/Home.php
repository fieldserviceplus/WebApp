<?php

class Home extends MY_Controller {

    function __construct() {
        parent::__construct();
       // $this->load->model('User/AuthModel');
        $this->load->model('User/HomeModel');
    }

    function index() { 

        $this->load->view('User/DashboardView');
    }

    function GetTaskData(){ 
       echo $this->HomeModel->GetTaskData();
    }
}
