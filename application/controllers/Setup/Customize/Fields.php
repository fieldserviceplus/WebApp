<?php
class Fields extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Setup/Customize/FieldsModel');
    }
   
    /*function index() {  
        $data = $this->FieldsModel->GetStandardAccountFields(); 
        $this->load->view('Setup/Customize/Fields/Fields', $data);
    }*/

    function Account() { 

        $data = $this->FieldsModel->GetStandardFields('Account'); 
        $data['Object'] = 'Account';
        $this->load->view('Setup/Customize/Fields/FieldList', $data);
    }

    function Contact() { 

        $data = $this->FieldsModel->GetStandardFields('Contact'); 
        $data['Object'] = 'Contact';
        $this->load->view('Setup/Customize/Fields/FieldList', $data);
    }

    function WorkOrder() { 

        $data = $this->FieldsModel->GetStandardFields('WorkOrder'); 
        $data['Object'] = 'WorkOrder';
        $this->load->view('Setup/Customize/Fields/FieldList', $data);
    }

    function Estimate() { 

        $data = $this->FieldsModel->GetStandardFields('Estimate'); 
        $data['Object'] = 'Estimate';
        $this->load->view('Setup/Customize/Fields/FieldList', $data);
    }

    function Invoice() { 

        $data = $this->FieldsModel->GetStandardFields('Invoice'); 
        $data['Object'] = 'Invoice';
        $this->load->view('Setup/Customize/Fields/FieldList', $data);
    }

    function Task() { 

        $data = $this->FieldsModel->GetStandardFields('Task'); 
        $data['Object'] = 'Task';
        $this->load->view('Setup/Customize/Fields/FieldList', $data);
    }

    function CreateCustomField($Object, $CustomFieldID = 0) { 

        $data['CustomFieldData'] = $this->FieldsModel->GetCustomFieldData($Object, $CustomFieldID);
        $data['Object'] = $Object;

        if((isset($_POST['CreateCustomFieldStep1']) && isset($_POST['FieldType'])) || isset($_POST['CreateCustomFieldStep2'])){

            $this->load->helper(array('form'));
            $this->load->library('form_validation');

            $data['FieldType'] = $_POST['FieldType'];

            if(isset($_POST['CreateCustomFieldStep2'])){
 
                $this->form_validation->set_rules('FieldName', 'field name', 'required');
                $this->form_validation->set_rules('FieldLabel', 'field label', 'required');
                
                if ($this->form_validation->run() == TRUE)
                {  
                    if($CustomFieldID == 0){
                        $UserID = $this->FieldsModel->CreateCustomField($Object);    
                        if(!empty($UserID)){

                            $this->session->set_flashdata('SuccessMsg', 'Custom field created successfully.');
                            redirect('Setup/Customize/Fields/'.$Object); 

                        } else {

                            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                            redirect('Setup/Customize/Fields/CreateCustomField/'.$Object);
                        }
                    } else {
                        $result = $this->FieldsModel->EditCustomField($Object, $CustomFieldID);    
                        if($result){

                            $this->session->set_flashdata('SuccessMsg', 'Custom field updated successfully.');
                            redirect('Setup/Customize/Fields/'.$Object); 

                        } else {

                            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                            redirect('Setup/Customize/Fields/EditCustomField/'.$Object.'/'.$CustomFieldID);
                        }
                    }
                } else {
                     $this->load->view('Setup/Customize/Fields/CreateCustomFieldStep2', $data);
                }

            } else {  
                 $this->load->view('Setup/Customize/Fields/CreateCustomFieldStep2', $data);
            }
        } else {
            $this->load->view('Setup/Customize/Fields/CreateCustomFieldStep1', $data);
        }
    }

    function EditCustomField($Object, $CustomFieldID) { 

        $data['CustomFieldData'] = $this->FieldsModel->GetCustomFieldData($Object, $CustomFieldID);
        $data['Object'] = $Object;

        if((isset($_POST['CreateCustomFieldStep1']) && isset($_POST['FieldType'])) || isset($_POST['CreateCustomFieldStep2'])){

            $this->load->helper(array('form'));
            $this->load->library('form_validation');

            $data['FieldType'] = $_POST['FieldType'];
            
            if(isset($_POST['CreateCustomFieldStep2'])){
 
                $this->form_validation->set_rules('FieldName', 'field name', 'required');
                $this->form_validation->set_rules('FieldLabel', 'field label', 'required');
                
                if ($this->form_validation->run() == TRUE)
                {  
                    $ID = $this->FieldsModel->EditCustomField($Object, $CustomFieldID);    
                    if(!empty($ID)){

                        $this->session->set_flashdata('SuccessMsg', 'Custom field updated successfully.');
                        redirect('Setup/Customize/Fields/'.$Object); 

                    } else {

                        $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                        redirect('Setup/Customize/Fields/EditCustomField/'.$Object.'/'.$CustomFieldID);
                    }
                } else {
                     $this->load->view('Setup/Customize/Fields/CreateCustomFieldStep2', $data);
                }

            } else {  
                 $this->load->view('Setup/Customize/Fields/CreateCustomFieldStep2', $data);
            }
        } else {
            $this->load->view('Setup/Customize/Fields/CreateCustomFieldStep1', $data);
        }
    }
}
