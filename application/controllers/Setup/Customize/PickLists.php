<?php

class PickLists extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Setup/Customize/PickListsModel');
    }

    public function _remap($method, $params = array())
    {
        if (method_exists($this, $method))
        {
            return call_user_func_array(array($this, $method), $params);
        } 
        else 
        {
            $this->index($method);
        }
    }

    function index($PickList) { 
        
        $data = $this->PickListsModel->GetPickList($PickList); 
        $data['PickList'] = $PickList;
        $data['PickListName'] = preg_replace('/(?<!\ )[A-Z]/', ' $0', str_replace("WO","WorkOrder", $PickList));
        $this->load->view('Setup/Customize/PickLists/PickList', $data);
        
    }
 
    function CreatePickList($PickList) { 

        $data =  $this->PickListsModel->GetPickList($PickList);
        $data['PickList'] = $PickList;
        $data['PickListName'] = preg_replace('/(?<!\ )[A-Z]/', ' $0', str_replace("WO","WorkOrder", $PickList));

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        if(isset($_POST['CreatePickList'])){

            $this->form_validation->set_rules('Name', 'name', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {  
                $ID = $this->PickListsModel->StorePickList($PickList);    
                if(!empty($ID)){

                    $this->session->set_flashdata('SuccessMsg', 'Pick list option saved successfully.');
                    redirect('Setup/Customize/PickLists/'.$PickList); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Setup/Customize/PickLists/CreatePickList/'.$PickList);
                }  
            } else {
                 $this->load->view('Setup/Customize/PickLists/CreatePickList/', $data);
            }

        } else {  
             $this->load->view('Setup/Customize/PickLists/CreatePickList', $data);
        }
    }

    function EditPickList($PickList, $PickListID) { 
    
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $PickListData = $this->PickListsModel->GetPickList($PickList, $PickListID);

        $data['PickListData'] = $PickListData['PickList'];
        $data['ShowDefaultCol'] = $PickListData['ShowDefaultCol'];
        $data['ShowColorCodeCol'] = $PickListData['ShowColorCodeCol'];
        $data['PickListName'] = preg_replace('/(?<!\ )[A-Z]/', ' $0', str_replace("WO","WorkOrder", $PickList));
        $data['PickList'] = $PickList;
        $data['PickListID'] = $PickListID;
   
        if((isset($_POST['EditPickList']) && isset($_POST['EditPickList']))){

            $this->form_validation->set_rules('Name', 'name', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {  
                $ID = $this->PickListsModel->StorePickList($PickList, $PickListID);    
                if(!empty($ID)){

                    $this->session->set_flashdata('SuccessMsg', 'Pick list option updated successfully.');
                    redirect('Setup/Customize/PickLists/'.$PickList); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Setup/Customize/PickLists/EditPickList/'.$PickList);
                }
            } else {
                 $this->load->view('Setup/Customize/PickLists/EditPickList', $data);
            }

        } else {
            $this->load->view('Setup/Customize/PickLists/EditPickList', $data);
        }
    }

    function DeletePickList($PickList, $PickListID){

        $result = $this->PickListsModel->DeletePickList($PickList, $PickListID);
        
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Pick list option deleted successfully.');
            redirect('Setup/Customize/PickLists/'.$PickList); 

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Setup/Customize/PickLists/'.$PickList); 
        }
    }
}