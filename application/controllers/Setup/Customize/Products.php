<?php
 
class Products extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Setup/Customize/ProductsModel');
    }
   
    /*function index() {  
        $data = $this->FieldsModel->GetStandardProductFields(); 
        $this->load->view('Setup/Customize/Fields/Fields', $data);
    }*/

    function ProductList() {
        if(isset($_POST['View']) && !empty($_POST['View'])){
            echo $this->ProductsModel->ProductList($_POST['View']); 
        } else {
            $this->load->view('Setup/Customize/Products/ProductList');
        }
    }

    function ChangeStatus($ProductID)
    {
        echo $this->ProductsModel->ChangeStatus($ProductID); 
    }

    function CreateProduct($ProductID = 0) { 
 
        $this->load->helper(array('form'));
        $this->load->library('form_validation');
 
        if(isset($_POST['CreateProduct'])){
 
            $this->form_validation->set_rules('ProductName', 'product name', 'required');
            $this->form_validation->set_rules('ProductFamily', 'product family', 'required');
            $this->form_validation->set_rules('QuantityUnitOfMeasure', 'quantity unit of measure', 'required');
            $this->form_validation->set_rules('ListPrice', 'list price', 'required');
            $this->form_validation->set_rules('DefaultQuantity', 'default quantity', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {  
                $ProductID = $this->ProductsModel->CreateProduct();    
                if(!empty($ProductID)){

                    $this->session->set_flashdata('SuccessMsg', 'Product created successfully.');
                    redirect('Setup/Customize/Products/ProductDetails/'.$ProductID);

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Setup/Customize/Products/CreateProduct');
                }
            } else {
                $this->load->view('Setup/Customize/Products/CreateProduct');
            }

        } else { 

            $data =  $this->ProductsModel->GetProductData($ProductID); 
            $this->load->view('Setup/Customize/Products/CreateProduct', $data);
        }
    }

    function ProductDetails($ProductID) { 
        $data = $this->ProductsModel->GetProductData($ProductID);
        $this->load->view('Setup/Customize/Products/ProductDetails', $data);
    }

    function EditProduct($ProductID) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data =  $this->ProductsModel->GetProductData($ProductID); 

        if(isset($_POST['EditProduct'])){

            $this->form_validation->set_rules('ProductName', 'product name', 'required');
            $this->form_validation->set_rules('ProductFamily', 'product family', 'required');
            $this->form_validation->set_rules('QuantityUnitOfMeasure', 'quantity unit of measure', 'required');
            $this->form_validation->set_rules('ListPrice', 'list price', 'required');
            $this->form_validation->set_rules('DefaultQuantity', 'default quantity', 'required');
             
            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->ProductsModel->EditProduct($ProductID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Product updated successfully.');
                    redirect('Setup/Customize/Products/ProductDetails/'.$ProductID); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Setup/Customize/Products/EditProduct/'.$ProductID); 
                }
            } else { 
                $this->load->view('Setup/Customize/Products/EditProduct', $data); 
            }

        } else { 
           
            $this->load->view('Setup/Customize/Products/EditProduct', $data);
        }
    }

    function DeleteProduct($ProductID)
    {
        $result =  $this->ProductsModel->DeleteProduct($ProductID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Product deleted successfully.');
            redirect('Setup/Customize/Products/ProductList');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Setup/Customize/Products/ProductDetails/'.$ProductID); 
        }
    }

}
