<?php
 
class Settings extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Setup/Customize/SettingsModel');
    }
   
   /* function index() {  
        $data['ChemicalSettings'] = $this->SettingsModel->GetChemicalSettings();
        $this->load->view('Setup/Customize/Settings/Chemicals', $data);
    }*/

    function ChemicalSettings() {

        if(isset($_POST['ChemicalSettings'])){
            $this->SettingsModel->ChemicalSettings();
            $data['ChemicalSettings'] = $this->SettingsModel->GetChemicalSetting();
            $this->load->view('Setup/Customize/Settings/Chemicals', $data);
        } else {
            $data['ChemicalSettings'] = $this->SettingsModel->GetChemicalSetting();
            $this->load->view('Setup/Customize/Settings/Chemicals', $data);
        }
    }

    function NoteSettings() {

        if(isset($_POST['NoteSettings'])){
            $this->SettingsModel->NoteSettings();
            $data['NoteSettings'] = $this->SettingsModel->GetNoteSetting();
            $this->load->view('Setup/Customize/Settings/Notes', $data);
        } else {
            $data['NoteSettings'] = $this->SettingsModel->GetNoteSetting();
            $this->load->view('Setup/Customize/Settings/Notes', $data);
        }
    }

    function EstimateSettings() {

        if(isset($_POST['EstimateSettings'])){
            $this->SettingsModel->EstimateSettings();
            $data['EstimateSettings'] = $this->SettingsModel->GetEstimateSetting();
            $this->load->view('Setup/Customize/Settings/Estimates', $data);
        } else {
            $data['EstimateSettings'] = $this->SettingsModel->GetEstimateSetting();
            $this->load->view('Setup/Customize/Settings/Estimates', $data);
        }
    }
}
