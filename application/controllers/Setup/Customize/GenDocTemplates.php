<?php

class GenDocTemplates extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Setup/Customize/GenDocTemplatesModel');
    }
   
    function WorkOrder() { 

        $data['Templates'] = $this->GenDocTemplatesModel->GetTemplates('WorkOrder'); 
        $data['Object'] = 'WorkOrder';
        $this->load->view('Setup/Customize/GenDocTemplates/GenDocTemplates', $data);
    }

    function Estimate() { 

        $data['Templates'] = $this->GenDocTemplatesModel->GetTemplates('Estimate'); 
        $data['Object'] = 'Estimate';
        $this->load->view('Setup/Customize/GenDocTemplates/GenDocTemplates', $data);
    }

    function Invoice() { 

        $data['Templates'] = $this->GenDocTemplatesModel->GetTemplates('Invoice');
        $data['Object'] = 'Invoice';
        $this->load->view('Setup/Customize/GenDocTemplates/GenDocTemplates', $data);
    }

    function CreateTemplate($RelatedTo) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
 
        $data['Object'] = $RelatedTo;

        if(isset($_POST['CreateTemplate'])){
 
            $this->form_validation->set_rules('TemplateName', 'name', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {  
                $UserID = $this->GenDocTemplatesModel->CreateTemplate($RelatedTo);    
                if(!empty($UserID)){

                    $this->session->set_flashdata('SuccessMsg', 'Template created successfully.');
                    redirect('Setup/Customize/GenDocTemplates/'.$RelatedTo); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Setup/Customize/GenDocTemplates/CreateTemplate/'.$RelatedTo);
                }
            } else {
                $this->load->view('Setup/Customize/GenDocTemplates/CreateGenDocTemplate', $data);
            }

        } else {  
            $this->load->view('Setup/Customize/GenDocTemplates/CreateGenDocTemplate', $data);
        }
    }

    function TemplateDetails($GenDocTemplateID) {

        $data['TemplateData'] =  $this->GenDocTemplatesModel->GetTemplateData($GenDocTemplateID); 
        $this->load->view('Setup/Customize/GenDocTemplates/GenDocTemplateDetails', $data);
    }

    function EditTemplate($GenDocTemplateID) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data['TemplateData'] =  $this->GenDocTemplatesModel->GetTemplateData($GenDocTemplateID); 

        if(isset($_POST['EditTemplate'])){

            $this->form_validation->set_rules('TemplateName', 'name', 'required');
             
            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->GenDocTemplatesModel->EditTemplate($GenDocTemplateID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Template updated successfully.');
                    redirect('Setup/Customize/GenDocTemplates/'.$_POST['RelatedTo']);

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Setup/Customize/GenDocTemplates/EditTemplate/'.$GenDocTemplateID);
                }
            } else { 
                $this->load->view('Setup/Customize/GenDocTemplates/EditGenDocTemplate', $data); 
            }

        } else { 
           
            $this->load->view('Setup/Customize/GenDocTemplates/EditGenDocTemplate', $data);
        }
    }

    function DownloadTemplate($FileName) { 

        $this->load->helper('download');
        $data = file_get_contents('./doc_templates/'.$FileName);
        force_download($FileName, $data);
    }

    function DeleteTemplate($GenDocTemplateID){

        $TemplateData =  $this->GenDocTemplatesModel->GetTemplateData($GenDocTemplateID); 
        $result = $this->GenDocTemplatesModel->DeleteTemplate($GenDocTemplateID);    
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Template deleted successfully.');
            redirect('Setup/Customize/GenDocTemplates/'.$TemplateData->RelatedTo);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Setup/Customize/GenDocTemplates/'.$TemplateData->RelatedTo);
        }
    }
}
