<?php
 
class Actions extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Setup/Organization/ActionsModel');
    }
   
    function index() { 
        $this->load->view('Setup/Organization/Home');
    }
  
    function CreateOrganization($OrganizationID = 0) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
 
        if(isset($_POST['CreateOrganization'])){
 
            $this->form_validation->set_rules('AssignedTo', 'owner', 'required');
            $this->form_validation->set_rules('OrganizationName', 'account name', 'required');
            $this->form_validation->set_rules('PhoneNo', 'phone no', 'required');
            //$this->form_validation->set_rules('PreferredTechnician', 'preferred technician', 'required');
            $this->form_validation->set_rules('BillingCountry', 'billing country', 'required');
            $this->form_validation->set_rules('BillingState', 'billing state', 'required');
            $this->form_validation->set_rules('BillingCity', 'billing city', 'required');
            $this->form_validation->set_rules('BillingAddress', 'billing address', 'required');
            $this->form_validation->set_rules('BillingPostalCode', 'billing postal code', 'required'); 
            $this->form_validation->set_rules('ShippingCountry', 'billing country', 'required');
            $this->form_validation->set_rules('ShippingState', 'billing state', 'required');
            $this->form_validation->set_rules('ShippingCity', 'billing city', 'required');
            $this->form_validation->set_rules('ShippingAddress', 'billing address', 'required');
            $this->form_validation->set_rules('ShippingPostalCode', 'billing postal code', 'required'); 
            $this->form_validation->set_rules('OrganizationType', 'account type', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {  
                $OrganizationID = $this->ActionsModel->CreateOrganization();    
                if(!empty($OrganizationID)){

                    $this->session->set_flashdata('SuccessMsg', 'Organization created successfully.');
                    redirect('Setup/Organization/Actions/OrganizationDetails/'.$OrganizationID);

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Setup/Organization/Actions/CreateOrganization');
                }
            } else {
                $this->load->view('Setup/Organization/CreateOrganization');
            }

        } else { 

            $data =  $this->ActionsModel->getOrganizationData($OrganizationID); 
            $this->load->view('Setup/Organization/CreateOrganization', $data);
        }
    }

    function OrganizationInfo() {

        $data =  $this->ActionsModel->getOrganizationData(); 
        $this->load->view('Setup/Organization/OrganizationInfo', $data);
    }

    function ChangePlan() { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data =  $this->ActionsModel->getOrganizationData(); 
        
        if(isset($_POST['ChangePlan'])){

            $this->form_validation->set_rules('OrgPlanID', 'plan', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->ActionsModel->ChangePlan();    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Plan updated successfully.');
                    redirect('Setup/Organization/Actions/ChangePlan/'); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Setup/Organization/Actions/ChangePlan/'); 
                }
            } else { 
                $this->load->view('Setup/Organization/ChangePlan', $data); 
            }

        } else { 
           
            $this->load->view('Setup/Organization/ChangePlan', $data);
        }
    }

    function ChangeLicenses() { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data =  $this->ActionsModel->getOrganizationData(); 

        if(isset($_POST['ChangeLicenses'])){

            $this->form_validation->set_rules('Action', 'action', 'required');
            $this->form_validation->set_rules('Licenses', 'licenses', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->ActionsModel->ChangeLicenses();    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Licenses updated successfully.');
                    redirect('Setup/Organization/Actions/ChangeLicenses/'); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Setup/Organization/Actions/ChangeLicenses/'); 
                }
            } else { 
                $this->load->view('Setup/Organization/ChangeLicenses', $data); 
            }

        } else { 
           
            $this->load->view('Setup/Organization/ChangeLicenses', $data);
        }
    }

    function FiscalYear() { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data =  $this->ActionsModel->getOrganizationData(); 

        if(isset($_POST['FiscalYear'])){

            $this->form_validation->set_rules('FiscalYearStart', 'month', 'required');
            $this->form_validation->set_rules('FiscalYearIsBasedOn', 'base on', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->ActionsModel->FiscalYear();    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Fiscal year updated successfully.');
                    redirect('Setup/Organization/Actions/FiscalYear/'); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Setup/Organization/Actions/FiscalYear/'); 
                }
            } else { 
                $this->load->view('Setup/Organization/FiscalYear', $data); 
            }

        } else { 
           
            $this->load->view('Setup/Organization/FiscalYear', $data);
        }
    }

    function DefaultTaxRate() { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data =  $this->ActionsModel->getOrganizationData(); 

        if(isset($_POST['DefaultTaxRate'])){

            $this->form_validation->set_rules('TaxRate', 'month', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->ActionsModel->DefaultTaxRate();    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Default tax rate updated successfully.');
                    redirect('Setup/Organization/Actions/DefaultTaxRate/'); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Setup/Organization/Actions/DefaultTaxRate/'); 
                }
            } else { 
                $this->load->view('Setup/Organization/DefaultTaxRate', $data); 
            }

        } else { 
           
            $this->load->view('Setup/Organization/DefaultTaxRate', $data);
        }
    }

    function DeactivateAccount() {

        $result =  $this->ActionsModel->DeactivateAccount(); 
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Your account has been de-active successfully.');
            redirect('Setup/Organization/Actions/ChangePlan/'); 
        } else {
            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Setup/Organization/Actions/ChangePlan/'); 
        }
    }

    function EditOrganization() { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data =  $this->ActionsModel->getOrganizationData(); 

        if(isset($_POST['EditOrganization'])){

            
            $this->form_validation->set_rules('CompanyName', 'company name', 'required');
            $this->form_validation->set_rules('Phone', 'phone no', 'required');
            $this->form_validation->set_rules('Website', 'website', 'required');
            $this->form_validation->set_rules('CompanyEdition', 'edition', 'required');
            $this->form_validation->set_rules('BillingCountry', 'billing country', 'required');
            $this->form_validation->set_rules('BillingState', 'billing state', 'required');
            $this->form_validation->set_rules('BillingCity', 'billing city', 'required');
            $this->form_validation->set_rules('BillingAddress', 'billing address', 'required');
            $this->form_validation->set_rules('BillingPostalCode', 'billing postal code', 'required'); 
            
             
            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->ActionsModel->EditOrganization($OrganizationID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Organization updated successfully.');
                    redirect('Setup/Organization/Actions/OrganizationInfo/'); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Setup/Organization/Actions/EditOrganization/'); 
                }
            } else { 
                $this->load->view('Setup/Organization/EditOrganization', $data); 
            }

        } else { 
           
            $this->load->view('Setup/Organization/EditOrganization', $data);
        }
    }

    function DeleteOrganization($OrganizationID){
        $result =  $this->ActionsModel->DeleteOrganization($OrganizationID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Organization deleted successfully.');
            redirect('Setup/Organization/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Setup/Organization/Actions/OrganizationDetails/'.$OrganizationID); 
        }
    }
}
