<?php
 
class Actions extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Setup/Communication/ActionsModel');
    }
   
    function index() { 
        $data['EmailTemplates'] = $this->ActionsModel->GetEmailTemplateList(); 
        $this->load->view('Setup/Communication/EmailTemplates', $data);
    }

    function CreateEmailTemplate() { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
 
        if(isset($_POST['CreateEmailTemplate'])){
 
            $this->form_validation->set_rules('Title', 'name', 'required');
            $this->form_validation->set_rules('Type', 'type', 'required');
            $this->form_validation->set_rules('Body', 'body', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {  
                $UserID = $this->ActionsModel->CreateEmailTemplate();    
                if(!empty($UserID)){

                    $this->session->set_flashdata('SuccessMsg', 'User created successfully.');
                    redirect('Setup/Communication/Actions'); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Setup/Communication/Actions/CreateEmailTemplate');
                }
            } else {
                $this->load->view('Setup/Communication/CreateEmailTemplate');
            }

        } else {  
            $this->load->view('Setup/Communication/CreateEmailTemplate');
        }
    }

    function EditEmailTemplate($EmailTemplateID) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data['EmailTemplateData'] =  $this->ActionsModel->getEmailTemplateData($EmailTemplateID); 

        if(isset($_POST['EditEmailTemplate'])){

            $this->form_validation->set_rules('Title', 'name', 'required');
            $this->form_validation->set_rules('Type', 'type', 'required');
            $this->form_validation->set_rules('Body', 'body', 'required');
             
            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->ActionsModel->EditEmailTemplate($EmailTemplateID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'User updated successfully.');
                    redirect('Setup/Communication/Actions'); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Setup/Communication/Actions/EditEmailTemplate/'.$UserID); 
                }
            } else { 
                $this->load->view('Setup/Communication/EditEmailTemplate', $data); 
            }

        } else { 
           
            $this->load->view('Setup/Communication/EditEmailTemplate', $data);
        }
    }
}
