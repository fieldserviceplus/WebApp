<?php
 
class Actions extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Setup/Administration/ActionsModel');
    }
   
    function index() { 
        $this->load->view('Setup/Administration/Home');
    }

    function LoginHistory() {

        $data['data'] =  $this->ActionsModel->GetLoginHistory(); 
        $this->load->view('Setup/Administration/LoginHistory', $data);
    }

    function UserList() {
        if(isset($_POST['View']) && !empty($_POST['View'])){
            echo $this->ActionsModel->GetUserList($_POST['View']); 
        } else {
            $this->load->view('Setup/Administration/UserList');
        }
    }

    function UserDetails($UserID) {
        $data['UserData'] =  $this->ActionsModel->getUserData($UserID); 
        $this->load->view('Setup/Administration/UserDetails', $data);
    }

    function CreateUser() { 
        
        $data = $this->ActionsModel->RegTotalUsers();
        if($data['TotalLicenses'] == $data['UsedLicenses']) {
            $this->session->set_flashdata('ErrorMsg', 'you’ll need to upgrade your plan before adding more users.');
            redirect('Setup/Administration/Actions/UserList');
        }  

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
 
        if(isset($_POST['CreateUser']) || isset($_POST['CreateUserSaveNew'])){
 
            $this->form_validation->set_rules('FirstName', 'first name', 'required');
            $this->form_validation->set_rules('LastName', 'last name', 'required');
            $this->form_validation->set_rules('Email', 'email', 'required');
            /*$this->form_validation->set_rules('OrganizationID', 'company', 'required');*/
           /* $this->form_validation->set_rules('Department', 'department', 'required');
            $this->form_validation->set_rules('Division', 'division', 'required');*/
            $this->form_validation->set_rules('Manager', 'manager', 'required');
            $this->form_validation->set_rules('MobileNo', 'mobile no', 'required');
            $this->form_validation->set_rules('Profile', 'profile', 'required');
            /*$this->form_validation->set_rules('StartDate', 'start date', 'required');
            $this->form_validation->set_rules('EndDate', 'end date', 'required');*/
            $this->form_validation->set_rules('Address', 'address', 'required');
            $this->form_validation->set_rules('City', 'city', 'required');
            $this->form_validation->set_rules('State', 'state', 'required');
            $this->form_validation->set_rules('Country', 'country', 'required');
            $this->form_validation->set_rules('PostalCode', 'postal code', 'required');
            $this->form_validation->set_rules('TimeZone', 'time zone', 'required');
            $this->form_validation->set_rules('StartOfDay', 'start of day', 'required');
            $this->form_validation->set_rules('EndOfDay', 'end of day', 'required');
            $this->form_validation->set_rules('SenderEmail', 'sender email', 'required');
            $this->form_validation->set_rules('SenderName', 'sender name', 'required');
            /*$this->form_validation->set_rules('EmailSignature', 'email signature', 'required');*/
            
            if ($this->form_validation->run() == TRUE)
            {  
                $UserID = $this->ActionsModel->CreateUser();    
                if(!empty($UserID)){

                    $this->session->set_flashdata('SuccessMsg', 'User created successfully.');
                    if(isset($_POST['CreateUser']))
                        redirect('Setup/Administration/Actions/UserDetails/'.$UserID); 
                    else if(isset($_POST['CreateUserSaveNew']))
                        redirect('Setup/Administration/Actions/CreateUser'); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Setup/Administration/Actions/CreateUser');
                }
            } else {
                $this->load->view('Setup/Administration/CreateUser');
            }

        } else { 

            //$data =  $this->ActionsModel->getFileData(); 
            $this->load->view('Setup/Administration/CreateUser');
        }
    }

    function EditUser($UserID) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data['UserData'] =  $this->ActionsModel->getUserData($UserID); 

        if(isset($_POST['EditUser']) || isset($_POST['EditUserSaveNew'])){

            $this->form_validation->set_rules('FirstName', 'first name', 'required');
            $this->form_validation->set_rules('LastName', 'last name', 'required');
            $this->form_validation->set_rules('Email', 'email', 'required');
           /* $this->form_validation->set_rules('OrganizationID', 'company', 'required');*/
            /*$this->form_validation->set_rules('Department', 'department', 'required');
            $this->form_validation->set_rules('Division', 'division', 'required');*/
            $this->form_validation->set_rules('Manager', 'manager', 'required');
            $this->form_validation->set_rules('MobileNo', 'mobile no', 'required');
            $this->form_validation->set_rules('Profile', 'profile', 'required');
           /* $this->form_validation->set_rules('StartDate', 'start date', 'required');
            $this->form_validation->set_rules('EndDate', 'end date', 'required');*/
            $this->form_validation->set_rules('Address', 'address', 'required');
            $this->form_validation->set_rules('City', 'city', 'required');
            $this->form_validation->set_rules('State', 'state', 'required');
            $this->form_validation->set_rules('Country', 'country', 'required');
            $this->form_validation->set_rules('PostalCode', 'postal code', 'required');
            $this->form_validation->set_rules('TimeZone', 'time zone', 'required');
            $this->form_validation->set_rules('StartOfDay', 'start of day', 'required');
            $this->form_validation->set_rules('EndOfDay', 'end of day', 'required');
            $this->form_validation->set_rules('SenderEmail', 'sender email', 'required');
            $this->form_validation->set_rules('SenderName', 'sender name', 'required');
            /*$this->form_validation->set_rules('EmailSignature', 'email signature', 'required');*/
             
            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->ActionsModel->EditUser($UserID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'User updated successfully.');
                    if(isset($_POST['EditUser']))
                        redirect('Setup/Administration/Actions/UserDetails/'.$UserID); 
                    else if(isset($_POST['EditUserSaveNew']))
                        redirect('Setup/Administration/Actions/CreateUser'); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Setup/Administration/Actions/EditUser/'.$UserID); 
                }
            } else { 
                $this->load->view('Setup/Administration/EditUser', $data); 
            }

        } else { 
           
            $this->load->view('Setup/Administration/EditUser', $data);
        }
    }

    function ResetPassword() {

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        if(isset($_POST['ResetPassword'])){

            if($_POST['NewPassword'] != $_POST['ConfirmNewPassword']){
                $this->session->set_flashdata('ErrorMsg', 'Your password and confirmation password do not match.');
                redirect('Setup/Administration/Actions/UserList');
            }

            $this->form_validation->set_rules('OldPassword', 'old password', 'required');
            $this->form_validation->set_rules('NewPassword', 'new password', 'required');
            $this->form_validation->set_rules('ConfirmNewPassword', 'confirm new password', 'required');

            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->ActionsModel->ResetPassword();    

                if($result == 1){

                    $this->session->set_flashdata('SuccessMsg', 'Password reset successfully.');
                    redirect('Setup/Administration/Actions/UserList');

                } else if($result == 2){

                    $this->session->set_flashdata('ErrorMsg', 'Your old password is worng.');
                    redirect('Setup/Administration/Actions/UserList');

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Setup/Administration/Actions/UserList');
                }
            } else { 
                $this->load->view('Setup/Administration/ResetPassword'); 
            }

        } else {

            $this->load->view('Setup/Administration/ResetPassword');
        }
        
    }
}
