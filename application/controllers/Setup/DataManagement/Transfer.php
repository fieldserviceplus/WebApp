<?php
class Transfer extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Setup/DataManagement/TransferModel');
    }
   
    function index() {  

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->load->view('Setup/DataManagement/MassTransfer');
    }

    function GetViewFields($Object){

        $option = "<option value=''>Select Filter Field</option>";
        $fields = MY_Model::getViewFields($Object);
        foreach ($fields as $key => $field) {
            $option .= "<option value='".$field->FieldName."'>".$field->FieldName."</option>";
        }
        echo $option;
    }

    function FindRecords() {  

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        if(isset($_POST['FindRecords'])){

            $this->form_validation->set_rules('Object', 'object', 'required');
            $this->form_validation->set_rules('TransferFrom', 'file', 'required');
            $this->form_validation->set_rules('TransferTo', 'file', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {  
                $Data = $this->TransferModel->FindRecords();    
                if(is_array($Data) && !empty($Data)){

                    $this->load->view('Setup/DataManagement/MassTransfer', $Data);

                } else {

                    $this->session->set_flashdata('ErrorMsg', $FileData);
                    redirect('Setup/DataManagement/Transfer/');
                }
            } else {
                 $this->load->view('Setup/DataManagement/MassTransfer');
            }
        } else {  
             $this->load->view('Setup/DataManagement/MassTransfer');
        }
        
    }

    function MassTransfer(){
        
        if(isset($_POST['Object']) && isset($_POST['TransferFrom']) && isset($_POST['TransferTo']) && isset($_POST['ID']) && !empty($_POST['ID'])){

            $result = $this->TransferModel->MassTransfer();

            if($result) {
                $this->session->set_flashdata('SuccessMsg', 'Ownership transfered successfully.');
                redirect('Setup/DataManagement/Transfer/');
            } else {
                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Setup/DataManagement/Transfer/');
            }
        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Setup/DataManagement/Transfer/');
        }
    }
}