<?php
class Import extends MY_Controller {

    public $LoginUserID;
    public $LoginOrganizationID;
 
    function __construct() {
        parent::__construct();
        $this->load->model('Setup/DataManagement/ImportModel');

        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }
   
    function index() {  
        $this->load->view('Setup/DataManagement/UploadData');
    }

    function MapData($Object, $FileName) {  
        $data = $this->ImportModel->FetchDataFromExcel($Object, $FileName);
        if($data == false){
            $this->session->set_flashdata('ErrorMsg', 'Columns count did not matches.');
            redirect('Setup/DataManagement/Import/');
        }
        $this->load->view('Setup/DataManagement/MapData', $data);
    }
 
    function UploadFile() { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        if(isset($_POST['UploadFile'])){

            $this->form_validation->set_rules('Object', 'object', 'required');
           //$this->form_validation->set_rules('UploadedFile', 'file', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {  
               $FileData = $this->ImportModel->UploadFile();    
                    if(is_array($FileData) && !empty($FileData)){

                        redirect('Setup/DataManagement/Import/MapData/'.$FileData['Object'].'/'.$FileData['FileName']);

                    } else {
 
                        $this->session->set_flashdata('ErrorMsg', $FileData);
                        redirect('Setup/DataManagement/Import/');
                    }
            } else {
                 $this->load->view('Setup/DataManagement/Import/UploadData');
            }
        } else {  
             $this->load->view('Setup/DataManagement/ImportUploadData');
        }
    }

    function ImportData(){

        $TEMP_PATH = str_replace('{org_id}', $this->LoginOrganizationID, TEMP_PATH);

        if(isset($_POST['ImportData'])){
            $result = $this->ImportModel->ImportData();  
             
            unlink($TEMP_PATH.$_POST['FileName']);  
            if($result == 1){

                $this->session->set_flashdata('SuccessMsg', 'Data imported successfully.');
                redirect('Setup/DataManagement/Import/'); 

            } else if($result == 2){

                $this->session->set_flashdata('ErrorMsg', 'Please select required fields.');
                redirect('Setup/DataManagement/Import/'); 
            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Setup/DataManagement/Import/');
            }


        }
    }
}

