<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class Actions extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Common/ActionsModel');
    }

    function CreateNewFile() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        $_POST['Description'] = isset($_POST['Description'])?$_POST['Description']:"";
   
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'Subject' => $Subject,
            'AssignedTo' => $AssignedTo,
            'FileName' => $_FILES['FileName']['name'][0],
            'RelatedTo' => $RelatedTo,
            'What' => $What, 
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $data = $this->ActionsModel->CreateNewFile(); 

        if(is_array($data)){
            return API_Controller::successResponse($data, 1, "File has been created successfully.", "True");
        } else {
            return API_Controller::responseMessage(0, $data, "False");
        }
    } 

    function SendEmail() { 
  
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        $_POST['EmailBody'] = isset($_POST['EmailBody'])?$_POST['EmailBody']:"";
   
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'FromName' => $FromName,
            'FromEmailID' => $FromEmailID,
            'ToEmailID' => $ToEmailID,
            'EmailSubject' => $EmailSubject,
            'RelatedTo' => $RelatedTo,
            'What' => $What, 
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $result = $this->ActionsModel->SendEmail(); 
        $data = array('RelatedTo' => $RelatedTo, 'What' => $What);

        if($result == 1){
            return API_Controller::successResponse($data, 1, "Email has been sent successfully.", "True");
        } else {
            return API_Controller::responseMessage(0, "Somthing went wrong.", "False");
        }
    }

    function DeleteObject() { 
                                                                        
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'RelatedTo' => $RelatedTo,
            'What' => $What
        ]);
 
        API_Controller::checkUserAuthentication($UserID); 
        
        $data['data'] = $this->ActionsModel->DeleteObject();  

        API_Controller::responseMessage(1, $RelatedTo.' has been deleted successfully.', 'True');
    } 

    function CreateEvent() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        $Description = isset($_POST['Description'])?$_POST['Description']:NULL;
        $IsAllDayEvent = isset($_POST['IsAllDayEvent'])?$_POST['IsAllDayEvent']:0;
        $Who = isset($_POST['Who'])?$_POST['Who']:NULL;
   
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'Subject' => $Subject,
            'AssignedTo' => $AssignedTo,
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            'Who' => $Who,
            'EventPriority' => $EventPriority,
            'EventType' => $EventType,
            'EventStartDate' => $EventStartDate,
            'EventEndDate' => $EventEndDate
        ]);

        if(isset($IsRecurring) && $IsRecurring == 1){
 
            $RepeatOn = !empty($_POST['RepeatOn'])?$_POST['RepeatOn']:NULL;
            $EndsOnDate = !empty($_POST['EndsOnDate'])?date("Y-m-d",strtotime($_POST['EndsOnDate'])):NULL;
            $EndsAfterOccurrences = !empty($_POST['EndsAfterOccurrences'])?$_POST['EndsAfterOccurrences']:NULL;
            API_Controller::requiredValidation([
                'StartOn' => $StartOn,
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'Ends' => $Ends,
                'StartTime' => $StartTime,
                'EndTime' => $EndTime
            ]);
        } else {
            $IsRecurring = 0;
            $StartOn = NULL;
            $RepeatEvery = NULL;
            $IntervalEvery = NULL;
            $Ends = NULL;
            $RepeatOn = NULL;
            $EndsOnDate = NULL;
            $EndsAfterOccurrences = NULL;
            $StartTime = NULL;
            $EndTime = NULL;  
        }
   
        API_Controller::checkUserAuthentication($UserID); 

        $data = $this->ActionsModel->CreateEvent($UserID, $OrganizationID, $Subject, $AssignedTo, $RelatedTo, $What, $Who, $Description, $EventPriority, $EventType, $EventStartDate, $EventEndDate, $IsAllDayEvent, $IsRecurring, $StartOn, $RepeatEvery, $IntervalEvery, $Ends, $RepeatOn, $EndsOnDate, $EndsAfterOccurrences, $StartTime, $EndTime); 

        return API_Controller::successResponse($data, 1, "Event has been created successfully.", "True");
    } 

    function EditEvent() { 

        $Description = isset($_POST['Description'])?$_POST['Description']:NULL;
        $IsAllDayEvent = isset($_POST['IsAllDayEvent'])?$_POST['IsAllDayEvent']:NULL;
        $Who = isset($_POST['Who'])?$_POST['Who']:NULL;
        
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'EventID' => $EventID,
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'Subject' => $Subject,
            'AssignedTo' => $AssignedTo,
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            /*'Who' => $Who,*/
            'EventPriority' => $EventPriority,
            'EventType' => $EventType,
            'EventStartDate' => $EventStartDate,
            'EventEndDate' => $EventEndDate,
        ]);

        API_Controller::checkUserAuthentication($UserID); 

        $data = $this->ActionsModel->EditEvent($EventID, $UserID, $OrganizationID, $Subject, $AssignedTo, $RelatedTo, $What, $Who, $Description, $EventPriority, $EventType, $EventStartDate, $EventEndDate, $IsAllDayEvent); 

        return API_Controller::responseMessage(1, "Edit has been updated successfully.", "True");
    }  

    function EventDetails() { 
                                                                        
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'EventID' => $EventID
        ]);
 
        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNull($this->ActionsModel->EventDetails($UserID, $EventID, $OrganizationID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Event details fetch successfully.', 'True');
    } 

    function CreateNote() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'Subject' => $Subject,
            'Body' => $Body,
            'RelatedTo' => $RelatedTo,
            'What' => $What
        ]);

        API_Controller::checkUserAuthentication($UserID); 

        $data['NoteID'] = $this->ActionsModel->CreateNote(); 

        return API_Controller::successResponse($data, 1, "Note has been created successfully.", "True");
    }  

    function EditNote() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'NoteID' => $NoteID,
            'Subject' => $Subject,
            'Body' => $Body,
            'RelatedTo' => $RelatedTo,
            'What' => $What
        ]);

        API_Controller::checkUserAuthentication($UserID); 

        $data = $this->ActionsModel->EditNote(); 

        return API_Controller::responseMessage(1, "Note has been updated successfully.", "True");
    }  

    function NoteDetails() { 
                                                                        
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'NoteID' => $NoteID
        ]);
 
        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNull($this->ActionsModel->NoteDetails($UserID, $NoteID, $OrganizationID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Note details fetch successfully.', 'True');
    }  
  
    function UserDetails() { 
                                                                        
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'ViewUserID' => $ViewUserID
        ]);
 
        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNull($this->ActionsModel->UserDetails($UserID, $OrganizationID, $ViewUserID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'User details fetch successfully.', 'True');
    } 

    function ProductDetails() { 
                                                                        
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'ProductID' => $ProductID
        ]);
 
        API_Controller::checkUserAuthentication($UserID); 
       
        $result = $this->ActionsModel->ProductDetails($UserID, $OrganizationID, $ProductID);

        $data['data'] = API_Controller::removeNull($result);  

        API_Controller::successResponse($data, 1, 'Product details fetch successfully.', 'True');
    } 

    function GenerateDocument() { 
                                                                        
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'RelatedTo' => $RelatedTo,
            'ObjectID' => $ObjectID,
            'OrganizationID' => $OrganizationID,
            'GenDocTemplateID' => $GenDocTemplateID,
            'SaveToObject' => $SaveToObject,
            'OutputFormat' => $OutputFormat,
        ]);
 
        API_Controller::checkUserAuthentication($UserID); 
       
        $result = $this->ActionsModel->GenerateDocument($UserID, $OrganizationID, $GenDocTemplateID, $RelatedTo, $ObjectID, $SaveToObject, $OutputFormat);

        $data['data'] = $result;  

        if($result == 2)
            API_Controller::responseMessage(0, 'Something went wrong.', 'False');
        else
            API_Controller::successResponse($data, 1, 'Document has been generated successfully.', 'True');
    } 
}
