<?php


require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class Data extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Common/DataModel');
    }
 
    function GetEmailTemplates() { 

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $result['data'] = $this->DataModel->GetEmailTemplates(); 

        return API_Controller::successResponse($result, 1, "Email templates fetch successfully.", "True");
    }

    function GetEventPriorities() { 

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $result['data'] = $this->DataModel->GetEventPriorities(); 

        return API_Controller::successResponse($result, 1, "Event priority fetch successfully.", "True");
    }
 
    function GetEventTypes() { 

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $result['data'] = $this->DataModel->GetEventTypes(); 

        return API_Controller::successResponse($result, 1, "Event type fetch successfully.", "True");
    }

    function GetEventStatus() { 

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $result['data'] = $this->DataModel->GetEventStatus(); 

        return API_Controller::successResponse($result, 1, "Event status fetch successfully.", "True");
    }

    function GetGenDocTemplates() { 

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'RelatedTo' => $RelatedTo,
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $result['data'] = $this->DataModel->GetGenDocTemplates(); 

        return API_Controller::successResponse($result, 1, "Generate document templates fetch successfully.", "True");
    }

    function GetCustomFields() { 

        API_Controller::varifyMethod("POST");
 
        $_POST['ObjectID'] = isset($_POST['ObjectID']) ? (empty($_POST['ObjectID']) ? 0 : $_POST['ObjectID']) : 0 ;

        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'Object' => $Object,
            'ObjectID' => $ObjectID,
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $data["data"] = API_Model::getCustomFields($ObjectID, $Object, $OrganizationID); 

        return API_Controller::successResponse($data, 1, "Get custom fields fetch successfully.", "True");
    }
}
