<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class View extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Common/ViewModel');
    }

    function CreateNewView() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'Object' => $Object,
            'OrganizationID' => $OrganizationID,
            'ViewName' => $ViewName,
            'RestrictVisibility' => $RestrictVisibility,
            'SpecifyFieldsDisplay' => is_array($SpecifyFieldsDisplay)?$SpecifyFieldsDisplay:NULL,   
            'FilterFields' => is_array($FilterFields)?$FilterFields:NULL,
            'FilterConditions' => is_array($FilterConditions)?$FilterConditions:NULL,
            'FilterValues' => is_array($FilterValues)?$FilterValues:NULL,
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $data['data'][$Object.'ViewID'] = $this->ViewModel->CreateNewView(); 
        $data['data']['Object'] = $Object;

        return API_Controller::successResponse($data, 1, "View has been created successfully.", "True");
    } 

    function GetCustomViewDetails()
    { 
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'Object' => $Object,
            'OrganizationID' => $OrganizationID,
            'ViewID' => $ViewID
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $data = $this->ViewModel->GetCustomViewDetails();

        return API_Controller::successResponse($data, 1, "Fetch view details successfully.", "True");
    }

    function CopyCustomView()
    { 
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'Object' => $Object,
            'OrganizationID' => $OrganizationID,
            'ViewID' => $ViewID
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $result = $this->ViewModel->CopyCustomView();

        $data['data'][$Object.'ViewID'] = $result; 
        $data['data']['Object'] = $Object;
        return API_Controller::successResponse($data, 1, "View has been copied successfully.", "True");
    }

    function RenameCustomView(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'Object' => $Object,
            'OrganizationID' => $OrganizationID,
            'ViewID' => $ViewID,
            'ViewName' => $ViewName
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $data['data'][$Object.'ViewID'] = $this->ViewModel->RenameCustomView(); 
        $data['data']['Object'] = $Object;

        return API_Controller::successResponse($data, 1, "View name has been updated successfully.", "True");
    }

    function EditSharingCustomView(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'Object' => $Object,
            'OrganizationID' => $OrganizationID,
            'ViewID' => $ViewID,
            'RestrictVisibility' => $RestrictVisibility
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $data['data'][$Object.'ViewID'] = $this->ViewModel->EditSharingCustomView(); 
        $data['data']['Object'] = $Object;

        return API_Controller::successResponse($data, 1, "Sharing has been updated successfully.", "True");
    }

    function EditFiltersCustomView(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'Object' => $Object,
            'OrganizationID' => $OrganizationID,
            'ViewID' => $ViewID,
            'FilterFields' => is_array($FilterFields)?$FilterFields:NULL,
            'FilterConditions' => is_array($FilterConditions)?$FilterConditions:NULL,
            'FilterValues' => is_array($FilterValues)?$FilterValues:NULL,
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $data['data'][$Object.'ViewID'] = $this->ViewModel->EditFiltersCustomView(); 
        $data['data']['Object'] = $Object;

        return API_Controller::successResponse($data, 1, "Filters has been updated successfully.", "True");
    }

    function EditDisplayedColumnsCustomView(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'Object' => $Object,
            'OrganizationID' => $OrganizationID,
            'ViewID' => $ViewID,
            'SpecifyFieldsDisplay' => is_array($SpecifyFieldsDisplay)?$SpecifyFieldsDisplay:NULL
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $data['data'][$Object.'ViewID'] = $this->ViewModel->EditDisplayedColumnsCustomView(); 
        $data['data']['Object'] = $Object;

        return API_Controller::successResponse($data, 1, "Displayed columns has been updated successfully.", "True");
    }

    function DeleteCustomView(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'Object' => $Object,
            'OrganizationID' => $OrganizationID,
            'ViewID' => $ViewID
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $result = $this->ViewModel->DeleteCustomView(); 

        return API_Controller::responseMessage(1, "View has been deleted successfully.", "True");
    }

    function SortCustomView(){
 
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'Object' => $Object,
            'OrganizationID' => $OrganizationID,
            'ViewID' => $ViewID,
            'SortByField' => $SortByField,
            'SortByValue' => $SortByValue
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $data['data'][$Object.'ViewID'] = $this->ViewModel->SortCustomView(); 
        $data['data']['Object'] = $Object;

        return API_Controller::successResponse($data, 1, "View sorting has been updated successfully.", "True");
    }
}
