<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class Data extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Account/DataModel');
    }
  
    function GetAccountType(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->DataModel->GetAccountType($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Account type fetch successfully.', 'True');
    }

    function GetAccounts(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->DataModel->GetAccounts($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Account list fetch successfully.', 'True');
    }
}
