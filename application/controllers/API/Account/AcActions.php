<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class AcActions extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Account/AcActionsModel');
    }
  
    function CreateAccount() { 
      
        API_Controller::varifyMethod("POST");

        $AccessNotes = !empty($_POST['AccessNotes'])?$_POST['AccessNotes']:"";
        $PopUpReminder = !empty($_POST['PopUpReminder'])?$_POST['PopUpReminder']:"";
        $Notes = !empty($_POST['Notes'])?$_POST['Notes']:"";
        $PreferredTechnician = !empty($_POST['PreferredTechnician'])?$_POST['PreferredTechnician']:NULL;
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'OrganizationID' => $OrganizationID,
            'UserID' => $UserID,
            'AssignedTo' => $AssignedTo,
            'AccountName' => $AccountName,
            'PhoneNo' => $PhoneNo,
            'BillingAddress' => $BillingAddress,
            'BillingCity' => $BillingCity,
            'BillingState' => $BillingState,
            'BillingCountry' => $BillingCountry,
            'BillingPostalCode' => $BillingPostalCode,
            'BillingLatitude' => $BillingLatitude,
            'BillingLongitude' => $BillingLongitude,
            'ShippingAddress' => $ShippingAddress,
            'ShippingCity' => $ShippingCity,
            'ShippingState' => $ShippingState,
            'ShippingCountry' => $ShippingCountry,
            'ShippingPostalCode' => $ShippingPostalCode,
            'ShippingLatitude' => $ShippingLatitude,
            'ShippingLongitude' => $ShippingLongitude,
            'AccountType' => $AccountType,
            'IsActive' => $IsActive
        ]);

        API_Controller::checkUserAuthentication($UserID); 

        $result = $this->AcActionsModel->CreateAccount($OrganizationID, $UserID, $AssignedTo, $AccountName, $PhoneNo, $BillingAddress, $BillingCity, $BillingState, $BillingCountry, $BillingPostalCode, $BillingLatitude, $BillingLongitude, $ShippingAddress, $ShippingCity, $ShippingState, $ShippingCountry, $ShippingPostalCode, $ShippingLatitude, $ShippingLongitude, $AccessNotes, $PopUpReminder, $Notes, $AccountType, $PreferredTechnician, $IsActive); 

        return API_Controller::successResponse($result, 1, "Account has been created successfully.", "True");
    }

    function RecentAccounts() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 

        $result = API_Controller::removeNull($this->AcActionsModel->RecentAccounts($UserID, $OrganizationID));
 
        $data['data'] = $result;//$this->AcActionsModel->RecentMyAccounts($UserID);  

        API_Controller::successResponse($data, 1, 'Recent accounts fetch successfully.', 'True');
    }

    function AccountDetails() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNull($this->AcActionsModel->AccountDetails($UserID, $AccountID, $OrganizationID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Account details fetch successfully.', 'True');
    }  

    function AccountRelatedList() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedList($UserID, $AccountID, $OrganizationID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Account related list fetch successfully.', 'True');
    }  

    function AccountRelatedContact() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedContact($UserID, $AccountID, $OrganizationID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);  

        API_Controller::successResponse($data, 1, 'Account related contact fetch successfully.', 'True');
    }  

    function AccountRelatedLocation() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedLocation($UserID, $AccountID, $OrganizationID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Account related location fetch successfully.', 'True');
    }  

    function AccountRelatedWorkOrder() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedWorkOrder($UserID, $AccountID, $OrganizationID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);   

        API_Controller::successResponse($data, 1, 'Account related work order fetch successfully.', 'True');
    }

    function AccountRelatedEstimate() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedEstimate($UserID, $AccountID, $OrganizationID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);   

        API_Controller::successResponse($data, 1, 'Account related estimate fetch successfully.', 'True');
    }

    function AccountRelatedInvoice() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedInvoice($UserID, $AccountID, $OrganizationID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);   

        API_Controller::successResponse($data, 1, 'Account related invoice fetch successfully.', 'True');
    }

    function AccountRelatedTask() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedTask($UserID, $AccountID, $OrganizationID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);   

        API_Controller::successResponse($data, 1, 'Account related task fetch successfully.', 'True');
    }

    function AccountRelatedEvent() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedEvent($UserID, $AccountID, $OrganizationID);

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'Account related event fetch successfully.', 'True');
    }

    function AccountRelatedFile() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedFile($UserID, $AccountID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Account related file fetch successfully.', 'True');
    }
 
    function AccountRelatedChemical() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedChemical($UserID, $AccountID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Account related chemical fetch successfully.', 'True');
    }

    function AccountRelatedProduct() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountID' => $AccountID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->AcActionsModel->AccountRelatedProduct($UserID, $AccountID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Account related product fetch successfully.', 'True');
    }

    function EditAccount() { 
      
        API_Controller::varifyMethod("POST");

        $AccessNotes = !empty($_POST['AccessNotes'])?$_POST['AccessNotes']:NULL;
        $PopUpReminder = !empty($_POST['PopUpReminder'])?$_POST['PopUpReminder']:NULL;
        $Notes = !empty($_POST['Notes'])?$_POST['Notes']:NULL;
        $PrimaryContact = !empty($_POST['PrimaryContact'])?$_POST['PrimaryContact']:NULL;
        $PreferredTechnician = !empty($_POST['PreferredTechnician'])?$_POST['PreferredTechnician']:NULL;
 
        extract($_POST);

         API_Controller::requiredValidation([
            'OrganizationID' => $OrganizationID,
            'AccountID' => $AccountID,
            'UserID' => $UserID,
            'AssignedTo' => $AssignedTo,
            'AccountName' => $AccountName,
            'PhoneNo' => $PhoneNo,
            'BillingAddress' => $BillingAddress,
            'BillingCity' => $BillingCity,
            'BillingState' => $BillingState,
            'BillingCountry' => $BillingCountry,
            'BillingPostalCode' => $BillingPostalCode,
            'BillingLatitude' => $BillingLatitude,
            'BillingLongitude' => $BillingLongitude,
            'ShippingAddress' => $ShippingAddress,
            'ShippingCity' => $ShippingCity,
            'ShippingState' => $ShippingState,
            'ShippingCountry' => $ShippingCountry,
            'ShippingPostalCode' => $ShippingPostalCode,
            'ShippingLatitude' => $ShippingLatitude,
            'ShippingLongitude' => $ShippingLongitude,
            'AccountType' => $AccountType,
            'LastActivityDate' => $LastActivityDate,
            'LastServiceDate' => $LastServiceDate,
            'IsActive' => $IsActive
        ]);

        API_Controller::checkUserAuthentication($UserID, $AccountID); 

        $result = $this->AcActionsModel->EditAccount($OrganizationID, $AccountID, $UserID, $AssignedTo, $AccountName, $PhoneNo, $BillingAddress, $BillingCity, $BillingState, $BillingCountry, $BillingPostalCode, $BillingLatitude, $BillingLongitude, $ShippingAddress, $ShippingCity, $ShippingState, $ShippingCountry, $ShippingPostalCode, $ShippingLatitude, $ShippingLongitude, $AccessNotes, $PopUpReminder, $Notes, $AccountType, $PreferredTechnician,$LastActivityDate, $LastServiceDate, $PrimaryContact, $IsActive);  

        return API_Controller::responseMessage(1, "Account details has been updated successfully.", "True");
    }  
}
