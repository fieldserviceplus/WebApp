<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class View extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Account/ViewModel');
    }
  
    function GetViews(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->ViewModel->GetViews($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Account view fetch successfully.', 'True');
    }

    function ViewAccountList(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'AccountViewID' => $AccountViewID,
            'OrganizationID' => $OrganizationID
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->ViewModel->ViewAccountList($OrganizationID, $UserID, $AccountViewID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);  

        API_Controller::successResponse($data, 1, 'Accounts fetch successfully.', 'True');
    }

    function AccountFilter() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'OrganizationID' => $OrganizationID,
            'UserID' => $UserID,
            'AccountViewID' => $AccountViewID,
            'FilterFields[]' => $FilterFields[0],
            'FilterConditions[]' => $FilterConditions[0],
            'FilterValues[]' => $FilterValues[0],
            'SortByField' => $SortByField,
            'SortByValue' => $SortByValue
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ViewModel->AccountFilter($OrganizationID, $UserID, $AccountViewID, $FilterFields, $FilterConditions, $FilterValues, $SortByField, $SortByValue);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Account filter has been saved successfully.', 'True');
    } 

    function GetAccountViewFields() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ViewModel->GetAccountViewFields($UserID, $OrganizationID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Account view fields fetch successfully.', 'True');
    } 
}
