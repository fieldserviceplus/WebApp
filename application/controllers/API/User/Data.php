<?php

require_once(APPPATH.'controllers/API/API_Controller.php');
 
header('Content-type: application/json; charset=utf-8');


class Data extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/User/DataModel'); 
    }
    
    function GetAllUsers(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->DataModel->GetAllUsers($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Users fetch successfully.', 'True');
    }

}
