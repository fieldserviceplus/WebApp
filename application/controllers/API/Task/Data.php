<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class Data extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Task/DataModel');
    }
  
    function GetRelatedObjList(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->DataModel->GetRelatedObjList($UserID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Related object list fetch successfully.', 'True');
    }

    function GetRelatedToList(){
 
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'RelatedTo' => $RelatedTo,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->DataModel->GetRelatedToList($UserID, $OrganizationID, $RelatedTo);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Related to list fetch successfully.', 'True');
    }

    function GetTaskStatus(){
 
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->DataModel->GetTaskStatus($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Task status fetch successfully.', 'True');
    }

    function GetTaskPriority(){
 
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->DataModel->GetTaskPriority($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Task priority fetch successfully.', 'True');
    }

    function GetTaskType(){
 
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->DataModel->GetTaskType($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Task priority fetch successfully.', 'True');
    }

}
