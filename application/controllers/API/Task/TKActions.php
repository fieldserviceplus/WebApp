<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class TKActions extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Task/TKActionsModel');
    }
  
    function CreateTask() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        $Description = isset($_POST['Description'])?$_POST['Description']:NULL;
        $Email = isset($_POST['Email'])?$_POST['Email']:NULL;
        $Phone = isset($_POST['Phone'])?$_POST['Phone']:NULL;
   
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'Subject' => $Subject,
            'AssignedTo' => $AssignedTo,
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            'Who' => $Who,
            'TaskStatus' => $TaskStatus,
            'TaskPriority' => $TaskPriority,
            'TaskType' => $TaskType
        ]);

        if(isset($IsRecurring) && $IsRecurring == 1){
 
            $RepeatOn = !empty($_POST['RepeatOn'])?$_POST['RepeatOn']:NULL;
            $EndsOnDate = !empty($_POST['EndsOnDate'])?date("Y-m-d",strtotime($_POST['EndsOnDate'])):NULL;
            $EndsAfterOccurrences = !empty($_POST['EndsAfterOccurrences'])?$_POST['EndsAfterOccurrences']:NULL;
            API_Controller::requiredValidation([
                'StartOn' => $StartOn,
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'Ends' => $Ends,
                'StartTime' => $StartTime,
                'EndTime' => $EndTime
            ]);
        } else {
            $IsRecurring = 0;
            $StartOn = NULL;
            $RepeatEvery = NULL;
            $IntervalEvery = NULL;
            $Ends = NULL;
            $RepeatOn = NULL;
            $EndsOnDate = NULL;
            $EndsAfterOccurrences = NULL;
            $StartTime = NULL;
            $EndTime = NULL;  
        }
   
        API_Controller::checkUserAuthentication($UserID); 

        $data = $this->TKActionsModel->CreateTask($UserID, $OrganizationID, $Subject, $RelatedTo, $What, $Who, $Description, $Email, $TaskStatus, $Phone, $TaskPriority, $TaskType, $IsRecurring, $StartOn, $RepeatEvery, $IntervalEvery, $Ends, $RepeatOn, $EndsOnDate, $EndsAfterOccurrences, $StartTime, $EndTime); 

        return API_Controller::successResponse($data, 1, "Task has been created successfully.", "True");
    }

    function RecentTasks() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);

        API_Controller::checkUserAuthentication($UserID); 
 
        $result = $this->TKActionsModel->RecentTasks($UserID, $OrganizationID);  

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Recent accounts fetch successfully.', 'True');
    } 

    function TaskDetails() { 
                                                                        
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'TaskID' => $TaskID
        ]);
 
        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNull($this->TKActionsModel->TaskDetails($UserID, $TaskID, $OrganizationID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Task details fetch successfully.', 'True');
    }  

    function EditTask() { 
      
        API_Controller::varifyMethod("POST");

        extract($_POST);

        $_POST['Description'] = isset($_POST['Description'])?$_POST['Description']:NULL;
        $_POST['Email'] = isset($_POST['Email'])?$_POST['Email']:NULL;
        $_POST['Phone'] = isset($_POST['Phone'])?$_POST['Phone']:NULL;

        API_Controller::requiredValidation([
            'TaskID' => $TaskID,
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'Subject' => $Subject,
            'AssignedTo' => $AssignedTo,
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            'Who' => $Who,
            'TaskStatus' => $TaskStatus,
            'TaskPriority' => $TaskPriority,
            'TaskType' => $TaskType
        ]);

        API_Controller::checkUserAuthentication($UserID); 

        $result = $this->TKActionsModel->EditTask(); 

        return API_Controller::responseMessage(1, "Task has been updated successfully.", "True");
    }  

    function MarkATask() { 
      
        API_Controller::varifyMethod("POST");

        extract($_POST);

        API_Controller::requiredValidation([
            'TaskID' => $TaskID,
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);

        API_Controller::checkUserAuthentication($UserID); 

        $result = $this->TKActionsModel->MarkATask(); 

        return API_Controller::responseMessage(1, "Task status has been updated successfully.", "True");
    }  
}
