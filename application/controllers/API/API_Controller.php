<?php
class API_Controller extends CI_Controller
{ 
    function __construct()
    {
        parent::__construct();  
 
        $this->load->model('API/User/AuthModel');     
    }

    static function getPostData()
    {
        try {

            $getData = json_decode(file_get_contents('php://input'), TRUE);

            if (empty($getData)){
                throw new Exception(API_Controller::responseMessage(0, "Something went wrong.", "False"));
                   
            } else {
                
                return $getData['PostData'];
            }
        }catch (Exception $e){ exit;} 
    }
    
    static function varifyMethod($method_type)
    {	
        try {
            if ($_SERVER['REQUEST_METHOD'] != $method_type){
                throw new Exception(API_Controller::responseMessage(0, "It seems wrong method type is being used. Please verify method type (GET/POST).", "False"));
                   
            }
        }catch (Exception $e){ exit;}   
    }
    
    static function generateToken() 
    {
        $token = openssl_random_pseudo_bytes(16);
        return $token = bin2hex($token); 
    }

    static function removeNull($elements_array) 
    {
        foreach ($elements_array as $key => $value) { 
            $elements_array[$key] = is_null($value)?"":$elements_array[$key];   
        }
        return $elements_array; 
    }

    static function removeNullFromMultiDimArr($elements_array) 
    {
        foreach ($elements_array as $key2 => $element_array) { 
            $elements_a = array();
            foreach ($element_array as $key => $value) { 
                $element_array[$key] = is_null($value)?"":$element_array[$key];
            }   
            $elements_array[$key2] = $element_array;
        }
        return $elements_array; 
    }
     
    static function requiredValidation($elements_array) 
    {
        foreach ($elements_array as $key => $value) {
            try { 
                if (is_null($value))
                {
                    throw new Exception(API_Controller::responseMessage(0, 'Please enter ' . $key . '.', "False"));
                }
            }catch (Exception $e){ exit;}   
        }
    }
    
    static function successResponse($array, $response, $message, $result) 
    {  
        $array['ResponseCode'] = $response; 
        $array['ResponseMsg'] = $message;
        $array['Result'] = $result;
        $array['ServerTime'] = date('T');

        echo json_encode($array);
    }
    
    static function responseMessage($res, $responseMessage, $result)
    {
        echo json_encode(array("ResponseCode"=>$res,"ResponseMsg"=> "$responseMessage","Result"=>"$result","ServerTime"=>date('T')));
    }
    
    static function errorResponse()
    {
        return API_Controller::responseMessage(0,"It seems web-server is too much busy, please try again.","False");
    }
    
    static function genNumRandCode($length) 
    {
        $alphabet = '1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return strtoupper(implode($pass));
    }

    static function genCode($prefix, $id) 
    {
        return $prefix.'-'.sprintf("%05d", $id); 
    }
    
    static function sendMail($email, $subject, $message) 
    {
        $CI =& get_instance();
        $CI->load->library('email');
        $CI->email->set_newline("\r\n");
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;
        $config['smtp_host'] = 'mail.bcat.org.uk';
        $config['smtp_port'] = '25';
        $config['smtp_user'] = 'fieldserviceplus@bcat.org.uk';
        $config['smtp_pass'] = '.n,,~EC;@u^o';
        $config['smtp_crypto'] = 'tls'; 
        $config['protocol'] = 'smtp'; 
        $config['mailtype'] = 'html'; 
        $config['send_multipart'] = FALSE;
        $CI->email->initialize($config);
        $CI->email->from(ADMIN_EMAIL);
        $CI->email->to($email);
        $CI->email->subject($subject);
        $CI->email->message($message);
        return $CI->email->send(); 
    }
    
    static function checkUserAuthentication($iUserID) 
    {  
        $key = empty($_SERVER["HTTP_KEY"]) ? "" : $_SERVER["HTTP_KEY"];
        $token = empty($_SERVER["HTTP_TOKEN"]) ? "" : $_SERVER["HTTP_TOKEN"];
        API_Controller::requiredValidation([
            'user_id' => $iUserID, 
            'key' => $key, 
            'token' => $token
        ]); 
        $CI=&get_instance();
        $CI->load->model('API/User/AuthModel');
        $result = $CI->AuthModel->checkUserAuthentication($iUserID, $token);
      
        if(AUTH_KEY != $key)
        {
            try{
                throw new Exception(API_Controller::ResponseMessage(0, 'Something went wrong while generating key, please try again.', "False"));
            }catch (Exception $e){ exit;}   
        } else if($result == 0){
             try{
                    $rand_code = 200;  
                    throw new Exception(API_Controller::ResponseMessage($rand_code, 'You are not authorized to access associated web-services; it seems there is mismatch in key-token pair.', "False"));
            }catch (Exception $e){ exit;}   
        }
    } 
    
    static function uploadImage($image_param_name)
    { 
        $new_image_name = 'image_'. uniqid().'_'.date('YmdHis');
        $config = array(
         'upload_path' => IMAGE_PATH,
         'file_name' => $new_image_name,
         'allowed_types' => "gif|jpg|png|jpeg",
         'overwrite' => TRUE
         //'max_size' => "1024000"   'max_height' => "768", 'max_width' => "1024" // Can be set to particular file size , here it is 1 MB(1024 Kb)
        );
        $CI = &get_instance();
        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);
        
        if($CI->upload->do_upload($image_param_name)){
            $fileData = $CI->upload->data();
            chmod(IMAGE_PATH.$fileData['file_name'], 0777);
            return $file_name = $fileData['file_name'];
        }
    }

    /*static function uploadImage($image_name)
    { 
        $image_name = str_replace('data:image/jpeg;base64,', '', $image_name);
        $image_name = str_replace(' ', '+', $image_name);
        $data = base64_decode($image_name);
        $vPhoto = uniqid() . '.jpeg';
        $file = IMAGE_PATH . $vPhoto;
        $success = file_put_contents($file, $data);
        chmod($file, 0777);
        return $success ? $vPhoto : false;
    }*/
    
    static function resizeImage($file_name)
    {
        $CI =&get_instance();
        $CI->load->library('image_lib');
        $config1['image_library'] = 'gd2';
        $config1['source_image'] = IMAGE_PATH.$file_name;
        $config1['create_thumb'] = FALSE;
        $config1['maintain_ratio'] = TRUE;
        $config1['new_image'] = IMAGE_THUMB_PATH.$file_name;
        $config1['width'] = 100;
        $config1['height'] = 100;

        $CI->image_lib->clear();
        $CI->image_lib->initialize($config1);
        $CI->image_lib->resize();

        chmod($config1['new_image'], 0777);
    }
    
    static function SendSMSUsingTwilio($to_phone_no, $message)
    { 
        $url = "https://api.twilio.com/2010-04-01/Accounts/".TWILIO_ID."/SMS/Messages.json";
        $id = TWILIO_ID;
        $token = TWILIO_TOKEN;
        
        $data = array (
                'From' => TWILIO_FROM_NO,
                'To' => $to_phone_no,       // twilio trial verified number
                'Body' => $message,
            );
        $post = http_build_query($data);
        $x = curl_init($url);
        curl_setopt($x, CURLOPT_POST, true);
        curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
        curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($x, CURLOPT_POSTFIELDS, $post);
        $y = curl_exec($x);
        //print_r($y); exit; 
        curl_close($x);
           
        return;
    }
    
    static function sendPushIos($device_udid_array,$message)
    { 
        $tHost = 'gateway.sandbox.push.apple.com';
        //$tHost = 'gateway.push.apple.com:2195';
        $tPort = 2195;
        $tCert =  IOS_PEM_FILE_PATH;
        $tPassphrase = IOS_PASSPHRASE; 
        $tAlert = $message;
        $tBadge = 0;
        $tSound = 'default';
        $tPayload = 'Success';
        $tBody['aps'] = array ('alert' => $tAlert,'badge' => $tBadge,'sound' => $tSound);
        $tBody ['payload'] = $tPayload;
        $tBody = json_encode ($tBody);
        $tContext = stream_context_create ();
        stream_context_set_option ($tContext, 'ssl', 'local_cert', $tCert);
        stream_context_set_option ($tContext, 'ssl', 'passphrase', $tPassphrase);
        $tSocket = stream_socket_client ('ssl://'.$tHost.':'.$tPort, $error, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $tContext);
        if (!$tSocket)
        {
                exit ("APNS Connection Failed: $error $errstr" . PHP_EOL);
        }
        else
        {
            for($x=0;$x<count($device_udid_array);$x++)
            { 
                $tMsg = chr (0) . chr (0) . chr (32) . pack ('H*', $device_udid_array[$x]) . pack ('n', strlen ($tBody)) . $tBody;
                $tResult = fwrite ($tSocket, $tMsg, strlen ($tMsg));
            } 
        }

        fclose ($tSocket);
        return $tResult;
    }
	
    static function sendPushAndroid($device_udid_array, $message)
    { 
        $url = 'https://fcm.googleapis.com/fcm/send';
 
        $fields = array(
            'registration_ids' => $device_udid_array,
            'data' => array(
               "message" => $message
            ),
            'notification' => array(
              "title" => 'Contactuall',
              "text" => $message
            ),
       );

        $headers = array(
           'Authorization: key=' . ANDROID_AUTHORIZATION_KEY,
           'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch); 
        if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
        }
        //print_r($result); exit; 
        curl_close($ch);
    }
    
    static function responseDate($date)
    {
        return date("m/d/Y", strtotime($date));
    }

    static function responseDateTime($datetime)
    { 
        return date("m/d/Y h:i A", strtotime($datetime));
    }

    static function convertDocxToPDF($DocxFilePath, $PDFFilePath)
    {
        // Uncomment this for debugging, if you want errors shown in the browser, rather than the error log file
        //ini_set('display_errors',1);
        //error_reporting(E_ALL);

        // In this example, we read a docx from a file
        $handle = fopen($DocxFilePath, "rb");
        $contents = fread($handle, filesize($DocxFilePath));
        fclose($handle);

        // Service endpoint
        $url = 'http://converter-eval.plutext.com/v1/00000000-0000-0000-0000-000000000000/convert';

        $options = array(
                'http' => array(
                'header'  => "Content-type: application/octet-stream\r\n",
                'method'  => 'POST',
                'content' => $contents
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);

        // Assume all went well; write the PDF output to a file
        // Another example might stream it to the user
        $myfile = fopen($PDFFilePath, "w") or die("Unable to open file!");
        fwrite($myfile, $result);
        fclose($myfile);

        chmod($PDFFilePath, 0777);
    }
}