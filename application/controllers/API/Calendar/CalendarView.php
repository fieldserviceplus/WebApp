<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class CalendarView extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Calendar/CalendarViewModel');
    }
  
    function GetEventTaskByDates(){

        API_Controller::varifyMethod("POST");

        $_POST['WorkOrderType'] = isset($_POST['WorkOrderType'])?$_POST['WorkOrderType']:NULL;
        $_POST['WOStatus'] = isset($_POST['WOStatus'])?$_POST['WOStatus']:NULL;
        $_POST['WOPriority'] = isset($_POST['WOPriority'])?$_POST['WOPriority']:NULL;
        $_POST['AssignedTo'] = isset($_POST['AssignedTo'])?$_POST['AssignedTo']:NULL;
        $_POST['ShowTasks'] = isset($_POST['ShowTasks'])?$_POST['ShowTasks']:1;
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'FromDate' => $FromDate,
            'ToDate' => $ToDate
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->CalendarViewModel->GetEventTaskByDates($UserID, $OrganizationID, $FromDate, $ToDate, $WorkOrderType, $WOStatus, $WOPriority, $AssignedTo, $ShowTasks);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Events fetch successfully.', 'True');
    }

    function CalendarFilterData(){

        API_Controller::varifyMethod("POST");

        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->CalendarViewModel->CalendarFilterData($OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Calendar filter data fetch successfully.', 'True');
    }

    function GetWorkOrderByDate(){

        API_Controller::varifyMethod("POST");

        $_POST['WorkOrderType'] = isset($_POST['WorkOrderType'])?$_POST['WorkOrderType']:NULL;
        $_POST['WOStatus'] = isset($_POST['WOStatus'])?$_POST['WOStatus']:NULL;
        $_POST['WOPriority'] = isset($_POST['WOPriority'])?$_POST['WOPriority']:NULL;
        $_POST['AssignedTo'] = isset($_POST['AssignedTo'])?$_POST['AssignedTo']:NULL;
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'FromDate' => $FromDate,
            'ToDate' => $ToDate
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->CalendarViewModel->GetWorkOrderByDate($UserID, $OrganizationID, $FromDate, $ToDate, $WorkOrderType, $WOStatus, $WOPriority, $AssignedTo);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Work order fetch successfully.', 'True');
    }
}
