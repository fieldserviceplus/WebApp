<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class LCActions extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Location/LCActionsModel');
    }
  
    function CreateLocation() { 
      
        API_Controller::varifyMethod("POST");

        $_POST['Notes'] = isset($_POST['Notes'])?$_POST['Notes']:NULL;
        $_POST['AccessNotes'] = isset($_POST['AccessNotes'])?$_POST['AccessNotes']:NULL;
        $_POST['PrimaryContact'] = isset($_POST['PrimaryContact'])?$_POST['PrimaryContact']:NULL;
        $_POST['PreferredTechnician'] = isset($_POST['PreferredTechnician'])?$_POST['PreferredTechnician']:NULL;
        $_POST['ParentLocation'] = isset($_POST['ParentLocation'])?$_POST['ParentLocation']:NULL;

        extract($_POST);
   
        API_Controller::requiredValidation([
            'OrganizationID' => $OrganizationID,
            'UserID' => $UserID,
            'Name' => $Name,
            'Account' => $Account,
            'Owner' => $Owner,
            'Address' => $Address,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => $Latitude,
            'Longitude' => $Longitude,
            'LocationType' => $LocationType,
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $data = $this->LCActionsModel->CreateLocation(); 

        return API_Controller::successResponse($data, 1, "Location has been created successfully.", "True");
    }

    function RecentLocations() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);

        API_Controller::checkUserAuthentication($UserID); 
 
        $data['data'] = API_Controller::removeNullFromMultiDimArr($this->LCActionsModel->RecentLocations($UserID, $OrganizationID));  

        API_Controller::successResponse($data, 1, 'Recent accounts fetch successfully.', 'True');
    } 

    function LocationDetails() { 
                                                                        
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'LocationID' => $LocationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNull($this->LCActionsModel->LocationDetails($UserID, $LocationID, $OrganizationID));

        $data['data'] = $result                                                                                                                                                                                                                                                                                                         ;  

        API_Controller::successResponse($data, 1, 'Location details fetch successfully.', 'True');
    }  

    /*function LocationRelatedList() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'LocationID' => $LocationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->LCActionsModel->LocationRelatedList($UserID, $LocationID, $OrganizationID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Location related list fetch successfully.', 'True');
    } 

    function LocationRelatedLineItem() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'LocationID' => $LocationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->LCActionsModel->LocationRelatedLineItem($UserID, $LocationID, $OrganizationID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);  

        API_Controller::successResponse($data, 1, 'Location related line item fetch successfully.', 'True');
    }   

    function LocationRelatedEvent() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'LocationID' => $LocationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNullFromMultiDimArr($this->LCActionsModel->LocationRelatedEvent($UserID, $LocationID, $OrganizationID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Location related event fetch successfully.', 'True');
    }  

    function LocationRelatedNote() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'LocationID' => $LocationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNullFromMultiDimArr($this->LCActionsModel->LocationRelatedNote($UserID, $LocationID, $OrganizationID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Location related event fetch successfully.', 'True');
    }  

    function LocationRelatedFile() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'LocationID' => $LocationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->LCActionsModel->LocationRelatedFile($UserID, $LocationID, $OrganizationID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Location related file fetch successfully.', 'True');
    }  

    function LocationRelatedTask() { 
       
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'LocationID' => $LocationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNullFromMultiDimArr($this->LCActionsModel->LocationRelatedTask($UserID, $LocationID, $OrganizationID));

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'Location related task fetch successfully.', 'True');
    }*/

    function EditLocation() { 
      
        API_Controller::varifyMethod("POST");

        extract($_POST);

        $_POST['Notes'] = isset($_POST['Notes'])?$_POST['Notes']:NULL;
        $_POST['AccessNotes'] = isset($_POST['AccessNotes'])?$_POST['AccessNotes']:NULL;
        $_POST['PrimaryContact'] = isset($_POST['PrimaryContact'])?$_POST['PrimaryContact']:NULL;
        $_POST['PreferredTechnician'] = isset($_POST['PreferredTechnician'])?$_POST['PreferredTechnician']:NULL;
        $_POST['ParentLocation'] = isset($_POST['ParentLocation'])?$_POST['ParentLocation']:NULL;
 
         API_Controller::requiredValidation([
            'OrganizationID' => $OrganizationID,
            'UserID' => $UserID,
            'LocationID' => $LocationID,
            'Name' => $Name,
            'Account' => $Account,
            'Owner' => $Owner,
            'Address' => $Address,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => $Latitude,
            'Longitude' => $Longitude,
            'LocationType' => $LocationType,
        ]);

        API_Controller::checkUserAuthentication($UserID); 

        $result = $this->LCActionsModel->EditLocation(); 

        return API_Controller::responseMessage(1, "Location has been updated successfully.", "True");
    }  
}
