<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class View extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Location/ViewModel');
    }
  
    function GetViews(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->ViewModel->GetViews($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Location view fetch successfully.', 'True');
    }

    function ViewLocationList(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'LocationViewID' => $LocationViewID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = API_Controller::removeNullFromMultiDimArr($this->ViewModel->ViewLocationList($UserID, $LocationViewID, $OrganizationID));

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Locations fetch successfully.', 'True');
    }

    function LocationFilter() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'LocationViewID' => $LocationViewID,
            'FilterFields[]' => $FilterFields[0],
            'FilterConditions[]' => $FilterConditions[0],
            'FilterValues[]' => $FilterValues[0],
            'SortByField' => $SortByField,
            'SortByValue' => $SortByValue,
            'OrganizationID' => $OrganizationID,
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ViewModel->LocationFilter($UserID, $LocationViewID, $FilterFields, $FilterConditions, $FilterValues, $SortByField, $SortByValue, $OrganizationID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Location filter has been saved successfully.', 'True');
    } 

    function GetLocationViewFields() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ViewModel->GetLocationViewFields($UserID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Location view fields fetch successfully.', 'True');
    } 
}
