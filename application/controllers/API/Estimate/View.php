<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class View extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Estimate/ViewModel');
    }
  
    function GetViews(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->ViewModel->GetViews($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Estimate view fetch successfully.', 'True');
    }

    function ViewEstimateList(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'EstimateViewID' => $EstimateViewID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = API_Controller::removeNullFromMultiDimArr($this->ViewModel->ViewEstimateList($UserID, $EstimateViewID, $OrganizationID));

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Estimates fetch successfully.', 'True');
    }

    function EstimateFilter() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'EstimateViewID' => $EstimateViewID,
            'FilterFields[]' => $FilterFields[0],
            'FilterConditions[]' => $FilterConditions[0],
            'FilterValues[]' => $FilterValues[0],
            'SortByField' => $SortByField,
            'SortByValue' => $SortByValue,
            'OrganizationID' => $OrganizationID,
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ViewModel->EstimateFilter($UserID, $EstimateViewID, $FilterFields, $FilterConditions, $FilterValues, $SortByField, $SortByValue, $OrganizationID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Estimate filter has been saved successfully.', 'True');
    } 

    function GetEstimateViewFields() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ViewModel->GetEstimateViewFields($UserID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Estimate view fields fetch successfully.', 'True');
    } 
}
