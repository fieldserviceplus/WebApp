<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class ETActions extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Estimate/ETActionsModel');
    }
  
    function CreateEstimate() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
   
        API_Controller::requiredValidation([
            'OrganizationID' => $OrganizationID,
            'UserID' => $UserID,
            'EstimateName' => $EstimateName,
            'Account' => $Account,
            'Owner' => $Owner,
            'BillingAddress' => $BillingAddress,
            'BillingCity' => $BillingCity,
            'BillingState' => $BillingState,
            'BillingCountry' => $BillingCountry,
            'BillingPostalCode' => $BillingPostalCode,
            'BillingLatitude' => $BillingLatitude,
            'BillingLongitude' => $BillingLongitude,
            'BillingName' => $BillingName,
            'Description' => $Description,
            'ExpirationDate' => $ExpirationDate,
            'ShippingAddress' => $ShippingAddress,
            'ShippingCity' => $ShippingCity,
            'ShippingState' => $ShippingState,
            'ShippingCountry' => $ShippingCountry,
            'ShippingPostalCode' => $ShippingPostalCode,
            'ShippingLatitude' => $ShippingLatitude,
            'ShippingLongitude' => $ShippingLongitude,
            'EstimateStatus' => $EstimateStatus
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $data = $this->ETActionsModel->CreateEstimate(); 

        return API_Controller::successResponse($data, 1, "Estimate has been created successfully.", "True");
    }

    function RecentEstimates() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);

        API_Controller::checkUserAuthentication($UserID); 
 
        $data['data'] = $this->ETActionsModel->RecentEstimates($UserID, $OrganizationID);  

        API_Controller::successResponse($data, 1, 'Recent accounts fetch successfully.', 'True');
    } 

    function EstimateDetails() { 
                                                                        
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'EstimateID' => $EstimateID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNull($this->ETActionsModel->EstimateDetails($UserID, $EstimateID, $OrganizationID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Estimate details fetch successfully.', 'True');
    }  

    function EstimateRelatedList() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'EstimateID' => $EstimateID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ETActionsModel->EstimateRelatedList($UserID, $EstimateID, $OrganizationID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Estimate related list fetch successfully.', 'True');
    } 

    function EstimateRelatedLineItem() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'EstimateID' => $EstimateID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ETActionsModel->EstimateRelatedLineItem($UserID, $EstimateID, $OrganizationID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);  

        API_Controller::successResponse($data, 1, 'Estimate related line item fetch successfully.', 'True');
    }   

    /*function EstimateRelatedChemical() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'EstimateID' => $EstimateID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ETActionsModel->EstimateRelatedChemical($UserID, $EstimateID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);  

        API_Controller::successResponse($data, 1, 'Estimate related chemical fetch successfully.', 'True');
    } 

    function EstimateRelatedInvoice() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'EstimateID' => $EstimateID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ETActionsModel->EstimateRelatedInvoice($UserID, $EstimateID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Estimate related invoice fetch successfully.', 'True');
    }   */

    function EstimateRelatedEvent() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'EstimateID' => $EstimateID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNullFromMultiDimArr($this->ETActionsModel->EstimateRelatedEvent($UserID, $EstimateID, $OrganizationID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Estimate related event fetch successfully.', 'True');
    }  

    function EstimateRelatedNote() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'EstimateID' => $EstimateID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNullFromMultiDimArr($this->ETActionsModel->EstimateRelatedNote($UserID, $EstimateID, $OrganizationID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Estimate related event fetch successfully.', 'True');
    }  

    function EstimateRelatedFile() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'EstimateID' => $EstimateID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ETActionsModel->EstimateRelatedFile($UserID, $EstimateID, $OrganizationID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Estimate related file fetch successfully.', 'True');
    }  

    function EstimateRelatedTask() { 
       
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'EstimateID' => $EstimateID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNullFromMultiDimArr($this->ETActionsModel->EstimateRelatedTask($UserID, $EstimateID, $OrganizationID));

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'Estimate related task fetch successfully.', 'True');
    }

    function GetProducts() { 
       
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'EstimateID' => $EstimateID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ETActionsModel->GetProducts();

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'Products fetch successfully.', 'True');
    }

    /*function GetChemicals() { 
       
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ETActionsModel->GetChemicals($UserID);

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'Chemicals fetch successfully.', 'True');
    }*/

    function EditEstimate() { 
      
        API_Controller::varifyMethod("POST");

        extract($_POST);

        $EstimateID = isset($EstimateID)?$EstimateID:NULL;
 
        API_Controller::requiredValidation([
            'OrganizationID' => $OrganizationID,
            'EstimateID' => $EstimateID,
            'UserID' => $UserID,
            'EstimateName' => $EstimateName,
            'Account' => $Account,
            'Owner' => $Owner,
            'BillingAddress' => $BillingAddress,
            'BillingCity' => $BillingCity,
            'BillingState' => $BillingState,
            'BillingCountry' => $BillingCountry,
            'BillingPostalCode' => $BillingPostalCode,
            'BillingLatitude' => $BillingLatitude,
            'BillingLongitude' => $BillingLongitude,
            'BillingName' => $BillingName,
            'Description' => $Description,
            'ExpirationDate' => $ExpirationDate,
            'ShippingAddress' => $ShippingAddress,
            'ShippingCity' => $ShippingCity,
            'ShippingState' => $ShippingState,
            'ShippingCountry' => $ShippingCountry,
            'ShippingPostalCode' => $ShippingPostalCode,
            'ShippingLatitude' => $ShippingLatitude,
            'ShippingLongitude' => $ShippingLongitude,
            'EstimateStatus' => $EstimateStatus
        ]);

        API_Controller::checkUserAuthentication($UserID); 

        $result = $this->ETActionsModel->EditEstimate(); 

        return API_Controller::responseMessage(1, "Estimate has been updated successfully.", "True");
    }  

    function SaveEstimateSignature() { 
       
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'EstimateID' => $EstimateID,
            'Signature' => $Signature
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ETActionsModel->SaveEstimateSignature($UserID, $EstimateID, $Signature, $OrganizationID);

        $data['data'] = $result;   

        API_Controller::responseMessage(1, 'Estimate signature save successfully.', 'True');
    }

    function SaveEstimateLineItems() { 
       
        API_Controller::varifyMethod("POST");
  
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'EstimateID' => $EstimateID,
           /* 'Product[]' => $Product,
            'ListPrice[]' => $ListPrice,
            'Discount[]' => $Discount,
            'UnitPrice[]' => $UnitPrice,
            'Quantity[]' => $Quantity,
            'SubTotal[]' => $SubTotal,
            'Taxable[]' => $Taxable,
            'TotalPrice[]' => $TotalPrice*/
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ETActionsModel->SaveEstimateLineItems(); 

        if($result)
            API_Controller::responseMessage(1, 'Estimate line items save successfully.', 'True');
        else 
            API_Controller::responseMessage(0, 'Something went wrong.', 'False');
    }

    function ConvertToWorkOrder() { 
                                                                        
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'EstimateID' => $EstimateID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ETActionsModel->ConvertToWorkOrder();

        $data['WorkOrderID'] = $result;  

        API_Controller::successResponse($data, 1, 'Work order converted successfully.', 'True');
    }  
}
