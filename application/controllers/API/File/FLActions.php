<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');
 
class FLActions extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/File/FLActionsModel');
    }
  
    function CreateFile() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        $_POST['Description'] = isset($_POST['Description'])?$_POST['Description']:"";
   
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'Subject' => $Subject,
            'AssignedTo' => $AssignedTo,
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            'FileName' => $_FILES['FileName']['name']
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $data = $this->FLActionsModel->CreateFile(); 

        if(is_array($data)){
            return API_Controller::successResponse($data, 1, "File has been created successfully.", "True");
        } else {
            return API_Controller::responseMessage(0, $data, "False");
        }
    }

    function RecentFiles() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);

        API_Controller::checkUserAuthentication($UserID); 
 
        $result = $this->FLActionsModel->RecentFiles($UserID, $OrganizationID);  

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Recent accounts fetch successfully.', 'True');
    } 

    function FileDetails() { 
                                                                        
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'FileID' => $FileID
        ]);
 
        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNull($this->FLActionsModel->FileDetails($UserID, $FileID, $OrganizationID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'File details fetch successfully.', 'True');
    }  

    function EditFile() { 
      
        API_Controller::varifyMethod("POST");

        extract($_POST);

        $_POST['Description'] = isset($_POST['Description'])?$_POST['Description']:"";

        API_Controller::requiredValidation([
            'FileID' => $FileID,
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'Subject' => $Subject,
            'AssignedTo' => $AssignedTo,
            'RelatedTo' => $RelatedTo,
            'What' => $What,
            /*'FileName' => $_FILES['FileName']['name']*/
        ]);

        API_Controller::checkUserAuthentication($UserID); 

        $result = $this->FLActionsModel->EditFile(); 
 
        if($result == 1){
            return API_Controller::responseMessage(1, "File has been updated successfully.", "True");
        } else {
            return API_Controller::responseMessage(0, $result, "False");
        }
        
    }  
}