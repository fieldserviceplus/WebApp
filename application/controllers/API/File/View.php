<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class View extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/File/ViewModel');
    }
  
    function GetViews(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->ViewModel->GetViews($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'File view fetch successfully.', 'True');
    }

    function ViewFileList(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'FileViewID' => $FileViewID
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = API_Controller::removeNullFromMultiDimArr($this->ViewModel->ViewFileList($UserID, $OrganizationID, $FileViewID));

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Files fetch successfully.', 'True');
    }

    function FileFilter() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'FileViewID' => $FileViewID,
            'FilterFields[]' => $FilterFields[0],
            'FilterConditions[]' => $FilterConditions[0],
            'FilterValues[]' => $FilterValues[0],
            'SortByField' => $SortByField,
            'SortByValue' => $SortByValue
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ViewModel->FileFilter($UserID, $OrganizationID, $FileViewID, $FilterFields, $FilterConditions, $FilterValues, $SortByField, $SortByValue);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'File filter has been saved successfully.', 'True');
    } 

    function GetFileViewFields() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ViewModel->GetFileViewFields($UserID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'File view fields fetch successfully.', 'True');
    } 
}
