<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class Data extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Home/DataModel');
    }
  
    function GetSchedule(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->DataModel->GetSchedule($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Schedule list fetch successfully.', 'True');
    }

    function GetTasks(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'Filter' => $Filter,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->DataModel->GetTasks($UserID, $OrganizationID, $Filter);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Task list fetch successfully.', 'True');
    }

    function GetRecents(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->DataModel->GetRecents($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Recents list fetch successfully.', 'True');
    }
}
