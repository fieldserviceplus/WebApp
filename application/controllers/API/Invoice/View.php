<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class View extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Invoice/ViewModel');
    }
  
    function GetViews(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->ViewModel->GetViews($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Invoice view fetch successfully.', 'True');
    }

    function ViewInvoiceList(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'InvoiceViewID' => $InvoiceViewID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = API_Controller::removeNullFromMultiDimArr($this->ViewModel->ViewInvoiceList($UserID, $InvoiceViewID, $OrganizationID));

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Invoices fetch successfully.', 'True');
    }

    function InvoiceFilter() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'InvoiceViewID' => $InvoiceViewID,
            'FilterFields[]' => $FilterFields[0],
            'FilterConditions[]' => $FilterConditions[0],
            'FilterValues[]' => $FilterValues[0],
            'SortByField' => $SortByField,
            'SortByValue' => $SortByValue,
            'OrganizationID' => $OrganizationID,
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ViewModel->InvoiceFilter($UserID, $InvoiceViewID, $FilterFields, $FilterConditions, $FilterValues, $SortByField, $SortByValue, $OrganizationID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Invoice filter has been saved successfully.', 'True');
    } 

    function GetInvoiceViewFields() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ViewModel->GetInvoiceViewFields($UserID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Invoice view fields fetch successfully.', 'True');
    } 
}
