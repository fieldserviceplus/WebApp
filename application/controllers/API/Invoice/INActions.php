<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class INActions extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Invoice/INActionsModel');
    }
  
    function CreateInvoice() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
   
        API_Controller::requiredValidation([
            'OrganizationID' => $OrganizationID,
            'UserID' => $UserID,
            'Account' => $Account,
            'AssignedTo' => $AssignedTo,
            'WorkOrder' => $WorkOrder,
            'Description' => $Description,
            'Address' => $Address,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => $Latitude,
            'Longitude' => $Longitude,
            'InvoiceStatus' => $InvoiceStatus,
            'InvoiceDate' => $InvoiceDate,
            'DueDate' => $DueDate,
            'PaymentTerms' => $PaymentTerms
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $data = $this->INActionsModel->CreateInvoice(); 

        return API_Controller::successResponse($data, 1, "Invoice has been created successfully.", "True");
    }

    function RecentInvoices() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);

        API_Controller::checkUserAuthentication($UserID); 
 
        $data['data'] = $this->INActionsModel->RecentInvoices($UserID, $OrganizationID);  

        API_Controller::successResponse($data, 1, 'Recent accounts fetch successfully.', 'True');
    } 

    function InvoiceDetails() { 
                                                                        
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'InvoiceID' => $InvoiceID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNull($this->INActionsModel->InvoiceDetails($UserID, $InvoiceID, $OrganizationID));

        $data['data'] = $result                                                                                                                                                                                                                                                                                                         ;  

        API_Controller::successResponse($data, 1, 'Invoice details fetch successfully.', 'True');
    }  

    function InvoiceRelatedList() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'InvoiceID' => $InvoiceID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->INActionsModel->InvoiceRelatedList($UserID, $InvoiceID, $OrganizationID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Invoice related list fetch successfully.', 'True');
    } 

    function InvoiceRelatedLineItem() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'InvoiceID' => $InvoiceID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->INActionsModel->InvoiceRelatedLineItem($UserID, $InvoiceID, $OrganizationID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);  

        API_Controller::successResponse($data, 1, 'Invoice related line item fetch successfully.', 'True');
    }   

    /*function InvoiceRelatedChemical() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'InvoiceID' => $InvoiceID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->INActionsModel->InvoiceRelatedChemical($UserID, $InvoiceID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);  

        API_Controller::successResponse($data, 1, 'Invoice related chemical fetch successfully.', 'True');
    } 

    function InvoiceRelatedInvoice() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'InvoiceID' => $InvoiceID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->INActionsModel->InvoiceRelatedInvoice($UserID, $InvoiceID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Invoice related invoice fetch successfully.', 'True');
    }   */

    function InvoiceRelatedEvent() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'InvoiceID' => $InvoiceID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNullFromMultiDimArr($this->INActionsModel->InvoiceRelatedEvent($UserID, $InvoiceID, $OrganizationID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Invoice related event fetch successfully.', 'True');
    }  

    function InvoiceRelatedNote() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'InvoiceID' => $InvoiceID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNullFromMultiDimArr($this->INActionsModel->InvoiceRelatedNote($UserID, $InvoiceID, $OrganizationID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Invoice related event fetch successfully.', 'True');
    }  

    function InvoiceRelatedFile() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'InvoiceID' => $InvoiceID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->INActionsModel->InvoiceRelatedFile($UserID, $InvoiceID, $OrganizationID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Invoice related file fetch successfully.', 'True');
    }  

    function InvoiceRelatedTask() { 
       
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'InvoiceID' => $InvoiceID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNullFromMultiDimArr($this->INActionsModel->InvoiceRelatedTask($UserID, $InvoiceID, $OrganizationID));

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'Invoice related task fetch successfully.', 'True');
    }

    function GetProducts() { 
       
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'InvoiceID' => $InvoiceID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->INActionsModel->GetProducts();

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'Products fetch successfully.', 'True');
    }

    /*function GetChemicals() { 
       
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->INActionsModel->GetChemicals($UserID);

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'Chemicals fetch successfully.', 'True');
    }*/

    function EditInvoice() { 
      
        API_Controller::varifyMethod("POST");

        extract($_POST);

        $InvoiceID = isset($InvoiceID)?$InvoiceID:NULL;
 
         API_Controller::requiredValidation([
            'OrganizationID' => $OrganizationID,
            'UserID' => $UserID,
            'Account' => $Account,
            'AssignedTo' => $AssignedTo,
            'WorkOrder' => $WorkOrder,
            'Description' => $Description,
            'Address' => $Address,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => $Latitude,
            'Longitude' => $Longitude,
            'InvoiceStatus' => $InvoiceStatus,
            'InvoiceDate' => $InvoiceDate,
            'DueDate' => $DueDate,
            'PaymentTerms' => $PaymentTerms
        ]);

        API_Controller::checkUserAuthentication($UserID); 

        $result = $this->INActionsModel->EditInvoice(); 

        return API_Controller::responseMessage(1, "Invoice has been updated successfully.", "True");
    }  

    /*function SaveInvoiceSignature() { 
       
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'InvoiceID' => $InvoiceID,
            'Signature' => $Signature
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->INActionsModel->SaveInvoiceSignature($UserID, $InvoiceID, $Signature, $OrganizationID);

        $data['data'] = $result;   

        API_Controller::responseMessage(1, 'Invoice signature save successfully.', 'True');
    }*/

    function SaveInvoiceLineItems() { 
       
        API_Controller::varifyMethod("POST");
  
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'InvoiceID' => $InvoiceID,
          /*  'Product[]' => $Product,
            'ListPrice[]' => $ListPrice,
            'Discount[]' => $Discount,
            'UnitPrice[]' => $UnitPrice,
            'Quantity[]' => $Quantity,
            'SubTotal[]' => $SubTotal,
            'Taxable[]' => $Taxable,
            'TotalPrice[]' => $TotalPrice*/
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->INActionsModel->SaveInvoiceLineItems(); 

        if($result)
            API_Controller::responseMessage(1, 'Invoice line items save successfully.', 'True');
        else 
            API_Controller::responseMessage(0, 'Something went wrong.', 'False');
    }
}
