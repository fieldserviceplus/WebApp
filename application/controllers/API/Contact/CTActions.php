<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class CTActions extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Contact/CTActionsModel');
    }
  
    function CreateContact() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
   
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'Salutation' => $Salutation,
            'Title' => $Title,
            'FirstName' => $FirstName,
            'LastName' => $LastName,
            'Account' => $Account,
            'AssignedTo' => $AssignedTo,
            'LeadSource' => $LeadSource,
            'MailingAddress' => $MailingAddress,
            'MailingCity' => $MailingCity,
            'MailingState' => $MailingState,
            'MailingPostalCode' => $MailingPostalCode,
            'MailingCountry' => $MailingCountry,
            'MailingLatitude' => $MailingLatitude,
            'MailingLongitude' => $MailingLongitude
        ]);
   
        API_Controller::checkUserAuthentication($UserID); 

        $data = $this->CTActionsModel->CreateContact(); 

        return API_Controller::successResponse($data, 1, "Contact has been created successfully.", "True");
    }

    function RecentContacts() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);

        API_Controller::checkUserAuthentication($UserID); 
 
        $data['data'] = $this->CTActionsModel->RecentContacts($UserID, $OrganizationID);  

        API_Controller::successResponse($data, 1, 'Recent accounts fetch successfully.', 'True');
    } 

    function ContactDetails() { 
                                                                        
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'ContactID' => $ContactID
        ]);
 
        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNull($this->CTActionsModel->ContactDetails($UserID, $ContactID, $OrganizationID));

        $data['data'] = $result                                                                                                                                                                                                                                                                                                         ;  

        API_Controller::successResponse($data, 1, 'Contact details fetch successfully.', 'True');
    }  

    function ContactRelatedList() { 
      
        API_Controller::varifyMethod("POST");
  
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'ContactID' => $ContactID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->CTActionsModel->ContactRelatedList($UserID, $ContactID, $OrganizationID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Contact related list fetch successfully.', 'True');
    } 

    function ContactRelatedAccount() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'ContactID' => $ContactID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->CTActionsModel->ContactRelatedAccount($UserID, $ContactID, $OrganizationID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);  

        API_Controller::successResponse($data, 1, 'Contact related accounts fetch successfully.', 'True');
    } 

    function ContactRelatedWorkOrder() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'ContactID' => $ContactID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->CTActionsModel->ContactRelatedWorkOrder($UserID, $ContactID, $OrganizationID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);  

        API_Controller::successResponse($data, 1, 'Contact related work orders fetch successfully.', 'True');
    }    

    function ContactRelatedEstimate() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'ContactID' => $ContactID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->CTActionsModel->ContactRelatedEstimate($UserID, $ContactID, $OrganizationID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);  

        API_Controller::successResponse($data, 1, 'Contact related estimates fetch successfully.', 'True');
    }   

    function ContactRelatedInvoice() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'ContactID' => $ContactID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->CTActionsModel->ContactRelatedInvoice($UserID, $ContactID, $OrganizationID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);  

        API_Controller::successResponse($data, 1, 'Contact related invoices fetch successfully.', 'True');
    }    

    function ContactRelatedNote() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'ContactID' => $ContactID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNullFromMultiDimArr($this->CTActionsModel->ContactRelatedNote($UserID, $ContactID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Contact related notes fetch successfully.', 'True');
    }  

    function ContactRelatedFile() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'ContactID' => $ContactID
        ]);
 
        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->CTActionsModel->ContactRelatedFile($UserID, $ContactID, $OrganizationID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Contact related file fetch successfully.', 'True');
    } 
 
    function ContactRelatedEvent() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'ContactID' => $ContactID
        ]);
 
        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNullFromMultiDimArr($this->CTActionsModel->ContactRelatedEvent($UserID, $ContactID, $OrganizationID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Contact related tasks fetch successfully.', 'True');
    }   

    function ContactRelatedTask() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'ContactID' => $ContactID
        ]);
 
        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNullFromMultiDimArr($this->CTActionsModel->ContactRelatedTask($UserID, $ContactID, $OrganizationID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Contact related tasks fetch successfully.', 'True');
    }  

    function EditContact() { 
      
        API_Controller::varifyMethod("POST");

        extract($_POST);

        $ContactID = isset($ContactID)?$ContactID:NULL;

        API_Controller::requiredValidation([
            'ContactID' => $ContactID,
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'Salutation' => $Salutation,
            'Title' => $Title,
            'FirstName' => $FirstName,
            'LastName' => $LastName,
            'Account' => $Account,
            'AssignedTo' => $AssignedTo,
            'LeadSource' => $LeadSource,
            'MailingAddress' => $MailingAddress,
            'MailingCity' => $MailingCity,
            'MailingState' => $MailingState,
            'MailingPostalCode' => $MailingPostalCode,
            'MailingCountry' => $MailingCountry,
            'MailingLatitude' => $MailingLatitude,
            'MailingLongitude' => $MailingLongitude
        ]);

        API_Controller::checkUserAuthentication($UserID); 

        $result = $this->CTActionsModel->EditContact(); 

        return API_Controller::responseMessage(1, "Contact has been updated successfully.", "True");
    }  
}
