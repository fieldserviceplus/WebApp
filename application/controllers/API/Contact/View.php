<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class View extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/Contact/ViewModel');
    }
  
    function GetViews(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->ViewModel->GetViews($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Contact view fetch successfully.', 'True');
    }

    function ViewContactList(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'ContactViewID' => $ContactViewID
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = API_Controller::removeNullFromMultiDimArr($this->ViewModel->ViewContactList($UserID, $OrganizationID, $ContactViewID));

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Contacts fetch successfully.', 'True');
    }

    function ContactFilter() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'ContactViewID' => $ContactViewID,
            'FilterFields[]' => $FilterFields[0],
            'FilterConditions[]' => $FilterConditions[0],
            'FilterValues[]' => $FilterValues[0],
            'SortByField' => $SortByField,
            'SortByValue' => $SortByValue
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ViewModel->ContactFilter($UserID, $OrganizationID, $ContactViewID, $FilterFields, $FilterConditions, $FilterValues, $SortByField, $SortByValue);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Contact filter has been saved successfully.', 'True');
    } 

    function GetContactViewFields() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ViewModel->GetContactViewFields($UserID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Contact view fields fetch successfully.', 'True');
    } 
}
