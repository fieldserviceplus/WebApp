<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class WoActions extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/WorkOrder/WoActionsModel');
    }
  
    function CreateWorkOrder() { 
      
        API_Controller::varifyMethod("POST");

        $ParentWorkOrder = !empty($_POST['ParentWorkOrder'])?$_POST['ParentWorkOrder']:NULL;
        $PopUpReminder = !empty($_POST['PopUpReminder'])?$_POST['PopUpReminder']:"";
        $Signature = !empty($_POST['Signature'])?$_POST['Signature']:"";
 
        extract($_POST);
   
        API_Controller::requiredValidation([
            'OrganizationID' => $OrganizationID,
            'UserID' => $UserID,
            'AssignedTo' => $AssignedTo,
            'Account' => $Account,
            'Subject' => $Subject,
            'Description' => $Description,
            'Address' => $Address,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => $Latitude,
            'Longitude' => $Longitude,
            'WorkOrderType' => $WorkOrderType,
            'WOStatus' => $WOStatus,
            'WOPriority' => $WOPriority,
            'WOCategory' => $WOCategory,
            'PrimaryContact' => $PrimaryContact,
            'StartDate' => $StartDate,
            'EndDate' => $EndDate,
            'WOStartTime' => $WOStartTime,
            'WOEndTime' => $WOEndTime
        ]);

        if(isset($IsRecurring) && $IsRecurring == 1){
 
            $RepeatOn = !empty($_POST['RepeatOn'])?$_POST['RepeatOn']:NULL;
            $EndsOnDate = !empty($_POST['EndsOnDate'])?date("Y-m-d",strtotime($_POST['EndsOnDate'])):NULL;
            $EndsAfterOccurrences = !empty($_POST['EndsAfterOccurrences'])?$_POST['EndsAfterOccurrences']:NULL;
            API_Controller::requiredValidation([
                'StartOn' => $StartOn,
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'Ends' => $Ends,
                'StartTime' => $StartTime,
                'EndTime' => $EndTime
            ]);
        } else {
            $IsRecurring = 0;
            $StartOn = NULL;
            $RepeatEvery = NULL;
            $IntervalEvery = NULL;
            $Ends = NULL;
            $RepeatOn = NULL;
            $EndsOnDate = NULL;
            $EndsAfterOccurrences = NULL;
            $StartTime = NULL;
            $EndTime = NULL;  
        }
   
        API_Controller::checkUserAuthentication($UserID); 

        $data = $this->WoActionsModel->CreateWorkOrder($OrganizationID, $UserID, $ParentWorkOrder, $AssignedTo, $Account, $Subject, $Description, $Address, $City, $State, $Country, $PostalCode, $Latitude, $Longitude, $Signature, $PopUpReminder, $WorkOrderType, $WOStatus, $WOPriority, $WOCategory, $StartDate, $EndDate, $WOStartTime, $WOEndTime, $PrimaryContact, $IsRecurring, $StartOn, $RepeatEvery, $IntervalEvery, $Ends, $StartTime, $EndTime, $RepeatOn, $EndsOnDate, $EndsAfterOccurrences); 

        return API_Controller::successResponse($data, 1, "Work order has been created successfully.", "True");
    }

    function RecentMyWorkOrders() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
 
        $data['data'] = $this->WoActionsModel->RecentMyWorkOrders($UserID, $OrganizationID);  

        API_Controller::successResponse($data, 1, 'Recent accounts fetch successfully.', 'True');
    }
 
    function RecentAllWorkOrders() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
 
        $data['data'] = $this->WoActionsModel->RecentAllWorkOrders($UserID, $OrganizationID);  

        API_Controller::successResponse($data, 1, 'Recent accounts fetch successfully.', 'True');
    }  

    function RecentWorkOrders() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
 
        $data['data'] = $this->WoActionsModel->RecentWorkOrders($UserID, $OrganizationID);  

        API_Controller::successResponse($data, 1, 'Recent accounts fetch successfully.', 'True');
    } 

    function WorkOrderDetails() { 
                                                                        
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'WorkOrderID' => $WorkOrderID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNull($this->WoActionsModel->WorkOrderDetails($UserID, $WorkOrderID, $OrganizationID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Work order details fetch successfully.', 'True');
    } 

    function WorkOrderRelatedList() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'WorkOrderID' => $WorkOrderID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->WorkOrderRelatedList($UserID, $WorkOrderID, $OrganizationID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Work order related list fetch successfully.', 'True');
    } 

    function WorkOrderRelatedLineItem() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'WorkOrderID' => $WorkOrderID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->WorkOrderRelatedLineItem($UserID, $WorkOrderID, $OrganizationID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);  

        API_Controller::successResponse($data, 1, 'Work order related line item fetch successfully.', 'True');
    }   

    function WorkOrderRelatedChemical() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'WorkOrderID' => $WorkOrderID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->WorkOrderRelatedChemical($UserID, $WorkOrderID, $OrganizationID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);  

        API_Controller::successResponse($data, 1, 'Work order related chemical fetch successfully.', 'True');
    } 

    function WorkOrderRelatedInvoice() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'WorkOrderID' => $WorkOrderID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->WorkOrderRelatedInvoice($UserID, $WorkOrderID, $OrganizationID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);  

        API_Controller::successResponse($data, 1, 'Work order related invoice fetch successfully.', 'True');
    }   

    function WorkOrderRelatedEvent() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'WorkOrderID' => $WorkOrderID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = API_Controller::removeNullFromMultiDimArr($this->WoActionsModel->WorkOrderRelatedEvent($UserID, $WorkOrderID, $OrganizationID));

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Work order related event fetch successfully.', 'True');
    }  

    function WorkOrderRelatedNote() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'WorkOrderID' => $WorkOrderID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->WorkOrderRelatedNote($UserID, $WorkOrderID, $OrganizationID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Work order related event fetch successfully.', 'True');
    }  

    function WorkOrderRelatedFile() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'WorkOrderID' => $WorkOrderID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->WorkOrderRelatedFile($UserID, $WorkOrderID, $OrganizationID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'Work order related file fetch successfully.', 'True');
    }  

    function WorkOrderRelatedTask() { 
       
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'WorkOrderID' => $WorkOrderID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->WorkOrderRelatedTask($UserID, $WorkOrderID, $OrganizationID);

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'Work order related task fetch successfully.', 'True');
    }

    function GetProducts() { 
       
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'WorkOrderID' => $WorkOrderID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->GetProducts($UserID, $OrganizationID);

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'Products fetch successfully.', 'True');
    }

    function GetChemicals() { 
       
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->GetChemicals($UserID, $OrganizationID);

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'Chemicals fetch successfully.', 'True');
    }

    function EditWorkOrder() { 
      
        API_Controller::varifyMethod("POST");

        $WORecurrenceID = !empty($_POST['WORecurrenceID'])?$_POST['WORecurrenceID']:NULL;
        $ParentWorkOrder = !empty($_POST['ParentWorkOrder'])?$_POST['ParentWorkOrder']:NULL;
        $PopUpReminder = !empty($_POST['PopUpReminder'])?$_POST['PopUpReminder']:"";
        $Signature = !empty($_POST['Signature'])?$_POST['Signature']:"";
  
        extract($_POST);
 
        API_Controller::requiredValidation([
            'OrganizationID' => $OrganizationID,
            'WorkOrderID' => $WorkOrderID,
            'UserID' => $UserID,
            'Account' => $Account,
            'AssignedTo' => $AssignedTo,
            'Subject' => $Subject,
            'Description' => $Description,
            'Address' => $Address,
            'City' => $City,
            'State' => $State,
            'Country' => $Country,
            'PostalCode' => $PostalCode,
            'Latitude' => $Latitude,
            'Longitude' => $Longitude,
            'WorkOrderType' => $WorkOrderType,
            'WOStatus' => $WOStatus,
            'WOPriority' => $WOPriority,
            'PrimaryContact' => $PrimaryContact,
            'WOCategory' => $WOCategory,
        ]);

        if(isset($IsRecurring) && $IsRecurring == 1){

            $RepeatOn = !empty($_POST['RepeatOn'])?$_POST['RepeatOn']:NULL;
            $EndsOnDate = !empty($_POST['EndsOnDate'])?date("Y-m-d",strtotime($_POST['EndsOnDate'])):NULL;
            $EndsAfterOccurrences = !empty($_POST['EndsAfterOccurrences'])?$_POST['EndsAfterOccurrences']:NULL; 
            API_Controller::requiredValidation([
                'StartOn' => $StartOn,
                'RepeatEvery' => $RepeatEvery,
                'IntervalEvery' => $IntervalEvery,
                'Ends' => $Ends,
                'StartTime' => $StartTime,
                'EndTime' => $EndTime
            ]);
        } else {
            $IsRecurring = 0;
            $StartOn = NULL;
            $RepeatEvery = NULL;
            $IntervalEvery = NULL;
            $Ends = NULL;
            $RepeatOn = NULL;
            $EndsOnDate = NULL;
            $EndsAfterOccurrences = NULL;
            $StartTime = NULL;
            $EndTime = NULL;  
        }
   
        API_Controller::checkUserAuthentication($UserID); 

        $result = $this->WoActionsModel->EditWorkOrder($OrganizationID, $WorkOrderID, $ParentWorkOrder, $UserID, $Account, $AssignedTo, $Subject, $Description, $Address, $City, $State, $Country, $PostalCode, $Latitude, $Longitude, $PopUpReminder, $WorkOrderType, $WOStatus, $WOPriority, $PrimaryContact, $WOCategory, $Signature, $IsRecurring, $StartOn, $RepeatEvery, $IntervalEvery, $Ends, $StartTime, $EndTime, $RepeatOn, $EndsOnDate, $EndsAfterOccurrences, $WORecurrenceID); 

        return API_Controller::responseMessage(1, "Work order has been updated successfully.", "True");
    }  

    function SaveWorkOrderSignature() { 
       
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'OrganizationID' => $OrganizationID,
            'UserID' => $UserID,
            'WorkOrderID' => $WorkOrderID,
            'Signature' => $Signature
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->SaveWorkOrderSignature($UserID, $WorkOrderID, $Signature, $OrganizationID);

        $data['data'] = $result;   

        API_Controller::responseMessage(1, 'Work order signature save successfully.', 'True');
    }

    function SaveWorkorderLineItems() { 
       
        API_Controller::varifyMethod("POST");
  
        extract($_POST);
 
        API_Controller::requiredValidation([
            'OrganizationID' => $OrganizationID,
            'UserID' => $UserID,
            'WorkOrderID' => $WorkOrderID,
           /* 'Product[]' => $Product,
            'ListPrice[]' => $ListPrice,
            'Discount[]' => $Discount,
            'UnitPrice[]' => $UnitPrice,
            'Quantity[]' => $Quantity,
            'SubTotal[]' => $SubTotal,
            'Taxable[]' => $Taxable,
            'NetTotal[]' => $NetTotal*/
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->SaveWorkorderLineItems(); 

        if($result)
            API_Controller::responseMessage(1, 'Work order line items save successfully.', 'True');
        else 
            API_Controller::responseMessage(0, 'Something went wrong.', 'False');
    }

    function GetProductFamily() { 
       
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->GetProductFamily($OrganizationID);

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'Product family fetch successfully.', 'True');
    }

    function GetUnitOfMeasurement() { 
       
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->GetUnitOfMeasurement($OrganizationID);

        $data['data'] = $result;   

        API_Controller::successResponse($data, 1, 'Unit of measurement fetch successfully.', 'True');
    }

    function SaveWorkorderChemicals() { 
       
        API_Controller::varifyMethod("POST");
  
        extract($_POST);
 
        API_Controller::requiredValidation([
            'OrganizationID' => $OrganizationID,
            'UserID' => $UserID,
            'WorkOrderID' => $WorkOrderID,
            'Account' => $Account,
            /*'Product[]' => $Product,
            'TestConcentration[]' => $TestConcentration,
            'TestedUnitOfMeasure[]' => $TestedUnitOfMeasure,
            'ApplicationAmount[]' => $ApplicationAmount,
            'ApplicationUnitOfMeasure[]' => $ApplicationUnitOfMeasure,
            'ApplicationArea[]' => $ApplicationArea,
            'AdditionalNotes[]' => $AdditionalNotes*/
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->SaveWorkorderChemicals();

        $data['data'] = $result;   

        API_Controller::responseMessage(1, 'Work order chemical products save successfully.', 'True');
    }

    function ConvertToInvoice() { 
                                                                        
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
            'WorkOrderID' => $WorkOrderID
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->WoActionsModel->ConvertToInvoice();

        $data['InvoiceID'] = $result;  

        API_Controller::successResponse($data, 1, 'Invoice converted successfully.', 'True');
    }  
}
