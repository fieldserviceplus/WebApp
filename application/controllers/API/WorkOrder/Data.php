<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class Data extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/WorkOrder/DataModel');
    }
  
    function GetWorkOrderTypes(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->DataModel->GetWorkOrderTypes($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Work Order type fetch successfully.', 'True');
    }

    function GetWorkOrderPriorities(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->DataModel->GetWorkOrderPriorities($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Work Order prioritie fetch successfully.', 'True');
    }

    function GetWorkOrderStatus(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->DataModel->GetWorkOrderStatus($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Work Order status fetch successfully.', 'True');
    }

    function GetWorkOrderCategories(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->DataModel->GetWorkOrderCategories($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Work order category fetch successfully.', 'True');
    }

    function GetParentWorkOrders(){
 
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->DataModel->GetParentWorkOrders($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'Work parent work order fetch successfully.', 'True');
    }
}
