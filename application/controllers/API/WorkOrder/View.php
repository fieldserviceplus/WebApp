<?php

require_once(APPPATH.'controllers/API/API_Controller.php');

header('Content-type: application/json; charset=utf-8');

class View extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API/WorkOrder/ViewModel');
    }
  
    function GetViews(){

        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);

        $result = $this->ViewModel->GetViews($UserID, $OrganizationID);

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'WorkOrder view fetch successfully.', 'True');
    }

    function ViewWorkOrderList(){
 
        API_Controller::varifyMethod("POST");
 
        extract($_POST);

        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'WorkOrderViewID' => $WorkOrderViewID,
            'OrganizationID' => $OrganizationID,
        ]);
 
        API_Controller::checkUserAuthentication($UserID);
 
        $result = API_Controller::removeNullFromMultiDimArr($this->ViewModel->ViewWorkOrderList($UserID, $WorkOrderViewID, $OrganizationID));

        $data['data'] = $result;

        API_Controller::successResponse($data, 1, 'WorkOrders fetch successfully.', 'True');
    }

    function WorkOrderFilter() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'OrganizationID' => $OrganizationID,
            'UserID' => $UserID,
            'WorkOrderViewID' => $WorkOrderViewID,
            'FilterFields[]' => $FilterFields[0],
            'FilterConditions[]' => $FilterConditions[0],
            'FilterValues[]' => $FilterValues[0],
            'SortByField' => $SortByField,
            'SortByValue' => $SortByValue
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ViewModel->WorkOrderFilter($UserID, $WorkOrderViewID, $FilterFields, $FilterConditions, $FilterValues, $SortByField, $SortByValue, $OrganizationID);

        $data['data'] = API_Controller::removeNullFromMultiDimArr($result);  

        API_Controller::successResponse($data, 1, 'WorkOrder filter has been saved successfully.', 'True');
    } 

    function GetWorkOrderViewFields() { 
      
        API_Controller::varifyMethod("POST");
 
        extract($_POST);
 
        API_Controller::requiredValidation([
            'UserID' => $UserID,
            'OrganizationID' => $OrganizationID,
        ]);

        API_Controller::checkUserAuthentication($UserID); 
        
        $result = $this->ViewModel->GetWorkOrderViewFields($UserID, $OrganizationID);

        $data['data'] = $result;  

        API_Controller::successResponse($data, 1, 'WorkOrder view fields fetch successfully.', 'True');
    } 
}
