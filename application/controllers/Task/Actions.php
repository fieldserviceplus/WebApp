<?php
 
class Actions extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Task/ActionsModel');
    }
   
    function index() { 
        $this->load->view('Task/TaskHome');
    }

    function RecentTasks() { 
        echo $this->ActionsModel->RecentTasks();
    }
  
    function CreateTask($TaskID = 0) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
 
        if(isset($_POST['CreateTask'])){
 
            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('RelatedTo', 'related to', 'required');
            $this->form_validation->set_rules('What', 'what', 'required');
            $this->form_validation->set_rules('AssignedTo', 'assigned to', 'required');
            /*$this->form_validation->set_rules('Who', 'who', 'required');*/
            $this->form_validation->set_rules('TaskStatus', 'task status', 'required');
            $this->form_validation->set_rules('TaskPriority', 'task priority', 'required');
            $this->form_validation->set_rules('TaskType', 'task type', 'required'); 
            $this->form_validation->set_rules('DueDate', 'date', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {  
                $TaskID = $this->ActionsModel->CreateTask();    
                if(!empty($TaskID)){

                    $this->session->set_flashdata('SuccessMsg', 'Task created successfully.');
                    redirect('Task/Actions/TaskDetails/'.$TaskID);

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Task/Actions/CreateTask');
                }
            } else {
                $this->load->view('Task/CreateTask');
            }

        } else { 

            $data =  $this->ActionsModel->getTaskData($TaskID); 
            $this->load->view('Task/CreateTask', $data);
        }
    }

    function TaskDetails($TaskID) {

        $data =  $this->ActionsModel->getTaskData($TaskID); 
        $this->load->view('Task/TaskDetails', $data);
    }

    function RelatedObject($TaskID, $RelatedObj) {

        $RelatedObjData =  $this->ActionsModel->getTaskData($TaskID, $RelatedObj); 
        $data['RelatedObjData'] = $RelatedObjData;
        $data['Data'] = $RelatedObjData['TaskData'];
        $data['RelatedObj'] = $RelatedObj;
        $this->load->view('Common/ObjectRelatedList/'.$RelatedObj, $data);
    }

    function EditTask($TaskID) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data =  $this->ActionsModel->getTaskData($TaskID); 

        if(isset($_POST['EditTask'])){

            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('RelatedTo', 'related to', 'required');
            $this->form_validation->set_rules('What', 'what', 'required');
            $this->form_validation->set_rules('AssignedTo', 'assigned to', 'required');
            /*$this->form_validation->set_rules('Who', 'who', 'required');*/
            $this->form_validation->set_rules('TaskStatus', 'task status', 'required');
            $this->form_validation->set_rules('TaskPriority', 'task priority', 'required');
            $this->form_validation->set_rules('TaskType', 'task type', 'required'); 
            $this->form_validation->set_rules('DueDate', 'date', 'required');
             
            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->ActionsModel->EditTask($TaskID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Task updated successfully.');
                    redirect('Task/Actions/TaskDetails/'.$TaskID); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Task/Actions/EditTask/'.$TaskID); 
                }
            } else { 
                $this->load->view('Task/EditTask', $data); 
            }

        } else { 
           
            $this->load->view('Task/EditTask', $data);
        }
    }

    function DeleteTask($TaskID){
        $result =  $this->ActionsModel->DeleteTask($TaskID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Task deleted successfully.');
            redirect('Task/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Task/Actions/TaskDetails/'.$TaskID); 
        }
    }

    /*function CreateNote($TaskID)
    {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        if(isset($_POST['CreateNote'])){

            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('Body', 'body', 'required');
            if ($this->form_validation->run() == TRUE)
            { 
                $result = $this->ActionsModel->CreateNote($TaskID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Note saved successfully.');
                    redirect('Task/Actions/TaskDetails/'.$TaskID);

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Task/Actions/TaskDetails/'.$TaskID);
                }
            }
        }
    }*/

    /*function GetRelatedToObjectData()
    {
        echo $this->ActionsModel->GetRelatedToObjectData();
    }*/

    function GetRelatedToObjectOptions($RelatedTo, $ObjectID = 0)
    {
        echo MY_Model::getRelatedToObjectOptions($RelatedTo, $ObjectID);
    }

    function SaveRecurrenceSetting(){
        echo $this->ActionsModel->SaveRecurrenceSetting();
    }
}
