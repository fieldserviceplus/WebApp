<?php

class View extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Task/ViewModel');
    }
   
    function index($TaskViewID = 0) {

        if(isset($TaskViewID) && $TaskViewID != 0)
            $data['SelectedTask'] = $TaskViewID;
        else 
            $data['SelectedTask'] = isset($_POST['SelectedTask'])?$_POST['SelectedTask']:"";

        if($data['SelectedTask'] == 'MyOpenTasksToday' || $data['SelectedTask'] == 'MyOpenTasksThisWeek' || $data['SelectedTask'] == 'AllMyOpenTasks' || $data['SelectedTask'] == 'MyCompletedTasksThisWeek' || $data['SelectedTask'] == ""){
            $this->load->view('Task/TaskView', $data);
        }
        else 
        {
            $data['TaskCustomViewData'] = $this->ViewModel->GetCustomView($data['SelectedTask']);
            $data['TaskData'] = $this->ViewModel->GetTaskViewData($data['SelectedTask']);
           
            $this->load->view('Task/TaskCustomView', $data);
        }
    }

    function ViewTasks() { 
        echo $this->ViewModel->ViewTasks();
    }

    /*function ViewResult()
    {
         if(isset($_POST['FilterForm'])){

            extract($_POST);
            $result = $this->ViewModel->SaveTaskFilter($FilterFields, $FilterConditions, $FilterValues); 
            $data['FilteredTasks'] = $result; 
            $this->load->view('Task/TaskViewResult', $data);

         } else { 
            $this->load->view('Task/TaskViewResult');
        }
    }*/

    function CreateNewView() { 

        $data['TaskViewFields'] = $this->ViewModel->TaskViewFields(); 
        if(isset($_POST['CreateNewView'])){

            $TaskViewID = $this->ViewModel->CreateNewView(); 

            if($TaskViewID){

                $this->session->set_flashdata('SuccessMsg', 'Task new view created successfully.');
                redirect('Task/View/Index/'.$TaskViewID);

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Task/View/CreateNewView', $data);
            }
        } else { 
            $this->load->view('Task/CreateNewView', $data);
        }
    }

    function EditCustomView($TaskViewID) { 

        $data = $this->ViewModel->GetCustomView($TaskViewID);

        if(isset($_POST['EditCustomView'])){

            $result = $this->ViewModel->EditCustomView($TaskViewID); 

            if($result){

                $this->session->set_flashdata('SuccessMsg', 'Task view updated successfully.');
                redirect('Task/Actions');

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Task/View/EditCustomView', $data);
            }
        } else { 
            $this->load->view('Task/EditCustomView', $data);
        }
    }

    function DeleteCustomView($TaskViewID) { 
        $result =  $this->ViewModel->DeleteCustomView($TaskViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Task view deleted successfully.');
            redirect('Task/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Task/View/Index/'.$TaskViewID);
        }
    }

    function CopyCustomView($TaskViewID)
    { 
        $result =  $this->ViewModel->CopyCustomView($TaskViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Task view copy successfully.');
            redirect('Task/View/Index/'.$TaskViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Task/View/Index/'.$TaskViewID);
        }
    }

    function RenameCustomView($TaskViewID){

        $result =  $this->ViewModel->RenameCustomView($TaskViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Task view rename successfully.');
            redirect('Task/View/Index/'.$TaskViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Task/View/Index/'.$TaskViewID);

        }
    }

    function EditSharingCustomView($TaskViewID){

        $result =  $this->ViewModel->EditSharingCustomView($TaskViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Task view sharing updated successfully.');
            redirect('Task/View/Index/'.$TaskViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Task/View/Index/'.$TaskViewID);

        }
    }
  
    function EditFiltersCustomView($TaskViewID)
    {
        $result =  $this->ViewModel->EditFiltersCustomView($TaskViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Task view filters updated successfully.');
            redirect('Task/View/Index/'.$TaskViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Task/View/Index/'.$TaskViewID);

        }
    }

    function EditDisplayedColumnsCustomView($TaskViewID)
    {
        $result =  $this->ViewModel->EditDisplayedColumnsCustomView($TaskViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Task view displayed columns updated successfully.');
            redirect('Task/View/Index/'.$TaskViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Task/View/Index/'.$TaskViewID);

        }
    }
}
