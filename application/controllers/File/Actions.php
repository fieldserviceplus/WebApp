<?php
 
class Actions extends MY_Controller {
 
    public $LoginUserID, $LoginOrganizationID;
    function __construct() {
        parent::__construct();
        $this->load->model('File/ActionsModel');

        $this->LoginUserID = $this->session->userdata('UserID');
        $this->LoginOrganizationID = $this->session->userdata('OrganizationID');
    }
   
    function index() { 
        $this->load->view('File/FileHome');
    }

    function RecentFiles() { 
        echo $this->ActionsModel->RecentFiles();
    }
  
    function CreateFile($FileID = 0) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
 
        if(isset($_POST['CreateFile'])){
 
            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('RelatedTo', 'related to', 'required');
            $this->form_validation->set_rules('What', 'what', 'required');
            $this->form_validation->set_rules('AssignedTo', 'assigned to', 'required');
            $this->form_validation->set_rules('FileName', 'file name', 'required');
            $this->form_validation->set_rules('FileSize', 'file size', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {  
                $FileID = $this->ActionsModel->CreateFile();    
                if(!empty($FileID)){

                    $this->session->set_flashdata('SuccessMsg', 'File created successfully.');
                    redirect('File/Actions/FileDetails/'.$FileID);

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('File/Actions/CreateFile');
                }
            } else {
                $this->load->view('File/CreateFile');
            }

        } else { 

            $data =  $this->ActionsModel->getFileData($FileID); 
            $this->load->view('File/CreateFile', $data);
        }
    }

    function FileDetails($FileID) {

        $data =  $this->ActionsModel->getFileData($FileID); 
        $this->load->view('File/FileDetails', $data);
    }

    function EditFile($FileID) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data =  $this->ActionsModel->getFileData($FileID); 

        if(isset($_POST['EditFile'])){

            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('RelatedTo', 'related to', 'required');
            $this->form_validation->set_rules('What', 'what', 'required');
            $this->form_validation->set_rules('AssignedTo', 'assigned to', 'required');
            $this->form_validation->set_rules('FileName', 'file name', 'required');
            $this->form_validation->set_rules('FileSize', 'file size', 'required');
             
            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->ActionsModel->EditFile($FileID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'File updated successfully.');
                    redirect('File/Actions/FileDetails/'.$FileID); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('File/Actions/EditFile/'.$FileID); 
                }
            } else { 
                $this->load->view('File/EditFile', $data); 
            }

        } else { 
           
            $this->load->view('File/EditFile', $data);
        }
    }

    function DeleteFile($FileID){
        $result =  $this->ActionsModel->DeleteFile($FileID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'File deleted successfully.');
            redirect('File/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('File/Actions/FileDetails/'.$FileID); 
        }
    }

    function CreateNote($FileID)
    {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        if(isset($_POST['CreateNote'])){

            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('Body', 'body', 'required');
            if ($this->form_validation->run() == TRUE)
            { 
                $result = $this->ActionsModel->CreateNote($FileID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Note saved successfully.');
                    redirect('File/Actions/FileDetails/'.$FileID);

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('File/Actions/FileDetails/'.$FileID);
                }
            }
        }
    }

    /*function GetRelatedToObjectData()
    {
        echo $this->ActionsModel->GetRelatedToObjectData();
    }*/
    function GetRelatedToObjectOptions($RelatedTo, $ObjectID = 0)
    {
        echo MY_Model::getRelatedToObjectOptions($RelatedTo, $ObjectID);
    }

    function DownloadFile($FileID) { 

        $FILE_PATH = str_replace('{org_id}', $this->LoginOrganizationID, FILE_PATH);

        $file =  $this->ActionsModel->getFileData($FileID);
        $this->load->helper('download');
        $data = file_get_contents($FILE_PATH.$file['FileData']->FileName);
        force_download($file['FileData']->FileName, $data);
    }

    function OpenFile($FileID) { 

        $FILE_PATH = str_replace('{org_id}', $this->LoginOrganizationID, FILE_PATH);
        
        $file =  $this->ActionsModel->getFileData($FileID);
        $file_ext = pathinfo($file['FileData']->FileName, PATHINFO_EXTENSION);
        if($file_ext == 'txt'){
            $this->output
               ->set_content_type('text/plain')
               ->set_output(file_get_contents($FILE_PATH.$file['FileData']->FileName));
        } else if($file_ext == 'pdf') {
             $this->output
               ->set_content_type('application/pdf')
               ->set_output(file_get_contents($FILE_PATH.$file['FileData']->FileName));
        } else if($file_ext == 'jpg' || $file_ext == 'jpeg') {
             $this->output
               ->set_content_type('image/jpeg')
               ->set_output(file_get_contents($FILE_PATH.$file['FileData']->FileName));
        } else if($file_ext == 'png') {
             $this->output
               ->set_content_type('image/png')
               ->set_output(file_get_contents($FILE_PATH.$file['FileData']->FileName));
        } else {
            $this->load->helper('download');
            $data = file_get_contents($FILE_PATH.$file['FileData']->FileName);
            force_download($file['FileData']->FileName, $data);
            
        }
       
    }
}
