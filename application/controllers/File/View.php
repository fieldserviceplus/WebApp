<?php

class View extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('File/ViewModel');
    }
   
    function index($FileViewID = 0) {

        if(isset($FileViewID) && $FileViewID != 0)
            $data['SelectedFile'] = $FileViewID;
        else 
            $data['SelectedFile'] = isset($_POST['SelectedFile'])?$_POST['SelectedFile']:"";

        if($data['SelectedFile'] == 'MyFiles' || $data['SelectedFile'] == 'AllFiles' || $data['SelectedFile'] == 'NewFilesCreatedThisWeek' || $data['SelectedFile'] == ""){
            $this->load->view('File/FileView', $data);
        }
        else 
        {
            $data['FileCustomViewData'] = $this->ViewModel->GetCustomView($data['SelectedFile']);
            $data['FileData'] = $this->ViewModel->GetFileViewData($data['SelectedFile']);
           
            $this->load->view('File/FileCustomView', $data);
        }
    }

    function ViewFiles() { 
        echo $this->ViewModel->ViewFiles();
    }

    /*function ViewResult()
    {
         if(isset($_POST['FilterForm'])){

            extract($_POST);
            $result = $this->ViewModel->SaveFileFilter($FilterFields, $FilterConditions, $FilterValues); 
            $data['FilteredFiles'] = $result; 
            $this->load->view('File/FileViewResult', $data);

         } else { 
            $this->load->view('File/FileViewResult');
        }
    }*/

    function CreateNewView() { 

        $data['FileViewFields'] = $this->ViewModel->FileViewFields(); 
        if(isset($_POST['CreateNewView'])){

            $FileViewID = $this->ViewModel->CreateNewView(); 

            if($FileViewID){

                $this->session->set_flashdata('SuccessMsg', 'File new view created successfully.');
                redirect('File/View/Index/'.$FileViewID);

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('File/View/CreateNewView', $data);
            }
        } else { 
            $this->load->view('File/CreateNewView', $data);
        }
    }

    function EditCustomView($FileViewID) { 

        $data = $this->ViewModel->GetCustomView($FileViewID);

        if(isset($_POST['EditCustomView'])){

            $result = $this->ViewModel->EditCustomView($FileViewID); 

            if($result){

                $this->session->set_flashdata('SuccessMsg', 'File view updated successfully.');
                redirect('File/Actions');

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('File/View/EditCustomView', $data);
            }
        } else { 
            $this->load->view('File/EditCustomView', $data);
        }
    }

    function DeleteCustomView($FileViewID) { 
        $result =  $this->ViewModel->DeleteCustomView($FileViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'File view deleted successfully.');
            redirect('File/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('File/View/Index/'.$FileViewID);
        }
    }

    function CopyCustomView($FileViewID)
    { 
        $result =  $this->ViewModel->CopyCustomView($FileViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'File view copy successfully.');
            redirect('File/View/Index/'.$FileViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('File/View/Index/'.$FileViewID);
        }
    }

    function RenameCustomView($FileViewID){

        $result =  $this->ViewModel->RenameCustomView($FileViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'File view rename successfully.');
            redirect('File/View/Index/'.$FileViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('File/View/Index/'.$FileViewID);

        }
    }

    function EditSharingCustomView($FileViewID){

        $result =  $this->ViewModel->EditSharingCustomView($FileViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'File view sharing updated successfully.');
            redirect('File/View/Index/'.$FileViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('File/View/Index/'.$FileViewID);

        }
    }
  
    function EditFiltersCustomView($FileViewID)
    {
        $result =  $this->ViewModel->EditFiltersCustomView($FileViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'File view filters updated successfully.');
            redirect('File/View/Index/'.$FileViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('File/View/Index/'.$FileViewID);

        }
    }

    function EditDisplayedColumnsCustomView($FileViewID)
    {
        $result =  $this->ViewModel->EditDisplayedColumnsCustomView($FileViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'File view displayed columns updated successfully.');
            redirect('File/View/Index/'.$FileViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('File/View/Index/'.$FileViewID);

        }
    }
}
