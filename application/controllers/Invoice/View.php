<?php

class View extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Invoice/ViewModel');
    }
   
    function index($InvoiceViewID = 0) {

        if(isset($InvoiceViewID) && $InvoiceViewID != 0)
            $data['SelectedInvoice'] = $InvoiceViewID;
        else 
            $data['SelectedInvoice'] = isset($_POST['SelectedInvoice'])?$_POST['SelectedInvoice']:"";

        if($data['SelectedInvoice'] == 'MyOpenInvoices' 
            || $data['SelectedInvoice'] == 'AllOpenInvoices' 
            || $data['SelectedInvoice'] == 'AllOpenInvoicesPastDue' 
            || $data['SelectedInvoice'] == 'InvoicesCreatedThisWeek'
            || $data['SelectedInvoice'] == ""){
           
            $this->load->view('Invoice/InvoiceView', $data);
        }
        else 
        {
            
            $data['InvoiceCustomViewData'] = $this->ViewModel->GetCustomView($data['SelectedInvoice']);
            $data['InvoiceData'] = $this->ViewModel->GetInvoiceViewData($data['SelectedInvoice']);
           
            $this->load->view('Invoice/InvoiceCustomView', $data);
        }
    }

    function ViewInvoices() { 
        echo $this->ViewModel->ViewInvoices();
    }

    /*function ViewResult()
    {
         if(isset($_POST['FilterForm'])){

            extract($_POST);
            $result = $this->ViewModel->SaveInvoiceFilter($FilterFields, $FilterConditions, $FilterValues); 
            $data['FilteredInvoices'] = $result; 
            $this->load->view('Invoice/InvoiceViewResult', $data);

         } else { 
            $this->load->view('Invoice/InvoiceViewResult');
        }
    }*/

    function CreateNewView() { 

        $data['InvoiceViewFields'] = $this->ViewModel->InvoiceViewFields(); 
        if(isset($_POST['CreateNewView'])){

            $InvoiceViewID = $this->ViewModel->CreateNewView(); 

            if($InvoiceViewID){

                $this->session->set_flashdata('SuccessMsg', 'Invoice new view created successfully.');
                redirect('Invoice/View/Index/'.$InvoiceViewID);

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Invoice/View/CreateNewView', $data);
            }
        } else { 
            $this->load->view('Invoice/CreateNewView', $data);
        }
    }

    function EditCustomView($InvoiceViewID) { 

        $data = $this->ViewModel->GetCustomView($InvoiceViewID);

        if(isset($_POST['EditCustomView'])){

            $result = $this->ViewModel->EditCustomView($InvoiceViewID); 

            if($result){

                $this->session->set_flashdata('SuccessMsg', 'Invoice view updated successfully.');
                redirect('Invoice/Actions');

            } else {

                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Invoice/View/EditCustomView', $data);
            }
        } else { 
            $this->load->view('Invoice/EditCustomView', $data);
        }
    }

    function DeleteCustomView($InvoiceViewID) { 
        $result =  $this->ViewModel->DeleteCustomView($InvoiceViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Invoice view deleted successfully.');
            redirect('Invoice/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Invoice/View/Index/'.$InvoiceViewID);
        }
    }

    function CopyCustomView($InvoiceViewID, $InvoiceViewName)
    { 
        $result =  $this->ViewModel->CopyCustomView($InvoiceViewID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Invoice view copy successfully.');
            redirect('Invoice/View/Index/'.$InvoiceViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Invoice/View/Index/'.$InvoiceViewID);
        }
    }

    function RenameCustomView($InvoiceViewID){

        $result =  $this->ViewModel->RenameCustomView($InvoiceViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Invoice view rename successfully.');
            redirect('Invoice/View/Index/'.$InvoiceViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Invoice/View/Index/'.$InvoiceViewID);

        }
    }

    function EditSharingCustomView($InvoiceViewID){

        $result =  $this->ViewModel->EditSharingCustomView($InvoiceViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Invoice view sharing updated successfully.');
            redirect('Invoice/View/Index/'.$InvoiceViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Invoice/View/Index/'.$InvoiceViewID);

        }
    }
  
    function EditFiltersCustomView($InvoiceViewID)
    {
        $result =  $this->ViewModel->EditFiltersCustomView($InvoiceViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Invoice view filters updated successfully.');
            redirect('Invoice/View/Index/'.$InvoiceViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Invoice/View/Index/'.$InvoiceViewID);

        }
    }

    function EditDisplayedColumnsCustomView($InvoiceViewID)
    {
        $result =  $this->ViewModel->EditDisplayedColumnsCustomView($InvoiceViewID);
        if($result){
            $this->session->set_flashdata('SuccessMsg', 'Invoice view displayed columns updated successfully.');
            redirect('Invoice/View/Index/'.$InvoiceViewID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Invoice/View/Index/'.$InvoiceViewID);

        }
    }
}
