<?php
class Actions extends MY_Controller {
 
    function __construct() {
        parent::__construct();
        $this->load->model('Invoice/ActionsModel');
    }
   
    function index() { 
        $this->load->view('Invoice/InvoiceHome');
    }

    function RecentInvoices() { 
        echo $this->ActionsModel->RecentInvoices();
    }
  
    function CreateInvoice($InvoiceID = 0) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');
 
        if(isset($_POST['CreateInvoice'])){

            $this->form_validation->set_rules('AssignedTo', 'assigned to', 'required');
            $this->form_validation->set_rules('WorkOrder', 'work order', 'required');
            $this->form_validation->set_rules('Account', 'account name', 'required');
            $this->form_validation->set_rules('Description', 'description', 'required'); 
            $this->form_validation->set_rules('Address', 'address', 'required');  
            $this->form_validation->set_rules('City', 'city', 'required');
            $this->form_validation->set_rules('State', 'state', 'required');
            $this->form_validation->set_rules('Country', 'country', 'required');
            $this->form_validation->set_rules('PostalCode', 'postal code', 'required');
            $this->form_validation->set_rules('InvoiceStatus', 'status', 'required');  
            $this->form_validation->set_rules('InvoiceDate', 'invoice date', 'required');  
            $this->form_validation->set_rules('DueDate', 'due date', 'required');
            $this->form_validation->set_rules('PaymentTerms', 'payment terms', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {   
                $InvoiceID = $this->ActionsModel->CreateInvoice();    
                if(!empty($InvoiceID)){ 

                    /*if(isset($_POST['Redirect']) && $_POST['Redirect'] = 'Account'){
                        $this->session->set_flashdata('SuccessMsg', 'Invoice created successfully.');
                        redirect('Account/Actions/AccountDetails/'.$_POST['Account']);
                    } else*/ {
                        $this->session->set_flashdata('SuccessMsg', 'Invoice created successfully.');
                        //redirect('Invoice/Actions');
                        redirect('Invoice/Actions/InvoiceDetails/'.$InvoiceID); 
                    }
                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Invoice/Actions/CreateInvoice');
                }
            } else {
                $this->load->view('Invoice/CreateInvoice');
            }

        } else { 

            $data =  $this->ActionsModel->getInvoiceData($InvoiceID);
            $this->load->view('Invoice/CreateInvoice', $data);
        }
    }

    function EditInvoice($InvoiceID) { 

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data =  $this->ActionsModel->getInvoiceData($InvoiceID); 

        if(isset($_POST['EditInvoice'])){

            $this->form_validation->set_rules('AssignedTo', 'assigned to', 'required');
            $this->form_validation->set_rules('WorkOrder', 'work order', 'required');
            $this->form_validation->set_rules('Account', 'account name', 'required');
            $this->form_validation->set_rules('Description', 'description', 'required'); 
            $this->form_validation->set_rules('Address', 'address', 'required');  
            $this->form_validation->set_rules('City', 'city', 'required');
            $this->form_validation->set_rules('State', 'state', 'required');
            $this->form_validation->set_rules('Country', 'country', 'required');
            $this->form_validation->set_rules('PostalCode', 'postal code', 'required');
            $this->form_validation->set_rules('InvoiceStatus', 'status', 'required');  
            $this->form_validation->set_rules('InvoiceDate', 'invoice date', 'required');  
            $this->form_validation->set_rules('DueDate', 'due date', 'required');
            $this->form_validation->set_rules('PaymentTerms', 'payment terms', 'required');

            if ($this->form_validation->run() == TRUE)
            {  
                $result = $this->ActionsModel->EditInvoice($InvoiceID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Invoice updated successfully.');
                    redirect('Invoice/Actions/InvoiceDetails/'.$InvoiceID); 

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Invoice/Actions/EditInvoice/'.$InvoiceID); 
                }
            } else { 
                $this->load->view('Invoice/EditInvoice', $data); 
            }

        } else { 
          
            $this->load->view('Invoice/EditInvoice', $data);
        }
    }

    function RelatedObject($InvoiceID, $RelatedObj) {

        $RelatedObjData =  $this->ActionsModel->getInvoiceData($InvoiceID, $RelatedObj); 
        $data['RelatedObjData'] = $RelatedObjData;
        $data['Data'] = $RelatedObjData['InvoiceData'];
        $data['RelatedObj'] = $RelatedObj;
        $this->load->view('Common/ObjectRelatedList/'.$RelatedObj, $data);
    }

    function CreateNote($InvoiceID)
    {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        if(isset($_POST['CreateNote'])){

            $this->form_validation->set_rules('Subject', 'subject', 'required');
            $this->form_validation->set_rules('Body', 'body', 'required');
            if ($this->form_validation->run() == TRUE)
            { 
                $result = $this->ActionsModel->CreateNote($InvoiceID);    
                if($result){

                    $this->session->set_flashdata('SuccessMsg', 'Note saved successfully.');
                    redirect('Invoice/Actions/InvoiceDetails/'.$InvoiceID);

                } else {

                    $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                    redirect('Invoice/Actions/InvoiceDetails/'.$InvoiceID);
                }
            }
        }
    }

    function InvoiceDetails($InvoiceID) {

        $data =  $this->ActionsModel->getInvoiceData($InvoiceID); 
        $this->load->view('Invoice/InvoiceDetails', $data);
    }

    function InvoiceLineItem($InvoiceID)
    {
        $result =  $this->ActionsModel->InvoiceLineItem($InvoiceID); 
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Line item saved successfully.');
            redirect('Invoice/Actions/InvoiceDetails/'.$InvoiceID);

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Invoice/Actions/InvoiceDetails/'.$InvoiceID);
        }
    }

    function GetAccountDetails($AccountID)
    {
        echo $this->ActionsModel->GetAccountDetails($AccountID); 
    }

    function GetContactDetails($ContactID)
    {
        echo $this->ActionsModel->GetContactDetails($ContactID); 
    }

    function GetProductDetails($ProductID)
    {
        echo $this->ActionsModel->GetProductDetails($ProductID); 
    }

    function GetPrimaryContactOptions()
    {
        echo $this->ActionsModel->GetPrimaryContactOptions(); 
    }

    function SaveScheduling()
    {
        echo $this->ActionsModel->SaveScheduling(); 
    }

    function GetScheduling($EventID)
    {
        echo $this->ActionsModel->GetScheduling($EventID); 
    }

    function UpdateScheduling($EventID)
    {
        echo $this->ActionsModel->UpdateScheduling($EventID); 
    }

    function DeleteScheduling()
    {
        echo $this->ActionsModel->DeleteScheduling();
    }

    function EditSchAssignedTo()
    { 
        echo $this->ActionsModel->EditSchAssignedTo();
    }

    function UpdateAssignedTo()
    {
         echo $this->ActionsModel->UpdateAssignedTo();
    }

    function getState(){
        echo MY_Model::getState($_POST['CountryID']);
    }

    function getCity(){
        echo MY_Model::getCity($_POST['StateID']);
    } 

    function DeleteInvoice($InvoiceID){
        $result =  $this->ActionsModel->DeleteInvoice($InvoiceID);
        if($result){

            $this->session->set_flashdata('SuccessMsg', 'Work Order deleted successfully.');
            redirect('Invoice/Actions');

        } else {

            $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
            redirect('Invoice/Actions/InvoiceDetails/'.$InvoiceID); 
        }
    }

    function GenerateDocument($InvoiceID) { 

        $data =  $this->ActionsModel->getInvoiceData($InvoiceID, 'InvoiceLineItems'); 

        if(isset($_REQUEST['GenerateDocument'])){

            $result = $this->ActionsModel->GenerateDocument($data);
            if($result == 2){
                
                $this->session->set_flashdata('ErrorMsg', 'Somthing went wrong.');
                redirect('Invoice/Actions/GenerateDocument/'.$InvoiceID); 
                
            } else {

                $data['PDFOutputFile'] = $result;
                $this->load->view('Invoice/GenerateDocument', $data);
            }
        } else {
            $this->load->view('Invoice/GenerateDocument', $data);

        }
    }
}
